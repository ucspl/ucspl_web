export class SystemConstants {
  static readonly SYSTEM_DOMAIN = "http://localhost:8090";
  static readonly PATH_URL = "/api/v1";

  static readonly MANAGE_APPROVERS_SEARCH_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + "/search_and_modify_user_by_documentType";

  // create user urls
  static readonly CREATE_USER_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/users';
  static readonly BRANCH_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/branch';
  static readonly DEPARTMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/department';
  static readonly   Security_Question_List_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/securityQuestion';
  static readonly CUSTOMER_LIST_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/customerlist';
  static readonly ROLE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/role';
  static readonly FORGOT_PASSWORD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/forgot_password';

  // manage-approvers urls
  static readonly SEARCH_AND_MODIFY_USER_BY_DOCUMENT_TYPE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srhusr_by_docty';
  static readonly SEARCH_AND_MODIFY_USER_BY_DOCUMENT_TYPE_ADDUSER_TABLE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srhusr_by_docty_adduserT';
  static readonly ADDUSER_TABLE_IN_DATABASE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/adduserT_in_DB';
  static readonly EXISTINGUSER_TABLE_IN_DATABASE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/extuserT_in_DB';
  static readonly MANAGE_APPROVERS_DOCUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/document';

  // search-user urls
  static readonly SEARCH_USER_BY_NAME_DEPT = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/search_user';
  static readonly SP_GET_CREATE_USERS_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CUserDetails';

  // create-customer urls
  static readonly CREATE_CUSTOMER = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/customers';
  static readonly REGIONAREACODE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/regionareacode';
  static readonly BILLING_CREDIT_PERIOD = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/billcredperiod';
  static readonly TAXES = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/taxes';
  static readonly ADDING_CUSTOMER_TAX_ID_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/addCustTaxId';
  static readonly GET_CUSTOMER_TAX_ID_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getCustTaxId';
  static readonly ADDING_CUSTOMER_TAX_ID_URL_TRIAL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/addCustTaxId1';
  static readonly CUSTOMER_DETAILS_FOR_ID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CCustDetails';
  static readonly CUSTOMER_TAXPAYER_TYPE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/taxptype';

  static readonly COUNTRY = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/country';
  static readonly STATES = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/states';

  static readonly CUST_CONTACT_LIST = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/custConList';
  static readonly GET_CUST_CONTACT_LIST = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getcustConList';
  static readonly CUST_ATTACHMENTS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/custAttach';
  static readonly CUST_ATTACHMENTS_GET_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getCustAttFiles';


  static readonly CUST_CONTACT_LIST_BY_CUST_ID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/conListForCustId';
  static readonly CUST_ATTACHMENTS_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadCustAttach';
  static readonly GET_SELECTED_CUSTATTACH_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getSelCustAttach';


  // search-customer urls
  static readonly SEARCH_CUST = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/search_cust';

  //create-po urls
  static readonly UPDATE_REMAINING_QUANTITY= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/updateRemQuantity';
  static readonly UPDATE_REMAINING_PO_VALUE= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/updateRemPoValue';

  static readonly CREATE_CUST_PO = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createcustpo';
  static readonly DOC_NO_FOR_PO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getPODocumentNo';
  static readonly CREATE_PO_DOCUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/poDoc';
  static readonly SP_SR_NO_PO_DOC_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnopoDoc';
  static readonly PO_ATTACHMENTS_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadPoAttach';
  static readonly PO_ATTACHMENTS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/PoAttach';

  static readonly PO_ATTACHMENTS_GET_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getPOAttFiles';
  static readonly GET_SELECTED_POATTACH_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getSelPOAttach';
  static readonly PO_ATTACHMENTS_FOR_DETETE_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/deleteAttachedPoFile';

  // po-document-calculation-urls
static readonly SP_DOC_DETAILS_PO_DOC_CAL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/PoDocCalDet';
static readonly PO_DOCUMENT_CAL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/PoDocCal';

// serach po

  static readonly SEARCH_PO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchPo';
  static readonly SP_GET_CREATE_PO_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createPoDetails';
  static readonly DELETE_PO_ROW_FOR_SR_NO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/deletePoRow';


  //create-sales urls
  static readonly CREATE_SALES_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createSales';
  static readonly ENQUIRY_ATTACHMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadEq';
  static readonly QUOTATION_ITEM_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadQuot';
  static readonly ADDING_CUSTOMER_TERM_ID_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/AddCustTermsId';
  static readonly REMOVE_CUSTOMER_TERM_ID_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DelCustTermsId';
  static readonly REMOVE_ALL_CUSTOMER_TERM_ID_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DelAllCustTermsId';
  static readonly DOC_NO_FOR_SALES_QUOT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DocNoForSales';
  static readonly CREATE_SALES_QUOT_DOCUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/SalesQuotDoc';
  static readonly TERMS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL +'/terms';
  static readonly QUOTATION_TYPE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL +'/quotationtype';
  static readonly ENQUIRY_ATTACHMENTS_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadEnqAttach';
  static readonly ENQUIRY_ATTACHMENTS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/EnqAttach';
  static readonly REFERENCE_FOR_SALES_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/referenceforsales';
  static readonly INSTRUMENT_LIST_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/instruList';
  static readonly DELETE_SALES_ROW_FOR_SR_NO_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/deleteSalesRow';
  //search-sales urls
  static readonly SEARCH_SALES_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchSales';
  static readonly SP_GET_CREATE_SALES_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CSalesDet';
  static readonly SP_GET_TERMS_ID_FOR_SEARCH_SALES_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CSalesTerms';

  //sales-quotation-document-urls
  static readonly SP_SR_NO_SALES_QUOT_DOC_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoSQDoc';

  // sales-quotation-document-calculation-urls
  static readonly SP_DOC_DETAILS_SALES_QUOT_DOC_CAL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/SQDocCalDet';
  static readonly SALES_QUOT_DOCUMENT_CAL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/SalesQuotDocCal';

  //CreateChallanCumInvoice Urls
  static readonly CREATE_INVOICE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createInv';
  static readonly DOC_NO_FOR_CHALLAN_CUM_INVOICE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DocNoForInv';
  static readonly INVOICE_DOCUMENT_CREATE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/invDoc';
  static readonly UPDATE_UNIQUE_NO_FOR_INVOICE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/updateUniqueInvNo';
  static readonly INVOICE_DOCUMENT_CALCULATION_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/invDocCal';
  static readonly SP_SR_NO_INVOICE_DOC_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srInvDoc';
  static readonly SP_INC_DOCUMENT_CAL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/IncDocCal';
  static readonly DOCUMENT_TYPE_FOR_INVOICE_URL=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DocTypeOfInv';
  static readonly INVOICE_TYPE_FOR_INVOICE_URL=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/InvoiceTypeOfInv';
  static readonly FINANCIAL_YEAR_DATE_URL=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/financialYear';

  static readonly DELETE_INVOICE_ROW_FOR_SR_NO_URL=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/deleteInvoiceRow';

  static readonly SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_CUST_ID_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqInfoForCaliTagAndInvoiceTagForCustId';
  static readonly SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqInfoForCaliTagAndInvoiceTagForReqDocNo';
  static readonly TRIAL_SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialreqInfoForCaliTagAndInvoiceTagForReqDocNo';
  static readonly TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_INSTRU_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetInvoiceTagToOneForInstru';
  static readonly TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_INSTRU_THROUGH_INVID_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetInvoiceTagToOneForInstruThroughInvId';
  static readonly TRIAL_SP_SET_INVOICE_TAG_TO_ZERO_FOR_INSTRU_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetInvoiceTagToZeroForInstru';
  static readonly TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_RANGE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetInvoiceTagToOneForRange';
  static readonly TRIAL_SP_SET_INVOICE_TAG_TO_ZERO_FOR_RANGE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetInvoiceTagToZeroForRange';
  static readonly SP_PO_DATA_FOR_PO_NO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/poDataForPoNo';
   static readonly SP_GET_ALL_PO_NUMBERS_FOR_CUST_ID =SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/allPoNumbersCustId';
   static readonly SAVE_REQUEST_NUMBER_FOR_INVOICE=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/saveRequsetNoForInvoice';
   static readonly SP_GET_ALL_REQ_NUMBERS_FOR_INVOICE_DOC_NO=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getAllRequstNoForInvoiceDocNo';
   static readonly SP_SEND_DOC_NO_FOR_GETTING_INVOICE_DATA=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getAllInvoiceDataWithInvoiceTagForInvDocNo';
   static readonly SP_REMOVE_ALL_REQ_NO_FOR_INV_DOC_NO=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/removeAllReqNoForInvDocNo';
   static readonly SP_REMOVE_ALL_SR_NO_FOR_INV_DOC_NO=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/removeAllSrNoForInvDocNo';
   static readonly SP_INVOICE_TAG_TO_ONE_FOR_REQ_NO_AND_INV_NO_URL=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/setInvTagToZeroForReqNoAndInvNo';
   static readonly SP_SET_INV_TAG_AND_UNI_NO_TO_ZERO_FOR_INV_DOC_NO=SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/setInvAndUniNoToZeroForInvDocNo';
   
   //proforma Invoice 
static readonly DOC_NO_FOR_PROFORMA_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DocNoForProforma';
static readonly CREATE_PROFORMA_INVOICE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createproforma';
static readonly PROFORMA_DOCUMENT_CREATE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/proformaDoc';
static readonly PROFORMA_DOCUMENT_CALCULATION_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/proformaDocCal';
static readonly SP_SR_NO_PROFORMA_DOC_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srProformaDoc';
static readonly SP_PROFORMA_DOCUMENT_CAL_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/proDocCal';
static readonly DELETE_PROFORMA_ROW_FOR_SR_NO_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/deleteProformaRow';
//static readonly SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqInfoForCaliTag';

// search proforma invoice
static readonly SEARCH_PROFORMA_INVOICE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchProforma';
static readonly SP_GET_CREATE_PROFORMA_INVOICE_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CProformaInvsDet';



  //Search-ChallanCumInvoice Urls
  static readonly SEARCH_INVOICE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchInvs';
  static readonly SP_GET_CREATE_INVOICE_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CInvsDet';

//crete Delivery Challan
static readonly DOC_NO_FOR_DELIVERY_CHALLAN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/DocNoForDeliChallan';
static readonly CREATE_DELIVERY_CHALLAN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createDeliChallan';
static readonly CREATE_SR_NO_DELI_CHALLAN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createSrNoDeliChallan';
static readonly UPDATE_UNIQUE_NO_FOR_DELI_CHALLAN = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/updateUniqueNoForsrNoDeliChallan';

static readonly SP_REQ_AND_INW_DATA_FOR_CALIBRATION_DC_AND_INVOICE_TAG_FOR_CUST_ID_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqInfoForCaliDeliAndInvoiceTagForCustId';
static readonly TRIAL_SP_REQ_AND_INW_DATA_FOR_CALIBRATION_DC_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialreqInfoForCaliDeliAndInvoiceTagForReqDocNo';
static readonly SP_SEND_DOC_NO_FOR_GETTING_DELIVERY_CHALLAN_DATA= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getAllDcDataWithDcTagForDcDocNo';
static readonly SP_SR_NO_DELI_CHALLAN_DOC_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srNoDataForDeliChallanDoc';

static readonly TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_INSTRU_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetDcTagToOneForInstru';
static readonly TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_INSTRU_THROUGH_INVID_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetDcTagToOneForInstruThroughInvId';
static readonly TRIAL_SP_SET_DC_TAG_TO_ZERO_FOR_INSTRU_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetDcTagToZeroForInstru';
static readonly TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_RANGE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetDcTagToOneForRange';
static readonly TRIAL_SP_SET_DC_TAG_TO_ZERO_FOR_RANGE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/trialsetDcTagToZeroForRange';



  //Create Supplier Urls
  static readonly CREATE_SUPPLIER = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/supplier';
  static readonly SUPPLIER_TYPE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/supplierType';


  //search suppliers Urls
  static readonly SEARCH_SUPPLIER = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchSupplier';
  static readonly SUPPLIER_DETAILS_FOR_ID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CSuppDetails';
 
  static readonly SUPP_ATTACHMENTS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/suppAttach';
  static readonly SUPP_ATTACHMENTS_GET_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getSuppAttFiles';

  static readonly SUPP_ATTACHMENTS_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadSuppAttach';
  static readonly GET_SELECTED_SUPPATTACH_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getSelSuppAttach';

  
// Definition
static readonly CATEGORY_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/definitioncategory';
static readonly CREATE_BRANCH_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createbranch';
static readonly CREATE_ROLE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createrole';
static readonly CREATE_DEPARTMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createdepartment';
static readonly CREATE_Security_Question_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createSecurityQuestion';
static readonly CREATE_COUNTRY_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createCountry';
static readonly CREATE_REGION_AREA_CODE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createRegionAreaCode';
static readonly CREATE_BILLING_CREDIT_PERIOD_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createBillingCreditPeriod';
static readonly CREATE_TAXPAYER_TYPE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createTaxpayerType';
static readonly CREATE_TYPE_OF_QUOTATION_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createTypeOfQuotation';
static readonly CREATE_REFERENCE_NUMBER_FOR_QUOTATION_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createRefNoForQuotation';
static readonly CREATE_INSTRUMENT_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createInstrument';
static readonly CREATE_TERMS_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createTerms';
static readonly CREATE_SUPPLIER_TYPE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createSupplierType';
static readonly CREATE_INWARD_TYPE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createInwardType';
static readonly CREATE_REQUEST_TYPE_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createRequestType';
static readonly CREATE_FINANCIAL_YEAR_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createFinancialYear';
static readonly SEARCH_DEFINITION_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchdefinition';
static readonly SP_GET_CREATE_DEFINITION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createDefDetail';

                

  //create and get functionalities for unc dropdowns
  static readonly CREATE_UNC_MASTER_TYPE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncMasterType';
  static readonly CREATE_UNC_MASTER_NUMBER_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncMastNo';
  static readonly CREATE_UNC_MASTER_NAME_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncMastName';
  static readonly CREATE_UNC_PROBABILITY_DISTRIBUTION_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncProbDistribu';
  static readonly CREATE_UNC_SOURCE_OF_UNCERTAINTY_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncSouOfUncer';
  static readonly CREATE_UNC_TYPE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncType';
  static readonly CREATE_UNC_DIVIDING_FACTOR_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncDivFactor';
  static readonly CREATE_UNC_DEGREE_OF_FREEDOM_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncDeFredom';


// Cerrtificate Table_________________________Nabl Scope Master
static readonly CREATE_NABL_SCOPE_MASTER_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtNablScopeMaster';
static readonly GET_GROUP_FOR_NABL_SCOPE_MASTER_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/groupForNabl';
static readonly GET_NABL_SCOPE_NAME_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/nablScopeName';
static readonly CREATE_NABL_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtNablSpeDet';
static readonly SP_SRNO_NABL_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srNoNablDet'; 
static readonly SP_SRNO_NABL_SPECIFICATION_DETAILS_BY_NABLID_PARANAME_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoNablSpecDetByNablIdPn';

// Search Nabl
static readonly SEARCH_NABL_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchNabl';
 static readonly SP_GET_CREATE_NABL_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createNablDet';
// static readonly SP_GET_TERMS_ID_FOR_SEARCH_SALES_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CSalesTerms';
static readonly SP_GET_CREATE_NABL_SPECIFICATION_DETAILS_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createNablSpecDet';




//request and inward urls
static readonly CREATE_REQUEST_AND_INWARD = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/createreq';
static readonly GET_CHILD_CUSTOMERS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getChildCust';
static readonly GET_REQUEST_AND_INWARD_TYPES = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/InwType';
static readonly GET_REQ_TYPES_OF_REQUEST_AND_INWARDS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqType';
static readonly DOC_NO_FOR_REQUEST_INWARD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/docNoForReq';
static readonly SP_COUNTER_DOC_NO_FOR_REQUEST_INWARD__URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/countDNoForReq';
static readonly CUSTOMER_INSTRUMENT_IDENTIFICATION_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/cusInsruIdenty';

static readonly REQUEST_INWARD_DOCUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqIwdDoc';
static readonly REQ_DOC_CUSTOMER_INSTRU_RELATION_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/reqDocCusInsIden';
static readonly SP_FOR_REQ_DOC_CUSTOMER_INSTRU_RELATION_BY_CUSTINSTRU_IDENTIFICATION_ID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoReqDocCusInsIden';
static readonly SP_FOR_REQ_DOC_CUSTOMER_INSTRU_RELATION_BY_CREATE_REQ_IWD_TABLE_ID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoReqDCusInsIdenByCRIId';



static readonly REQUEST_INWARD_DOCUMENT_FOR_UPDATECRECERTIURL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uReqIwdDocCer';
static readonly INSTRUMENT_LIST_REQ_IWD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/instruList';
static readonly PARAMETER_LIST_REQ_IWD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/parameterList';
static readonly UOM_LIST_REQ_IWD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uomList';

// request and inward instrument list sr no urls
static readonly SP_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoRIDoc';
static readonly SP_TRIAL_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoIdentiRIDoc';
static readonly SP_GET_CUSTINSTRUID_FOR_REQDOCNO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/ciIdForReqNo';

// static readonly SP_SR_NO_REQ_IWD_INSTRU_LIST_WITH_NAMES_SCREEN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnonmRIDoc';
// static readonly SP_SR_NO_REQ_IWD_INSTRU_LIST_WITH_NAMES_BY_INSTRUNO_SCREEN_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnmRIDocInstrNo';
static readonly SP_MULTIPARA_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mulParaSN';
static readonly FORMULA_ACCURACY_LIST_REQ_IWD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/formulaAccuList';

// search-request and inward urls
static readonly SEARCH_REQUEST_AND_INWARD_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchReq';
static readonly SP_GET_CREATE_REQUEST_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CReqDet';

// certificate tab urls        
static readonly CREATE_MASTER_INSTRU_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtMInstruDet';
static readonly CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtMSpeD';
static readonly CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtMCCerD';
static readonly SP_SRNO_MASTER_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoMSDet'; 
static readonly SP_SRNO_MASTER_SPECIFICATION_DETAILS_BY_MASTID_PARANAME_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoMSDetByMipn';
static readonly SP_SRNO_MASTER_CAL_CERTIFICATE_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/snMCalCerDet'; 
static readonly SP_SRNO_MASTER_CAL_CERTIFICATE_DETAILS_BY_STATUS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/snMCalCerDetBySt'; 

static readonly GET_MASTER_TYPES_OF_MASTERINSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mMasType';
static readonly GET_CALIBRATIONLAB_TYPES_OF_MASTERINSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mCLType';
static readonly GET_INSTRUMENT_TYPES_OF_MASTERINSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mInsType';
static readonly GET_RANGE_TYPES_OF_MASTERINSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mRangeType'; 
static readonly GET_MODE_TYPES_OF_MASTERINSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mModeType';   
static readonly GET_TEMP_CHART_TYPES_OF_MASTERINSTRUMENT_URL =  SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/temChart';   
static readonly GET_CALIBRATION_AGENCY_TYPES_OF_MASTERINSTRUMENT_URL =  SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/calAgency'; 
static readonly GET_FREQUENCY_UOM_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/frequencyUom';
static readonly GET_UNC_FORMULA_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uncFormula';

static readonly MAST_CAL_CERTIFICATE_ATTACHMENTS = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mICalCertAttach';
static readonly MAST_CAL_CERTIFICATE_ATTACHMENTS_GET_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getMICalCertAttach';
static readonly MAST_CAL_CERTIFICATE_ATTACHMENTS_FILE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/uploadMICalCertAttach';
static readonly GET_SELECTED_MAST_CAL_CERTIFICATEATTACH_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/getSelMICalCertAttach';
static readonly MAST_CAL_CERTIFICATE_ATTACHMENTS_FOR_DETETE_FILE = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/delSelMICalCertAttach';
static readonly MAST_CAL_CERTIFICATE_ATTACHMENTS_GET_BY_MASTID_AND_CALID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/mICalCAttach';
static readonly DELETE_MAST_CAL_CERTIFICATE_ATTACHMENTS_BY_CALID = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/delMICalCAttByCalId';

static readonly CREATE_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL= SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtCRDForPara';
static readonly SP_SRNO_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL =  SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoCRDForPara';
static readonly SP_SRNO_CALIBRATION_RESULT_DATA_FOR_CREATEMASTERINSTRUID_URL =  SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoCRDForCreeMInstId';

static readonly CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_FOR_ARROVE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtMSpeDFrAppro';
static readonly CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_PARANO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtMSpeDFrParaNo';
static readonly SP_SRNO_MASTER_CAL_SPEC_DETAILS_BY_MASTID_STATUS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/snMCalSpetByMastIdSt';
static readonly SP_UPDATE_SRNO_CALIBRATION_RESULT_DATA_FOR_UNIQUESRNO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtCRDForUniqueNo';
static readonly SP_SRNO_MI_SPECIFI_DETAILS_BY_MASTID_PARANAME_FOR_CALIBRATION_RESULT_DATA_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoMSDetByMipnForCRD';



// search - master instrument urls
static readonly SEARCH_MASTER_INSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchMI';
static readonly SP_GET_CREATE_MASTER_INSTRUMENT_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CMInstruDet';



// certificate tab urls        



//Certificate Tab_________________________Uuc name master  
static readonly CREATE_UUC_NAME_MASTER_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUucNmMaster';
static readonly CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUucNmMSpe';
static readonly SP_SRNO_UUC_MASTER_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoUucMSDet';
static readonly SP_UPDATE_SRNO_UUC_PARA_SPECIFICATION_FOR_PARANO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtCUMIForParaNo';








//Certificate Tab_________________________UNC name master                  
static readonly CREATE_UNC_NAME_MASTER_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncNmMaster';
static readonly CREATE_UNC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUncNmMSpe';
static readonly SP_SRNO_UNC_MASTER_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoUncMSDet';
static readonly SP_UPDATE_SRNO_UNC_PARA_SPECIFICATION_FOR_PARANO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtCUncMIForParaNo';

//Unc search screen                                                                                                                                                                                                                                                                                                                                                                       
static readonly SEARCH_UNC_MASTER_INSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchUncMaster';




//search - Env master instrument urls
static readonly SEARCH_ENV_MASTER_INSTRUMENT_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchEnvMI';
static readonly SP_GET_CREATE_ENV_MASTER_INSTRUMENT_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/CEnvMInstruDet';
static readonly GET_LAST_CREATED_ENVIRONMENTAL_MASTER_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/lastCEnvMInstru';
static readonly GET_SECOND_LAST_CREATED_ENVIRONMENTAL_MASTER_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/secLastCEnvMInstru';

static readonly UPDATE_TO_DATE_OF_ENVIRONMENTAL_MASTER_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtEnvMToDate';
static readonly SAVE_UOM_AND_UUC_SPECI_DATA_IN_JUNC_TABLE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/saveUomToUucJunTable';
static readonly SAVE_INPUT_UOM_AND_UUC_SPECI_DATA_IN_JUNC_TABLE_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/saveIpUomToUucJunTable';



//certificate tab ____Environmental master screen
static readonly CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtEnvMaster';

//Certificate Tab_________________________UOM name master                  
static readonly CREATE_UOM_CALCULATOR_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUomCal';
static readonly CREATE_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtUomCalSpe';
static readonly SP_SRNO_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/srnoUomCalSDet';
static readonly SP_UPDATE_SRNO_UOM_CALCULATOR_SPECIFICATION_FOR_PARANO_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/crtCUomCalForParaNo';



//UOM search screen                                                                                                                                                                                                                                                                                                                                                                       
static readonly SEARCH_UOM_CALCULATOR_URL = SystemConstants.SYSTEM_DOMAIN + SystemConstants.PATH_URL + '/searchUomCal';













}