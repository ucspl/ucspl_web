import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { enableProdMode } from '@angular/core';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';

//Modules
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgQRCodeReaderModule } from 'ng2-qrcode-reader'; 

// Services
import { AuthService } from './services/auth/auth.service';
import { UserService } from './services/user/user.service';
import { StudentService } from './services/student/student.service';
import { EmployeeService } from './services/serviceconnection/employee-service.service';
import { CreateUserService } from './services/serviceconnection/create-service/create-user.service';
import { SearchUserService } from './services/serviceconnection/search-service/search-user.service';
import { MngApproversService } from './services/serviceconnection/create-service/mng-approvers.service';
import { CreateCustomerService } from './services/serviceconnection/create-service/create-customer.service';
import { SearchCustomerService } from './services/serviceconnection/search-service/search-customer.service';
import { CreateCustPoService } from './services/serviceconnection/create-service/create-cust-po.service';
import { FileUploadService } from './services/serviceconnection/create-service/file-upload.service';
import { CreateSalesService } from './services/serviceconnection/create-service/create-sales.service';
import { SearchSalesService } from './services/serviceconnection/search-service/search-sales.service';
import { CreateInvoiceService  } from './services/serviceconnection/create-service/create-invoice.service';
import { SearchInvoiceService } from './services/serviceconnection/search-service/search-invoice.service';
import { CreateSupplierService } from './services/serviceconnection/create-service/create-supplier.service';
import { SearchSupplierService } from './services/serviceconnection/search-service/search-supplier.service';
import{CreateDefService} from './services/serviceconnection/create-service/create-def.service';
//import {SearchDefinitionService } from './services/serviceconnection/search-service/search-definition.service';
import {CreateReqAndInwardService} from './services/serviceconnection/create-service/create-req-and-inward.service';
import {SearchPoService} from './services/serviceconnection/search-service/search-po.service';
//import { CreateReqAndInwardService } from './services/serviceconnection/create-service/create-req-and-inward.service';
//import { SearchReqAndInwardService } from './services/serviceconnection/search-service/search-req-and-inward.service';
import {CreateNablScopeService} from './services/serviceconnection/create-service/create-nabl-scope.service';

import{SearchNablService} from './services/serviceconnection/search-service/search-nabl.service';
import{CreateProformaService} from './services/serviceconnection/create-service/create-proforma.service';
import{SearchProformaInvoiceService} from './services/serviceconnection/search-service/search-proforma-invoice.service';
import{CreateDeliChallanService} from './services/serviceconnection/create-service/create-deli-challan.service';

import { ExcelServiceService} from './services/serviceconnection/create-service/excel-service.service';

// Pipes
import { FilterPipe } from './pipes/filter.pipe';
import { PhonePipe } from './pipes/phone.pipe';
import { DatePipe } from '@angular/common';

// Components
import { AppComponent } from './components/index/app.component';
import { StudentListComponent } from './components/student/list/student-list.component';
import { StudentDetailsComponent } from './components/student/details/student-details.component';
import { StudentAddComponent } from './components/student/add/student-add.component';
import { HomeComponent, homeChildRoutes } from './components/home/home.component';
import { HighlightStudentDirective } from './directives/highlight-student.directive';
import { RegistrationComponent } from './components/registration/registration.component';
import { RegisterDetailsComponent } from './components/student/register-details/register-details.component';
import { GetinTouchComponent } from './components/getin-touch/getin-touch.component';
import { AddressComponent } from './components/address/address.component';
import { EmailUsComponent } from './components/email-us/email-us.component';
import { CallUsComponent } from './components/call-us/call-us.component';
import { EmployeeListComponent } from './components/employee/employee-list/employee-list.component';
import { CreateEmployeeComponent } from './components/employee/create-employee/create-employee.component';
import { UpdateEmployeeComponent } from './components/employee/update-employee/update-employee.component';
import { EmployeeDetailsComponent } from './components/employee/employee-details/employee-details.component';



import { ValidationComponent } from './components/validation/validation.component';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
//import { ManageApproversComponent } from './components/manage-approvers/manage-approvers.component';
//import { UserTabCreateUpdateUserComponent } from './components/user-tab-create-update-user/user-tab-create-update-user.component';
//import { UserTabSearchAndModifyUserComponent } from './components/user-tab-search-and-modify-user/user-tab-search-and-modify-user.component';
import { UsrCreateComponent } from './components/user/usr-create/usr-create.component';
import { UsrSearchComponent } from './components/user/usr-search/usr-search.component';
import { UsrManageComponent } from './components/user/usr-manage/usr-manage.component';
import { UsrUpdateComponent } from './components/user/usr-update/usr-update.component';


//import { CustomerTabCustomerMasterSearchAndModifyCustomerComponent } from './components/customer-tab-customer-master-search-and-modify-customer/customer-tab-customer-master-search-and-modify-customer.component';
//import { CustomerTabCustomerMasterSearchAndModifyPaymentReceiptComponent } from './components/customer-tab-customer-master-search-and-modify-payment-receipt/customer-tab-customer-master-search-and-modify-payment-receipt.component';
////import { CustomerTabCustomerMasterAddCustomerComponent } from './components/customer-tab-customer-master-add-customer/customer-tab-customer-master-add-customer.component';
//import { CustomerTabCreateCustomerPurchaseOrderComponent } from './components/customer-tab-create-customer-purchase-order/customer-tab-create-customer-purchase-order.component';
////import { CustomerTabSearchAndModifyCustomerPurchaseOrderComponent } from './components/customer-tab-search-and-modify-customer-purchase-order/customer-tab-search-and-modify-customer-purchase-order.component';
//import { CustomerTabCustomerLedgerComponent } from './components/customer-tab-customer-ledger/customer-tab-customer-ledger.component';
import { CustCreateComponent } from './components/customer/customer-master/cust-create/cust-create.component';
import { CustSearchComponent } from './components/customer/customer-master/cust-search/cust-search.component';
import { CustUpdateComponent } from './components/customer/customer-master/cust-update/cust-update.component';
import { SearchPtmReceiptComponent } from './components/customer/customer-master/search-ptm-receipt/search-ptm-receipt.component';
import { CreatePoComponent } from './components/customer/customer-purchase-order/create-po/create-po.component';
import { SearchPoComponent } from './components/customer/customer-purchase-order/search-po/search-po.component';
import { LedgerComponent } from './components/customer/ledger/ledger.component';
import { SearchSmryRepComponent } from './components/customer/customer-summary-report/search-smry-rep/search-smry-rep.component';
import { PrtSmryRepComponent } from './components/customer/customer-summary-report/prt-smry-rep/prt-smry-rep.component';
import { PrtDueLtrComponent } from './components/customer/customer-summary-report/prt-due-ltr/prt-due-ltr.component';
//import { CustomerTabCalibrationSearchAndModifyCustomerSummaryReportComponent } from './components/customer-tab-calibration-search-and-modify-customer-summary-report/customer-tab-calibration-search-and-modify-customer-summary-report.component';


import { DefCreateComponent } from './components/definition/def-create/def-create.component';
import { DefSearchComponent } from './components/definition/def-search/def-search.component';
//import { DefUpdateComponent } from './components/definition/def-update/def-update.component';
//import { DefintionTabSearchAndModifyDefintionComponent } from './components/defintion-tab-search-and-modify-defintion/defintion-tab-search-and-modify-defintion.component';


import { SearchDeliChnComponent } from './components/invoice/search-deli-chn/search-deli-chn.component';
//import { SearchProIncComponent } from './components/invoice/search-pro-inc/search-pro-inc.component';
import { CreateInvdocComponent } from './components/invoice/create-delivery-challan/create-invdoc/create-invdoc.component';
import { SearchCustInvComponent } from './components/invoice/create-delivery-challan/search-cust-inv/search-cust-inv.component';

//import { InvoiceTabSearchAndModifyDeliveryChallanCumInvoiceComponent } from './components/invoice-tab-search-and-modify-delivery-challan-cum-invoice/invoice-tab-search-and-modify-delivery-challan-cum-invoice.component';
//import { InvoiceTabSearchAndModifyProformaInvoiceComponent } from './components/invoice-tab-search-and-modify-proforma-invoice/invoice-tab-search-and-modify-proforma-invoice.component';


//import { RequestAndInwardTabMyRequestForCollectionComponent } from './components/request-and-inward-tab-my-request-for-collection/request-and-inward-tab-my-request-for-collection.component';
//import { RequestAndInwardTabRequestForOnsiteCalibrationComponent } from './components/request-and-inward-tab-request-for-onsite-calibration/request-and-inward-tab-request-for-onsite-calibration.component';
//import { RequestAndInwardTabMyRequestForOnsiteCalibrationComponent } from './components/request-and-inward-tab-my-request-for-onsite-calibration/request-and-inward-tab-my-request-for-onsite-calibration.component';
//import { RequestAndInwardTabSearchAndModifyRequestAndInwardComponent } from './components/request-and-inward-tab-search-and-modify-request-and-inward/request-and-inward-tab-search-and-modify-request-and-inward.component';
//import { RequestAndInwardTabToDoListForCalibrationComponent } from './components/request-and-inward-tab-to-do-list-for-calibration/request-and-inward-tab-to-do-list-for-calibration.component';
//import { RequestAndInwardTabSearchPendingRequestAndInwardForCalibrationComponent } from './components/request-and-inward-tab-search-pending-request-and-inward-for-calibration/request-and-inward-tab-search-pending-request-and-inward-for-calibration.component';
//import { RequestAndInwardTabSearchPendingRequestAndInwardForCertificationComponent } from './components/request-and-inward-tab-search-pending-request-and-inward-for-certification/request-and-inward-tab-search-pending-request-and-inward-for-certification.component';
//import { RequestAndInwardTabToDoListForRepairComponent } from './components/request-and-inward-tab-to-do-list-for-repair/request-and-inward-tab-to-do-list-for-repair.component';
//import { RequestAndInwardTabSearchPendingRequestAndInwardsForRepairComponent } from './components/request-and-inward-tab-search-pending-request-and-inwards-for-repair/request-and-inward-tab-search-pending-request-and-inwards-for-repair.component';
import { ReqCreateComponent } from './components/request-and-inward/request-inwards/req-create/req-create.component';
import { ReqSearchComponent } from './components/request-and-inward/request-inwards/req-search/req-search.component';
import { ReqColleComponent } from './components/request-and-inward/request-collection/req-colle/req-colle.component';
import { MyReqColleComponent } from './components/request-and-inward/request-collection/my-req-colle/my-req-colle.component';
import { InwColleInstruComponent } from './components/request-and-inward/request-collection/inw-colle-instru/inw-colle-instru.component';
import { ReqOsCaliComponent } from './components/request-and-inward/request-collection/req-os-cali/req-os-cali.component';
import { MyReqOsCaliComponent } from './components/request-and-inward/request-collection/my-req-os-cali/my-req-os-cali.component';
import { InwOsCaliComponent } from './components/request-and-inward/request-collection/inw-os-cali/inw-os-cali.component';
import { ServiceListComponent } from './components/request-and-inward/to-do-list/service/service-list/service-list.component';
import { ServiceSearchComponent } from './components/request-and-inward/to-do-list/service/service-search/service-search.component';
import { CaliListComponent } from './components/request-and-inward/to-do-list/calibration/cali-list/cali-list.component';
import { CaliSearchComponent } from './components/request-and-inward/to-do-list/calibration/cali-search/cali-search.component';
import { CertiListComponent } from './components/request-and-inward/to-do-list/certification/certi-list/certi-list.component';
import { CertiSearchComponent } from './components/request-and-inward/to-do-list/certification/certi-search/certi-search.component';
import { DeliListComponent } from './components/request-and-inward/to-do-list/delivery/deli-list/deli-list.component';
import { DeliSearchComponent } from './components/request-and-inward/to-do-list/delivery/deli-search/deli-search.component';
import { RepairListComponent } from './components/request-and-inward/to-do-list/repair/repair-list/repair-list.component';
import { RepairSearchComponent } from './components/request-and-inward/to-do-list/repair/repair-search/repair-search.component';
//import { SupplierTabSearchAndModifySupplierComponent } from './components/supplier-tab-search-and-modify-supplier/supplier-tab-search-and-modify-supplier.component';
//import { SupplierTabSearchAndModifyDeliveryChallanPurchaseOrderComponent } from './components/supplier-tab-search-and-modify-delivery-challan-purchase-order/supplier-tab-search-and-modify-delivery-challan-purchase-order.component';
//import { SupplierTabCreateDeliveryChallanPurchaseOrderComponent } from './components/supplier-tab-create-delivery-challan-purchase-order/supplier-tab-create-delivery-challan-purchase-order.component';
import { SuppCreateComponent } from './components/supplier/supplier-master/supp-create/supp-create.component';
import { SuppSearchComponent } from './components/supplier/supplier-master/supp-search/supp-search.component';
import { CreateDcPoComponent } from './components/supplier/delivery-challan-or-purchase-order/create-dc-po/create-dc-po.component';
import { SearchDcPoComponent } from './components/supplier/delivery-challan-or-purchase-order/search-dc-po/search-dc-po.component';


//import {SalesAndQuotationTabCreateSalesAndQuotationComponent } from './components/sales-and-quotation-tab-create-sales-and-quotation/sales-and-quotation-tab-create-sales-and-quotation.component';
//import { SalesAndQuotationTabCreateSalesAndQuotationComponent } from './components/sales-and-quotation-tab-create-sales-and-quotation/sales-and-quotation-tab-create-sales-and-quotation.component';
//import { SalesQuotationDocumentForCreateSalesQuotationComponent } from './components/sales-quotation-document-for-create-sales-quotation/sales-quotation-document-for-create-sales-quotation.component';
//import { SalesQuotationTabSearchAndModifySalesQuatationComponent } from './components/sales-quotation-tab-search-and-modify-sales-quatation/sales-quotation-tab-search-and-modify-sales-quatation.component';
//import { ManageDocumentOfSalesQuotationDocumentComponent } from './components/manage-document-of-sales-quotation-document/manage-document-of-sales-quotation-document.component';
//import { ManageItemsOfSalesQuotationDocumentComponent } from './components/manage-items-of-sales-quotation-document/manage-items-of-sales-quotation-document.component';
//import { ManageFootersOfSalesQuotationDocumentComponent } from './components/manage-footers-of-sales-quotation-document/manage-footers-of-sales-quotation-document.component';
//import { ManageHeadersOfSalesQuotationDocumentComponent } from './components/manage-headers-of-sales-quotation-document/manage-headers-of-sales-quotation-document.component';
import { CreateSalesComponent } from './components/sales-and-quotation/create-salesquotation/create-sales/create-sales.component';
import { SearchSalesComponent } from './components/sales-and-quotation/search-sales/search-sales.component';
import { CreateSqdocComponent } from './components/sales-and-quotation/create-salesquotation/create-sqdoc/create-sqdoc.component';
import { UpdateSalesQuotationDocumentComponent } from './components/sales-and-quotation/create-salesquotation/update-sales-quotation-document/update-sales-quotation-document.component';




//import { ReportTabInvoiceAndPurchaseReportsInvoiceDetailReportComponent } from './components/report-tab-invoice-and-purchase-reports-invoice-detail-report/report-tab-invoice-and-purchase-reports-invoice-detail-report.component';
//import { ReportTabQuotationReportCustomerSalesQuotationDetailReportComponent } from './components/report-tab-quotation-report-customer-sales-quotation-detail-report/report-tab-quotation-report-customer-sales-quotation-detail-report.component';
import { QuotSmryRepComponent } from './components/reports/list-of-report/quotation-report/quot-smry-rep/quot-smry-rep.component';
import { QuotDetRepComponent } from './components/reports/list-of-report/quotation-report/quot-det-rep/quot-det-rep.component';
import { DetRepComponent } from './components/reports/list-of-report/invoice-and-purchase-report/det-rep/det-rep.component';
import { SmryRepComponent } from './components/reports/list-of-report/invoice-and-purchase-report/smry-rep/smry-rep.component';
import { TaxesRepComponent } from './components/reports/list-of-report/invoice-and-purchase-report/taxes-rep/taxes-rep.component';


import { CreatePtmRecptComponent } from './components/payment/create-ptm-recpt/create-ptm-recpt.component';
import { SearchPtmRecptComponent } from './components/payment/search-ptm-recpt/search-ptm-recpt.component';
//import { PaymentTabSearchAndModifyPaymentReceiptComponent } from './components/payment-tab-search-and-modify-payment-receipt/payment-tab-search-and-modify-payment-receipt.component';



//import { CertificateTabUomCalculatorCreateUomCalculatorComponent } from './components/certificate-tab-uom-calculator-create-uom-calculator/certificate-tab-uom-calculator-create-uom-calculator.component';
//import { CertificateTabUucNameMastersSearchAndModifyEnvironmentalMasterComponent } from './components/certificate-tab-uuc-name-masters-search-and-modify-environmental-master/certificate-tab-uuc-name-masters-search-and-modify-environmental-master.component';
//import { CertificateTabUucNameMastersSearchAndModifyUucNameComponent } from './components/certificate-tab-uuc-name-masters-search-and-modify-uuc-name/certificate-tab-uuc-name-masters-search-and-modify-uuc-name.component';
//import { CertifiacteTabUncNameMastersCreateUncMasterDetailsComponent } from './components/certifiacte-tab-unc-name-masters-create-unc-master-details/certifiacte-tab-unc-name-masters-create-unc-master-details.component';
//import { CertificateTabThermocoupleDetailsSearchAndModifyThermocoupleChartComponent } from './components/certificate-tab-thermocouple-details-search-and-modify-thermocouple-chart/certificate-tab-thermocouple-details-search-and-modify-thermocouple-chart.component';
//import { CertificateTabUomCalculatorSearchAndModifyUomCalculatorComponent } from './components/certificate-tab-uom-calculator-search-and-modify-uom-calculator/certificate-tab-uom-calculator-search-and-modify-uom-calculator.component';
//import { CertificateTabUncNameMastersSearchAndModifyUncMastersDetailsComponent } from './components/certificate-tab-unc-name-masters-search-and-modify-unc-masters-details/certificate-tab-unc-name-masters-search-and-modify-unc-masters-details.component';
//import { CertificateTabMasterInstrumentDetailSearchAndModifyMasterInstrumentDetailComponent } from './components/certificate-tab-master-instrument-detail-search-and-modify-master-instrument-detail/certificate-tab-master-instrument-detail-search-and-modify-master-instrument-detail.component';
//import { CrtificateTabNablScopeMastersSearchAndModifyNablScopeMastersComponent } from './components/crtificate-tab-nabl-scope-masters-search-and-modify-nabl-scope-masters/crtificate-tab-nabl-scope-masters-search-and-modify-nabl-scope-masters.component';
import { CreateInstrumentComponent } from './components/certificate/master-instrument-detail/create-instrument/create-instrument.component';
import { SearchInstrumentComponent } from './components/certificate/master-instrument-detail/search-instrument/search-instrument.component';
import { CreateUucComponent } from './components/certificate/uuc-name-masters/create-uuc/create-uuc.component';
import { SearchUucComponent } from './components/certificate/uuc-name-masters/search-uuc/search-uuc.component';
import { CreateEnvMasterComponent } from './components/certificate/uuc-name-masters/create-env-master/create-env-master.component';
import { SearchEnvMasterComponent } from './components/certificate/uuc-name-masters/search-env-master/search-env-master.component';
import { CreateUomComponent } from './components/certificate/uom-calculator/create-uom/create-uom.component';
import { SearchUomComponent } from './components/certificate/uom-calculator/search-uom/search-uom.component';
import { CreateNablComponent } from './components/certificate/nabl-scope-masters/create-nabl/create-nabl.component';
import { SearchNablComponent } from './components/certificate/nabl-scope-masters/search-nabl/search-nabl.component';
import { CreateUncComponent } from './components/certificate/unc-name-masters/create-unc/create-unc.component';
import { SearchUncComponent } from './components/certificate/unc-name-masters/search-unc/search-unc.component';
import { SearchThermocoupleComponent } from './components/certificate/search-thermocouple/search-thermocouple.component';
import { TrialPopupComponent } from './components/trial-popup/trial-popup.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { SuppUpdateComponent } from './components/supplier/supplier-master/supp-update/supp-update.component';
import { UpdateInvdocComponent } from './components/invoice/create-delivery-challan/update-invdoc/update-invdoc.component';
import { CreatePoDocumentComponent } from './components/customer/customer-purchase-order/create-po-document/create-po-document.component';
import { UpdatePoDocumentComponent } from './components/customer/customer-purchase-order/update-po-document/update-po-document.component';
import { CreateNablSpecificationComponent } from './components/certificate/nabl-scope-masters/create-nabl-specification/create-nabl-specification.component';
import { UpdateNablComponent } from './components/certificate/nabl-scope-masters/update-nabl/update-nabl.component';
import { UpdateNablSpecificationComponent} from './components/certificate/nabl-scope-masters/update-nabl-specification/update-nabl-specification.component';


//req and inw & certificate
import { CreateReqdocComponent } from './components/request-and-inward/request-inwards/create-reqdoc/create-reqdoc.component';
import { ReqdocParaDetailsComponent } from './components/request-and-inward/request-inwards/reqdoc-para-details/reqdoc-para-details.component';
import { ReqdocMultiparaDetailsComponent } from './components/request-and-inward/request-inwards/reqdoc-multipara-details/reqdoc-multipara-details.component';
import { UpdateReqdocComponent } from './components/request-and-inward/request-inwards/update-reqdoc/update-reqdoc.component';
import { UpdateReqdocParaDetailsComponent } from './components/request-and-inward/request-inwards/update-reqdoc-para-details/update-reqdoc-para-details.component';
import { UpdateReqdocMultiparaDetailsComponent } from './components/request-and-inward/request-inwards/update-reqdoc-multipara-details/update-reqdoc-multipara-details.component';
import { CreateCalibrationCertificateComponent } from './components/request-and-inward/request-inwards/create-calibration-certificate/create-calibration-certificate.component';
import { UpdateCalibrationCertificateComponent } from './components/request-and-inward/request-inwards/update-calibration-certificate/update-calibration-certificate.component';
import { CreateSpecificationDetailsComponent } from './components/certificate/master-instrument-detail/create-specification-details/create-specification-details.component';
import { CreateCalibrationCertificateDetailsComponent } from './components/certificate/master-instrument-detail/create-calibration-certificate-details/create-calibration-certificate-details.component';
import { SearchCertificateService } from './services/serviceconnection/search-service/search-certificate.service';
import { UpdateMasterInstrumentComponent } from './components/certificate/master-instrument-detail/update-master-instrument/update-master-instrument.component';
import { UpdateSpecificationDetailsComponent } from './components/certificate/master-instrument-detail/update-specification-details/update-specification-details.component';
import { UpdateCalibrationCertificateDetailsComponent } from './components/certificate/master-instrument-detail/update-calibration-certificate-details/update-calibration-certificate-details.component';
//import { CreateCalibrationResultDataForMasterInstrumentComponent } from './components/certificate/master-instrument-detail/create-calibration-result-data-for-master-instrument/create-calibration-result-data-for-master-instrument.component';
import { CalibrationTagForRequestAndInwardDocumentComponent } from './components/calibration/calibration-tag-for-request-and-inward-document/calibration-tag-for-request-and-inward-document.component';
import { SearchReqForSettingCalibrationTagComponent } from './components/calibration/search-req-for-setting-calibration-tag/search-req-for-setting-calibration-tag.component';
import { SearchPipe } from './pipes/search.pipe';
import { SearchReqAndInwardService } from './services/serviceconnection/search-service/search-req-and-inward.service';
import { CreateCertificateService } from './services/serviceconnection/create-service/create-certificate.service';
import { CreateProformaComponent } from './components/invoice/create-delivery-challan/create-proforma/create-proforma.component';
import { CreateProformadocComponent } from './components/invoice/create-delivery-challan/create-proformadoc/create-proformadoc.component';
import { UpdateProformaInvdocComponent } from './components/invoice/create-delivery-challan/update-proforma-invdoc/update-proforma-invdoc.component';
import { SearchProIncComponent } from './components/invoice/search-pro-inc/search-pro-inc.component';
import { CreateDelivChallanComponent } from './components/invoice/create-delivery-challan/create-deliv-challan/create-deliv-challan.component';
import { CreateDelivChallandocComponent } from './components/invoice/create-delivery-challan/create-deliv-challandoc/create-deliv-challandoc.component';
//import { SearchDelivChallanComponent } from './components/invoice/create-delivery-challan/search-deliv-challan/search-deliv-challan.component';
import { UpdateDelivChallanComponent } from './components/invoice/create-delivery-challan/update-deliv-challan/update-deliv-challan.component';
import { SearchInvoiceComponent } from './components/invoice/create-delivery-challan/search-invoice/search-invoice.component';
import { CreateInvoiceComponent } from './components/invoice/create-delivery-challan/create-invoice/create-invoice.component';

import { CreateUucParameterMasterSpecificationDetailsComponent } from './components/certificate/uuc-name-masters/create-uuc-parameter-master-specification-details/create-uuc-parameter-master-specification-details.component';
import { TrialExamppleComponent } from './components/trial-exampple/trial-exampple.component';
import { UpdateEnvMasterComponent } from './components/certificate/uuc-name-masters/update-env-master/update-env-master.component';
import { CreateUncSpecificationComponent } from './components/certificate/unc-name-masters/create-unc-specification/create-unc-specification.component';
import { UpdateUncComponent } from './components/certificate/unc-name-masters/update-unc/update-unc.component';
import { CreateUomCalculatorDetailsComponent } from './components/certificate/uom-calculator/create-uom-calculator-details/create-uom-calculator-details.component';
import { UpdateUomComponent } from './components/certificate/uom-calculator/update-uom/update-uom.component';





// Parent Routes
const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		children: homeChildRoutes,
		canActivate: [AuthService]        //through this  
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'crtPo',
		component: CreatePoComponent
	},
	{
		path: 'updatePoDoc',
		component: UpdatePoDocumentComponent
	},
	{
		path: 'forgotpassword',
		component: ForgotPasswordComponent
	},
	{
		path: 'registration',
		component: RegistrationComponent 
	},
	{
		path: 'RegisterDetail',
		component: RegisterDetailsComponent               //
	},
	{
		path: 'validate',
		component: ValidationComponent
	},
	{
		path: 'createsqdoc',
		component: CreateSqdocComponent
	},
	
	{
		path: 'crtinvdoc',
		component: CreateInvdocComponent 
	},
	{
		path: 'crtproformadoc',
		component: CreateProformadocComponent 
	},
	
	{
		path: 'crtdelichallandoc',
		component: CreateDelivChallandocComponent 
	},
	{
		path: 'updateinvdoc',
		component: UpdateInvdocComponent 
	},
	{
		path: 'updateproinvdoc',
		component: UpdateProformaInvdocComponent
	},
	{
		path: 'updatedelichallandoc',
		component: UpdateDelivChallanComponent 
	},



	{ path: 'updatesalesdoc', 
	  component: UpdateSalesQuotationDocumentComponent 
	},
	{ path: 'createpodoc', 
	  component: CreatePoDocumentComponent 
	},
	{ path: 'updatepodoc', 
	  component: UpdatePoDocumentComponent 
	},
	{ path: 'createnablespecdet', 
	  component: CreateNablSpecificationComponent 
	},
	{ path: 'updatenablespecdet', 
	  component:  UpdateNablSpecificationComponent 
	},
	{ 
		path: 'createReqDoc', 
		component: CreateReqdocComponent,
	//	children: [{path: 'reqDocParaList', component: ReqdocParaDetailsComponent  }]
	},
	{ 
		path: 'reqDocParaList', 
		component: ReqdocParaDetailsComponent 
	},
	{ 
		path: 'reqDMultiList', 
		component: ReqdocMultiparaDetailsComponent 
	},

	{ 
		path: 'updateReqDoc', 
		component: UpdateReqdocComponent,
	},
	{ 
		path: 'updReqDocParaList', 
		component: UpdateReqdocParaDetailsComponent 
	},
	{ 
		path: 'updReqDMultiList', 
		component: UpdateReqdocMultiparaDetailsComponent 
	},
	{ 
		path: 'crtCalCer', 
		component: CreateCalibrationCertificateComponent 
	},
	{ 
		path: 'updCalCer', 
		component: UpdateCalibrationCertificateComponent 
	},
	{ 
		path: 'crtSpeDet', 
		component: CreateSpecificationDetailsComponent 
	},
	{ 
		path: 'crtMCalCerDet', 
		component: CreateCalibrationCertificateDetailsComponent 
	},
	// { 
	// 	path: 'crtMCalResuData', 
	// 	component: CreateCalibrationResultDataForMasterInstrumentComponent 
	// },
	{ 
		path: 'updSpeDet', 
		component: UpdateSpecificationDetailsComponent 
	},
	{ 
		path: 'updMCalCerDet', 
		component: UpdateCalibrationCertificateDetailsComponent 
	},
	// { 
	// 	path: 'employee', 
	// 	component: EmployeComponent 
	// },
	{   path: 'caliTagSet', 
	    component: CalibrationTagForRequestAndInwardDocumentComponent
	},

	{   path: 'crtUucSpeDet', 
		component: CreateUucParameterMasterSpecificationDetailsComponent
	},
	{   path: 'crtUncSpeDet', 
		component: CreateUncSpecificationComponent
	},
	{   path: 'crtUomCalDet', 
	    component: CreateUomCalculatorDetailsComponent  
    },



	{
		path: 'nav', component: NavigationBarComponent,
		children: [{ path: 'GetinTouch', component: GetinTouchComponent },{ path: 'home', component: HomePageComponent },
		{ path: 'csvtohtml', component: AddressComponent }, { path: 'emailus', component: EmailUsComponent },
		{ path: 'callus', component: CallUsComponent },{ path: 'popupwindow', component: TrialPopupComponent },

		//{ path: 'manageapprovers', component: ManageApproversComponent }, { path: 'SearchandModifyUser', component: UserTabSearchAndModifyUserComponent }, { path: 'CreateorUpdateUser', component: UserTabCreateUpdateUserComponent },
		{ path: 'usrcreate', component: UsrCreateComponent },{ path: 'usrupdate', component: UsrUpdateComponent }, { path: 'usrsearch', component: UsrSearchComponent }, { path: 'usrmanage', component: UsrManageComponent },

		{ path: 'custcreate', component: CustCreateComponent }, { path: 'custsearch', component: CustSearchComponent }, { path: 'custupdate', component: CustUpdateComponent } , { path: 'searchptm', component: SearchPtmReceiptComponent }, { path: 'createpo', component: CreatePoComponent }, { path: 'searchpo', component: SearchPoComponent }, { path: 'ledger', component: LedgerComponent }, { path: 'smryrep', component: SearchSmryRepComponent },
		//{ path: 'CustomerMasterSearchModifyCustomer', component: CustomerTabCustomerMasterSearchAndModifyCustomerComponent }, { path: 'CreateCustomerPurchaseOrder', component: CustomerTabCreateCustomerPurchaseOrderComponent }, { path: 'CustomerTabCustomerLedger', component: CustomerTabCustomerLedgerComponent }, { path: 'CustomerMasterSearchModifyPaymentReceipt', component: CustomerTabCustomerMasterSearchAndModifyPaymentReceiptComponent }, { path: 'CustomerMasterAddCustomer', component: CustomerTabCustomerMasterAddCustomerComponent }, { path: 'SearchAndModifyCustomerPurchaseOrder', component: CustomerTabSearchAndModifyCustomerPurchaseOrderComponent }, { path: 'SearchandModifyCustomerSummaryReport', component: CustomerTabCalibrationSearchAndModifyCustomerSummaryReportComponent },

		{ path: 'defcreate', component: DefCreateComponent }, { path: 'defsearch', component: DefSearchComponent },//{ path: 'defupdate', component: DefUpdateComponent },
		//{ path: 'SearchandModifyDefintion', component: DefintionTabSearchAndModifyDefintionComponent },
		
		  
		{ path: 'replist', component: RepairListComponent  },{ path: 'repsearch', component: RepairSearchComponent  },{ path: 'servlist', component: ServiceListComponent },{ path: 'servsearch', component: ServiceSearchComponent },{ path: 'calilist', component:  CaliListComponent },{ path: 'calisearch', component: CaliSearchComponent },{ path: 'certilist', component: CertiListComponent },{ path: 'certisearch', component:  CertiSearchComponent },{ path: 'delilist', component: DeliListComponent },{ path: 'delisearch', component: DeliSearchComponent},{ path: 'createreq', component: ReqCreateComponent },{ path: 'searchreq', component: ReqSearchComponent }, { path: 'reqcolle', component: ReqColleComponent},{ path: 'myreqcol', component: MyReqColleComponent},{ path: 'incolinstru', component: InwColleInstruComponent },{ path: 'reqos', component:  ReqOsCaliComponent },{ path: 'myreqos', component:MyReqOsCaliComponent },{ path: 'inoscali', component:  InwOsCaliComponent },
		//{ path: 'MyRequestForCollection', component: RequestAndInwardTabMyRequestForCollectionComponent }, { path: 'SearchPendingRequestAndInwardsForRepair', component: RequestAndInwardTabSearchPendingRequestAndInwardsForRepairComponent }, { path: 'ToDoListForRepair', component: RequestAndInwardTabToDoListForRepairComponent },{ path: 'SearchPendingRequestAndInwardsForCertification', component: RequestAndInwardTabSearchPendingRequestAndInwardForCertificationComponent }, { path: 'SearchPendingRequestAndInwardsForCalibration', component: RequestAndInwardTabSearchPendingRequestAndInwardForCalibrationComponent }, { path: 'ToDoListForCalibration', component: RequestAndInwardTabToDoListForCalibrationComponent }, { path: 'SearchAndModifyRequestAndInward', component: RequestAndInwardTabSearchAndModifyRequestAndInwardComponent }, { path: 'MyRequestForOnsiteCalibration', component: RequestAndInwardTabMyRequestForOnsiteCalibrationComponent }, { path: 'RequestForOnsiteCalibration', component: RequestAndInwardTabRequestForOnsiteCalibrationComponent },
    
		//{ path: 'CreateDeliveryChallanPurchaseOrder', component: SupplierTabCreateDeliveryChallanPurchaseOrderComponent }, { path: 'SearchandModifyDeliveryChallanPurchaseOrder', component: SupplierTabSearchAndModifyDeliveryChallanPurchaseOrderComponent }, { path: 'SearchandModifySupplier', component: SupplierTabSearchAndModifySupplierComponent },
		{ path: 'createsupp', component: SuppCreateComponent }, { path: 'searchsupp', component: SuppSearchComponent }, { path: 'updatesupp', component: SuppUpdateComponent }, { path: 'createdcpo', component: CreateDcPoComponent }, { path: 'searchdcpo', component: SearchDcPoComponent },

		{ path: 'createptm', component: CreatePtmRecptComponent }, { path: 'searchptmrecept', component: SearchPtmRecptComponent },
		//{ path: 'SearchandMofdifyPaymentReceipt', component: PaymentTabSearchAndModifyPaymentReceiptComponent },

		//{ path: 'CreateSalesQuotation', component: SalesAndQuotationTabCreateSalesAndQuotationComponent }, { path: 'SearchandModifySalesQuotation', component: SalesQuotationTabSearchAndModifySalesQuatationComponent },
		{ path: 'createsales', component: CreateSalesComponent }, { path: 'searchsales', component: SearchSalesComponent },

		{ path: 'createInvoice', component: CreateInvoiceComponent },{ path: 'searchproinc', component: SearchProIncComponent },{ path: 'crtdelichallan', component: CreateDelivChallanComponent },{ path: 'crtproforma', component: CreateProformaComponent },{ path: 'srhcustinv', component: SearchCustInvComponent }, { path: 'searchinvoice', component: SearchInvoiceComponent }, { path: 'searchDeliChn', component: SearchDeliChnComponent } 
		// ,{ path: 'SearchandModifyDeliveryChallanCumInvoice', component: InvoiceTabSearchAndModifyDeliveryChallanCumInvoiceComponent }, { path: 'SearchandModifyProformaInvoice', component: InvoiceTabSearchAndModifyProformaInvoiceComponent }, 

		//{ path: 'InvoiceDetailReport', component: ReportTabInvoiceAndPurchaseReportsInvoiceDetailReportComponent }, { path: 'CustomerSalesQuotationDetailReport', component: ReportTabQuotationReportCustomerSalesQuotationDetailReportComponent },
		,{ path: 'quotsmr', component: QuotSmryRepComponent }, { path: 'quotder', component: QuotDetRepComponent }, { path: 'detrep', component: DetRepComponent }, { path: 'smryreport', component: SmryRepComponent }, { path: 'taxrep', component: TaxesRepComponent },

		//{ path: 'CreateUOMCalculator', component: CertificateTabUomCalculatorCreateUomCalculatorComponent }, { path: 'SearchandModifyNABLScopeMaster', component: CrtificateTabNablScopeMastersSearchAndModifyNablScopeMastersComponent }, { path: 'SearchandModifyUNCMastersDetails', component: CertificateTabUncNameMastersSearchAndModifyUncMastersDetailsComponent }, { path: 'SearchandModifyMasterInstrumentDetail', component: CertificateTabMasterInstrumentDetailSearchAndModifyMasterInstrumentDetailComponent }, { path: 'SearchandModifyUOMCalculator', component: CertificateTabUomCalculatorSearchAndModifyUomCalculatorComponent }, { path: 'SearchandModifyThermocoupleChart', component: CertificateTabThermocoupleDetailsSearchAndModifyThermocoupleChartComponent }, { path: 'CreateUNCMastersDetails', component: CertifiacteTabUncNameMastersCreateUncMasterDetailsComponent }, { path: 'SearchandModifyEnvironmentalMaster', component: CertificateTabUucNameMastersSearchAndModifyEnvironmentalMasterComponent }, { path: 'CertificateTabUucNameMastersSearchAndModifyUucName', component: CertificateTabUucNameMastersSearchAndModifyUucNameComponent },
{ path: 'createinstru', component: CreateInstrumentComponent },{ path: 'updateinstru', component: UpdateMasterInstrumentComponent },{ path: 'searchinstru', component: SearchInstrumentComponent },{ path: 'createuuc', component: CreateUucComponent},{ path: 'searchuuc', component: SearchUucComponent},{ path: 'createenv', component: CreateEnvMasterComponent},{ path: 'searchenv', component: SearchEnvMasterComponent}, { path: 'updateenv', component: UpdateEnvMasterComponent}, { path: 'createuom', component: CreateUomComponent },{ path: 'searchuom', component:SearchUomComponent }, { path: 'updateuom', component: UpdateUomComponent },{ path: 'createnabl', component: CreateNablComponent },{ path: 'searchnabl', component: SearchNablComponent },{ path: 'createunc', component: CreateUncComponent },{ path: 'searchunc', component: SearchUncComponent },{ path: 'updateunc', component: UpdateUncComponent },{ path: 'searchthermo', component: SearchThermocoupleComponent },
	

		{ path: 'updatenabl', component: UpdateNablComponent },

		{ path: 'sreqForCaliTag', component: SearchReqForSettingCalibrationTagComponent},

































	]
	}, //  
	{
		path: '**',
		redirectTo: ''
	}
];

@NgModule({
	declarations: [
		AppComponent,
		StudentListComponent,
		StudentDetailsComponent,
		StudentAddComponent,
		LoginComponent,
		HomeComponent,
		RegistrationComponent,
		FilterPipe,
		PhonePipe,
		HighlightStudentDirective,
		RegisterDetailsComponent,
		NavigationBarComponent,
		GetinTouchComponent,
		AddressComponent,
		EmailUsComponent,
		CallUsComponent,
		ValidationComponent,
		//UserTabCreateUpdateUserComponent,
		//UserTabSearchAndModifyUserComponent,
	
		//ManageApproversComponent,
		//CustomerTabCustomerMasterSearchAndModifyCustomerComponent,
		//CustomerTabCustomerMasterSearchAndModifyPaymentReceiptComponent,
		//CustomerTabCustomerMasterAddCustomerComponent,
		//CustomerTabCreateCustomerPurchaseOrderComponent,
		//CustomerTabSearchAndModifyCustomerPurchaseOrderComponent,
		//CustomerTabCalibrationSearchAndModifyCustomerSummaryReportComponent,
		//CustomerTabCustomerLedgerComponent,
		//DefintionTabSearchAndModifyDefintionComponent,
		//RequestAndInwardTabMyRequestForCollectionComponent,
		//RequestAndInwardTabRequestForOnsiteCalibrationComponent,
		//RequestAndInwardTabMyRequestForOnsiteCalibrationComponent,
		//RequestAndInwardTabSearchAndModifyRequestAndInwardComponent,
		//RequestAndInwardTabToDoListForCalibrationComponent,
		//RequestAndInwardTabSearchPendingRequestAndInwardForCalibrationComponent,
		//RequestAndInwardTabSearchPendingRequestAndInwardForCertificationComponent,
		//RequestAndInwardTabToDoListForRepairComponent,
		//RequestAndInwardTabSearchPendingRequestAndInwardsForRepairComponent,
		EmployeeDetailsComponent,
		UpdateEmployeeComponent,
		EmployeeListComponent,
		CreateEmployeeComponent,
		//SupplierTabSearchAndModifySupplierComponent,
		//SupplierTabSearchAndModifyDeliveryChallanPurchaseOrderComponent,
		//SupplierTabCreateDeliveryChallanPurchaseOrderComponent,
		//PaymentTabSearchAndModifyPaymentReceiptComponent,
		//SalesAndQuotationTabCreateSalesAndQuotationComponent,
		//SalesQuotationTabSearchAndModifySalesQuatationComponent,
		//ReportTabInvoiceAndPurchaseReportsInvoiceDetailReportComponent,
		//ReportTabQuotationReportCustomerSalesQuotationDetailReportComponent,
		//InvoiceTabSearchAndModifyDeliveryChallanCumInvoiceComponent,
		//InvoiceTabSearchAndModifyProformaInvoiceComponent,

		//CertificateTabUomCalculatorCreateUomCalculatorComponent,
		//CertificateTabUucNameMastersSearchAndModifyEnvironmentalMasterComponent,
		//CertificateTabUucNameMastersSearchAndModifyUucNameComponent,
		//CertifiacteTabUncNameMastersCreateUncMasterDetailsComponent,
		//CertificateTabThermocoupleDetailsSearchAndModifyThermocoupleChartComponent,
		//CertificateTabUomCalculatorSearchAndModifyUomCalculatorComponent,
		//CertificateTabUncNameMastersSearchAndModifyUncMastersDetailsComponent,
		//CertificateTabMasterInstrumentDetailSearchAndModifyMasterInstrumentDetailComponent,
		//CrtificateTabNablScopeMastersSearchAndModifyNablScopeMastersComponent,
		//SalesQuotationDocumentForCreateSalesQuotationComponent,
		//ManageDocumentOfSalesQuotationDocumentComponent,
		//ManageItemsOfSalesQuotationDocumentComponent,
		//ManageFootersOfSalesQuotationDocumentComponent,
		//ManageHeadersOfSalesQuotationDocumentComponent,
		UsrCreateComponent,
		UsrSearchComponent,
		UsrUpdateComponent,
		UsrManageComponent,
		CustCreateComponent,
		CustSearchComponent,
		SearchPtmReceiptComponent,
		CreatePoComponent,
		SearchPoComponent,
		LedgerComponent,
		SearchSmryRepComponent,
		PrtSmryRepComponent,
		PrtDueLtrComponent,
		DefCreateComponent,
		DefSearchComponent,
		SearchDeliChnComponent,
	//	SearchProIncComponent,
		CreatePtmRecptComponent,
		SearchPtmRecptComponent,
		SuppCreateComponent,
		SuppSearchComponent,
		CreateDcPoComponent,
		SearchDcPoComponent,
		QuotSmryRepComponent,
		QuotDetRepComponent,
		DetRepComponent,
		SmryRepComponent,
		TaxesRepComponent,
		CreateSalesComponent,
		SearchSalesComponent,
		CreateSqdocComponent,
		ReqCreateComponent,
		ReqSearchComponent,
		ReqColleComponent,
		MyReqColleComponent,
		InwColleInstruComponent,
		ReqOsCaliComponent,
		MyReqOsCaliComponent,
		InwOsCaliComponent,
		ServiceListComponent,
		ServiceSearchComponent,
		CaliListComponent,
		CaliSearchComponent,
		CertiListComponent,
		CertiSearchComponent,
		DeliListComponent,
		DeliSearchComponent,
		RepairListComponent,
		RepairSearchComponent,
		CreateInstrumentComponent,
		SearchInstrumentComponent,
		CreateUucComponent,
		SearchUucComponent,
		CreateEnvMasterComponent,
		SearchEnvMasterComponent,
		CreateUomComponent,
		SearchUomComponent,
		CreateNablComponent,
		SearchNablComponent,
		CreateUncComponent,
		SearchUncComponent,
		SearchThermocoupleComponent,
		TrialPopupComponent,
		CreateInvdocComponent,
		SearchCustInvComponent,
		ForgotPasswordComponent,
		ResetPasswordComponent,
		UsrUpdateComponent,
		CustUpdateComponent,
		HomePageComponent,
		SuppUpdateComponent,
		UpdateSalesQuotationDocumentComponent,
		UpdateInvdocComponent,
		CreatePoDocumentComponent,
		UpdatePoDocumentComponent,
		CreateNablSpecificationComponent,
		UpdateNablComponent,
		UpdateNablSpecificationComponent,
		// CreateProformaComponent,
		// CreateProformadocComponent,
		 CreateInvoiceComponent,
		// UpdateProformaInvdocComponent,
		CreateReqdocComponent,
		ReqdocParaDetailsComponent,
		ReqdocMultiparaDetailsComponent,
		SearchPipe,
		UpdateReqdocComponent,
		UpdateReqdocParaDetailsComponent,
		UpdateReqdocMultiparaDetailsComponent,
		CreateCalibrationCertificateComponent,
		UpdateCalibrationCertificateComponent,
		CreateSpecificationDetailsComponent,
		CreateCalibrationCertificateDetailsComponent,
		UpdateMasterInstrumentComponent,
		UpdateSpecificationDetailsComponent,
		UpdateCalibrationCertificateDetailsComponent,
		//CreateCalibrationResultDataForMasterInstrumentComponent,
		CalibrationTagForRequestAndInwardDocumentComponent,
		SearchReqForSettingCalibrationTagComponent,
		CreateProformaComponent,
		CreateProformadocComponent,
		UpdateProformaInvdocComponent,
		SearchProIncComponent,
		CreateDelivChallanComponent,
		CreateDelivChallandocComponent,
	//	SearchDelivChallanComponent,
		UpdateDelivChallanComponent,
		SearchInvoiceComponent,
		CreateUucParameterMasterSpecificationDetailsComponent,
		
		TrialExamppleComponent,
		
		UpdateEnvMasterComponent,
		
		CreateUncSpecificationComponent,
		
		UpdateUncComponent,
		
		CreateUomCalculatorDetailsComponent,
		
		UpdateUomComponent,
		
		//DefUpdateComponent,
		//DefUpdateComponent,

	],
	imports: [
		BrowserModule,
		RouterModule,
		RouterModule.forRoot(routes),
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		DatePickerModule,
		NgxPaginationModule,
		NgQRCodeReaderModule,
		NgMultiSelectDropDownModule.forRoot(),
		//	BrowserAnimationsModule,     // I have had previous Components append to the DOM when trying to load a different route so I 
		//                                put comment on BrowserAnimationsModule and wrote { useHash: false } in RouterModule.forRoot()
		//                                and This fixed all my problems with unwanted appended components.
		ToastrModule.forRoot({
			timeOut: 3000,
			positionClass: 'toast-bottom-right',                                          
			preventDuplicates: true,
		}),
	],
	exports:   [RouterModule],
	providers: [DatePipe, AuthService, UserService, StudentService, RegistrationComponent, EmployeeService, CreateUserService , SearchUserService, MngApproversService, CreateCustomerService, SearchCustomerService , CreateCustPoService, FileUploadService, CreateSalesService, SearchSalesService,CreateInvoiceService,SearchInvoiceService,CreateSupplierService,SearchSupplierService,CreateDefService,CreateReqAndInwardService,SearchPoService,CreateNablScopeService,CreateCertificateService,SearchNablService,CreateProformaService,SearchProformaInvoiceService,SearchReqAndInwardService,SearchCertificateService,CreateDeliChallanService],
	bootstrap: [AppComponent]
})

// enableProdMode();

export class AppModule { }

export class DynamicGrid {
	name: string;
	email: string;
	phone: string;
}
