/**
 * Created By : PoojaG MechLean 
 */

import { Directive, ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlightStudent]'
})
export class HighlightStudentDirective {

	constructor(private el: ElementRef) {
	}

	@HostListener('mouseenter') onMouseEnter() {
		this.highlight('green');
	}

	@HostListener('mouseleave') onMouseLeave() {
		this.highlight('pink');
	}

	private highlight(color: string) {
		this.el.nativeElement.style.backgroundColor = color;
	}
}

/**
 * Created By : PoojaG MechLean 
 */