import { ANALYZE_FOR_ENTRY_COMPONENTS, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../services/config/config.service';
import { CreateUserService, User } from '../../services/serviceconnection/create-service/create-user.service';

declare var $: any;
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  user: User = new User();
  userForUserName: User = new User();
  usersArray: Array<any> = [];
  usersIds: Array<any> = [];
  userIdLength: number;

  usersUserName: Array<any> = [];
  userNamesLength: number;
  userName: string;
  forgotPasswordForm: FormGroup;
  i: number = 0;
  userStatus: any;

  ;
  constructor(private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService,) {

    this.forgotPasswordForm = this.formBuilder.group({


      userName: ['', Validators.required],

    });


  }

  ngOnInit() {
  }

  backToLogin() {
    this.router.navigate(['/login']);
  }

  userNameValid() {
    this.createUserService.getUserList().subscribe(data => {
      console.log(data);
      this.userForUserName = data;
      for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {


        this.usersUserName.push(this.userForUserName[i].userName);

      }
      this.userNamesLength = Object.keys(this.usersUserName).length;

      console.log("usersUserName" + this.usersUserName)


      for (this.i = 0; this.i < this.userNamesLength; this.i++) {
        if (this.usersUserName[this.i] == this.userName.toLowerCase()) {


          if (this.userForUserName[this.i].status == 1) {


            this.userStatus = 1;
            this.user = this.userForUserName[this.i];
            $("#userNameValidation").html(" ");
          }

          break;
        }
        else {
          $("#userNameValidation").html(" ");
          this.userStatus = 0;
        }

      }

      if (this.i >= this.userNamesLength) {

        $("#userNameValidation").html("User is Inactive or does not exist!! Please enter valid User Name to get Password..");


      }


    });
  }

  onSubmit() {

    console.log(this.forgotPasswordForm.value);
    console.log(this.user);
    alert("password is sent to the registerd email!!!");

    this.userStatus = 0;
    this.save();
  }

  save() {
    this.createUserService
      .forgotPassword(this.user).subscribe(data => {
        console.log(data)
        this.user = new User();
      },
        error => console.log(error));


  }

}


