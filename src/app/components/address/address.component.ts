import { Component, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  constructor() { }
 
  ngOnInit() {
  
        $(function () {
            var csv = $("#fileUploadCSV").val();
            $("#btnUpload").bind("click", function () {
                debugger;
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;//regex for Checking valid files csv of txt 

                if (regex.test($("#fileUploadCSV").val().toLowerCase())) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                          var rows = ((<FileReader>e.target).result as string).split("\r\n");

                            if (rows.length > 0) {
                                var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings

                                if (first_Row_Cells.length != 3 ) {
                                    alert('Please upload a valid csv ,Colums count not matching ');
                                    return;
                                }
                                if (first_Row_Cells[0] != "Name") {
                                    alert('Please upload a valid csv, Check Heading Name');
                                    return;
                                }
                                if (first_Row_Cells[1] != "Email") {
                                    alert('Please upload a valid csv, Check Heading Email');
                                    return;

                                }
                                if (first_Row_Cells[2] != "Phone_Number") {
                                    
                                    alert('Please upload a valid csv, Check heading Phone Number');
                                    return;
                                }

                                var jsonArray = new Array();
                                for (var i = 1; i < rows.length; i++) {
                                    var cells = splitCSVtoCells(rows[i], ",");


                                    var obj = {};
                                    for (var j = 0; j < cells.length; j++) {
                                        obj[first_Row_Cells[j]] = cells[j];
                                    }
                                    jsonArray.push(obj);
                                }
                              
                                console.log(jsonArray);
                                var html = "";
                                for (i = 0; i < jsonArray.length; i++) {
                                    debugger;
                                    if (jsonArray[i].Name != "") {
                                        html += "<tr id=\"rowitem\"" + i + "><td style=\"display:none;\">" + i + "</td><td> <input type=\"text\" value=\"" + jsonArray[i].Name + "\">  </input> </td>";
                                        html += "<td><input type=\"email\" value= \"" + jsonArray[i].Email + "\"></input></td>";
                                        html += "<td><input type=\"text\" value= \"" + jsonArray[i].Phone_Number + "\" ></input></td>";

                                      
                                    }
                                }
                                document.getElementById('tbodyLeads').innerHTML = html;




                            }
                        }
                        reader.readAsText($("#fileUploadCSV")[0].files[0]);
                    }
                    else {
                        alert("This browser does not support HTML5.");
                    }
                } else {
                    alert("Select a valid CSV File.");
                }
            });
        });
        function splitCSVtoCells(row, separator) {
            return row.split(separator);
        }

  
}

}