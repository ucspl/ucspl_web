import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';


declare var $:any;

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})

export class NavigationBarComponent implements OnInit {
  loginUserName: any;
  active:string;
  constructor(private router: Router) { 
        // Detect route changes for active sidebar menu
    this.router.events.subscribe((val) => {
      this.routeChanged(val);
      console.log(val);
    });
  }

  ngOnInit() {

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

/*var $accordionIO = $('.accordion h5');
$accordionIO.prev('div').hide();
$accordionIO.click(function() {
  $(this).prev('div').slideToggle();
});
$(".accordion h5").click(function() {
  var jqInner = $(this).next();
  jqInner.slideToggle();
  var t = $(this).find('.plus').html();
  t == "+" ? $(this).find('.plus').html('-') : $(this).find('.plus').html('+');
})

  


var $accordionIO1 = $('.accordion h5');
$accordionIO1.prev('div').hide();
$accordionIO1.click(function() {
  $(this).prev('div').slideToggle();
});
$(".accordion h5").click(function() {
  var jqInner1 = $(this).next();
  jqInner1.slideToggle();
  var t = $(this).find('.plus1').html();
  t == "+" ? $(this).find('.plus1').html('-') : $(this).find('.plus1').html('+');
})
*/

  
var $accordionIO = $('.accordion h5');
$accordionIO.prev('div').hide();
$accordionIO.click(function() {
  $(this).prev('div').slideToggle();
});
$(".accordion h5").click(function() {

  var jqInner = $(this).next();
  jqInner.slideToggle();
  var t = $(this).find('.plus').html();
  t == "▶" ? $(this).find('.plus').html('▼') : $(this).find('.plus').html('▶');
  //var t1 = $(this).find('.plus').html();
  //t1 == "▼" ? $(this).find('.plus').html('▼') : $(this).find('.plus').html('▶');
  
})

/*$(document).on("click","div.action-label",function(){
  console.log("triggered");
}*/


var $accordionIO1 = $('.accordion h5');
$accordionIO1.prev('div').hide();
$accordionIO1.click(function() {
  $(this).prev('div').slideToggle();
});
$(".accordion h5").click(function() {
  var jqInner1 = $(this).next();
  jqInner1.slideToggle();
  var t = $(this).find('.plus1').html();
  t == "▶" ? $(this).find('.plus1').html('▼') : $(this).find('.plus1').html('▶');
 // var t1 = $(this).find('.plus1').html();
  //t1 == "▼" ? $(this).find('.plus1').html('▼') : $(this).find('.plus1').html('▶');
  
}) 

}

  address1()
  {
 //  this.router.navigateByUrl('/address')
    this.router.navigate(['/GetinTouch']);
  }

    // Detect route changes for active sidebar menu 
     routeChanged(val){
      this.active = val.url;
    }

    @HostListener('window:scroll', ['$event'])
  onScroll(e) {
   // console.log('window', e);
  }

  divScroll(e) {
  //  console.log('div App', e);
  }


  myFunction() {
    var txt;
    if (confirm("Do you want to Logout?")) {
      this.router.navigate(['/login']);
    } 
    else {
      txt = " ";
    }
    document.getElementById("demo").innerHTML = txt;
  }
  
}
