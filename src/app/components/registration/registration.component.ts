import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes ,Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../services/config/config.service';
import { StudentService } from '../../services/student/student.service';
import { Injectable } from '@angular/core'; 
import { ToastrService } from 'ngx-toastr';
//import { RegisterDetailsComponent } from '../student/register-details/register-details.component';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  public minDate: Date = new Date ("05/07/1017");
  public maxDate: Date = new Date ("05/27/3017");
  public dateValue: Date = new Date ();

  constructor( private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) { 
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required,ValidationService.nameValidator]],
      middleName:['', [Validators.required,ValidationService.nameValidator]],
      lastName: ['', [Validators.required,ValidationService.nameValidator]],
      username: ['', Validators.required],
      address:[],
      gender:[],
      birthdate:[],
      course:[],
      email: ['',  [Validators.required, ValidationService.emailValidator]],
      phoneno: ['',  [Validators.required, ValidationService.phoneValidator]],
      password: ['', [Validators.required, ValidationService.passwordValidator]]
  });
   }

  ngOnInit() {
   //  this.store();
   //  this.studentService.setRegistrationInfo(this.registerForm.value);
  }

  navigateToRegisterDetail()
  {
    this.store();
    this.studentService.setRegistrationInfo(this.registerForm.value);
    this.router.navigateByUrl('/RegisterDetail')
  }


 // get f() { return this.registerForm.controls; }


    store () {

      localStorage.setItem("firstname",this.registerForm.value.firstName );
      localStorage.setItem("middlename",this.registerForm.value.middleName );
      localStorage.setItem("lastname",this.registerForm.value.lastName );
      localStorage.setItem("birthdate",this.registerForm.value.birthdate );
      localStorage.setItem("gender",this.registerForm.value.gender );
      localStorage.setItem("course",this.registerForm.value.course );
      localStorage.setItem("phonenumber",this.registerForm.value.phoneno );
      localStorage.setItem("email", this.registerForm.value.email );
      localStorage.setItem("username", this.registerForm.value.username );
      localStorage.setItem("password", this.registerForm.value.password );
   }



}



/*

        <div class="w3-col s6">
          <label>User Name</label>
          <input type="text" name="uname" class="w3-input w3-border" formControlName="username">
          </div>


*/