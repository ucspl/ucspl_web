import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator } from '../../../../services/config/config.service';
import { SearchCertificateService } from '../../../../services/serviceconnection/search-service/search-certificate.service';

@Component({
  selector: 'app-search-unc',
  templateUrl: './search-unc.component.html',
  styleUrls: ['./search-unc.component.css']
})
export class SearchUncComponent implements OnInit {

  searchUncForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  model: any = {};
  uncData: any;

  userIdLength: number;
  userData: any;
  idForSearchUnc: Object;
  status: any;
  searchUncMasterData: any;
  searchUncMaster: any;
  usersArray: any;
  uncNo: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private searchCertificateService: SearchCertificateService) {

    this.searchUncForm = this.formBuilder.group({

      name: ['', [optionalValidator([Validators.required, Validators.minLength(3)])]],
      status: [],
      itemsPerPage: []
    })
  }

  ngOnInit() {
    this.status = "1";
    console.log("unc name to search ")
  }


  searchData() {
    debugger;

    this.searchUncMasterData = this.searchUncForm.value;
    this.searchUncMaster = this.searchUncForm.value.name;
    console.log("unc name to search : " + this.searchUncMaster)
    if (this.searchUncMaster == "") {
      this.searchUncMaster = " ";
    }

    this.searchCertificateService.searchUncMasterData(this.searchUncMaster).subscribe((res: any) => {

      this.uncData = res;
      this.usersArray = res;

    })

  }

  uncNameToUpdateUnc(i) {

    this.uncNo = i.target.innerText;
    // this.uncNo=parseInt(this.uncNo);

    for (var s = 0; s < this.uncData.length; s++) {
      if (this.uncData[s][1] == this.uncNo) {
        this.idForSearchUnc = this.uncData[s][0];
        localStorage.setItem('uncIdForUpdate', JSON.stringify(this.idForSearchUnc));
        //   localStorage.setItem('createUncNameMasterObj', JSON.stringify(this.idForSearchUnc));

        break;
      }
    }

    this.router.navigateByUrl('/nav/updateunc')


  }
}






