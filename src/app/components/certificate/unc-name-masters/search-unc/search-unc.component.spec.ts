import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchUncComponent } from './search-unc.component';

describe('SearchUncComponent', () => {
  let component: SearchUncComponent;
  let fixture: ComponentFixture<SearchUncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchUncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchUncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
