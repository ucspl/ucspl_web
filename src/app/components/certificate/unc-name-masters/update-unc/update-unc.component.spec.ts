import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUncComponent } from './update-unc.component';

describe('UpdateUncComponent', () => {
  let component: UpdateUncComponent;
  let fixture: ComponentFixture<UpdateUncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
