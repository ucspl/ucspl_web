import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUncSpecificationComponent } from './create-unc-specification.component';

describe('CreateUncSpecificationComponent', () => {
  let component: CreateUncSpecificationComponent;
  let fixture: ComponentFixture<CreateUncSpecificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUncSpecificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUncSpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
