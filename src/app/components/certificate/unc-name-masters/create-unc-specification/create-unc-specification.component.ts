import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CodegenComponentFactoryResolver } from '@angular/core/src/linker/component_factory_resolver';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../services/config/config.service';
import { CreateCertificateService, CreateMasterInstrumentSpecificationDetails, CreateUncNameMaster, CreateUncParameterMasterSpecificationDetails, CreateUucNameMaster, CreateUucParameterMasterSpecificationDetails, ModeTypeForMasterInstrument, SearchMasterInstruByMIdPNmAndStatus, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateDefService, UncDegreeOfFreedom, UncDividingFactor, UncMasterNumber, UncMasterType, UncProbabilityDistribution, UncSourceOfUncertainty, UncType } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';


@Component({
  selector: 'app-create-unc-specification',
  templateUrl: './create-unc-specification.component.html',
  styleUrls: ['./create-unc-specification.component.css']
})
export class CreateUncSpecificationComponent implements OnInit {

  public createMasterSpecificationForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 30;

  searchMasterInstruByMIdPNmAndStatusObj: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();

  createMasterInstrumentSpecificationObj: CreateMasterInstrumentSpecificationDetails = new CreateMasterInstrumentSpecificationDetails();
  dynamicArray: Array<CreateMasterInstrumentSpecificationDetails> = [];

  createUncParameterMasterSpecificationDetailsObj: CreateUncParameterMasterSpecificationDetails = new CreateUncParameterMasterSpecificationDetails();

  modeTypeForMasterInstrumentObj: ModeTypeForMasterInstrument = new ModeTypeForMasterInstrument();
  arrayModeType: Array<any> = [];
  arrayModeTypeListId: Array<any> = [];
  arrayModeTypeListName: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracy: Array<any> = [];
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];
  arrayOfParameterFrequency: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  tempChartTypeForMasterInstrumentObj: TempChartTypeForMasterInstrument = new TempChartTypeForMasterInstrument();
  arrayOfTempChartType: Array<any> = [];
  arrayTempChartTypeId: Array<any> = [];
  arrayTempChartTypeNames: Array<any> = [];

  //FrequencyUom
  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames: Array<any> = [];
  arrayOfFrequencyUomId: Array<any> = [];


  dynamicArrayT: Array<CreateUncParameterMasterSpecificationDetails> = [];
  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArray1: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  showFreq: any;
  freq: any;
  previousStatus: any;

  dropdownList = [];
  dropdownList1 = [];
  dropdownList2 = [];

  dropdownListForUom = [];
  dropdownListForUom1 = [];
  dropdownListForIpUom = [];
  dropdownListForIpUom1 = [];

  trialDropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  createUncNameMasterObj: CreateUncNameMaster = new CreateUncNameMaster();

  arrayOfSelectedUom: Array<any> = [];
  arrayOfSelectedIpUom: Array<any> = [];

  requestNumbersCollectionArray: Array<any> = [];
  uncMasterId: number;

  srNoMasterSpecifiDetailsArrayByMastIdParaName: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaName1: any;
  arrayOfCalDataResultForSelectedPara: Array<any> = [];
  srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate: Array<any> = [];
  srNoCalibrationResultDataDetailsArrayForUniqueNoChange: any;
  createMasterInstrumentSpecificationObjForUniqueSrNoChange: CreateUucParameterMasterSpecificationDetails = new CreateUucParameterMasterSpecificationDetails();


  uncMasterTypeData: UncMasterType = new UncMasterType();
  uncMasterTypeName: Array<any> = [];
  uncMasterTypeId: Array<any> = [];

  uncMasterNumberData: UncMasterNumber = new UncMasterNumber();
  uncMasterNumberName: Array<any> = [];
  uncMasterNumberId: Array<any> = [];

  uncProbabilityDistributionData: UncProbabilityDistribution = new UncProbabilityDistribution();
  uncProbabilityDistributionName: Array<any> = [];
  uncProbabilityDistributionId: Array<any> = [];

  uncSourceOfUncertaintyData: UncSourceOfUncertainty = new UncSourceOfUncertainty();
  uncSourceOfUncertaintyName: Array<any> = [];
  uncSourceOfUncertaintyId: Array<any> = [];

  uncTypeData: UncType = new UncType();
  uncTypeNameArray: Array<any> = [];
  uncTypeIdArray: Array<any> = [];

  uncDividingFactorData: UncDividingFactor = new UncDividingFactor();
  uncDividingFactorName: Array<any> = [];
  uncDividingFactorId: Array<any> = [];

  uncDegreeOfFreedomData: UncDegreeOfFreedom = new UncDegreeOfFreedom();
  uncDegreeOfFreedomName: Array<any> = [];
  uncDegreeOfFreedomId: Array<any> = [];

  newDynamic: any = {};
  newDynamic1: any = [{}];
  idForSearchUnc: number;



  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService, private createDefService: CreateDefService) {
  }

  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.createMasterSpecificationForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({

      sourceOfUncertainity: [],
      probabiltyDistribution: [],
      type: [],
      dividingFactor: [],
      sensitivityCoefficient: ['', [Validators.required, ValidationService.numberValidator]], 
      valueForEstimate: ['', [Validators.required, ValidationService.numberValidator]], 
      percentageForEstimateDg: ['', [Validators.required, ValidationService.numberValidator]], 
      percentageForEstimateAg: ['', [Validators.required, ValidationService.numberValidator]], 
      degreeOfFreedom: [],
      masterType: [],
      masterNumber: []

    });
  }

  //add new row 
  addNewRow() {
    this.formArr.push(this.initRows());
  }


  //To apply button color 
  ngAfterViewInit() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null) {
        if (this.dynamicArrayTrial[l].sourceOfUncertainity != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";  //green
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";  //red
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
      }


    }

  }


  ngOnInit() {

    // get Unc Master Type
    this.createDefService.getUncMasterType().subscribe(data => {
      this.uncMasterTypeData = data;

      for (var i = 0, l = Object.keys(this.uncMasterTypeData).length; i < l; i++) {
        this.uncMasterTypeName.push(this.uncMasterTypeData[i].uncMasterTypeName);
        this.uncMasterTypeId.push(this.uncMasterTypeData[i].uncMasterTypeId);
      }

    },
      error => console.log(error));

    // get Unc Master Number
    this.createDefService.getUncMasterNumber().subscribe(data => {
      this.uncMasterNumberData = data;

      for (var i = 0, l = Object.keys(this.uncMasterNumberData).length; i < l; i++) {
        this.uncMasterNumberName.push(this.uncMasterNumberData[i].uncMasterNumberName);
        this.uncMasterNumberId.push(this.uncMasterNumberData[i].uncMasterNumberId);
      }

    },
      error => console.log(error));


    // get Unc Probability Distribution
    this.createDefService.getUncProbabilityDistribution().subscribe(data => {
      this.uncProbabilityDistributionData = data;

      for (var i = 0, l = Object.keys(this.uncProbabilityDistributionData).length; i < l; i++) {
        this.uncProbabilityDistributionName.push(this.uncProbabilityDistributionData[i].probabilityDistributionName);
        this.uncProbabilityDistributionId.push(this.uncProbabilityDistributionData[i].probabilityDistributionId);
      }

    },
      error => console.log(error));


    // get Unc Source Of Uncertainty
    this.createDefService.getUncSourceOfUncertainty().subscribe(data => {
      this.uncSourceOfUncertaintyData = data;

      for (var i = 0, l = Object.keys(this.uncSourceOfUncertaintyData).length; i < l; i++) {
        this.uncSourceOfUncertaintyName.push(this.uncSourceOfUncertaintyData[i].uncSourceOfUncertaintyName);
        this.uncSourceOfUncertaintyId.push(this.uncSourceOfUncertaintyData[i].uncSourceOfUncertaintyId);

      }

    },
      error => console.log(error));


    // get Unc Type
    this.createDefService.getUncType().subscribe(data => {
      this.uncTypeData = data;

      for (var i = 0, l = Object.keys(this.uncTypeData).length; i < l; i++) {
        this.uncTypeNameArray.push(this.uncTypeData[i].uncTypeName);
        this.uncTypeIdArray.push(this.uncTypeData[i].uncTypeId);
      }
    },
      error => console.log(error));

    // get Unc Dividing Factor
    this.createDefService.getUncDividingFactor().subscribe(data => {
      this.uncDividingFactorData = data;

      for (var i = 0, l = Object.keys(this.uncDividingFactorData).length; i < l; i++) {
        this.uncDividingFactorName.push(this.uncDividingFactorData[i].uncDividingFactorName);
        this.uncDividingFactorId.push(this.uncDividingFactorData[i].uncDividingFactorId);
      }
    },
      error => console.log(error));


    // get Unc Degree Of Freedom
    this.createDefService.getUncDegreeOfFreedom().subscribe(data => {
      this.uncDegreeOfFreedomData = data;

      for (var i = 0, l = Object.keys(this.uncDegreeOfFreedomData).length; i < l; i++) {
        this.uncDegreeOfFreedomName.push(this.uncDegreeOfFreedomData[i].degreeOfFreedomName);
        this.uncDegreeOfFreedomId.push(this.uncDegreeOfFreedomData[i].degreeOfFreedomId);
      }
    },
      error => console.log(error));



    
    this.createUncNameMasterObj = JSON.parse(localStorage.getItem('createUncNameMasterObj'));
    this.uncMasterId = this.createUncNameMasterObj.createUncNameMasterId
    this.idForSearchUnc = this.createUncNameMasterObj.createUncNameMasterId
    console.log(this.createUncNameMasterObj);


    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType)

      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);

        this.dropdownList1.push({
          itemId: this.arrayOfCalibrationLabTypeId[i], itemText: this.arrayOfCalibrationLabTypeName[i]
        })
      }
      this.dropdownList = this.dropdownList1;
    },
      error => console.log(error));


    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);

        this.dropdownListForUom1.push({
          itemId: this.arrayUomId[i], itemText: this.arrayUomName[i]
        })
      }
      this.dropdownListForUom = this.dropdownListForUom1;
      this.dropdownListForIpUom = this.dropdownListForUom1;
    });


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.ParameterListForReqAndIwdObj[i].frequency)
      }
    })

    this.dynamicArrayTrial = null;
    this.selectParameter = "Select Parameter";


    this.dynamicArrayTrial = JSON.parse(localStorage.getItem('uncParaDetails'));



    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array([this.initRows()])
      });

    }
    else {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
            createUncParameterMasterSpecificationId,
            sourceOfUncertainity,
            probabiltyDistribution,
            type,
            dividingFactor,
            sensitivityCoefficient,
            valueForEstimate,
            percentageForEstimateDg,
            percentageForEstimateAg,
            degreeOfFreedom,
            masterType,
            masterNumber
          }) =>
            this.formBuilder.group({

              id: [createUncParameterMasterSpecificationId],
              sourceOfUncertainity: [sourceOfUncertainity],
              probabiltyDistribution: [probabiltyDistribution],
              type: [type],
              dividingFactor: [dividingFactor],
              sensitivityCoefficient: [sensitivityCoefficient , [Validators.required, ValidationService.numberValidator]],
              valueForEstimate: [valueForEstimate , [Validators.required, ValidationService.numberValidator]],
              percentageForEstimateDg: [percentageForEstimateDg , [Validators.required, ValidationService.numberValidator]],
              percentageForEstimateAg: [percentageForEstimateAg , [Validators.required, ValidationService.numberValidator]],
              degreeOfFreedom: [degreeOfFreedom],
              masterType: [masterType],
              masterNumber: [masterNumber]
            })
          )
        )
      })
    }



// detect value changes 
    (this.createMasterSpecificationForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial != null) {

        for (var i = 0, len = values.length; i < len; i++) {
          if ((values[i].sourceOfUncertainity != null && values[i].probabiltyDistribution != null &&
            values[i].type != null && values[i].dividingFactor != null && values[i].sensitivityCoefficient != null &&
            values[i].valueForEstimate != null && values[i].percentageForEstimateDg != null && values[i].percentageForEstimateAg != null &&
            values[i].degreeOfFreedom != null && values[i].masterType != null && values[i].masterNumber != null)) {
            if ((this.dynamicArrayTrial[i].sourceOfUncertainity !== values[i].sourceOfUncertainity || this.dynamicArrayTrial[i].probabiltyDistribution !== values[i].probabiltyDistribution
              || this.dynamicArrayTrial[i].type !== values[i].type || this.dynamicArrayTrial[i].dividingFactor !== values[i].dividingFactor
              || this.dynamicArrayTrial[i].sensitivityCoefficient !== values[i].sensitivityCoefficient || this.dynamicArrayTrial[i].valueForEstimate !== values[i].valueForEstimate
              || this.dynamicArrayTrial[i].percentageForEstimateDg !== values[i].percentageForEstimateDg || this.dynamicArrayTrial[i].percentageForEstimateAg !== values[i].percentageForEstimateAg
              || this.dynamicArrayTrial[i].degreeOfFreedom !== values[i].degreeOfFreedom || this.dynamicArrayTrial[i].masterType !== values[i].masterType
              || this.dynamicArrayTrial[i].masterNumber !== values[i].masterNumber)) {
              console.log("change" + i);
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";
            }
          }

        }
      }
    });

    //this.uncParameterValues();




    this.selectedItems = [

    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      //   itemsShowLimit: 2,
      //   allowSearchFilter: true,
    };



  }

  //delete unc specification row based on matching db data 
  deleteRow(i) { 

    this.createCertificateService.getCreatedSrNoUncParameterMasterSpecificationDetails(this.idForSearchUnc).subscribe(data => {
      console.log("sr no : " + data);
      //alert("The ")
      this.srNoMasterSpecifiDetailsArray1 = data;

      for (var g = 0; g < this.srNoMasterSpecifiDetailsArray1.length; g++) {
        if ((this.dynamicArrayTrial[i].sourceOfUncertainity == this.srNoMasterSpecifiDetailsArray1[g][3] && this.dynamicArrayTrial[i].probabiltyDistribution == this.srNoMasterSpecifiDetailsArray1[g][4]
          && this.dynamicArrayTrial[i].type == this.srNoMasterSpecifiDetailsArray1[g][5] && this.dynamicArrayTrial[i].dividingFactor == this.srNoMasterSpecifiDetailsArray1[g][6]
          && this.dynamicArrayTrial[i].sensitivityCoefficient == this.srNoMasterSpecifiDetailsArray1[g][7] && this.dynamicArrayTrial[i].valueForEstimate == this.srNoMasterSpecifiDetailsArray1[g][8]
          && this.dynamicArrayTrial[i].percentageForEstimateDg == this.srNoMasterSpecifiDetailsArray1[g][9] && this.dynamicArrayTrial[i].percentageForEstimateAg == this.srNoMasterSpecifiDetailsArray1[g][10]
          && this.dynamicArrayTrial[i].degreeOfFreedom == this.srNoMasterSpecifiDetailsArray1[g][11] && this.dynamicArrayTrial[i].masterType == this.srNoMasterSpecifiDetailsArray1[g][12]
          && this.dynamicArrayTrial[i].masterNumber == this.srNoMasterSpecifiDetailsArray1[g][13])) {

          this.createCertificateService.deleteCreatedUncParameterMasterSpecificationDetails(this.srNoMasterSpecifiDetailsArray1[g][0]).subscribe(data => {
            alert("deleted successfully ")
            this.dynamicArrayTrial.splice(i, 1);

            this.formArr.removeAt(i);

          })

        }
      }

    })


  }



  uncParameterValues() {


    // this.calibrationId_calibrationAt = this.createCertificateService.getCreatedEnvironmentalMaster().subscribe(data=>{
    //   this.createCertificateService.updateEnvironmentalMasterDetail(this.id, data).subscribe(result=>{

    //     this.searchEnvData = this.and_async( params : String) => {

    //         console.log("The calibration id and calibration at : "+this.envMasterInstruArray.push("The to :", "this"));   
    //          //                                                                                                                       

    //     }                                                                                                    
    //   })                                      
    // });



    this.createCertificateService.getCreatedSrNoUncParameterMasterSpecificationDetails(this.uncMasterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      if (this.srNoMasterSpecifiDetailsArray.length != 0) {

        for (var n = 0; n < this.srNoMasterSpecifiDetailsArray.length; n++) {

          this.newDynamic = {
            createUncParameterMasterSpecificationId: this.srNoMasterSpecifiDetailsArray[n][0],
            uncMasterInstruId: this.srNoMasterSpecifiDetailsArray[n][1],
            parameterNumber: this.srNoMasterSpecifiDetailsArray[n][2],
            sourceOfUncertainity: this.srNoMasterSpecifiDetailsArray[n][3],
            probabiltyDistribution: this.srNoMasterSpecifiDetailsArray[n][4],
            type: this.srNoMasterSpecifiDetailsArray[n][5],
            dividingFactor: this.srNoMasterSpecifiDetailsArray[n][6],
            sensitivityCoefficient: this.srNoMasterSpecifiDetailsArray[n][7],
            valueForEstimate: this.srNoMasterSpecifiDetailsArray[n][8],
            percentageForEstimateDg: this.srNoMasterSpecifiDetailsArray[n][9],
            percentageForEstimateAg: this.srNoMasterSpecifiDetailsArray[n][10],
            degreeOfFreedom: this.srNoMasterSpecifiDetailsArray[n][11],
            masterType: this.srNoMasterSpecifiDetailsArray[n][12],
            masterNumber: this.srNoMasterSpecifiDetailsArray[n][13]

          };
          this.dynamicArray.push(this.newDynamic);

        }

        this.dynamicArrayTrial = this.dynamicArray;


        var leng1 = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;

        if (leng1 == 0) {
          this.addNewRow();
        }
        if (leng1 >= 1) {
          while (leng1 >= 1) {

            leng1--;
            this.formArr.removeAt(leng1);

          }
        }

        for (var i = 0; i < this.dynamicArrayTrial.length; i++) {

          this.addNewRow();

          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('sourceOfUncertainity').patchValue(this.dynamicArrayTrial[i].sourceOfUncertainity);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('probabiltyDistribution').patchValue(this.dynamicArrayTrial[i].probabiltyDistribution);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('type').patchValue(this.dynamicArrayTrial[i].type);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('dividingFactor').patchValue(this.dynamicArrayTrial[i].dividingFactor);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('sensitivityCoefficient').patchValue(this.dynamicArrayTrial[i].sensitivityCoefficient);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('valueForEstimate').patchValue(this.dynamicArrayTrial[i].valueForEstimate);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('percentageForEstimateDg').patchValue(this.dynamicArrayTrial[i].percentageForEstimateDg);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('percentageForEstimateAg').patchValue(this.dynamicArrayTrial[i].percentageForEstimateAg);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('degreeOfFreedom').patchValue(this.dynamicArrayTrial[i].degreeOfFreedom);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('masterType').patchValue(this.dynamicArrayTrial[i].masterType);
          ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('masterNumber').patchValue(this.dynamicArrayTrial[i].masterNumber);

        }

      }
    })



  }







  ///////////////////////////////// save functionality ////////////////////////

  saveData(i) {

    this.dynamicArrayT = this.createMasterSpecificationForm.get('Rows').value;
    this.dynamicArrayTrial = this.dynamicArrayT;
    document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
    //console.                                                                                                                                              

    switch (this.status) {
      case 'Draft': this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Archieved': this.createMasterInstrumentSpecificationObj.draft = 0
        this.createMasterInstrumentSpecificationObj.archieved = 1;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Rejected': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 1;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Approved': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 1;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Submitted': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 1;
        break;

      default:
        this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

    }

    this.createUncParameterMasterSpecificationDetailsObj.uncMasterInstruId = this.uncMasterId;
    this.selectParameter = this.uncMasterId.toString();
    this.createUncParameterMasterSpecificationDetailsObj.percentageForEstimateAg = this.dynamicArrayT[i].percentageForEstimateAg;
    this.createUncParameterMasterSpecificationDetailsObj.percentageForEstimateDg = this.dynamicArrayT[i].percentageForEstimateDg;
    this.createUncParameterMasterSpecificationDetailsObj.valueForEstimate = this.dynamicArrayT[i].valueForEstimate;
    this.createUncParameterMasterSpecificationDetailsObj.sensitivityCoefficient = this.dynamicArrayT[i].sensitivityCoefficient;

    // all for - loops are used for converting selected names into respective ids
    for (var k = 0, l = Object.keys(this.uncMasterTypeName).length; k < l; k++) {
      if (this.dynamicArrayT[i].masterType == this.uncMasterTypeName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.masterType = this.uncMasterTypeId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.masterType = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncMasterNumberName).length; k < l; k++) {
      if (this.dynamicArrayT[i].masterNumber == this.uncMasterNumberName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.masterNumber = this.uncMasterNumberId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.masterNumber = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncProbabilityDistributionName).length; k < l; k++) {
      if (this.dynamicArrayT[i].probabiltyDistribution == this.uncProbabilityDistributionName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.probabiltyDistribution = this.uncProbabilityDistributionId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.probabiltyDistribution = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncSourceOfUncertaintyName).length; k < l; k++) {
      if (this.dynamicArrayT[i].sourceOfUncertainity == this.uncSourceOfUncertaintyName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.sourceOfUncertainity = this.uncSourceOfUncertaintyId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.sourceOfUncertainity = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncTypeNameArray).length; k < l; k++) {
      if (this.dynamicArrayT[i].type == this.uncTypeNameArray[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.type = this.uncTypeIdArray[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.type = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncDividingFactorName).length; k < l; k++) {
      if (this.dynamicArrayT[i].dividingFactor == this.uncDividingFactorName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.dividingFactor = this.uncDividingFactorId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.dividingFactor = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uncDegreeOfFreedomName).length; k < l; k++) {
      if (this.dynamicArrayT[i].degreeOfFreedom == this.uncDegreeOfFreedomName[k]) {
        this.createUncParameterMasterSpecificationDetailsObj.degreeOfFreedom = this.uncDegreeOfFreedomId[k];
        break;
      }
      else {
        this.createUncParameterMasterSpecificationDetailsObj.degreeOfFreedom = 1;
      }
    }

//  get list of all created unc specifications 
    this.createCertificateService.getCreatedSrNoUncParameterMasterSpecificationDetails(this.uncMasterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      this.srNoMasterSpecifiDetailsArrayByMastIdParaName = data;
      this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.splice(0, this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.length);

      for (var p = 0; p < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; p++) {
        this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.push(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[p][2]);
      }

      this.checkIfSrNoAlreadyPresent(i);
    })


  }

// check if unc specification already present or not
  checkIfSrNoAlreadyPresent(i) {

    if (this.srNoMasterSpecifiDetailsArray.length == 0) {

      this.createCertificateService.createdUncParameterMasterSpecificationDetailsFunction(this.createUncParameterMasterSpecificationDetailsObj).subscribe(data => {
        console.log("Unc Parameter Master specification details : " + data)
        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUncParameterMasterSpecificationId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;
        this.dynamicArrayT[i].parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        alert("Unc Parameter Master specification details  saved successfully !");
        this.createCertificateService.updateSrNoUncParameterMasterSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
          alert("parameter number updated successfully Unc!")
        })
      })

    }
    else {

      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; this.w++) {
        if (typeof this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[this.w] != 'undefined' && this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[this.w] != null) {
          if (this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][2] == this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[i] && this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][1] == this.uncMasterId) {
            this.createCertificateService.updateCreatedUncParameterMasterSpecificationDetails(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][0], this.createUncParameterMasterSpecificationDetailsObj).subscribe(data => {
              console.log("Unc Parameter Master specification details : " + data)
              alert("Unc Parameter Master specification details  updated successfully !");

              this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
              var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUncParameterMasterSpecificationId;
              this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.createMasterInstrumentSpecificationObj.masterIdParaName = this.selectParameter + "_" + idToChangeUniqueSrNo;

            })
            break;
          }
        }
      }

    }
    if (this.w >= this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length) {

      this.createCertificateService.createdUncParameterMasterSpecificationDetailsFunction(this.createUncParameterMasterSpecificationDetailsObj).subscribe(data => {
        console.log("Unc Parameter Master specification details : " + data)
        alert("Unc Parameter Master specification details  saved successfully !");

        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUncParameterMasterSpecificationId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;
        this.dynamicArrayT[i].parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        this.createCertificateService.updateSrNoUncParameterMasterSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
          alert("parameter number updated successfully For UNC !")
        })

      })
    }

  }



}



































