
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateUncNameMaster, CreateUncParameterMasterSpecificationDetails } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateDefService, UncMasterName } from '../../../../services/serviceconnection/create-service/create-def.service';
import { DynamicGridReqdocParaDetails, InstrumentListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-create-unc',
  templateUrl: './create-unc.component.html',
  styleUrls: ['./create-unc.component.css']
})
export class CreateUncComponent implements OnInit {
  // CreateUNCmasterDetailform: FormGroup;
  // dateString = '';
  // format = 'dd/MM/yyyy';

  // public dateValue = new Date();

  // row = [
  //   {
  //     select: '',
  //     sorceofuncertanity: '',
  //     probabilitydistribution: '',
  //     type: '',
  //     dicvidingfactor: '',
  //     sensitivitycoefficient: '',
  //     valueforestimate: '',
  //     percentageforestimate: '',
  //     degreeforfreedom: '',
  //     mastertype: '',
  //     mastername: ''
  //   }

  // ];

  // constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {
  //   this.CreateUNCmasterDetailform = this.formBuilder.group({

  //     UNC: [],
  //     Date: [],
  //     status: [],
  //     select: [],
  //     sourceofuncertainity: [],
  //     probabiltydistribution: [],
  //     type: [],
  //     dividingfactor: [],
  //     sensitivitycoefficient: [],
  //     valueforestimate: [],
  //     percentageforestimate: [],
  //     degreeoffreedom: [],
  //     mastertype: [],
  //     masternumber: []
  //   });
  // }

  // addTable() {
  //   const obj = {
  //     select: '',
  //     sorceofuncertanity: '',
  //     probabilitydistribution: '',
  //     type: '',
  //     dicvidingfactor: '',
  //     sensitivitycoefficient: '',
  //     valueforestimate: '',
  //     percentageforestimate: '',
  //     degreeforfreedom: '',
  //     mastertype: '',
  //     mastername: ''
  //   }
  //   this.row.push(obj)
  // }

  // deleteRow(x) {
  //   var delBtn = confirm(" Do you want to delete ?");
  //   if (delBtn == true) {
  //     this.row.splice(x, 1);
  //   }
  // }



  // ngOnInit() {
  // }


  createUNCNameform: FormGroup;
  createform: FormGroup;

  uncNameMastersObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayOfUncName: Array<any> = [];
  arrayOfUncId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  windowRef = null;

  uncName: any;
  dateOfConfiguration: any;
  status: any;

  createUncNameMasterObj: CreateUncNameMaster = new CreateUncNameMaster();
  data123: any;

  masterData: any;
  unc
  allUncNameMasterDetails: any;
  a: any;
  dateValue = Date.now();

  dynamicArrayT: Array<CreateUncParameterMasterSpecificationDetails> = [];
  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  newDynamic: any = {};
  newDynamic1: any = [{}];

  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray2: Array<DynamicGridReqdocParaDetails> = [];
  uncNameMastersObj1: any;
  uncParamScreen: number = 0;
  idForSearchUnc: any;
  alreadyPresent: number = 0;

  uncMasterNameList: UncMasterName = new UncMasterName();
  arrayUncMasterName: Array<any> = [];
  arrayUncMasterNameId: Array<any> = [];


  constructor(private formBuilder: FormBuilder, private datepipe: DatePipe, private router: Router, private createCertificateService: CreateCertificateService, private createDefService: CreateDefService) {

    this.createUNCNameform = this.formBuilder.group({

      uncName: [],
      dateOfConfiguration: [],
      status: []

    });

    this.createform = this.formBuilder.group({});

  }

  ngOnInit() {

    // this.createCertificateService.getCreatedUncNameMaster().subscribe(data => {
    //   this.uncNameMastersObj = data;
    //   for (var i = 0, l = Object.keys(this.uncNameMastersObj).length; i < l; i++) {
    //     this.arrayOfUncName.push(this.uncNameMastersObj[i].uncName);
    //     this.arrayOfUncId.push(this.uncNameMastersObj[i].createUucNameMasterId);     
    //   }
    // })

    this.createDefService.getUncMasterName().subscribe(data => {
      this.uncMasterNameList = data;
      for (var i = 0, l = Object.keys(this.uncMasterNameList).length; i < l; i++) {
        this.arrayUncMasterName.push(this.uncMasterNameList[i].uncMasterName);
        this.arrayUncMasterNameId.push(this.uncMasterNameList[i].uncMasterNameId);

      }
    })

  }

  openChildWindow() {

    if (this.uncParamScreen == 1) { //error

      this.uncParameterValues();
      var url = '/crtUncSpeDet';
      this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
      this.windowRef.focus();
      this.windowRef.addEventListener("message", this.receivemessage.bind(this), false);
    }
    else {
      alert("First Create UNC Master");
    }

  }

  receivemessage(evt: any) {
    console.log(evt.data);
  }


  create() {

    this.alreadyPresent = 0;

    switch (this.status) {
      case 'selectStatus': this.createUncNameMasterObj.draft = 0;
        this.createUncNameMasterObj.archieved = 0;
        this.createUncNameMasterObj.rejected = 0;
        this.createUncNameMasterObj.approved = 0;
        this.createUncNameMasterObj.submitted = 0;
        break;
      case 'Draft': this.createUncNameMasterObj.draft = 1;
        this.createUncNameMasterObj.archieved = 0;
        this.createUncNameMasterObj.rejected = 0;
        this.createUncNameMasterObj.approved = 0;
        this.createUncNameMasterObj.submitted = 0;
        break;
      case 'Approved': this.createUncNameMasterObj.draft = 0;
        this.createUncNameMasterObj.archieved = 0;
        this.createUncNameMasterObj.rejected = 0;
        this.createUncNameMasterObj.approved = 1;
        this.createUncNameMasterObj.submitted = 0;
        break;
      case 'Approved': this.createUncNameMasterObj.draft = 0;
        this.createUncNameMasterObj.archieved = 0;
        this.createUncNameMasterObj.rejected = 0;
        this.createUncNameMasterObj.approved = 0;
        this.createUncNameMasterObj.submitted = 1;
        break;

    }

    var dateOfConfig = this.convertStringToDate(this.createUNCNameform.value.dateOfConfiguration);
    this.createUncNameMasterObj.dateOfConfiguration = this.datepipe.transform(dateOfConfig, 'dd-MM-yyyy');
    this.createUncNameMasterObj.uncName = this.uncName;




    for (var k = 0, l = Object.keys(this.uncMasterNameList).length; k < l; k++) {
      if (this.uncName == this.arrayUncMasterName[k]) {
        this.createUncNameMasterObj.uncName = this.arrayUncMasterNameId[k];
        break;
      }
    }





    this.createCertificateService.getCreatedUncNameMaster().subscribe(data => {
      this.allUncNameMasterDetails = data;

      console.log("allMasterDetails : " + this.allUncNameMasterDetails)

      for (this.a = 0; this.a < Object.keys(this.allUncNameMasterDetails).length; this.a++) {
        //if ((this.allUncNameMasterDetails[this.a].uncName == this.createUncNameMasterObj.uncName.toLowerCase) ) {
        if ((this.allUncNameMasterDetails[this.a].dateOfConfiguration == this.createUncNameMasterObj.dateOfConfiguration) &&
          (this.allUncNameMasterDetails[this.a].uncName == this.createUncNameMasterObj.uncName)) {
          alert("Unc name master is already present !");
          this.alreadyPresent = 1;
          break;
        }
      }

      if (this.alreadyPresent != 1) {
        for (this.a = 0; this.a < Object.keys(this.allUncNameMasterDetails).length; this.a++) {
          if ((this.allUncNameMasterDetails[this.a].uncName == this.createUncNameMasterObj.uncName) &&
            (this.allUncNameMasterDetails[this.a].dateOfConfiguration != this.createUncNameMasterObj.dateOfConfiguration)) {
            this.myFunction();
            break;
          }

        }
      }
      if (this.a >= Object.keys(this.allUncNameMasterDetails).length) {

        this.createCertificateService.createUncNameMasterDetail(this.createUncNameMasterObj).subscribe(data => {
          console.log(data);
          this.uncNameMastersObj1 = data;
          this.idForSearchUnc = this.uncNameMastersObj1.createUncNameMasterId
          this.uncParamScreen = 1;
          localStorage.setItem('createUncNameMasterObj', JSON.stringify(data));
          alert("Unc Master details saved successfully!!");

        })
      }
    })


  }

  myFunction() {
    var txt;
    if (confirm("Unc name master is already present with the same unc name. Do you want to create new One ?!")) {
      this.createCertificateService.createUncNameMasterDetail(this.createUncNameMasterObj).subscribe(data => {
        console.log(data);
        this.uncNameMastersObj1 = data;
        this.idForSearchUnc = this.uncNameMastersObj1.createUncNameMasterId
        this.uncParamScreen = 1;

        localStorage.setItem('createUncNameMasterObj', JSON.stringify(data));
        alert("Unc Master details saved successfully!!");

      })
    }
    else {
      txt = " ";
    }
    document.getElementById("demo").innerHTML = txt;
  }



  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }


  uncParameterValues() {

    this.createCertificateService.getCreatedSrNoUncParameterMasterSpecificationDetails(this.idForSearchUnc).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      if (this.srNoMasterSpecifiDetailsArray.length != 0) {

        for (var n = 0; n < this.srNoMasterSpecifiDetailsArray.length; n++) {

          this.newDynamic = {
            createUncParameterMasterSpecificationId: this.srNoMasterSpecifiDetailsArray[n][0],
            uncMasterInstruId: this.srNoMasterSpecifiDetailsArray[n][1],
            parameterNumber: this.srNoMasterSpecifiDetailsArray[n][2],
            sourceOfUncertainity: this.srNoMasterSpecifiDetailsArray[n][3],
            probabiltyDistribution: this.srNoMasterSpecifiDetailsArray[n][4],
            type: this.srNoMasterSpecifiDetailsArray[n][5],
            dividingFactor: this.srNoMasterSpecifiDetailsArray[n][6],
            sensitivityCoefficient: this.srNoMasterSpecifiDetailsArray[n][7],
            valueForEstimate: this.srNoMasterSpecifiDetailsArray[n][8],
            percentageForEstimateDg: this.srNoMasterSpecifiDetailsArray[n][9],
            percentageForEstimateAg: this.srNoMasterSpecifiDetailsArray[n][10],
            degreeOfFreedom: this.srNoMasterSpecifiDetailsArray[n][11],
            masterType: this.srNoMasterSpecifiDetailsArray[n][12],
            masterNumber: this.srNoMasterSpecifiDetailsArray[n][13]

          };
          this.dynamicArray.push(this.newDynamic);

        }

        this.dynamicArrayTrial = this.dynamicArray;

      }

      localStorage.setItem('uncParaDetails', JSON.stringify(this.dynamicArrayTrial));
    })



  }




}
