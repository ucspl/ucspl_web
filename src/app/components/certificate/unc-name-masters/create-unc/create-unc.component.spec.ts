import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUncComponent } from './create-unc.component';

describe('CreateUncComponent', () => {
  let component: CreateUncComponent;
  let fixture: ComponentFixture<CreateUncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
