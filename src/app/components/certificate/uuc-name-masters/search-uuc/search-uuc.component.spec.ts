import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchUucComponent } from './search-uuc.component';

describe('SearchUucComponent', () => {
  let component: SearchUucComponent;
  let fixture: ComponentFixture<SearchUucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchUucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchUucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
