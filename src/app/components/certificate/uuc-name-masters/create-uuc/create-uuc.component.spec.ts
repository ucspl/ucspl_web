import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUucComponent } from './create-uuc.component';

describe('CreateUucComponent', () => {
  let component: CreateUucComponent;
  let fixture: ComponentFixture<CreateUucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
