import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateUucNameMaster } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService, InstrumentListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-create-uuc',
  templateUrl: './create-uuc.component.html',
  styleUrls: ['./create-uuc.component.css']
})
export class CreateUucComponent implements OnInit {

  CreateUUCNameform: FormGroup;
  createform: FormGroup;

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  windowRef = null;

  nomenclature: any;
  status: any;

  createUucNameMasterObj: CreateUucNameMaster = new CreateUucNameMaster();
  data123: any;

  masterData: any;
  allUucNameMasterDetails: any;
  a: any;
  uucParamScreen:any;

  constructor(private formBuilder: FormBuilder, private router: Router, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {

    this.CreateUUCNameform = this.formBuilder.group({

      nomenclature: [],
      nomenclature1: [],
      status: []

    });

    this.createform = this.formBuilder.group({});

  }

  ngOnInit() {

    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterIdFromNom.push(this.instruListForRequestAndInwardObj[i].parameterId);
      }
    })

  }

  openChildWindow() {
   if(this.uucParamScreen==1){ //error
    var url = '/crtUucSpeDet';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
  // console.
  //  this.win
    this.windowRef.addEventListener("message", this.receivemessage.bind(this), false);
  }
  else{
    alert("First Create UUC Master");
  }
  }
  receivemessage(evt: any) {
    console.log(evt.data);
  }


  create() {

    switch (this.status) {
      case 'selectStatus': this.createUucNameMasterObj.draft = 0;
        this.createUucNameMasterObj.archieved = 0;
        this.createUucNameMasterObj.rejected = 0;
        this.createUucNameMasterObj.approved = 0;
        this.createUucNameMasterObj.submitted = 0;
        break;
      case 'Draft': this.createUucNameMasterObj.draft = 1;
        this.createUucNameMasterObj.archieved = 0;
        this.createUucNameMasterObj.rejected = 0;
        this.createUucNameMasterObj.approved = 0;
        this.createUucNameMasterObj.submitted = 0;
        break;
      case 'Approved': this.createUucNameMasterObj.draft = 0;
        this.createUucNameMasterObj.archieved = 0;
        this.createUucNameMasterObj.rejected = 0;
        this.createUucNameMasterObj.approved = 1;
        this.createUucNameMasterObj.submitted = 0;
        break;

    }
    
    for (var k = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; k < l; k++) {

      if (this.nomenclature == this.arrayInstrumentListName[k]) {
        this.createUucNameMasterObj.instrument = this.arrayInstrumentListId[k];
        break;
      }
    }


    this.createCertificateService.getCreatedUucNameMaster().subscribe(data => {
      this.allUucNameMasterDetails = data;

      console.log("allMasterDetails : " + this.allUucNameMasterDetails)

      for (this.a = 0; this.a < Object.keys(this.allUucNameMasterDetails).length; this.a++) {
        if ((this.allUucNameMasterDetails[this.a].instrument == this.createUucNameMasterObj.instrument)) {
          alert("UUC Name Master already present !!");
          break;
        }
      }
      if (this.a >= Object.keys(this.allUucNameMasterDetails).length) {

        this.createCertificateService.createUucNameMasterDetail(this.createUucNameMasterObj).subscribe(data => {
          console.log(data);
          localStorage.setItem('createUucNameMasterObj', JSON.stringify(data));
          alert("UUC Master details saved successfully!!");
          this.uucParamScreen=1;

        })
      }
    })


      // create functionality to save the data in the database... 

      // trial below code delete if not needed afterwards                                                      

      // this.createReqAndInwardService.getChildCustomers().subscribe(dataOfChildCustomers=>{
      //   console.log(dataOfChildCustomers);

      //   for(var q=0;q<dataOfChildCustomers.length;q++){
      //     dataOfChildCustomers.push(this.data123);

      //     if(this.status != 1){
      //       console.log(this.data123); 
      //       this.arrayParameterIdFromNom.slice(0,this.data123.length);
      //       this.receivemessage( );
      //       this.

      //     }
      //   }
      // })

      //trial above code 

   

  }
}
