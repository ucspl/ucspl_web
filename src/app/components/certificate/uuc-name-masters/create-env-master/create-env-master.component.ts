import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateEnvMaster } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { SearchEnvMasterInstrument } from '../../../../services/serviceconnection/search-service/search-certificate.service';

declare var $:any;

@Component({
  selector: 'app-create-env-master',
  templateUrl: './create-env-master.component.html',
  styleUrls: ['./create-env-master.component.css']
})
export class CreateEnvMasterComponent implements OnInit {

  createEnvironmentalMasterForm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();
  createEnvMaster: CreateEnvMaster = new CreateEnvMaster();
  createEnvMasterObjForUpdate: CreateEnvMaster = new CreateEnvMaster();
  searchEnvMasterInstrument: SearchEnvMasterInstrument = new SearchEnvMasterInstrument();
  savedDataOfCreateEnvMaster: any;
  validFrmDtForUpdatingToDt:any;
  lastCreatedLab:any;

  arrayOfCreateSavedData  
  status: any;
  calibratedAt:any;

  arrayOflastCreatedLab: Array<any> = [];
  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  masterData: any;
  allEnvMasterDetails: any;
  a: any;
  calibrationLab: any;
  validFromDate: any;
  validFrmDtForUpdToDt: any;


  constructor(private formBuilder: FormBuilder,private datepipe:DatePipe, private router: Router, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {

    this.createEnvironmentalMasterForm = this.formBuilder.group({
      calibrationLab: [],
      calibratedAt: [],
      status: [],
      validFromDate: [],
      validToDate: [],
      temperature1: [],
      tempSign: [],
      temperature2: [],
      humidity1: [],
      humiditySign: [],
      humidity2: [],
      atmpress1: [],
      atmpressSign: [],
      atmpress2: []
    });
  }

  ngOnInit() {

    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType)

      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);
      }
    },
      error => console.log(error));

  }

  create() {

    this.createEnvMaster = this.createEnvironmentalMasterForm.value;
    console.log("data to be saved " + this.createEnvMaster);

    switch (this.status) {
      case 'Draft': this.createEnvMaster.draft = 1;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Archieved': this.createEnvMaster.draft = 0
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Rejected': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Approved': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 1;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Submitted': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 1;
        break;

      default: this.createEnvMaster.draft = 1;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

    }

    //save calibration lab Id
    for (var k = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; k < l; k++) {
      if (this.calibrationLab == this.arrayOfCalibrationLabTypeName[k]) {
        this.createEnvMaster.calibrationLab = this.arrayOfCalibrationLabTypeId[k];                
        break;
      }
      else {
        this.createEnvMaster.calibrationLab = 1;
      }
    }


    // valid from date   
    // var validFromDt = this.convertStringToDate(this.createEnvMaster.validFromDate); 
      
   var validFromDt = this.convertStringToDate(this.validFromDate);
   var validFromDt1 = this.datepipe.transform(validFromDt, 'dd-MM-yyyy');
    
    // var validFrmDtForUpdToDt = this.convertStringToDate1(this.createEnvMaster.validFromDate);


    /////////////////////////////////// uncomment below 4 lines 

    this.createEnvironmentalMasterForm.value.validToDate=this.validFromDate;
    this.createEnvironmentalMasterForm.value.validToDate.setDate(this.validFromDate.getDate() - 1);
    this.validFrmDtForUpdToDt=this.createEnvironmentalMasterForm.value.validToDate

    this.validFrmDtForUpdatingToDt = this.datepipe.transform(this.validFrmDtForUpdToDt, 'dd-MM-yyyy');
  
    this.createEnvMaster.validFromDate = validFromDt1;
    
   
    // valid to date     
    // var validToDt = this.convertStringToDate(this.createEnvMaster.validToDate);
    // this.createEnvMaster.validToDate = this.datepipe.transform(validToDt, 'dd-MM-yyyy');

                                                                                                                                                                                                                                  


    this.createCertificateService.getCreatedEnvironmentalMaster().subscribe(data => {
      this.allEnvMasterDetails = data;
      //this.arrayOfDynamicDataFromDt: 

      console.log("allMasterDetails : " + this.allEnvMasterDetails)

      for (this.a = 0; this.a < Object.keys(this.allEnvMasterDetails).length; this.a++) {
        if ((this.allEnvMasterDetails[this.a].calibrationLab == this.createEnvironmentalMasterForm.value.calibrationLab && this.allEnvMasterDetails[this.a].validFromDate == this.createEnvMaster.validFromDate )) {
          alert("ENV Name Master already present !!");        
          break;
        }
      }
      if (this.a >= Object.keys(this.allEnvMasterDetails).length) {

        this.searchEnvMasterInstrument.searchDocument=this.calibrationLab;
        this.searchEnvMasterInstrument.calibratedAt=this.calibratedAt;

        this.createCertificateService.getLastCreatedEnvMasterDetails(this.searchEnvMasterInstrument).subscribe(data=>{

          console.log(data);
          this.lastCreatedLab=data;
          this.arrayOflastCreatedLab=this.lastCreatedLab
          this.createEnvMasterObjForUpdate.validToDate = this.validFrmDtForUpdatingToDt;

          //tp below code//

          // if(this.createEnvironmentalMasterForm.value.validToDate){
          //    this.expConToFieldStruFor.push(this.allEnvMasterDetails[0][13]);
          //    this.arrayOfCalibrationLabType.
          // }

          //tp above code//

          if(this.arrayOflastCreatedLab!=null && typeof this.arrayOflastCreatedLab!='undefined' &&  this.arrayOflastCreatedLab.length!=0){
           
            this.createCertificateService.updateToDateForLab(this.lastCreatedLab[0][14],this.createEnvMasterObjForUpdate).subscribe(data=>{

            console.log(data);

            this.createEnvMaster.validToDate =null;
            this.createCertificateService.createEnvironmentalMasterDetail(this.createEnvMaster).subscribe(data => {
              console.log(data);
              this.savedDataOfCreateEnvMaster = data;  
                                                                                                             
              localStorage.setItem('createEnvMasterObj', JSON.stringify(data));                                 
              alert("ENV Master details saved successfully!!"); 
              
            })
  
          })
                                                                                                           
        }
        else{

          this.createEnvMaster.validToDate =null;
          this.createCertificateService.createEnvironmentalMasterDetail(this.createEnvMaster).subscribe(data => {
            console.log(data);
            this.savedDataOfCreateEnvMaster = data;  
                                                                                                           
            localStorage.setItem('createEnvMasterObj', JSON.stringify(data));                                 
            alert("ENV Master details saved successfully!!");

          })
        }

      })
    }
  })
       
    
  }

  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV =  value.slice(0, 2) 
      var monthV = value.slice(3, 5) 
      var yearV =  value.slice(6, 10) 
      var yyyyV = parseInt(yearV)    
      var mmV =   parseInt(monthV)  
      var ddV =   parseInt(dateV)  

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }

  convertStringToDate1(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV =  value.slice(0, 2) 
      var monthV = value.slice(3, 5) 
      var yearV =  value.slice(6, 10) 
      var yyyyV = parseInt(yearV)    
      var mmV =   parseInt(monthV)  
      var ddV =   parseInt(dateV)  

      mmV = mmV - 1;
      ddV = ddV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }

}

