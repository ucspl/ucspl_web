import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEnvMasterComponent } from './create-env-master.component';

describe('CreateEnvMasterComponent', () => {
  let component: CreateEnvMasterComponent;
  let fixture: ComponentFixture<CreateEnvMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEnvMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEnvMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
