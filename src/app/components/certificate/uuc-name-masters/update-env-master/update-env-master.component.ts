import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateEnvMaster } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { SearchEnvMasterInstrument } from '../../../../services/serviceconnection/search-service/search-certificate.service';

declare var $: any;
@Component({
  selector: 'app-update-env-master',
  templateUrl: './update-env-master.component.html',
  styleUrls: ['./update-env-master.component.css']
})
export class UpdateEnvMasterComponent implements OnInit {

  updateEnvironmentalMasterForm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();
  createEnvMaster: CreateEnvMaster = new CreateEnvMaster();
  createEnvMasterObjForUpdate: CreateEnvMaster = new CreateEnvMaster();
  searchEnvMasterInstrument: SearchEnvMasterInstrument = new SearchEnvMasterInstrument();
  savedDataOfCreateEnvMaster: any;
  validFrmDtForUpdatingToDt: any;
  lastCreatedLab: any;

  arrayOfCreateSavedData 
  calibrationLab: any;
  calibratedAt: any;
  validFromDate: any;
  temperature1: any;
  temperature2: any;
  humidity1: any;
  humidity2: any;
  atmpress1: any;
  atmpress2: any;

  status: any;
  draft: any;
  submitted: any;
  approved: any;

  arrayOflastCreatedLab: Array<any> = [];
  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  masterData: any;
  allEnvMasterDetails: any;
  a: any;
  validFrmDtForUpdToDt: any;
  updEnvMasterIdData: any;
  datePickerDisabled: number;
  statusApprove: number;

  constructor(private formBuilder: FormBuilder, private datepipe: DatePipe, private router: Router, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {

    this.updateEnvironmentalMasterForm = this.formBuilder.group({
      calibrationLab: [],
      calibratedAt: [],
      status: [],
      validFromDate: [],
      validToDate: [],
      temperature1: [],
      tempSign: [],
      temperature2: [],
      humidity1: [],
      humiditySign: [],
      humidity2: [],
      atmpress1: [],
      atmpressSign: [],
      atmpress2: []
    });
  }

  ngOnInit() {

    this.updEnvMasterIdData = JSON.parse(localStorage.getItem('updEnvMasterId'));
    this.checkStatus();
    this.displayData();


    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType);


      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);
      }
    },
      error => console.log(error));


  }

  setStatus() {

    switch (this.status) {
      case 'Draft': this.createEnvMaster.draft = 1;
        this.draft = 1;
        this.statusApprove = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Archieved': this.createEnvMaster.draft = 0
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Rejected': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Approved': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 1;
        this.approved = 1;
        this.statusApprove = 1;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Submitted': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 1;
        this.submitted = 1;
        this.statusApprove = 0;
        break;

      default: this.createEnvMaster.draft = 1;
        this.draft = 1;
        this.statusApprove = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;

        break;

    }
    this.checkStatus();
  }

  checkStatus() {

    if (this.updEnvMasterIdData[13] != 0 || this.approved == 1) {
      (<HTMLInputElement>document.getElementById("calibrationLabTag")).disabled = true;
      (<HTMLInputElement>document.getElementById("calibrationAtTag")).disabled = true;
      (<HTMLInputElement>document.getElementById("statusTag")).disabled = true;

      // (<HTMLInputElement>document.getElementById("datepicker1")).disabled = true;                        
      (<HTMLInputElement>document.getElementById("temp1Tag")).disabled = true;
      (<HTMLInputElement>document.getElementById("temp2Tag")).disabled = true;
      (<HTMLInputElement>document.getElementById("humidity1Tag")).disabled = true;
      (<HTMLInputElement>document.getElementById("humidity2Tag")).disabled = true;
      (<HTMLInputElement>document.getElementById("atmpress1Tag")).disabled = true;
      (<HTMLInputElement>document.getElementById("atmpress2Tag")).disabled = true;
      this.statusApprove = 1;
      this.status="Approved"
    }
    else {
      this.statusApprove = 0;
      this.status="Draft";
    }
  }

  unlockFunctionalities() {

    this.status = "Draft";
    this.statusApprove = 0;
    (<HTMLInputElement>document.getElementById("calibrationLabTag")).disabled = false;
    (<HTMLInputElement>document.getElementById("calibrationAtTag")).disabled = false;
    (<HTMLInputElement>document.getElementById("statusTag")).disabled = false;

    //(<HTMLInputElement>document.getElementById("datepicker1")).disabled = false;
    (<HTMLInputElement>document.getElementById("temp1Tag")).disabled = false;
    (<HTMLInputElement>document.getElementById("temp2Tag")).disabled = false;
    (<HTMLInputElement>document.getElementById("humidity1Tag")).disabled = false;
    (<HTMLInputElement>document.getElementById("humidity2Tag")).disabled = false;
    (<HTMLInputElement>document.getElementById("atmpress1Tag")).disabled = false;
    (<HTMLInputElement>document.getElementById("atmpress2Tag")).disabled = false;
  }

  displayData() {

    this.calibrationLab = this.updEnvMasterIdData[14];
    this.calibratedAt = this.updEnvMasterIdData[1];
    this.validFromDate = this.updEnvMasterIdData[3];
    this.temperature1 = this.updEnvMasterIdData[5];
    this.temperature2 = this.updEnvMasterIdData[6];
    this.humidity1 = this.updEnvMasterIdData[7];
    this.humidity2 = this.updEnvMasterIdData[8];
    this.atmpress1 = this.updEnvMasterIdData[9];
    this.atmpress2 = this.updEnvMasterIdData[10];

  }


  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }



  update() {

    this.createEnvMaster = this.updateEnvironmentalMasterForm.value;
    console.log("data to be saved " + this.createEnvMaster);

    switch (this.status) {
      case 'Draft': this.createEnvMaster.draft = 1;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Archieved': this.createEnvMaster.draft = 0
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Rejected': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Approved': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 1;
        this.createEnvMaster.submitted = 0;
        break;

      case 'Submitted': this.createEnvMaster.draft = 0;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 1;
        break;

      default: this.createEnvMaster.draft = 1;
        this.createEnvMaster.approved = 0;
        this.createEnvMaster.submitted = 0;
        break;

    }

    //save calibration lab Id
    for (var k = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; k < l; k++) {
      if (this.calibrationLab == this.arrayOfCalibrationLabTypeName[k]) {
        this.createEnvMaster.calibrationLab = this.arrayOfCalibrationLabTypeId[k];
        break;
      }
      else {
        this.createEnvMaster.calibrationLab = 1;
      }
    }


    // valid from date   
    // var validFromDt = this.convertStringToDate(this.createEnvMaster.validFromDate); 

    var validFromDt = this.convertStringToDate(this.validFromDate);
    var validFromDt1 = this.datepipe.transform(validFromDt, 'dd-MM-yyyy');

    // var validFrmDtForUpdToDt = this.convertStringToDate1(this.createEnvMaster.validFromDate);

    this.updateEnvironmentalMasterForm.value.validToDate = validFromDt;
    this.updateEnvironmentalMasterForm.value.validToDate.setDate(validFromDt.getDate() - 1);
    this.validFrmDtForUpdToDt = this.updateEnvironmentalMasterForm.value.validToDate

    this.validFrmDtForUpdatingToDt = this.datepipe.transform(this.validFrmDtForUpdToDt, 'dd-MM-yyyy');

    this.createEnvMaster.validFromDate = validFromDt1;

    

  
    // valid to date     
    // var validToDt = this.convertStringToDate(this.createEnvMaster.validToDate);
    // this.createEnvMaster.validToDate = this.datepipe.transform(validToDt, 'dd-MM-yyyy');

                                                                                                                                                                                                                                     


    this.searchEnvMasterInstrument.searchDocument = this.calibrationLab;
    this.searchEnvMasterInstrument.calibratedAt = this.calibratedAt;
    this.searchEnvMasterInstrument.envMasterId = this.updEnvMasterIdData[2];

    this.createCertificateService.getSecondLastCreatedEnvMasterDetails(this.searchEnvMasterInstrument).subscribe(data => {

      console.log(data);
      this.lastCreatedLab = data;
      this.arrayOflastCreatedLab = this.lastCreatedLab
      this.createEnvMasterObjForUpdate.validToDate = this.validFrmDtForUpdatingToDt;


      if (this.arrayOflastCreatedLab != null && typeof this.arrayOflastCreatedLab != 'undefined' && this.arrayOflastCreatedLab.length != 0) {

        this.createCertificateService.updateToDateForLab(this.lastCreatedLab[0][14], this.createEnvMasterObjForUpdate).subscribe(data => {

          console.log(data);

          this.createEnvMaster.validToDate = this.updEnvMasterIdData[4];
          this.createCertificateService.updateEnvironmentalMasterDetail(this.updEnvMasterIdData[2], this.createEnvMaster).subscribe(data => {
            console.log(data);
            this.savedDataOfCreateEnvMaster = data;

            localStorage.setItem('updEnvMasterId', JSON.stringify(data));
            alert("ENV Master details updated successfully!!");

          })

        })

      }
      else {

        this.createEnvMaster.validToDate = this.updEnvMasterIdData[4];
        this.createCertificateService.updateEnvironmentalMasterDetail(this.updEnvMasterIdData[2], this.createEnvMaster).subscribe(data => {
          console.log(data);
          this.savedDataOfCreateEnvMaster = data;

          localStorage.setItem('updEnvMasterId', JSON.stringify(data));
          alert("ENV Master details updated successfully!!");

        })
      }

    })

  }

}
