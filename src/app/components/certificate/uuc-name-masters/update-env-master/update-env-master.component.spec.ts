import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEnvMasterComponent } from './update-env-master.component';

describe('UpdateEnvMasterComponent', () => {
  let component: UpdateEnvMasterComponent;
  let fixture: ComponentFixture<UpdateEnvMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEnvMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEnvMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
