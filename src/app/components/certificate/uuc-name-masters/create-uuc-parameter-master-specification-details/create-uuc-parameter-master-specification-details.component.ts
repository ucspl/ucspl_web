import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CodegenComponentFactoryResolver } from '@angular/core/src/linker/component_factory_resolver';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateMasterInstrumentSpecificationDetails, CreateUucNameMaster, CreateUucParameterMasterSpecificationDetails, JunctionOfInputUomAndCreateUUCParaSpecification, JunctionOfUomAndCreateUUCParaSpecification, ModeTypeForMasterInstrument, SearchMasterInstruByMIdPNmAndStatus, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-create-uuc-parameter-master-specification-details',
  templateUrl: './create-uuc-parameter-master-specification-details.component.html',
  styleUrls: ['./create-uuc-parameter-master-specification-details.component.css']
})
export class CreateUucParameterMasterSpecificationDetailsComponent implements OnInit {

  public createMasterSpecificationForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 30;

  searchMasterInstruByMIdPNmAndStatusObj: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();

  createMasterInstrumentSpecificationObj: CreateMasterInstrumentSpecificationDetails = new CreateMasterInstrumentSpecificationDetails();
  dynamicArray: Array<CreateMasterInstrumentSpecificationDetails> = [];

  createUucParameterMasterSpecificationDetailsObj: CreateUucParameterMasterSpecificationDetails = new CreateUucParameterMasterSpecificationDetails();

  modeTypeForMasterInstrumentObj: ModeTypeForMasterInstrument = new ModeTypeForMasterInstrument();
  arrayModeType: Array<any> = [];
  arrayModeTypeListId: Array<any> = [];
  arrayModeTypeListName: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracy: Array<any> = [];
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];
  arrayOfParameterFrequency: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  tempChartTypeForMasterInstrumentObj: TempChartTypeForMasterInstrument = new TempChartTypeForMasterInstrument();
  arrayOfTempChartType: Array<any> = [];
  arrayTempChartTypeId: Array<any> = [];
  arrayTempChartTypeNames: Array<any> = [];

  //FrequencyUom
  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames: Array<any> = [];
  arrayOfFrequencyUomId: Array<any> = [];


  dynamicArrayT: Array<CreateUucParameterMasterSpecificationDetails> = [];
  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  showFreq: any;
  freq: any;
  previousStatus: any;

  dropdownList = [];
  dropdownList1 = [];
  dropdownList2 = [];

  dropdownListForUom = [];
  dropdownListForUom1 = [];
  dropdownListForIpUom = [];
  dropdownListForIpUom1 = [];

  trialDropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  createUucNameMasterObj: CreateUucNameMaster = new CreateUucNameMaster();


  arrayOfSelectedUom: Array<any> = [];
  arrayOfSelectedIpUom: Array<any> = [];

  requestNumbersCollectionArray: Array<any> = [];
  uucMasterId: number;

  srNoMasterSpecifiDetailsArrayByMastIdParaName: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaName1: any;
  arrayOfCalDataResultForSelectedPara: Array<any> = [];
  srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate: Array<any> = [];
  srNoCalibrationResultDataDetailsArrayForUniqueNoChange: any;
  createMasterInstrumentSpecificationObjForUniqueSrNoChange: CreateUucParameterMasterSpecificationDetails = new CreateUucParameterMasterSpecificationDetails();

  juncOfIpUomObj: JunctionOfInputUomAndCreateUUCParaSpecification = new JunctionOfInputUomAndCreateUUCParaSpecification();
  juncOfUomObj: JunctionOfUomAndCreateUUCParaSpecification = new JunctionOfUomAndCreateUUCParaSpecification();

  arrayJuncOfIpUomObj: Array<any> = [];
  arrayJuncOfUomObj: Array<any> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {
  }

  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.createMasterSpecificationForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({

      parameter: [],
      instruUom: [],
      calibrationLabParameter: [],
      rAndRTable: [],
      uncMaster: [],
      ipParameter: [],
      ipParameterUom: [],
      calibrationProcedure: [],
      defaultRepetability: [],
      uncReading: [],
      typeOfRepetability: [],
      uncPrintUnit: [],
      massCalMethod: [],
      nablScopeName: [],
      remarks: [],
      accrediation: []


    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }



  ngOnInit() {

    this.createUucNameMasterObj = JSON.parse(localStorage.getItem('createUucNameMasterObj'));
    this.uucMasterId = this.createUucNameMasterObj.createUucNameMasterId
    console.log(this.createUucNameMasterObj);


    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType)

      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);

        this.dropdownList1.push({
          itemId: this.arrayOfCalibrationLabTypeId[i], itemText: this.arrayOfCalibrationLabTypeName[i]
        })

      }

      this.dropdownList = this.dropdownList1;
    },
      error => console.log(error));


    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 1, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);

        this.dropdownListForUom1.push({
          itemId: this.arrayUomId[i - 1], itemText: this.arrayUomName[i - 1]
        })
      }
      this.dropdownListForUom = this.dropdownListForUom1;
      this.dropdownListForIpUom = this.dropdownListForUom1;
    });


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.ParameterListForReqAndIwdObj[i].frequency)
      }
    })

    this.dynamicArrayTrial = null;
    this.selectParameter = "Select Parameter";

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array([this.initRows()])
      });
    }
    else {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
            id,
            parameter,
            instruUom,
            calibrationLabParameter,
            rAndRTable,
            uncMaster,
            ipParameter,
            ipParameterUom,
            calibrationProcedure,
            defaultRepetability,
            uncReading,
            typeOfRepetability,
            uncPrintUnit,
            massCalMethod,
            nablScopeName,
            remarks,
            accrediation
          }) =>
            this.formBuilder.group({

              id: [id],
              parameter: [parameter],
              instruUom: [instruUom],
              calibrationLabParameter: [calibrationLabParameter],
              rAndRTable: [rAndRTable],
              uncMaster: [uncMaster],
              ipParameter: [ipParameter],
              ipParameterUom: [ipParameterUom],
              calibrationProcedure: [calibrationProcedure],
              defaultRepetability: [defaultRepetability],
              uncReading: [uncReading],
              typeOfRepetability: [typeOfRepetability],
              uncPrintUnit: [uncPrintUnit],
              massCalMethod: [massCalMethod],
              nablScopeName: [nablScopeName],
              remarks: [remarks],
              accrediation: [accrediation]
            })
          )
        )
      })
    }

    this.selectedItems = [

    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      //   itemsShowLimit: 2,
      //   allowSearchFilter: true,
    };

  }

  ////////////////////////// select deselect of uom ////////////////////////////

  onItemSelectUom(item: any) {
    console.log(item);
    this.arrayOfSelectedUom.push(item);

    this.newDynamic = {

      createUucId: this.uucMasterId,
      createUucParameterMasterSpecificationId: 1,
      uomId: item.itemId

    };
    this.arrayJuncOfUomObj.push(this.newDynamic);
  }

  onSelectAllUom(items: any) {
    console.log(items);
    this.arrayOfSelectedUom.splice(0, this.arrayOfSelectedUom.length);
    this.arrayJuncOfUomObj.splice(0, this.arrayJuncOfUomObj.length);
    this.arrayOfSelectedUom = items;


    for (var j = 0; j < this.arrayOfSelectedUom.length; j++) {
      //  this.arrayOfSelectedUom.push(items[j]);

      this.newDynamic = {

        createUucId: this.uucMasterId,
        createUucParameterMasterSpecificationId: 1 ,
        uomId: this.arrayOfSelectedUom[j].itemId

      };

      this.arrayJuncOfUomObj.push(this.newDynamic);

    }
    console.log(this.arrayJuncOfUomObj);
  }



  onDeselectAllUom(items: any) {
    this.arrayOfSelectedUom.splice(0, this.arrayOfSelectedUom.length);
    this.arrayJuncOfUomObj.slice(0, this.arrayJuncOfUomObj.length);

    //console.log("itemId On the item gets displayed by ")
  }

  onItemDeSelectUom(item: any) {
    console.log(item);
    //write filter code here 
    for (var i = 0; i < this.arrayOfSelectedUom.length; i++) {
      if (item.itemId == this.arrayOfSelectedUom[i].itemId) {

        var trial = [this.arrayOfSelectedUom[i].itemId];

        this.arrayOfSelectedUom = this.arrayOfSelectedUom.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.arrayOfSelectedUom);

        this.arrayJuncOfUomObj = this.arrayJuncOfUomObj.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.arrayJuncOfUomObj);

        /*************************this code is trial delet if any error occurs */
        //  var trialDynamicArray=this.dynamicArray.sort();

        //  while( this.indexToDelete != 0){
        //   if(this.selectedItems.length !=0){
        //     this.        
        //   }
        //  }


                                             


        /*************************  this code is trial delet if any error occurs  ***************************/ 

      }
    }
    console.log(this.arrayJuncOfUomObj);
  }

  ////////////////////////// select deselect of input parameter uom ////////////////////////////


  onItemSelectIpUom(item: any) {
    console.log(item);
    this.arrayOfSelectedIpUom.push(item);

    this.newDynamic1 = {

      createUucId: this.uucMasterId,
      createUucParameterMasterSpecificationId: 1,
      uomId: item.itemId

    };
    this.arrayJuncOfIpUomObj.push(this.newDynamic);

  }

  onSelectAllIpUom(items: any) {
    console.log(items);
    this.arrayOfSelectedIpUom.splice(0, this.arrayOfSelectedIpUom.length);
    this.arrayJuncOfIpUomObj.splice(0, this.arrayOfSelectedIpUom.length);
    this.arrayOfSelectedIpUom = items;


    for (var j = 0; j < this.arrayOfSelectedIpUom.length; j++) {
      //  this.arrayOfSelectedUom.push(items[j]);

      this.newDynamic = {

        createUucId: this.uucMasterId,
        createUucParameterMasterSpecificationId: 1,
        uomId: this.arrayOfSelectedIpUom[j].itemId

      };

      this.arrayJuncOfIpUomObj.push(this.newDynamic);

    }
    console.log(this.arrayJuncOfIpUomObj);
  }

  onDeselectAllIpUom(items: any) {
    this.arrayOfSelectedIpUom.splice(0, this.arrayOfSelectedIpUom.length);
    this.arrayJuncOfIpUomObj.splice(0, this.arrayJuncOfIpUomObj.length);
  }

  onItemDeSelectIpUom(item: any) {
    console.log(item);
    for (var i = 0; i < this.arrayOfSelectedIpUom.length; i++) {
      if (item.itemId == this.arrayOfSelectedIpUom[i].itemId) {

        var trial = [this.arrayOfSelectedIpUom[i].itemId];

        this.arrayOfSelectedIpUom = this.arrayOfSelectedIpUom.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.arrayOfSelectedIpUom);

        this.arrayJuncOfIpUomObj = this.arrayJuncOfIpUomObj.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.arrayJuncOfIpUomObj);

      }
    }
    console.log(this.arrayJuncOfUomObj);
  }



  /////////////////////////////////////////////////////////////////////////////////////////////

  onItemSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    console.log(item);
    // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.requestNumbersCollectionArray.push(item);
    // this.getSrNoData();      uncomment this afterwards
  }
  onSelectAll(items: any) {

    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);

    for (var j = 0; j < items.length; j++) {
      this.requestNumbersCollectionArray.push(items[j]);

    }
    // this.DocTypeFunForGrouping();   uncomment this afterwards
    // this.getSrNoData();
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";

  }
  onDeselectAll(items: any) {

    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    //document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";

    //    var length=(this.reqdocParaDetailsForm.get('Rows')as FormArray).length;
    //    if(length>=1){
    //   while(length>=0){
    //     length--;
    //     this.formArr.removeAt(length);

    // }
    // this.addNewRow();

    //    }

  }
  onItemDeSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {
      if (item.itemId == this.requestNumbersCollectionArray[i].itemId) {

        // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
        var trial = [this.requestNumbersCollectionArray[i].itemId];
        var trailReqNo = [this.requestNumbersCollectionArray[i].itemText];
        console.log(trailReqNo);
        // this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(trailReqNo).subscribe(data => {
        //   this.trailReqNoData=data;

        this.requestNumbersCollectionArray = this.requestNumbersCollectionArray.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.requestNumbersCollectionArray);

        // })

      }

    }
    // this.getSrNoData();     //uncomment this afterwards

  }



  ///////////////////////////////// save functionality ////////////////////////

  saveData(i) {

    this.selectParameter = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('parameter').value;
    this.dynamicArrayT = this.createMasterSpecificationForm.get('Rows').value;

    this.createUucParameterMasterSpecificationDetailsObj.uucMasterInstruId = this.uucMasterId;
    this.createUucParameterMasterSpecificationDetailsObj.parameter = this.dynamicArrayT[i].parameter;
    this.createUucParameterMasterSpecificationDetailsObj.parameterNumber = this.dynamicArrayT[i].parameterNumber;
    this.createUucParameterMasterSpecificationDetailsObj.instruUom = this.dynamicArrayT[i].instruUom;
    this.createUucParameterMasterSpecificationDetailsObj.calibrationLabParameter = this.dynamicArrayT[i].calibrationLabParameter;
    this.createUucParameterMasterSpecificationDetailsObj.rAndRTable = this.dynamicArrayT[i].rAndRTable;
    this.createUucParameterMasterSpecificationDetailsObj.uncMaster = this.dynamicArrayT[i].uncMaster;
    this.createUucParameterMasterSpecificationDetailsObj.ipParameter = this.dynamicArrayT[i].ipParameter;
    this.createUucParameterMasterSpecificationDetailsObj.ipParameterUom = this.dynamicArrayT[i].ipParameterUom;
    this.createUucParameterMasterSpecificationDetailsObj.calibrationProcedure = this.dynamicArrayT[i].calibrationProcedure;            
    this.createUucParameterMasterSpecificationDetailsObj.defaultRepetability = this.dynamicArrayT[i].defaultRepetability;
    this.createUucParameterMasterSpecificationDetailsObj.uncReading = this.dynamicArrayT[i].uncReading;
    this.createUucParameterMasterSpecificationDetailsObj.typeOfRepetability = this.dynamicArrayT[i].typeOfRepetability;
    this.createUucParameterMasterSpecificationDetailsObj.uncPrintUnit = this.dynamicArrayT[i].uncPrintUnit;
    this.createUucParameterMasterSpecificationDetailsObj.nablScopeName = this.dynamicArrayT[i].nablScopeName;
    this.createUucParameterMasterSpecificationDetailsObj.remarks = this.dynamicArrayT[i].remarks;
    this.createUucParameterMasterSpecificationDetailsObj.accrediation = this.dynamicArrayT[i].accrediation;


    //temperary
    this.createUucParameterMasterSpecificationDetailsObj.parameter = this.dynamicArrayT[i].parameter;
    this.createUucParameterMasterSpecificationDetailsObj.parameterNumber = this.dynamicArrayT[i].parameterNumber;
    this.createUucParameterMasterSpecificationDetailsObj.instruUom = 2;
    this.createUucParameterMasterSpecificationDetailsObj.calibrationLabParameter = 2;
    this.createUucParameterMasterSpecificationDetailsObj.rAndRTable = 2;
    this.createUucParameterMasterSpecificationDetailsObj.uncMaster = 2;
    this.createUucParameterMasterSpecificationDetailsObj.ipParameter = 2;
    this.createUucParameterMasterSpecificationDetailsObj.ipParameterUom = 2;
    this.createUucParameterMasterSpecificationDetailsObj.calibrationProcedure = 2;
    this.createUucParameterMasterSpecificationDetailsObj.defaultRepetability = 2;
    this.createUucParameterMasterSpecificationDetailsObj.uncReading = this.dynamicArrayT[i].uncReading;
    this.createUucParameterMasterSpecificationDetailsObj.typeOfRepetability = this.dynamicArrayT[i].typeOfRepetability;
    this.createUucParameterMasterSpecificationDetailsObj.uncPrintUnit = this.dynamicArrayT[i].uncPrintUnit;
    this.createUucParameterMasterSpecificationDetailsObj.nablScopeName = 2;
    this.createUucParameterMasterSpecificationDetailsObj.remarks = 2;
    this.createUucParameterMasterSpecificationDetailsObj.accrediation = this.dynamicArrayT[i].accrediation;
    this.createUucParameterMasterSpecificationDetailsObj.parameterNumber = "para_num";

    this.createUucParameterMasterSpecificationDetailsObj.massCalMethod = "masscal";


    switch (this.status) {
      case 'Draft': this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Archieved': this.createMasterInstrumentSpecificationObj.draft = 0
        this.createMasterInstrumentSpecificationObj.archieved = 1;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Rejected': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 1;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Approved': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 1;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Submitted': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 1;
        break;

      default: this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

    }

    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
      if (this.selectParameter == this.arrayParameterNames[k]) {
        this.createUucParameterMasterSpecificationDetailsObj.parameter = this.arrayParameterId[k];
        break;
      }
      else {
        this.createUucParameterMasterSpecificationDetailsObj.parameter = 1;
      }
    }


    this.createCertificateService.getCreatedSrNoUucParameterMasterSpecificationDetails(this.uucMasterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      this.srNoMasterSpecifiDetailsArrayByMastIdParaName = data;

      for (var p = 0; p < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; p++) {
        this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.push(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[p][3]);
      }


      this.checkIfSrNoAlreadyPresent(i);
    })


    // this.createCertificateService.createdUucParameterMasterSpecificationDetailsFunction(this.createUucParameterMasterSpecificationDetailsObj).subscribe(data => {
    //   console.log("Data saved successfully : " + data);
    // })

    

  }

  checkIfSrNoAlreadyPresent(i) {


    if (this.srNoMasterSpecifiDetailsArray.length == 0) {
      this.createCertificateService.createdUucParameterMasterSpecificationDetailsFunction(this.createUucParameterMasterSpecificationDetailsObj).subscribe(data => {
        console.log("Uuc Parameter Master specification details : " + data)
        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUucParameterMasterSpecificationId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        alert("Uuc Parameter Master specification details saved successfully !");
        this.createCertificateService.updateSrNoUucParameterMasterSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          //console.log(data1);
          console.log("updating uuc list and parameter number to ")
          alert("parameter number updated successfully!")


          // this.createCertificateService.saveUomAndUucSpeciDataInJuncTable(this.juncOfIpUomObj).subscribe(data => {
          //   console.log(data);
          // })


        })
      })
    }
    else {
      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; this.w++) {
        if (typeof this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[i] != 'undefined') {
          if (this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][3] == this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[i] && this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][1] == this.uucMasterId) {
            this.createCertificateService.updateCreatedUucParameterMasterSpecificationDetails(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][0], this.createUucParameterMasterSpecificationDetailsObj).subscribe(data => {
              console.log("Uuc Parameter Master specification details : " + data)
              alert("Uuc Parameter Master specification details  saved successfully !!");

              this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
              var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUucParameterMasterSpecificationId;
              this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.createMasterInstrumentSpecificationObj.masterIdParaName = this.selectParameter + "_" + idToChangeUniqueSrNo;


              // this.createCertificateService.updateSrNoUucParameterMasterSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
              //   console.log(data1);
              //   alert("parameter number updated successfully!")
              // })

            })
            break;
          }
        }
      }
    }
    if (this.w >= this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length) {
      this.createCertificateService.createdUucParameterMasterSpecificationDetailsFunction(this.createUucParameterMasterSpecificationDetailsObj).subscribe(data => {
        console.log("Uuc Parameter Master specification details : " + data)
        alert("Uuc Parameter Master specification details saved successfully !!");

        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUucParameterMasterSpecificationId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;


        this.createCertificateService.updateSrNoUucParameterMasterSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
          alert("parameter number updated successfully !!");

          console.log("unique para number updated in ")

        })

      })
    }
  }
}





