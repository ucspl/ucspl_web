import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUucParameterMasterSpecificationDetailsComponent } from './create-uuc-parameter-master-specification-details.component';

describe('CreateUucParameterMasterSpecificationDetailsComponent', () => {
  let component: CreateUucParameterMasterSpecificationDetailsComponent;
  let fixture: ComponentFixture<CreateUucParameterMasterSpecificationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUucParameterMasterSpecificationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUucParameterMasterSpecificationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
