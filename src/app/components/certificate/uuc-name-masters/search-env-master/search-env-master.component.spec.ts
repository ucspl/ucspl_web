import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEnvMasterComponent } from './search-env-master.component';

describe('SearchEnvMasterComponent', () => {
  let component: SearchEnvMasterComponent;
  let fixture: ComponentFixture<SearchEnvMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchEnvMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEnvMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
