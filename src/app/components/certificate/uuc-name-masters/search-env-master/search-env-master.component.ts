import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { SearchCertificateService, SearchMasterInstrument } from '../../../../services/serviceconnection/search-service/search-certificate.service';

@Component({
  selector: 'app-search-env-master',
  templateUrl: './search-env-master.component.html',
  styleUrls: ['./search-env-master.component.css']
})
export class SearchEnvMasterComponent implements OnInit {

  searchEnvMasterForm: FormGroup;
  searchEnvMaster: SearchMasterInstrument = new SearchMasterInstrument();

  data: any;
  envMasterInstruArray: Array<any> = [];

  p: number = 1;
  itemsPerPage: number = 5;

  public dateValue1 = new Date();
  public dateValue2 = new Date();
  docNo: any;
  idForSearchSales: any;
  id: number;

  constructor(private http: HttpClient, private router: Router, private formBuilder: FormBuilder, private searchCertificateService: SearchCertificateService, private createCertificateService: CreateCertificateService) {

    this.searchEnvMasterForm = formBuilder.group({
      searchDocument: [],                         //                                                                                                                                                                                                                                                                                                          
      itemsPerPage: []
    })
  }

  ngOnInit() {


  }


  searchData() {

    debugger;
    this.searchEnvMaster.searchDocument = this.searchEnvMasterForm.value.searchDocument;

          

/*********** trial code ***********/

    


/*********** trial code ***********/

    if (this.searchEnvMaster.searchDocument == null) {
      this.searchEnvMaster.searchDocument = ''
    }
    else {
      this.searchEnvMaster.searchDocument = this.searchEnvMaster.searchDocument.trim();
    }

    this.searchCertificateService.searchEnvMasterData(this.searchEnvMaster).subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.envMasterInstruArray = this.data;            //comment bcitsnn  option                                                                          
      // this.create                                    //console.log("this.")                                                                                                                                                                                                                                              
      // console.log("this.")                           //                                                                                      

      // this.calibrationId_calibrationAt = this.createCertificateService.getCreatedEnvironmentalMaster().subscribe(data=>{
      //   this.createCertificateService.updateEnvironmentalMasterDetail(this.id, data).subscribe(result=>{
      //     console.log("Result :: " + );        
      //   })                                      
      // })                                                                      

    })


  }



  moveToMIUpdateScreen(i) {


    i = ((this.itemsPerPage * (this.p - 1)) + (i));
    console.log("index will be...." + i);

    this.docNo = parseInt(i);

    this.searchCertificateService.setDocNoForMasterInstruDocScreen(this.envMasterInstruArray[this.docNo]);

    this.idForSearchSales = this.envMasterInstruArray[this.docNo];
    localStorage.setItem('updEnvMasterId', JSON.stringify(this.idForSearchSales));

    this.router.navigateByUrl('/nav/updateenv');

    console.log(" this.updateEnv  = this.count ++ ");

  }







}
