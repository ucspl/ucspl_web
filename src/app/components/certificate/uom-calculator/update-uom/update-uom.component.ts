import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateUncParameterMasterSpecificationDetails, CreateUomCalculator } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService, DynamicGridReqdocParaDetails, InstrumentListForRequestAndInward, ParameterListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-update-uom',
  templateUrl: './update-uom.component.html',
  styleUrls: ['./update-uom.component.css']
})
export class UpdateUomComponent implements OnInit {

  createUOMCalculatorForm: FormGroup;

  parameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];
  arrayOfParameterFrequency: Array<any> = [];

  uomParamScreen: number = 0;
  windowRef = null;

  createUNCNameform: FormGroup;
  createform: FormGroup;

  uncNameMastersObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayOfUncName: Array<any> = [];
  arrayOfUncId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];


  uncName: any;
  dateOfConfiguration: any;
  status: any;

  createUomCalculatorObj: CreateUomCalculator = new CreateUomCalculator();
  data123: any;

  masterData: any;
  unc
  allUomCalculatorDetails: any;
  a: any;
  dateValue = Date.now();

  dynamicArrayT: Array<CreateUncParameterMasterSpecificationDetails> = [];
  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  newDynamic: any = {};
  newDynamic1: any = [{}];

  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray2: Array<DynamicGridReqdocParaDetails> = [];
  uncNameMastersObj1: any;
  idForSearchUom: any;
  alreadyPresent: number = 0;


  arrayUncMasterName: Array<any> = [];
  arrayUncMasterNameId: Array<any> = [];
  selectParameter: any;
  idForSearchUOM: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private datepipe: DatePipe, private createCertificateService: CreateCertificateService, private createReqAndInwardService: CreateReqAndInwardService) {

    this.createUOMCalculatorForm = this.formBuilder.group({

      selectParameter: ['', [Validators.required]],
      parameter: [],
      status: ['', [Validators.required]]

    });
  }


  ngOnInit() {


    this.idForSearchUOM = JSON.parse(localStorage.getItem('uomIdForUpdate'));
    this.idForSearchUOM = parseInt(this.idForSearchUOM);



    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.parameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.parameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.parameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.parameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.parameterListForReqAndIwdObj[i].frequency)
      }




      this.createCertificateService.getCreatedUomCalculatorById(this.idForSearchUOM).subscribe(data => {
        console.log("info of id :")

        this.uncNameMastersObj1 = data;

        this.selectParameter = this.uncNameMastersObj1.parameter;

        localStorage.setItem('createUomCalculatorObj', JSON.stringify(data));

        if (this.uncNameMastersObj1.draft == 1) {
          this.status = "Draft";
        } else if (this.uncNameMastersObj1.approved == 1) {
          this.status = "Approved";
        } else if (this.uncNameMastersObj1.submitted == 1) {
          this.status = "Submitted";
        }


        for (var k = 0, l = Object.keys(this.parameterListForReqAndIwdObj).length; k < l; k++) {
          if (this.selectParameter == this.arrayParameterId[k]) {
            this.selectParameter = this.arrayParameterNames[k];
            break;
          }
        }

      });


    })


  }


  openChildWindow() {

    this.uomParameterValues();

  }

  receivemessage(evt: any) {
    console.log(evt.data);
  }


  create() {

    this.alreadyPresent = 0;

    switch (this.status) {
      case 'selectStatus': this.createUomCalculatorObj.draft = 0;
        this.createUomCalculatorObj.archieved = 0;
        this.createUomCalculatorObj.rejected = 0;
        this.createUomCalculatorObj.approved = 0;
        this.createUomCalculatorObj.submitted = 0;
        break;
      case 'Draft': this.createUomCalculatorObj.draft = 1;
        this.createUomCalculatorObj.archieved = 0;
        this.createUomCalculatorObj.rejected = 0;
        this.createUomCalculatorObj.approved = 0;
        this.createUomCalculatorObj.submitted = 0;
        break;
      case 'Approved': this.createUomCalculatorObj.draft = 0;
        this.createUomCalculatorObj.archieved = 0;
        this.createUomCalculatorObj.rejected = 0;
        this.createUomCalculatorObj.approved = 1;
        this.createUomCalculatorObj.submitted = 0;
        break;
      case 'Submitted': this.createUomCalculatorObj.draft = 0;
        this.createUomCalculatorObj.archieved = 0;
        this.createUomCalculatorObj.rejected = 0;
        this.createUomCalculatorObj.approved = 0;
        this.createUomCalculatorObj.submitted = 1;
        break;
    }


    this.createUomCalculatorObj.parameter = this.selectParameter;


    if (typeof this.selectParameter != 'undefined') {

      for (var k = 0, l = Object.keys(this.parameterListForReqAndIwdObj).length; k < l; k++) {
        if (this.selectParameter == this.arrayParameterNames[k]) {
          this.createUomCalculatorObj.parameter = this.arrayParameterId[k];
          break;
        }
      }


      this.createCertificateService.updateUomCalculator(this.idForSearchUOM, this.createUomCalculatorObj).subscribe(data => {
        this.allUomCalculatorDetails = data;

        localStorage.setItem('createUomCalculatorObj', JSON.stringify(this.allUomCalculatorDetails));
        console.log("allMasterDetails : " + this.allUomCalculatorDetails);
        alert("Updated Successfully");

      })

    }




  }

  myFunction() {
    var txt;
    if (confirm("Uom calculator parameter is already created. Do you want to create new One ?!")) {
      // this.createCertificateService.createUncNameMasterDetail(this.createUncNameMasterObj).subscribe(data => {
      //   console.log(data);
      //   this.uncNameMastersObj1=data;
      //   this.idForSearchUnc=this.uncNameMastersObj1.createUncNameMasterId
      //   this.uncParamScreen=1;

      //   localStorage.setItem('createUncNameMasterObj', JSON.stringify(data));
      //   alert("Unc Master details saved successfully!!");

      // })
    }
    else {
      txt = " ";
    }
    document.getElementById("demo").innerHTML = txt;
  }



  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }


  uomParameterValues() {

    this.dynamicArray.splice(0, this.dynamicArray.length);

    this.createCertificateService.getCreatedSrNoUomCalculatorSpecificationDetails(this.idForSearchUOM).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      if (this.srNoMasterSpecifiDetailsArray.length != 0) {

        for (var n = 0; n < this.srNoMasterSpecifiDetailsArray.length; n++) {

          this.newDynamic = {
            createUomCalculatorDetailsId: this.srNoMasterSpecifiDetailsArray[n][0],
            uomCalculatorId: this.srNoMasterSpecifiDetailsArray[n][1],
            parameterNumber: this.srNoMasterSpecifiDetailsArray[n][2],
            uomFrom: this.srNoMasterSpecifiDetailsArray[n][3],
            uomTo: this.srNoMasterSpecifiDetailsArray[n][4],
            multiplyBy: this.srNoMasterSpecifiDetailsArray[n][5]

          };
          this.dynamicArray.push(this.newDynamic);
        }
        this.dynamicArrayTrial = this.dynamicArray;
      }
      localStorage.setItem('uomParaDetails', JSON.stringify(this.dynamicArrayTrial));


      var url = '/crtUomCalDet';
      this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
      this.windowRef.focus();
      this.windowRef.addEventListener("message", this.receivemessage.bind(this), false);

    })


  }


}
