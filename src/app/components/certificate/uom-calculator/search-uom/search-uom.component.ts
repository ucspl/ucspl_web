import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator } from '../../../../services/config/config.service';
import { SearchCertificateService } from '../../../../services/serviceconnection/search-service/search-certificate.service';

@Component({
  selector: 'app-search-uom',
  templateUrl: './search-uom.component.html',
  styleUrls: ['./search-uom.component.css']
})
export class SearchUomComponent implements OnInit {

  searchUomForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  model: any = {};
  uomCalculatorData: any;

  userIdLength: number;
  userData: any;
  idForSearchUomCalculator: Object;
  status: any;
  searchUncMasterData: any;
  searchUomCalculator: any;
  usersArray: any;
  uomCalculatorNo: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private searchCertificateService: SearchCertificateService) {

    this.searchUomForm = this.formBuilder.group({

      name: [''],
      status: [],
      itemsPerPage: []
    })
  }

  ngOnInit() {
    this.status = "1";
    console.log("uom calculator name to search ")
  }


  searchData() {
    debugger;

    this.searchUncMasterData = this.searchUomForm.value;
    this.searchUomCalculator = this.searchUomForm.value.name;
    console.log("unc name to search : " + this.searchUomCalculator)
    if (this.searchUomCalculator == "") {
      this.searchUomCalculator = " ";
    }

    this.searchCertificateService.searchUomCalculatorData(this.searchUomCalculator).subscribe((res: any) => {

      this.uomCalculatorData = res;
      this.usersArray = res;

    })

  }

  uncNameToUpdateUnc(i) {

    this.uomCalculatorNo = i.target.innerText;
    // this.uncNo=parseInt(this.uncNo);

    for (var s = 0; s < this.uomCalculatorData.length; s++) {
      if (this.uomCalculatorData[s][1] == this.uomCalculatorNo) {
        this.idForSearchUomCalculator = this.uomCalculatorData[s][0];
        localStorage.setItem('uomIdForUpdate', JSON.stringify(this.idForSearchUomCalculator));                
        // localStorage.setItem('createUncNameMasterObj', JSON.stringify(this.idForSearchUnc));             

        break;
      }
    }

    this.router.navigateByUrl('/nav/updateuom')


  }
}
