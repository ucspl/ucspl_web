import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUomCalculatorDetailsComponent } from './create-uom-calculator-details.component';

describe('CreateUomCalculatorDetailsComponent', () => {
  let component: CreateUomCalculatorDetailsComponent;
  let fixture: ComponentFixture<CreateUomCalculatorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUomCalculatorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUomCalculatorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
