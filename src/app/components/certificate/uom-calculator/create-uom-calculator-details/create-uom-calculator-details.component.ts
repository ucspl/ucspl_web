import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../services/config/config.service';
import { CreateCertificateService, CreateUomCalculator, CreateUomCalculatorDetails } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { UncMasterNumber, UncMasterType } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateReqAndInwardService, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

declare var $: any;
@Component({
  selector: 'app-create-uom-calculator-details',
  templateUrl: './create-uom-calculator-details.component.html',
  styleUrls: ['./create-uom-calculator-details.component.css']
})
export class CreateUomCalculatorDetailsComponent implements OnInit {

  dateString = '';
  format = 'dd/MM/yyyy';
  date: any;

  public dateValue = new Date();
  public createMasterSpecificationForm: FormGroup;

  p: number = 1;
  itemsPerPage: number = 30;

  parameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  dynamicArrayTrial: Array<any> = [];
  srNoNablSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArray1: Array<any> = [];
  srNoNablSpecifiDetailsArrayByNablIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  nablScopeMasterId: any;
  calibrationLabParameter: any;

  arrayGroupNameAccorToLab1: Array<any> = [];
  arrayUncFormulaList: Array<any> = [];
  arrayOfUncFormulaNames: Array<any> = [];
  arrayOfUncFormulaId: Array<any> = [];

  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames: Array<any> = [];
  arrayOfFrequencyUomId: Array<any> = [];
  groupParameter: any;
  nablScopeName: any;
  showFreq: any;
  freq: any;
  arrayOfParameterFrequency: Array<any> = [];
  previousStatus: any;
  forAccuValue1: any;
  parameterFromCreateScreen: any;
  createUomCalculatorObj: any;
  uomCalculatorId: any;
  parameter: any;

  createMasterInstrumentSpecificationObj: CreateUomCalculator = new CreateUomCalculator();
  dynamicArray: Array<CreateUomCalculator> = [];
  dynamicArrayForUomChecking: Array<CreateUomCalculator> = [];

  createUomCalculatorDetailsObj: CreateUomCalculatorDetails = new CreateUomCalculatorDetails();

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  createMasterInstrumentSpecificationObjForUniqueSrNoChange: CreateUomCalculatorDetails = new CreateUomCalculatorDetails();

  dynamicArrayForCheckingUom:  Array<any> = [];

  dynamicArrayT: Array<CreateUomCalculatorDetails> = [];

  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  dropdownList = [];
  dropdownList1 = [];
  dropdownList2 = [];

  dropdownListForUom = [];
  dropdownListForUom1 = [];
  dropdownListForIpUom = [];
  dropdownListForIpUom1 = [];

  trialDropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  createUncNameMasterObj: CreateUomCalculator = new CreateUomCalculator();

  arrayOfSelectedUom: Array<any> = [];
  arrayOfSelectedIpUom: Array<any> = [];

  requestNumbersCollectionArray: Array<any> = [];
  uncMasterId: number;

  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaName: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaName1: any;
  arrayOfCalDataResultForSelectedPara: Array<any> = [];
  srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate: Array<any> = [];
  srNoCalibrationResultDataDetailsArrayForUniqueNoChange: any;


  uncMasterTypeData: UncMasterType = new UncMasterType();
  uncMasterTypeName: Array<any> = [];
  uncMasterTypeId: Array<any> = [];

  uncMasterNumberData: UncMasterNumber = new UncMasterNumber();
  uncMasterNumberName: Array<any> = [];
  uncMasterNumberId: Array<any> = [];

  newDynamic: any = {};
  newDynamic1: any = [{}];
  idForSearchUnc: number;
  uomFromForChecking: any;
  uomToForChecking: any;
  showMessage: number;
  c: number = 0;
  dontSave: number = 0;
  status1: any;
  click: boolean = false;
  g: number;


  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }

  get formArr() {
    return this.createMasterSpecificationForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      createUomCalculatorDetailsId: [],
      uomCalculatorId: [],
      parameterNumber: [],
      uomFrom: [],
      uomTo: [],
      multiplyBy: ['', [Validators.required, ValidationService.numberValidator]]
    });
  }


  constructor(private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {

  }





  //add new row 
  addNewRow() {
    this.formArr.push(this.initRows());
  }


  deleteRow(i) {

     if(this.click!=true){
    this.createCertificateService.getCreatedSrNoUomCalculatorSpecificationDetails(this.uomCalculatorId).subscribe(data => {
      console.log("sr no : " + data);
      //alert("The ")
      this.srNoMasterSpecifiDetailsArray1 = data;

      if (typeof this.dynamicArrayTrial[i] != 'undefined' && this.dynamicArrayTrial[i] != null) {
        for (this.g = 0; this.g < this.srNoMasterSpecifiDetailsArray1.length; this.g++) {
          if (this.dynamicArrayTrial[i].uomFrom == this.srNoMasterSpecifiDetailsArray1[this.g][3] && this.dynamicArrayTrial[i].uomTo == this.srNoMasterSpecifiDetailsArray1[this.g][4]
            && this.dynamicArrayTrial[i].multiplyBy == this.srNoMasterSpecifiDetailsArray1[this.g][5]) {

            this.createCertificateService.deleteCreatedUomCalculatorSpecificationDetails(this.srNoMasterSpecifiDetailsArray1[this.g][0]).subscribe(data => {
              alert("deleted successfully ")
              this.dynamicArrayTrial.splice(i, 1);

              this.formArr.removeAt(i);

            })

          }
        }
      }

      else { 
        // if(this.g>=this.srNoMasterSpecifiDetailsArray1.length){                                                                                                                     

        this.formArr.removeAt(i);                                                

        // }
      }

    })
     }

  }


  //To apply button color 
  ngAfterViewInit() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
        if (typeof this.dynamicArrayTrial[l] != 'undefined' && this.dynamicArrayTrial[l].multiplyBy != "" && typeof this.dynamicArrayTrial[l].multiplyBy != 'undefined') {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";  //green
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";  //red
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";                             
      }


    }

  }


  ngOnInit() {


    this.createUomCalculatorObj = JSON.parse(localStorage.getItem('createUomCalculatorObj'));
    this.uomCalculatorId = this.createUomCalculatorObj.uomCalculatorId;



    //this is the 

    //if(this.status1=="Approved"){}

    this.parameter = this.createUomCalculatorObj.parameter;
    console.log(this.createUomCalculatorObj);

    this.dynamicArrayTrial = JSON.parse(localStorage.getItem('uomParaDetails'));
  //  this.dynamicArrayForCheckingUom=   JSON.parse(localStorage.getItem('uomParaDetails'));

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array([this.initRows()])
      });

    }
    else {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
            createUomCalculatorDetailsId,
            uomCalculatorId,
            parameterNumber,
            uomFrom,
            uomTo,
            multiplyBy,

          }) =>
            this.formBuilder.group({

              createUomCalculatorDetailsId: [createUomCalculatorDetailsId],
              uomCalculatorId: [uomCalculatorId],
              parameterNumber: [parameterNumber],
              uomFrom: [uomFrom],
              uomTo: [uomTo],
              multiplyBy: [multiplyBy, [Validators.required, ValidationService.numberValidator]]

            })
          )
        )
      })
    }

    if (this.createUomCalculatorObj.draft == 1) {
      this.status1 = "Draft";
    } else if (this.createUomCalculatorObj.approved == 1) {
      this.status1 = "Approved";
      this.createMasterSpecificationForm.disable();
       this.click=true;
    } else if (this.createUomCalculatorObj.submitted == 1) {
      this.status1 = "Submitted";
    }

    //get parameter list                                                                                
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.parameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.parameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.parameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.parameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.parameterListForReqAndIwdObj[i].frequency);
      }

      //get UOM list                                                                             
      this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
        this.uomListForRequestAndInwardObj = data;
        for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
          this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
          this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
          this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
        }
        this.uomForParameter();
      });

    });

    // detect value changes 
    (this.createMasterSpecificationForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined' && values != null && typeof values != 'undefined') {

        for (var i = 0, len = values.length; i < len; i++) {
          if ((values[i].uomFrom != null && values[i].uomTo != null && values[i].multiplyBy != null && typeof this.dynamicArrayTrial[i] != 'undefined')) {
            if ((this.dynamicArrayTrial[i].uomFrom !== values[i].uomFrom || this.dynamicArrayTrial[i].uomTo !== values[i].uomTo
              || this.dynamicArrayTrial[i].multiplyBy !== values[i].multiplyBy)) {
              console.log("change" + i);
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";
            }
          }

        }
      }
    });

  }



  uomForParameter() {

    for (var iii = 0, lll = Object.keys(this.uomListForRequestAndInwardObj).length; iii < lll; iii++) {
      if (this.uomListForRequestAndInwardObj[iii].parameterId == this.parameter) {
        this.arrayUomNameAccorToParam1.push(this.uomListForRequestAndInwardObj[iii].uomName)
      }
    }

    console.log("this.arrayUomNameAccorToParam1 " + this.arrayUomNameAccorToParam1);

  }

  checkUom(i) {
    this.dontSave = 0;
    this.dynamicArrayForCheckingUom = this.createMasterSpecificationForm.get('Rows').value;
    this.dynamicArrayForCheckingUom.splice(i,1);
  // okonaimasu                                                                                                                                                                                                                                                                                                                                                            

    this.uomFromForChecking = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomFrom').value;
    this.uomToForChecking = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomTo').value;

    if (this.uomFromForChecking != null && this.uomToForChecking != null) {

      for (this.c = 0; this.c < this.dynamicArrayForCheckingUom.length; this.c++) {
        if (this.dynamicArrayForCheckingUom[this.c].uomFrom == this.uomFromForChecking && this.dynamicArrayForCheckingUom[this.c].uomTo == this.uomToForChecking) {
    
          $("#showMsg-" + i).html("Duplicate Entry !! ");
          $("#showMsg1-" + i).html("Duplicate Entry !! ");
          this.dontSave = 1;

          break;
        }
        else {
       
          this.dontSave = 0;
          $("#showMsg-" + i).html(" ");
          $("#showMsg1-" + i).html(" ");
        }
      }

      if (this.c >= this.dynamicArrayForCheckingUom.length) {
       
        this.dontSave = 0;
        $("#showMsg-" + i).html(" ");
        $("#showMsg1-" + i).html(" ");
      }

    }


  }

  ///////////////////////////////// save functionality //////////////////////// 

  saveData(i) {
    if (this.dontSave == 1) {
      alert("Can not save Dublicate Entry !!")
    }
    if (this.dontSave == 0) {
      this.dynamicArrayT = this.createMasterSpecificationForm.get('Rows').value;
      this.dynamicArrayTrial = this.dynamicArrayT;
      document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c"; 
      //console.                                                                                                                                              

      switch (this.status) {
        case 'Draft': this.createMasterInstrumentSpecificationObj.draft = 1;
          this.createMasterInstrumentSpecificationObj.archieved = 0;
          this.createMasterInstrumentSpecificationObj.rejected = 0;
          this.createMasterInstrumentSpecificationObj.approved = 0;
          this.createMasterInstrumentSpecificationObj.submitted = 0;
          break;

        case 'Archieved': this.createMasterInstrumentSpecificationObj.draft = 0
          this.createMasterInstrumentSpecificationObj.archieved = 1;
          this.createMasterInstrumentSpecificationObj.rejected = 0;
          this.createMasterInstrumentSpecificationObj.approved = 0;
          this.createMasterInstrumentSpecificationObj.submitted = 0;
          break;

        case 'Rejected': this.createMasterInstrumentSpecificationObj.draft = 0;
          this.createMasterInstrumentSpecificationObj.archieved = 0;
          this.createMasterInstrumentSpecificationObj.rejected = 1;
          this.createMasterInstrumentSpecificationObj.approved = 0;
          this.createMasterInstrumentSpecificationObj.submitted = 0;
          break;

        case 'Approved': this.createMasterInstrumentSpecificationObj.draft = 0;
          this.createMasterInstrumentSpecificationObj.archieved = 0;
          this.createMasterInstrumentSpecificationObj.rejected = 0;
          this.createMasterInstrumentSpecificationObj.approved = 1;
          this.createMasterInstrumentSpecificationObj.submitted = 0;
          break;

        case 'Submitted': this.createMasterInstrumentSpecificationObj.draft = 0;
          this.createMasterInstrumentSpecificationObj.archieved = 0;
          this.createMasterInstrumentSpecificationObj.rejected = 0;
          this.createMasterInstrumentSpecificationObj.approved = 0;
          this.createMasterInstrumentSpecificationObj.submitted = 1;
          break;

        default:
          this.createMasterInstrumentSpecificationObj.draft = 1;
          this.createMasterInstrumentSpecificationObj.archieved = 0;
          this.createMasterInstrumentSpecificationObj.rejected = 0;
          this.createMasterInstrumentSpecificationObj.approved = 0;
          this.createMasterInstrumentSpecificationObj.submitted = 0;
          break;

      }

      this.createUomCalculatorDetailsObj.uomCalculatorId = this.uomCalculatorId;
      this.selectParameter = this.uomCalculatorId.toString();
      this.createUomCalculatorDetailsObj.uomFrom = this.dynamicArrayT[i].uomFrom;
      this.createUomCalculatorDetailsObj.uomTo = this.dynamicArrayT[i].uomTo;
      this.createUomCalculatorDetailsObj.multiplyBy = this.dynamicArrayT[i].multiplyBy;

      // all for - loops are used for converting selected names into respective ids
      for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
        if (this.dynamicArrayT[i].uomFrom == this.arrayUomName[k]) {
          this.createUomCalculatorDetailsObj.uomFrom = this.arrayUomId[k];
          break;
        }
        else {
          this.createUomCalculatorDetailsObj.uomFrom = 1;
        }
      }

      for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
        if (this.dynamicArrayT[i].uomTo == this.arrayUomName[k]) {
          this.createUomCalculatorDetailsObj.uomTo = this.arrayUomId[k];
          break;
        }
        else {
          this.createUomCalculatorDetailsObj.uomTo = 1;
        }
      }



      //  get list of all created uom calculator specifications  
      this.createCertificateService.getCreatedSrNoUomCalculatorSpecificationDetails(this.uomCalculatorId).subscribe(data => {
        console.log("sr no : " + data);
        this.srNoMasterSpecifiDetailsArray = data;

      //  this.dynamicArrayForCheckingUom=data;


        // if (this.srNoMasterSpecifiDetailsArray.length != 0) {

        //   for (var n = 0; n < this.srNoMasterSpecifiDetailsArray.length; n++) {
  
        //     this.newDynamic = {
        //       createUomCalculatorDetailsId: this.srNoMasterSpecifiDetailsArray[n][0],
        //       uomCalculatorId: this.srNoMasterSpecifiDetailsArray[n][1],
        //       parameterNumber: this.srNoMasterSpecifiDetailsArray[n][2],
        //       uomFrom: this.srNoMasterSpecifiDetailsArray[n][3],
        //       uomTo: this.srNoMasterSpecifiDetailsArray[n][4],
        //       multiplyBy: this.srNoMasterSpecifiDetailsArray[n][5]
  
        //     };
        //     this.dynamicArrayForUomChecking.push(this.newDynamic);
        //   }
        //   this.dynamicArrayForCheckingUom = this.dynamicArrayForUomChecking;
        // }



        this.srNoMasterSpecifiDetailsArrayByMastIdParaName = data;

        //console.log("The data specification list ");

        this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.splice(0, this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.length);

        for (var p = 0; p < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; p++) {
          this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.push(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[p][2]);
        }

        this.checkIfSrNoAlreadyPresent(i);
      })

    }
  }

  // check if unc specification already present or not
  checkIfSrNoAlreadyPresent(i) {

    if (this.srNoMasterSpecifiDetailsArray.length == 0) {

      this.createCertificateService.createUomCalculatorDetailsFunction(this.createUomCalculatorDetailsObj).subscribe(data => {
        console.log("Uom calculator details : " + data)
        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUomCalculatorDetailsId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;
        this.dynamicArrayT[i].parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        alert("Uom calculator details  saved successfully !");
        this.createCertificateService.updateSrNoUomCalculatorSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
          alert("parameter number updated successfully Unc!")

          this.assignDataToLocalStorage();
        })
      })

    }
    else {

      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; this.w++) {
        if (typeof this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[this.w] != 'undefined' && this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[this.w] != null) {
          if (this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][2] == this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[i] && this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][1] == this.uomCalculatorId) {
            this.createCertificateService.updateCreatedUomCalculatorSpecificationDetails(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][0], this.createUomCalculatorDetailsObj).subscribe(data => {
              console.log("Unc Parameter Master specification details : " + data)
              alert("Unc Parameter Master specification details  updated successfully !");

              this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
              var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUomCalculatorDetailsId;
              this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

              this.assignDataToLocalStorage();
            })
            break;
          }
        }
      }

    }
    if (this.w >= this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length) {

      this.createCertificateService.createUomCalculatorDetailsFunction(this.createUomCalculatorDetailsObj).subscribe(data => {
        console.log("Unc Parameter Master specification details : " + data)
        alert("Unc Parameter Master specification details  saved successfully !");

        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.createUomCalculatorDetailsId;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;
        this.dynamicArrayT[i].parameterNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        this.createCertificateService.updateSrNoUomCalculatorSpecificationDataForParaNumber(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
          alert("parameter number updated successfully For UNC !")

          this.assignDataToLocalStorage();
        })

      })
    }

  }


assignDataToLocalStorage(){

 //  get list of all created uom calculator specifications  
 this.createCertificateService.getCreatedSrNoUomCalculatorSpecificationDetails(this.uomCalculatorId).subscribe(data => {
  console.log("sr no : " + data);
  this.srNoMasterSpecifiDetailsArray = data;

 this.dynamicArrayForCheckingUom=data;


  if (this.srNoMasterSpecifiDetailsArray.length != 0) {

    for (var n = 0; n < this.srNoMasterSpecifiDetailsArray.length; n++) {

      this.newDynamic = {
        createUomCalculatorDetailsId: this.srNoMasterSpecifiDetailsArray[n][0],
        uomCalculatorId: this.srNoMasterSpecifiDetailsArray[n][1],
        parameterNumber: this.srNoMasterSpecifiDetailsArray[n][2],
        uomFrom: this.srNoMasterSpecifiDetailsArray[n][3],
        uomTo: this.srNoMasterSpecifiDetailsArray[n][4],
        multiplyBy: this.srNoMasterSpecifiDetailsArray[n][5]

      };
      this.dynamicArrayForUomChecking.push(this.newDynamic);
    }
    this.dynamicArrayForCheckingUom = this.dynamicArrayForUomChecking;
  }

  localStorage.setItem('uomParaDetails', JSON.stringify(this.dynamicArrayForCheckingUom));

 });









 }

}




