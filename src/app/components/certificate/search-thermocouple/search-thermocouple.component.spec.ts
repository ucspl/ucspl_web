import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchThermocoupleComponent } from './search-thermocouple.component';

describe('SearchThermocoupleComponent', () => {
  let component: SearchThermocoupleComponent;
  let fixture: ComponentFixture<SearchThermocoupleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchThermocoupleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchThermocoupleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
