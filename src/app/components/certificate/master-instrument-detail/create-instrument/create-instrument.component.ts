import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CalibrationLabTypeForMasterInstrument, CreateCertificateService, CreateMasterInstruCalibrationCertificateDetails, CreateMasterInstrumentDetails, InstrumentTypeForMasterInstrument, MasterTypeForMasterInstrument, RangeTypeForMasterInstrument, SearchMasterInstruByMastIdAndStatus, SearchMasterInstruByMIdPNmAndStatus } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { Branch } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';

declare var jQuery: any;

@Component({
  selector: 'app-create-instrument',
  templateUrl: './create-instrument.component.html',
  styleUrls: ['./create-instrument.component.css']
})
export class CreateInstrumentComponent implements OnInit {

  createMasterInstrumentDetailsForm: FormGroup;

  createMasterInstrumentDetailsObj: CreateMasterInstrumentDetails = new CreateMasterInstrumentDetails();
  searchMasterInstruByMIdPNmAndStatus: SearchMasterInstruByMastIdAndStatus = new SearchMasterInstruByMastIdAndStatus();
  calibrationLabTypeForMasterInstrumentObj: CalibrationLabTypeForMasterInstrument = new CalibrationLabTypeForMasterInstrument();
  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];

  instrumentTypeForMasterInstrumentObj: InstrumentTypeForMasterInstrument = new InstrumentTypeForMasterInstrument();
  arrayOfInstrumentType: Array<any> = [];
  arrayOfInstrumentTypeId: Array<any> = [];
  arrayOfInstrumentTypeName: Array<any> = [];

  masterTypeForMasterInstrumentObj: MasterTypeForMasterInstrument = new MasterTypeForMasterInstrument();
  arrayOfMasterType: Array<any> = [];
  arrayOfMasterTypeId: Array<any> = [];
  arrayOfMasterTypeName: Array<any> = [];

  rangeTypeForMasterInstrumentObj: RangeTypeForMasterInstrument = new RangeTypeForMasterInstrument();
  arrayOfRangeType: Array<any> = [];
  arrayOfRangeTypeId: Array<any> = [];
  arrayOfRangeTypeName: Array<any> = [];

  branchObj: Branch = new Branch();
  arrayOfBranches: Array<any> = [];
  arrayOfBrancheId: Array<any> = [];
  arrayOfBranchName: Array<any> = [];

  masterData: any;
  allMasterDetails: any;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  windowRef = null;
  windowRef1 = null;
  f2: FormGroup;

  
  trial1: any;
  trial2: any;
  a: number;
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArray1: Array<CreateMasterInstruCalibrationCertificateDetails> = [];
  mastId1: any;
  newDynamic: any = [{}];

  constructor(private formBuilder: FormBuilder, private router: Router, private createCertificateService: CreateCertificateService, private createUserService: CreateUserService, private datepipe: DatePipe) {

    this.createMasterInstrumentDetailsForm = this.formBuilder.group({

      calibrationLab: [],
      idNo: [],
      personHolding: [],
      masterName: [],
      instrumentType: [],
      calibrationFrequency: [],
      serialNo: [],
      rangeType: [],
      masterType: [],
      purchasedFrom: [],
      make: [],
      dateOfPurchase: [],
      readingOverloading: [],
      model: [],
      status: [],
      dateOfInstallation: [],
      branch: [],

    });

    this.f2 = this.formBuilder.group({
      tName: [''],
    })

  }

  

  Create() {
    console.log(this.createMasterInstrumentDetailsForm.value);
  }


  Create1() {
    console.log(this.createMasterInstrumentDetailsForm.value);
  }

  ngOnInit() {
    (function ($) {
      $(document).ready(function () {
        console.log("Hello from jQuery!");
      });
    })(jQuery);

    // get master type list
    this.createCertificateService.getMasterTypesListForMaster().subscribe(data => {

      this.arrayOfMasterType = data;
      console.log(this.arrayOfMasterType)

      for (var i = 0, l = Object.keys(this.arrayOfMasterType).length; i < l; i++) {
        this.arrayOfMasterTypeName.push(this.arrayOfMasterType[i].masterTypeName);
        this.arrayOfMasterTypeId.push(this.arrayOfMasterType[i].masterTypeId);
      }
    },
      error => console.log(error));



    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType)

      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);
      }
    },
      error => console.log(error));


    // get instrument type list
    this.createCertificateService.getInstrumentTypesListForMaster().subscribe(data => {

      this.arrayOfInstrumentType = data;
      console.log(this.arrayOfInstrumentType)

      for (var i = 0, l = Object.keys(this.arrayOfInstrumentType).length; i < l; i++) {
        this.arrayOfInstrumentTypeName.push(this.arrayOfInstrumentType[i].instruTypeName);
        this.arrayOfInstrumentTypeId.push(this.arrayOfInstrumentType[i].instruTypeId);
      }
    },
      error => console.log(error));


    // get range type list
    this.createCertificateService.getRangeTypesListForMaster().subscribe(data => {

      this.arrayOfRangeType = data;
      console.log(this.arrayOfRangeType)

      for (var i = 0, l = Object.keys(this.arrayOfRangeType).length; i < l; i++) {
        this.arrayOfRangeTypeName.push(this.arrayOfRangeType[i].rangeTypeName);
        this.arrayOfRangeTypeId.push(this.arrayOfRangeType[i].rangeTypeId);
      }
    },
      error => console.log(error));

    // Get Branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.arrayOfBranches = data;

      for (var i = 0, l = Object.keys(this.arrayOfBranches).length; i < l; i++) {
        this.arrayOfBranchName.push(this.arrayOfBranches[i].branchName);
        this.arrayOfBrancheId.push(this.arrayOfBranches[i].branchId);
      }
    },
      error => console.log(error));


    this.newDynamic = [{

      mastInstruCalCertificateDetId: "",
      masterInstruId: "",
      parameterNumber: "",
      parameterId: "",
      calCertificate: "",
      calCertificateAgencyId: "",
      calibrationDate: "",
      calibrationDue: "",
      dateOfOutward: "",
      dateOfInward: "",
      calibrationCharges: "",
      calibrationData: "",
      attachment: "",
      draft: "",
      approved: "",
      archieved: "",
      submitted: "",
      rejected: "",

    }]

  }

  //Save data
  saveData() {
    this.createMasterInstrumentDetailsObj.idNo = this.createMasterInstrumentDetailsForm.value.idNo;
    this.createMasterInstrumentDetailsObj.masterName = this.createMasterInstrumentDetailsForm.value.masterName;
    this.createMasterInstrumentDetailsObj.masterTypeId = this.createMasterInstrumentDetailsForm.value.masterType;
    this.createMasterInstrumentDetailsObj.instrumentTypeId = this.createMasterInstrumentDetailsForm.value.instrumentType;
    this.createMasterInstrumentDetailsObj.rangeTypeId = this.createMasterInstrumentDetailsForm.value.rangeType;
    this.createMasterInstrumentDetailsObj.srNo = this.createMasterInstrumentDetailsForm.value.serialNo;
    this.createMasterInstrumentDetailsObj.model = this.createMasterInstrumentDetailsForm.value.model;
    this.createMasterInstrumentDetailsObj.branchId = this.createMasterInstrumentDetailsForm.value.branch;
    this.createMasterInstrumentDetailsObj.calibrationLabId = this.createMasterInstrumentDetailsForm.value.calibrationLab;
    this.createMasterInstrumentDetailsObj.calibrationFrequency = this.createMasterInstrumentDetailsForm.value.calibrationFrequency;
    this.createMasterInstrumentDetailsObj.make = this.createMasterInstrumentDetailsForm.value.make;
    this.createMasterInstrumentDetailsObj.personHolding = this.createMasterInstrumentDetailsForm.value.personHolding;
    this.createMasterInstrumentDetailsObj.readingOverloading = this.createMasterInstrumentDetailsForm.value.readingOverloading;
    this.createMasterInstrumentDetailsObj.purchasedFrom = this.createMasterInstrumentDetailsForm.value.purchasedFrom;

    this.createMasterInstrumentDetailsObj.dateOfPurchase = this.createMasterInstrumentDetailsForm.value.dateOfPurchase;
    var dtOfPurchase = this.convertStringToDate(this.createMasterInstrumentDetailsObj.dateOfPurchase);
    this.createMasterInstrumentDetailsObj.dateOfPurchase = this.datepipe.transform(dtOfPurchase, 'dd-MM-yyyy');

    this.createMasterInstrumentDetailsObj.dateOfInstallation = this.createMasterInstrumentDetailsForm.value.dateOfInstallation;
    var dtOfInstallation = this.convertStringToDate(this.createMasterInstrumentDetailsObj.dateOfInstallation);
    this.createMasterInstrumentDetailsObj.dateOfInstallation = this.datepipe.transform(dtOfInstallation, 'dd-MM-yyyy');

    switch (this.createMasterInstrumentDetailsForm.value.status) {
      case 'Draft': this.createMasterInstrumentDetailsObj.draft = 1;
        this.createMasterInstrumentDetailsObj.archieved = 0;
        this.createMasterInstrumentDetailsObj.rejected = 0;
        this.createMasterInstrumentDetailsObj.approved = 0;
        this.createMasterInstrumentDetailsObj.submitted = 0;
        break;
      case 'Archieved': this.createMasterInstrumentDetailsObj.draft = 0
        this.createMasterInstrumentDetailsObj.archieved = 1;
        this.createMasterInstrumentDetailsObj.rejected = 0;
        this.createMasterInstrumentDetailsObj.approved = 0;
        this.createMasterInstrumentDetailsObj.submitted = 0;
        break;
      case 'Rejected': this.createMasterInstrumentDetailsObj.draft = 0;
        this.createMasterInstrumentDetailsObj.archieved = 0;
        this.createMasterInstrumentDetailsObj.rejected = 1;
        this.createMasterInstrumentDetailsObj.approved = 0;
        this.createMasterInstrumentDetailsObj.submitted = 0;
        break;
      case 'Approved': this.createMasterInstrumentDetailsObj.draft = 0;
        this.createMasterInstrumentDetailsObj.archieved = 0;
        this.createMasterInstrumentDetailsObj.rejected = 0;
        this.createMasterInstrumentDetailsObj.approved = 1;
        this.createMasterInstrumentDetailsObj.submitted = 0;
        break;
      case 'Submitted': this.createMasterInstrumentDetailsObj.draft = 0;
        this.createMasterInstrumentDetailsObj.archieved = 0;
        this.createMasterInstrumentDetailsObj.rejected = 0;
        this.createMasterInstrumentDetailsObj.approved = 0;
        this.createMasterInstrumentDetailsObj.submitted = 1;
        break;
    }

    for (var i = 0, l = Object.keys(this.arrayOfBranches).length; i < l; i++) {
      if (this.arrayOfBranches[i].branchName == this.createMasterInstrumentDetailsForm.value.branch) {
        this.createMasterInstrumentDetailsObj.branchId = this.arrayOfBranches[i].branchId;
      }
    }

    for (var i = 0, l = Object.keys(this.arrayOfMasterType).length; i < l; i++) {
      if (this.arrayOfMasterType[i].masterTypeName == this.createMasterInstrumentDetailsForm.value.masterType) {
        this.createMasterInstrumentDetailsObj.masterTypeId = this.arrayOfMasterType[i].masterTypeId;
      }
    }

    for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
      if (this.arrayOfCalibrationLabType[i].calibrationLabTypeName == this.createMasterInstrumentDetailsForm.value.calibrationLab) {
        this.createMasterInstrumentDetailsObj.calibrationLabId = this.arrayOfCalibrationLabType[i].calibrationLabTypeId;
      }
    }

    for (var i = 0, l = Object.keys(this.arrayOfInstrumentType).length; i < l; i++) {
      if (this.arrayOfInstrumentType[i].instruTypeName == this.createMasterInstrumentDetailsForm.value.instrumentType) {
        this.createMasterInstrumentDetailsObj.instrumentTypeId = this.arrayOfInstrumentType[i].instruTypeId;
      }
    }

    for (var i = 0, l = Object.keys(this.arrayOfRangeType).length; i < l; i++) {
      if (this.arrayOfRangeType[i].rangeTypeName == this.createMasterInstrumentDetailsForm.value.rangeType) {
        this.createMasterInstrumentDetailsObj.rangeTypeId = this.arrayOfRangeType[i].rangeTypeId;
      }
    }

    console.log("Create Master Instrument Details Obj" + this.createMasterInstrumentDetailsObj);

    this.createCertificateService.getMasterInstrumentDetails().subscribe(data => {
      this.allMasterDetails = data;

      console.log("allMasterDetails : " + this.allMasterDetails)

      for (this.a = 0; this.a < Object.keys(this.allMasterDetails).length; this.a++) {
        if ((this.allMasterDetails[this.a].idNo == this.createMasterInstrumentDetailsObj.idNo || this.allMasterDetails[this.a].srNo == this.createMasterInstrumentDetailsObj.srNo)) {
          alert("Master already present !!");
          break;
        }
      }
      if (this.a >= Object.keys(this.allMasterDetails).length) {
        this.createCertificateService.createMasterInstrumentDetail(this.createMasterInstrumentDetailsObj).subscribe(data => {
          console.log(data);
          this.masterData = data;
          alert("Master details saved successfully!!")
          localStorage.setItem('createMasterInstruId', JSON.stringify(this.masterData.id));


        })
      }
    })
  }

  convertStringToDate(value) {

    if (typeof value === 'string') {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);

      return dateFormat;
    }
    else {
      return value;
    }
  }


  openChildWindow() {
    var url = '/crtSpeDet';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receivemessage.bind(this), false);
  }
  receivemessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }

  openChildWindow1() {

    this.srNoMasterSpecifiDetailsArray1.splice(0,this.srNoMasterSpecifiDetailsArray1.length);
    this.newDynamic.splice(0,this.newDynamic.length);

    this.mastId1 = JSON.parse(localStorage.getItem('createMasterInstruId'));

    this.searchMasterInstruByMIdPNmAndStatus.mastId = this.mastId1;
    this.searchMasterInstruByMIdPNmAndStatus.draft = 1;
    this.searchMasterInstruByMIdPNmAndStatus.approved = 0;
    this.searchMasterInstruByMIdPNmAndStatus.archieved = 0;
    this.searchMasterInstruByMIdPNmAndStatus.rejected = 0;
    this.searchMasterInstruByMIdPNmAndStatus.submitted = 0;

    this.createCertificateService.getSrNoOfMasterInstrumentCalCertificateDetailByStatus(this.searchMasterInstruByMIdPNmAndStatus).subscribe(data => {
      console.log(data)
      this.srNoMasterSpecifiDetailsArray = data;

      for(let i=0;i<this.srNoMasterSpecifiDetailsArray.length;i++){

        this.newDynamic.push({mastInstruCalCertificateDetId:this.srNoMasterSpecifiDetailsArray[i][0],
          masterInstruId: this.srNoMasterSpecifiDetailsArray[i][1],
          calCertificate: this.srNoMasterSpecifiDetailsArray[i][2],
          calCertificateAgencyId: this.srNoMasterSpecifiDetailsArray[i][3],
          calibrationDate: this.srNoMasterSpecifiDetailsArray[i][4],
          calibrationDue: this.srNoMasterSpecifiDetailsArray[i][5],
          dateOfOutward:this.srNoMasterSpecifiDetailsArray[i][6],
          dateOfInward: this.srNoMasterSpecifiDetailsArray[i][7],
          calibrationCharges: this.srNoMasterSpecifiDetailsArray[i][8],
          calibrationData: this.srNoMasterSpecifiDetailsArray[i][9]
        
        })
      }

      for(var l=0;l<this.newDynamic.length;l++)
      { 
       this.srNoMasterSpecifiDetailsArray1.push(this.newDynamic[l]);
      }
    localStorage.setItem('mastCalCerti', JSON.stringify(this.srNoMasterSpecifiDetailsArray1))
    var url = '/crtMCalCerDet';
    this.windowRef1 = window.open(url, "child1", "width=1030,height=420,top=100");
    this.windowRef1.focus();
    this.windowRef1.addEventListener("message1", this.receivemessage1.bind(this), false);
     })

  }
  receivemessage1(evt: any) {
    console.log(evt.data);
    this.trial2 = evt.data;
  }

}
