import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { SearchCertificateService, SearchMasterInstrument } from '../../../../services/serviceconnection/search-service/search-certificate.service';

@Component({
  selector: 'app-search-instrument',
  templateUrl: './search-instrument.component.html',
  styleUrls: ['./search-instrument.component.css']
})
export class SearchInstrumentComponent implements OnInit {

  searchMasterForm:FormGroup;
  searchReqAndIwdByDocument: SearchMasterInstrument = new SearchMasterInstrument();

  data:any;
  masterInstruArray: Array<any> = [];

  p: number = 1;
  itemsPerPage: number = 5;

  public dateValue1 = new Date();
  public dateValue2 = new Date();
  docNo: any;
  idForSearchSales: any;

  constructor(private http:HttpClient,private router:Router,private formBuilder: FormBuilder ,private searchCertificateService:SearchCertificateService,private createCertificateService: CreateCertificateService) { 

    this.searchMasterForm=formBuilder.group({
      searchDocument:[],
      itemsPerPage:[]
    })
  }

  ngOnInit() {
 
  }

  searchData() {
    debugger;
    this.searchReqAndIwdByDocument.searchDocument = this.searchMasterForm.value.searchDocument;
    
    if(this.searchReqAndIwdByDocument.searchDocument==null){
      this.searchReqAndIwdByDocument.searchDocument=''
    }
    else{
      this.searchReqAndIwdByDocument.searchDocument=this.searchReqAndIwdByDocument.searchDocument.trim();
    }

      this.searchCertificateService.searchData(this.searchReqAndIwdByDocument).subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
        this.masterInstruArray = this.data;
      })
     
  }



  moveToMIUpdateScreen(i){

    i=((this.itemsPerPage * (this.p-1))+(i));
    console.log("index will be...."+i);
  
    this.docNo=parseInt(i);
  
    this.searchCertificateService.setDocNoForMasterInstruDocScreen(this.masterInstruArray[this.docNo][0]);
    this.idForSearchSales = this.masterInstruArray[this.docNo][0];
    localStorage.setItem('updMasterInstruId', JSON.stringify(this.idForSearchSales));
  
    this.router.navigateByUrl('/nav/updateinstru');
  }

}
