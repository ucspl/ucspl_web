import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSpecificationDetailsComponent } from './update-specification-details.component';

describe('UpdateSpecificationDetailsComponent', () => {
  let component: UpdateSpecificationDetailsComponent;
  let fixture: ComponentFixture<UpdateSpecificationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSpecificationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSpecificationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
