import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCalibrationResultDataForMasterInstrumentComponent } from './create-calibration-result-data-for-master-instrument.component';

describe('CreateCalibrationResultDataForMasterInstrumentComponent', () => {
  let component: CreateCalibrationResultDataForMasterInstrumentComponent;
  let fixture: ComponentFixture<CreateCalibrationResultDataForMasterInstrumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCalibrationResultDataForMasterInstrumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCalibrationResultDataForMasterInstrumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
