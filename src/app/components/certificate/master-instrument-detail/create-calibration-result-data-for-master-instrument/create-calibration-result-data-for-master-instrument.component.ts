
import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCalibrationResultDataForMasterInstru, CreateCertificateService, CreateMasterInstrumentSpecificationDetails, ModeTypeForMasterInstrument, SearchMasterInstruByMastIdAndParameterId, SearchMasterInstruByMastIdAndStatus, SearchMasterInstruByMIdPNmAndStatus, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateNablScopeService } from '../../../../services/serviceconnection/create-service/create-nabl-scope.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

declare var $: any;

@Component({
  selector: 'app-create-calibration-result-data-for-master-instrument',
  templateUrl: './create-calibration-result-data-for-master-instrument.component.html',
  styleUrls: ['./create-calibration-result-data-for-master-instrument.component.css']
})
export class CreateCalibrationResultDataForMasterInstrumentComponent implements OnInit {

  public createMasterSpecificationForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 30;

  formGroup = new FormArray([
    new FormArray([new FormControl(true), new FormControl(false)])
  ]);

  searchMasterInstruByMIdPNmAndStatusObj: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();
  searchMasterInstruByMastIdAndParameterId: SearchMasterInstruByMastIdAndParameterId = new SearchMasterInstruByMastIdAndParameterId();



  createMasterInstrumentSpecificationObj: CreateMasterInstrumentSpecificationDetails = new CreateMasterInstrumentSpecificationDetails();
  dynamicArray: Array<CreateMasterInstrumentSpecificationDetails> = [];

  createCalibrationResultDataForMasterInstru: CreateCalibrationResultDataForMasterInstru = new CreateCalibrationResultDataForMasterInstru();
  createCalibrationResultDataForMasterInstru1: CreateCalibrationResultDataForMasterInstru = new CreateCalibrationResultDataForMasterInstru();
  createMasterInstrumentSpecificationObjForUniqueSrNoChange: CreateCalibrationResultDataForMasterInstru = new CreateCalibrationResultDataForMasterInstru();

  srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate: Array<any> = [];

  searchMasterInstruByMastIdAndStatusObj: SearchMasterInstruByMastIdAndStatus = new SearchMasterInstruByMastIdAndStatus();
  searchMasterInstruByMIdPNmAndStatusObj2: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();
  searchMasterInstruByMIdPNmAndStatusObj3: SearchMasterInstruByMastIdAndStatus = new SearchMasterInstruByMastIdAndStatus();


  modeTypeForMasterInstrumentObj: ModeTypeForMasterInstrument = new ModeTypeForMasterInstrument();
  arrayModeType: Array<any> = [];
  arrayModeTypeListId: Array<any> = [];
  arrayModeTypeListName: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracy: Array<any> = [];
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];
  arrayOfParameterFrequency: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  tempChartTypeForMasterInstrumentObj: TempChartTypeForMasterInstrument = new TempChartTypeForMasterInstrument();
  arrayOfTempChartType: Array<any> = [];
  arrayTempChartTypeId: Array<any> = [];
  arrayTempChartTypeNames: Array<any> = [];

  srNoCalibrationResultDataDetailsArrayForUniqueNoChange: any;

  //FrequencyUom
  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames: Array<any> = [];
  arrayOfFrequencyUomId: Array<any> = [];


  dynamicArrayTrial: Array<any> = [];
  srNoCalResultDetailsArray: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm2: Array<any> = [];


  arrayGroupNameAccorToLab1: Array<any> = [];
  arrayUncFormulaList: Array<any> = [];
  arrayOfUncFormulaNames: Array<any> = [];
  arrayOfUncFormulaId: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  showFreq: any;
  freq: any;
  previousStatus: any;
  parametersAvailable: Array<any> = [];
  approvedParametersAvailable: Array<any> = [];
  paraId: any;
  calCertificateResultData: any;

  newArray: Array<any> = []
  dynamicArrayT: Array<any> = []
  dynamicParaNames: Array<any> = []
  dynamicAT: Array<any> = []
  dynamicAT1: Array<any> = []
  dynamicAT2: Array<any> = []
  dynamicAT3: Array<any> = []
  dynamicAT4: Array<any> = []
  dynamicAT5: Array<any> = []
  dynamicArrayT1: Array<any> = []
  newDynamic: any = [{}];
  newDynamic1: any = [{}];
  newDynamic2: any = [{}];
  trialA: Array<any> = [];
  trialA1: any;

  retrieveResponse1: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaName: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaName1: any;
  arrayOfCalDataResultForSelectedPara: Array<any> = [];

  countForFunctionCall = 0;

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService, private createNablScopeService: CreateNablScopeService) {

  }


  ngAfterViewInit() {
    // this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    // for (var l = 0; l < this.leng; l++) {

    //   if (this.dynamicArrayTrial != null) {
    //     if (this.dynamicArrayTrial[l].parameterId != 0 && this.dynamicArrayTrial[l].parameterId != "") {   //if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
    //       document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
    //     }
    //     else {
    //       document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
    //     }
    //   }
    //   else {
    //     document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
    //   }

    //   (<HTMLInputElement>document.getElementById("selectTag-" + l)).disabled = false;
    //   (<HTMLInputElement>document.getElementById("selectTag1-" + l)).disabled = false;

    // }

  }

  accuracyValue(pp, ppp) {
    this.forAccuValue = this.dynamicAT[pp][ppp].uncFormula;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "" && this.forAccuValue != "--Select--") {

      (<HTMLInputElement>document.getElementById("selectTag-" + pp + ppp)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag-" + pp + ppp)).disabled = false;
    }

  }

  accuracyValue1(pp, ppp) {
    this.forAccuValue = this.dynamicAT[pp][ppp].uncUom;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "" && this.forAccuValue != "--Select--") {

      (<HTMLInputElement>document.getElementById("selectTag1-" + pp + ppp)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag1-" + pp + ppp)).disabled = false;
    }
  }



  findErrorField(pp,ppp){

    var err= this.dynamicAT1[pp][ppp].stdReading - this.dynamicAT1[pp][ppp].uucReading;
    this.dynamicAT1[pp][ppp].error=err;
    
  }



  onChange() {

    // this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    var arrayUomNameAccor = new Array();

    for (var ii = 0, ll = Object.keys(this.ParameterListForReqAndIwdObj).length; ii < ll; ii++) {
      if (this.selectParameter == this.arrayParameterNames[ii]) {

        this.freq = this.arrayOfParameterFrequency[ii];
        if (this.freq == 1) {
          this.showFreq = 1;
        }
        else {
          this.showFreq = 0;
        }

        for (var iii = 0, lll = Object.keys(this.uomListForRequestAndInwardObj).length; iii < lll; iii++) {
          if (this.uomListForRequestAndInwardObj[iii].parameterId == this.arrayParameterId[ii]) {
            arrayUomNameAccor.push(this.uomListForRequestAndInwardObj[iii].uomName)
          }
        }
      }
    }

    this.arrayUomNameAccorToParam1 = arrayUomNameAccor;
    console.log("this.arrayUomNameAccorToParam1 " + this.arrayUomNameAccorToParam1)
    localStorage.setItem('arrayUomNameAccorToParam1ForMastInstru', JSON.stringify(this.arrayUomNameAccorToParam1));

    // for (let x = 0; x < this.leng; x++) {
    //   ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(x) as FormGroup).get('parameterId').patchValue(this.selectParameter);
    // }

    this.parameterSpecifiDisplay();

    // for(var e1=1;e1<3;e1++){
    //   this.dynamicArrayT1.push(this.newDynamic);
    // }

    // this.newDynamic.splice(0,this.newDynamic.length);
    // this.newDynamic1.splice(0,this.newDynamic1.length);

    // this.newDynamic1.push({nameofparameter:"pressure"})      
    // this.newDynamic.push({id: e1 ,name:"name1 of newDynamic"})      

    // this.dynamicAT.push(this.newDynamic1)
    // this.dynamicAT.push(this.newDynamic)

    // this.dynamicAT1.push(this.dynamicAT)

    // for (var e = 0; e < 2; e++) {
    //   this.dynamicArrayT.push(this.dynamicAT1);
    // }

    // for(var e1 = 0; e1 < 2; e1++){
    //   this.dynamicArrayT1[e1]=this.dynamicArrayT[e1];
    // }

  }

  deleteRow(pp, ppp) {
    console.log("deleterow");

    var valueToDelete = this.dynamicAT1[pp][ppp].id;  
    console.log("value to delete is :" + valueToDelete);  
    // this.dynamicAT1[pp].splice(ppp, 1);                                                     

    this.createCertificateService.deleteCalCertificateResultDataForSelectedParameter(valueToDelete).subscribe(data => {
      console.log(data);
      this.dynamicAT1[pp].splice(ppp, 1);
    })

  }


  addReading(pp) {

    //  this.trialA = this.dynamicAT[pp].filter(function (f) { return f; })
    //  this.trialA.push(this.newDynamic);
    //  this.trialA.push(this.newDynamic);

    this.dynamicAT[pp].push(this.newDynamic);
    this.dynamicAT1[pp].push(this.newDynamic2);

    this.dynamicAT1 = $.extend(true, [], this.dynamicAT1);
    //  this.dynamicAT.shift().shift();

    console.log("this.dynamicAT : " + this.dynamicAT)
  }


  parameterSpecifiDisplay() {
    console.log("hii")
    this.dynamicAT.splice(0, this.dynamicAT.length);
    this.dynamicParaNames.splice(0, this.dynamicParaNames.length);

    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
      if (this.selectParameter == this.arrayParameterNames[k]) {
        this.paraId = this.arrayParameterId[k];
        break;
      }
    }

    this.searchMasterInstruByMastIdAndParameterId.createMasterInstruId = this.masterId;
    this.searchMasterInstruByMastIdAndParameterId.parameterId = this.paraId;

    this.status = "Approved"
    var msIdParaName1 = this.masterId + "_" + this.selectParameter + "_" + this.status;
    this.searchMasterInstruByMIdPNmAndStatusObj.msIdParaName = msIdParaName1;

    this.searchMasterInstruByMIdPNmAndStatusObj.draft = 0;
    this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 0;
    this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 0;
    this.searchMasterInstruByMIdPNmAndStatusObj.approved = 1;
    this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 0;

    //this.createCertificateService.getSrNoOfMasterInstruSpecifiDetailByMastIdParaName(this.searchMasterInstruByMastIdAndParameterId).subscribe(data => {
    this.createCertificateService.getSrNoOfMasterInstruSpecifiDetailByMastIdParaName(this.searchMasterInstruByMIdPNmAndStatusObj).subscribe(data => {

      console.log(data);
      this.calCertificateResultData = data;

      console.log("this.calCertificateResultData  : " + data);

      for (var e1 = 1; e1 < 3; e1++) {
        this.dynamicArrayT1.push(this.newDynamic);
      }

      this.newDynamic.splice(0, this.newDynamic.length);
      this.newDynamic1.splice(0, this.newDynamic1.length);
      this.newArray.splice(0, this.newArray.length);
      this.dynamicAT3.splice(0, this.dynamicAT3.length);
      this.dynamicAT1.splice(0, this.dynamicAT1.length);
      this.countForFunctionCall=0;                                            // uncomment this line

      //this.newDynamic1.push({ nameofparameter : "pressure" });

      for (var n = 0; n < this.calCertificateResultData.length; n++) {
        this.newDynamic1.push({
          nameofparameter: this.calCertificateResultData[n][2],
          createMasterInstruId: this.calCertificateResultData[n][1],
          masterSpecificationId: this.calCertificateResultData[n][0],
          parameterId: this.calCertificateResultData[n][2],
          parameterNumber: this.calCertificateResultData[n][3],
          masterIdParaName: this.calCertificateResultData[n][26],
          rangeFrom: this.calCertificateResultData[n][4],
          rangeTo: this.calCertificateResultData[n][5],
          freqFrom: this.calCertificateResultData[n][7],
          freqTo: this.calCertificateResultData[n][9],
          tempchartType: this.calCertificateResultData[n][19],
          mode: this.calCertificateResultData[n][20]
        })
      }


      //below trial code delete if error occurs/////////////////////////////////////////////


      for (var e = 0; e < this.calCertificateResultData.length; e++) {
        this.dynamicParaNames.push(this.newDynamic1[e]);
      }

      this.newArray.splice(0, this.newArray.length);

      for (var e = 0; e < this.calCertificateResultData.length; e++) {

        // this.newArray.push(this.newDynamic1[e])
        //  this.dynamicAT1.push(this.newDynamic1[e])
      }

      for (var i = 0; i < this.calCertificateResultData.length; i++) {


        this.searchMasterInstruByMIdPNmAndStatusObj3.mastId = this.newDynamic1[i].masterSpecificationId;
        var j = i;
        this.newArray.push(this.newDynamic)
        this.getParameterSpecificationData(j, this.searchMasterInstruByMIdPNmAndStatusObj3);

      }

      //above trial code delete if error occurs/////////////////////////////////////////////

      this.retrieveResponse1 = this.newDynamic1.filter(function (f) { return f; })

      // this.newDynamic.push({
      //   id: e1,
      //   name: "name1 of newDynamic",
      //   stdReading: "",
      //   unc: "",
      //   uucReading: "",
      //   uncFormula: "",
      //   uncUom: "",
      //   acFrequency: "",
      //   acFrequencyUom: ""
      // })


      // this.dynamicAT3.push(this.newDynamic);

      // for (var e = 0; e < this.calCertificateResultData.length; e++) {


      //   this.dynamicParaNames.push(this.newDynamic1[e]);
      //   this.newArray.push(this.dynamicAT3)
      //   // commented three lines
      //   //  this.newArray.push(this.newDynamic)                         // uncomment this   
      //   //  this.dynamicAT.push(this.newDynamic);                       // comment this 
      //   //  this.trialArray();
      // }


      // var arr = [['abc'], ['xyz']];                                     // uncomment this   
      // this.dynamicAT = $.extend(true, [], this.newArray);

      // console.log(arr);
      console.log(" this.newArray : " + this.dynamicAT);
      console.log(" this.dynamicAT1 : " + this.dynamicAT1);


      //not used below code 

      // for (var e = 0; e < this.calCertificateResultData.length; e++) {      
      //   this.dynamicArrayT.push(this.newDynamic1);
      //  }

      // for (var e = 0; e < this.calCertificateResultData.length; e++) {
      //  // this.dynamicArrayT.push(this.dynamicAT1);
      //  // this.dynamicArrayT.push(this.newDynamic1);
      //  this.dynamicArrayT[e].push(this.retrieveResponse1[e]);
      // }

      // for (var e1 = 0; e1 < this.calCertificateResultData.length; e1++) {
      //   this.dynamicArrayT1[e1] = this.retrieveResponse1[e1];
      // }

      // for (var e = 0; e < this.calCertificateResultData.length; e++) {
      //   this.dynamicArrayT[e].push("hiii");
      //  }

    })

  }

  trialArray() {
    this.dynamicAT2 = this.dynamicAT.filter(function (f) { return f; })
    this.dynamicAT1.push(this.dynamicAT2)

    this.dynamicAT.splice(0, this.dynamicAT.length);

  }

  trialfun(pp, ppp) {
    console.log("pp : " + pp + " ppp : " + ppp);
    console.log(this.dynamicAT);

  }

  getParameterSpecificationData(j, value) {

    this.createCertificateService.getSrNoOfMISpecifiByMastIdParaNameForCalResultData(value).subscribe(data => {

     
      console.log(data);
      this.srNoMasterSpecifiDetailsArrayByMastIdParaName1 = data;
      this.arrayOfCalDataResultForSelectedPara.push(this.srNoMasterSpecifiDetailsArrayByMastIdParaName1);

      this.newDynamic.splice(0, this.newDynamic.length);
      this.dynamicAT3.splice(0, this.dynamicAT3.length);
      

      for (var ii = 0; ii < this.srNoMasterSpecifiDetailsArrayByMastIdParaName1.length; ii++) {
        this.newDynamic.push({
          id: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][0],
          name: "name1 of newDynamic",
          stdReading: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][6],
          uucReading: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][7],
          error: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][8],
          unc: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][9],
          uncFormula: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][11],
          uncUom: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][10],
          acFrequency: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][14],
          acFrequencyUom: this.srNoMasterSpecifiDetailsArrayByMastIdParaName1[ii][12],
        })
      }

      // 
      this.dynamicAT3.push(this.newDynamic);
      var trialDynamicAT3 = $.extend(true, [], this.dynamicAT3);

      // for (var e = 0; e < this.srNoMasterSpecifiDetailsArrayByMastIdParaName1.length; e++) {
      this.newArray[j] = trialDynamicAT3;
      // }

      this.dynamicAT = $.extend(true, [], this.newArray);

      var count = this.countForFunctionCall;

      if (typeof this.dynamicAT[count] != 'undefined' && typeof this.dynamicAT[count][0] != 'undefined' && this.dynamicAT[count][0].length != 0) {

        this.dynamicAT1.push(this.dynamicAT[count][0]);

        for (var r = 0; r < this.dynamicAT[count][0]; r++) {
          this.dynamicAT1[count].push(this.dynamicAT[r][0])
        }

      }
      if (typeof this.dynamicAT[count] != 'undefined') {
        if (this.dynamicAT[count][0] != 'undefined') {
          if (this.dynamicAT[count][0].length == 0) {
            this.dynamicAT1.push(this.newDynamic2);
          }
        }
      }
      if (typeof this.dynamicAT[count] == 'undefined') {

        this.dynamicAT1.push(this.newDynamic2);

      }
      this.countForFunctionCall++;

      this.dynamicAT1 = $.extend(true, [], this.dynamicAT1);
      console.log(" this.dynamicAT1 : " + this.dynamicAT1);

    })

    // this.dynamicAT1[j].push(this.dynamicAT[j][0]);
    // var count1=this.countForFunctionCall;

    // if( count1> this.dynamicAT1.length && count1<  this.calCertificateResultData.length){
    //   var diff= this.calCertificateResultData.length-count1;
    //   for(var rr=0;rr<diff;rr++){
    //     this.dynamicAT1.push(this.newDynamic);
    //   }
    // }

  }

  saveData(pp, ppp) {
    this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.splice(0, this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.length);

    //do code here with pp operator overloading using their passed parameters                    
    this.createCalibrationResultDataForMasterInstru.createMasterInstruId = this.masterId; 
    this.createCalibrationResultDataForMasterInstru.masterIdParaName = this.retrieveResponse1[pp].masterIdParaName;   
    this.createCalibrationResultDataForMasterInstru.parameterId = this.retrieveResponse1[pp].parameterId;
    this.createCalibrationResultDataForMasterInstru.parameterNumber = this.retrieveResponse1[pp].parameterNumber;                                
    this.createCalibrationResultDataForMasterInstru.masterSpecificationId = this.retrieveResponse1[pp].masterSpecificationId;
    this.createCalibrationResultDataForMasterInstru.stdReading = this.dynamicAT1[pp][ppp].stdReading;                                            
    this.createCalibrationResultDataForMasterInstru.uucReading = this.dynamicAT1[pp][ppp].uucReading;
    this.createCalibrationResultDataForMasterInstru.error = this.dynamicAT1[pp][ppp].error;
    this.createCalibrationResultDataForMasterInstru.unc = this.dynamicAT1[pp][ppp].unc;
    this.createCalibrationResultDataForMasterInstru.uncFormula = this.dynamicAT1[pp][ppp].uncFormula;
    this.createCalibrationResultDataForMasterInstru.uncUom = this.dynamicAT1[pp][ppp].uncUom;
    this.createCalibrationResultDataForMasterInstru.acFrequency = this.dynamicAT1[pp][ppp].acFrequency;
    this.createCalibrationResultDataForMasterInstru.acFrequencyUom = this.dynamicAT1[pp][ppp].acFrequencyUom;       

    //save parameter 
    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
      if (this.selectParameter == this.arrayParameterNames[k]) {
        this.createCalibrationResultDataForMasterInstru.parameterId = this.arrayParameterId[k];
        break;
      }
      else {
        this.createCalibrationResultDataForMasterInstru.parameterId = 1;
      }
    }

    // save ac frequency  uom
    for (var k = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; k < l; k++) {
      if (this.dynamicAT1[pp][ppp].acFrequencyUom == this.arrayOfFrequencyUomNames[k]) {
        this.createCalibrationResultDataForMasterInstru.acFrequencyUom = this.arrayOfFrequencyUomId[k];
        break;
      }
      else {
        this.createCalibrationResultDataForMasterInstru.acFrequencyUom = 1;
      }
    }

    // save  unc formula
    for (var k = 0, l = Object.keys(this.arrayUncFormulaList).length; k < l; k++) {
      if (this.dynamicAT1[pp][ppp].uncFormula == this.arrayOfUncFormulaNames[k]) {
        this.createCalibrationResultDataForMasterInstru.uncFormula = this.arrayOfUncFormulaId[k];
        break;
      }
      else {
        this.createCalibrationResultDataForMasterInstru.uncFormula = 1;
      }
    }

    // save unc uom
    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicAT1[pp][ppp].uncUom == this.arrayUomName[k]) { 
        this.createCalibrationResultDataForMasterInstru.uncUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createCalibrationResultDataForMasterInstru.uncUom = 1;
      }
    }

    // this.checkIfSrNoAlreadyPresent(pp, ppp);

    // this.createCertificateService.createCalCertificateResultDataForSelectedParameter(this.createCalibrationResultDataForMasterInstru).subscribe(data => {
    //   console.log(data);
    //   this.srNoCalResultDetailsArray = data;

    // })

    this.searchMasterInstruByMIdPNmAndStatusObj3.mastId = this.retrieveResponse1[pp].masterSpecificationId;

    this.createCertificateService.getSrNoOfMISpecifiByMastIdParaNameForCalResultData(this.searchMasterInstruByMIdPNmAndStatusObj3).subscribe(data => {
      console.log(data);
      this.srNoMasterSpecifiDetailsArrayByMastIdParaName = data;

      for (var p = 0; p < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; p++) {
        this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate.push(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[p][5]);
      }

      this.checkIfSrNoAlreadyPresent(pp, ppp);

    })

  }
                                                                                                              
  checkIfSrNoAlreadyPresent(e, ppp) {                                                                      

    if (this.srNoCalResultDetailsArray.length == 0) {
      this.createCertificateService.createCalCertificateResultDataForSelectedParameter(this.createCalibrationResultDataForMasterInstru).subscribe(data => {
        console.log("Master specification details : " + data)
        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.id;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.uniqueSrNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        alert("unique number updated successfully successfully !");
        this.createCertificateService.updateCalCertificateResultDataForUniqueSrNo(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
        })
      })
    }
    else {
      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length; this.w++) {
        if (typeof this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[ppp] != 'undefined') {
          if (this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][1] == this.retrieveResponse1[e].masterSpecificationId && this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][5] == this.srNoMasterSpecifiDetailsArrayForParaNoCheckForUpdate[ppp]) {
            this.createCertificateService.updateCalCertificateResultDataForSelectedParameter(this.srNoMasterSpecifiDetailsArrayByMastIdParaName[this.w][0], this.createCalibrationResultDataForMasterInstru).subscribe(data => {
              console.log("Master specification details : " + data)
              alert("data updated successfully !")

              this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
              var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.id;
              this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.uniqueSrNumber = this.createMasterInstrumentSpecificationObj.masterIdParaName = this.selectParameter + "_" + idToChangeUniqueSrNo;

              alert("unique number updated successfully successfully !")
              this.createCertificateService.updateCalCertificateResultDataForUniqueSrNo(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
                console.log(data1);
              })

            })
            break;
          }
        }
      }
    }
    if (this.w >= this.srNoMasterSpecifiDetailsArrayByMastIdParaName.length) {
      this.createCertificateService.createCalCertificateResultDataForSelectedParameter(this.createCalibrationResultDataForMasterInstru).subscribe(data => {
        console.log("Master specification details : " + data)
        alert("data saved successfully !")

        this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange = data;
        var idToChangeUniqueSrNo = this.srNoCalibrationResultDataDetailsArrayForUniqueNoChange.id;
        this.createMasterInstrumentSpecificationObjForUniqueSrNoChange.uniqueSrNumber = this.selectParameter + "_" + idToChangeUniqueSrNo;

        alert("unique number updated successfully successfully !")
        this.createCertificateService.updateCalCertificateResultDataForUniqueSrNo(idToChangeUniqueSrNo, this.createMasterInstrumentSpecificationObjForUniqueSrNoChange).subscribe(data1 => {
          console.log(data1);
        })

      })
    }
    // this.dynamicArrayTrial = this.dynamicArray;        
  }


  ngOnInit() {

    this.masterId = JSON.parse(localStorage.getItem('createMasterInstruId'));
    this.masterId = parseInt(this.masterId);

    this.searchMasterInstruByMastIdAndStatusObj.approved = 1;
    this.searchMasterInstruByMastIdAndStatusObj.draft = 0;
    this.searchMasterInstruByMastIdAndStatusObj.rejected = 0;
    this.searchMasterInstruByMastIdAndStatusObj.submitted = 0;
    this.searchMasterInstruByMastIdAndStatusObj.mastId = this.masterId;
    this.searchMasterInstruByMastIdAndStatusObj.archieved = 0;

    this.createCertificateService.getSrNoOfMasterInstrumentCalSpeciDetailByMastIdStatus(this.searchMasterInstruByMastIdAndStatusObj).subscribe(data => {
      console.log("sr no    :  " + data);
      this.srNoCalResultDetailsArray = data;

      for (var cp = 0; cp < this.srNoCalResultDetailsArray.length; cp++) {                                    
        let value = this.srNoCalResultDetailsArray[cp][3]
        let value1 = value.lastIndexOf('_');
        let value2 = value.slice(0, value1)
        console.log(value2);

        this.parametersAvailable.push(value2);

      }
      console.log(this.parametersAvailable);                             //parameters available                        

      //  this.parametersAvailable.filter(function(value, index){ return this.parametersAvailable.indexOf(value) == index });
      //  this.parametersAvailable = this.parametersAvailable.filter(function(val,ind) { return this.parametersAvailable.indexOf(val) == ind; })
      var filter = function (value, index) { return this.indexOf(value) == index };
      this.approvedParametersAvailable = this.parametersAvailable.filter(filter, this.parametersAvailable);
      console.log(this.approvedParametersAvailable);

    })



    this.newDynamic = [{

      id: "",
      name: "",
      stdReading: "",
      uucReading: "",
      error: "",
      unc: "",
      uncFormula: "",
      uncUom: "",
      acFrequency: "",
      acFrequencyUom: ""

    }]

    this.newDynamic2 = $.extend(true, [], this.newDynamic);

    this.newDynamic1 = [{

      nameofparameter: "",
      createMasterInstruId: "",
      masterSpecificationId: "",
      parameterId: "",
      parameterNumber: "",
      masterIdParaName: "",
      rangeFrom: "",
      rangeTo: "",
      freqFrom: "",
      freqTo: "",
      tempchartType: "",
      mode: ""

    }]

    //get mode list
    this.createCertificateService.getModeTypesListForMaster().subscribe(data => {
      this.arrayModeType = data;
      for (var i = 0, l = Object.keys(this.arrayModeType).length; i < l; i++) {
        this.arrayModeTypeListName.push(this.arrayModeType[i].modeName);
        this.arrayModeTypeListId.push(this.arrayModeType[i].modeId);
      }
    })

    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })

    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.ParameterListForReqAndIwdObj[i].frequency)
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    });

    //get Temp chart list
    this.createCertificateService.getTempChartTypesListForMaster().subscribe(data => {
      this.tempChartTypeForMasterInstrumentObj = data;
      for (var i = 0, l = Object.keys(this.tempChartTypeForMasterInstrumentObj).length; i < l; i++) {
        this.arrayTempChartTypeNames.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartName);
        this.arrayTempChartTypeId.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartId);
      }
    });

    //get frequency uom list
    this.createCertificateService.getFrequencyUomList().subscribe(data => {
      this.arrayOfFrequencyUomList = data;
      console.log(this.arrayOfFrequencyUomList)
      for (var i = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; i < l; i++) {
        this.arrayOfFrequencyUomNames.push(this.arrayOfFrequencyUomList[i].freqUomName);
        this.arrayOfFrequencyUomId.push(this.arrayOfFrequencyUomList[i].freqUomId);
      }
    },
      error => console.log(error));

    //get unc formula list
    this.createCertificateService.getUncFormulaList().subscribe(data => {

      this.arrayUncFormulaList = data;
      for (var i = 0, l = Object.keys(this.arrayUncFormulaList).length; i < l; i++) {
        this.arrayOfUncFormulaNames.push(this.arrayUncFormulaList[i].uncFormulaName);
        this.arrayOfUncFormulaId.push(this.arrayUncFormulaList[i].uncFormulaId);
      }
    },
      error => console.log(error));

  }

}
