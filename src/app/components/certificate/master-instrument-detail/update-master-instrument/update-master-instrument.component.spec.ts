import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMasterInstrumentComponent } from './update-master-instrument.component';

describe('UpdateMasterInstrumentComponent', () => {
  let component: UpdateMasterInstrumentComponent;
  let fixture: ComponentFixture<UpdateMasterInstrumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMasterInstrumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMasterInstrumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
