import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CalibrationAgencyOfMasterInstrument, CreateCertificateService, CreateMasterInstruCalibrationCertificateDetails, CreateMasterInstrumentSpecificationDetails, MasterInstruCalCertAttachments, ModeTypeForMasterInstrument, SearchAttachmentByMastIdAndCalCertId, SearchMasterInstruByMIdPNmAndStatus, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

declare var $: any;

@Component({
  selector: 'app-create-calibration-certificate-details',
  templateUrl: './create-calibration-certificate-details.component.html',
  styleUrls: ['./create-calibration-certificate-details.component.css']
})
export class CreateCalibrationCertificateDetailsComponent implements OnInit {

  public createCalCertifiMasInstruDetailsForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 30;

  searchMasterInstruByMIdPNmAndStatusObj: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();

  createMasterInstrumentCalCertificateObj1: CreateMasterInstrumentSpecificationDetails = new CreateMasterInstrumentSpecificationDetails();
  createMasterInstrumentCalCertificateObj: CreateMasterInstruCalibrationCertificateDetails = new CreateMasterInstruCalibrationCertificateDetails();
  dynamicArray: Array<CreateMasterInstruCalibrationCertificateDetails> = [];

  calibrationAgencyOfMasterInstruObj: CalibrationAgencyOfMasterInstrument = new CalibrationAgencyOfMasterInstrument();
  arrayOfCalAgencyType: Array<any> = [];
  arrayOfCalAgencyId: Array<any> = [];
  arrayOfCalAgencyNames: Array<any> = [];

  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArray1: any;
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  searchAttachmentByMastIdAndCalCertId: SearchAttachmentByMastIdAndCalCertId = new SearchAttachmentByMastIdAndCalCertId()
  customerIdForCustName: any;
  selectedFile: File;
  selectedFile1: File;
  selectedFileForAttachment: File;
  selectedFileForCustAttachmentTrial: any;
  selectedFileForCalCerti: any;
  trialFileReader: any;
  retrievedImage: any;
  base64Data: any;
  arrayBase64Data: Array<any> = [];
  arrayRetrievedImage: Array<any> = [];
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  enquiryAttachmentData1: Object;
  custAttachmentFileData: MasterInstruCalCertAttachments = new MasterInstruCalCertAttachments();
  printTermsId: any;
  docName: string;
  docType: string;
  docNumber: string;
  custAttachFiles: any;
  custAttachUpload: MasterInstruCalCertAttachments = new MasterInstruCalCertAttachments();
  custAttachObj: MasterInstruCalCertAttachments = new MasterInstruCalCertAttachments();
  custAttach: any;
  arrayOfMasterInstruFileAttach: Array<any> = [];
  retrieveResponse1: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  nameToDelete: any;
  indexToDelete: any;
  showFreq: any;
  freq: any;
  mastNameForNewFileName: string;
  indexToDeleteFile: any;
  deletedFile: any;
  arrayOfMasterInstruFileAttach1: Array<any> = [];
  arrayOfMasterInstruFileAttach2: Array<any> = [];
  arrayOfMasterInstruFileAttach3: Array<any> = [];
  newDynamic: any = [{}];
  mtCalCertId: any;
  windowRef = null;
  windowRef1 = null;
  trial1: any;
  
  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {

  }

  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }

  get formArr() {
    return this.createCalCertifiMasInstruDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      parameterId: [this.selectParameter],
      parameterNumber: [],
      calCertificate: [],
      calCertificateAgencyId: [],
      calibrationDate: [],
      calibrationDue: [],
      dateOfOutward: [],
      dateOfInward: [],
      calibrationCharges: [],
      attachment: [this.selectedFile1]
    });
  }

  ngAfterViewInit() {

    this.leng = (this.createCalCertifiMasInstruDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial!='undefined') {
        if (this.dynamicArrayTrial[l].parameterId != 0 && this.dynamicArrayTrial[l].parameterId != "" && typeof this.dynamicArrayTrial[l].parameterId!='undefined') {   //if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
      }

    }
  }


  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow(index) {

    var index1 = parseInt((this.itemsPerPage * (this.p - 1)) + (index));
    this.nameToDelete = ((this.createCalCertifiMasInstruDetailsForm.get('Rows') as FormArray).at(index1) as FormGroup).get('calCertificate').value;

    this.createCertificateService.getSrNoOfMasterInstrumentCalCertificateDetail(this.masterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;
      for (var i = 0; i < this.srNoMasterSpecifiDetailsArray.length; i++) {
        if (this.srNoMasterSpecifiDetailsArray[i][2] == this.nameToDelete) {
          this.indexToDelete = this.srNoMasterSpecifiDetailsArray[i][0];

          this.createCertificateService.deleteMastCalCertiAttchDetailsByCalId(this.indexToDelete).subscribe(data=>{
          this.createCertificateService.deleteMasterInstrumentCalCertificateDetail(this.indexToDelete).subscribe(data => {
            console.log("*********deleted row********" + data);
            alert("deleted successfully !!")
            })
          })
        }
      }
    });

    this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.splice(index1, 1);
    this.dynamicArrayTrial.splice(index1, 1);
    this.formArr.removeAt(index1);

  }

  deleteFile(e) {
    this.indexToDeleteFile = this.custAttachFiles[e].id;
    this.createCertificateService.deleteMastCalCertiAttchFile(this.indexToDeleteFile).subscribe((res) => {
      this.deletedFile = res;
      console.log(this.deletedFile);
      this.custAttachFiles.splice(e, 1);
    })
  }

  ngOnInit() {

    this.dynamicArrayTrial = JSON.parse(localStorage.getItem('mastCalCerti'));

    this.selectParameter = "Select Parameter";
    this.masterId = JSON.parse(localStorage.getItem('createMasterInstruId'));

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.createCalCertifiMasInstruDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        status: [],
        Rows: this.formBuilder.array([this.initRows()])
      });
    }
    else {

      this.createCalCertifiMasInstruDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        status: [],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
            parameterId,
            parameterNumber,
            calCertificate,
            calCertificateAgencyId,
            calibrationDate,
            calibrationDue,
            dateOfOutward,
            dateOfInward,
            calibrationCharges,
            attachment
          }) =>
            this.formBuilder.group({
              parameterId: [],
              parameterNumber: [parameterNumber],
              calCertificate: [calCertificate],
              calCertificateAgencyId: [calCertificateAgencyId],
              calibrationDate: [calibrationDate],
              calibrationDue: [calibrationDue],
              dateOfOutward: [dateOfOutward],
              dateOfInward: [dateOfInward],
              calibrationCharges: [calibrationCharges],
              attachment: []
            })
          )
        )
      })
    }

    //get Cal agency list
    this.createCertificateService.getCalibrationAgencyTypesForMaster().subscribe(data => {
      this.calibrationAgencyOfMasterInstruObj = data;
      for (var i = 0, l = Object.keys(this.calibrationAgencyOfMasterInstruObj).length; i < l; i++) {
        this.arrayOfCalAgencyNames.push(this.calibrationAgencyOfMasterInstruObj[i].calibrationAgencyName);
        this.arrayOfCalAgencyId.push(this.calibrationAgencyOfMasterInstruObj[i].calibrationAgencyId);
      }
    });

    (this.createCalCertifiMasInstruDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(this.dynamicArrayTrial);
      console.log(values);

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
        for (var i = 0, len = values.length; i < len; i++) {
          if (typeof this.dynamicArrayTrial[i] != 'undefined' && this.dynamicArrayTrial[i] != null && typeof values[i] != 'undefined') {
            if ((this.dynamicArrayTrial[i].calCertificate !== null && typeof this.dynamicArrayTrial[i].calCertificate !== 'undefined') && this.dynamicArrayTrial[i].calCertificateAgencyId !== null && this.dynamicArrayTrial[i].calibrationDate !== null
              && this.dynamicArrayTrial[i].calibrationDue !== null && this.dynamicArrayTrial[i].dateOfOutward !== null && this.dynamicArrayTrial[i].dateOfInward !== null && this.dynamicArrayTrial[i].calibrationCharges !== null) {
              if ((this.dynamicArrayTrial[i].calCertificate !== values[i].calCertificate || this.dynamicArrayTrial[i].calCertificateAgencyId !== values[i].calCertificateAgencyId || this.dynamicArrayTrial[i].calibrationDate !== values[i].calibrationDate
                || this.dynamicArrayTrial[i].calibrationDue !== values[i].calibrationDue || this.dynamicArrayTrial[i].dateOfOutward !== values[i].dateOfOutward || this.dynamicArrayTrial[i].dateOfInward !== values[i].dateOfInward || this.dynamicArrayTrial[i].calibrationCharges !== values[i].calibrationCharges)) {
                console.log("change" + i);
                var o = this.createCalCertifiMasInstruDetailsForm.get('Rows').value[i].active;

                document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";
              }
            }
          }
        }
      }
    });

    this.status = "Draft";

    this.newDynamic = [{

      id: "",
      mastId: "",
      mastCalCertId: "",
      documentType: "",
      documentName: "",
      documentNumber: "",
      originalFileName: "",
      attachedFileName: "",
      type: "",
      picbyte: "",
      fileLocation: "",

    }]

  }

  /////////////////Save data//////////////////////////
  saveData(e) {

    var j = e + 1;
    var index = parseInt((this.itemsPerPage * (this.p - 1)) + (e));
    var srnoCount = parseInt((this.itemsPerPage * (this.p - 1)) + (j));

    this.dynamicArray = this.createCalCertifiMasInstruDetailsForm.get('Rows').value;

    this.setDynamicArray();

    this.createMasterInstrumentCalCertificateObj.masterInstruId = this.masterId;
    this.createMasterInstrumentCalCertificateObj.calCertificate = this.dynamicArray[index].calCertificate;
    this.createMasterInstrumentCalCertificateObj.calCertificateAgencyId = this.dynamicArray[index].calCertificateAgencyId

   

    var dtOfCalibration = this.dynamicArray[index].calibrationDate;
    this.createMasterInstrumentCalCertificateObj.calibrationDate = this.convertStringToDate(dtOfCalibration);
    this.createMasterInstrumentCalCertificateObj.calibrationDate= this.datepipe.transform( this.createMasterInstrumentCalCertificateObj.calibrationDate, 'dd-MM-yyyy');

    var dtOfCalibrationDue = this.dynamicArray[index].calibrationDue;
    this.createMasterInstrumentCalCertificateObj.calibrationDue = this.convertStringToDate(dtOfCalibrationDue);
    this.createMasterInstrumentCalCertificateObj.calibrationDue= this.datepipe.transform( this.createMasterInstrumentCalCertificateObj.calibrationDue, 'dd-MM-yyyy');


    var dtOfOutward = this.dynamicArray[index].dateOfOutward
    this.createMasterInstrumentCalCertificateObj.dateOfOutward = this.convertStringToDate(dtOfOutward);
    this.createMasterInstrumentCalCertificateObj.dateOfOutward= this.datepipe.transform( this.createMasterInstrumentCalCertificateObj.dateOfOutward, 'dd-MM-yyyy');
   

    var dtOfInward = this.dynamicArray[index].dateOfInward;
    this.createMasterInstrumentCalCertificateObj.dateOfInward = this.convertStringToDate(dtOfInward);
    this.createMasterInstrumentCalCertificateObj.dateOfInward= this.datepipe.transform( this.createMasterInstrumentCalCertificateObj.dateOfInward, 'dd-MM-yyyy');


    this.createMasterInstrumentCalCertificateObj.calibrationCharges = this.dynamicArray[index].calibrationCharges;

    switch (this.status) {
      case 'Draft': this.createMasterInstrumentCalCertificateObj.draft = 1;
        this.createMasterInstrumentCalCertificateObj.archieved = 0;
        this.createMasterInstrumentCalCertificateObj.rejected = 0;
        this.createMasterInstrumentCalCertificateObj.approved = 0;
        this.createMasterInstrumentCalCertificateObj.submitted = 0;
        break;
      case 'Archieved': this.createMasterInstrumentCalCertificateObj.draft = 0
        this.createMasterInstrumentCalCertificateObj.archieved = 1;
        this.createMasterInstrumentCalCertificateObj.rejected = 0;
        this.createMasterInstrumentCalCertificateObj.approved = 0;
        this.createMasterInstrumentCalCertificateObj.submitted = 0;
        break;
      case 'Rejected': this.createMasterInstrumentCalCertificateObj.draft = 0;
        this.createMasterInstrumentCalCertificateObj.archieved = 0;
        this.createMasterInstrumentCalCertificateObj.rejected = 1;
        this.createMasterInstrumentCalCertificateObj.approved = 0;
        this.createMasterInstrumentCalCertificateObj.submitted = 0;
        break;
      case 'Approved': this.createMasterInstrumentCalCertificateObj.draft = 0;
        this.createMasterInstrumentCalCertificateObj.archieved = 0;
        this.createMasterInstrumentCalCertificateObj.rejected = 0;
        this.createMasterInstrumentCalCertificateObj.approved = 1;
        this.createMasterInstrumentCalCertificateObj.submitted = 0;
        break;
      case 'Submitted': this.createMasterInstrumentCalCertificateObj.draft = 0;
        this.createMasterInstrumentCalCertificateObj.archieved = 0;
        this.createMasterInstrumentCalCertificateObj.rejected = 0;
        this.createMasterInstrumentCalCertificateObj.approved = 0;
        this.createMasterInstrumentCalCertificateObj.submitted = 1;
        break;

    }

    for (var k = 0, l = Object.keys(this.calibrationAgencyOfMasterInstruObj).length; k < l; k++) {
      if (this.dynamicArray[index].calCertificateAgencyId == this.arrayOfCalAgencyNames[k]) {
        this.createMasterInstrumentCalCertificateObj.calCertificateAgencyId = this.arrayOfCalAgencyId[k];
        break;
      }
      else {
        this.createMasterInstrumentCalCertificateObj.calCertificateAgencyId = 1;
      }
    }


    this.createCertificateService.getSrNoOfMasterInstrumentCalCertificateDetail(this.masterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      this.checkIfSrNoAlreadyPresent(e);
    })
    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";
  }

  convertStringToDate(value) {

    if (typeof value === 'string') {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);

      return dateFormat;
    }
    else {
      return value;
    }
  }


  setDynamicArray() {

    localStorage.setItem('mastCalCerti', JSON.stringify(this.dynamicArray));
  }

  checkIfSrNoAlreadyPresent(e) {
    if (this.srNoMasterSpecifiDetailsArray.length == 0) {
      this.createCertificateService.createMasterInstrumentCalCertificateDetail(this.createMasterInstrumentCalCertificateObj).subscribe(data => {
        console.log("Master cal certificate details : " + data)
        this.srNoMasterSpecifiDetailsArray1 = data;
        alert("data saved successfully !")
        this.onUploadForDocument(e);
      })
    }
    else {
      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArray.length; this.w++) {
        this.srNoMasterSpecifiDetailsArray[this.w][2] = this.srNoMasterSpecifiDetailsArray[this.w][2].trim();
        this.createMasterInstrumentCalCertificateObj.calCertificate = this.createMasterInstrumentCalCertificateObj.calCertificate.trim();

        if (this.srNoMasterSpecifiDetailsArray[this.w][2] == this.createMasterInstrumentCalCertificateObj.calCertificate) {
          this.createCertificateService.updateMasterInstrumentCalCertificateDetail(this.srNoMasterSpecifiDetailsArray[this.w][0], this.createMasterInstrumentCalCertificateObj).subscribe(data => {
            console.log("Master cal certificate details : " + data)
            this.srNoMasterSpecifiDetailsArray1 = data;
            alert("data updated successfully !")
            this.onUploadForDocument(e);
          })
          break;
        }
      }
    }
    if (this.w >= this.srNoMasterSpecifiDetailsArray.length) {
      this.createCertificateService.createMasterInstrumentCalCertificateDetail(this.createMasterInstrumentCalCertificateObj).subscribe(data => {
        console.log("Master cal certificate details : " + data)
        this.srNoMasterSpecifiDetailsArray1 = data;
        alert("data saved successfully !")
        this.onUploadForDocument(e);
      })
    }
    this.dynamicArrayTrial = this.dynamicArray;
  }


  /////////////////// calibration certificate attachments ///////////////////////////


  public onFileChangedForDocument(event, i) {

    console.log(i);
    this.selectedFileForAttachment = event.target.files[0];
    this.selectedFileForCalCerti = $("#docFile")[0].files[0]
    this.selectedFileForCustAttachmentTrial = event.target;
    console.log(" this.selectedFileForAttachment " + this.selectedFileForAttachment)
  }

  onUploadForDocument(i) {

    console.log(this.selectedFileForAttachment);
    const uploadFileData = new FormData();
    var trial_for_target = this.selectedFileForCustAttachmentTrial;
    debugger;
    if (typeof this.selectedFileForAttachment != 'undefined') {
      this.addCustAttach(i);
    }

  }

  addCustAttach(i) {
    
    var originalName = this.selectedFileForAttachment.name;
    const uploadImageData = new FormData();
    var ii = i + 1;
    var newFileName = this.masterId + "_" + ii + "_" + this.selectedFileForAttachment.name;

    uploadImageData.append('imageFile', this.selectedFileForAttachment, newFileName);

    this.createCertificateService.mastCalCertiAttachmentsFileUpload(uploadImageData).subscribe(data => {
      console.log(data);
      this.custAttachUpload = data;

      this.custAttachObj.mastId = this.masterId;
      this.custAttachObj.mastCalCertId = this.srNoMasterSpecifiDetailsArray1.mastInstruCalCertificateDetId;
      this.mtCalCertId = this.custAttachObj.mastCalCertId;
      this.custAttachObj.originalFileName = originalName;

      this.createCertificateService.createMastCalCertiAttachments(this.custAttachObj).subscribe(data => {
        this.custAttach = data;
        console.log(data);
        alert("Document uploaded successfully !!");
        this.getCustomerAttachmentsFiles(i);
       
      },
        error => console.log(error));
    });


  }

  getCustomerAttachmentsFiles(i) {
   
    for (var e = 0; e < (this.createCalCertifiMasInstruDetailsForm.get('Rows') as FormArray).length; e++) {
      this.arrayOfMasterInstruFileAttach1.push(this.newDynamic);
    }

    this.searchAttachmentByMastIdAndCalCertId.mastId = this.masterId;
    this.searchAttachmentByMastIdAndCalCertId.mastCalCertId = this.mtCalCertId;

    
    this.createCertificateService.getMastCalCertiAttchDetailsByMastIdCalId(this.searchAttachmentByMastIdAndCalCertId).subscribe(data => {
      this.custAttachFiles = data as any[];
      this.retrieveResonse = data as any[];
      this.retrieveResponse1 = this.retrieveResonse.filter(function (f) { return f; })
      console.log(data);
     

      this.arrayOfMasterInstruFileAttach1[i] = this.retrieveResponse1;
    },
      error => console.log(error));
  }


  getSelectedFile(event, val1) {   //send file format data  t

    this.createCertificateService.getMastCalCertiAttachLists(this.masterId).subscribe(data => {
      this.custAttach = data;

      for (var ct = 0; ct < this.custAttach.length; ct++) {
        if (this.arrayOfMasterInstruFileAttach1[event][val1][7] == this.custAttach[ct].attachedFileName) {

          this.createCertificateService.getMastCalCertiAttchDetailsToGetDoc(this.custAttach[ct]).subscribe(data => {
            console.log(data);

            this.retrieveResonse = data;

            if (this.retrieveResonse.type.includes('/png') || this.retrieveResonse.type.includes('/jpg') || this.retrieveResonse.type.includes('/jpeg') || this.retrieveResonse.type.includes('/gif')) {

              this.base64Data = this.retrieveResonse.picByte;
              this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
              var img = '<img src="' + this.retrievedImage + '">';
              var popup = window.open();
              popup.document.write(img);
              popup.document.title = this.custAttachFiles[event].originalFileName;
            }
            else {

              this.base64Data = this.retrieveResonse.picByte;
              this.base64Datafile = atob(this.base64Data);
              const retrievedImage1 = 'data:application/pdf;base64,' + this.base64Data;
              let pdfWindow = window.open("")
              pdfWindow.document.write(
                "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
                encodeURI(this.base64Data) + "'></iframe>"
              )
            }
          })
        }
      }
    })
  }

  openChildWindow() {
    var url = '/crtMCalResuData';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receiveMessage.bind(this), false);
  }
  receiveMessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }
}
