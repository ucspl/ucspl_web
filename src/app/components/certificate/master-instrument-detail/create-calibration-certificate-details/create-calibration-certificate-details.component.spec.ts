import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCalibrationCertificateDetailsComponent } from './create-calibration-certificate-details.component';

describe('CreateCalibrationCertificateDetailsComponent', () => {
  let component: CreateCalibrationCertificateDetailsComponent;
  let fixture: ComponentFixture<CreateCalibrationCertificateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCalibrationCertificateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCalibrationCertificateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
