import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCalibrationCertificateDetailsComponent } from './update-calibration-certificate-details.component';

describe('UpdateCalibrationCertificateDetailsComponent', () => {
  let component: UpdateCalibrationCertificateDetailsComponent;
  let fixture: ComponentFixture<UpdateCalibrationCertificateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCalibrationCertificateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCalibrationCertificateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
