import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpecificationDetailsComponent } from './create-specification-details.component';

describe('CreateSpecificationDetailsComponent', () => {
  let component: CreateSpecificationDetailsComponent;
  let fixture: ComponentFixture<CreateSpecificationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSpecificationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpecificationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
