import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, CreateMasterInstrumentSpecificationDetails, ModeTypeForMasterInstrument, SearchMasterInstruByMIdPNmAndStatus, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-create-specification-details',
  templateUrl: './create-specification-details.component.html',
  styleUrls: ['./create-specification-details.component.css']
})
export class CreateSpecificationDetailsComponent implements OnInit {

  public createMasterSpecificationForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 30;

  searchMasterInstruByMIdPNmAndStatusObj: SearchMasterInstruByMIdPNmAndStatus = new SearchMasterInstruByMIdPNmAndStatus();

  createMasterInstrumentSpecificationObj: CreateMasterInstrumentSpecificationDetails = new CreateMasterInstrumentSpecificationDetails();
  dynamicArray: Array<CreateMasterInstrumentSpecificationDetails> = [];


  modeTypeForMasterInstrumentObj: ModeTypeForMasterInstrument = new ModeTypeForMasterInstrument();
  arrayModeType: Array<any> = [];
  arrayModeTypeListId: Array<any> = [];
  arrayModeTypeListName: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracy: Array<any> = [];
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];
  arrayOfParameterFrequency: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  tempChartTypeForMasterInstrumentObj: TempChartTypeForMasterInstrument = new TempChartTypeForMasterInstrument();
  arrayOfTempChartType: Array<any> = [];
  arrayTempChartTypeId: Array<any> = [];
  arrayTempChartTypeNames: Array<any> = [];

  //FrequencyUom
  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames:Array<any> = [];
  arrayOfFrequencyUomId:Array<any> = [];


  dynamicArrayTrial: Array<any> = [];
  srNoMasterSpecifiDetailsArray: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  showFreq: any;
  freq: any;
  previousStatus: any;

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService) {
  }

  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.createMasterSpecificationForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
     
      parameterId: [this.selectParameter],
      parameterNumber: [],
      rangeFrom: [],
      rangeTo: [],
      rangeUom: [],
      freqFrom: [],
      freqFromUom: [],
      freqTo: [],
      freqToUom: [],
      leastCount: [],
      leastCountUom: [],
      accuracy: [],
      formulaAccuracy: [],
      uomAccuracy: [],
      accuracy1: [],
      formulaAccuracy1: [],
      uomAccuracy1: [],
      tempChartType: [],
      modeType: []

    });
  }

  ngAfterViewInit() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null) {
        if (this.dynamicArrayTrial[l].parameterId != 0 && this.dynamicArrayTrial[l].parameterId != "") {   //if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
      }

      (<HTMLInputElement>document.getElementById("selectTag-" + l)).disabled = false;
      (<HTMLInputElement>document.getElementById("selectTag1-" + l)).disabled = false;

    }

  }

  accuracyValue(i) {
    this.forAccuValue = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0);
        console.log(((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0));
      }
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = false;
    }

  }

  accuracyUomValue(i){
    this.forAccuValue = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {     
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = false;
    }
  }

  accuracyValue1(i) {
    this.forAccuValue = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0);
        console.log(((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0));
      }
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = false;
    }
  }

  accuracyUomValue1(i){
    this.forAccuValue = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {     
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = true; 
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = false; 
    }

  }
  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow(index) {
    var index1 = parseInt((this.itemsPerPage * (this.p - 1)) + (index));

    this.srNoToDelete = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(index1) as FormGroup).get('parameterNumber').value;
    for (var i = 0; i < this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length; i++) {
      if (this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[i][3] == this.srNoToDelete) {
        this.indexToDelete = this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[i][0];
        this.createCertificateService.deleteMasterInstrumentSpecificationDetail(this.indexToDelete).subscribe(data => {
          console.log("*********deleted row********" + data);
          alert("deleted successfully !!")
        })
        break;
      }
    }

    this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.splice(index1, 1);
    this.dynamicArrayTrial.splice(index1, 1);
    this.formArr.removeAt(index1);

  }

  onChange() {

    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    var arrayUomNameAccor = new Array();

    for (var ii = 0, ll = Object.keys(this.ParameterListForReqAndIwdObj).length; ii < ll; ii++) {
      if (this.selectParameter == this.arrayParameterNames[ii]) {

        this.freq = this.arrayOfParameterFrequency[ii];
        if (this.freq == 1) {
          this.showFreq = 1;
        }
        else {
          this.showFreq = 0;
        }

        for (var iii = 0, lll = Object.keys(this.uomListForRequestAndInwardObj).length; iii < lll; iii++) {
          if (this.uomListForRequestAndInwardObj[iii].parameterId == this.arrayParameterId[ii]) {
            arrayUomNameAccor.push(this.uomListForRequestAndInwardObj[iii].uomName)
          }
        }
      }
    }

    this.arrayUomNameAccorToParam1 = arrayUomNameAccor;
    console.log("this.arrayUomNameAccorToParam1 " + this.arrayUomNameAccorToParam1)
    localStorage.setItem('arrayUomNameAccorToParam1ForMastInstru', JSON.stringify(this.arrayUomNameAccorToParam1));

    
    this.paraSpecifications();

  }

  ngOnInit() {

    this.dynamicArrayTrial = null;
    this.selectParameter = "Select Parameter";
    this.masterId = JSON.parse(localStorage.getItem('createMasterInstruId'));    
    //     

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        selectParameter: [],
        status: [],
        Rows: this.formBuilder.array([this.initRows()])
      });
    }
    else {
      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        selectParameter: [],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
          
            parameterId,
            parameterNumber,
            rangeFrom,
            rangeTo,
            rangeUom,
            freqFrom,
            freqFromUom,
            freqTo,
            freqToUom,
            leastCount,
            leastCountUom,
            accuracy,
            formulaAccuracy,
            uomAccuracy,
            accuracy1,
            formulaAccuracy1,
            uomAccuracy1,
            tempChartType,
            modeType
          }) =>
            this.formBuilder.group({
            
              parameterId: [],
              parameterNumber: [parameterNumber],
              rangeFrom: [rangeFrom],
              rangeTo: [rangeTo],
              rangeUom: [rangeUom],
              freqFrom: [freqFrom],
              freqFromUom: [freqFromUom],
              freqTo: [freqTo],
              freqToUom: [freqToUom],
              leastCount: [leastCount],
              leastCountUom: [leastCountUom],
              accuracy: [accuracy],
              formulaAccuracy: [formulaAccuracy],
              uomAccuracy: [uomAccuracy],
              accuracy1: [accuracy1],
              formulaAccuracy1: [formulaAccuracy1],
              uomAccuracy1: [uomAccuracy1],
              tempChartType: [tempChartType],
              modeType: [modeType]
            })
          )
        )

      })

      this.arrayUomNameAccorToParam2 = JSON.parse(localStorage.getItem('arrayUomNameAccorToParam1ForMastInstru'));
      if (this.arrayUomNameAccorToParam2 != null || typeof this.arrayUomNameAccorToParam2 != 'undefined') {
        this.arrayUomNameAccorToParam1 = this.arrayUomNameAccorToParam2;
      }

    }

    //get mode list
    this.createCertificateService.getModeTypesListForMaster().subscribe(data => {
      this.arrayModeType = data;
      for (var i = 0, l = Object.keys(this.arrayModeType).length; i < l; i++) {
        this.arrayModeTypeListName.push(this.arrayModeType[i].modeName);
        this.arrayModeTypeListId.push(this.arrayModeType[i].modeId);
      }
    })

    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })

    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.ParameterListForReqAndIwdObj[i].frequency)
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    });

    //get Temp chart list
    this.createCertificateService.getTempChartTypesListForMaster().subscribe(data => {
      this.tempChartTypeForMasterInstrumentObj = data;
      for (var i = 0, l = Object.keys(this.tempChartTypeForMasterInstrumentObj).length; i < l; i++) {
        this.arrayTempChartTypeNames.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartName);
        this.arrayTempChartTypeId.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartId);
      }
    });

    // get frequency uom list
    this.createCertificateService.getFrequencyUomList().subscribe(data => {

      this.arrayOfFrequencyUomList = data;
      console.log(this.arrayOfFrequencyUomList)
      for (var i = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; i < l; i++) {
        this.arrayOfFrequencyUomNames.push(this.arrayOfFrequencyUomList[i].freqUomName);
        this.arrayOfFrequencyUomId.push(this.arrayOfFrequencyUomList[i].freqUomId);
      }
    },
      error => console.log(error));

    
    (this.createMasterSpecificationForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(this.dynamicArrayTrial);
      console.log(values);

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
        for (var i = 0, len = values.length; i < len; i++) {
          if (typeof this.dynamicArrayTrial[i] != 'undefined' && this.dynamicArrayTrial[i] != null && typeof values[i] != 'undefined') {
              if ((this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].rangeUom !== values[i].rangeUom
                || this.dynamicArrayTrial[i].freqFrom !== values[i].freqFrom || this.dynamicArrayTrial[i].freqTo !== values[i].freqTo || this.dynamicArrayTrial[i].freqFromUom !== values[i].freqFromUom || this.dynamicArrayTrial[i].freqToUom !== values[i].freqToUom
                || this.dynamicArrayTrial[i].leastCount !== values[i].leastCount || this.dynamicArrayTrial[i].leastCountUom !== values[i].leastCountUom
                || this.dynamicArrayTrial[i].accuracy !== values[i].accuracy || this.dynamicArrayTrial[i].formulaAccuracy !== values[i].formulaAccuracy || this.dynamicArrayTrial[i].uomAccuracy !== values[i].uomAccuracy
                || this.dynamicArrayTrial[i].accuracy1 !== values[i].accuracy1 || this.dynamicArrayTrial[i].formulaAccuracy1 !== values[i].formulaAccuracy1 || this.dynamicArrayTrial[i].uomAccuracy1 !== values[i].uomAccuracy1
                || this.dynamicArrayTrial[i].tempChartType !== values[i].tempChartType || this.dynamicArrayTrial[i].modeType !== values[i].modeType)
                ) {
                console.log("change" + i);
                var o = this.createMasterSpecificationForm.get('Rows').value[i].active;

                if(document.getElementById("btn-" + i) != null)
                {
                  document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";
                }
                
              }
            }
          
        }
      }
    });


  }



  paraSpecifications() {

   if(this.previousSelectedParameter!=this.selectParameter ||   this.previousStatus != this.status)
   {

    this.srNoMasterSpecifiDetailsArrayByMastIdParaNm = this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.splice(0, this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length);
    this.itemsPerPage = 50;
    var msIdParaName1 = this.masterId + "_" + this.selectParameter+"_"+this.status;

    this.searchMasterInstruByMIdPNmAndStatusObj.msIdParaName = msIdParaName1;
    switch (this.status) {
      case 'Draft': this.searchMasterInstruByMIdPNmAndStatusObj.draft = 1;
        this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.approved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 0;
        break;
      case 'Archieved': this.searchMasterInstruByMIdPNmAndStatusObj.draft = 0
        this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 1;
        this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.approved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 0;
        break;
      case 'Rejected': this.searchMasterInstruByMIdPNmAndStatusObj.draft = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 1;
        this.searchMasterInstruByMIdPNmAndStatusObj.approved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 0;
        break;
      case 'Approved': this.searchMasterInstruByMIdPNmAndStatusObj.draft = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.approved = 1;
        this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 0;
        break;
      case 'Submitted': this.searchMasterInstruByMIdPNmAndStatusObj.draft = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.archieved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.rejected = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.approved = 0;
        this.searchMasterInstruByMIdPNmAndStatusObj.submitted = 1;
        break;

    }

    (this.createMasterSpecificationForm.get('Rows') as FormArray).reset();
    var leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    console.log(leng);

    for (let c = 0; c < leng; c++) {
      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
        this.dynamicArrayTrial.splice(c, 1);
        this.formArr.removeAt(c);
      }

    }


    this.createCertificateService.getSrNoOfMasterInstruSpecifiDetailByMastIdParaName(this.searchMasterInstruByMIdPNmAndStatusObj).subscribe(data => {
      console.log(data);
      this.srNoMasterSpecifiDetailsArrayByMastIdParaNm = data;
      this.srNoMasterSpecifiDetailsArrayByMastIdParaNm1 = data;

      var leng1 = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;

      if (leng1 == 0) {
        this.addNewRow();
      }
      if (leng1 > 1) {
        while (leng1 > 1) {
       
          leng1--;
          this.formArr.removeAt(leng1);
          
        }
      }

      if (this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length != 0) {

        for (this.mi = 0; this.mi < this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length; this.mi++) {
         if(typeof((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup)!='undefined'){
          if (((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeFrom').value == null) {

            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('parameterNumber').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][3]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeFrom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][4]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeTo').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][5]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeUom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][6]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqFrom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][7]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqFromUom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][8]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqTo').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][9]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqToUom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][10]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('leastCount').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][11]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('leastCountUom').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][12]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('accuracy').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][13]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('formulaAccuracy').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][14]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uomAccuracy').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][15]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('accuracy1').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][16]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('formulaAccuracy1').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][17]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uomAccuracy1').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][18]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('tempChartType').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][19]);
            ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('modeType').patchValue(this.srNoMasterSpecifiDetailsArrayByMastIdParaNm[this.mi][20]);

            this.addNewRow();
          }
        }
        }
        this.dynamicArrayTrial = this.createMasterSpecificationForm.get('Rows').value;
      }
    })

    this.previousSelectedParameter = this.selectParameter;
    this.previousStatus = this.status;

    this.createCertificateService.getModeTypesListForMaster().subscribe(data => {
      for (let c = 0; c < this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length; c++) {

        if(typeof ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup)!='undefined')
        {
          var rangeF = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup).get('rangeFrom').value
          if (rangeF != null && typeof rangeF != 'undefined') {
  
            if (c < this.itemsPerPage)
              document.getElementById("btn-" + c).style.backgroundColor = "#5cb85c";
          }
        }
      }
   

      if (this.mi >= this.srNoMasterSpecifiDetailsArrayByMastIdParaNm.length) {
        var leng2 = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
        var val = this.mi;
        for (this.mi; this.mi <= leng2;) {
      
          this.formArr.removeAt(val);
          leng2--; 
        }
      }
    })
 }
  }

  getPage(page: number, sort: string = 'asc', order: string = 'created') {
    this.p = page;
    var count = (this.createMasterSpecificationForm.get('Rows') as FormArray).length - (this.itemsPerPage * this.p)

    if (count < 0)
      count = -(count);

    for (let c = 0; c < count; c++) {
      if (((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup).get('rangeFrom').value != null) {
        document.getElementById("btn-" + c).style.backgroundColor = "#5cb85c";
      }
    }
  }

  saveData(e) {

    var j = e + 1;
    var index = parseInt((this.itemsPerPage * (this.p - 1)) + (e));
    var srNoCount = parseInt((this.itemsPerPage * (this.p - 1)) + (j));

    this.dynamicArray = this.createMasterSpecificationForm.get('Rows').value;
    ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(index) as FormGroup).get('parameterNumber').patchValue(this.dynamicArray[index].parameterId + "_" + srNoCount);
    this.dynamicArray = this.createMasterSpecificationForm.get('Rows').value;

    this.setDynamicArray()
    this.createMasterInstrumentSpecificationObj.createMasterInstruId = this.masterId;
    this.createMasterInstrumentSpecificationObj.tempChartType = this.dynamicArray[index].tempChartType;
    this.createMasterInstrumentSpecificationObj.parameterNumber = this.selectParameter + "_" + srNoCount;
    this.createMasterInstrumentSpecificationObj.masterIdParaName = this.masterId + "_" + this.selectParameter+"_"+this.status;


    switch (this.status) {
      case 'Draft': this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Archieved': this.createMasterInstrumentSpecificationObj.draft = 0
        this.createMasterInstrumentSpecificationObj.archieved = 1;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Rejected': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 1;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Approved': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 1;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

      case 'Submitted': this.createMasterInstrumentSpecificationObj.draft = 0;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 1;
        break;

      default: this.createMasterInstrumentSpecificationObj.draft = 1;
        this.createMasterInstrumentSpecificationObj.archieved = 0;
        this.createMasterInstrumentSpecificationObj.rejected = 0;
        this.createMasterInstrumentSpecificationObj.approved = 0;
        this.createMasterInstrumentSpecificationObj.submitted = 0;
        break;

    }

    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
      if (this.selectParameter == this.arrayParameterNames[k]) {
        this.createMasterInstrumentSpecificationObj.parameterId = this.arrayParameterId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.parameterId = 1;
      }
    }

    this.createMasterInstrumentSpecificationObj.rangeFrom = this.dynamicArray[index].rangeFrom
    this.createMasterInstrumentSpecificationObj.rangeTo = this.dynamicArray[index].rangeTo

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].rangeUom == this.arrayUomName[k]) {
        this.createMasterInstrumentSpecificationObj.rangeUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.rangeUom = 1;
      }
    }

    this.createMasterInstrumentSpecificationObj.freqFrom = this.dynamicArray[index].freqFrom

   
    // save  frequency from uom
    for (var k = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; k < l; k++) {
      if (this.dynamicArray[index].freqFromUom == this.arrayOfFrequencyUomNames[k]) {
        this.createMasterInstrumentSpecificationObj.freqFromUom =this.arrayOfFrequencyUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.freqFromUom = 1;
      }
    }

    this.createMasterInstrumentSpecificationObj.freqTo = this.dynamicArray[index].freqTo

    // save  frequency to uom
    for (var k = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; k < l; k++) {
      if (this.dynamicArray[index].freqToUom == this.arrayOfFrequencyUomNames[k]) {
        this.createMasterInstrumentSpecificationObj.freqToUom =this.arrayOfFrequencyUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.freqToUom = 1;
      }
    }


    this.createMasterInstrumentSpecificationObj.leastCount = this.dynamicArray[index].leastCount

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].leastCountUom == this.arrayUomName[k]) {
        this.createMasterInstrumentSpecificationObj.leastCountUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.leastCountUom = 1;
      }
    }

    this.createMasterInstrumentSpecificationObj.accuracy = this.dynamicArray[index].accuracy

    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {
      if (this.dynamicArray[index].formulaAccuracy == this.arrayFormulaAccuracyName[k]) {
        this.createMasterInstrumentSpecificationObj.formulaAccuracy = this.arrayFormulaAccuracyId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.formulaAccuracy = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].uomAccuracy == this.arrayUomName[k]) {
        this.createMasterInstrumentSpecificationObj.uomAccuracy = this.arrayUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.uomAccuracy = 1;
      }
    }

    this.createMasterInstrumentSpecificationObj.accuracy1 = this.dynamicArray[index].accuracy1;

    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {
      if (this.dynamicArray[index].formulaAccuracy1 == this.arrayFormulaAccuracyName[k]) {
        this.createMasterInstrumentSpecificationObj.formulaAccuracy1 = this.arrayFormulaAccuracyId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.formulaAccuracy1 = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].uomAccuracy1 == this.arrayUomName[k]) {
        this.createMasterInstrumentSpecificationObj.uomAccuracy1 = this.arrayUomId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.uomAccuracy1 = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.arrayModeType).length; k < l; k++) {
      if (this.dynamicArray[index].modeType == this.arrayModeTypeListName[k]) {
        this.createMasterInstrumentSpecificationObj.modeType = this.arrayModeTypeListId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.modeType = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.tempChartTypeForMasterInstrumentObj).length; k < l; k++) {
      if (this.dynamicArray[index].tempChartType == this.arrayTempChartTypeNames[k]) {
        this.createMasterInstrumentSpecificationObj.tempChartType = this.arrayTempChartTypeId[k];
        break;
      }
      else {
        this.createMasterInstrumentSpecificationObj.tempChartType = 1;
      }
    }


    this.createCertificateService.getSrNoOfMasterInstrumentSpecificationDetail(this.masterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoMasterSpecifiDetailsArray = data;

      this.checkIfSrNoAlreadyPresent();
    })
    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";
  }

  setDynamicArray() {

    localStorage.setItem('masterSpeci', JSON.stringify(this.dynamicArray));
  }

  checkIfSrNoAlreadyPresent() {
    if (this.srNoMasterSpecifiDetailsArray.length == 0) {
      this.createCertificateService.createMasterInstrumentSpecificationDetail(this.createMasterInstrumentSpecificationObj).subscribe(data => {
        console.log("Master specification details : " + data)
        alert("data saved successfully !")
      })
    }
    else {
      for (this.w = 0; this.w < this.srNoMasterSpecifiDetailsArray.length; this.w++) {
        if (this.srNoMasterSpecifiDetailsArray[this.w][26] == this.createMasterInstrumentSpecificationObj.masterIdParaName && this.srNoMasterSpecifiDetailsArray[this.w][3] == this.createMasterInstrumentSpecificationObj.parameterNumber) {
          this.createCertificateService.updateMasterInstrumentSpecificationDetail(this.srNoMasterSpecifiDetailsArray[this.w][0], this.createMasterInstrumentSpecificationObj).subscribe(data => {
            console.log("Master specification details : " + data)
            alert("data updated successfully !")
          })
          break;
        }
      }
    }
    if (this.w >= this.srNoMasterSpecifiDetailsArray.length) {
      this.createCertificateService.createMasterInstrumentSpecificationDetail(this.createMasterInstrumentSpecificationObj).subscribe(data => {
        console.log("Master specification details : " + data)
        alert("data saved successfully !")
      })
    }
    this.dynamicArrayTrial = this.dynamicArray;
  }

}
