import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNablComponent } from './create-nabl.component';

describe('CreateNablComponent', () => {
  let component: CreateNablComponent;
  let fixture: ComponentFixture<CreateNablComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNablComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNablComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
