import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateNablScopeMaster, CreateNablScopeService } from '../../../../services/serviceconnection/create-service/create-nabl-scope.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';

@Component({
  selector: 'app-create-nabl',
  templateUrl: './create-nabl.component.html',
  styleUrls: ['./create-nabl.component.css']
})
export class CreateNablComponent implements OnInit {

  createNABLScopeMastersForm: FormGroup;
  createNablScopeMasterObj: CreateNablScopeMaster = new CreateNablScopeMaster();

  masterData: any;
  dateString = '';
  format = 'dd/MM/yyyy';

  dateValue: any;
  dateValue1: any;

  windowRef = null;
  windowRef1 = null;
  f2: FormGroup;


  trial1: any;
  trial2: any;
  loginUserName: any;
  systemCertificateNo: any;
  nablCertificateNo: any;
  issueDate: any;
  validDate: any;


  constructor(private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService, private datepipe: DatePipe, private createNablScopeService: CreateNablScopeService) {

    this.createNABLScopeMastersForm = this.formBuilder.group({

      nablCertificateNo: [],
      labLocation: [],
      dateOfIssue: [],
      validUpTo: [],
      status: [],
      systemCertificateNo: [],

    });

    this.f2 = this.formBuilder.group({
      tName: [''],
    })

  }


  ngOnInit() {

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.dateValue = new Date();
    this.dateValue1 = new Date();
  }

  change() {
    var dtOfIssue = this.convertStringToDate(this.dateValue);
    this.issueDate = this.datepipe.transform(dtOfIssue, 'dd-MM-yyyy');
    this.issueDate = this.issueDate.slice(6, 10);
    console.log(this.issueDate);

    var dtOfValidUpTo = this.convertStringToDate(this.dateValue1);
    this.validDate = this.datepipe.transform(dtOfValidUpTo, 'dd-MM-yyyy');
    this.validDate = this.validDate.slice(6, 10);

    this.systemCertificateNo = this.nablCertificateNo + "_" + this.issueDate + "_" + this.validDate;
  }
  saveData() {
    this.createNablScopeMasterObj.nablCertificateNo = this.createNABLScopeMastersForm.value.nablCertificateNo;
    this.createNablScopeMasterObj.labLocation = this.createNABLScopeMastersForm.value.labLocation;
    this.createNablScopeMasterObj.createdBy = this.loginUserName
    this.createNablScopeMasterObj.systemCertificateNo = this.systemCertificateNo;

    this.createNablScopeMasterObj.dateOfIssue = this.createNABLScopeMastersForm.value.dateOfIssue;
    var dtOfIssue = this.convertStringToDate(this.createNablScopeMasterObj.dateOfIssue);
    this.createNablScopeMasterObj.dateOfIssue = this.datepipe.transform(dtOfIssue, 'dd-MM-yyyy');

    this.createNablScopeMasterObj.validUpTo = this.createNABLScopeMastersForm.value.validUpTo;
    var dtOfValidUpTo = this.convertStringToDate(this.createNablScopeMasterObj.validUpTo);
    this.createNablScopeMasterObj.validUpTo = this.datepipe.transform(dtOfValidUpTo, 'dd-MM-yyyy');



    switch (this.createNABLScopeMastersForm.value.status) {
      case 'Draft': this.createNablScopeMasterObj.draft = 1;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Archieved': this.createNablScopeMasterObj.draft = 0
        this.createNablScopeMasterObj.archieved = 1;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Rejected': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 1;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Approved': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 1;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Submitted': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 1;
        break;

    }


    console.log("Create Master Instrument Details Obj" + this.createNablScopeMasterObj);
    this.createNablScopeService.createNablScopeMaster(this.createNablScopeMasterObj).subscribe(data => {
      console.log(data);
      this.masterData = data;
      alert("nabl scope Master saved successfully!!")
      localStorage.setItem('createNableScopeMasterId', JSON.stringify(this.masterData.createNableScopeMasterId));
    })


  }

  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }


  openChildWindow() {
    var url = '/createnablespecdet';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receiveMessage.bind(this), false);
  }

  receiveMessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }


}
