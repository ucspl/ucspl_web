import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateNablScopeMaster, CreateNablScopeService, DynamicGridForNabl } from '../../../../services/serviceconnection/create-service/create-nabl-scope.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { SearchNablService } from '../../../../services/serviceconnection/search-service/search-nabl.service';

@Component({
  selector: 'app-update-nabl',
  templateUrl: './update-nabl.component.html',
  styleUrls: ['./update-nabl.component.css']
})
export class UpdateNablComponent implements OnInit {

  createNABLScopeMastersForm: FormGroup;
  createNablScopeMasterObj: CreateNablScopeMaster = new CreateNablScopeMaster();

  masterData: any;
  dateString = '';
  format = 'dd/MM/yyyy';

  dateValue: any;
  dateValue1: any;

  windowRef = null;
  windowRef1 = null;
  f2: FormGroup;

  trial1: any;
  trial2: any;
  loginUserName: any;
  systemCertificateNo: any;
  nablCertificateNo: any;
  issueDate: any;
  validDate: any;
  certiNoFromSearchNabl: any;
  createNablInfoForSearchNablObject: any;
  labLocation: any;
  status: string;
  createNableScopeMasterId: number;

  dynamicArray: Array<DynamicGridForNabl> = [];
  dynamicArrayTrial: Array<DynamicGridForNabl> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  nablSpecificationVariableArrayForSearchNabl: any;
  calibrationLabId: any;
  groupId: any;

  constructor(private searchNablService: SearchNablService, private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService, private datepipe: DatePipe, private createNablScopeService: CreateNablScopeService) {

    this.createNABLScopeMastersForm = this.formBuilder.group({

      nablCertificateNo: [],
      labLocation: [],
      dateOfIssue: [],
      validUpTo: [],
      status: [],
      systemCertificateNo: [],
    });

    this.f2 = this.formBuilder.group({
      tName: [''],
    })

  }


  ngOnInit() {
    window.localStorage.removeItem('groupId');
    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.dateValue = new Date();
    this.dateValue1 = new Date();
    this.certiNoFromSearchNabl = this.searchNablService.getCertificateNoForUpdateNablScreen();
    this.dataFromSearchNabl();
  }

  dataFromSearchNabl() {

    if (typeof this.certiNoFromSearchNabl != 'undefined') {

      this.searchNablService.sendCertificateNoForGettingCreateNablDetails(this.certiNoFromSearchNabl).subscribe(data => {
        console.log(data);

        this.searchNablService.getCreateNablDetailsForSearchNabl().subscribe(data => {
          this.createNablInfoForSearchNablObject = data;

          if (typeof this.createNablInfoForSearchNablObject != 'undefined') {
            this.createNableScopeMasterId = this.createNablInfoForSearchNablObject[0][0];
            this.nablCertificateNo = this.createNablInfoForSearchNablObject[0][1];
            this.labLocation = this.createNablInfoForSearchNablObject[0][2];
            this.dateValue = this.createNablInfoForSearchNablObject[0][3];
            this.dateValue1 = this.createNablInfoForSearchNablObject[0][4];
            this.systemCertificateNo = this.createNablInfoForSearchNablObject[0][13];
            this.convertStatusIdToName();

          }
        })

      })
    }
  }

  convertStatusIdToName() {

    if (this.createNablInfoForSearchNablObject[0][5] == 1) {
      this.status = "Draft";
    }
    if (this.createNablInfoForSearchNablObject[0][6] == 1) {
      this.status = "Approved";
    }
    if (this.createNablInfoForSearchNablObject[0][7] == 1) {
      this.status = "Submitted";
    }
    if (this.createNablInfoForSearchNablObject[0][8] == 1) {
      this.status = "Rejected";
    }
    if (this.createNablInfoForSearchNablObject[0][9] == 1) {
      this.status = "Archieved";
    }

  }

  change() {
    var dtOfIssue = this.convertStringToDate(this.dateValue);
    this.issueDate = this.datepipe.transform(dtOfIssue, 'dd-MM-yyyy');
    this.issueDate = this.issueDate.slice(6, 10);
    console.log(this.issueDate);

    var dtOfValidUpTo = this.convertStringToDate(this.dateValue1);
    this.validDate = this.datepipe.transform(dtOfValidUpTo, 'dd-MM-yyyy');
    this.validDate = this.validDate.slice(6, 10);
    this.systemCertificateNo = this.nablCertificateNo + "_" + this.issueDate + "_" + this.validDate;
  }

  saveData() {
    this.createNablScopeMasterObj.nablCertificateNo = this.createNABLScopeMastersForm.value.nablCertificateNo;
    this.createNablScopeMasterObj.labLocation = this.createNABLScopeMastersForm.value.labLocation;
    this.createNablScopeMasterObj.createdBy = this.loginUserName
    this.createNablScopeMasterObj.systemCertificateNo = this.systemCertificateNo;

    this.createNablScopeMasterObj.dateOfIssue = this.createNABLScopeMastersForm.value.dateOfIssue;
    var dtOfIssue = this.convertStringToDate(this.createNablScopeMasterObj.dateOfIssue);
    this.createNablScopeMasterObj.dateOfIssue = this.datepipe.transform(dtOfIssue, 'dd-MM-yyyy');

    this.createNablScopeMasterObj.validUpTo = this.createNABLScopeMastersForm.value.validUpTo;
    var dtOfValidUpTo = this.convertStringToDate(this.createNablScopeMasterObj.validUpTo);
    this.createNablScopeMasterObj.validUpTo = this.datepipe.transform(dtOfValidUpTo, 'dd-MM-yyyy');

    switch (this.createNABLScopeMastersForm.value.status) {
      case 'Draft': this.createNablScopeMasterObj.draft = 1;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Archieved': this.createNablScopeMasterObj.draft = 0
        this.createNablScopeMasterObj.archieved = 1;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Rejected': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 1;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Approved': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 1;
        this.createNablScopeMasterObj.submitted = 0;
        break;
      case 'Submitted': this.createNablScopeMasterObj.draft = 0;
        this.createNablScopeMasterObj.archieved = 0;
        this.createNablScopeMasterObj.rejected = 0;
        this.createNablScopeMasterObj.approved = 0;
        this.createNablScopeMasterObj.submitted = 1;
        break;

    }


    console.log("Create Master Instrument Details Obj" + this.createNablScopeMasterObj);
    this.createNablScopeService.updateNablScopeMaster(this.createNableScopeMasterId, this.createNablScopeMasterObj).subscribe(data => {
      console.log(data);
      this.masterData = data;
      alert("nabl scope Master updated successfully!!")

    })

  }

  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }


  openChildWindow() {
    this.getNablSpecificationDetailForSearchNabl();
  }

  receiveMessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }


  getNablSpecificationDetailForSearchNabl() {
    window.localStorage.removeItem('dynamicArrayTrialForNabl');

    this.newDynamic1.splice(0, this.newDynamic1.length - 1);
    this.createNablScopeService.sendNablIdForGettingCreateNablSpecificationDetails(this.createNableScopeMasterId).subscribe(data => {
      console.log("*********sr no for doc no********" + data);

      this.createNablScopeService.getCreateNablSpecificationDetailsForSearchNabl().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.nablSpecificationVariableArrayForSearchNabl = data;

        if (this.nablSpecificationVariableArrayForSearchNabl.length != 0) {

          for (var z = 0; z < this.nablSpecificationVariableArrayForSearchNabl.length; z++) {
            this.newDynamic1.push({
              nablScopeId: this.nablSpecificationVariableArrayForSearchNabl[z][1],
              rangeFrom: this.nablSpecificationVariableArrayForSearchNabl[z][7],
              rangeTo: this.nablSpecificationVariableArrayForSearchNabl[z][8],
              rangeUom: this.nablSpecificationVariableArrayForSearchNabl[z][9],
              freqFrom: this.nablSpecificationVariableArrayForSearchNabl[z][10],
              freqFromUom: this.nablSpecificationVariableArrayForSearchNabl[z][11],
              freqTo: this.nablSpecificationVariableArrayForSearchNabl[z][12],
              freqToUom: this.nablSpecificationVariableArrayForSearchNabl[z][13],
              leastCount: this.nablSpecificationVariableArrayForSearchNabl[z][14],
              leastCountUom: this.nablSpecificationVariableArrayForSearchNabl[z][15],
              uncValue: this.nablSpecificationVariableArrayForSearchNabl[z][16],
              uncFormulaId: this.nablSpecificationVariableArrayForSearchNabl[z][17],
              uncUom: this.nablSpecificationVariableArrayForSearchNabl[z][18],
              tempChartType: this.nablSpecificationVariableArrayForSearchNabl[z][19],
              modeType: this.nablSpecificationVariableArrayForSearchNabl[z][20],
              amendmentDate: this.nablSpecificationVariableArrayForSearchNabl[z][21],
              inhouse: this.nablSpecificationVariableArrayForSearchNabl[z][22],
              onsite: this.nablSpecificationVariableArrayForSearchNabl[z][23],

            })

          }

          this.dynamicArray.splice(0, this.dynamicArray.length);
          for (var l = 1; l < this.newDynamic1.length; l++) {

            this.dynamicArray.push(this.newDynamic1[l]);
          }
        }

        console.log(this.nablSpecificationVariableArrayForSearchNabl[0][6]);
        localStorage.setItem('groupId1', JSON.stringify(this.nablSpecificationVariableArrayForSearchNabl[0][6]));
        localStorage.setItem('calibrationLabId', JSON.stringify(this.nablSpecificationVariableArrayForSearchNabl[0][0]));
        localStorage.setItem('parameterId', JSON.stringify(this.nablSpecificationVariableArrayForSearchNabl[0][4]));
        localStorage.setItem('dynamicArrayTrialForNabl', JSON.stringify(this.dynamicArray));
        localStorage.setItem('createNableScopeMasterIdForUpdate', JSON.stringify(this.createNableScopeMasterId));

        var url = '/updatenablespecdet';
        this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
        this.windowRef.focus();
        this.windowRef.addEventListener("message", this.receiveMessage.bind(this), false);
      })

    })
  }

  backToMenu() {
    this.router.navigateByUrl('nav/searchnabl');
  }
}
