import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNablComponent } from './update-nabl.component';

describe('UpdateNablComponent', () => {
  let component: UpdateNablComponent;
  let fixture: ComponentFixture<UpdateNablComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateNablComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNablComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
