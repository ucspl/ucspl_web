import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNablSpecificationComponent } from './update-nabl-specification.component';

describe('UpdateNablSpecificationComponent', () => {
  let component: UpdateNablSpecificationComponent;
  let fixture: ComponentFixture<UpdateNablSpecificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateNablSpecificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNablSpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
