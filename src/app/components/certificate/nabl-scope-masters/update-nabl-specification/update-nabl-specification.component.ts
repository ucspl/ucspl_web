import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCertificateService, TempChartTypeForMasterInstrument } from '../../../../services/serviceconnection/create-service/create-certificate.service';
import { CreateNablSpecificationDetails, CreateNablScopeService, GroupForNablScopeMaster, ModeTypeForMasterInstrument, NablScopeName, SearchNablSpecificationByNablIdPNmAndStatus } from '../../../../services/serviceconnection/create-service/create-nabl-scope.service';
import { CreateReqAndInwardService, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-update-nabl-specification',
  templateUrl: './update-nabl-specification.component.html',
  styleUrls: ['./update-nabl-specification.component.css']
})
export class UpdateNablSpecificationComponent implements OnInit {

  dateString = '';
  format = 'dd/MM/yyyy';
  date: any;

  public dateValue = new Date();
  public createMasterSpecificationForm: FormGroup;

  p: number = 1;
  itemsPerPage: number = 30;

  searchNablSpecificationByNablIdPNmAndStatusObj: SearchNablSpecificationByNablIdPNmAndStatus = new SearchNablSpecificationByNablIdPNmAndStatus();
  createNablSpecificationObj: CreateNablSpecificationDetails = new CreateNablSpecificationDetails();
  dynamicArray: Array<CreateNablSpecificationDetails> = [];

  modeTypeForMasterInstrumentObj: ModeTypeForMasterInstrument = new ModeTypeForMasterInstrument();
  arrayModeType: Array<any> = [];
  arrayModeTypeListId: Array<any> = [];
  arrayModeTypeListName: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracy: Array<any> = [];
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  tempChartTypeForMasterInstrumentObj: TempChartTypeForMasterInstrument = new TempChartTypeForMasterInstrument();
  arrayOfTempChartType: Array<any> = [];
  arrayTempChartTypeId: Array<any> = [];
  arrayTempChartTypeNames: Array<any> = [];

  arrayOfCalibrationLabType: Array<any> = [];
  arrayOfCalibrationLabTypeName: Array<any> = [];
  arrayOfCalibrationLabTypeId: Array<any> = [];

  groupForNablScopeMasterObj: GroupForNablScopeMaster = new GroupForNablScopeMaster();
  arrayOfGroupForNablScopeMaster: Array<any> = [];
  arrayOfGroupForNablScopeMasterId: Array<any> = [];
  arrayOfGroupForNablScopeMasterNames: Array<any> = [];
  arrayOfCalibrationLabTypeIdForGroup: Array<any> = [];

  NablScopeNameObj: NablScopeName = new NablScopeName();
  arrayOfNablScopeName: Array<any> = [];
  arrayOfNablScopeId: Array<any> = [];
  arrayOfNablScopeNames: Array<any> = [];


  dynamicArrayTrial: Array<any> = [];
  srNoNablSpecifiDetailsArray: Array<any> = [];
  srNoNablSpecifiDetailsArrayByNablIdParaNm: Array<any> = [];
  srNoMasterSpecifiDetailsArrayByMastIdParaNm1: Array<any> = [];

  searchText: String = '';
  selectParameter: String;
  leng: number;
  masterId: any;
  dynamicArray1: any;
  forAccuValue: any;
  w: number;
  previousSelectedParameter: any;
  mi: any;
  status: any;
  srNoToDelete: any;
  indexToDelete: any;
  NablScopemasterId: any;
  calibrationLabParameter: any;

  arrayGroupNameAccorToLab1: Array<any> = [];
  arrayUncFormulaList: Array<any> = [];
  arrayOfUncFormulaNames: Array<any> = [];
  arrayOfUncFormulaId: Array<any> = [];

  //FrequencyUom
  arrayOfFrequencyUomList: Array<any> = [];
  arrayOfFrequencyUomNames: Array<any> = [];
  arrayOfFrequencyUomId: Array<any> = [];
  groupParameter: any;
  nablScopeName: any;
  showFreq: any;
  freq: any;
  arrayOfParameterFrequency: Array<any> = [];
  previousStatus: any;
  forAccuValue1: any;

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService, private createCertificateService: CreateCertificateService, private createNablScopeService: CreateNablScopeService) {
                                                         
  }

  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }

  get formArr() {
    return this.createMasterSpecificationForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      nablScopeId: [],
      parameterNumber: [],
      rangeFrom: [],
      rangeTo: [],
      rangeUom: [],
      freqFrom: [],
      freqFromUom: [],
      freqTo: [],
      freqToUom: [],
      leastCount: [],
      leastCountUom: [],
      uncValue: [],
      uncFormulaId: [],
      uncUom: [],
      tempChartType: [],
      modeType: [],
      amendmentDate: [],
      inhouse: [],
      onsite: [],

    });
  }


  ngAfterViewInit() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null) {
        if (this.dynamicArrayTrial[l].parameterId != 0 && this.dynamicArrayTrial[l].parameterId != "") {   //if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
      }

    }

  }


  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow(index) {

    this.createNablScopeService.getSrNoOfNablSpecifiDetailByNablIdParaName(this.searchNablSpecificationByNablIdPNmAndStatusObj).subscribe(data => {
      console.log(data);
      this.srNoNablSpecifiDetailsArrayByNablIdParaNm = data;
      var index1 = parseInt((this.itemsPerPage * (this.p - 1)) + (index));

      this.srNoToDelete = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(index1) as FormGroup).get('parameterNumber').value;
      for (var i = 0; i < this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length; i++) {
        if (this.srNoNablSpecifiDetailsArrayByNablIdParaNm[i][5] == this.srNoToDelete) {
          this.indexToDelete = this.srNoNablSpecifiDetailsArrayByNablIdParaNm[i][2];
          this.createNablScopeService.deleteNablSpecificationDetail(this.indexToDelete).subscribe(data => {
            console.log("*********deleted row********" + data);
            alert("deleted successfully !!");
          })
          break;
        }
      }

      this.srNoNablSpecifiDetailsArrayByNablIdParaNm.splice(index1, 1);
      this.dynamicArrayTrial.splice(index1, 1);
      this.formArr.removeAt(index1);

    })
  }

  onChange() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    var arrayUomNameAccor = new Array();

    for (var ii = 0, ll = Object.keys(this.ParameterListForReqAndIwdObj).length; ii < ll; ii++) {
      if (this.selectParameter == this.arrayParameterNames[ii]) {
        this.freq = this.arrayOfParameterFrequency[ii];
        if (this.freq == 1) {
          this.showFreq = 1;
        }
        else {
          this.showFreq = 0;
        }

        for (var iii = 0, lll = Object.keys(this.uomListForRequestAndInwardObj).length; iii < lll; iii++) {
          if (this.uomListForRequestAndInwardObj[iii].parameterId == this.arrayParameterId[ii]) {
            arrayUomNameAccor.push(this.uomListForRequestAndInwardObj[iii].uomName)
          }
        }
      }
    }

    this.arrayUomNameAccorToParam1 = arrayUomNameAccor;
    console.log("this.arrayUomNameAccorToParam1 " + this.arrayUomNameAccorToParam1)
    localStorage.setItem('arrayUomNameAccorToParam1ForNabl', JSON.stringify(this.arrayUomNameAccorToParam1));
    this.paraSpecifications();
  }

  onChange1() {
    this.leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
    var arrayGroupNameAccor = new Array();

    for (var ii = 0, ll = Object.keys(this.arrayOfCalibrationLabType).length; ii < ll; ii++) {
      if (this.calibrationLabParameter == this.arrayOfCalibrationLabTypeName[ii]) {

        for (var iii = 0, lll = Object.keys(this.arrayOfGroupForNablScopeMaster).length; iii < lll; iii++) {
          if (this.arrayOfGroupForNablScopeMaster[iii].calibrationLabId == this.arrayOfCalibrationLabTypeId[ii]) {
            arrayGroupNameAccor.push(this.arrayOfGroupForNablScopeMaster[iii].groupName)
          }
        }
      }
    }

    this.arrayGroupNameAccorToLab1 = arrayGroupNameAccor;
    console.log("this.arrayGroupNameAccorToLab1 " + this.arrayGroupNameAccorToLab1)
    localStorage.setItem('arrayGroupNameAccorToLab1', JSON.stringify(this.arrayGroupNameAccorToLab1));

  }
  ngOnInit() {
    this.dynamicArrayTrial = null;
    this.NablScopemasterId = JSON.parse(localStorage.getItem('createNableScopeMasterIdForUpdate'));

    this.groupParameter = JSON.parse(localStorage.getItem('groupId1'));
    this.calibrationLabParameter = JSON.parse(localStorage.getItem('calibrationLabId'));

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        selectParameter: [],
        calibrationLabParameter: [],
        groupParameter: [],
        status: [],
        Rows: this.formBuilder.array([this.initRows()])
      });
    }
    else {

      this.createMasterSpecificationForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        selectParameter: [],
        calibrationLabParameter: [],
        groupParameter: [this.groupParameter],
        status: [],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({
            nablScopeId,
            parameterNumber,
            rangeFrom,
            rangeTo,
            rangeUom,
            freqFrom,
            freqFromUom,
            freqTo,
            freqToUom,
            leastCount,
            leastCountUom,
            uncValue,
            uncFormulaId,
            uncUom,
            tempChartType,
            modeType,
            amendmentDate,
            inhouse,
            onsite
          }) =>
            this.formBuilder.group({

              nablScopeId: [nablScopeId],
              parameterNumber: [parameterNumber],
              rangeFrom: [rangeFrom],
              rangeTo: [rangeTo],
              rangeUom: [rangeUom],
              freqFrom: [freqFrom],
              freqFromUom: [freqFromUom],
              freqTo: [freqTo],
              freqToUom: [freqToUom],
              leastCount: [leastCount],
              leastCountUom: [leastCountUom],
              uncValue: [uncValue],
              uncFormulaId: [uncFormulaId],
              uncUom: [uncUom],
              tempChartType: [tempChartType],
              modeType: [modeType],
              amendmentDate: [amendmentDate],
              inhouse: [inhouse],
              onsite: [onsite]

            })
          )
        )

      })


      this.arrayUomNameAccorToParam2 = JSON.parse(localStorage.getItem('arrayUomNameAccorToParam1ForNabl'));
      if (this.arrayUomNameAccorToParam2 != null || typeof this.arrayUomNameAccorToParam2 != 'undefined') {
        this.arrayUomNameAccorToParam1 = this.arrayUomNameAccorToParam2;
      }

    }

    //get mode list
    this.createCertificateService.getModeTypesListForMaster().subscribe(data => {
      this.arrayModeType = data;
      for (var i = 0, l = Object.keys(this.arrayModeType).length; i < l; i++) {
        this.arrayModeTypeListName.push(this.arrayModeType[i].modeName);
        this.arrayModeTypeListId.push(this.arrayModeType[i].modeId);
      }
    })

    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })

    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
        this.arrayOfParameterFrequency.push(this.ParameterListForReqAndIwdObj[i].frequency);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    });

    //get Temp chart list
    this.createCertificateService.getTempChartTypesListForMaster().subscribe(data => {
      this.tempChartTypeForMasterInstrumentObj = data;
      for (var i = 0, l = Object.keys(this.tempChartTypeForMasterInstrumentObj).length; i < l; i++) {
        this.arrayTempChartTypeNames.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartName);
        this.arrayTempChartTypeId.push(this.tempChartTypeForMasterInstrumentObj[i].tempChartId);
      }
    });

    // get calibration lab type list
    this.createCertificateService.getCalibrationLabListForMaster().subscribe(data => {

      this.arrayOfCalibrationLabType = data;
      console.log(this.arrayOfCalibrationLabType)

      for (var i = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; i < l; i++) {
        this.arrayOfCalibrationLabTypeName.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeName);
        this.arrayOfCalibrationLabTypeId.push(this.arrayOfCalibrationLabType[i].calibrationLabTypeId);
      }


      // get group for nabl scope master
      this.createNablScopeService.getGroupForNablScopeMaster().subscribe(data => {

        this.arrayOfGroupForNablScopeMaster = data;
        console.log(this.arrayOfGroupForNablScopeMaster)

        for (var i = 0, l = Object.keys(this.arrayOfGroupForNablScopeMaster).length; i < l; i++) {
          this.arrayOfGroupForNablScopeMasterNames.push(this.arrayOfGroupForNablScopeMaster[i].groupName);
          this.arrayOfGroupForNablScopeMasterId.push(this.arrayOfGroupForNablScopeMaster[i].groupId);
          this.arrayOfCalibrationLabTypeIdForGroup.push(this.arrayOfGroupForNablScopeMaster[i].calibrationLabId);
        }

        this.onChange1();

      },
        error => console.log(error));


    },
      error => console.log(error));





    // get Nabl scope name
    this.createNablScopeService.getNablScopeName().subscribe(data => {

      this.arrayOfNablScopeName = data;
      console.log(this.arrayOfNablScopeName)

      for (var i = 0, l = Object.keys(this.arrayOfNablScopeName).length; i < l; i++) {
        this.arrayOfNablScopeNames.push(this.arrayOfNablScopeName[i].nablScopeName);
        this.arrayOfNablScopeId.push(this.arrayOfNablScopeName[i].nablScopeNameId);
      }
    },
      error => console.log(error));

    // get unc formula list
    this.createNablScopeService.getUncFormulaList().subscribe(data => {

      this.arrayUncFormulaList = data;
      console.log(this.arrayOfNablScopeName)

      for (var i = 0, l = Object.keys(this.arrayUncFormulaList).length; i < l; i++) {
        this.arrayOfUncFormulaNames.push(this.arrayUncFormulaList[i].uncFormulaName);
        this.arrayOfUncFormulaId.push(this.arrayUncFormulaList[i].uncFormulaId);
      }
    },
      error => console.log(error));

    // get frequency uom list
    this.createNablScopeService.getFrequencyUomList().subscribe(data => {

      this.arrayOfFrequencyUomList = data;
      console.log(this.arrayOfFrequencyUomList)

      for (var i = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; i < l; i++) {
        this.arrayOfFrequencyUomNames.push(this.arrayOfFrequencyUomList[i].freqUomName);
        this.arrayOfFrequencyUomId.push(this.arrayOfFrequencyUomList[i].freqUomId);
      }
    },
      error => console.log(error));


    (this.createMasterSpecificationForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(this.dynamicArrayTrial);
      console.log(values);

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
        for (var i = 0, len = values.length; i < len; i++) {
          if (typeof this.dynamicArrayTrial[i] != 'undefined' && this.dynamicArrayTrial[i] != null && typeof values[i] != 'undefined') {

            {
              if ((this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].rangeUom !== values[i].rangeUom
                || this.dynamicArrayTrial[i].freqFrom !== values[i].freqFrom || this.dynamicArrayTrial[i].freqTo !== values[i].freqTo || this.dynamicArrayTrial[i].freqFromUom !== values[i].freqFromUom || this.dynamicArrayTrial[i].freqToUom !== values[i].freqToUom
                || this.dynamicArrayTrial[i].leastCount !== values[i].leastCount || this.dynamicArrayTrial[i].leastCountUom !== values[i].leastCountUom
                || this.dynamicArrayTrial[i].uncValue !== values[i].uncValue || this.dynamicArrayTrial[i].uncFormulaId !== values[i].uncFormulaId || this.dynamicArrayTrial[i].uncUom !== values[i].uncUom
                || this.dynamicArrayTrial[i].amendmentDate !== values[i].amendmentDate || this.dynamicArrayTrial[i].inhouse !== values[i].inhouse || this.dynamicArrayTrial[i].onsite !== values[i].onsite
                || this.dynamicArrayTrial[i].tempChartType !== values[i].tempChartType || this.dynamicArrayTrial[i].modeType !== values[i].modeType || this.dynamicArrayTrial[i].nablScopeId !== values[i].nablScopeId)
              ) {
                console.log("change" + i);
                var o = this.createMasterSpecificationForm.get('Rows').value[i].active;
                if (document.getElementById("btn-" + i) != null) {
                  document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";
                }
              }
            }
          }
        }
      }
    });

  }

  paraSpecifications() {
    if (this.previousSelectedParameter != this.selectParameter || this.previousStatus != this.status) {

      this.srNoNablSpecifiDetailsArrayByNablIdParaNm = this.srNoNablSpecifiDetailsArrayByNablIdParaNm.splice(0, this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length);
      this.itemsPerPage = 50;
      var nablIdParaName1 = this.NablScopemasterId + "_" + this.selectParameter + "_" + this.status;

      this.searchNablSpecificationByNablIdPNmAndStatusObj.nablIdParaName = nablIdParaName1;
      switch (this.status) {
        case 'Draft': this.searchNablSpecificationByNablIdPNmAndStatusObj.draft = 1;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.archieved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.rejected = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.approved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.submitted = 0;
          break;
        case 'Archieved': this.searchNablSpecificationByNablIdPNmAndStatusObj.draft = 0
          this.searchNablSpecificationByNablIdPNmAndStatusObj.archieved = 1;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.rejected = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.approved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.submitted = 0;
          break;
        case 'Rejected': this.searchNablSpecificationByNablIdPNmAndStatusObj.draft = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.archieved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.rejected = 1;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.approved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.submitted = 0;
          break;
        case 'Approved': this.searchNablSpecificationByNablIdPNmAndStatusObj.draft = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.archieved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.rejected = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.approved = 1;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.submitted = 0;
          break;
        case 'Submitted': this.searchNablSpecificationByNablIdPNmAndStatusObj.draft = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.archieved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.rejected = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.approved = 0;
          this.searchNablSpecificationByNablIdPNmAndStatusObj.submitted = 1;
          break;

      }

      (this.createMasterSpecificationForm.get('Rows') as FormArray).reset();
      var leng = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
      console.log(leng);

      for (let c = 0; c < leng; c++) {
        if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined') {
          this.dynamicArrayTrial.splice(c, 1);
          this.formArr.removeAt(c);
        }

      }


      this.createNablScopeService.getSrNoOfNablSpecifiDetailByNablIdParaName(this.searchNablSpecificationByNablIdPNmAndStatusObj).subscribe(data => {
        console.log(data);
        this.srNoNablSpecifiDetailsArrayByNablIdParaNm = data;
        this.srNoMasterSpecifiDetailsArrayByMastIdParaNm1 = data;

        var leng1 = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;

        if (leng1 == 0) {
          this.addNewRow();
        }
        if (leng1 > 1) {
          while (leng1 > 1) {

            leng1--;
            this.formArr.removeAt(leng1);

          }
        }

        if (this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length != 0) {

          for (this.mi = 0; this.mi < this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length; this.mi++) {
            if (typeof ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup) != 'undefined') {

              if (((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeFrom').value == null) {

                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('parameterNumber').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][5]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('nablScopeId').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][1]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeFrom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][7]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeTo').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][8]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('rangeUom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][9]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqFrom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][10]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqFromUom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][11]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqTo').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][12]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('freqToUom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][13]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('leastCount').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][14]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('leastCountUom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][15]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uncValue').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][16]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uncFormulaId').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][17]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uncUom').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][18]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('tempChartType').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][19]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('modeType').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][20]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('amendmentDate').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][21]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('inhouse').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][22]);
                ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('onsite').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][23]);
                this.addNewRow();

              }
            }
          }
          this.dynamicArrayTrial = this.createMasterSpecificationForm.get('Rows').value;
        }


      })

      this.previousSelectedParameter = this.selectParameter;
      this.previousStatus = this.status;
      this.createCertificateService.getModeTypesListForMaster().subscribe(data => {
        for (let c = 0; c < this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length; c++) {
          if (typeof ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup) != 'undefined') {
            var rangeF = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup).get('rangeFrom').value
            if (rangeF != null && typeof rangeF != 'undefined' && rangeF != "") {

              if (c < this.itemsPerPage)
                document.getElementById("btn-" + c).style.backgroundColor = "#5cb85c";
            }
          }
        }


        if (this.mi >= this.srNoNablSpecifiDetailsArrayByNablIdParaNm.length) {
          var leng2 = (this.createMasterSpecificationForm.get('Rows') as FormArray).length;
          var val = this.mi;
          for (this.mi; this.mi <= leng2;) {
            this.formArr.removeAt(val);
            leng2--;

          }
        }
      })
    }

  }

  getPage(page: number, sort: string = 'asc', order: string = 'created') {
    this.p = page;
    var count = (this.createMasterSpecificationForm.get('Rows') as FormArray).length - (this.itemsPerPage * this.p)

    if (count < 0)
      count = -(count);

    for (let c = 0; c < count; c++) {
      if (((this.createMasterSpecificationForm.get('Rows') as FormArray).at(c) as FormGroup).get('rangeFrom').value != null) {
        document.getElementById("btn-" + c).style.backgroundColor = "#5cb85c";
      }
    }
  }


  //disable functionality
  accuracyValue(i) {
    this.forAccuValue = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uncFormulaId').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = false;
    }

  }

  accuracyValue1(i) {
    this.forAccuValue1 = ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(i) as FormGroup).get('uncUom').value;
    if (this.forAccuValue1 != null && typeof this.forAccuValue1 != 'undefined' && this.forAccuValue1 != "") {
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = false;
    }

  }

  saveData(e) {

    var j = e + 1;
    var index = parseInt((this.itemsPerPage * (this.p - 1)) + (e));
    var srnoCount = parseInt((this.itemsPerPage * (this.p - 1)) + (j));

    this.dynamicArray = this.createMasterSpecificationForm.get('Rows').value;
    ((this.createMasterSpecificationForm.get('Rows') as FormArray).at(index) as FormGroup).get('parameterNumber').patchValue(this.selectParameter + "_" + srnoCount);
    this.dynamicArray = this.createMasterSpecificationForm.get('Rows').value;

    this.createNablSpecificationObj.createNablId = this.NablScopemasterId;
    this.createNablSpecificationObj.tempChartType = this.dynamicArray[index].tempChartType;
    this.createNablSpecificationObj.parameterNumber = this.selectParameter + "_" + srnoCount;
    this.createNablSpecificationObj.nablIdWithParameterName = this.NablScopemasterId + "_" + this.selectParameter + "_" + this.status;


    switch (this.status) {
      case 'Draft': this.createNablSpecificationObj.draft = 1;
        this.createNablSpecificationObj.archieved = 0;
        this.createNablSpecificationObj.rejected = 0;
        this.createNablSpecificationObj.approved = 0;
        this.createNablSpecificationObj.submitted = 0;
        break;
      case 'Archieved': this.createNablSpecificationObj.draft = 0
        this.createNablSpecificationObj.archieved = 1;
        this.createNablSpecificationObj.rejected = 0;
        this.createNablSpecificationObj.approved = 0;
        this.createNablSpecificationObj.submitted = 0;
        break;
      case 'Rejected': this.createNablSpecificationObj.draft = 0;
        this.createNablSpecificationObj.archieved = 0;
        this.createNablSpecificationObj.rejected = 1;
        this.createNablSpecificationObj.approved = 0;
        this.createNablSpecificationObj.submitted = 0;
        break;
      case 'Approved': this.createNablSpecificationObj.draft = 0;
        this.createNablSpecificationObj.archieved = 0;
        this.createNablSpecificationObj.rejected = 0;
        this.createNablSpecificationObj.approved = 1;
        this.createNablSpecificationObj.submitted = 0;
        break;
      case 'Submitted': this.createNablSpecificationObj.draft = 0;
        this.createNablSpecificationObj.archieved = 0;
        this.createNablSpecificationObj.rejected = 0;
        this.createNablSpecificationObj.approved = 0;
        this.createNablSpecificationObj.submitted = 1;
        break;

    }

    //save parameter Id
    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
      if (this.selectParameter == this.arrayParameterNames[k]) {
        this.createNablSpecificationObj.parameterId = this.arrayParameterId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.parameterId = 1;
      }
    }


    //save group Id
    for (var k = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; k < l; k++) {
      if (this.groupParameter == this.arrayOfGroupForNablScopeMasterNames[k]) {
        this.createNablSpecificationObj.groupId = this.arrayOfGroupForNablScopeMasterId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.groupId = 1;
      }
    }

    //save calibration lab Id
    for (var k = 0, l = Object.keys(this.arrayOfCalibrationLabType).length; k < l; k++) {
      if (this.calibrationLabParameter == this.arrayOfCalibrationLabTypeName[k]) {
        this.createNablSpecificationObj.calibrationLabId = this.arrayOfCalibrationLabTypeId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.calibrationLabId = 1;
      }
    }

    //save nabl scope Id
    for (var k = 0, l = Object.keys(this.arrayOfNablScopeName).length; k < l; k++) {
      if (this.dynamicArray[index].nablScopeId == this.arrayOfNablScopeNames[k]) {
        this.createNablSpecificationObj.nablScopeId = this.arrayOfNablScopeId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.nablScopeId = 1;
      }
    }

    this.createNablSpecificationObj.rangeFrom = this.dynamicArray[index].rangeFrom
    this.createNablSpecificationObj.rangeTo = this.dynamicArray[index].rangeTo

    // save range Uom
    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].rangeUom == this.arrayUomName[k]) {
        this.createNablSpecificationObj.rangeUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.rangeUom = 1;
      }
    }


    this.createNablSpecificationObj.freqFrom = this.dynamicArray[index].freqFrom

    // save  frequency from uom
    for (var k = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; k < l; k++) {
      if (this.dynamicArray[index].freqFromUom == this.arrayOfFrequencyUomNames[k]) {
        this.createNablSpecificationObj.freqFromUom = this.arrayOfFrequencyUomId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.freqFromUom = 1;
      }
    }

    this.createNablSpecificationObj.freqTo = this.dynamicArray[index].freqTo

    // save  frequency to uom
    for (var k = 0, l = Object.keys(this.arrayOfFrequencyUomList).length; k < l; k++) {
      if (this.dynamicArray[index].freqToUom == this.arrayOfFrequencyUomNames[k]) {
        this.createNablSpecificationObj.freqToUom = this.arrayOfFrequencyUomId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.freqToUom = 1;
      }
    }

    this.createNablSpecificationObj.uncValue = this.dynamicArray[index].uncValue;

    // save  unc formula
    for (var k = 0, l = Object.keys(this.arrayUncFormulaList).length; k < l; k++) {
      if (this.dynamicArray[index].uncFormulaId == this.arrayOfUncFormulaNames[k]) {
        this.createNablSpecificationObj.uncFormulaId = this.arrayOfUncFormulaId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.uncFormulaId = 1;
      }
    }

    // save  unc uom
    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].uncUom == this.arrayUomName[k]) {
        this.createNablSpecificationObj.uncUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.uncUom = 1;
      }
    }


    this.createNablSpecificationObj.leastCount = this.dynamicArray[index].leastCount

    // save least count  uom
    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[index].leastCountUom == this.arrayUomName[k]) {
        this.createNablSpecificationObj.leastCountUom = this.arrayUomId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.leastCountUom = 1;
      }
    }




    // save  mode type
    for (var k = 0, l = Object.keys(this.arrayModeType).length; k < l; k++) {
      if (this.dynamicArray[index].modeType == this.arrayModeTypeListName[k]) {
        this.createNablSpecificationObj.modeType = this.arrayModeTypeListId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.modeType = 1;
      }
    }
    // save  temp Chart Type
    for (var k = 0, l = Object.keys(this.tempChartTypeForMasterInstrumentObj).length; k < l; k++) {
      if (this.dynamicArray[index].tempChartType == this.arrayTempChartTypeNames[k]) {
        this.createNablSpecificationObj.tempChartType = this.arrayTempChartTypeId[k];
        break;
      }
      else {
        this.createNablSpecificationObj.tempChartType = 1;
      }
    }

    if (this.dynamicArray[index].onsite == 1) {
      this.createNablSpecificationObj.onsite = 1;
    }
    else {
      this.createNablSpecificationObj.onsite = 0;
    }

    if (this.dynamicArray[index].inhouse == 1) {
      this.createNablSpecificationObj.inhouse = 1;
    }
    else {
      this.createNablSpecificationObj.inhouse = 0;
    }


    var amenDate = this.convertStringToDate(this.dynamicArray[index].amendmentDate);
    this.createNablSpecificationObj.amendmentDate = this.datepipe.transform(amenDate, 'dd-MM-yyyy');

    this.createNablScopeService.getSrNoOfNablSpecificationDetail(this.NablScopemasterId).subscribe(data => {
      console.log("sr no : " + data);
      this.srNoNablSpecifiDetailsArray = data;

      this.checkIfSrNoAlreadyPresent();
    })
    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";
  }

  checkIfSrNoAlreadyPresent() {
    if (this.srNoNablSpecifiDetailsArray.length == 0) {
      this.createNablScopeService.createNablSpecificationDetail(this.createNablSpecificationObj).subscribe(data => {
        console.log("Nabl specification details : " + data)
        alert("data saved successfully !")
      })
    }
    else {
      for (this.w = 0; this.w < this.srNoNablSpecifiDetailsArray.length; this.w++) {
        if (this.srNoNablSpecifiDetailsArray[this.w][5] == this.createNablSpecificationObj.parameterNumber && this.srNoNablSpecifiDetailsArray[this.w][28] == this.createNablSpecificationObj.nablIdWithParameterName) {
          this.createNablScopeService.updateNablSpecificationDetail(this.srNoNablSpecifiDetailsArray[this.w][2], this.createNablSpecificationObj).subscribe(data => {
            console.log("Nabl specification details : " + data)
            alert("data updated successfully !");
          })
          break;
        }
      }
    }
    if (this.w >= this.srNoNablSpecifiDetailsArray.length) {
      this.createNablScopeService.createNablSpecificationDetail(this.createNablSpecificationObj).subscribe(data => {
        console.log("Nabl specification details : " + data)
        alert("data saved successfully !")
      })
    }
    this.dynamicArrayTrial = this.dynamicArray;
  }
  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }

}




