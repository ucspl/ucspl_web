import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchNablComponent } from './search-nabl.component';

describe('SearchNablComponent', () => {
  let component: SearchNablComponent;
  let fixture: ComponentFixture<SearchNablComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchNablComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchNablComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
