import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchNablScopeMaster, SearchNablService } from '../../../../services/serviceconnection/search-service/search-nabl.service';

@Component({
  selector: 'app-search-nabl',
  templateUrl: './search-nabl.component.html',
  styleUrls: ['./search-nabl.component.css']
})


export class SearchNablComponent implements OnInit {
  searchNablForm: FormGroup;
  searchNablScopeMasterObject: SearchNablScopeMaster = new SearchNablScopeMaster();
  data: any;
  p: number = 1;
  itemsPerPage: number = 5;
  nablScopeDataArray: Array<any> = [];
  certificateNo: any;
  idForSearchNabl: any;
  constructor(private searchNablService: SearchNablService, private formBuilder: FormBuilder, private router: Router) {
    this.searchNablForm = this.formBuilder.group({
      searchDocument: [],
      itemsPerPage: [],

    });
  }

  ngOnInit() {
  }


  searchNabl() {
    debugger

    this.searchNablScopeMasterObject.searchNablScope = this.searchNablForm.value.searchDocument;
    if (this.searchNablScopeMasterObject.searchNablScope != null) {
      this.searchNablScopeMasterObject.searchNablScope = this.searchNablScopeMasterObject.searchNablScope.trim();
    }

    if (this.searchNablScopeMasterObject.searchNablScope == null) {
      this.searchNablScopeMasterObject.searchNablScope = '';
    }
    this.searchNablService.searchData(this.searchNablScopeMasterObject).subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.showdata();
    })



  }
  showdata() {
    this.searchNablService.showdata().subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.nablScopeDataArray = this.data;


    })
  }

  moveToNablUpdateScreen(i) {

    console.log("index is : " + i.target.innerHTML);
    this.certificateNo = i.target.innerText;
    this.searchNablService.setCertificateNoForUpdateNablScreen(this.certificateNo);
    this.router.navigateByUrl('/nav/updatenabl');

  }
}
