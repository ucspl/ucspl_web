import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNablSpecificationComponent } from './create-nabl-specification.component';

describe('CreateNablSpecificationComponent', () => {
  let component: CreateNablSpecificationComponent;
  let fixture: ComponentFixture<CreateNablSpecificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNablSpecificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNablSpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
