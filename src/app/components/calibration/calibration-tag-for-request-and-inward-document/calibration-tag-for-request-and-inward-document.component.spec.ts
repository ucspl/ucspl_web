import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalibrationTagForRequestAndInwardDocumentComponent } from './calibration-tag-for-request-and-inward-document.component';

describe('CalibrationTagForRequestAndInwardDocumentComponent', () => {
  let component: CalibrationTagForRequestAndInwardDocumentComponent;
  let fixture: ComponentFixture<CalibrationTagForRequestAndInwardDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalibrationTagForRequestAndInwardDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalibrationTagForRequestAndInwardDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
