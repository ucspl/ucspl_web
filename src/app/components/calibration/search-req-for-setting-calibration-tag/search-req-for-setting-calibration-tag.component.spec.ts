import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchReqForSettingCalibrationTagComponent } from './search-req-for-setting-calibration-tag.component';

describe('SearchReqForSettingCalibrationTagComponent', () => {
  let component: SearchReqForSettingCalibrationTagComponent;
  let fixture: ComponentFixture<SearchReqForSettingCalibrationTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchReqForSettingCalibrationTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchReqForSettingCalibrationTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
