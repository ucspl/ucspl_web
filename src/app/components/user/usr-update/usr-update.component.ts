import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../services/config/config.service';
import { Customer, CreateCustomerService } from '../../../services/serviceconnection/create-service/create-customer.service';
import { Branch, Department, Role, SecurityQuestion} from '../../../services/serviceconnection/create-service/create-def.service';
import { CreateUserService, User } from '../../../services/serviceconnection/create-service/create-user.service';
import { SearchUserService } from '../../../services/serviceconnection/search-service/search-user.service';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-usr-update',
  templateUrl: './usr-update.component.html',
  styleUrls: ['./usr-update.component.css']
})
export class UsrUpdateComponent implements OnInit {
  addUserForm: FormGroup;
  countryNm1: string;
  trialPhoneNumber1: any;
  phoneNumberCountryNm1: any;
  branches: Array<any> = [];
  branchesIds: Array<any> = [];
  user: User = new User();
  createUserInfoForSearchSalesObject: any;

  branch: Branch = new Branch();
  branchData: Branch = new Branch();

  departments: Array<any> = [];
  departmentsIds: Array<any> = [];
 // Department: User = new User();
  department: Department = new Department();
  departmentData: Department = new Department();


  customers: Array<any> = [];
  customersIds: Array<any> = [];
  // Customer: User = new User();
  customer: Customer = new Customer();
  customerData: Customer = new Customer();

  roles: Array<any> = [];
  rolesIds: Array<any> = [];
  role: Role = new Role();
  roleData: Role = new Role();

  userForUserName: User = new User();
  usersArray: Array<any> = [];
  usersIds: Array<any> = [];
  userIdLength: number;

  usersUserName: Array<any> = [];
  userNamesLength: number;


  public dateValue = Date.now();
  dateValue1: any;

  submitted = false;
  loginUserName: any;
  userNumber: any;
  idForSearchUser: any;
  firstName: any;
  middleName: any;
  lastName: any;
  gender: any;
  status: any;
  roleName: any;
  departmentName: any;
  branchName: any;
  securityQuestion: any;
  securityAnswer: any;
  phoneNo: any;
  email: any;
  userName: any;
  password: any;
  id: any;
  repassword: any;
  trialPhoneNumber: any;
  modifiedBy: string;
  securityQuestionId:Array<any> = [];
  securityQuestionName:Array<any> = [];
  securityQuestionData: SecurityQuestion =new SecurityQuestion()
  constructor(private formBuilder: FormBuilder, private createUserService: CreateUserService, private router: Router, private serviceForCreateCustomerService: CreateCustomerService, private searchUserService: SearchUserService) {

    this.addUserForm = this.formBuilder.group({
      middleName: [],
      firstName: ['', [Validators.required, ValidationService.nameValidator]],
      lastName: ['', [Validators.required, ValidationService.nameValidator]],
      gender: [],
      status: [],
      customerName: [],
      roleName: [],
      departmentName: [],
      branchName: [],
      email: ['', [Validators.required, ValidationService.emailValidator]],
      phoneNo: [],
      userNumber: [],
      password: ['', [Validators.required, ValidationService.passwordValidator]],
      userName: ['', Validators.required],
      repassword: [],

      securityQuestion: [],
      securityAnswer: [],
    });

  }

  ngOnInit() {


    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.idForSearchUser = this.searchUserService.getUserNumberForCreateUser();
    this.countryNm1 = this.searchUserService.getCustomerCountryForMobileNo1();



// Get Security Question list
this.createUserService.getSecurityQuestionList().subscribe(data => {
  this.securityQuestionData = data;
  console.log( this.securityQuestionData);

  for (var i = 0, l = Object.keys(this.securityQuestionData).length; i < l; i++) {
    this.securityQuestionId.push(this.securityQuestionData[i].securityQuestionId);

    this.securityQuestionName.push(this.securityQuestionData[i].securityQuestionName);

  }

},
  error => console.log(error));

    
    //Get User list
    this.createUserService.getUserList().subscribe(data => {
    
      this.userForUserName = data;
      for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {

        this.usersIds.push(this.userForUserName[i].id);

      }
      this.userIdLength = Object.keys(this.usersIds).length;

    });


 //Get Branch list
    this.createUserService
      .getBranchList().subscribe(data => {
        this.branchData = data;
   
        for (var i = 0, l = Object.keys(this.branchData).length; i < l; i++) {
          this.branches.push(this.branchData[i].branchName);

          this.branchesIds.push(this.branchData[i].branchId);

        }

      },
        error => console.log(error));


 //Get Department list
    this.createUserService
      .getDepartmentList().subscribe(data => {
        this.departmentData = data;

      
        for (var i = 0, l = Object.keys(this.departmentData).length; i < l; i++) {
          this.departments.push(this.departmentData[i].departmentName);

          this.departmentsIds.push(this.departmentData[i].departmentId);

        }

      },
        error => console.log(error));


 //Get Customer list
    this.serviceForCreateCustomerService
      .getCustomerList().subscribe(data => {
        this.customerData = data;

      


        for (var i = 0, l = Object.keys(this.customerData).length; i < l; i++) {
          this.customers.push(this.customerData[i].name);

          this.customersIds.push(this.customerData[i].id);

        }


      },
        error => console.log(error));

 //Get Role list
    this.createUserService
      .getRoleList().subscribe(data => {
        this.roleData = data;
       

        for (var i = 0, l = Object.keys(this.roleData).length; i < l; i++) {
          this.roles.push(this.roleData[i].roleName);
          this.rolesIds.push(this.roleData[i].roleId);

        }

      },
        error => console.log(error));




    $(".toggle-password").click(function () {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    function checkPasswordMatch() {                    // for password matching
      var password = $("#password").val();
      var confirmPassword = $("#repassword").val();
      if (password != confirmPassword) {
        $("#CheckPasswordMatch").html("Passwords does not match!");
        console.log("password does not match!!")
      }
      else
        $("#CheckPasswordMatch").html("");
    }
    $(document).ready(function () {
      $("#repassword").keyup(checkPasswordMatch);
    });


    this.getCreateUserSearchInfo();


    // mobileNumber code

var input1 = document.querySelector("#phoneno");
var country1 = $('#country1');
var iti1 = (<any>window).intlTelInput(input1, {
  // any initialisation options go here
  "preferredCountries": [this.countryNm1],
  "separateDialCode": true
});
//  iti1.setNumber("+917733123456");
var number1 = iti1.getNumber();

var countryData1 = iti1.getSelectedCountryData();


//  this.trialPhoneNumber1 = 91;

input1.addEventListener('countrychange', (e) => {
  // change the hidden input value to the selected country code
  var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
  console.log(a1);
  this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
  document.getElementById("country1").innerText = this.trialPhoneNumber1;
  this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
  console.log(this.trialPhoneNumber1);
});


  }

  change() {
    var dot = new String(".");
    var firstName = this.addUserForm.value.firstName.toString();
    var lastName = this.addUserForm.value.lastName.toString();
    dot = dot.concat(lastName.toString());
    this.user.userName = firstName.concat(dot);
  }

  userNameValid() {
    this.createUserService.getUserList().subscribe(data => {
      console.log(data);
      this.userForUserName = data;
      for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {


        this.usersUserName.push(this.userForUserName[i].userName);

      }
      this.userNamesLength = Object.keys(this.usersUserName).length;


      console.log("usersUserName" + this.usersUserName)

      for (var i = 0; i < this.userNamesLength; i++) {
        if (this.usersUserName[i] == this.user.userName) {
          $("#userNameValidation").html("User Name already exist please enter distinct user name!!!");
          break;
        }
        else {
          $("#userNameValidation").html(" ");

        }

      }

    });
  }



  spacesNotAllowed() {
    if ( typeof this.securityAnswer != 'undefined') {
    if (!this.securityAnswer.trim()) {

      $("#spaceNotAllowed").html("space no allowed!!!");

    }
    else {
      $("#spaceNotAllowed").html(" ");

    }
  }
  }


  getCreateUserSearchInfo() {

    if (typeof this.idForSearchUser != 'undefined') {


      this.createUserService.sendUserNoForGettingCreateUserDetails(this.idForSearchUser).subscribe(data => {
      
        this.createUserInfoForSearchSalesObject = data;

        if (typeof this.createUserInfoForSearchSalesObject != 'undefined') {

          this.convertIdToName();

          this.firstName = this.createUserInfoForSearchSalesObject.firstName;
          this.middleName = this.createUserInfoForSearchSalesObject.middleName;
          this.lastName = this.createUserInfoForSearchSalesObject.lastName;
          this.gender = this.createUserInfoForSearchSalesObject.gender;
          this.status = this.createUserInfoForSearchSalesObject.status;
         // this.securityQuestion = this.createUserInfoForSearchSalesObject.securityQuestion;
          this.securityAnswer = this.createUserInfoForSearchSalesObject.securityAnswer;
         // this.phoneNo = this.createUserInfoForSearchSalesObject.phoneNo;
          this.email = this.createUserInfoForSearchSalesObject.email;
          this.userNumber = this.createUserInfoForSearchSalesObject.userNumber;
          this.userName = this.createUserInfoForSearchSalesObject.userName;
          this.password = this.createUserInfoForSearchSalesObject.password;
          this.repassword = this.createUserInfoForSearchSalesObject.password;
          this.dateValue1 = this.createUserInfoForSearchSalesObject.dateCreated;
          this.id = this.createUserInfoForSearchSalesObject.id;



        this.phoneNo = this.createUserInfoForSearchSalesObject.phoneNo.slice(-10);
    var mb1 = new String(this.createUserInfoForSearchSalesObject.phoneNo)
    this.trialPhoneNumber1 = this.createUserInfoForSearchSalesObject.phoneNo.slice(-mb1.length + 1, -10);
    console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

          // this.customer.countryPrefixForMobile1 = this.custObject.countryPrefixForMobile1

        }
      })

    }

  }

  convertIdToName() {
    for (var i = 0, l = Object.keys(this.branchData).length; i < l; i++) {
      if (this.branchData[i].branchId == this.createUserInfoForSearchSalesObject.selectBranch) {
        this.branchName = this.branchData[i].branchName;

      }

    }

    for (var i = 0, l = Object.keys(this.customerData).length; i < l; i++) {
      if (this.customerData[i].id == this.createUserInfoForSearchSalesObject.customer) {
        this.customer = this.customerData[i].name;

      }

    }

    for (var i = 0, l = Object.keys(this.departmentData).length; i < l; i++) {
      if (this.departmentData[i].departmentId == this.createUserInfoForSearchSalesObject.department) {
        this.departmentName = this.departmentData[i].departmentName;

      }

    }

    for (var i = 0, l = Object.keys(this.roleData).length; i < l; i++) {
      if (this.roleData[i].roleId == this.createUserInfoForSearchSalesObject.role) {
        this.roleName = this.roleData[i].roleName;


      }

    }

    for (var i = 0, l = Object.keys(this.securityQuestionData).length; i < l; i++) {

      if(this.securityQuestionData[i].securityQuestionId==this.createUserInfoForSearchSalesObject.securityQuestionId)
      
      this.securityQuestion=this.securityQuestionData[i].securityQuestionName;
  
    }

  }

  onSubmit() {

    for (var i = 0, l = Object.keys(this.branchData).length; i < l; i++) {
      if (this.branchData[i].branchName == this.addUserForm.value.branchName) {
        this.user.selectBranch = this.branchData[i].branchId;
      
      }

    }

    for (var i = 0, l = Object.keys(this.customerData).length; i < l; i++) {
      if (this.customerData[i].name == this.addUserForm.value.customerName) {
        this.user.customer = this.customerData[i].id;
       
      }

    }

    for (var i = 0, l = Object.keys(this.departmentData).length; i < l; i++) {
      if (this.departmentData[i].departmentName == this.addUserForm.value.departmentName) {
        this.user.department = this.departmentData[i].departmentId;
        
      }

    }

    for (var i = 0, l = Object.keys(this.roleData).length; i < l; i++) {
      if (this.roleData[i].roleName == this.addUserForm.value.roleName) {
      }
        this.user.role = this.roleData[i].roleId;
       
      }
      for (var i = 0, l = Object.keys(this.securityQuestionData).length; i < l; i++) {
        if(this.securityQuestionData[i].securityQuestionName==this.addUserForm.value.securityQuestion){
        this.user.securityQuestionId=this.securityQuestionData[i].securityQuestionId;
  
      }
    }
    //this.user.phoneNo="+" + this.trialPhoneNumber+this.user.phoneNo;
    this.createUserInfoForSearchSalesObject.firstName = this.firstName;
    this.createUserInfoForSearchSalesObject.middleName = this.middleName;
    this.createUserInfoForSearchSalesObject.lastName = this.lastName;
    this.createUserInfoForSearchSalesObject.gender = this.gender;
    this.createUserInfoForSearchSalesObject.status = this.status;
    this.createUserInfoForSearchSalesObject.securityQuestionId = this.user.securityQuestionId;
    this.createUserInfoForSearchSalesObject.securityAnswer = this.securityAnswer;
    this.createUserInfoForSearchSalesObject.phoneNo = this.phoneNo;
    this.createUserInfoForSearchSalesObject.email = this.email;
    this.createUserInfoForSearchSalesObject.userNumber = this.userNumber;
    this.createUserInfoForSearchSalesObject.userName = this.userName;
    this.createUserInfoForSearchSalesObject.password = this.password;
    this.createUserInfoForSearchSalesObject.dateCreated = this.dateValue1;
    this.createUserInfoForSearchSalesObject.selectBranch = this.user.selectBranch;
    this.createUserInfoForSearchSalesObject.customer = this.user.customer;
    this.createUserInfoForSearchSalesObject.department = this.user.department;
    this.createUserInfoForSearchSalesObject.role = this.user.role;
    this.createUserInfoForSearchSalesObject.modifiedBy = this.loginUserName;


     //save Mobile number with country code
     if( typeof this.phoneNo != 'undefined')
     {
       this.createUserInfoForSearchSalesObject.phoneNo = "+" + this.trialPhoneNumber1 + this.phoneNo;
      
     }
     else{
        this.createUserInfoForSearchSalesObject.phoneNo =null;
     }
 
     this.createUserInfoForSearchSalesObject.countryPrefixForMobile = this.phoneNumberCountryNm1;

    alert("Data updated successfully!!!");
    this.createUserService.updateUser(this.id, this.createUserInfoForSearchSalesObject).subscribe(data => {
     
    })
   // this.router.navigateByUrl('/nav/home');
  }
  
  backToMenu(){
    this.router.navigateByUrl('/nav/usrsearch');
  }

}



