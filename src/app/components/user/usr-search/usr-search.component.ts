
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../services/config/config.service';
import { User } from '../../../services/serviceconnection/create-service/create-user.service';
import { SearchUserService, UserByName } from '../../../services/serviceconnection/search-service/search-user.service';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-usr-search',
  templateUrl: './usr-search.component.html',
  styleUrls: ['./usr-search.component.css']
})
export class UsrSearchComponent implements OnInit {

  searchUserForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  searchUserData: UserByName = new UserByName();
  model: any = {};
  emp: any;

  userObject:any;
  user: User = new User();
  userForUserName: User = new User();
  usersArray: Array<any> = [];
  usersFirstName: Array<any> = [];
  usersLastName: Array<any> = [];
  usersUserName: Array<any> = [];
  usersUserNumber: Array<any> = [];
  usersRoleName: Array<any> = [];
  usersDepartmentName: Array<any> = [];

  usersIds: Array<any> = [];
  userIdLength: number;
  userData: any;

  firstName: any;
  middleName: any;
  gender: any;
  userNo: any;
  idForSearchUser: Object;
  status:any;

  constructor(private formBuilder: FormBuilder, private router: Router, private searchUserService: SearchUserService) {

    this.searchUserForm = this.formBuilder.group({


      name: ['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      status: [],
      department: ['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      itemsPerPage: []
    })
  }

  ngOnInit() {
this.status="1";
  }


  showData() {
    this.searchUserService.showData().subscribe((res: any) => {
      this.userData = res;
    
      this.listOfUsers();

    })
  }
  searchData() {
    debugger;

    this.searchUserData = this.searchUserForm.value;

    
     if(this.searchUserData.name == "" &&  this.searchUserData.department == ""){
    alert("Please enter criteria to search user");
   }
   else{
   this.searchUserService.searchData(this.searchUserData).subscribe((res: any) => {

      this.emp = res;
    
    })
    this.showData();
  }

  }

  listOfUsers() {

    this.searchUserService
      .showData().subscribe(data => {

        this.usersArray = data;
      

        for (var i = 0; i < Object.keys(this.usersArray).length; i++) {
          if (this.usersArray[i][5] == 0) {
            this.usersArray[i][5] = "Inactive";
          }
          else
            this.usersArray[i][5] = "Active";
        }


      },
        error => console.log(error));
  }

  userNumberToUpdateUser(i) {
  
    this.userNo = i.target.innerText;

    this.userNo=parseInt(this.userNo);

    for(var s=0; s<this.userData.length; s++){
      if(this.userData[s][3]==this.userNo)
      {
        this.searchUserService.setUserNumberForCreateUser(this.userData[s][0]);
        this.idForSearchUser=this.userData[s][0];
        break;
      }
    }
   //this.searchCustomerService.setCustomerCountryForMobileNo1( this.custObject.countryPrefixForMobile1);

// get user details for id

  this.searchUserService.sendUserNoForGettingCreateUserDetails(this.idForSearchUser).subscribe(data => {
  this.userObject = data;
  console.log(this.userObject);
  this.searchUserService.setCustomerCountryForMobileNo1( this.userObject.countryPrefixForMobile);
  
  this.router.navigateByUrl('/nav/usrupdate')
},
  error => console.log(error));
  
  }

}
