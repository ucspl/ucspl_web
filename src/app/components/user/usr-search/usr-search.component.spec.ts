import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrSearchComponent } from './usr-search.component';

describe('UsrSearchComponent', () => {
  let component: UsrSearchComponent;
  let fixture: ComponentFixture<UsrSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
