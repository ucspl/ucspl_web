import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrManageComponent } from './usr-manage.component';

describe('UsrManageComponent', () => {
  let component: UsrManageComponent;
  let fixture: ComponentFixture<UsrManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
