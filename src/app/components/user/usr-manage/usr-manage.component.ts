
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
//import * as jsPDF from 'jspdf';
//import * as jspdf from 'jspdf';  
//import html2canvas from 'html2canvas';
import { Router } from '@angular/router';
import { MngApproversService } from '../../../services/serviceconnection/create-service/mng-approvers.service';
import { DocumentType,CreateUserService, User } from '../../../services/serviceconnection/create-service/create-user.service';


//let jsPDF:any;
//const doc = new jspdf.jsPDF();
@Component({
  selector: 'app-usr-manage',
  templateUrl: './usr-manage.component.html',
  styleUrls: ['./usr-manage.component.css']
})

export class UsrManageComponent implements OnInit {

  SearchUserform: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  document: DocumentType = new DocumentType();
  document1: DocumentType = new DocumentType();
  documents: Array<any> = [];
  documentsids: Array<any> = [];

  emp: Array<any> = [];
  emp1: Array<any> = [];
  emp2: any;
  printdocid: any;

  usersarray: Array<any> = [];
  usersarray1: Array<any> = [];

  usersarray_addusers_table: Array<any> = [];
  UserNumberAndDocumentId: Array<any> = [];
  documentnamebyid: any;
  content: any;


  constructor(private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService, private mngApproversService: MngApproversService) {

    this.SearchUserform = this.formBuilder.group({


      //   name: ['', [Validators.required,ValidationService.nameValidator]],
      //   status:[],
      documentname: [],
      itemsPerPage: []
    })
  }

  ngOnInit() {








    this.createUserService
      .getDocumentList().subscribe(data => {
        this.document1 = data;
        // this.document1.push(data);
        console.log(this.document1);      //object of DocumentType


        for (var i = 0, l = Object.keys(this.document1).length; i < l; i++) {
          this.documents.push(this.document1[i].documentName);      // arrays of documentname

          this.documentsids.push(this.document1[i].documentId);

        }
        console.log(this.documents);
        console.log(this.documentsids);
      },
        error => console.log(error));



  }



  deleteUser(value) {
    alert("user removed from existing table");

    const index: number = this.usersarray.indexOf(value);
    this.usersarray1.push(value);
    this.usersarray.splice(index, 1);

    console.log(this.usersarray1);

    this.UserNumberAndDocumentId.push(value[2]);
    this.UserNumberAndDocumentId.push(this.documentnamebyid);

    this.mngApproversService.RemoveUserFromExistingTable(this.UserNumberAndDocumentId).subscribe((res) => {
      this.printdocid = res;
      console.log(this.printdocid);



    })

    this.UserNumberAndDocumentId.splice(0, this.UserNumberAndDocumentId.length);

  }
  addUser(value) {
    alert("user added successfully");

    const index: number = this.usersarray1.indexOf(value);
    //   this.myArray.splice(index, 1);
    //   this.details.push(value);
    //   this.records.splice(index,1);
    this.usersarray.push(value);
    this.usersarray1.splice(index, 1);
    //   this.records.splice(this.records.indexOf(value),1);

    this.UserNumberAndDocumentId.push(value[2]);
    this.UserNumberAndDocumentId.push(this.documentnamebyid);

    this.mngApproversService.AddUserInExistingTable(this.UserNumberAndDocumentId).subscribe((res) => {
      this.printdocid = res;
      console.log(this.printdocid);

    })
    this.UserNumberAndDocumentId.splice(0, this.UserNumberAndDocumentId.length);
  }


  showdata() {
    this.mngApproversService.showdata().subscribe((res) => {
      this.emp = res;
      console.log(this.emp);
      this.list_of_users();

    })



  }

  searchdata(event) {
    debugger;
    //  this.SearchUserform.value.departmentname=" ";
    //   this.userbyname= this.SearchUserform.value;
    this.documentnamebyid = event
    this.mngApproversService.searchdata(this.documentnamebyid).subscribe((res: any) => {

      this.emp2 = res;
      console.log(this.emp2);
    })

    this.showdata();
  }

  list_of_users() {

    this.mngApproversService.showdata_adduser_table().subscribe((res) => {
      this.emp1 = res;
      console.log(this.emp1);


    })
    this.usersarray = this.emp;
    console.log("...trial" + this.usersarray);

    this.usersarray1 = this.emp1;
    console.log("...Add user table" + this.usersarray1);

    this.usersarray_addusers_table = this.arrayDiff(this.usersarray1, this.usersarray)
    console.log("...Add user table......." + this.usersarray_addusers_table);

    if (typeof this.documentnamebyid === 'undefined') {
      this.usersarray_addusers_table = null;
      // color is undefined
    }
  }


  arrayDiff(a, b) {
    //  const finalarray =[];
    //  let difference = a.filter(x => !b.includes(x));

    //console.log(difference);

    //return difference;



    for (var i = 0; i < a.length; i++) {
      for (var j = 0; j < b.length; j++) {
        if (b[j][2] == a[i][2]) {
          //   b.splice(j,1);
          a.splice(i, 1);

        }
      }
    }

    return a;



  }
  /*
 public convetToPDF()
 {
 
 
 var data = document.getElementById('contentToConvert');
 html2canvas(data).then(canvas => {
  
 // Few necessary setting options
 var imgWidth = 208;
 var pageHeight = 295;
 var imgHeight = canvas.height * imgWidth / canvas.width;
 var heightLeft = imgHeight;
  
 const contentDataURL = canvas.toDataURL('image/png')
 const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
 var position = 0;
 pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
 pdf.save('new-file.pdf'); // Generated PDF
 });
 }
 */

  printPage() {
    window.print();
  }

}









