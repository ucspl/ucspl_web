import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrCreateComponent } from './usr-create.component';

describe('UsrCreateComponent', () => {
  let component: UsrCreateComponent;
  let fixture: ComponentFixture<UsrCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
