import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../services/config/config.service';
import { Customer, CreateCustomerService } from '../../../services/serviceconnection/create-service/create-customer.service';
import { Branch, Department, Role, SecurityQuestion} from '../../../services/serviceconnection/create-service/create-def.service';
import { SearchUserService } from '../../../services/serviceconnection/search-service/search-user.service';
import {CreateUserService, User } from '../../../services/serviceconnection/create-service/create-user.service';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-usr-create',
  templateUrl: './usr-create.component.html',
  styleUrls: ['./usr-create.component.css']
  // providers:[DatePipe]
})
export class UsrCreateComponent implements OnInit {

  //   branches = ['Pune', 'Mumbai', 'Kolhapur','Nagpur'];

  addUserForm: FormGroup;


  branches: Array<any> = [];
  branchesIds: Array<any> = [];
  securityQuestionId:Array<any> = [];
  securityQuestionName:Array<any> = [];

  user: User = new User();


  branch: Branch = new Branch();
  branchData: Branch = new Branch();
  securityQuestionData: SecurityQuestion =new SecurityQuestion()

  departments: Array<any> = [];
  departmentsIds: Array<any> = [];
  //Department: User = new User();
  department: Department = new Department();
  departmentData: Department = new Department();


  customers: Array<any> = [];
  customersIds: Array<any> = [];
 // Customer: User = new User();
  customer: Customer = new Customer();
  customerData: Customer = new Customer();

  roles: Array<any> = [];
  rolesIds: Array<any> = [];
  role: Role = new Role();
  roleData: Role = new Role();

  userForUserName: User = new User();
  usersArray: Array<any> = [];
  usersIds: Array<any> = [];
  userIdLength: number;

  usersUserName: Array<any> = [];
  userNamesLength: number;


  public dateValue1 = Date.now();



  submitted = false;
  loginUserName: any;
  userNumber: any;
  detailsForUserNumber: any;
  trialPhoneNumber: any;
  phoneNo: string;
  phoneNumberCountryNm1:any;
  trialPhoneNumber1: any;



  constructor(private formBuilder: FormBuilder, private createUserService: CreateUserService, private router: Router, private serviceForCreateCustomerService: CreateCustomerService, private searchUserService: SearchUserService) {

    this.addUserForm = this.formBuilder.group({
      middleName: ['', [optionalValidator([Validators.required,Validators.maxLength(60), ValidationService.nameValidator])]],
      firstName: ['', [Validators.required, Validators.maxLength(60),ValidationService.nameValidator]],

      lastName: ['', [Validators.required, Validators.maxLength(60),ValidationService.nameValidator]],
      gender: [],
      status: [],
      customerName: [],
      roleName: [],
      departmentName: [],
      branchName: [],
      email: ['', [Validators.required, ValidationService.emailValidator]],
      phoneNo: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      userNumber: [],
      password: ['', [Validators.required, ValidationService.passwordValidator]],
      userName: ['', Validators.required],
      repassword: [],

      securityQuestion: [],
      securityAnswer: [],
    });


  }

  ngOnInit() {
     
    this.user.status=1;
    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    // Get User list
    this.createUserService.getUserList().subscribe(data => {
      this.userForUserName = data;
      for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {


        this.usersIds.push(this.userForUserName[i].userNumber);

      }
      this.userIdLength = Object.keys(this.usersIds).length;

      this.user.userNumber = this.usersIds[this.userIdLength - 1] + 1;

    });

    // Get Branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branchData = data;


      for (var i = 0, l = Object.keys(this.branchData).length; i < l; i++) {
        this.branches.push(this.branchData[i].branchName);

        this.branchesIds.push(this.branchData[i].branchId);

      }

    },
      error => console.log(error));

// Get Security Question list
this.createUserService.getSecurityQuestionList().subscribe(data => {
  this.securityQuestionData = data;
  console.log( this.securityQuestionData);

  for (var i = 0, l = Object.keys(this.securityQuestionData).length; i < l; i++) {
    this.securityQuestionId.push(this.securityQuestionData[i].securityQuestionId);

    this.securityQuestionName.push(this.securityQuestionData[i].securityQuestionName);

  }

},
  error => console.log(error));


    // Get department list
    this.createUserService.getDepartmentList().subscribe(data => {
      this.departmentData = data;

      for (var i = 0, l = Object.keys(this.departmentData).length; i < l; i++) {
        this.departments.push(this.departmentData[i].departmentName);

        this.departmentsIds.push(this.departmentData[i].departmentId);
      }

    },
      error => console.log(error));


    // Get Customer list
    this.serviceForCreateCustomerService.getCustomerList().subscribe(data => {
      this.customerData = data;

      for (var i = 0, l = Object.keys(this.customerData).length; i < l; i++) {
        this.customers.push(this.customerData[i].name);

        this.customersIds.push(this.customerData[i].id);

      }

    },
      error => console.log(error));

   // Get Role list
    this.createUserService
      .getRoleList().subscribe(data => {
        this.roleData = data;

        for (var i = 0, l = Object.keys(this.roleData).length; i < l; i++) {
          this.roles.push(this.roleData[i].roleName);
          this.rolesIds.push(this.roleData[i].roleId);

        }

      },
        error => console.log(error));



    $(".toggle-password").click(function () {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    function checkPasswordMatch() {                    // for password matching
      var password = $("#password").val();
      var confirmPassword = $("#repassword").val();
      if (password != confirmPassword) {
        $("#CheckPasswordMatch").html("Passwords does not match!");
        console.log("password does not match!!")
      }
      else
        $("#CheckPasswordMatch").html("");
    }
    $(document).ready(function () {
      $("#repassword").keyup(checkPasswordMatch);
    });


// mobileNumber code

var input1 = document.querySelector("#phoneNo");
var country1 = $('#country1');
var iti1 = (<any>window).intlTelInput(input1, {
  // any initialisation options go here
  "preferredCountries": ["in"],
  "separateDialCode": true
});
iti1.setNumber("+917733123456");
var number1 = iti1.getNumber();
var countryData1 = iti1.getSelectedCountryData();


this.trialPhoneNumber1 = 91;
this.phoneNumberCountryNm1 = "in";
input1.addEventListener('countrychange', (e) => {
  // change the hidden input value to the selected country code
  var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
 
  this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
  
      
  //  this.prefixCountryCode1=this.trialp1
  document.getElementById("country1").innerText = this.trialPhoneNumber1;

});

  }

  save() {


    for (var i = 0, l = Object.keys(this.branchData).length; i < l; i++) {
      if (this.branchData[i].branchName == this.addUserForm.value.branchName) {
        this.user.selectBranch = this.branchData[i].branchId;
      
      }

    }

    for (var i = 0, l = Object.keys(this.customerData).length; i < l; i++) {
      if (this.customerData[i].name == this.addUserForm.value.customerName) {

        this.user.customer = this.customerData[i].id;
        
      }

    }


    for (var i = 0, l = Object.keys(this.departmentData).length; i < l; i++) {
      if (this.departmentData[i].departmentName == this.addUserForm.value.departmentName) {
        this.user.department = this.departmentData[i].departmentId;
       
      }

    }

    for (var i = 0, l = Object.keys(this.roleData).length; i < l; i++) {
      if (this.roleData[i].roleName == this.addUserForm.value.roleName) {
        this.user.role = this.roleData[i].roleId;
      
      }
      
    }

    for (var i = 0, l = Object.keys(this.securityQuestionData).length; i < l; i++) {
      if (this.securityQuestionData[i].securityQuestionName == this.addUserForm.value.securityQuestion) {
        this.user.securityQuestionId = this.securityQuestionData[i].securityQuestionId;
      
      }
    }
    this.user.createdBy = this.loginUserName;
    this.user.userName = this.user.userName.toLowerCase();
    this.user.dateCreated = this.dateValue1;

     //save Mobile number with country code
    if( typeof this.phoneNo != 'undefined')
    {
      this.user.phoneNo = "+" + this.trialPhoneNumber1 + this.phoneNo;
     
    }
    else{
       this.user.phoneNo =null;
    }

    this.user.countryPrefixForMobile = this.phoneNumberCountryNm1;

    //Save the data
    this.createUserService.createUser(this.user).subscribe(data => {
     
    },
      error => console.log(error));

    this.windowReload();

  }
  windowReload() {
    window.location.reload();

  }

  onSubmit() {
    this.submitted = true;

    alert("User created Successfully");
    this.save();
  }

  change() {
    var dot = ".";

    if(typeof this.user.firstName != 'undefined' && typeof this.user.lastName != 'undefined')
    {
      var firstName = this.user.firstName.toString();
      var lastName = this.user.lastName.toString();
      dot = dot.concat(lastName.toString());
      this.user.userName = this.user.firstName.concat(dot);
    }
   
  }

  userNameValid() {
    this.createUserService.getUserList().subscribe(data => {
   
      this.userForUserName = data;
      for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {


        this.usersUserName.push(this.userForUserName[i].userName);

      }
      this.userNamesLength = Object.keys(this.usersUserName).length;


      for (var i = 0; i < this.userNamesLength; i++) {
        if (this.usersUserName[i].toLowerCase() == this.user.userName.toLowerCase()) {
          $("#userNameValidation").html("User Name already exist please edit user name!!!");
          break;
        }
        else {
          $("#userNameValidation").html(" ");

        }

      }


    });
  }

  spacesNotAllowed() {
   

    if (!this.user.securityAnswer.trim()) {

      $("#spaceNotAllowed").html("Space not allowed as Answer!!!");

    }
    else {
      $("#spaceNotAllowed").html(" ");

    }
  }


 /* customerNameValid() {

    for (var i = 0; i < this.customers.length; i++) {
      if (this.customers[i] == this.user.customer) {
        $("#customerNameValidation").html(" ");
        break;
      }
      else {
        $("#customerNameValidation").html("customer name does not exist in the list ");

      }
    }
  } */
  
 
}
