import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { StudentService } from '../../../services/student/student.service';
//import { RegistrationComponent } from '../../registration/registration.component';

@Component({
  selector: 'app-register-details',
  templateUrl: './register-details.component.html',
  styleUrls: ['./register-details.component.css']
})
export class RegisterDetailsComponent implements OnInit {
  //data : JSON.parse(localStorage.getItem('students'))

  getStudentDetail: any;                                  //first i wrote formgroup but it's giving an error so I took any datatype now its working fine
  firstname:string;lastname:string;middlename:string;
  address:string;gender:string;birthdate:string;
  course:string;email:string;phoneno:string;
  username:string;password:string;

  constructor( private studentService:StudentService) {
  
//    this.getRegistrationInfo();
     this.getStudentDetail = this.studentService.getRegistrationInfo(); 

     this.firstname=this.getStudentDetail.firstName;
     this.middlename=this.getStudentDetail.middleName;
     this.lastname=this.getStudentDetail.lastName;

     this.birthdate=this.getStudentDetail.birthdate;
     this.gender=this.getStudentDetail.gender;
     this.email=this.getStudentDetail.email;
     this.phoneno=this.getStudentDetail.phoneno;

     this.address=this.getStudentDetail.address;
     this.course=this.getStudentDetail.course;
     this.username=this.getStudentDetail.username;
     this.password=this.getStudentDetail.password;
     
//   this.firstname=this.getStudentDetail.firstname;
    

     

   }

  ngOnInit() {
  }

}
