import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../services/config/config.service';
import { StudentService } from '../../services/student/student.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {
  validateForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) {
    this.validateForm = this.formBuilder.group({
      firstName: ['', [Validators.required,ValidationService.nameValidator]],
      middleName:['', [Validators.required,ValidationService.nameValidator]],
      lastName: ['', [Validators.required,ValidationService.nameValidator]],
      Username: ['', Validators.required],
      PANnumber:['', [Validators.required,ValidationService.panNumberValidator]],
      GSTnumber:['', [Validators.required,ValidationService.GSTValidator]],
      Aadharnumber:['', [Validators.required,ValidationService.aadharNoValidator]],
      address:[],
      email: ['',  [Validators.required, ValidationService.emailValidator]],
      phoneno: ['',  [Validators.required, ValidationService.phoneValidator]],
      Password: ['', [Validators.required, ValidationService.passwordValidator]],
      emailPassword: ['', [Validators.required, ValidationService.passwordValidator]]
  });
   }

  ngOnInit() {
  }

}
