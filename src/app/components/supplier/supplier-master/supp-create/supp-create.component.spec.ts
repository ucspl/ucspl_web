import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppCreateComponent } from './supp-create.component';

describe('SuppCreateComponent', () => {
  let component: SuppCreateComponent;
  let fixture: ComponentFixture<SuppCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuppCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
