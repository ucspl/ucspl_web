import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { Country, CreateCustomerService, States } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateSupplierService, Supplier} from '../../../../services/serviceconnection/create-service/create-supplier.service';
import {  CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';


import { Branch,  Department, Role, SupplierType } from '../../../../services/serviceconnection/create-service/create-def.service';
import { StudentService } from '../../../../services/student/student.service';

declare var $: any;

@Component({
  selector: 'app-supp-create',
  templateUrl: './supp-create.component.html',
  styleUrls: ['./supp-create.component.css']
})
export class SuppCreateComponent implements OnInit {

  AddSupplierform: FormGroup;
  selectCountryid;
  supplier: Supplier = new Supplier();
  supplierObj: Supplier = new Supplier();
  supplierData: any;

  dateString = '';
  format = 'dd/MM/yyyy';

  name: string;
  primaryContact: string;
  secondaryContact: string;
//  phoneNo: string;
  primaryPhoneNo: string;
  secondaryPhoneNo: string;
  supplierType: string;
  email: string;
  approvalStatus: string;
  status: number;
  country: string;
  addressLine1: string;
  addressLine2: string;
  state: string;
  city: string;
  code: string;

  panNo: string;
  gstNo1: string;
  gstNo2: string;
  gstNo3: string;


  branches: Array<any> = [];
  branchesIds: Array<any> = [];
  branch: Branch = new Branch();
  branch1: Branch = new Branch();

  departments: Array<any> = [];
  departmentsIds: Array<any> = [];
  department: Department = new Department();
  department1: Department = new Department();

  roles: Array<any> = [];
  rolesIds: Array<any> = [];
  role: Role = new Role();
  role1: Role = new Role();

  supplierTypeObj: SupplierType = new SupplierType();
  arraySupplierTypeName: Array<any> = [];
  arraySupplierTypeId: Array<any> = [];
  arraySuppliers: Array<any> = [];
  supplierGST: Array<any> = [];
  static suppliersName: Array<any> = [];
  suppliersId: Array<any> = [];

  countries: Country = new Country();
  arrayCountryName: Array<any> = [];
  arrayCountryId: Array<any> = [];

  statesData: States = new States();
  arrayStatesName: Array<any> = [];
  arrayStatesId: Array<any> = [];
  arrayStatesCode: Array<any> = [];
  arrState: Array<{ id: number, nm: string }> = [];
  arrayStatesCountryId: Array<any> = [];
  arraySuppliersEmail: Array<any> = [];

  countryList: any[] = [
    { name: '--Select Country--', states: [] },
    //  { name: 'India', states: [{ id: 1, nm: 'Andhra Pradesh' }, { id: 2, nm: 'Arunachal Pradesh' }, { id: 3, nm: 'Assam' }, { id: 4, nm: 'Bihar' }, { id: 5, nm: 'Chhattisgarh' }, { id: 6, nm: 'Goa' }, { id: 7, nm: 'Gujarat' }, { id: 8, nm: 'Haryana' }, { id: 9, nm: 'Himachal Pradesh' }, { id: 10, nm: 'Jammu and Kashmir' }, { id: 11, nm: 'Jharkhand' }, { id: 12, nm: 'Karnataka' }, { id: 13, nm: 'Kerala' }, { id: 14, nm: 'Ladakh' }, { id: 15, nm: 'Madhya Pradesh' }, { id: 16, nm: 'Maharashtra' }, { id: 17, nm: 'Manipur' }, { id: 18, nm: 'Meghalaya' }, { id: 19, nm: 'Mizoram' }, { id: 20, nm: 'Nagaland' }, { id: 21, nm: 'Odisha' }, { id: 22, nm: 'Puducherry' }, { id: 23, nm: 'Punjab' }, { id: 24, nm: 'Rajasthan' }, { id: 25, nm: 'Sikkim' }, { id: 26, nm: 'Tamil Nadu' }, { id: 27, nm: 'Telangana' }, { id: 28, nm: 'Tripura' }, { id: 29, nm: 'Uttar Pradesh' }, { id: 30, nm: 'Uttarakhand' }, { id: 31, nm: 'West Bengal' }] },

  ];
  states: Array<any>;

  triale: any;
  trialp:any;
  trialPhoneNumber: any;
  trialPhoneNumber1: any;
  trialPhoneNumber2: any;
  phoneNumberCountryNm: any;
  phoneNumberCountryNm1: any;
  phoneNumberCountryNm2: any;
  prefixCountryCode1: any;
  prefixCountryCode2: any;

  public dateValue = new Date(); //dateValue= Date.now();
  loginUserName: any;
  count: number;

  constructor(private formBuilder: FormBuilder, private router: Router, private createCustomerService: CreateCustomerService, private createSupplierService: CreateSupplierService) {

    this.AddSupplierform = this.formBuilder.group({
      name: ['', [Validators.required,Validators.maxLength(60)]],
      primaryContact: [],
      secondaryContact: [],
      phoneNo: ['', [Validators.required, ValidationService.phoneValidator]],
      primaryPhoneNo:  ['',[optionalValidator([Validators.maxLength(255), ValidationService.phoneValidator])]],
      secondaryPhoneNo:['',[optionalValidator([Validators.maxLength(255), ValidationService.phoneValidator])]],
      supplierType: [],
      email: ['', [Validators.required, ValidationService.emailValidator]],
      panNo:  ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo1: ['', [optionalValidator([Validators.maxLength(2), ValidationService.StateCodeValidator])]],
      gstNo2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo3: ['', [optionalValidator([Validators.maxLength(3), ValidationService.lastThreeDigitOfGSTValidator])]],
      approvalStatus: [],
      status: [],
      country: [],
      addressLine1: [],
      addressLine2: [],
      state: [],
      city: [],
      code: []
    });

  }


  ngOnInit() {

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    this.supplier.status=1;
// get suppliers list
this.createSupplierService.getSuppliersList().subscribe(data => {
  this.supplierObj = data;

  console.log(this.supplierObj);
  for (var i = 0, l = Object.keys(this.supplierObj).length; i < l; i++) {
    this.arraySuppliers.push(this.supplierObj[i].name);
    this.suppliersId.push(this.supplierObj[i].id);
    this.supplierGST.push(this.supplierObj[i].gstNo);
    this.arraySuppliersEmail.push(this.supplierObj[i].email);
  }

  console.log(this.arraySuppliers);
  console.log(this.supplierGST);
},
  error => console.log(error));



    // get supplier type list
    this.createSupplierService.getSupplierTypes().subscribe(data => {

      this.supplierTypeObj = data;

      for (var i = 0, l = Object.keys(this.supplierTypeObj).length; i < l; i++) {
        this.arraySupplierTypeName.push(this.supplierTypeObj[i].supplierTypeName);
        this.arraySupplierTypeId.push(this.supplierTypeObj[i].supplierTypeId);
      }
    },
      error => console.log(error));


    // get countries list
    this.createCustomerService.getCountryList().subscribe(data => {

      this.countries = data;

      for (var i = 0, l = Object.keys(this.countries).length; i < l; i++) {
        this.arrayCountryName.push(this.countries[i].countryName);
        this.arrayCountryId.push(this.countries[i].countryId);

      }
    },
      error => console.log(error));



    // get states list
    this.createCustomerService.getStatesList().subscribe(data => {

      this.statesData = data;

      for (var i = 0, l = Object.keys(this.statesData).length; i < l; i++) {
        this.arrayStatesName.push(this.statesData[i].stateName);
        this.arrayStatesId.push(this.statesData[i].stateId);
        this.arrayStatesCode.push(this.statesData[i].stateCode);
        this.arrayStatesCountryId.push(this.statesData[i].countryId);

        this.arrState.push({ id: this.statesData[i].stateId, nm: this.statesData[i].stateName });

        //     console.log(this.arrayStatesName)
      }
      console.log(this.arrState)
      this.countryAndStates();

    },
      error => console.log(error));


    // mobileNumber code

    var input = document.querySelector("#phoneNo");
    var country = $('#countryphno');
    var iti = (<any>window).intlTelInput(input, {
      // any initialisation options go here
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti.setNumber("+917733123456");
    var number = iti.getNumber();

    var countryData = iti.getSelectedCountryData();


    this.trialPhoneNumber = 91;
    this.phoneNumberCountryNm = "in";
    input.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a = country.val(iti.getSelectedCountryData().dialCode);
      console.log(a);
      this.trialPhoneNumber = iti.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm = iti.getSelectedCountryData().iso2;
      document.getElementById("countryphno").innerText = this.trialPhoneNumber;

      console.log(this.trialPhoneNumber);
    });




    var input1 = document.querySelector("#mobileNumber1");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();

    var countryData1 = iti1.getSelectedCountryData();

    this.trialPhoneNumber1 = 91;
    this.phoneNumberCountryNm1 = "in";
    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      document.getElementById("country1").innerText = this.trialPhoneNumber1;

      console.log(this.trialPhoneNumber1);
    });


    var input2 = document.querySelector("#mobileNumber2");
    var country2 = $('#country2');
    var iti2 = (<any>window).intlTelInput(input2, {
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti2.setNumber("+917733123456");
    var number2 = iti2.getNumber();

    var countryData2 = iti2.getSelectedCountryData();

    this.trialPhoneNumber2 = 91;
    this.phoneNumberCountryNm2 = "in";
    input2.addEventListener('countrychange', (e) => {

      var a2 = country2.val(iti2.getSelectedCountryData().dialCode);
      console.log(a2);
      this.trialPhoneNumber2 = iti2.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm2 = iti2.getSelectedCountryData().iso2;
    });

  }

  countryAndStates() {
    for (var i = 0; i < this.arrayCountryName.length; i++) {
      if (this.arrayCountryId[i] == this.arrayStatesCountryId[i]) {
        this.countryList.push({ name: this.arrayCountryName[i], states: this.arrState })
      }
    }

    console.log(this.countryList);
  }


  changeCountry(count) {
    this.states = this.countryList.find(con => con.name == count).states;
  }


  onSubmit() {

    var gst;
    var gst1 = this.AddSupplierform.value.gstNo1.toString();
    var gst2 = this.panNo.toString();
    var gst3 = this.AddSupplierform.value.gstNo3.toString();

    gst = gst1.concat(gst2.toString());
    this.AddSupplierform.value.gstNo = gst.concat(gst3);

    if (typeof this.primaryPhoneNo != 'undefined') {
      this.supplier.primaryPhoneNo = "+" + this.trialPhoneNumber1 + this.primaryPhoneNo;
    }
    else {
      this.supplier.primaryPhoneNo = null;
    }
   
    if (typeof this.secondaryPhoneNo != 'undefined') {
      this.supplier.secondaryPhoneNo = "+" + this.trialPhoneNumber2 + this.secondaryPhoneNo;
    }
    else {
      this.supplier.secondaryPhoneNo = null;
    }

    for (var i = 0; i < Object.keys(this.supplierTypeObj).length; i++) {
      if (this.supplierTypeObj[i].supplierTypeName == this.AddSupplierform.value.supplierType) {
        this.supplier.supplierType = this.supplierTypeObj[i].supplierTypeId;
      }
    }

    this.supplier.phoneNo = "+" + this.trialPhoneNumber + this.AddSupplierform.value.phoneNo;
    this.supplier.panNo = this.panNo;
    this.supplier.gstNo = this.AddSupplierform.value.gstNo;
    this.supplier.createdBy = this.loginUserName;
//  this.supplier.name = this.name;  
    this.supplier.dateCreated = this.dateValue;
    this.supplier.countryPrefixForPhoneNo=this.phoneNumberCountryNm;
    this.supplier.countryPrefixForPrimaryMobile = this.phoneNumberCountryNm1;
    this.supplier.countryPrefixForSecondaryMobile = this.phoneNumberCountryNm2;

    this.checkForDuplicate();

    // this.createSupplierService.createSupplier(this.supplier).subscribe(data => {
    //   console.log(data);
    //   this.supplierData=data;
    // },
    //   error => console.log(error));
  }


  /////////////////////////////////// Check for dublicancy /////////////////////////////

  checkForDuplicate() {

    const str = "abc's@thy#^g&test#s";
    console.log(str.replace(/[^a-zA-Z ]/g, ""));
    console.log(str.replace(/[^a-zA-Z0-9 ]/g, ""));


    for (this.count = 0; this.count < Object.keys(this.supplierObj).length; this.count++) {

      if (this.arraySuppliers[this.count].toLowerCase() == this.supplier.name.toLowerCase() && this.arraySuppliersEmail[this.count] == this.supplier.email || this.supplierGST[this.count] == this.supplier.gstNo) {
        alert("This Supplier is already present");
        break;
      }
      else {

      }
    }

    if (this.count >= Object.keys(this.supplierObj).length) {

      this.createSupplierService.createSupplier(this.supplier).subscribe(data => {
        console.log(data);
        this.supplierData=data;
        alert("Supplier created Successfully!!");

        this.clearForm();
      },
        error => console.log(error));

     
    }
  }

  clearForm(){
    window.location.reload();
  }

}