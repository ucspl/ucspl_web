import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { Country, CreateCustomerService, States } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateSupplierService, Supplier, SupplierAttachments } from '../../../../services/serviceconnection/create-service/create-supplier.service';
import { Branch, Department, Role, SupplierType } from '../../../../services/serviceconnection/create-service/create-def.service';
import { SearchSupplierService } from '../../../../services/serviceconnection/search-service/search-supplier.service';

declare var $: any;
@Component({
  selector: 'app-supp-update',
  templateUrl: './supp-update.component.html',
  styleUrls: ['./supp-update.component.css']
})
export class SuppUpdateComponent implements OnInit {

  AddSupplierform: FormGroup;
  selectCountryid;
  supplier: Supplier = new Supplier();
  suppObj: Supplier = new Supplier();
  supplierObj: Supplier = new Supplier();
  supplierData: any;

  dateString = '';
  format = 'dd/MM/yyyy';

  name: string;
  primaryContact: string;
  secondaryContact: string;
  //  phoneNo: string;
  primaryPhoneNo: string;
  secondaryPhoneNo: string;
  supplierType: string;
  email: string;
  approvalStatus: string;
  status: number;
  // country: string;
  addressLine1: string;
  addressLine2: string;
  state: string;
  city: string;
  code: string;

  panNo: string;
  gstNo1: string;
  gstNo2: string;
  gstNo3: string;


  branches: Array<any> = [];
  branchesIds: Array<any> = [];
  branch: Branch = new Branch();
  branch1: Branch = new Branch();

  departments: Array<any> = [];
  departmentsIds: Array<any> = [];
  department: Department = new Department();
  department1: Department = new Department();

  roles: Array<any> = [];
  rolesIds: Array<any> = [];
  role: Role = new Role();
  role1: Role = new Role();

  supplierTypeObj: SupplierType = new SupplierType();
  arraySupplierTypeName: Array<any> = [];
  arraySupplierTypeId: Array<any> = [];
  arraySuppliers: Array<any> = [];
  supplierGST: Array<any> = [];
  static suppliersName: Array<any> = [];
  suppliersId: Array<any> = [];

  countries: Country = new Country();
  country: Country = new Country();
  arrayCountryName: Array<any> = [];
  arrayCountryId: Array<any> = [];

  statesData: States = new States();
  arrayStatesName: Array<any> = [];
  arrayStatesId: Array<any> = [];
  arrayStatesCode: Array<any> = [];
  arrState: Array<{ id: number, nm: string }> = [];
  arrayStatesCountryId: Array<any> = [];
  arraySuppliersEmail: Array<any> = [];

  supplierIdForSuppName: number;

  countryList: any[] = [
    { name: '--Select Country--', states: [] },
    //  { name: 'India', states: [{ id: 1, nm: 'Andhra Pradesh' }, { id: 2, nm: 'Arunachal Pradesh' }, { id: 3, nm: 'Assam' }, { id: 4, nm: 'Bihar' }, { id: 5, nm: 'Chhattisgarh' }, { id: 6, nm: 'Goa' }, { id: 7, nm: 'Gujarat' }, { id: 8, nm: 'Haryana' }, { id: 9, nm: 'Himachal Pradesh' }, { id: 10, nm: 'Jammu and Kashmir' }, { id: 11, nm: 'Jharkhand' }, { id: 12, nm: 'Karnataka' }, { id: 13, nm: 'Kerala' }, { id: 14, nm: 'Ladakh' }, { id: 15, nm: 'Madhya Pradesh' }, { id: 16, nm: 'Maharashtra' }, { id: 17, nm: 'Manipur' }, { id: 18, nm: 'Meghalaya' }, { id: 19, nm: 'Mizoram' }, { id: 20, nm: 'Nagaland' }, { id: 21, nm: 'Odisha' }, { id: 22, nm: 'Puducherry' }, { id: 23, nm: 'Punjab' }, { id: 24, nm: 'Rajasthan' }, { id: 25, nm: 'Sikkim' }, { id: 26, nm: 'Tamil Nadu' }, { id: 27, nm: 'Telangana' }, { id: 28, nm: 'Tripura' }, { id: 29, nm: 'Uttar Pradesh' }, { id: 30, nm: 'Uttarakhand' }, { id: 31, nm: 'West Bengal' }] },

  ];
  states: Array<any>;

  triale: any;
  trialp: any;
  trialPhoneNumber: any;
  trialPhoneNumber1: any;
  trialPhoneNumber2: any;
  phoneNumberCountryNm: any;
  phoneNumberCountryNm1: any;
  phoneNumberCountryNm2: any;
  prefixCountryCode1: any;
  prefixCountryCode2: any;

  public dateValue = new Date(); //dateValue= Date.now();
  loginUserName: any;
  count: number;
  countryNm2: any;
  countryNm1: any;
  countryNm: any;
  phoneNo: string;
  suppType: any;
  supplierTypeSelected: any;
  suppType1: any;

  selectedFile: File;
  selectedFileForSuppAttachment: File;
  selectedFileForSuppAttachmentTrial: any;
  selectedFileForSupplierAttachment: any;
  trialFileReader: any;
  retrievedImage: any;
  base64Data: any;
  arrayBase64Data: Array<any> = [];
  arrayRetrievedImage: Array<any> = [];
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  suppAttachUpload: SupplierAttachments = new SupplierAttachments();
  suppAttachObj: SupplierAttachments = new SupplierAttachments();
  suppAttach: any;
  suppNameForNewFileName: any;

  docName: string;
  docType: string;
  docNumber: string;
  suppAttachFiles: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private createCustomerService: CreateCustomerService, private createSupplierService: CreateSupplierService, private searchSupplierService: SearchSupplierService) {

    this.AddSupplierform = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(60)]],
      primaryContact: [],
      secondaryContact: [],
      phoneNo: ['', [Validators.required, ValidationService.phoneValidator]],
      primaryPhoneNo: ['', [optionalValidator([Validators.maxLength(255), ValidationService.phoneValidator])]],
      secondaryPhoneNo: ['', [optionalValidator([Validators.maxLength(255), ValidationService.phoneValidator])]],
      supplierTypes: [],
      email: ['', [Validators.required, ValidationService.emailValidator]],
      panNo: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo1: ['', [optionalValidator([Validators.maxLength(2), ValidationService.StateCodeValidator])]],
      gstNo2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo3: ['', [optionalValidator([Validators.maxLength(3), ValidationService.lastThreeDigitOfGSTValidator])]],
      approvalStatus: [],
      status: [],
      country: [],
      addressLine1: [],
      addressLine2: [],
      state: [],
      city: [],
      code: []
    });

  }


  ngOnInit() {

    this.countryNm = this.searchSupplierService.getSupplierCountryForPhoneNo();
    this.countryNm1 = this.searchSupplierService.getSupplierCountryForMobileNo1();
    this.countryNm2 = this.searchSupplierService.getSupplierCountryForMobileNo2();


    // get countries list
    this.createCustomerService.getCountryList().subscribe(data => {

      this.country = data;

      for (var i = 0, l = Object.keys(this.country).length; i < l; i++) {
        this.arrayCountryName.push(this.country[i].countryName);
        this.arrayCountryId.push(this.country[i].countryId);
      }
    },
      error => console.log(error));


    // get states list
    this.createCustomerService.getStatesList().subscribe(data => {

      this.statesData = data;

      for (var i = 0, l = Object.keys(this.statesData).length; i < l; i++) {
        this.arrayStatesName.push(this.statesData[i].stateName);
        this.arrayStatesId.push(this.statesData[i].stateId);
        this.arrayStatesCode.push(this.statesData[i].stateCode);
        this.arrayStatesCountryId.push(this.statesData[i].countryId);
        this.arrState.push({ id: this.statesData[i].stateId, nm: this.statesData[i].stateName });
        //console.log(this.arrayStatesName)
      }
      console.log(this.arrState)
      this.countryAndStates();

    },
      error => console.log(error));

    // get supplier type list
    this.createSupplierService.getSupplierTypes().subscribe(data => {

      this.supplierTypeObj = data;

      for (var i = 0, l = Object.keys(this.supplierTypeObj).length; i < l; i++) {
        this.arraySupplierTypeName.push(this.supplierTypeObj[i].supplierTypeName);
        this.arraySupplierTypeId.push(this.supplierTypeObj[i].supplierTypeId);
      }
    },
      error => console.log(error));


    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.supplierIdForSuppName = this.searchSupplierService.getIdForSupplierName();

    console.log(this.supplierIdForSuppName);

    // get supplier details for id
    this.searchSupplierService.getSupplierDetailsById(this.supplierIdForSuppName).subscribe(data => {

      this.suppObj = data;
      console.log(this.suppObj);
      this.dataForUpdateSupplier();
    },
      error => console.log(error));


    this.createSupplierService.getSuppliersList().subscribe(data => {

      this.supplierObj = data;
      console.log(this.supplierObj);

    },
      error => console.log(error));





    // mobileNumber code

    var input = document.querySelector("#phoneNo");
    var country = $('#countryphno');
    var iti = (<any>window).intlTelInput(input, {
      // any initialisation options go here
      "preferredCountries": [this.countryNm],
      "separateDialCode": true
    });
    // iti.setNumber("+917733123456");
    var number = iti.getNumber();

    var countryData = iti.getSelectedCountryData();


    // this.trialPhoneNumber = 91;
    // this.phoneNumberCountryNm = "in";
    input.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a = country.val(iti.getSelectedCountryData().dialCode);
      console.log(a);
      this.trialPhoneNumber = iti.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm = iti.getSelectedCountryData().iso2;
      document.getElementById("countryphno").innerText = this.trialPhoneNumber;

      console.log(this.trialPhoneNumber);
    });




    var input1 = document.querySelector("#mobileNumber1");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": [this.countryNm1],
      "separateDialCode": true
    });
    // iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();

    var countryData1 = iti1.getSelectedCountryData();

    // this.trialPhoneNumber1 = 91;
    // this.phoneNumberCountryNm1 = "in";
    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      document.getElementById("country1").innerText = this.trialPhoneNumber1;

      console.log(this.trialPhoneNumber1);
    });


    var input2 = document.querySelector("#mobileNumber2");
    var country2 = $('#country2');
    var iti2 = (<any>window).intlTelInput(input2, {
      "preferredCountries": [this.countryNm2],
      "separateDialCode": true
    });
    //  iti2.setNumber("+917733123456");
    var number2 = iti2.getNumber();

    var countryData2 = iti2.getSelectedCountryData();

    // this.trialPhoneNumber2 = 91;
    // this.phoneNumberCountryNm2 = "in";
    input2.addEventListener('countrychange', (e) => {

      var a2 = country2.val(iti2.getSelectedCountryData().dialCode);
      console.log(a2);
      this.trialPhoneNumber2 = iti2.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm2 = iti2.getSelectedCountryData().iso2;
    });



  }

  countryAndStates() {

    for (var i = 0; i < this.arrayCountryName.length; i++) {
      if (this.arrayCountryId[i] == this.arrayStatesCountryId[i]) {
        this.countryList.push({ name: this.arrayCountryName[i], states: this.arrState })
      }
    }

  }

  phoneNumberUpdate() {
    this.trialPhoneNumber = this.supplier.phoneNo.slice(1, 3);
    this.trialPhoneNumber1 = this.supplier.primaryPhoneNo.slice(1, 3);
    this.trialPhoneNumber2 = this.supplier.secondaryPhoneNo.slice(1, 3);

    this.supplier.phoneNo = this.supplier.phoneNo.slice(3, 13);
    this.supplier.primaryPhoneNo = this.supplier.primaryPhoneNo.slice(3, 13);
    this.supplier.secondaryPhoneNo = this.supplier.secondaryPhoneNo.slice(3, 13);
  }

  changeCountry(count) {
    this.states = this.countryList.find(con => con.name == count).states;

  }

  dataForUpdateSupplier() {

    this.supplier.supplierType = this.suppObj.supplierType;

    for (var i = 0; i < Object.keys(this.supplierTypeObj).length; i++) {
      if (this.supplierTypeObj[i].supplierTypeId == this.suppObj.supplierType) {
        this.supplier.supplierType = this.supplierTypeObj[i].supplierTypeName;
        this.supplierTypeSelected = this.supplier.supplierType;
        //  this.suppType1 = this.supplierTypeObj[i].supplierTypeName;
      }
    }

    this.supplier.name = this.suppObj.name;

    this.supplier.panNo = this.suppObj.panNo;
    this.panNo = this.suppObj.panNo;
    this.supplier.gstNo = this.suppObj.gstNo;

    if (typeof this.supplier.gstNo != 'undefined') {
      this.gstNo1 = this.supplier.gstNo.slice(0, 2);
      this.gstNo2 = this.supplier.gstNo.slice(2, 12);
      this.gstNo3 = this.supplier.gstNo.slice(12);

    }

    this.supplier.phoneNo = this.suppObj.phoneNo;
    this.supplier.primaryContact = this.suppObj.primaryContact;
    this.supplier.secondaryContact = this.suppObj.secondaryContact;

    this.phoneNo = this.suppObj.phoneNo.slice(-10);
    var mb1 = new String(this.suppObj.phoneNo)
    this.trialPhoneNumber = this.suppObj.phoneNo.slice(-mb1.length, -10);
    console.log("value of this.trialPhoneNumber" + this.trialPhoneNumber)

    if (this.suppObj.primaryPhoneNo != null) {
      this.primaryPhoneNo = this.suppObj.primaryPhoneNo.slice(-10);
      var mb1 = new String(this.suppObj.primaryPhoneNo)
      this.trialPhoneNumber1 = this.suppObj.primaryPhoneNo.slice(-mb1.length, -10);
      console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)
    }

    if (this.suppObj.secondaryPhoneNo != null) {
      var mb2 = new String(this.suppObj.secondaryPhoneNo)
      this.trialPhoneNumber2 = this.suppObj.secondaryPhoneNo.slice(-mb2.length, -10);
      this.secondaryPhoneNo = this.suppObj.secondaryPhoneNo.slice(-10);
      console.log("value of this.trialPhoneNumber2" + this.trialPhoneNumber2);
    }

    this.supplier.countryPrefixForPhoneNo = this.suppObj.countryPrefixForPhoneNo
    this.supplier.countryPrefixForPrimaryMobile = this.suppObj.countryPrefixForPrimaryMobile
    this.supplier.countryPrefixForSecondaryMobile = this.suppObj.countryPrefixForSecondaryMobile

    this.supplier.code = this.suppObj.code;
    this.supplier.approvalStatus = this.suppObj.approvalStatus;
    //  this.phoneNumberUpdate();

    this.supplier.email = this.suppObj.email;
    this.supplier.addressLine1 = this.suppObj.addressLine1;
    this.supplier.addressLine2 = this.suppObj.addressLine2

    this.supplier.country = this.suppObj.country;
    this.supplier.state = this.suppObj.state;

    this.changeCountry(this.supplier.country);

    this.supplier.city = this.suppObj.city;
    this.supplier.code = this.suppObj.code;
    this.supplier.status = this.suppObj.status;
    this.supplier.createdBy = this.suppObj.createdBy;
    this.supplier.dateCreated = this.suppObj.dateCreated;
  }

  onSubmit() {

    this.supplier.phoneNo = "+" + this.trialPhoneNumber + this.phoneNo;

    if (typeof this.primaryPhoneNo != 'undefined' || this.primaryPhoneNo != null) {
      this.supplier.primaryPhoneNo = "+" + this.trialPhoneNumber1 + this.primaryPhoneNo;
    }
    else {
      this.supplier.primaryPhoneNo = null;
    }

    if (typeof this.secondaryPhoneNo != 'undefined' || this.secondaryPhoneNo != null) {
      this.supplier.secondaryPhoneNo = "+" + this.trialPhoneNumber2 + this.secondaryPhoneNo;
    }
    else {
      this.supplier.secondaryPhoneNo = null;
    }


    this.supplier.countryPrefixForPhoneNo = this.phoneNumberCountryNm;
    this.supplier.countryPrefixForPrimaryMobile = this.phoneNumberCountryNm1;
    this.supplier.countryPrefixForSecondaryMobile = this.phoneNumberCountryNm2;

    for (var i = 0; i < Object.keys(this.supplierTypeObj).length; i++) {
      if (this.supplierTypeObj[i].supplierTypeName == this.supplierTypeSelected) {
        this.supplier.supplierType = this.supplierTypeObj[i].supplierTypeId;
      }
    }

    this.supplier.panNo = this.panNo;
    var gst;
    var gst1 = this.AddSupplierform.value.gstNo1.toString();
    var gst2 = this.AddSupplierform.value.gstNo2.toString();
    var gst3 = this.AddSupplierform.value.gstNo3.toString();

    gst = gst1.concat(gst2.toString());
    this.supplier.gstNo = gst.concat(gst3);

    this.supplier.dateModified = new Date();
    this.supplier.modifiedBy = this.loginUserName;

    this.createSupplierService.updateSupplier(this.supplierIdForSuppName, this.supplier).subscribe(data => {
      console.log(data);
      alert("Supplier updated successfully !!");
    })
  }



  /////////////////// customer attachments ///////////////////////////


  public onFileChangedForDocument(event) {

    this.selectedFileForSuppAttachment = event.target.files[0];
    this.selectedFileForSupplierAttachment = $("#docFile")[0].files[0]
    this.selectedFileForSuppAttachmentTrial = event.target;
    console.log(" this.selectedFileForCustAttachment " + this.selectedFileForSuppAttachment)

  }

  onUploadForDocument() {

    console.log(this.selectedFileForSuppAttachment);

    const uploadFileData = new FormData();
    var trialForTarget = this.selectedFileForSuppAttachmentTrial;

    debugger;
    this.addSuppAttach();


  }


  addSuppAttach() {

    // get suppliers list    
    for (var i = 0, l = Object.keys(this.supplierObj).length; i < l; i++) {
      if (this.supplierIdForSuppName == this.supplierObj[i].id) {
        this.suppNameForNewFileName = this.supplierObj[i].name;
      }
    }

    var originalName = this.selectedFileForSuppAttachment.name;
    const uploadImageData = new FormData();
    var newFileName = this.supplierIdForSuppName + "_" + this.suppNameForNewFileName + "_" + this.selectedFileForSuppAttachment.name;

    uploadImageData.append('imageFile', this.selectedFileForSuppAttachment, newFileName);

    this.createSupplierService.suppAttachmentsFileUpload(uploadImageData).subscribe(data => {
      console.log(data);
      this.suppAttachUpload = data;

      this.suppAttachObj.suppId = this.supplierIdForSuppName;
      this.suppAttachObj.documentName = this.docName;
      this.suppAttachObj.documentType = this.supplier.supplierType;
      this.suppAttachObj.documentNumber = this.docNumber;
      this.suppAttachObj.originalFileName = originalName;

      this.createSupplierService.createSupplierAttachments(this.suppAttachObj).subscribe(data => {
        this.suppAttach = data;
        console.log(data);
        alert("Document uploaded successfully !!");
        this.getSupplierAttachmentsFiles();
      },
        error => console.log(error));
    });


  }


  getSupplierAttachmentsFiles() {

    this.docType=this.supplier.supplierType;
    this.createSupplierService.getSupplierAttachLists(this.supplierIdForSuppName).subscribe(data => {

      this.suppAttachFiles = data;
      this.retrieveResonse = data;
      console.log(data);

      this.suppAttachFiles;


    },
      error => console.log(error));

  }



  getSelectedFile(event) {

    this.createSupplierService.getSuppAttchDetailsToGetDoc(this.suppAttachFiles[event]).subscribe(data => {
      console.log(data);

      this.retrieveResonse = data;
      // this.base64Data = this.retrieveResonse.picByte;
      // this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;

      // var img = '<img src="' + this.retrievedImage + '">';
      // var popup = window.open();
      // popup.document.write(img);
      // popup.document.title = this.suppAttachFiles[event].originalFileName;


    if(this.retrieveResonse.type.includes('/png') || this.retrieveResonse.type.includes('/jpg') || this.retrieveResonse.type.includes('/jpeg') || this.retrieveResonse.type.includes('/gif'))
    {    
  
         this.base64Data = this.retrieveResonse.picByte;
         this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
         var img = '<img src="' + this.retrievedImage + '">';
         var popup = window.open();
         popup.document.write(img);
         popup.document.title = this.suppAttachFiles[event].originalFileName;

    }
    else{
        
        this.base64Data = this.retrieveResonse.picByte;
        this.base64Datafile = atob(this.base64Data); 
        const retrievedImage1 = 'data:application/pdf;base64,' + this.base64Data;
        //window.open('data:application/pdf;base64,' + retrievedImage1);
        
        // var img = '<a href="' + retrievedImage1 + '" >';
        // var popup = window.open();
        // popup.document.write(img);

        let pdfWindow = window.open("")
pdfWindow.document.write(
    "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
    encodeURI(this.base64Data) + "'></iframe>"
)
        // const downloadLink = document.createElement("a");
        // const fileName = this.imageName;
        // downloadLink.href =  retrievedImage1;
        // downloadLink.download = fileName;
        // downloadLink.click();

    }

  })

  }


  //////////////////////////////// pop-up screens /////////////////////////

  openModal1() {
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()

  }

  openModal2() {
    const buttonModal2 = document.getElementById("openModalButton2")
    console.log('buttonModal2', buttonModal2)
    buttonModal2.click()
    this.getSupplierAttachmentsFiles();
  }



}
