import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppUpdateComponent } from './supp-update.component';

describe('SuppUpdateComponent', () => {
  let component: SuppUpdateComponent;
  let fixture: ComponentFixture<SuppUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuppUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
