import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator } from '../../../../services/config/config.service';
import { SupplierType } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateSupplierService, Supplier } from '../../../../services/serviceconnection/create-service/create-supplier.service';
import { SearchSupplierService, SupplierByNameOrAddress } from '../../../../services/serviceconnection/search-service/search-supplier.service';

@Component({
  selector: 'app-supp-search',
  templateUrl: './supp-search.component.html',
  styleUrls: ['./supp-search.component.css']
})
export class SuppSearchComponent implements OnInit {

  searchSupplierform:FormGroup;
  p: number = 1;
  itemsPerPage:number=5;

  supplierByNameorAddr:SupplierByNameOrAddress = new SupplierByNameOrAddress();
  supplierObj:Supplier = new Supplier();

  model : any={};    
  emp:any;  

  supplierArray:Array<any> =[];
  suppliersName:Array<any> =[];
  suppliersAddressLine1:Array<any> =[];
  suppliersAddressLine2:Array<any> =[];
  suppliersCountry:Array<any> =[];
  suppliersState:Array<any> =[];
  suppliersCity:Array<any> =[];
  suppliersPin:Array<any> =[];

  suppObject:Supplier =new Supplier();

  supplierIds:Array<any>=[];
  supplierIdLength:number;
 
  supplierIdForSuppName:number;
  
  supplierTypeObj: SupplierType = new SupplierType();
  arraySupplierTypeName: Array<any> = [];
  arraySupplierTypeId: Array<any> = [];
  status: any;
  
  constructor(private formBuilder: FormBuilder, private router: Router, private searchSupplierService:SearchSupplierService, private createSupplierService:CreateSupplierService ) {

    this.searchSupplierform = this.formBuilder.group({
      
      name: ['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      status:[],
      address:['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      itemsPerPage:[]
    })
   }


  ngOnInit() {
    this.status="1";

    
    // get supplier type list
    this.createSupplierService.getSupplierTypes().subscribe(data => {

      this.supplierTypeObj = data;

      for (var i = 0, l = Object.keys(this.supplierTypeObj).length; i < l; i++) {
        this.arraySupplierTypeName.push(this.supplierTypeObj[i].supplierTypeName);
        this.arraySupplierTypeId.push(this.supplierTypeObj[i].supplierTypeId);
      }
    },
      error => console.log(error));

  }
  searchandmodifycustomer(){
    alert("search supplier");
  }

  


showData()  
{  
  this.searchSupplierService.showData().subscribe((res) => {  
  //  this.emp=res; 
    this.supplierObj =res; 
    console.log(this.supplierObj); 
  
    this.listOfCustomers();
})  
}  
searchData() {  
 debugger;  

 this.supplierByNameorAddr= this.searchSupplierform.value;

 if( this.supplierByNameorAddr.name=="" && this.supplierByNameorAddr.address==""){
  alert("Please enter criteria to search supplier")
}
else{

 
   if((!this.searchSupplierform.value.name.includes('and')) && (!this.searchSupplierform.value.name.includes('&')))
     this.supplierByNameorAddr.nameAnd="";
   

 this.searchSupplierService.searchData(this.supplierByNameorAddr).subscribe((res: any) => {  
          
      this.emp=res;   
      console.log(this.emp);   
  })  

  this.showData();
}
}  

listOfCustomers()
{

  this.searchSupplierService.showData().subscribe(data => {

    this.supplierArray=data;
    this.supplierObj=data;
    console.log("..." +this.supplierObj);
    console.log("...trial" +this.supplierArray);

    // for (var i = 0; i < Object.keys(this.supplierTypeObj).length; i++) {
    //   if (this.supplierTypeObj[i].supplierTypeId == this.supplierArray[i][10]) {
    //     this.supplierArray[i][10] = this.supplierTypeObj[i].supplierTypeName;
    //   }
    // }

    for(var i=0;i< Object.keys(this.supplierArray).length;i++)
      {
        if(this.supplierArray[i][15]==0)
        {
          this.supplierArray[i][15]="Inactive";
        }          
        else
           this.supplierArray[i][15]="Active";
      }
     
 
  }, 
  error => console.log(error));

}

updateCustomer(e)
{
  console.log(this.itemsPerPage);
  console.log(this.p);
  console.log(e);
  e=((this.itemsPerPage * (this.p-1))+(e));
  console.log("index will be...."+e);

  this.supplierIdForSuppName=parseInt(e);

  this.searchSupplierService.setIdForSupplierName(this.supplierArray[this.supplierIdForSuppName][0]);

// get supplier details for id
  this.searchSupplierService.getSupplierDetailsById(this.supplierArray[this.supplierIdForSuppName][0]).subscribe(data => {

  this.suppObject = data;
  console.log(this.suppObject);
  this.searchSupplierService.setSupplierCountryForPhoneNo( this.suppObject.countryPrefixForPhoneNo); 
  this.searchSupplierService.setSupplierCountryForMobileNo1( this.suppObject.countryPrefixForPrimaryMobile);
  this.searchSupplierService.setSupplierCountryForMobileNo2( this.suppObject.countryPrefixForSecondaryMobile);
  this.router.navigateByUrl('/nav/updatesupp')
},
  error => console.log(error));
  
 
}


}
