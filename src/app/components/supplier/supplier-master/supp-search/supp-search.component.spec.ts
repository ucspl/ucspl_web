import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppSearchComponent } from './supp-search.component';

describe('SuppSearchComponent', () => {
  let component: SuppSearchComponent;
  let fixture: ComponentFixture<SuppSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuppSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
