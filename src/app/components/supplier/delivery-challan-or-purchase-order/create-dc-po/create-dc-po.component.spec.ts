import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDcPoComponent } from './create-dc-po.component';

describe('CreateDcPoComponent', () => {
  let component: CreateDcPoComponent;
  let fixture: ComponentFixture<CreateDcPoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDcPoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDcPoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
