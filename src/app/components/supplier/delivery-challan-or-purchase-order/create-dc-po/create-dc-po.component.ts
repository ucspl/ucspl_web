
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-create-dc-po',
  templateUrl: './create-dc-po.component.html',
  styleUrls: ['./create-dc-po.component.css']
})
export class  CreateDcPoComponent implements OnInit {
  CreateDeliveryChallanform: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();
  

  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) { 
    this.CreateDeliveryChallanform = this.formBuilder.group({
      DocType:[],
      DocName:[],
      CompanyPrefix:[],
      Branch:[],
      DocumentDate:[]
      

    });
  }

  ngOnInit() {
  }

}
