
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-search-dc-po',
  templateUrl: './search-dc-po.component.html',
  styleUrls: ['./search-dc-po.component.css']
})
export class SearchDcPoComponent implements OnInit {

  deliverychallanpurchaseorderform:FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) { 
    this.deliverychallanpurchaseorderform= this.formBuilder.group({
      search:[]
      });
    }

  ngOnInit() {
  }

}
