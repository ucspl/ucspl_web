import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDcPoComponent } from './search-dc-po.component';

describe('SearchDcPoComponent', () => {
  let component: SearchDcPoComponent;
  let fixture: ComponentFixture<SearchDcPoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDcPoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDcPoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
