import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaliListComponent } from './cali-list.component';

describe('CaliListComponent', () => {
  let component: CaliListComponent;
  let fixture: ComponentFixture<CaliListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaliListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaliListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
