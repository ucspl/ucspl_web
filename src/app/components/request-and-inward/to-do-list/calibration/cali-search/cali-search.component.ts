
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cali-search',
  templateUrl: './cali-search.component.html',
  styleUrls: ['./cali-search.component.css']
})
export class CaliSearchComponent implements OnInit {
  Searchform: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();


  constructor(private formBuilder: FormBuilder, private router: Router) {
    this.Searchform = this.formBuilder.group({

      fromDate: [],
      toDate: [],
      status: [],

    });

  }

  ngOnInit() {
  }

}
