import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaliSearchComponent } from './cali-search.component';

describe('CaliSearchComponent', () => {
  let component: CaliSearchComponent;
  let fixture: ComponentFixture<CaliSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaliSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaliSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
