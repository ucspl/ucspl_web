import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertiSearchComponent } from './certi-search.component';

describe('CertiSearchComponent', () => {
  let component: CertiSearchComponent;
  let fixture: ComponentFixture<CertiSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertiSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertiSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
