
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../../services/student/student.service';

@Component({
  selector: 'app-certi-search',
  templateUrl: './certi-search.component.html',
  styleUrls: ['./certi-search.component.css']
})
export class CertiSearchComponent implements OnInit {
  Searchform:FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) {
    this.Searchform=this.formBuilder.group({

      fromDate:[],
      toDate:[],
      status: [],
    
  });
   }

  ngOnInit() {
  }

}
