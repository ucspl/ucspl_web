import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliListComponent } from './deli-list.component';

describe('DeliListComponent', () => {
  let component: DeliListComponent;
  let fixture: ComponentFixture<DeliListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
