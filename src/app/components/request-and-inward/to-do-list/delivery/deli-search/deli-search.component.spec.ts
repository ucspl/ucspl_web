import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliSearchComponent } from './deli-search.component';

describe('DeliSearchComponent', () => {
  let component: DeliSearchComponent;
  let fixture: ComponentFixture<DeliSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
