
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../../services/student/student.service';

@Component({
  selector: 'app-repair-search',
  templateUrl: './repair-search.component.html',
  styleUrls: ['./repair-search.component.css']
})
export class RepairSearchComponent implements OnInit {
  Searchform:FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) {
    this.Searchform=this.formBuilder.group({

      fromDate:[],
      toDate:[],
      status: [],
    
  });
   }

  ngOnInit() {
  }

}
