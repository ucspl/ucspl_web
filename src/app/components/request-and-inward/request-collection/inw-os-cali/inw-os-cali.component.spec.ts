import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwOsCaliComponent } from './inw-os-cali.component';

describe('InwOsCaliComponent', () => {
  let component: InwOsCaliComponent;
  let fixture: ComponentFixture<InwOsCaliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwOsCaliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwOsCaliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
