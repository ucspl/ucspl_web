import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwColleInstruComponent } from './inw-colle-instru.component';

describe('InwColleInstruComponent', () => {
  let component: InwColleInstruComponent;
  let fixture: ComponentFixture<InwColleInstruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwColleInstruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwColleInstruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
