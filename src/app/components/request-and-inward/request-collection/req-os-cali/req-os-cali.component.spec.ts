import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqOsCaliComponent } from './req-os-cali.component';

describe('ReqOsCaliComponent', () => {
  let component: ReqOsCaliComponent;
  let fixture: ComponentFixture<ReqOsCaliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqOsCaliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqOsCaliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
