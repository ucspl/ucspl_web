
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../services/config/config.service';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-req-os-cali',
  templateUrl: './req-os-cali.component.html',
  styleUrls: ['./req-os-cali.component.css']
})
export class ReqOsCaliComponent implements OnInit {

  ReqOnsiteCaliform: FormGroup;
  Searchform:FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();
  
  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService ) { 

    this.ReqOnsiteCaliform = this.formBuilder.group({

      RequestedDate:[],
      CustomerName: ['',  [Validators.required, ValidationService.nameValidator]],
      phoneno: ['',  [Validators.required, ValidationService.phoneValidator]],
      selectcustomer:[],
      RequestedQuantity:[],
      attachedfile1:[],
      attachedfile2:[],
      attachedfile3:[],
      attachedfile4:[],
      attachedfile5:[],

    });

    this.Searchform=this.formBuilder.group({

      fromDate:[],
      toDate:[],
      status: [],
      region:[]
  });
 }

  ngOnInit() {
  }
  selectcustomer(){
    alert("customer selected successfully");
  }

}
