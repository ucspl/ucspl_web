import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReqColleComponent } from './my-req-colle.component';

describe('MyReqColleComponent', () => {
  let component: MyReqColleComponent;
  let fixture: ComponentFixture<MyReqColleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyReqColleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyReqColleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
