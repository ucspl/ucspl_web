import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqColleComponent } from './req-colle.component';

describe('ReqColleComponent', () => {
  let component: ReqColleComponent;
  let fixture: ComponentFixture<ReqColleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqColleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqColleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
