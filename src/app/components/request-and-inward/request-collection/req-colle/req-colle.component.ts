
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../services/config/config.service';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-req-colle',
  templateUrl: './req-colle.component.html',
  styleUrls: ['./req-colle.component.css']
})
export class ReqColleComponent implements OnInit {

  RequestForCollectionform: FormGroup;
  Searchform:FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService ) { 


    this.RequestForCollectionform = this.formBuilder.group({

      CustomerName:[],
      StartDate:[],
      phoneno: ['',  [Validators.required, ValidationService.phoneValidator]],
      CollectionQuantity:[],
      attachedfile1:[],
      attachedfile2:[],
      attachedfile3:[],
      attachedfile4:[],
      attachedfile5:[],
  });

  this.Searchform=this.formBuilder.group({

    fromDate:[],
    toDate:[],
    status: [],
    region:[]
});
  }

  ngOnInit() {
  }
}