import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReqOsCaliComponent } from './my-req-os-cali.component';

describe('MyReqOsCaliComponent', () => {
  let component: MyReqOsCaliComponent;
  let fixture: ComponentFixture<MyReqOsCaliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyReqOsCaliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyReqOsCaliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
