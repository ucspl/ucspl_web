import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqCreateComponent } from './req-create.component';

describe('ReqCreateComponent', () => {
  let component: ReqCreateComponent;
  let fixture: ComponentFixture<ReqCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
