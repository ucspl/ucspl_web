import { DatePipe } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateReqAndInward, CreateReqAndInwardService, ReqTypeOfRequestAndInward, InwardTypeOfRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { StudentService } from '../../../../services/student/student.service';

declare var $: any;

@Component({
  selector: 'app-req-create',
  templateUrl: './req-create.component.html',
  styleUrls: ['./req-create.component.css']
})
export class ReqCreateComponent implements OnInit {

  createRequestInwardDocumentForm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';
  universal = "Universal Instruments Lab."

  requestAndInwardTypeObj: InwardTypeOfRequestAndInward = new InwardTypeOfRequestAndInward();
  arrayReqAndInwTypeName: Array<any> = [];
  arrayReqAndInwTypeId: Array<any> = [];

  reqTypeOfRequestAndInwardObj: ReqTypeOfRequestAndInward = new ReqTypeOfRequestAndInward();
  arrayReqTypeOfRequestAndInwardName: Array<any> = [];
  arrayReqTypeOfRequestAndInwardId: Array<any> = [];

  reqAndInwardObject: CreateReqAndInward = new CreateReqAndInward();
  public dateValue = new Date();
  public dateValue1 = new Date();
  public dateValue2 = new Date();
  trialPhoneNumber1: number;
  phoneNumberCountryNm1: string;

  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  childCustomerObj: Customer = new Customer();
  childCustomers: Array<any> = [];
  customerObject: any;
  static customerNameLength: number;

  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;
  loginUserName: any;
  inwardType: any;
  inwardDate = Date.now();
  reqContactPhone: any;
  noOfInstruments: any;
  shippedTo: any;
  customerWithAddress: Array<{ name: string, address: string }> = [];
  prefixForInward: any;
  dateOfDocumentNo: any;
  counterForDocumentNoString: any;
  counterForDocumentNo: number;

  dateValue3: any;
  finalDocumentNumber: string;
  reqAndInwardTypeNameSelected: string;
  inwardReqNo: any;

  d = new Date();
  prefixForInward1: string;
  dateOfDocumentNo1: string;
  counterForDocumentNoString1: string;
  counterForDocumentNo1: number;

  lastDayOfMonth = new Date(Date.now());
  displayDCDetails: any;
  customerName: any;
  reportsInNameof: any;
  labLocation: number;

  constructor(private formBuilder: FormBuilder, private router: Router, public datepipe: DatePipe, private studentService: StudentService, private createCustomerService: CreateCustomerService, private createReqAndInwardService: CreateReqAndInwardService) {

    this.createRequestInwardDocumentForm = this.formBuilder.group({


      inwardType: [],
      inwardDate: [],
      customerName: [],
      shippedTo: [],
      endDate: [],
      reportInNameOf: [],
      customerDCNo: [],
      dcDate: [],
      expectedDeliveryDate: [],
      calibratedAt: [],
      labLocation: [],
      noOfInstruments: [],
      reqContactName: [],
      reqContactPhone: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      reqEmail: ['', [optionalValidator([Validators.maxLength(255), ValidationService.emailValidator])]],
      reqType: []
    });
  }


  ngOnInit() {

    window.localStorage.removeItem('reqAndInwardDataObj1');
    window.localStorage.removeItem('dynamicArrayForReqAndIwd');

    window.localStorage.removeItem('inwardReqNo');
    window.localStorage.removeItem('reqInwDocumentNo');
    window.localStorage.removeItem('arrayUomNameAccorToParam1ForReqAndIwd');
    this.dateValue2 = new Date();
    this.dateValue2.setDate(this.dateValue2.getDate() + 7);


    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    this.labLocation = 0;

    this.createReqAndInwardService.getInwardTypesOfRequestAndInwards().subscribe(data => {
      this.requestAndInwardTypeObj = data;
      for (var i = 0, l = Object.keys(this.requestAndInwardTypeObj).length; i < l; i++) {
        this.arrayReqAndInwTypeName.push(this.requestAndInwardTypeObj[i].inwardTypeName)
        this.arrayReqAndInwTypeId.push(this.requestAndInwardTypeObj[i].inwardTypeId)
      }
    })

    this.createReqAndInwardService.getReqTypeOfRequestAndInwards().subscribe(data => {
      this.reqTypeOfRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
        this.arrayReqTypeOfRequestAndInwardName.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeName)
        this.arrayReqTypeOfRequestAndInwardId.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeId)
      }
    })


    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);
        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);

        if (this.customerObj[i].addressLine2 != null) {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].addressLine2 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        else {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }

        if (this.customerObj[i].isParent == 1) {
          this.parentCustNames.push(this.customerObj[i].name);
          this.parentCustIds.push(this.customerObj[i].id);
        }
      }
    },
      error => console.log(error));

    // get child customers list
    this.createReqAndInwardService.getChildCustomers().subscribe(data => {
      console.log(data);
      this.childCustomerObj = data;
      if (typeof this.childCustomerObj != 'undefined') {
        for (var i = 0, l = Object.keys(this.childCustomerObj).length; i < l; i++) {
          this.childCustomers.push(this.childCustomerObj[i].name);
        }
      }
    },
      error => console.log(error));

    // mobileNumber code
    var input1 = document.querySelector("#reqPhone");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      //any initialisation options go here
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();
    var countryData1 = iti1.getSelectedCountryData();
    this.trialPhoneNumber1 = 91;
    this.phoneNumberCountryNm1 = "in";
    input1.addEventListener('countrychange', (e) => {
      //change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      console.log(this.trialPhoneNumber1);
    });
  }

  changeCalibratedAt() {
    for (var i = 0, l = Object.keys(this.requestAndInwardTypeObj).length; i < l; i++) {
      if (this.requestAndInwardTypeObj[i].inwardTypeName == this.inwardType) {
        this.reqAndInwardObject.inwardType = this.requestAndInwardTypeObj[i].inwardTypeId;
      }
    }

    switch (this.reqAndInwardObject.inwardType) {
      case 1: this.universal = "UCSPL Lab"; this.displayDCDetails = 1; break;
      case 2: this.universal = "On Site"; this.displayDCDetails = 0; break;
      case 3: this.universal = "UCSPL Lab"; this.displayDCDetails = 1; break;
      case 4: this.universal = "On Site"; this.displayDCDetails = 0; break;
      case 5: this.universal = "UCSPL Lab"; break;
      default: break;
    }


  }

  setCustomerName() {
    this.shippedTo = this.customerName;
    this.reportsInNameof = this.customerName;
  }

  addOneToDocumentNumber() {

    this.prefixForInward = this.reqAndInwardObject.reqInwDocumentNo.slice(0, 10);
    this.dateOfDocumentNo = this.reqAndInwardObject.reqInwDocumentNo.slice(10, 18);
    this.counterForDocumentNoString = this.reqAndInwardObject.reqInwDocumentNo.slice(19, 24);
    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);
    this.dateValue3 = this.datepipe.transform(this.dateValue1, 'yyyyMMdd');



    var d = this.reqAndInwardObject.inwardDate.slice(0, 2)
    var m = this.reqAndInwardObject.inwardDate.slice(3, 5)
    var yy = this.reqAndInwardObject.inwardDate.slice(6, 10)
    var yyyy = parseInt(yy)
    var mm = parseInt(m)
    var dd = parseInt(d)

    var d1 = this.dateOfDocumentNo.slice(6, 8)
    var m1 = this.dateOfDocumentNo.slice(4, 6)
    var yy1 = this.dateOfDocumentNo.slice(0, 4)
    var yyyy1 = parseInt(yy1)
    var mm1 = parseInt(m1)
    var dd1 = parseInt(d1)

    mm = mm - 1;
    mm1 = mm1 - 1;
    var iwdDate = new Date(yyyy, mm, dd);
    var dateOfDocumentNo1 = new Date(yyyy1, mm1, dd1);

    var dateOfDocumentNo2 = d1 + "-" + m1 + "-" + yy1;

    if (dateOfDocumentNo2 != this.reqAndInwardObject.inwardDate) {

      this.createReqAndInwardService.getCounterReqInwDocumentNo(this.reqAndInwardObject).subscribe(data => {
        console.log(data);

        var data1 = data.number;
        if (iwdDate < dateOfDocumentNo1 && data1 != "nothing") {
          this.dateOfDocumentNo = this.datepipe.transform(iwdDate, 'yyyyMMdd');
          console.log("inward date is smaller")

          if (data != null || typeof data != 'undefined') {
            this.prefixForInward1 = data1.slice(0, 10);
            this.dateOfDocumentNo1 = data1.slice(10, 18); //16
            this.counterForDocumentNoString1 = data1.slice(19, 24); //17
            this.counterForDocumentNo1 = parseInt(this.counterForDocumentNoString1);
            this.counterForDocumentNo = this.counterForDocumentNo1 + 1;
          }

          this.docNoGeneration();
        }
        else {
          this.dateOfDocumentNo = this.dateValue3

          console.log("inward date is large")
          if (this.reqAndInwardTypeNameSelected == "InHouse") {
            this.counterForDocumentNo = 1;
          }
          else if (this.reqAndInwardTypeNameSelected == "Onsite") {
            this.counterForDocumentNo = 101;
          }
          else if (this.reqAndInwardTypeNameSelected == "InHouse NR") {
            this.counterForDocumentNo = 301;
          }
          else if (this.reqAndInwardTypeNameSelected == "Onsite NR") {
            this.counterForDocumentNo = 501;
          }
          else if (this.reqAndInwardTypeNameSelected == "Repaired and Returned") {
            this.counterForDocumentNo = 701;
          }
          else {

          }

          this.docNoGeneration();
        }

      })


    }
    else {
      this.dateOfDocumentNo = this.dateValue3
      this.counterForDocumentNo = this.counterForDocumentNo + 1;
      this.docNoGeneration();

    }



  }

  docNoGeneration() {
    var output = [], n = 1, padded;

    padded = ('0000' + this.counterForDocumentNo).slice(-5);
    output.push(padded);

    var a = this.prefixForInward + this.dateOfDocumentNo + "_" + padded;
    this.finalDocumentNumber = a;


    this.reqAndInwardObject.reqInwDocumentNo = a;
    console.log("a :" + a);

    this.inwardReqNo = this.reqAndInwardObject.reqInwDocumentNo.slice(10, 24);
    this.reqAndInwardObject.inwardReqNo = this.inwardReqNo;

    this.createReqAndInwardService.createReqAndInward(this.reqAndInwardObject).subscribe(data => {
      console.log(data);
      this.createReqAndInwardService.setReqAndInwDataFromCreateReq(data);
      alert("Request created successfully !!");
      this.clearForm();
    })

  }
  onSubmit() {

    this.reqAndInwardObject = this.createRequestInwardDocumentForm.value;
    this.reqAndInwardObject.countryPrefixForMobile1 = this.phoneNumberCountryNm1;
    this.reqAndInwardObject.createdBy = this.loginUserName;
    this.reqAndInwardObject.calibratedAt = this.universal;
    this.reqAndInwardObject.noOfInstruments = parseInt(this.noOfInstruments);
    this.reqAndInwardObject.inwardDate = this.createRequestInwardDocumentForm.value.inwardDate
    if (this.reqAndInwardObject.inwardDate.length != 10) {
      this.reqAndInwardObject.inwardDate = this.datepipe.transform(this.reqAndInwardObject.inwardDate, 'dd-MM-yyyy');
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.createRequestInwardDocumentForm.value.customerName) {
        this.reqAndInwardObject.customerName = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.createRequestInwardDocumentForm.value.reportInNameOf) {
        this.reqAndInwardObject.reportInNameOf = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.createRequestInwardDocumentForm.value.shippedTo) {
        this.reqAndInwardObject.shippedTo = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.requestAndInwardTypeObj).length; i < l; i++) {
      if (this.requestAndInwardTypeObj[i].inwardTypeName == this.createRequestInwardDocumentForm.value.inwardType) {
        this.reqAndInwardTypeNameSelected = this.requestAndInwardTypeObj[i].inwardTypeName;
        this.reqAndInwardObject.inwardType = this.requestAndInwardTypeObj[i].inwardTypeId;
      }
    }

    for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
      if (this.reqTypeOfRequestAndInwardObj[i].reqTypeName == this.createRequestInwardDocumentForm.value.reqType) {
        this.reqAndInwardObject.reqType = this.reqTypeOfRequestAndInwardObj[i].reqTypeId;
      }
    }

    if (typeof this.reqContactPhone != 'undefined') {
      this.reqAndInwardObject.reqContactPhone = "+" + this.trialPhoneNumber1 + this.reqContactPhone;
    }
    else {
      this.reqAndInwardObject.reqContactPhone = null;
    }


    this.reqAndInwardObject.draft = 1;
    this.reqAndInwardObject.approved = 0;
    this.reqAndInwardObject.submitted = 0;
    this.reqAndInwardObject.rejected = 0;
    this.reqAndInwardObject.archieved = 0;
    this.reqAndInwardObject.typeOfDocument = "order_accept_v1.xsl";

    this.createReqAndInwardService.setPhoneNoCountryFlag(this.reqAndInwardObject.countryPrefixForMobile1);
    this.reqAndInwardObject.dateCreated = this.datepipe.transform(this.dateValue, 'yyyy-MM-dd');


    this.createReqAndInwardService.getReqInwDocumentNo(this.reqAndInwardObject.inwardType).subscribe(data => {
      console.log(data);
      this.reqAndInwardObject.reqInwDocumentNo = data.number;
      this.addOneToDocumentNumber();


    })

  }

  clearForm() {
    this.router.navigateByUrl('/createReqDoc')
  }

}