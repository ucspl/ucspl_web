import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReqdocParaDetailsComponent } from './update-reqdoc-para-details.component';

describe('UpdateReqdocParaDetailsComponent', () => {
  let component: UpdateReqdocParaDetailsComponent;
  let fixture: ComponentFixture<UpdateReqdocParaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReqdocParaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReqdocParaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
