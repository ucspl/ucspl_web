import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReqdocMultiparaDetailsComponent } from './update-reqdoc-multipara-details.component';

describe('UpdateReqdocMultiparaDetailsComponent', () => {
  let component: UpdateReqdocMultiparaDetailsComponent;
  let fixture: ComponentFixture<UpdateReqdocMultiparaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReqdocMultiparaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReqdocMultiparaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
