import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { CreateReqAndInward, CreateReqAndInwardService, CustomerInstrumentIdentification, DynamicGridReqdocParaDetails, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, ReqDocnoAndCustInstruRelationship, RequestAndInwardDocument, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

@Component({
  selector: 'app-update-reqdoc-multipara-details',
  templateUrl: './update-reqdoc-multipara-details.component.html',
  styleUrls: ['./update-reqdoc-multipara-details.component.css']
})
export class UpdateReqdocMultiparaDetailsComponent implements OnInit {


  public reqdocMultiParaDetailsForm: FormGroup;

  text = 'SAVE';

  p: number = 1;
  itemsPerPage: number = 5;

  requestAndInwardDocObj: RequestAndInwardDocument = new RequestAndInwardDocument();

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();

  requestAndInwardDocSaveObj2: any;

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];


  dynamicArrayMp: DynamicGridReqdocParaDetails = new DynamicGridReqdocParaDetails();


  requestAndInwardDocSaveObj: RequestAndInwardDocument = new RequestAndInwardDocument();

  customerInstrumentIdentificationObj: CustomerInstrumentIdentification = new CustomerInstrumentIdentification();

  reqDocnoAndCustInstruRelationshipObj: ReqDocnoAndCustInstruRelationship = new ReqDocnoAndCustInstruRelationship();


  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();

  srNoInstruListForReqIwdArray: Array<any> = [];
  srNoInstruListForReqIwdArray1: Array<any> = [];
  srNoInstruListForReqIwdArray2: Array<any> = [];

  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];


  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray2: Array<DynamicGridReqdocParaDetails> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  dateValue1 = new Date();
  dateValue3: any;

  byDefault: string;
  parameterToSelect: string;
  inwardReqNo: any;
  iwdInstruDetailSrNo: any;
  counterForDocumentNoString: any;
  prefixDate: any;
  suffixCounter: any;
  counterForDocumentNo: number;
  finalDocumentNumber: string;
  reqInwDocumentNo: any;
  n: any;
  w: number;
  nomenclatureName: any;
  windowRef1 = null;
  inwardInstruNo: string;
  parameterNo: string;
  parameter: string;

  dynamicArrayTrial: Array<DynamicGridReqdocParaDetails> = [];
  leng: any;
  custInstruIdentificationList: any;
  forAccuValue: any;

  constructor(private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService) {

  }
  get formArr() {
    return this.reqdocMultiParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({

      inwardInstruNo: [""],
      nomenclature: [""],
      statusOfOrder: [""],
      parameter: [this.dynamicArrayMp.parameter],
      parameterNumber: [""],
      idNo: [""],
      make: [""],
      srNo: [""],
      rangeFrom: [""],
      rangeTo: [""],
      ruom: [""],
      leastCount: [""],
      lcuom1: [""],
      accuracy: [""],
      formulaAccuracy: [""],
      uomAccuracy: [""],
      accuracy1: [""],
      formulaAccuracy1: [""],
      uomAccuracy1: [""],
      location: [""],
      calibrationFrequency: [""],
      instruType: [""],
      labType: [""],
      active: [false]
    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());

  }

  color() {
    document.getElementById("btn-" + (this.leng - 1)).style.backgroundColor = "#d9534f";
  }
  deleteRow1(index: number) {
    this.formArr.removeAt(index);
  }

  ngAfterViewInit() {
    this.leng = (this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (typeof this.dynamicArrayTrial != 'undefined') {
        if (this.dynamicArrayTrial != null) {
          if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
            document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
          }
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }

      (<HTMLInputElement>document.getElementById("selectTag-" + l)).disabled = false;
      (<HTMLInputElement>document.getElementById("selectTag1-" + l)).disabled = false;


    }
  }

  ngOnInit() {

    this.dynamicArrayMp = JSON.parse(localStorage.getItem('arrayForMultiPara'));
    console.log(this.dynamicArrayMp);
    this.requestAndInwardDocSaveObj.inwardInstrumentNo = this.dynamicArrayMp.inwardInstruNo;
    this.inwardInstruNo = this.requestAndInwardDocSaveObj.inwardInstrumentNo;
    this.nomenclatureName = this.dynamicArrayMp.nomenclature;
    this.parameterNo = this.dynamicArrayMp.parameterNumber;
    this.parameter = this.dynamicArrayMp.parameter;


    this.reqAndInwardDataObj = JSON.parse(localStorage.getItem('reqAndInwardDataObj1'));
    console.log(this.reqAndInwardDataObj);
    this.reqInwDocumentNo = this.reqAndInwardDataObj.reqInwDocumentNo;
    this.inwardReqNo = this.reqAndInwardDataObj.inwardReqNo;

    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterName.push(this.instruListForRequestAndInwardObj[i].parameterName);
      }
    })

    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })

    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    })


    this.dynamicArrayTrial = JSON.parse(localStorage.getItem('multiDynamicArray'));
    console.log(this.dynamicArrayTrial);

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {

      this.reqdocMultiParaDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array([this.initRows()])

      });

      this.arrayUomNameAccorToParam2 = JSON.parse(localStorage.getItem('arrayUomNameAccorToParam1ForMultiPara'));
      if (this.arrayUomNameAccorToParam2 != null || typeof this.arrayUomNameAccorToParam2 != 'undefined') {
        this.arrayUomNameAccorToParam1 = this.arrayUomNameAccorToParam2;
      }

    } else {

      this.reqdocMultiParaDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({ inwardInstruNo,
            nomenclature,
            statusOfOrder,
            parameter,
            parameterNumber,
            idNo,
            make,
            srNo,
            rangeFrom,
            rangeTo,
            ruom,
            leastCount,
            lcuom1,
            accuracy,
            formulaAccuracy,
            uomAccuracy,
            accuracy1,
            formulaAccuracy1,
            uomAccuracy1,
            location,
            calibrationFrequency,
            instruType,
            labType,
            active
          }) =>
            this.formBuilder.group({

              inwardInstruNo: [inwardInstruNo],
              nomenclature: [nomenclature],
              statusOfOrder: [statusOfOrder],
              parameter: [parameter],
              parameterNumber: [parameterNumber],
              idNo: [idNo],
              make: [make],
              srNo: [srNo],
              rangeFrom: [rangeFrom],
              rangeTo: [rangeTo],
              ruom: [ruom],
              leastCount: [leastCount],
              lcuom1: [lcuom1],
              accuracy: [accuracy],
              formulaAccuracy: [formulaAccuracy],
              uomAccuracy: [uomAccuracy],
              accuracy1: [accuracy1],
              formulaAccuracy1: [formulaAccuracy1],
              uomAccuracy1: [uomAccuracy1],
              location: [location],
              calibrationFrequency: [calibrationFrequency],
              instruType: [instruType],
              labType: [labType],
              active: [active]

            })
          )
        )

      })

      this.leng = (this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).length;
      this.arrayUomNameAccorToParam2 = JSON.parse(localStorage.getItem('arrayUomNameAccorToParam1ForMultiPara'));
      if (this.arrayUomNameAccorToParam2 != null || typeof this.arrayUomNameAccorToParam2 != 'undefined') {
        this.arrayUomNameAccorToParam1 = this.arrayUomNameAccorToParam2;
      }
    }




    (this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);


      if (this.dynamicArrayTrial != null) {


        for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].inwardInstruNo !== "") {
            if ((this.dynamicArrayTrial[i].nomenclature !== values[i].nomenclature || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].make !== values[i].make || this.dynamicArrayTrial[i].srNo !== values[i].srNo
              || this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].ruom !== values[i].ruom || this.dynamicArrayTrial[i].leastCount !== values[i].leastCount
              || this.dynamicArrayTrial[i].lcuom1 !== values[i].lcuom1 || this.dynamicArrayTrial[i].accuracy !== values[i].accuracy || this.dynamicArrayTrial[i].formulaAccuracy !== values[i].formulaAccuracy || this.dynamicArrayTrial[i].uomAccuracy !== values[i].uomAccuracy
              || this.dynamicArrayTrial[i].accuracy1 !== values[i].accuracy1 || this.dynamicArrayTrial[i].formulaAccuracy1 !== values[i].formulaAccuracy1 || this.dynamicArrayTrial[i].uomAccuracy1 !== values[i].uomAccuracy1 || this.dynamicArrayTrial[i].location !== values[i].location
              || this.dynamicArrayTrial[i].calibrationFrequency !== values[i].calibrationFrequency || this.dynamicArrayTrial[i].instruType !== values[i].instruType || this.dynamicArrayTrial[i].labType !== values[i].labType)
            ) {
              console.log("change" + i);
              var o = this.reqdocMultiParaDetailsForm.get('Rows').value[i].active;
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";

            }

          }
        }
      }
    });


    for (var i = 0, l = (this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).length; i < l; i++) {
      ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('parameter').patchValue(this.parameter);

    }

    this.newDynamic1 = [{
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: ""

    }]


    this.newDynamic = {
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: ""

    };
    this.dynamicArray.push(this.newDynamic);

  }


  //////////////////////////////////////////////// code for adding new row //////////////////////////////////

  toClose() {
    window.localStorage.removeItem('multiDynamicArray');
    self.close();
  }

  iwdInstruDetailsSrNo() {

    this.iwdInstruDetailSrNo = this.inwardReqNo;
    this.prefixDate = this.iwdInstruDetailSrNo.slice(0, 9);
    console.log("this.prefixDate " + this.prefixDate)

    this.counterForDocumentNoString = this.iwdInstruDetailSrNo.slice(9, 14);
    console.log("this.counterForDocumentNoString " + this.counterForDocumentNoString)
    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);

    this.iwdInstruDetailSrNo = this.prefixDate + this.counterForDocumentNo;
    console.log(this.iwdInstruDetailSrNo);

  }


  ///////////////////////////////////// save data /////////////////////////////////////////

  saveAllTrial(i) {

    var j = i + 1;
    var z = i + 2;
    var srNoCount1 = parseInt((this.itemsPerPage * (this.p - 1)) + (j));

    var o = this.reqdocMultiParaDetailsForm.get('Rows').value[i].active;

    console.log(this.dynamicArray);

    var sr = this.inwardInstruNo + "_" + srNoCount1;
    var final = this.inwardInstruNo + "_" + z;

    ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('parameterNumber').patchValue(final);
    this.requestAndInwardDocSaveObj.parameterNumber = final;

    if (o == false) {
      ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('active').patchValue(!o);
    }
    else {

    }


    var s = this.reqdocMultiParaDetailsForm.get('Rows').value;
    this.dynamicArray = this.reqdocMultiParaDetailsForm.get('Rows').value;
    console.log(this.dynamicArray);
    this.saveAllFC(i);

  }

  saveAllFC(e) {

    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";

    this.saveAllDataFC(e);

  }

  setDynamicArrayFC() {

    localStorage.setItem('multiDynamicArray', JSON.stringify(this.dynamicArray));

  }

  windowRef1FC = null;


  checkIfSrNoAlreadyPresentFC(e) {

    if (this.srNoInstruListForReqIwdArray.length == 0) {
      this.createReqAndInwardService.createReqAndInwardDocument(this.requestAndInwardDocSaveObj).subscribe(data => {
        console.log("*********data saved********" + data);
        alert("all instru new data created!!");
      })

    }

    else {

      for (this.w = 0; this.w < this.srNoInstruListForReqIwdArray.length; this.w++) {
        if (this.srNoInstruListForReqIwdArray[this.w][6] == this.requestAndInwardDocSaveObj.parameterNumber) {
          this.createReqAndInwardService.updateReqAndInwardDocument(this.srNoInstruListForReqIwdArray[this.w][0], this.requestAndInwardDocSaveObj).subscribe(data => {
            console.log("data updated successfully" + data);
            alert("updated data created!!")
          }
          );
          break;
        }
      }


      if (this.w >= this.srNoInstruListForReqIwdArray.length) {
        this.createReqAndInwardService.createReqAndInwardDocument(this.requestAndInwardDocSaveObj).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("new data created!!");
        })
      }

    }


    this.createReqAndInwardService.getSrNoInstruListForReqIwd().subscribe(data => {
      console.log("*********sr nos collection********" + data);                                                                              
      this.srNoInstruListForReqIwdArray = data;

    })

  }


  saveAllDataFC(e) {
    console.log(this.dynamicArray);
    var j = e + 1;
    var count = e + 2;

    this.requestAndInwardDocSaveObj.createReqAndIwdTableId = this.reqAndInwardDataObj.id;
    this.requestAndInwardDocSaveObj.reqInwDocumentNo = this.reqInwDocumentNo;
    this.requestAndInwardDocSaveObj.inwardReqNo = this.inwardReqNo;
    this.dynamicArray[e].nomenclature = this.nomenclatureName;
    this.requestAndInwardDocSaveObj.isMultiPara=1;

    for (var k = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; k < l; k++) {

      if (this.dynamicArray[e].nomenclature == this.arrayInstrumentListName[k]) {
        this.requestAndInwardDocSaveObj.nomenclature = this.arrayInstrumentListId[k];
        break;
      }
    }

    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {

      if (this.dynamicArray[e].parameter == this.arrayParameterNames[k]) {
        this.requestAndInwardDocSaveObj.parameter = this.arrayParameterId[k];
        break;
      }
    }

    this.requestAndInwardDocSaveObj.statusOfOrder = this.dynamicArrayMp.statusOfOrder;
    this.requestAndInwardDocSaveObj.inwardInstrumentNo = this.inwardInstruNo;
    this.dynamicArray[e].inwardInstruNo = this.inwardInstruNo;
    this.requestAndInwardDocSaveObj.idNo = this.dynamicArrayMp.idNo
    this.requestAndInwardDocSaveObj.make = this.dynamicArrayMp.make
    this.requestAndInwardDocSaveObj.srNo = this.dynamicArrayMp.srNo

    // this.requestAndInwardDocSaveObj.rangeFrom = this.dynamicArray[e].rangeFrom
    // this.requestAndInwardDocSaveObj.rangeTo = this.dynamicArray[e].rangeTo


    var check = parseFloat(this.dynamicArray[e].rangeFrom);
    if( check < 0 || check >= 0){
      this.requestAndInwardDocSaveObj.rangeFrom = this.dynamicArray[e].rangeFrom
    }
    else{
      this.requestAndInwardDocSaveObj.rangeFrom = "0";
      this.dynamicArray[e].rangeFrom ="0"
    }


    // this.requestAndInwardDocSaveObj.rangeTo = this.dynamicArray[e].rangeTo
    var check1 = parseFloat(this.dynamicArray[e].rangeTo);
    if( check1 < 0 || check1 >= 0){
      this.requestAndInwardDocSaveObj.rangeTo = this.dynamicArray[e].rangeTo
    }
    else{
      this.requestAndInwardDocSaveObj.rangeTo = "0";
      this.dynamicArray[e].rangeTo="0"
    }



    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].ruom == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.ruom = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.ruom = 1;
      }
    }

    this.requestAndInwardDocSaveObj.leastCount = this.dynamicArray[e].leastCount;
  //  this.requestAndInwardDocSaveObj.leastCount = parseFloat(this.requestAndInwardDocSaveObj.leastCount);
    if (this.requestAndInwardDocSaveObj.leastCount == "NaN") {
      this.requestAndInwardDocSaveObj.leastCount = null;
    }



    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].lcuom1 == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.lcuom1 = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.lcuom1 = 1;
      }
    }

    this.requestAndInwardDocSaveObj.accuracy = this.dynamicArray[e].accuracy

    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {

      if (this.dynamicArray[e].formulaAccuracy == this.arrayFormulaAccuracyName[k]) {
        this.requestAndInwardDocSaveObj.formulaAccuracy = this.arrayFormulaAccuracyId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.formulaAccuracy = 1;
      }
    }


    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].uomAccuracy == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.uomAccuracy = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.uomAccuracy = 1;
      }
    }


    this.requestAndInwardDocSaveObj.accuracy1 = this.dynamicArray[e].accuracy1;
    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {

      if (this.dynamicArray[e].formulaAccuracy1 == this.arrayFormulaAccuracyName[k]) {
        this.requestAndInwardDocSaveObj.formulaAccuracy1 = this.arrayFormulaAccuracyId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.formulaAccuracy1 = 1;
      }
    }

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].uomAccuracy1 == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.uomAccuracy1 = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.uomAccuracy1 = 1;
      }
    }


    this.requestAndInwardDocSaveObj.location = this.dynamicArrayMp.location
    if (this.requestAndInwardDocSaveObj.location == "") {
      this.requestAndInwardDocSaveObj.location = null;
    }

    this.requestAndInwardDocSaveObj.calibrationFrequency = this.dynamicArrayMp.calibrationFrequency
    if (this.requestAndInwardDocSaveObj.calibrationFrequency == "") {
      this.requestAndInwardDocSaveObj.calibrationFrequency = null;
    }

    this.requestAndInwardDocSaveObj.instrumentType = this.dynamicArrayMp.instruType
    if (this.requestAndInwardDocSaveObj.instrumentType == "") {
      this.requestAndInwardDocSaveObj.instrumentType = null;
    }

    this.requestAndInwardDocSaveObj.labType = this.dynamicArray[e].labType
    if (this.requestAndInwardDocSaveObj.labType == "") {
      this.requestAndInwardDocSaveObj.labType = null;
    }



    this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;
    this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
    this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
    this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;


    this.createReqAndInwardService.sendSrNoInstruIdenListForReqIwd(this.customerInstrumentIdentificationObj).subscribe(data => {
      console.log("*********sr no for doc no********" + data);

      this.createReqAndInwardService.getSrNoInstruIdenListForReqIwd().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.srNoInstruListForReqIwdArray = data;

        this.createReqAndInwardService.sendSrNoInstruListForReqIwd(this.srNoInstruListForReqIwdArray[0][1]).subscribe(data => {
          console.log("*********sr no for doc no********" + data);

        })

        this.createReqAndInwardService.getSrNoInstruListForReqIwd().subscribe(data => {
          console.log("*********sr nos collection********" + data);
          this.srNoInstruListForReqIwdArray2 = data;

          this.checkIfSrNoAlreadyPresentFC1(e);
        })

      })
    })


    console.log("this.callToFunction()");
    this.dynamicArrayTrial = this.dynamicArray;
  }


  checkIfSrNoAlreadyPresentFC1(e) {

    if (this.srNoInstruListForReqIwdArray.length == 0) {

      alert("First fill single parameter details then fill multiparadetails...")


    }
    else {

      for (this.w = 0; this.w < this.srNoInstruListForReqIwdArray2.length; this.w++) {


        if (this.srNoInstruListForReqIwdArray2[this.w][7] == this.requestAndInwardDocSaveObj.parameterNumber && this.srNoInstruListForReqIwdArray[this.w][70] == 1) {

          this.customerInstrumentIdentificationObj.custInstruIdentificationId = this.srNoInstruListForReqIwdArray[this.w][1]
          this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;
          this.customerInstrumentIdentificationObj.reqDocId = this.srNoInstruListForReqIwdArray[this.w][0];
          this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
          this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
          this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;


          this.requestAndInwardDocSaveObj.custInstruIdentificationId = this.customerInstrumentIdentificationObj.custInstruIdentificationId;

          this.createReqAndInwardService.updateReqAndInwardDocument(this.srNoInstruListForReqIwdArray[this.w][0], this.requestAndInwardDocSaveObj).subscribe(data => {
            console.log("data updated successfully" + data);
            alert("updated data created!!")
          })


          break;
        }


      }
      if (this.w >= this.srNoInstruListForReqIwdArray2.length) {

        this.requestAndInwardDocSaveObj.custInstruIdentificationId = this.srNoInstruListForReqIwdArray[0][1];
        this.reqDocnoAndCustInstruRelationshipObj.multipleParameter = 1;
        this.createReqAndInwardService.createReqAndInwardDocument(this.requestAndInwardDocSaveObj).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("new data created!!");
          this.requestAndInwardDocSaveObj2 = data;

          this.reqDocnoAndCustInstruRelationshipObj.reqDocId = this.requestAndInwardDocSaveObj2.id;
          this.reqDocnoAndCustInstruRelationshipObj.custInstruIdentificationId = this.srNoInstruListForReqIwdArray[0][1];
          this.reqDocnoAndCustInstruRelationshipObj.createReqAndIwdTableId = this.requestAndInwardDocSaveObj.createReqAndIwdTableId;
          this.reqDocnoAndCustInstruRelationshipObj.reqInwDocumentNo = this.requestAndInwardDocSaveObj2.reqInwDocumentNo;
          this.reqDocnoAndCustInstruRelationshipObj.inwardReqNo = this.requestAndInwardDocSaveObj2.inwardReqNo
          this.reqDocnoAndCustInstruRelationshipObj.inwardInstrumentNo = this.requestAndInwardDocSaveObj2.inwardInstrumentNo
          this.reqDocnoAndCustInstruRelationshipObj.parameterNumber = this.requestAndInwardDocSaveObj2.parameterNumber


          this.createReqAndInwardService.createReqDocNoCustInstruRelationship(this.reqDocnoAndCustInstruRelationshipObj).subscribe(data => {
            console.log("*********data saved in ReqDocNoCustInstruRelationship ********" + data);
            alert("all instru new data created in ReqDocNoCustInstruRelationship!!");
          });
        })

      }


    }

  }

  accuracyValue(i) {
    this.forAccuValue = ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {

      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0);
        console.log(((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0));
      }
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = false;
    }
  }

  accuracyUomValue(i) {
    this.forAccuValue = ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = false;
    }

  }

  accuracyValue1(i) {
    this.forAccuValue = ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {

      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0);
        console.log(((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0));
      }
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = false;
    }
  }

  accuracyUomValue1(i) {
    this.forAccuValue = ((this.reqdocMultiParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = false;
    }

  }

}
