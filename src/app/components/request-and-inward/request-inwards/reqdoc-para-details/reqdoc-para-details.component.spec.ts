import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqdocParaDetailsComponent } from './reqdoc-para-details.component';

describe('ReqdocParaDetailsComponent', () => {
  let component: ReqdocParaDetailsComponent;
  let fixture: ComponentFixture<ReqdocParaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqdocParaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqdocParaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
