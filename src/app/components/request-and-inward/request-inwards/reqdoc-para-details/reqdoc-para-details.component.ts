import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateReqAndInward, CreateReqAndInwardService, ReqDocnoAndCustInstruRelationship, CustomerInstrumentIdentification, DynamicGridReqdocParaDetails, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, RequestAndInwardDocument, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { CreateReqdocComponent } from '../create-reqdoc/create-reqdoc.component';

declare var $: any;
@Component({
  selector: 'app-reqdoc-para-details',
  templateUrl: './reqdoc-para-details.component.html',
  styleUrls: ['./reqdoc-para-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReqdocParaDetailsComponent implements OnInit {


  name = new FormControl('');
  category = new FormControl('');

  searchTerm = '';

  text = 'SAVE';

  frm: FormGroup;
  public reqdocParaDetailsForm: FormGroup;

  p: number = 1;
  itemsPerPage: number = 5;
  reqDocnoAndCustInstruRelationshipObj: ReqDocnoAndCustInstruRelationship = new ReqDocnoAndCustInstruRelationship();

  customerInstrumentIdentificationObj: CustomerInstrumentIdentification = new CustomerInstrumentIdentification();

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();

  requestAndInwardDocObj: RequestAndInwardDocument = new RequestAndInwardDocument();
  requestAndInwardDocSaveObj: RequestAndInwardDocument = new RequestAndInwardDocument();
  requestAndInwardDocObj1: any;
  requestAndInwardDocSaveObj2: any;

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayParameterId: Array<any> = []; 
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  srNoInstruListForReqIwdArray: Array<any> = [];
  srNoInstruListForReqIwdArray1: Array<any> = [];

  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray2: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArrayTrial: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArrayForCustInstruId: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArrayForCustInstruIdMultiPara: Array<DynamicGridReqdocParaDetails> = [];

  multiDynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  dateValue1 = new Date();
  dateValue3: any;

  searchText: String = '';


  @Input() data: any[]
  byDefault: string;
  parameterToSelect: any;
  inwardReqNo: any;
  iwdInstruDetailSrNo: any;
  counterForDocumentNoString: any;
  prefixDate: any;
  suffixCounter: any;
  counterForDocumentNo: number;
  finalDocumentNumber: string;
  reqInwDocumentNo: any;
  n: any;
  w: number;
  noOfInstruments: any;
  leng: number;
  at: any;
  itemsPerPage1: number;
  custInstruIdentificationList: any;
  forAccuValue: any;
  custInstruId: any;
  docNo1: number;

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createReqAndInwardService: CreateReqAndInwardService) {
    this.frm = this.formBuilder.group({
      cName: [''],
    })

  }

  changeItemPerPage() {
    this.itemsPerPage1 = this.itemsPerPage;
    this.itemsPerPage = 500;
    this.p = 1;
  }



  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.reqdocParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      inwardInstruNo: [""],
      nomenclature: ["", Validators.required],
      statusOfOrder: ["Order created"],
      parameter: [""],
      parameterNumber: [""],
      idNo: ["", Validators.required],
      make: [""],
      srNo: [""],
      rangeFrom: [null],
      rangeTo: [null],
      ruom: [],
      leastCount: [null],
      lcuom1: [],
      accuracy: [null],
      formulaAccuracy: [],
      uomAccuracy: [],
      accuracy1: [null],
      formulaAccuracy1: [],
      uomAccuracy1: [],
      location: [""],
      calibrationFrequency: [""],
      instruType: [""],
      labType: [""],
      active: [false],
      custInstruIdentificationId: []
    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow1(index: number) {
    this.formArr.removeAt(index);
  }

  toClose() {

    localStorage.setItem('arrayUomNameAccorToParam1ForReqAndIwd', JSON.stringify(this.arrayUomNameAccorToParam1));
    parent.postMessage(this.frm.value, location.origin);
    opener.location.reload(true);
    self.close();
  }


  parameterChange(e) {
    console.log("parameter function" + e.value);
    // alert(e);
    this.parameterToSelect = this.dynamicArray[e].nomenclature;
    for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
      if (this.arrayInstrumentListName[i] == this.parameterToSelect) {
        this.dynamicArray[e].parameter = this.arrayParameterIdFromNom[i];

        for (var ii = 0, ll = Object.keys(this.ParameterListForReqAndIwdObj).length; ii < ll; ii++) {
          if (this.dynamicArray[e].parameter == this.arrayParameterId[ii]) {
            this.dynamicArray[e].parameter = this.arrayParameterNames[ii]
          }
        }

      }
    }
  }

  onChange(e) {

    var arrayUomNameAccor = new Array();

    e = this.itemsPerPage * (this.p - 1) + e;
    console.log(this.reqdocParaDetailsForm.get('Rows').value[e].nomenclature);
    this.parameterToSelect = this.reqdocParaDetailsForm.get('Rows').value[e].nomenclature;
    for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
      if (this.arrayInstrumentListName[i] == this.parameterToSelect) {

        for (var ii = 0, ll = Object.keys(this.ParameterListForReqAndIwdObj).length; ii < ll; ii++) {
          if (this.arrayParameterIdFromNom[i] == this.arrayParameterId[ii]) {
            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('parameter').patchValue(this.arrayParameterNames[ii]);

            for (var iii = 0, lll = Object.keys(this.uomListForRequestAndInwardObj).length; iii < lll; iii++) {
              if (this.uomListForRequestAndInwardObj[iii].parameterId == this.arrayParameterId[ii]) {
                arrayUomNameAccor.push(this.uomListForRequestAndInwardObj[iii].uomName)

              }
            }
          }
        }

      }
    }

    this.arrayUomNameAccorToParam1[e] = arrayUomNameAccor;
    localStorage.setItem('arrayUomNameAccorToParam1ForReqAndIwd', JSON.stringify(this.arrayUomNameAccorToParam1));
  }

  removeLastIndex() {

    console.log((this.reqdocParaDetailsForm.get('Rows') as FormArray).value);
  }


  chnageButtonColor() {

    this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial[l].inwardInstruNo !== "") {
        document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
      }
    }
  }

  ngAfterViewInit() {
    this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null) {
        if (this.dynamicArrayTrial[l].nomenclature != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
      }

      (<HTMLInputElement>document.getElementById("selectTag-" + l)).disabled = false;
      (<HTMLInputElement>document.getElementById("selectTag1-" + l)).disabled = false;


    }

  }

  ngDestroy() {
    localStorage.setItem('arrayUomNameAccorToParam1ForReqAndIwd', JSON.stringify(this.arrayUomNameAccorToParam1));
  }

  ngOnInit() {



    this.inwardReqNo = JSON.parse(localStorage.getItem('inwardReqNo'));
    console.log(this.inwardReqNo);


    this.reqInwDocumentNo = JSON.parse(localStorage.getItem('reqInwDocumentNo'));
    console.log(this.reqInwDocumentNo);


    this.reqAndInwardDataObj = JSON.parse(localStorage.getItem('reqAndInwardDataObj1'));
    this.reqAndInwardDataObj.inwardReqNo = this.inwardReqNo;
    localStorage.setItem('reqAndInwardDataObj1', JSON.stringify(this.reqAndInwardDataObj));
    console.log(this.reqAndInwardDataObj);


    this.noOfInstruments = this.reqAndInwardDataObj.noOfInstruments;





    this.dynamicArrayTrial = JSON.parse(localStorage.getItem('dynamicArrayForReqAndIwd'));
    console.log(this.dynamicArrayTrial);

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.reqdocParaDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        Rows: this.formBuilder.array([this.initRows()])

      });

      this.noOfRowsAccordingToNoOfInstu();

    } else {


      this.reqdocParaDetailsForm = this.formBuilder.group({
        itemsPerPage: [5],
        accountSearch: '',
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({ inwardInstruNo,
            nomenclature,
            statusOfOrder,
            parameter,
            parameterNumber,
            idNo,
            make,
            srNo,
            rangeFrom,
            rangeTo,
            ruom,
            leastCount,
            lcuom1,
            accuracy,
            formulaAccuracy,
            uomAccuracy,
            accuracy1,
            formulaAccuracy1,
            uomAccuracy1,
            location,
            calibrationFrequency,
            instruType,
            labType,
            active,
            custInstruIdentificationId
          }) =>
            this.formBuilder.group({

              inwardInstruNo: [inwardInstruNo],
              nomenclature: [nomenclature],
              statusOfOrder: [statusOfOrder],
              parameter: [parameter],
              parameterNumber: [parameterNumber],
              idNo: [idNo],
              make: [make],
              srNo: [srNo],
              rangeFrom: [rangeFrom],
              rangeTo: [rangeTo],
              ruom: [ruom],
              leastCount: [leastCount],
              lcuom1: [lcuom1],
              accuracy: [accuracy],
              formulaAccuracy: [formulaAccuracy],
              uomAccuracy: [uomAccuracy],
              accuracy1: [accuracy1],
              formulaAccuracy1: [formulaAccuracy1],
              uomAccuracy1: [uomAccuracy1],
              location: [location],
              calibrationFrequency: [calibrationFrequency],
              instruType: [instruType],
              labType: [labType],
              active: [active],
              custInstruIdentificationId: [custInstruIdentificationId]

            })
          )
        )

      })




      this.removeLastIndex();
      this.arrayUomNameAccorToParam2 = JSON.parse(localStorage.getItem('arrayUomNameAccorToParam1ForReqAndIwd'));
      if (this.arrayUomNameAccorToParam2 != null || typeof this.arrayUomNameAccorToParam2 != 'undefined') {
        this.arrayUomNameAccorToParam1 = this.arrayUomNameAccorToParam2;
      }

    }


    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);


      if (this.dynamicArrayTrial != null) {


        for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].inwardInstruNo !== "") {
            if ((this.dynamicArrayTrial[i].nomenclature !== values[i].nomenclature || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].make !== values[i].make || this.dynamicArrayTrial[i].srNo !== values[i].srNo
              || this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].ruom !== values[i].ruom || this.dynamicArrayTrial[i].leastCount !== values[i].leastCount
              || this.dynamicArrayTrial[i].lcuom1 !== values[i].lcuom1 || this.dynamicArrayTrial[i].accuracy !== values[i].accuracy || this.dynamicArrayTrial[i].formulaAccuracy !== values[i].formulaAccuracy || this.dynamicArrayTrial[i].uomAccuracy !== values[i].uomAccuracy
              || this.dynamicArrayTrial[i].accuracy1 !== values[i].accuracy1 || this.dynamicArrayTrial[i].formulaAccuracy1 !== values[i].formulaAccuracy1 || this.dynamicArrayTrial[i].uomAccuracy1 !== values[i].uomAccuracy1 || this.dynamicArrayTrial[i].location !== values[i].location
              || this.dynamicArrayTrial[i].calibrationFrequency !== values[i].calibrationFrequency || this.dynamicArrayTrial[i].instruType !== values[i].instruType || this.dynamicArrayTrial[i].labType !== values[i].labType)
            ) {
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;

              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";


            }

          }
        }
      }
    });




    $("input[name=TypeList]").change(function () {
      alert($(this).val());
    });



    this.iwdInstruDetailsSrNo();


    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterIdFromNom.push(this.instruListForRequestAndInwardObj[i].parameterId);
      }
    })


    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    })


    this.newDynamic1 = [{
      // trial:"",
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "Order created",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: false
    }]


    this.newDynamic = {
      // trial:"",
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: false
    };
    this.dynamicArray.push(this.newDynamic);


  }

  noOfRowsAccordingToNoOfInstu() {
    for (var r = 0; r < this.noOfInstruments - 1; r++) {
      this.addNewRow()
    }
  }


  iwdInstruDetailsSrNo() {

    this.iwdInstruDetailSrNo = this.inwardReqNo;
    this.prefixDate = this.iwdInstruDetailSrNo.slice(0, 9);
    console.log("this.prefixDate " + this.prefixDate)


    this.counterForDocumentNoString = this.iwdInstruDetailSrNo.slice(9, 14);
    console.log("this.counterForDocumentNoString " + this.counterForDocumentNoString);
    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);

    this.iwdInstruDetailSrNo = this.prefixDate + this.counterForDocumentNo;
    console.log(this.iwdInstruDetailSrNo);

  }


  RefreshParent() {
    if (window.opener != null && !window.opener.closed) {
      window.opener.location.reload();
    }
  }



  //////////////////////////////////////////////// code for adding new row //////////////////////////////////




  accuracyValue(i) {
    this.forAccuValue = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0);
        console.log(((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy').patchValue(0));
      }

      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = true;

    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag-" + i)).disabled = false;
    }

  }

  accuracyUomValue(i) {
    this.forAccuValue = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA-" + i)).disabled = false;
    }

  }

  accuracyValue1(i) {
    this.forAccuValue = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('formulaAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      if (this.forAccuValue == "1/10 DIN" || this.forAccuValue == "1/3 DIN" || this.forAccuValue == "CLASS A" || this.forAccuValue == "CLASS B" || this.forAccuValue == "CLASS C" || this.forAccuValue == "Not Specified") {
        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0);
        console.log(((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('accuracy1').patchValue(0));
      }
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTag1-" + i)).disabled = false;
    }
  }


  accuracyUomValue1(i) {
    this.forAccuValue = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uomAccuracy1').value;
    if (this.forAccuValue != null && typeof this.forAccuValue != 'undefined' && this.forAccuValue != "") {
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = true;
    }
    else {
      (<HTMLInputElement>document.getElementById("selectTagFA1-" + i)).disabled = false;
    }

  }


  /////////////save all //////////////////////

  saveAll() {

    this.saveAllData()

    this.frm.value.cName = "admin";

    parent.postMessage(this.frm.value, location.origin);
    parent.postMessage(this.dynamicArray, location.origin);
    this.createReqAndInwardService.setSingleParaListTrial(this.dynamicArray);
    this.setDynamicArray();
    opener.location.reload(true);

  }

  setDynamicArray() {

    localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArrayForCustInstruId));

  }

  windowRef1 = null;

  saveAllData() {
    console.log(this.dynamicArray);
    for (var i = 0; i < this.dynamicArray.length; i++) {
      var j = i + 1;
      this.dynamicArray[i].inwardInstruNo = this.iwdInstruDetailSrNo + "_" + j;


    }

    console.log("this.callToFunction()");

    console.log("use this function to save the data of ")

  }



  changeNomclNameToNomcId(i) {
    for (var k = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; k < l; k++) {

      if (this.dynamicArray[i].nomenclature == this.arrayInstrumentListName[k]) {
        this.requestAndInwardDocSaveObj.nomenclature = this.arrayInstrumentListId[k];
        this.requestAndInwardDocSaveObj.parameter = this.arrayParameterIdFromNom[k];
        break;
      }
    }
  }


  saveAllTrial(i) {

    var j = i + 1;
    var srnoCount1 = parseInt((this.itemsPerPage * (this.p - 1)) + (j));

    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('inwardInstruNo').patchValue(this.iwdInstruDetailSrNo + "_" + srnoCount1);


    var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;

    console.log(this.dynamicArray);

    var sr = this.iwdInstruDetailSrNo + "_" + srnoCount1;
    var final = sr + "_" + 1;


    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('parameterNumber').patchValue(final);



    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.dynamicArrayForCustInstruId = this.dynamicArray;

    this.saveAllFC(i);

  }



  saveAllFC(e) {

    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";

    this.saveAllDataFC(e)

    this.frm.value.cName = "admin";

    parent.postMessage(this.frm.value, location.origin);
    parent.postMessage(this.dynamicArray, location.origin);

    this.createReqAndInwardService.setSingleParaListTrial(this.dynamicArray);
    this.setDynamicArrayFC();


  }

  setDynamicArrayFC() {

    localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArrayForCustInstruId));
    this.dynamicArrayTrial = this.dynamicArray;

  }

  windowRef1FC = null;

  openMultiParaListFC(i) {



    window.localStorage.removeItem('multiDynamicArray');

    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    var j = i + 1;
    var k = i + 2;

    var srnoCount = parseInt((this.itemsPerPage * (this.p - 1)) + (j));
    this.dynamicArray[i].inwardInstruNo = this.iwdInstruDetailSrNo + "_" + srnoCount;

    this.dynamicArray[i].parameterNumber = this.dynamicArray[i].inwardInstruNo + "_" + 1;


    this.dynamicArrayForCustInstruIdMultiPara = JSON.parse(localStorage.getItem('dynamicArrayForReqAndIwd'));


    this.docNo1 = this.dynamicArrayForCustInstruIdMultiPara[i].custInstruIdentificationId;


    this.createReqAndInwardService.getMultiParaDetails(this.docNo1).subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.srNoInstruListForReqIwdArray1 = data;

      var t = this.dynamicArray[i].inwardInstruNo + "_" + 2;
      var sn = this.dynamicArray[i].inwardInstruNo;

      if (this.srNoInstruListForReqIwdArray1.length >= 2) {
        for (var s = 1; s < this.srNoInstruListForReqIwdArray1.length; s++) {
          this.newDynamic = {

            inwardInstruNo: this.srNoInstruListForReqIwdArray1[s][5],
            nomenclature: this.srNoInstruListForReqIwdArray1[s][7],
            statusOfOrder: this.srNoInstruListForReqIwdArray1[s][30],
            parameter: this.srNoInstruListForReqIwdArray1[s][8],
            parameterNumber: this.srNoInstruListForReqIwdArray1[s][6],
            idNo: this.srNoInstruListForReqIwdArray1[s][11],
            make: this.srNoInstruListForReqIwdArray1[s][10],
            srNo: this.srNoInstruListForReqIwdArray1[s][4],
            rangeFrom: this.srNoInstruListForReqIwdArray1[s][14],
            rangeTo: this.srNoInstruListForReqIwdArray1[s][15],
            ruom: this.srNoInstruListForReqIwdArray1[s][16],
            leastCount: this.srNoInstruListForReqIwdArray1[s][17],
            lcuom1: this.srNoInstruListForReqIwdArray1[s][18],
            accuracy: this.srNoInstruListForReqIwdArray1[s][19],
            formulaAccuracy: this.srNoInstruListForReqIwdArray1[s][20],
            uomAccuracy: this.srNoInstruListForReqIwdArray1[s][21],
            accuracy1: this.srNoInstruListForReqIwdArray1[s][22],
            formulaAccuracy1: this.srNoInstruListForReqIwdArray1[s][23],
            uomAccuracy1: this.srNoInstruListForReqIwdArray1[s][24],
            location: this.srNoInstruListForReqIwdArray1[s][25],
            calibrationFrequency: this.srNoInstruListForReqIwdArray1[s][26],
            instruType: this.srNoInstruListForReqIwdArray1[s][13],
            labType: this.srNoInstruListForReqIwdArray1[s][27],
            active: this.srNoInstruListForReqIwdArray1[s][28],
            custInstruIdentificationId: this.srNoInstruListForReqIwdArray1[s][32]
          };

          this.multiDynamicArray.push(this.newDynamic);

        }


      }
      else {
        window.localStorage.removeItem('multiDynamicArray');
      }

      if (this.multiDynamicArray != null) {
        localStorage.setItem('multiDynamicArray', JSON.stringify(this.multiDynamicArray));
      }


      localStorage.setItem('arrayForMultiPara', JSON.stringify(this.dynamicArray[i]));
      localStorage.setItem('arrayUomNameAccorToParam1ForMultiPara', JSON.stringify(this.arrayUomNameAccorToParam1[i]));

      this.windowRef1 = window.open('http://localhost:4200/reqDMultiList', "child1", "toolbar=no,location=no,directories=no,status=no,menubar=no,titlebar=no,fullscreen=no,scrollbars=1,resizable=no,width=430,height=220,left=500,top=100");
      this.windowRef1.addEventListener("message", this.receivemessage1FC.bind(this), false);

      this.multiDynamicArray.splice(0, this.multiDynamicArray.length);


    })


  }
  receivemessage1FC(evt: any) {
    console.log(evt.data);
  }



  saveAllDataFC(e) {
    console.log(this.dynamicArray);
    this.customerInstrumentIdentificationObj.custInstruIdentificationId = null;

    var j = e + 1;

    this.requestAndInwardDocSaveObj.createReqAndIwdTableId = this.reqAndInwardDataObj.id;
    this.requestAndInwardDocSaveObj.reqInwDocumentNo = this.reqInwDocumentNo;
    this.requestAndInwardDocSaveObj.inwardReqNo = this.inwardReqNo;


    for (var k = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; k < l; k++) {

      if (this.dynamicArray[e].nomenclature == this.arrayInstrumentListName[k]) {
        this.requestAndInwardDocSaveObj.nomenclature = this.arrayInstrumentListId[k];
        break;
      }
    }

    for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {

      if (this.dynamicArray[e].parameter == this.arrayParameterNames[k]) {
        this.requestAndInwardDocSaveObj.parameter = this.arrayParameterId[k];
        break;
      }
    }

    this.requestAndInwardDocSaveObj.statusOfOrder = this.dynamicArray[e].statusOfOrder;
    var srnoCount1 = parseInt((this.itemsPerPage * (this.p - 1)) + (j));
    this.requestAndInwardDocSaveObj.inwardInstrumentNo = this.iwdInstruDetailSrNo + "_" + srnoCount1;

    this.requestAndInwardDocSaveObj.parameterNumber = this.requestAndInwardDocSaveObj.inwardInstrumentNo + "_" + 1;
    this.requestAndInwardDocSaveObj.idNo = this.dynamicArray[e].idNo
    if (this.requestAndInwardDocSaveObj.idNo == "") {
      this.requestAndInwardDocSaveObj.idNo == null;
    }

    this.requestAndInwardDocSaveObj.make = this.dynamicArray[e].make
    if (this.requestAndInwardDocSaveObj.make == "") {
      this.requestAndInwardDocSaveObj.make = null;
    }

    this.requestAndInwardDocSaveObj.srNo = this.dynamicArray[e].srNo
    if (this.requestAndInwardDocSaveObj.srNo == "") {
      this.requestAndInwardDocSaveObj.srNo = null;
    }

  

    var check = parseFloat(this.dynamicArray[e].rangeFrom);
    if( check < 0 || check >= 0){
      this.requestAndInwardDocSaveObj.rangeFrom = this.dynamicArray[e].rangeFrom
    }
    else{
      this.requestAndInwardDocSaveObj.rangeFrom = "0";
      this.dynamicArray[e].rangeFrom="0";
      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('rangeFrom').patchValue("0");

    }


    // this.requestAndInwardDocSaveObj.rangeTo = this.dynamicArray[e].rangeTo
    var check1 = parseFloat(this.dynamicArray[e].rangeTo);
    if( check1 < 0 || check1 >= 0){
      this.requestAndInwardDocSaveObj.rangeTo = this.dynamicArray[e].rangeTo
    }
    else{
      this.requestAndInwardDocSaveObj.rangeTo = "0";
      this.dynamicArray[e].rangeTo="0";
      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('rangeTo').patchValue("0");

    }





    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].ruom == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.ruom = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.ruom = 1;                                                   
      }
    }

    this.requestAndInwardDocSaveObj.leastCount = this.dynamicArray[e].leastCount;                  
   // this.requestAndInwardDocSaveObj.leastCount = parseFloat(this.requestAndInwardDocSaveObj.leastCount).toFixed(2);
    if (this.requestAndInwardDocSaveObj.leastCount == "NaN") {
      this.requestAndInwardDocSaveObj.leastCount = null;
    }

    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {                               
      if (this.dynamicArray[e].lcuom1 == this.arrayUomName[k]) {                                                                  
        this.requestAndInwardDocSaveObj.lcuom1 = this.arrayUomId[k];                                                      
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.lcuom1 = 1;                                                                                              
      }
    }


    this.requestAndInwardDocSaveObj.accuracy = this.dynamicArray[e].accuracy;                                                                             
    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {                                                                      

      if (this.dynamicArray[e].formulaAccuracy == this.arrayFormulaAccuracyName[k]) {                                  
        this.requestAndInwardDocSaveObj.formulaAccuracy = this.arrayFormulaAccuracyId[k];                               
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.formulaAccuracy = 1;                                                     
      }
    }


    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].uomAccuracy == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.uomAccuracy = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.uomAccuracy = 1;
      }
    }


    this.requestAndInwardDocSaveObj.accuracy1 = this.dynamicArray[e].accuracy1
    for (var k = 0, l = Object.keys(this.formulaAccuracyListObj).length; k < l; k++) {

      if (this.dynamicArray[e].formulaAccuracy1 == this.arrayFormulaAccuracyName[k]) {
        this.requestAndInwardDocSaveObj.formulaAccuracy1 = this.arrayFormulaAccuracyId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.formulaAccuracy1 = 1;
      }
    }


    for (var k = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; k < l; k++) {
      if (this.dynamicArray[e].uomAccuracy1 == this.arrayUomName[k]) {
        this.requestAndInwardDocSaveObj.uomAccuracy1 = this.arrayUomId[k];
        break;
      }
      else {
        this.requestAndInwardDocSaveObj.uomAccuracy1 = 1;
      }
    }


    this.requestAndInwardDocSaveObj.location = this.dynamicArray[e].location;
    if (this.requestAndInwardDocSaveObj.location == "") {
      this.requestAndInwardDocSaveObj.location = null;
    }

    this.requestAndInwardDocSaveObj.calibrationFrequency = this.dynamicArray[e].calibrationFrequency
    if (this.requestAndInwardDocSaveObj.calibrationFrequency == "") {
      this.requestAndInwardDocSaveObj.calibrationFrequency = null;
    }

    this.requestAndInwardDocSaveObj.instrumentType = this.dynamicArray[e].instruType
    if (this.requestAndInwardDocSaveObj.instrumentType == "") {
      this.requestAndInwardDocSaveObj.instrumentType = null;
    }

    this.requestAndInwardDocSaveObj.labType = this.dynamicArray[e].labType
    if (this.requestAndInwardDocSaveObj.labType == "") {
      this.requestAndInwardDocSaveObj.labType = null;
    }

    this.requestAndInwardDocSaveObj.active = true;

    this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;
    this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
    this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
    this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;

    this.createReqAndInwardService.sendSrNoInstruIdenListForReqIwd(this.customerInstrumentIdentificationObj).subscribe(data => {
      console.log("*********sr no for doc no********" + data);

      this.createReqAndInwardService.getSrNoInstruIdenListForReqIwd().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.srNoInstruListForReqIwdArray = data;

        this.checkIfSrNoAlreadyPresentFC1(e);

      })
    })

    console.log("this.callToFunction()");
  }


  checkIfSrNoAlreadyPresentFC1(e) {

    document.getElementById("btn-" + e).style.backgroundColor = "#5cb85c";

    
    if (this.srNoInstruListForReqIwdArray.length == 0) {

      this.createReqAndInwardService.createCustInstruIdentification(this.customerInstrumentIdentificationObj).subscribe(data => {
        console.log("*********data saved********" + data);
        alert("customerInstrumentIdentification new data created!!");

        this.requestAndInwardDocObj1 = data;
        this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;

        this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
        this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
        this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;
        this.requestAndInwardDocSaveObj.custInstruIdentificationId = this.requestAndInwardDocObj1.custInstruIdentificationId;

        this.createReqAndInwardService.createReqAndInwardDocument(this.requestAndInwardDocSaveObj).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("all instru new data created!!");
          this.requestAndInwardDocSaveObj2 = data;

          this.reqDocnoAndCustInstruRelationshipObj.reqDocId = this.requestAndInwardDocSaveObj2.id;
          this.reqDocnoAndCustInstruRelationshipObj.custInstruIdentificationId = this.requestAndInwardDocObj1.custInstruIdentificationId;
          this.reqDocnoAndCustInstruRelationshipObj.createReqAndIwdTableId = this.requestAndInwardDocSaveObj.createReqAndIwdTableId
          this.reqDocnoAndCustInstruRelationshipObj.reqInwDocumentNo = this.requestAndInwardDocSaveObj2.reqInwDocumentNo;
          this.reqDocnoAndCustInstruRelationshipObj.inwardReqNo = this.requestAndInwardDocSaveObj2.inwardReqNo
          this.reqDocnoAndCustInstruRelationshipObj.inwardInstrumentNo = this.requestAndInwardDocSaveObj2.inwardInstrumentNo
          this.reqDocnoAndCustInstruRelationshipObj.parameterNumber = this.requestAndInwardDocSaveObj2.parameterNumber
          this.reqDocnoAndCustInstruRelationshipObj.singleParameter = 1;



          ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('custInstruIdentificationId').patchValue(this.reqDocnoAndCustInstruRelationshipObj.custInstruIdentificationId);


          this.dynamicArrayForCustInstruId = this.reqdocParaDetailsForm.get('Rows').value;
          console.log(this.dynamicArrayForCustInstruId);
          localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArrayForCustInstruId));

          this.createReqAndInwardService.createReqDocNoCustInstruRelationship(this.reqDocnoAndCustInstruRelationshipObj).subscribe(data => {
            console.log("*********data saved in ReqDocNoCustInstruRelationship ********" + data);
            alert("all instru new data created in ReqDocNoCustInstruRelationship!!");
          });


        })

      })

    }
    else {

      for (this.w = 0; this.w < this.srNoInstruListForReqIwdArray.length; this.w++) {

        if (this.srNoInstruListForReqIwdArray[this.w][69] == 1) {
          this.customerInstrumentIdentificationObj.custInstruIdentificationId = this.srNoInstruListForReqIwdArray[this.w][1]
          this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;
          this.customerInstrumentIdentificationObj.reqDocId = this.srNoInstruListForReqIwdArray[this.w][0];
          this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
          this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
          this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;

          this.createReqAndInwardService.updateCustInstruIdentification(this.customerInstrumentIdentificationObj.custInstruIdentificationId, this.customerInstrumentIdentificationObj).subscribe(data => {
            console.log("*********data saved********" + data);
            alert("customerInstrumentIdentification updated!!");



            this.requestAndInwardDocSaveObj.custInstruIdentificationId = this.customerInstrumentIdentificationObj.custInstruIdentificationId;


            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('custInstruIdentificationId').patchValue(this.requestAndInwardDocSaveObj.custInstruIdentificationId);

            this.dynamicArrayForCustInstruId = this.reqdocParaDetailsForm.get('Rows').value;
            console.log(this.dynamicArrayForCustInstruId);
            localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArrayForCustInstruId));

            this.createReqAndInwardService.updateReqAndInwardDocument(this.srNoInstruListForReqIwdArray[this.w][0], this.requestAndInwardDocSaveObj).subscribe(data => {
              console.log("data updated successfully" + data);
              alert("updated data created!!")

              // write create and update query for createReqDocNoCustInstruRelationship beacause even if custInstruId is updated but we need to create new record for each year 
              // and if once created then we need update query to avoid multiple crearion of same data.
              // first check whether reqdoc_cust_relationship table contains any data for new inward req number ,if does not contain then first create the new data
              // if already present then update query. -> this.createReqAndInwardService.createReqDocNoCustInstruRelationship query with condition checked otherwise it will create multiple
              // data for same inward number.

            })

          })

          break;
        }
      }
      if (this.w >= this.srNoInstruListForReqIwdArray.length) {

        this.createReqAndInwardService.createCustInstruIdentification(this.customerInstrumentIdentificationObj).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("customerInstrumentIdentification new data created!!");

          this.requestAndInwardDocObj1 = data;
          this.customerInstrumentIdentificationObj.custId = this.reqAndInwardDataObj.customerName;
          this.customerInstrumentIdentificationObj.reqDocId = this.requestAndInwardDocObj1.id;
          this.customerInstrumentIdentificationObj.nomenclatureId = this.requestAndInwardDocSaveObj.nomenclature;
          this.customerInstrumentIdentificationObj.idNo = this.requestAndInwardDocSaveObj.idNo;
          this.customerInstrumentIdentificationObj.srNo = this.requestAndInwardDocSaveObj.srNo;
          this.requestAndInwardDocSaveObj.custInstruIdentificationId = this.requestAndInwardDocObj1.custInstruIdentificationId;

          this.createReqAndInwardService.createReqAndInwardDocument(this.requestAndInwardDocSaveObj).subscribe(data => {
            console.log("*********data saved********" + data);
            alert("new data created!!");
            this.requestAndInwardDocSaveObj2 = data;

            this.reqDocnoAndCustInstruRelationshipObj.reqDocId = this.requestAndInwardDocSaveObj2.id;
            this.reqDocnoAndCustInstruRelationshipObj.custInstruIdentificationId = this.requestAndInwardDocObj1.custInstruIdentificationId;
            this.reqDocnoAndCustInstruRelationshipObj.createReqAndIwdTableId = this.requestAndInwardDocSaveObj.createReqAndIwdTableId
            this.reqDocnoAndCustInstruRelationshipObj.reqInwDocumentNo = this.requestAndInwardDocSaveObj2.reqInwDocumentNo;
            this.reqDocnoAndCustInstruRelationshipObj.inwardReqNo = this.requestAndInwardDocSaveObj2.inwardReqNo
            this.reqDocnoAndCustInstruRelationshipObj.inwardInstrumentNo = this.requestAndInwardDocSaveObj2.inwardInstrumentNo
            this.reqDocnoAndCustInstruRelationshipObj.parameterNumber = this.requestAndInwardDocSaveObj2.parameterNumber
            this.reqDocnoAndCustInstruRelationshipObj.singleParameter = 1;

            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(e) as FormGroup).get('custInstruIdentificationId').patchValue(this.requestAndInwardDocSaveObj.custInstruIdentificationId);

            this.dynamicArrayForCustInstruId = this.reqdocParaDetailsForm.get('Rows').value;
            console.log(this.dynamicArrayForCustInstruId);
            localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArrayForCustInstruId));

            this.createReqAndInwardService.createReqDocNoCustInstruRelationship(this.reqDocnoAndCustInstruRelationshipObj).subscribe(data => {
              console.log("*********data saved in ReqDocNoCustInstruRelationship ********" + data);
              alert("all instru new data created in ReqDocNoCustInstruRelationship!!");
            });
          })
        })
      }

    }

  }



  fun1() {

  }

  toggleMe(id: number): void {
    document.getElementById("btn-" + id).innerHTML = document.getElementById("btn-" + id).innerHTML == "Disable" ? "Enable" : "Disable";
  }
}
