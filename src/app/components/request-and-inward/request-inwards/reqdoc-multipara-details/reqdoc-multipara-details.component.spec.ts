import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqdocMultiparaDetailsComponent } from './reqdoc-multipara-details.component';

describe('ReqdocMultiparaDetailsComponent', () => {
  let component: ReqdocMultiparaDetailsComponent;
  let fixture: ComponentFixture<ReqdocMultiparaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqdocMultiparaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqdocMultiparaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
