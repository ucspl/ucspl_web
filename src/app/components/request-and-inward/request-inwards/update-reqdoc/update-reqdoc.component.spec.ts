import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReqdocComponent } from './update-reqdoc.component';

describe('UpdateReqdocComponent', () => {
  let component: UpdateReqdocComponent;
  let fixture: ComponentFixture<UpdateReqdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReqdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReqdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
