import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateReqAndInward, CreateReqAndInwardService, DynamicGrid, DynamicGridReqdocParaDetails, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, ReqTypeOfRequestAndInward, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { ExcelServiceService } from '../../../../services/serviceconnection/create-service/excel-service.service';
import { SearchReqAndInwardService } from '../../../../services/serviceconnection/search-service/search-req-and-inward.service';

declare var $: any;
@Component({
  selector: 'app-update-reqdoc',
  templateUrl: './update-reqdoc.component.html',
  styleUrls: ['./update-reqdoc.component.css']
})
export class UpdateReqdocComponent implements OnInit {

  manageDocumentForm: FormGroup;
  f2: FormGroup;

  idForSearchSales: string;

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();
  reqAndInwardDataObj1: CreateReqAndInward = new CreateReqAndInward();
  manageDocumentVariable: CreateReqAndInward = new CreateReqAndInward();

  reqTypeOfRequestAndInwardObj: ReqTypeOfRequestAndInward = new ReqTypeOfRequestAndInward();
  arrayReqTypeOfRequestAndInwardName: Array<any> = [];
  arrayReqTypeOfRequestAndInwardId: Array<any> = [];

  srNoInstruListForReqIwdArray: Array<any> = [];
  srNoInstruListForReqIwdArray1: Array<any> = [];

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  childCustomerObj: Customer = new Customer();
  childCustomers: Array<any> = [];
  customerObject: any;
  static customerNameLength: number;

  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;
  loginUserName: any;
  inwardType: any;
  reqContactPhone: any;
  noOfInstruments: any;
  customerWithAddress: Array<{ name: string, address: string }> = [];

  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray12: Array<DynamicGridReqdocParaDetails> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  inwardReqNo: any;
  inwardDate: string;
  customerNameSelected: any;
  expectedDeliveryDate: string;
  reportsInNameof: any;
  customerDcNo: string;
  reqContactName: string;
  customerDcDate: string;
  reqEmail: string;
  reqTypeOfRequestAndInwardName: any;
  id: number;
  assignedTo: string;
  createdBy: string;
  status: any;
  typeOfDocument: string;

  msgOnButtonClick: String;
  msgOnChildCompInit: String;
  notSelected: boolean;

  datetrial = new Date();

  reqInwDocumentNo: string;
  reqTypeSelected: string;
  phoneNumberCountryNm1: any;
  trialPhoneNumber1: any;
  mobileNumber1: any;

  rowNum = 0;
  pName: any;
  tableDisplay: any;
  trial1: any;
  prefixForQuotation: string;

  docNo: any;

  createReqAndIwdObject: any;
  documentNumberFromSearchRequest: string;

  windowRef = null;
  windowRef1 = null;
  custInstruIdForReqDocNo: any;
  docNo1: number;
  displayButton: number;

  constructor(private formBuilder: FormBuilder, private excelService:ExcelServiceService, private router: Router, public datepipe: DatePipe, private createCustomerService: CreateCustomerService, private createReqAndInwardService: CreateReqAndInwardService, private searchReqAndInwardService: SearchReqAndInwardService) {

    this.manageDocumentForm = this.formBuilder.group({
      reqInwDocumentNo: [],
      typeOfQuotation: [],
      reqType: [],
      status: [],
      remark: [],
      createdBy: [],
      assignedTo: []
    });

    this.f2 = this.formBuilder.group({
      tName: [''],
    })

  }


  ngOnInit() {


    this.phoneNumberCountryNm1 = this.searchReqAndInwardService.getCustomerCountryForMobileNo1();

    this.reqAndInwardDataObj = this.createReqAndInwardService.getReqAndInwDataFromCreateReq();
    console.log(this.reqAndInwardDataObj);
    if (Object.keys(this.reqAndInwardDataObj).length === 0) {
      this.reqAndInwardDataObj = JSON.parse(localStorage.getItem('reqAndInwardDataObj1'));
    }
    else {
      this.reqAndInwardDataObj1 = this.reqAndInwardDataObj;
      localStorage.setItem('reqAndInwardDataObj1', JSON.stringify(this.reqAndInwardDataObj1));

    }

    if (this.phoneNumberCountryNm1 == null) {
      this.phoneNumberCountryNm1 = this.reqAndInwardDataObj.countryPrefixForMobile1;
    }


    // mobileNumber code
    var input1 = document.querySelector("#reqPhone");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": [this.phoneNumberCountryNm1],
      "separateDialCode": true
    });

    var number1 = iti1.getNumber();
    var countryData1 = iti1.getSelectedCountryData();

    input1.addEventListener('countrychange', (e) => {

      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;

      console.log(this.trialPhoneNumber1);
    });

    //get customer list  
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);
        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);

        if (this.customerObj[i].addressLine2 != null) {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].addressLine2 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        else {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }

        if (this.customerObj[i].isParent == 1) {
          this.parentCustNames.push(this.customerObj[i].name);
          this.parentCustIds.push(this.customerObj[i].id);
        }
      }

      this.displayDataFromCreateReq();
      this.getSrNoData();

    },
      error => console.log(error));

    //get instrument list
    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterIdFromNom.push(this.instruListForRequestAndInwardObj[i].parameterId);
      }
    })

    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterIdFromNom.push(this.instruListForRequestAndInwardObj[i].parameterId);
      }
    })



    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    })

  }

  displayDataFromCreateReq() {
    this.inwardReqNo;
    this.id = this.reqAndInwardDataObj.id;
    this.inwardDate = this.reqAndInwardDataObj.inwardDate;
    this.expectedDeliveryDate = this.reqAndInwardDataObj.expectedDeliveryDate;
    this.noOfInstruments = this.reqAndInwardDataObj.noOfInstruments;
    this.reqContactName = this.reqAndInwardDataObj.reqContactName;
    this.customerDcNo = this.reqAndInwardDataObj.customerDCNo;
    this.reqContactPhone = this.reqAndInwardDataObj.reqContactPhone;
    this.assignedTo = this.reqAndInwardDataObj.assignedTo;

    this.reqContactPhone = this.reqAndInwardDataObj.reqContactPhone.slice(-10);
    var mb1 = new String(this.reqAndInwardDataObj.reqContactPhone)
    this.trialPhoneNumber1 = this.reqAndInwardDataObj.reqContactPhone.slice(-mb1.length + 1, -10);
    console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

    this.customerDcDate = this.reqAndInwardDataObj.dcDate;
    this.reqEmail = this.reqAndInwardDataObj.reqEmail;
    this.createdBy = this.reqAndInwardDataObj.createdBy;
    this.reqTypeSelected = this.reqAndInwardDataObj.reqType;

    this.inwardReqNo = this.reqAndInwardDataObj.inwardReqNo
    this.createReqAndInwardService.setInwardReqNoFromReqDoc(this.inwardReqNo);
    localStorage.setItem('inwardReqNo', JSON.stringify(this.inwardReqNo));

    this.reqInwDocumentNo = this.reqAndInwardDataObj.reqInwDocumentNo;
    localStorage.setItem('reqInwDocumentNo', JSON.stringify(this.reqInwDocumentNo));

    this.typeOfDocument = "order_accept_v1.xsl";



    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj.customerName) {
        this.customerNameSelected = this.customerWithAddress[i].name;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj.reportInNameOf) {
        this.reportsInNameof = this.customerWithAddress[i].name;
      }
    }

    //get request type list
    this.createReqAndInwardService.getReqTypeOfRequestAndInwards().subscribe(data => {
      this.reqTypeOfRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
        this.arrayReqTypeOfRequestAndInwardName.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeName)
        this.arrayReqTypeOfRequestAndInwardId.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeId)

        if (this.reqTypeOfRequestAndInwardObj[i].reqTypeId == parseInt(this.reqAndInwardDataObj.reqType)) {
          this.reqTypeSelected = this.reqTypeOfRequestAndInwardObj[i].reqTypeName;
        }

      }
    })

    //display status 
    if (this.reqAndInwardDataObj.draft == 1) {
      this.status = "Draft"
    }
    else if (this.reqAndInwardDataObj.approved == 1) {
      this.status = "Approved"
    }
    else if (this.reqAndInwardDataObj.archieved == 1) {
      this.status = "Archieved"
    }
    else if (this.reqAndInwardDataObj.rejected == 1) {
      this.status = "Rejected"
    }
    else if (this.reqAndInwardDataObj.submitted == 1) {
      this.status = "Submitted"
    } else {

    }
  }

  //display create request data in respective fields
  getDataFromCreateReq() {
    this.inwardReqNo;
    this.id = this.reqAndInwardDataObj[0][0];
    this.inwardDate = this.reqAndInwardDataObj[0][2];
    this.expectedDeliveryDate = this.reqAndInwardDataObj[0][9];
    this.noOfInstruments = this.reqAndInwardDataObj[0][13];
    this.reqContactName = this.reqAndInwardDataObj[0][14];
    this.customerDcNo = this.reqAndInwardDataObj[0][7];
    this.reqContactPhone = this.reqAndInwardDataObj[0][16];
    this.assignedTo = this.reqAndInwardDataObj[0][20];

    this.reqContactPhone = this.reqAndInwardDataObj[0][16].slice(-10);
    var mb1 = new String(this.reqAndInwardDataObj[0][16])
    this.trialPhoneNumber1 = this.reqAndInwardDataObj[0][16].slice(-mb1.length + 1, -10);
    console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

    this.customerDcDate = this.reqAndInwardDataObj[0][8];
    this.reqEmail = this.reqAndInwardDataObj[0][17];
    this.createdBy = this.reqAndInwardDataObj[0][21];
    this.reqTypeSelected = this.reqAndInwardDataObj[0][18];

    this.inwardReqNo = this.reqAndInwardDataObj[0][4]
    this.createReqAndInwardService.setInwardReqNoFromReqDoc(this.inwardReqNo);
    localStorage.setItem('inwardReqNo', JSON.stringify(this.inwardReqNo));

    this.reqInwDocumentNo = this.reqAndInwardDataObj[0][3];
    localStorage.setItem('reqInwDocumentNo', JSON.stringify(this.reqInwDocumentNo));

    this.typeOfDocument = "order_accept_v1.xsl";



    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj[0][5]) {
        this.customerNameSelected = this.customerWithAddress[i].name;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj[0][6]) {
        this.reportsInNameof = this.customerWithAddress[i].name;
      }
    }

    //get request type list
    this.createReqAndInwardService.getReqTypeOfRequestAndInwards().subscribe(data => {
      this.reqTypeOfRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
        this.arrayReqTypeOfRequestAndInwardName.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeName)
        this.arrayReqTypeOfRequestAndInwardId.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeId)

        if (this.reqTypeOfRequestAndInwardObj[i].reqTypeId == parseInt(this.reqAndInwardDataObj[0][18])) {
          this.reqTypeSelected = this.reqTypeOfRequestAndInwardObj[i].reqTypeName;
        }

      }
    })

    //display status 
    if (this.reqAndInwardDataObj[0][22] == 1) {
      this.status = "Draft"
    }
    else if (this.reqAndInwardDataObj[0][23] == 1) {
      this.status = "Approved"
    }
    else if (this.reqAndInwardDataObj[0][24] == 1) {
      this.status = "Archieved"
    }
    else if (this.reqAndInwardDataObj[0][25] == 1) {
      this.status = "Rejected"
    }
    else if (this.reqAndInwardDataObj[0][26] == 1) {
      this.status = "Submitted"
    } else {

    }
  }

  displayButtonFun() {
    this.displayButton = 1;
  }


  convertInObj() {
    
    this.reqAndInwardDataObj1.id = this.createReqAndIwdObject[0][0];
    this.reqAndInwardDataObj1.inwardType = this.createReqAndIwdObject[0][1];
    this.reqAndInwardDataObj1.inwardDate = this.createReqAndIwdObject[0][2];
    this.reqAndInwardDataObj1.reqInwDocumentNo = this.createReqAndIwdObject[0][3];
    this.reqAndInwardDataObj1.inwardReqNo = this.createReqAndIwdObject[0][4];
    this.reqAndInwardDataObj1.customerName = this.createReqAndIwdObject[0][5];
    this.reqAndInwardDataObj1.reportInNameOf = this.createReqAndIwdObject[0][6];
    this.reqAndInwardDataObj1.customerDCNo = this.createReqAndIwdObject[0][7];
    this.reqAndInwardDataObj1.dcDate = this.createReqAndIwdObject[0][8];
    this.reqAndInwardDataObj1.expectedDeliveryDate = this.createReqAndIwdObject[0][9];
    this.reqAndInwardDataObj1.endDate = this.createReqAndIwdObject[0][10];
    this.reqAndInwardDataObj1.calibratedAt = this.createReqAndIwdObject[0][11];
    this.reqAndInwardDataObj1.labLocation = this.createReqAndIwdObject[0][12];
    this.reqAndInwardDataObj1.noOfInstruments = this.createReqAndIwdObject[0][13];
    this.reqAndInwardDataObj1.reqContactName = this.createReqAndIwdObject[0][14];
    this.reqAndInwardDataObj1.countryPrefixForMobile1 = this.createReqAndIwdObject[0][15];
    this.reqAndInwardDataObj1.reqContactPhone = this.createReqAndIwdObject[0][16];
    this.reqAndInwardDataObj1.reqEmail = this.createReqAndIwdObject[0][17];
    this.reqAndInwardDataObj1.reqType = this.createReqAndIwdObject[0][18];
    this.reqAndInwardDataObj1.typeOfDocument = this.createReqAndIwdObject[0][19];
    this.reqAndInwardDataObj1.assignedTo = this.createReqAndIwdObject[0][20];
    this.reqAndInwardDataObj1.createdBy = this.createReqAndIwdObject[0][21];
    this.reqAndInwardDataObj1.draft = this.createReqAndIwdObject[0][22];
    this.reqAndInwardDataObj1.approved = this.createReqAndIwdObject[0][23];
    this.reqAndInwardDataObj1.archieved = this.createReqAndIwdObject[0][24];
    this.reqAndInwardDataObj1.rejected = this.createReqAndIwdObject[0][25];
    this.reqAndInwardDataObj1.submitted = this.createReqAndIwdObject[0][26];

  }


  getSrNoData() {

    this.createReqAndInwardService.getCustInstruIdForReqDocNo(this.reqInwDocumentNo).subscribe(data => {
      this.custInstruIdForReqDocNo = data;


      this.srNoInstruListForReqIwdArray = this.custInstruIdForReqDocNo;

      if (this.srNoInstruListForReqIwdArray.length != 0) {
        this.displaytSrNoData()
      }
      else {
        this.dynamicArray12 = JSON.parse(localStorage.getItem('dynamicArrayForReqAndIwd'));
        console.log(this.dynamicArray12);
        if (this.dynamicArray12 != null) {
          this.dynamicArray = this.dynamicArray12;
        }
      }
    })

  }

  displaytSrNoData() {
    console.log(this.srNoInstruListForReqIwdArray[0][6].length);

    for (var s = 0; s < this.srNoInstruListForReqIwdArray.length; s++) {
      var pn = new String(this.srNoInstruListForReqIwdArray[s][6])
      var no=this.srNoInstruListForReqIwdArray[s][6].slice(-2);
      console.log("no :"+no);
      if(no=="_1"){
      var variable = this.srNoInstruListForReqIwdArray[s][6].slice(-1);
      console.log("var containing last character :" + variable);
      if (variable == "1") {
        this.dynamicArray.push({
          inwardInstruNo: this.srNoInstruListForReqIwdArray[s][5],
          nomenclature: this.srNoInstruListForReqIwdArray[s][7],
          statusOfOrder: this.srNoInstruListForReqIwdArray[s][30],
          parameter: this.srNoInstruListForReqIwdArray[s][8],
          parameterNumber: this.srNoInstruListForReqIwdArray[s][6],
          idNo: this.srNoInstruListForReqIwdArray[s][11],
          make: this.srNoInstruListForReqIwdArray[s][10],
          srNo: this.srNoInstruListForReqIwdArray[s][4],
          rangeFrom: this.srNoInstruListForReqIwdArray[s][14],
          rangeTo: this.srNoInstruListForReqIwdArray[s][15],
          ruom: this.srNoInstruListForReqIwdArray[s][16],
          leastCount: this.srNoInstruListForReqIwdArray[s][17],
          lcuom1: this.srNoInstruListForReqIwdArray[s][18],
          accuracy: this.srNoInstruListForReqIwdArray[s][19],
          formulaAccuracy: this.srNoInstruListForReqIwdArray[s][20],
          uomAccuracy: this.srNoInstruListForReqIwdArray[s][21],
          accuracy1: this.srNoInstruListForReqIwdArray[s][22],
          formulaAccuracy1: this.srNoInstruListForReqIwdArray[s][23],
          uomAccuracy1: this.srNoInstruListForReqIwdArray[s][24],
          location: this.srNoInstruListForReqIwdArray[s][25],
          calibrationFrequency: this.srNoInstruListForReqIwdArray[s][26],
          instruType: this.srNoInstruListForReqIwdArray[s][13],
          labType: this.srNoInstruListForReqIwdArray[s][27],
          active: this.srNoInstruListForReqIwdArray[s][28],
          custInstruIdentificationId: this.srNoInstruListForReqIwdArray[s][32]
        })

      }
    }

    }


    if (this.noOfInstruments > this.dynamicArray.length) {
      var value = this.noOfInstruments - this.dynamicArray.length
      this.noOfRowsAccordingToNoOfInstu1(value);
    }



    localStorage.setItem('dynamicArrayForReqAndIwd', JSON.stringify(this.dynamicArray));

  }

  noOfRowsAccordingToNoOfInstu1(value) {

    for (var s = 0; s < value; s++) {
      this.dynamicArray.push({
        inwardInstruNo: "",
        nomenclature: "",
        statusOfOrder: "",
        parameter: "",
        parameterNumber: "",
        idNo: "",
        make: "",
        srNo: "",
        rangeFrom: "",
        rangeTo: "",
        ruom: 0,
        leastCount: 0,
        lcuom1: 0,
        accuracy: "",
        formulaAccuracy: 0,
        uomAccuracy: 0,
        accuracy1:"",
        formulaAccuracy1: 0,
        uomAccuracy1: 0,
        location: "",
        calibrationFrequency: "",
        instruType: "",
        labType: "",
        active: false,
        custInstruIdentificationId: 0
      })
    }
  }

  updateManageDoc() {

    this.manageDocumentVariable.assignedTo = this.assignedTo;
    this.manageDocumentVariable.createdBy = this.createdBy;
    this.manageDocumentVariable.typeOfDocument = this.typeOfDocument;
    this.manageDocumentVariable.reqInwDocumentNo = this.reqInwDocumentNo;
    this.manageDocumentVariable.inwardReqNo = this.inwardReqNo;
    this.manageDocumentVariable.inwardDate = this.datepipe.transform(this.inwardDate, 'dd-MM-yyyy');
    this.manageDocumentVariable.expectedDeliveryDate = this.datepipe.transform(this.expectedDeliveryDate, 'dd-MM-yyyy');
    this.manageDocumentVariable.reqContactName = this.reqContactName;
    this.manageDocumentVariable.customerDCNo = this.customerDcNo;
    this.manageDocumentVariable.reqContactPhone = this.reqContactPhone;
    this.manageDocumentVariable.dcDate = this.datepipe.transform(this.customerDcDate, 'dd-MM-yyyy');
    this.manageDocumentVariable.reqEmail = this.reqEmail;
    this.manageDocumentVariable.reqType = this.reqTypeSelected;
    this.manageDocumentVariable.multiParameterFlag = 0;
    this.manageDocumentVariable.inwardType = this.reqAndInwardDataObj[0][1];
    this.manageDocumentVariable.noOfInstruments = this.noOfInstruments;
    this.manageDocumentVariable.countryPrefixForMobile1 = this.phoneNumberCountryNm1;
    this.manageDocumentVariable.dateModified = Date.now();

    switch (this.status) {
      case 'Draft': this.manageDocumentVariable.draft = 1;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Archieved': this.manageDocumentVariable.draft = 0
        this.manageDocumentVariable.archieved = 1;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Rejected': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 1;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Approved': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 1;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Submitted': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 1;
        break;
        
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.customerNameSelected) {
        this.manageDocumentVariable.customerName = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.reportsInNameof) {
        this.manageDocumentVariable.reportInNameOf = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
      if (this.reqTypeOfRequestAndInwardObj[i].reqTypeName == this.reqTypeSelected) {
        this.manageDocumentVariable.reqType = this.reqTypeOfRequestAndInwardObj[i].reqTypeId;
      }
    }

    this.createReqAndInwardService.updateReqAndInward(this.id, this.manageDocumentVariable).subscribe(data => {
      console.log(data);
      alert("Saved Successfully!!");
    })
  }

  saveAllData() {
    this.updateManageDoc();
  }

  openChildWindow() {
    var url = '/updReqDocParaList';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receivemessage.bind(this), false);
  }

  receivemessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }

  paraListShow() {
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }

  openModal1() {
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()
  }

  backToMenu() {
    this.router.navigateByUrl('/nav/searchreq');
  }

  createCertificate(i) {
    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;

    for (var ii = 0; ii < this.dynamicArray.length; ii++) {
      if (i.target.innerText == this.dynamicArray[ii].inwardInstruNo) {
        this.docNo1 = this.dynamicArray[ii].custInstruIdentificationId
      }
    }

    localStorage.setItem('docNo', JSON.stringify(this.docNo));
    localStorage.setItem('custinstruid', JSON.stringify(this.docNo1));

    var url = '/crtCalCer';
    this.windowRef1 = window.open(url, "child1", "width=1030,height=420,top=100");
    this.windowRef1.focus();
    this.windowRef1.addEventListener("message1", this.receivemessage1.bind(this), false);
  }

  receivemessage1(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }


  exportAsXLSX():void {
     this.excelService.exportAsExcelFile(this.dynamicArray, 'sample');
  }


}
