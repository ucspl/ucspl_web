import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateReqAndInward, CreateReqAndInwardService, ReqTypeOfRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { SearchReqAndInwardService, SearchReqAndIwdByDocument } from '../../../../services/serviceconnection/search-service/search-req-and-inward.service';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-req-search',
  templateUrl: './req-search.component.html',
  styleUrls: ['./req-search.component.css']
})
export class ReqSearchComponent implements OnInit {

  searchRequestAndInwardForm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';
  p: number = 1;
  itemsPerPage: number = 5;
  public dateValue1 = new Date();
  public dateValue2 = new Date();

  data: any;
  searchReqAndIwdByDocument: SearchReqAndIwdByDocument = new SearchReqAndIwdByDocument();
  reqAndIwdDataArray: Array<any> = [];

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();

  docNo: string;
  statusChoice: any;
  status: string;
  idForSearchSales: string;
  salesObject: any;
  createReqAndIwdObject: any;
  documentNumberFromSearchRequest: string;

  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  childCustomerObj: Customer = new Customer();
  childCustomers: Array<any> = [];
  customerObject: any;
  static customerNameLength: number;

  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;
  loginUserName: any;
  inwardType: any;
  reqContactPhone: any;
  noOfInstruments: any;
  customerWithAddress: Array<{ name: string, address: string }> = [];

  reqTypeOfRequestAndInwardObj: ReqTypeOfRequestAndInward = new ReqTypeOfRequestAndInward();
  arrayReqTypeOfRequestAndInwardName: Array<any> = [];
  arrayReqTypeOfRequestAndInwardId: Array<any> = [];
  inwardRequestNumberForInvoice: any;
  inwardRequestNoForInvoice: number;
  back: number;

  constructor(private formBuilder: FormBuilder, public datepipe: DatePipe, private router: Router, private studentService: StudentService, private searchReqAndInwardService: SearchReqAndInwardService, private createReqAndInwardService: CreateReqAndInwardService, private createCustomerService: CreateCustomerService) {
    this.searchRequestAndInwardForm = this.formBuilder.group({
      searchDocument: [],
      itemsPerPage: [],
      toDate: [],
      fromDate: [],
      statusChoice: []
    });
  }

  ngOnInit() {
    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);
        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);

        if (this.customerObj[i].addressLine2 != null) {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].addressLine2 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        else {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }

        if (this.customerObj[i].isParent == 1) {
          this.parentCustNames.push(this.customerObj[i].name);
          this.parentCustIds.push(this.customerObj[i].id);
        }
      }
      // this.getDataFromCreateReq();
    },
      error => console.log(error));

    window.localStorage.removeItem('dynamicArrayForReqAndIwd');
  }

  showData() {
    this.searchReqAndInwardService.showData().subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.reqAndIwdDataArray = this.data;

      for (var i = 0; i < this.reqAndIwdDataArray.length; i++) {

        if (this.reqAndIwdDataArray[i][6] == 1) {
          this.status = "Draft";
          this.reqAndIwdDataArray[i].push("Draft")
        }
        if (this.reqAndIwdDataArray[i][8] == 1) {
          this.status = "Archieved";
          this.reqAndIwdDataArray[i].push("Archieved")
        }
        if (this.reqAndIwdDataArray[i][9] == 1) {
          this.status = "Submitted";
          this.reqAndIwdDataArray[i].push("Submitted")
        }
        if (this.reqAndIwdDataArray[i][7] == 1) {
          this.status = "Approved";
          this.reqAndIwdDataArray[i].push("Approved")
        }
        if (this.reqAndIwdDataArray[i][10] == 1) {
          this.status = "Rejected";
          this.reqAndIwdDataArray[i].push("Rejected")
        }
      }
    })
  }

  searchData() {
    debugger;
    if (typeof this.searchRequestAndInwardForm.value.toDate == "undefined" || this.searchRequestAndInwardForm.value.toDate == null) {
      this.searchReqAndIwdByDocument.searchDocument = this.searchRequestAndInwardForm.value.searchDocument;
      this.searchReqAndIwdByDocument.searchDocument = this.searchReqAndIwdByDocument.searchDocument.trim();

      switch (this.statusChoice) {
        case 'selectStatus': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Draft': this.searchReqAndIwdByDocument.draft = 1;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Archieved': this.searchReqAndIwdByDocument.draft = 0
          this.searchReqAndIwdByDocument.archieved = 1;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Rejected': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 1;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Approved': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 1;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Submitted': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 1;
          break;

      }
    }
    else {
      this.searchRequestAndInwardForm.value.toDate.setDate(this.searchRequestAndInwardForm.value.toDate.getDate() + 1);
      this.searchReqAndIwdByDocument.toDate = this.datepipe.transform(this.searchRequestAndInwardForm.value.toDate, 'yyyy-MM-dd');
      this.searchReqAndIwdByDocument.fromDate = this.datepipe.transform(this.searchRequestAndInwardForm.value.fromDate, 'yyyy-MM-dd');

      switch (this.statusChoice) {
        case 'selectStatus': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Draft': this.searchReqAndIwdByDocument.draft = 1;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Archieved': this.searchReqAndIwdByDocument.draft = 0
          this.searchReqAndIwdByDocument.archieved = 1;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Rejected': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 1;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Approved': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 1;
          this.searchReqAndIwdByDocument.submitted = 0;
          break;
        case 'Submitted': this.searchReqAndIwdByDocument.draft = 0;
          this.searchReqAndIwdByDocument.archieved = 0;
          this.searchReqAndIwdByDocument.rejected = 0;
          this.searchReqAndIwdByDocument.approved = 0;
          this.searchReqAndIwdByDocument.submitted = 1;
          break;
      }
    }


    if (this.searchReqAndIwdByDocument == null) {
      alert("please enter criteria to search");
    }
    else {
      this.searchReqAndInwardService.searchData(this.searchReqAndIwdByDocument).subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
      })
      this.showData();
    }
  }

  moveToRIDocUpdateScreen(i) {

    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;
    this.searchReqAndInwardService.setDocNoForReqAndIwdDocScreen(this.docNo);
    this.idForSearchSales = this.docNo;

    this.searchReqAndInwardService.sendDocNoForGettingCreateReqDetails(this.idForSearchSales).subscribe(data => {
      console.log(data);
    })

    this.searchReqAndInwardService.getCreateReqDetailsForSearchReq().subscribe(data => {
      this.createReqAndIwdObject = data;

      this.convertInObj();

      this.searchReqAndInwardService.setCustomerCountryForMobileNo1(this.createReqAndIwdObject[0][15]);
      this.router.navigateByUrl('/updateReqDoc');

    })
  }

  moveToCreateInvoice(e){
    
    e=((this.itemsPerPage * (this.p-1))+(e));
  console.log("index will be...."+e);

  this.inwardRequestNoForInvoice=parseInt(e);
  console.log("inwardRequestNumber"+this.reqAndIwdDataArray[this.inwardRequestNoForInvoice][1]);
    localStorage.setItem('RequestAndInwardArrayForInvoice',JSON.stringify(this.reqAndIwdDataArray[this.inwardRequestNoForInvoice]));
    this.back=0;
    localStorage.setItem('back',JSON.stringify(this.back));
    this.router.navigateByUrl('nav/createInvoice'); 
  }

  moveToCreateProformaInvoice(e){
    
    e=((this.itemsPerPage * (this.p-1))+(e));
  console.log("index will be...."+e);

  this.inwardRequestNoForInvoice=parseInt(e);
  console.log("inwardRequestNumber"+this.reqAndIwdDataArray[this.inwardRequestNoForInvoice][1]);
    localStorage.setItem('RequestAndInwardArrayForInvoice',JSON.stringify(this.reqAndIwdDataArray[this.inwardRequestNoForInvoice]));
    this.back=0;
    localStorage.setItem('back',JSON.stringify(this.back));
  //  this.router.navigateByUrl('nav/createinvoice'); 
    this.router.navigateByUrl('nav/crtproforma');
  } 


  moveToCreateDeliveryChallan(e){

 e=((this.itemsPerPage * (this.p-1))+(e));
  console.log("index will be...."+e);

  this.inwardRequestNoForInvoice=parseInt(e);
  console.log("inwardRequestNumber"+this.reqAndIwdDataArray[this.inwardRequestNoForInvoice][1]);
    localStorage.setItem('RequestAndInwardArrayForInvoice',JSON.stringify(this.reqAndIwdDataArray[this.inwardRequestNoForInvoice]));
    this.back=0;
    localStorage.setItem('back',JSON.stringify(this.back));
  //  this.router.navigateByUrl('nav/createinvoice'); 
    this.router.navigateByUrl('nav/crtdelichallan');

  }


  convertInObj() {
    this.reqAndInwardDataObj.id = this.createReqAndIwdObject[0][0];
    this.reqAndInwardDataObj.inwardType = this.createReqAndIwdObject[0][1];
    this.reqAndInwardDataObj.inwardDate = this.createReqAndIwdObject[0][2];
    this.reqAndInwardDataObj.reqInwDocumentNo = this.createReqAndIwdObject[0][3];
    this.reqAndInwardDataObj.inwardReqNo = this.createReqAndIwdObject[0][4];
    this.reqAndInwardDataObj.customerName = this.createReqAndIwdObject[0][5];
    this.reqAndInwardDataObj.reportInNameOf = this.createReqAndIwdObject[0][6];
    this.reqAndInwardDataObj.customerDCNo = this.createReqAndIwdObject[0][7];
    this.reqAndInwardDataObj.dcDate = this.createReqAndIwdObject[0][8];
    this.reqAndInwardDataObj.expectedDeliveryDate = this.createReqAndIwdObject[0][9];
    this.reqAndInwardDataObj.endDate = this.createReqAndIwdObject[0][10];
    this.reqAndInwardDataObj.calibratedAt = this.createReqAndIwdObject[0][11];
    this.reqAndInwardDataObj.labLocation = this.createReqAndIwdObject[0][12];
    this.reqAndInwardDataObj.noOfInstruments = this.createReqAndIwdObject[0][13];
    this.reqAndInwardDataObj.reqContactName = this.createReqAndIwdObject[0][14];
    this.reqAndInwardDataObj.countryPrefixForMobile1 = this.createReqAndIwdObject[0][15];
    this.reqAndInwardDataObj.reqContactPhone = this.createReqAndIwdObject[0][16];
    this.reqAndInwardDataObj.reqEmail = this.createReqAndIwdObject[0][17];
    this.reqAndInwardDataObj.reqType = this.createReqAndIwdObject[0][18];
    this.reqAndInwardDataObj.typeOfDocument = this.createReqAndIwdObject[0][19];
    this.reqAndInwardDataObj.assignedTo = this.createReqAndIwdObject[0][20];
    this.reqAndInwardDataObj.createdBy = this.createReqAndIwdObject[0][21];
    this.reqAndInwardDataObj.draft = this.createReqAndIwdObject[0][22];
    this.reqAndInwardDataObj.approved = this.createReqAndIwdObject[0][23];
    this.reqAndInwardDataObj.archieved = this.createReqAndIwdObject[0][24];
    this.reqAndInwardDataObj.rejected = this.createReqAndIwdObject[0][25];
    this.reqAndInwardDataObj.submitted = this.createReqAndIwdObject[0][26];

    this.createReqAndInwardService.setReqAndInwDataFromCreateReq(this.reqAndInwardDataObj);


  }
}
