import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqSearchComponent } from './req-search.component';

describe('ReqSearchComponent', () => {
  let component: ReqSearchComponent;
  let fixture: ComponentFixture<ReqSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
