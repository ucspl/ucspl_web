import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCalibrationCertificateComponent } from './update-calibration-certificate.component';

describe('UpdateCalibrationCertificateComponent', () => {
  let component: UpdateCalibrationCertificateComponent;
  let fixture: ComponentFixture<UpdateCalibrationCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCalibrationCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCalibrationCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
