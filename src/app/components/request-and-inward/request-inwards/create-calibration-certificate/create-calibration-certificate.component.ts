import { DatePipe } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateReqAndInward, CreateReqAndInwardService, DynamicGridReqdocParaDetails, FormulaAccuracyReqAndIwd, InstrumentListForRequestAndInward, ParameterListForRequestAndInward, RequestAndInwardDocument, UomListForRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';

declare var $: any;
@Component({
  selector: 'app-create-calibration-certificate',
  templateUrl: './create-calibration-certificate.component.html',
  styleUrls: ['./create-calibration-certificate.component.css']
})
export class CreateCalibrationCertificateComponent implements OnInit {

  frm: FormGroup;
  calibCertificateForm: FormGroup;
  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();

  requestAndInwardDocObj: RequestAndInwardDocument = new RequestAndInwardDocument();
  requestAndInwardDocSaveObj: RequestAndInwardDocument = new RequestAndInwardDocument();

  instruListForRequestAndInwardObj: InstrumentListForRequestAndInward = new InstrumentListForRequestAndInward();
  arrayInstrumentListName: Array<any> = [];
  arrayInstrumentListId: Array<any> = [];
  arrayParameterIdFromNom: Array<any> = [];

  formulaAccuracyListObj: FormulaAccuracyReqAndIwd = new FormulaAccuracyReqAndIwd();
  arrayFormulaAccuracyId: Array<any> = [];
  arrayFormulaAccuracyName: Array<any> = [];

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  inwardInstruNo: string;
  srNoInstruListForReqIwdArray: Array<any> = [];
  srNoInstruListForReqIwdArray1: Array<any> = [];

  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  childCustomerObj: Customer = new Customer();
  childCustomers: Array<any> = [];
  customerObject: any;
  static customerNameLength: number;
  customerWithAddress: Array<{ name: string, address: string }> = [];
  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  dynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray1: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArray2: Array<DynamicGridReqdocParaDetails> = [];
  dynamicArrayTrial: Array<DynamicGridReqdocParaDetails> = [];
  multiDynamicArray: Array<DynamicGridReqdocParaDetails> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  certificateNo: string;
  dateCalibration: any;
  dateCertificate: any;
  dateReceived: any;
  calibratedFor: any;
  nomenclature: any;
  make: any;
  rangeFrom: any;
  rangeTo: any;
  idNo: any;
  srNo: any;
  orderNo: any;
  leastCount: any;
  accuracy: any;
  accuracy1: any;
  condOfInstru: any;
  remark: any;
  calibratedAt: any;
  location: any;
  instrumentType: any;
  certificatePrintMode: any;
  attachments: any;
  calibratedDueOn: any;
  frequency: any;
  masterCertificate: any;
  reminderDate = Date.now()
  frequency1: any;
  calibratedBy: any;
  authorizedBy: any;
  dt: any;
  instruCondition: string;
  ruom: any;
  lcuom1: any;
  formulaAccuracy: any;
  uomAccuracy: any;
  formulaAccuracy1: any;
  uomAccuracy1: any;
  counterForCertificateNo: any;
  prefixForInward: any;
  finalCertificateNumber: string;
  inputO: any;
  masterIp: any;
  thermocoupleType: any;
  type: any;
  masterOp: any;
  masterOp1: any;
  masterIpSelection: any;
  certificateNo1: string;
  dateOfIssueCertificate: any;
  custInstruId: any;

  constructor(private cdr: ChangeDetectorRef, private router: Router, public datepipe: DatePipe, private formBuilder: FormBuilder, private createCustomerService: CreateCustomerService, private createReqAndInwardService: CreateReqAndInwardService) {
    this.frm = this.formBuilder.group({
      cName: [''],
    })

    this.calibCertificateForm = this.formBuilder.group({
      certificateNo: [''],
      dateCalibration: [''],
      dateCertificate: [''],
      dateOfIssueCertificate: [''],
      dateReceived: [''],
      calibratedFor: [''],
      nomenclature: [''],
      make: [''],
      rangeFrom: [''],
      rangeTo: [''],
      idNo: [''],
      srNo: [''],
      orderNo: [''],
      leastCount: [''],
      accuracy: [''],
      accuracy1: [''],
      instruCondition: [''],
      remark: [''],
      calibratedAt: [''],
      location: [''],
      instrumentType: [''],
      certificatePrintMode: [''],
      attachments: [''],
      calibratedDueOn: [Date.now()],
      frequency: [''],
      masterCertificate: [''],
      reminderDate: [''],
      frequency1: [''],
      calibratedBy: [''],
      authorizedBy: [''],
      inputO: [''],
      inputOptional: [''],
      inputOptional1: [''],
      inputOptionalS: [''],
      outputOptional: [''],
      outputOptional1: [''],
      outputOptionalS: [''],
      thermocoupleType: [''],
      masterIp: [''],
      type: [''],
      masterOp: [''],
      masterOp1: [''],
      uucSensor: [''],
      masterType: [''],
      uomMaster1: [''],
      uomMaster2: [''],
      masterIpSelection: ['']
    })
  }

  ngOnInit() {

    this.authorizedBy = "K. M. Bhosale";


    this.inwardInstruNo = JSON.parse(localStorage.getItem('docNo'));

    this.reqAndInwardDataObj = JSON.parse(localStorage.getItem('reqAndInwardDataObj1'));
    console.log(this.reqAndInwardDataObj);

    this.createReqAndInwardService.getInstrumentList().subscribe(data => {
      this.instruListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.instruListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayInstrumentListName.push(this.instruListForRequestAndInwardObj[i].instrumentName);
        this.arrayInstrumentListId.push(this.instruListForRequestAndInwardObj[i].instrumentId);
        this.arrayParameterIdFromNom.push(this.instruListForRequestAndInwardObj[i].parameterId);
      }
    })


    //get formula accuracy list
    this.createReqAndInwardService.getFormulaAccuracyListOfRequestAndInwards().subscribe(data => {
      this.formulaAccuracyListObj = data;
      for (var i = 0, l = Object.keys(this.formulaAccuracyListObj).length; i < l; i++) {
        this.arrayFormulaAccuracyName.push(this.formulaAccuracyListObj[i].formulaAccuracyName);
        this.arrayFormulaAccuracyId.push(this.formulaAccuracyListObj[i].formulaAccuracyId);
      }
    })


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    })

    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);
        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);

        if (this.customerObj[i].addressLine2 != null) {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].addressLine2 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        else {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        if (this.customerObj[i].isParent == 1) {
          this.parentCustNames.push(this.customerObj[i].name);
          this.parentCustIds.push(this.customerObj[i].id);
        }
      }
    },
      error => console.log(error));


    // get customers list
    this.createReqAndInwardService.getChildCustomers().subscribe(data => {
      console.log(data);
      this.childCustomerObj = data;
      if (typeof this.childCustomerObj != 'undefined') {
        for (var i = 0, l = Object.keys(this.childCustomerObj).length; i < l; i++) {
          this.childCustomers.push(this.childCustomerObj[i].name);
        }
      }
    },
      error => console.log(error));


    this.newDynamic1 = [{
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "Order created",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: false
    }]

    this.newDynamic = {
      inwardInstruNo: "",
      nomenclature: "",
      statusOfOrder: "",
      parameter: "",
      parameterNumber: "",
      idNo: "",
      make: "",
      srNo: "",
      rangeFrom: "",
      rangeTo: "",
      ruom: "",
      leastCount: "",
      lcuom1: "",
      accuracy: "",
      formulaAccuracy: "",
      uomAccuracy: "",
      accuracy1: "",
      formulaAccuracy1: "",
      uomAccuracy1: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      labType: "",
      active: false
    };

    this.dynamicArray.push(this.newDynamic);

    this.custInstruId = JSON.parse(localStorage.getItem('custinstruid'));


    this.createReqAndInwardService.getMultiParaDetails(this.custInstruId).subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.srNoInstruListForReqIwdArray1 = data;


      this.orderNo = this.srNoInstruListForReqIwdArray1[0][36]
      this.certificateNo = this.srNoInstruListForReqIwdArray1[0][37]

      var d = this.reqAndInwardDataObj.inwardDate.slice(0, 2)
      var m = this.reqAndInwardDataObj.inwardDate.slice(3, 5)
      var yy = this.reqAndInwardDataObj.inwardDate.slice(6, 10)
      var yyyy = parseInt(yy)
      var mm = parseInt(m)
      var dd = parseInt(d)

      mm = mm - 1;

      var iwdDate = new Date(yyyy, mm, dd);
      var iwdDate1 = new Date(yyyy, mm, dd);



      var output = [], n = 1, padded;
      padded = ('0' + mm).slice(-2);

      console.log(d + " " + mm + " " + yy);
      var mydate = new Date(yyyy, padded, dd);
      var freqNo = parseInt(this.srNoInstruListForReqIwdArray1[0][26]);


      this.calibCertificateForm.value.certificateNo = this.srNoInstruListForReqIwdArray1[0][5]
      this.certificateNo = this.srNoInstruListForReqIwdArray1[0][5];

      this.certificateNo1 = this.certificateNo;

      this.prefixForInward = this.certificateNo.slice(0, this.certificateNo.length - 1);
      console.log(this.prefixForInward);
      var t = this.certificateNo.lastIndexOf('_');
      console.log(t);
      this.orderNo = this.certificateNo1.slice(0, t);

      this.counterForCertificateNo = this.certificateNo.slice(t + 1, (this.certificateNo.length));
      console.log(this.counterForCertificateNo);

      var output1 = [], n1 = 1, padded1;
      padded1 = ('00' + this.counterForCertificateNo).slice(-3);
      output.push(padded1);

      var a1 = this.prefixForInward + padded1;
      this.finalCertificateNumber = a1;

      console.log("this.finalCertificateNumber " + this.finalCertificateNumber);
      this.certificateNo = this.finalCertificateNumber;

      this.calibCertificateForm.value.dateCalibration = this.reqAndInwardDataObj.inwardDate
      this.dateCalibration = iwdDate;
      this.calibCertificateForm.value.dateOfIssueCertificate = this.reqAndInwardDataObj.inwardDate
      this.dateOfIssueCertificate = iwdDate;
      this.calibCertificateForm.value.dateReceived = this.reqAndInwardDataObj.inwardDate
      this.dateReceived = iwdDate;
      this.calibCertificateForm.value.calibratedFor = this.reqAndInwardDataObj.customerName
      this.calibratedFor = this.reqAndInwardDataObj.customerName;

      this.calibCertificateForm.value.nomenclature = this.srNoInstruListForReqIwdArray1[0][7]
      this.nomenclature = this.srNoInstruListForReqIwdArray1[0][7]

      this.calibCertificateForm.value.make = this.srNoInstruListForReqIwdArray1[0][10]
      this.make = this.srNoInstruListForReqIwdArray1[0][10];

      this.calibCertificateForm.value.rangeFrom = this.srNoInstruListForReqIwdArray1[0][14]
      this.rangeFrom = this.srNoInstruListForReqIwdArray1[0][14];

      this.calibCertificateForm.value.rangeTo = this.srNoInstruListForReqIwdArray1[0][15]
      this.rangeTo = this.srNoInstruListForReqIwdArray1[0][15];

      this.calibCertificateForm.value.idNo = this.srNoInstruListForReqIwdArray1[0][11]
      this.idNo = this.srNoInstruListForReqIwdArray1[0][11]

      this.calibCertificateForm.value.srNo = this.srNoInstruListForReqIwdArray1[0][4]
      this.srNo = this.srNoInstruListForReqIwdArray1[0][4]
      this.calibCertificateForm.value.leastCount = this.srNoInstruListForReqIwdArray1[0][17]
      this.leastCount = this.srNoInstruListForReqIwdArray1[0][17]

      this.calibCertificateForm.value.accuracy = this.srNoInstruListForReqIwdArray1[0][19]
      this.accuracy = this.srNoInstruListForReqIwdArray1[0][19]
      this.accuracy1 = this.srNoInstruListForReqIwdArray1[0][22]

      this.ruom = this.srNoInstruListForReqIwdArray1[0][16]
      this.lcuom1 = this.srNoInstruListForReqIwdArray1[0][18]
      this.formulaAccuracy = this.srNoInstruListForReqIwdArray1[0][20]
      this.uomAccuracy = this.srNoInstruListForReqIwdArray1[0][21]
      this.accuracy1 = this.srNoInstruListForReqIwdArray1[0][22]
      this.formulaAccuracy1 = this.srNoInstruListForReqIwdArray1[0][23]
      this.uomAccuracy1 = this.srNoInstruListForReqIwdArray1[0][24]

      this.calibCertificateForm.value.calibratedAt = this.reqAndInwardDataObj.calibratedAt
      this.calibratedAt = this.reqAndInwardDataObj.calibratedAt

      this.calibCertificateForm.value.location = this.srNoInstruListForReqIwdArray1[0][25]
      this.location = this.srNoInstruListForReqIwdArray1[0][25]

      this.calibCertificateForm.value.instrumentType = this.srNoInstruListForReqIwdArray1[0][13]
      this.instrumentType = this.srNoInstruListForReqIwdArray1[0][13]

      this.calibratedDueOn = iwdDate1;
      this.dt = iwdDate1;

      iwdDate1.setDate(iwdDate1.getDate() - 1);
      this.dt = this.dt.setMonth(iwdDate1.getMonth() + freqNo);
      this.calibratedDueOn = this.dt;

      console.log("date: " + this.calibratedDueOn)

      this.calibCertificateForm.value.frequency = this.srNoInstruListForReqIwdArray1[0][26]
      this.frequency = this.srNoInstruListForReqIwdArray1[0][26];

      this.calibCertificateForm.value.reminderDate = this.calibCertificateForm.value.calibratedDueOn
      this.reminderDate = this.calibratedDueOn
      this.calibCertificateForm.value.frequency1 = this.srNoInstruListForReqIwdArray1[0][26]
      this.frequency1 = this.srNoInstruListForReqIwdArray1[0][26]

      this.dateCalibration = this.convertStringToDate(this.srNoInstruListForReqIwdArray1[0][33]);


      this.dateOfIssueCertificate = this.convertStringToDate(this.srNoInstruListForReqIwdArray1[0][34]);


      this.dateReceived = this.convertStringToDate(this.srNoInstruListForReqIwdArray1[0][35]);



      this.certificatePrintMode = this.srNoInstruListForReqIwdArray1[0][38]
      this.calibCertificateForm.value.certificatePrintMode = this.srNoInstruListForReqIwdArray1[0][38]

      this.remark = this.srNoInstruListForReqIwdArray1[0][39]
      this.calibCertificateForm.get('remark').patchValue(this.srNoInstruListForReqIwdArray1[0][39]);



      this.calibratedBy = this.srNoInstruListForReqIwdArray1[0][41]
      this.calibCertificateForm.get('calibratedBy').patchValue(this.srNoInstruListForReqIwdArray1[0][41]);


      this.authorizedBy = this.srNoInstruListForReqIwdArray1[0][42]
      this.instruCondition = this.srNoInstruListForReqIwdArray1[0][44]
      this.calibCertificateForm.get('instruCondition').patchValue(this.srNoInstruListForReqIwdArray1[0][44]);


      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        if (this.customerObj[i].id == this.calibratedFor) {
          this.calibratedFor = this.customerWithAddress[i].name;
        }
      }
    })

    console.log();

  }

  convertStringToDate(value) {

    if (value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return Date.now();
    }
  }

  toClose() {
    parent.postMessage(this.frm.value, location.origin);
    opener.location.reload(true);
    self.close();
  }

  deleteObservationSheet() {
    console.log("Function to remove observation sheet : ");
  }

  removeMasters() {

  }

  updateData() {

    this.requestAndInwardDocSaveObj.certificateNo = this.certificateNo;
    this.requestAndInwardDocSaveObj.receivedDate = this.datepipe.transform(this.dateReceived, 'dd-MM-yyyy');
    this.requestAndInwardDocSaveObj.dateOfCalibration = this.datepipe.transform(this.dateCalibration, 'dd-MM-yyyy');
    this.requestAndInwardDocSaveObj.dateOfIssueCertificate = this.datepipe.transform(this.dateOfIssueCertificate, 'dd-MM-yyyy');
    this.requestAndInwardDocSaveObj.calibrationDueOn = this.datepipe.transform(this.calibratedDueOn, 'dd-MM-yyyy');

    this.requestAndInwardDocSaveObj.make = this.make;
    this.requestAndInwardDocSaveObj.rangeFrom = this.rangeFrom;
    this.requestAndInwardDocSaveObj.rangeTo = this.rangeTo;
    this.requestAndInwardDocSaveObj.leastCount = this.leastCount;

    this.requestAndInwardDocSaveObj.idNo = this.idNo;
    this.requestAndInwardDocSaveObj.srNo = this.srNo;
    this.requestAndInwardDocSaveObj.orderNo = this.orderNo;
    this.requestAndInwardDocSaveObj.certificatePrintMode = this.certificatePrintMode;

    this.requestAndInwardDocSaveObj.accuracy = this.accuracy;
    this.requestAndInwardDocSaveObj.accuracy1 = this.accuracy1;
    this.requestAndInwardDocSaveObj.instruCondition = this.instruCondition;
    this.requestAndInwardDocSaveObj.remark = this.remark;

    this.requestAndInwardDocSaveObj.location = this.location;
    this.requestAndInwardDocSaveObj.instrumentType = this.instrumentType;

    this.requestAndInwardDocSaveObj.calibratedBy = this.calibratedBy;
    this.requestAndInwardDocSaveObj.authorizedBy = this.authorizedBy;
    this.requestAndInwardDocSaveObj.reminderDate = this.datepipe.transform(this.reminderDate, 'dd-MM-yyyy');


    this.createReqAndInwardService.updateReqAndInwardForCerti(this.srNoInstruListForReqIwdArray1[0][0], this.requestAndInwardDocSaveObj).subscribe(data => {
      console.log("data updated successfully" + data);
      alert("updated data created!!");
    }
    );

  }
}
