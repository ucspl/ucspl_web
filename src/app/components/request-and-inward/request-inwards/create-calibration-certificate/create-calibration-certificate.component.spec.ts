import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCalibrationCertificateComponent } from './create-calibration-certificate.component';

describe('CreateCalibrationCertificateComponent', () => {
  let component: CreateCalibrationCertificateComponent;
  let fixture: ComponentFixture<CreateCalibrationCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCalibrationCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCalibrationCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
