import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { heLocale } from 'ngx-bootstrap';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateReqAndInward, CreateReqAndInwardService, DynamicGrid, ReqTypeOfRequestAndInward } from '../../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { StudentService } from '../../../../services/student/student.service';
import { ReqdocParaDetailsComponent } from '../reqdoc-para-details/reqdoc-para-details.component';

declare var $: any;
@Component({
  selector: 'app-create-reqdoc',
  templateUrl: './create-reqdoc.component.html',
  styleUrls: ['./create-reqdoc.component.css']
})
export class CreateReqdocComponent implements OnInit {


  manageDocumentForm: FormGroup;
  reqDocForm: FormGroup;
  f2: FormGroup;

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();
  reqAndInwardDataObj1: CreateReqAndInward = new CreateReqAndInward();
  manageDocumentVariable: CreateReqAndInward = new CreateReqAndInward();

  reqTypeOfRequestAndInwardObj: ReqTypeOfRequestAndInward = new ReqTypeOfRequestAndInward();
  arrayReqTypeOfRequestAndInwardName: Array<any> = [];
  arrayReqTypeOfRequestAndInwardId: Array<any> = [];

  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  childCustomerObj: Customer = new Customer();
  childCustomers: Array<any> = [];
  customerObject: any;
  static customerNameLength: number;

  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;
  loginUserName: any;
  inwardType: any;
  reqContactPhone: any;
  noOfInstruments: any;
  customerWithAddress: Array<{ name: string, address: string }> = [];

  dynamicArray: Array<DynamicGrid> = [];
  dynamicArray1: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  inwardReqNo: any;
  inwardDate: string;
  customerNameSelected: any;
  expectedDeliveryDate: string;
  reportsInNameof: any;
  customerDcNo: string;
  reqContactName: string;
  customerDcDate: string;
  reqEmail: string;
  reqTypeOfRequestAndInwardName: any;
  id: number;
  assignedTo: string;
  createdBy: string;
  status: any;
  typeOfDocument: string;

  msgOnButtonClick: String;
  msgOnChildCompInit: String;
  notSelected: boolean;

  dateTrial = new Date();

  idForSearchSales: string;

  reqInwDocumentNo: string;
  reqTypeSelected: string;
  phoneNumberCountryNm1: any;
  trialPhoneNumber1: any;
  mobileNumber1: any;

  rowNum = 0;
  pName: any;
  tableDisplay: any;
  trial1: any;
  prefixForQuotation: string;
  docNo: any;
  displayButton: number;
  docNo1: number;
  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService, private createCustomerService: CreateCustomerService, private createReqAndInwardService: CreateReqAndInwardService) {

    this.manageDocumentForm = this.formBuilder.group({
      reqInwDocumentNo: [],
      typeOfQuotation: [],
      reqType: [],
      status: [],
      remark: [],
      createdBy: [],
      assignedTo: []
    });

    this.f2 = this.formBuilder.group({
      tName: [''],
    })

    this.reqDocForm = this.formBuilder.group({
      customerNameSelected: [''],
      expectedDeliveryDate: [''],
      reportsInNameof: [''],
      reqContactName: [''],
      customerDcNo: [''],
      reqEmail: [''],
      customerDcDate: [''],
      reqContactPhone: ['']
    })
  }

  windowRef = null;
  windowRef1 = null;

  hello() {
    console.log()
    alert("hello");

    this.dynamicArray = this.trial1;
    this.pName = this.f2.value.Tname;

  }

  hello1(e) {

    this.tableDisplay = 1;
  }

  hello2(e) {

    this.tableDisplay = 1;
  }

  displayButtonFun() {
    this.displayButton = 1;
  }

  createCertificate(i) {
    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;

    for (var ii = 0; ii < this.dynamicArray.length; ii++) {
      if (i.target.innerText == this.dynamicArray[ii].inwardInstruNo) {
        this.docNo1 = this.dynamicArray[ii].custInstruIdentificationId
      }
    }



    localStorage.setItem('docNo', JSON.stringify(this.docNo));
    localStorage.setItem('custinstruid', JSON.stringify(this.docNo1));

    var url = '/crtCalCer';
    this.windowRef1 = window.open(url, "child1", "width=1030,height=420,top=100");
    this.windowRef1.focus();
    this.windowRef1.addEventListener("message1", this.receiveMessage1.bind(this), false);
  }

  receiveMessage1(evt: any) {
    console.log(evt.data);

    this.trial1 = evt.data;
  }

  openChildWindow() {

    var url = '/reqDocParaList';
    this.windowRef = window.open(url, "child", "width=1030,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receiveMessage.bind(this), false);
  }
  receiveMessage(evt: any) {
    console.log(evt.data);

    this.trial1 = evt.data;


    if (this.trial1 != null && typeof this.trial1 != 'undefined' && typeof this.trial1 != 'object' && this.trial1 != "" || Array.isArray(this.trial1)) {
      if (this.trial1[0].nomenclature != null || typeof this.trial1[0].nomenclature != 'undefined') {

      }

    }



  }


  ngOnInit() {

    document.getElementById('id1').addEventListener('change', function () {
      $('#id2').val("GeeksForGeeks");
    });


    this.pName = "hiii parent";
    this.dynamicArray = JSON.parse(localStorage.getItem('dynamicArrayForReqAndIwd'));  //change back to this.dynamicArray 
    console.log(this.dynamicArray);


    this.phoneNumberCountryNm1 = this.createReqAndInwardService.getPhoneNoCountryFlag();

    this.reqAndInwardDataObj = this.createReqAndInwardService.getReqAndInwDataFromCreateReq();
    console.log(this.reqAndInwardDataObj);

    if (Object.keys(this.reqAndInwardDataObj).length === 0) {
      this.reqAndInwardDataObj = JSON.parse(localStorage.getItem('reqAndInwardDataObj1'));
    }
    else {
      this.reqAndInwardDataObj1 = this.reqAndInwardDataObj;
      localStorage.setItem('reqAndInwardDataObj1', JSON.stringify(this.reqAndInwardDataObj1));

    }

    if (this.phoneNumberCountryNm1 == null) {
      this.phoneNumberCountryNm1 = this.reqAndInwardDataObj.countryPrefixForMobile1;
    }


    this.newDynamic1 = [{
      srNo: "",
      inwardInstruNo: "",
      nomenclature: "",
      description: "",
      rangeSize: "",
      rangeFrom: "",
      rangeTo: "",
      leastCount: "",
      idNo: "",
      make: "",
      modelNo: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      assignedTo: ""

    }]



    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);
        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);

        if (this.customerObj[i].addressLine2 != null) {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].addressLine2 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }
        else {
          this.customerWithAddress.push({ name: this.customerObj[i].name + " / " + this.customerObj[i].department, address: this.customerObj[i].addressLine1 + "," + this.customerObj[i].city + "," + this.customerObj[i].pin + "," + this.customerObj[i].state + "," + this.customerObj[i].country });
        }

        if (this.customerObj[i].isParent == 1) {
          this.parentCustNames.push(this.customerObj[i].name);
          this.parentCustIds.push(this.customerObj[i].id);
        }
      }
      this.getDataFromCreateReq();
    },
      error => console.log(error));


    // mobileNumber code
    var input1 = document.querySelector("#reqPhone");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": [this.phoneNumberCountryNm1],
      "separateDialCode": true
    });

    var number1 = iti1.getNumber();
    var countryData1 = iti1.getSelectedCountryData();
    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      console.log(this.trialPhoneNumber1);
    });


    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {
      $('[data-toggle="tooltip_edit_uuc_name"]').tooltip();
    });

  }

  getDataFromCreateReq() {
    this.inwardReqNo;
    this.id = this.reqAndInwardDataObj.id;
    this.inwardDate = this.reqAndInwardDataObj.inwardDate;
    this.expectedDeliveryDate = this.reqAndInwardDataObj.expectedDeliveryDate;
    this.noOfInstruments = this.reqAndInwardDataObj.noOfInstruments;
    this.reqContactName = this.reqAndInwardDataObj.reqContactName;
    this.customerDcNo = this.reqAndInwardDataObj.customerDCNo;
    this.reqContactPhone = this.reqAndInwardDataObj.reqContactPhone;

    this.reqContactPhone = this.reqAndInwardDataObj.reqContactPhone.slice(-10);
    var mb1 = new String(this.reqAndInwardDataObj.reqContactPhone)
    this.trialPhoneNumber1 = this.reqAndInwardDataObj.reqContactPhone.slice(-mb1.length + 1, -10);
    console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

    this.customerDcDate = this.reqAndInwardDataObj.dcDate;
    this.reqEmail = this.reqAndInwardDataObj.reqEmail;
    this.createdBy = this.reqAndInwardDataObj.createdBy;
    this.reqTypeSelected = this.reqAndInwardDataObj.reqType;

    this.inwardReqNo = this.reqAndInwardDataObj.reqInwDocumentNo.slice(10, 24);
    this.createReqAndInwardService.setInwardReqNoFromReqDoc(this.inwardReqNo);
    localStorage.setItem('inwardReqNo', JSON.stringify(this.inwardReqNo));

    this.reqInwDocumentNo = this.reqAndInwardDataObj.reqInwDocumentNo;
    localStorage.setItem('reqInwDocumentNo', JSON.stringify(this.reqInwDocumentNo));

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj.customerName) {
        this.customerNameSelected = this.customerWithAddress[i].name;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.reqAndInwardDataObj.reportInNameOf) {
        this.reportsInNameof = this.customerWithAddress[i].name;
      }
    }

    this.createReqAndInwardService.getReqTypeOfRequestAndInwards().subscribe(data => {
      this.reqTypeOfRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {
        this.arrayReqTypeOfRequestAndInwardName.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeName)
        this.arrayReqTypeOfRequestAndInwardId.push(this.reqTypeOfRequestAndInwardObj[i].reqTypeId)

        if (this.reqTypeOfRequestAndInwardObj[i].reqTypeId == parseInt(this.reqAndInwardDataObj.reqType)) {
          this.reqTypeSelected = this.reqTypeOfRequestAndInwardObj[i].reqTypeName;
        }

      }
    })

  }

  getinformationForManageDoc() {

  }

  createInwardReqNo() {

    this.reqInwDocumentNo
    console.log("this.manage document is updated successfully");

  }

  updateManageDoc() {

    this.manageDocumentVariable.assignedTo = this.assignedTo;
    this.manageDocumentVariable.createdBy = this.createdBy;
    this.manageDocumentVariable.typeOfDocument = this.typeOfDocument;

    this.manageDocumentVariable.reqInwDocumentNo = this.reqInwDocumentNo;
    this.manageDocumentVariable.inwardReqNo = this.inwardReqNo;
    this.manageDocumentVariable.inwardDate = this.inwardDate;
    this.manageDocumentVariable.expectedDeliveryDate = this.expectedDeliveryDate;
    this.manageDocumentVariable.reqContactName = this.reqContactName;
    this.manageDocumentVariable.customerDCNo = this.customerDcNo;
    this.manageDocumentVariable.reqContactPhone = this.reqContactPhone;
    this.manageDocumentVariable.dcDate = this.customerDcDate;
    this.manageDocumentVariable.reqEmail = this.reqEmail;
    this.manageDocumentVariable.reqType = this.reqTypeSelected;


    switch (this.status) {
      case 'Draft': this.manageDocumentVariable.draft = 1;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Archieved': this.manageDocumentVariable.draft = 0
        this.manageDocumentVariable.archieved = 1;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Rejected': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 1;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Approved': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 1;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Submitted': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 1;
        break;

    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.customerNameSelected) {
        this.manageDocumentVariable.customerName = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerWithAddress[i].name == this.reportsInNameof) {
        this.manageDocumentVariable.reportInNameOf = this.customerObj[i].id;
      }
    }

    for (var i = 0, l = Object.keys(this.reqTypeOfRequestAndInwardObj).length; i < l; i++) {

      if (this.reqTypeOfRequestAndInwardObj[i].reqTypeName == this.reqTypeSelected) {
        this.manageDocumentVariable.reqType = this.reqTypeOfRequestAndInwardObj[i].reqTypeId;
      }

    }

    localStorage.setItem('reqAndInwardDataObj1', JSON.stringify(this.manageDocumentVariable));//comment if not working properly

    this.createReqAndInwardService.updateReqAndInward(this.id, this.manageDocumentVariable).subscribe(data => {
      console.log(data);
      alert("Saved Successfully!!");
    })
  }


  paraListShow() {
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }

  openModal1() {
    this.getinformationForManageDoc();
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()
  }

  backToMenu() {
    this.router.navigateByUrl('/nav/createreq');
  }

  //////////////////////////////////////////////// code for adding new row //////////////////////////////////

  addRow() {


    this.newDynamic = {
      srNo: "",
      inwardInstruNo: "",
      nomenclature: "",
      description: "",
      rangeSize: "",
      rangeFrom: "",
      rangeTo: "",
      leastCount: "",
      idNo: "",
      make: "",
      modelNo: "",
      location: "",
      calibrationFrequency: "",
      instruType: "",
      assignedTo: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }

  deleteRow(index) {
    if (this.dynamicArray.length == 1) {

      return false;
    } else {
      this.dynamicArray.splice(index, 1);

      return true;
    }
  }

  saveAllData() {
    this.updateManageDoc();
  }


}
