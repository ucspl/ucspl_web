import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReqdocComponent } from './create-reqdoc.component';

describe('CreateReqdocComponent', () => {
  let component: CreateReqdocComponent;
  let fixture: ComponentFixture<CreateReqdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReqdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReqdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
