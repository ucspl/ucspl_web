import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialExamppleComponent } from './trial-exampple.component';

describe('TrialExamppleComponent', () => {
  let component: TrialExamppleComponent;
  let fixture: ComponentFixture<TrialExamppleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrialExamppleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialExamppleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
