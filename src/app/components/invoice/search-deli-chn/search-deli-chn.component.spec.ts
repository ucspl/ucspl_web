import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDeliChnComponent } from './search-deli-chn.component';

describe('SearchDeliChnComponent', () => {
  let component: SearchDeliChnComponent;
  let fixture: ComponentFixture<SearchDeliChnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDeliChnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDeliChnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
