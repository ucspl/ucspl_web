import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchProIncComponent } from './search-pro-inc.component';

describe('SearchProIncComponent', () => {
  let component: SearchProIncComponent;
  let fixture: ComponentFixture<SearchProIncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchProIncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchProIncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
