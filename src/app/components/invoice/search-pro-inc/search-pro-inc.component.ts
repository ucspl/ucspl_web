
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DynamicGrid } from '../../../app.module';
import { Instrument } from '../../../services/serviceconnection/create-service/create-def.service';
import { CreateInvoiceService } from '../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateSalesService } from '../../../services/serviceconnection/create-service/create-sales.service';
import { SearchInvoiceDocByDocument, SearchInvoiceService } from '../../../services/serviceconnection/search-service/search-invoice.service';


@Component({
  selector: 'app-search-pro-inc',
  templateUrl: './search-pro-inc.component.html',
  styleUrls: ['./search-pro-inc.component.css']
})
export class SearchProIncComponent implements OnInit {

  searchAndModifyInvoiceForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;
  dateString = '';
  format = 'dd/MM/yyyy';
  emp: any;
  searchInvoiceDocObject: SearchInvoiceDocByDocument = new SearchInvoiceDocByDocument();
  invoiceDocArray: Array<any> = [];

  docNo: string;
  statusChoice: any;
  data: any;
  status: string;
  public dateValue1 = new Date();
  public dateValue2 = new Date();
  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  documentNumberFromSearchInvoice: string;
  challanCumInvoiceArrayForSearchInvoice: any;
  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];
  toDate: any;
  fromDate: any;

  constructor(private formBuilder: FormBuilder, private router: Router, public datepipe: DatePipe, private searchInvoiceService: SearchInvoiceService, private createInvoiceService: CreateInvoiceService, private createSalesService: CreateSalesService) {
    this.searchAndModifyInvoiceForm = this.formBuilder.group({
      searchDocument: [],
      itemsPerPage: [],
      toDate: [],
      fromDate: [],
      statusChoice: []
    });
  }

  ngOnInit() {
    this.statusChoice = "selectStatus";

    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));
  }


  showData() {
    this.searchInvoiceService.showData().subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.invoiceDocArray = this.data;

      for (var i = 0; i < this.invoiceDocArray.length; i++) {

        if (this.invoiceDocArray[i][5] == 1) {
          this.status = "Draft";
          this.invoiceDocArray[i].push("Draft")
        }
        if (this.invoiceDocArray[i][6] == 1) {
          this.status = "Archieved";
          this.invoiceDocArray[i].push("Archieved")
        }
        if (this.invoiceDocArray[i][7] == 1) {
          this.status = "Submitted";
          this.invoiceDocArray[i].push("Submitted")
        }
        if (this.invoiceDocArray[i][8] == 1) {
          this.status = "Approved";
          this.invoiceDocArray[i].push("Approved")
        }
        if (this.invoiceDocArray[i][9] == 1) {
          this.status = "Rejected";
          this.invoiceDocArray[i].push("Rejected")
        }
      }

      for (var i = 0; i < this.invoiceDocArray.length; i++) {

        if (this.invoiceDocArray[i][4].length != 10) {
          this.invoiceDocArray[i][4] = this.invoiceDocArray[i][4].slice(0, 10);
          this.invoiceDocArray[i][4] = this.datepipe.transform(this.invoiceDocArray[i][4], 'dd-MM-yyyy');
        }

      }

    })
  }


  searchData() {
    debugger;
    if (typeof this.searchAndModifyInvoiceForm.value.toDate == "undefined" || this.searchAndModifyInvoiceForm.value.toDate == null) {
      this.searchInvoiceDocObject.searchDocument = this.searchAndModifyInvoiceForm.value.searchDocument;
      if (this.searchInvoiceDocObject.searchDocument != null && typeof this.searchInvoiceDocObject.searchDocument != 'undefined') {
        this.searchInvoiceDocObject.searchDocument = this.searchInvoiceDocObject.searchDocument.trim();
      }
      switch (this.statusChoice) {
        case 'selectStatus': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Draft': this.searchInvoiceDocObject.draft = 1;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Archieved': this.searchInvoiceDocObject.draft = 0
          this.searchInvoiceDocObject.archieved = 1;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Rejected': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 1;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Approved': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 1;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Submitted': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 1;
          break;

      }
    }
    else {
      console.log(this.toDate);
      this.toDate.setDate(this.toDate.getDate() + 1);
      this.searchInvoiceDocObject.toDate = this.datepipe.transform(this.searchAndModifyInvoiceForm.value.toDate, 'yyyy-MM-dd');
      this.searchInvoiceDocObject.fromDate = this.datepipe.transform(this.searchAndModifyInvoiceForm.value.fromDate, 'yyyy-MM-dd');
      this.searchInvoiceDocObject.searchDocument = this.searchAndModifyInvoiceForm.value.searchDocument;
      if (this.searchInvoiceDocObject.searchDocument != null && typeof this.searchInvoiceDocObject.searchDocument != 'undefined') {
        this.searchInvoiceDocObject.searchDocument = this.searchInvoiceDocObject.searchDocument.trim();
      }
      switch (this.statusChoice) {
        case 'selectStatus': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Draft': this.searchInvoiceDocObject.draft = 1;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Archieved': this.searchInvoiceDocObject.draft = 0
          this.searchInvoiceDocObject.archieved = 1;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Rejected': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 1;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Approved': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 1;
          this.searchInvoiceDocObject.submitted = 0;
          break;
        case 'Submitted': this.searchInvoiceDocObject.draft = 0;
          this.searchInvoiceDocObject.archieved = 0;
          this.searchInvoiceDocObject.rejected = 0;
          this.searchInvoiceDocObject.approved = 0;
          this.searchInvoiceDocObject.submitted = 1;
          break;

      }
    }



    if (this.searchInvoiceDocObject == null) {
      alert("please enter criteria to search");
    }
    else {
      this.searchInvoiceService.searchData(this.searchInvoiceDocObject).subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
      })
      this.showData();
    }

  }

  clearForm(i) {

    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;
    this.searchInvoiceService.setDocNoToUpdateInvoiceDoc(this.docNo);
    this.getSrNoForSearchInvoice()

  }

  getSrNoForSearchInvoice() {

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.docNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
   

    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceArrayForSearchInvoice = data;

      if (this.challanCumInvoiceArrayForSearchInvoice.length != 0) {

        for (var z = 0; z < this.challanCumInvoiceArrayForSearchInvoice.length; z++) {


          this.newDynamic1.push({
            uniqueId: this.challanCumInvoiceArrayForSearchInvoice[z][0],
            srNo: this.challanCumInvoiceArrayForSearchInvoice[z][3],
            poSrNO: this.challanCumInvoiceArrayForSearchInvoice[z][4],
            itemOrPartCode: this.challanCumInvoiceArrayForSearchInvoice[z][5],
            instruName: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            description: this.challanCumInvoiceArrayForSearchInvoice[z][7],
           // idNo: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            rangeFrom: this.challanCumInvoiceArrayForSearchInvoice[z][8],
            rangeTo: this.challanCumInvoiceArrayForSearchInvoice[z][9],
            sacCode: this.challanCumInvoiceArrayForSearchInvoice[z][10],
            hsnNo: this.challanCumInvoiceArrayForSearchInvoice[z][11],
            quantity: this.challanCumInvoiceArrayForSearchInvoice[z][12],
            editQuant: this.challanCumInvoiceArrayForSearchInvoice[z][12],
            unitPrice: this.challanCumInvoiceArrayForSearchInvoice[z][13],
            amount: this.challanCumInvoiceArrayForSearchInvoice[z][14]

          })

        }

        this.dynamicArray.splice(0, 1);
        for (var l = 1; l < this.newDynamic1.length; l++) {
          this.convertInsruIdtoName(l);
          this.dynamicArray.push(this.newDynamic1[l]);
        }
      }
      localStorage.setItem('dynamicArrayForInvoice', JSON.stringify(this.dynamicArray));
      localStorage.setItem('invoiceDocNo', JSON.stringify(this.docNo));
      this.router.navigateByUrl('/updateinvdoc')

    })
  })
  }

  convertInsruIdtoName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;

      }
    }
  }

}