import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInvdocComponent } from './update-invdoc.component';

describe('UpdateInvdocComponent', () => {
  let component: UpdateInvdocComponent;
  let fixture: ComponentFixture<UpdateInvdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInvdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInvdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
