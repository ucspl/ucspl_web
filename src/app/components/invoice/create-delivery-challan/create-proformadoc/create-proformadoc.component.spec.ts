import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProformadocComponent } from './create-proformadoc.component';

describe('CreateProformadocComponent', () => {
  let component: CreateProformadocComponent;
  let fixture: ComponentFixture<CreateProformadocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProformadocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProformadocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
