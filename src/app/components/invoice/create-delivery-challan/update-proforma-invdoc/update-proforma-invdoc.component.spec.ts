import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProformaInvdocComponent } from './update-proforma-invdoc.component';

describe('UpdateProformaInvdocComponent', () => {
  let component: UpdateProformaInvdocComponent;
  let fixture: ComponentFixture<UpdateProformaInvdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProformaInvdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProformaInvdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
