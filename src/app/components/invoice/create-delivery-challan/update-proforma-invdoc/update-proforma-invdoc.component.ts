

import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { mergeNsAndName, ngModuleJitUrl, Statement, templateSourceUrl } from '@angular/compiler';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { asTextData } from '@angular/core/src/view';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateCustomerPurchaseOrder, CreateCustPoService, PurchaseOrderDocument } from '../../../../services/serviceconnection/create-service/create-cust-po.service';

//import { SSL_OP_SINGLE_DH_USE } from 'constants';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { Branch, DocumentTypeForInvoice, Instrument, InvoiceTypeForInvoice } from '../../../../services/serviceconnection/create-service/create-def.service';
import { ChallanInvoiceDocCalculation, ChallanInvoiceDocument, CreateChallanCumInvoice, CreateInvoiceService, DynamicGrid, InvoiceAndrequestNumberJunction } from '../../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateSalesService } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { SearchInvoiceService } from '../../../../services/serviceconnection/search-service/search-invoice.service';
import { CreateInvdocComponent } from '../create-invdoc/create-invdoc.component';

declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-update-proforma-invdoc',
  templateUrl: './update-proforma-invdoc.component.html',
  styleUrls: ['./update-proforma-invdoc.component.css']
})
export class UpdateProformaInvdocComponent implements OnInit {

  manageDocumentForm: FormGroup;
  reqdocParaDetailsForm: FormGroup;

  createChallanCumInvoiceObj: CreateChallanCumInvoice = new CreateChallanCumInvoice();
  createChallanCumInvoiceObj1: CreateChallanCumInvoice = new CreateChallanCumInvoice();
  challanCumInvoiceDocumentObject: ChallanInvoiceDocument = new ChallanInvoiceDocument();
  challanInvoiceDocCalculationObject: ChallanInvoiceDocCalculation = new ChallanInvoiceDocCalculation();

  createInvoiceInfoForSearchInvoiceObject: CreateChallanCumInvoice = new CreateChallanCumInvoice();
  challanInvoiceForDocument: CreateChallanCumInvoice = new CreateChallanCumInvoice();

  j: any = 0;
  l: any = 0;
  tl: Array<number> = [0];
  k: any;
  n: any = 0;
  sot: any = 0;
  sumAll: any = 0;
  tcode: string;
  subTotal: number = 0;
  discOnSubTotal: number = 0;
  total: number = 0;
  netValue: any;
  valueConvertToWord: any = 0;
  gstTotalValue: any = 0;
  gstValueInWords: string;
  cgstValueInWords: string;
  igstValueInWords: string;
  sgstValueInWords: string;

  invoiceTypeOfDoc: string;

  cgstAmount: any = 0;
  sgstAmount: any = 0;
  igstAmount: any = 0;
  amountArray: Array<any> = [];
  data: string[] = [];
  arrayOfDescription: string[] = [];
  arrayOfIdNo: string[] = [];
  arrayOfAccrediation: string[] = [];
  arrayOfRangeAccuracy: string[] = [];
  arrayOfSacCode: string[] = [];
  arrayOfQuantity: Array<number> = [];
  arrayOfRate: Array<number> = [];
  arrayOfDiscountOnItem: Array<number> = [];
  arrayOfDiscountOnTotal: Array<number> = [];
  arrayOfAmount: Array<number> = [];
  arrayOfTerms: Array<number> = [];

  selectedFileForQuotation: any;

  customers: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  static customerNameLength: number;

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  name: String;
  invDocNumber: string;
  prefixInvDocNumber: any;
  salesDocumentInfo: Array<any> = [];
  salesQuotationDocumentForm: FormGroup

  selectedRow: Number;
  checkBoxes: boolean[];

  kindAttnTrial: string;

  static customerDepartments: Array<any> = [];
  customer: Customer = new Customer();
  customer1: Customer = new Customer();
  custRefTrial: any;
  netValueInWords: string;
  valueInWords: string;

  headerType: any;
  billedCustNameTrial: any;
  billedCustPanNo: any;
  billedCustGstNo: any;
  billedCustAddressline1: any;
  billedCustAddressline2: any;
  billedCustCountry: any;
  billedCustState: any;
  billedCustCity: any;
  billedCustPin: any;
  billedCustDepartment: any;
  billedNameAndDept: any;

  shippedCustNameTrial: any;
  shippedCustPanNo: any;
  shippedCustGstNo: any;
  shippedCustAddressline1: any;
  shippedCustAddressline2: any;
  shippedCustCountry: any;
  shippedCustState: any;
  shippedCustCity: any;
  shippedCustPin: any;
  shippedCustDepartment: any;
  shippedNameAndDept: any;
  dateOfQuotation1: any;
  custRefNo1: number;
  refDate1: any;
  kindAttachment1: number;
  stateCode: any;

  termsIdArray: Array<any> = [];
  termsIdArray1: any;
  termsIdArray2: Array<any> = [];
  termsIdArray3: Array<any> = [];
  termsIdArray4: Array<any> = [];

  selectTagEvent: any;
  m: any = 0;
  s: any = 0;
  taxesArray: Array<any> = [];
  taxesArrayName: Array<any> = [];

  dateString = '';
  format = 'dd/MM/yyyy';
  alive = true;
  public dateValue = new Date();
  dateOfQuotation: any;
  custRefNo: string;
  refDate: any;
  archieveDate = new Date();
  kindAttnTrial1: string;
  documentNumber: string;
  typeOfQuotation: string;
  contactNo: string;
  email: string;
  status: string;
  remark: string;
  createdBy: string;
  manageDocumentVariable: any;
  salesQuotationDocumentVariable: any;
  challanCumInvoiceDocumentVariableArray: any;
  id: any;

  custNameTrialShippedTo: string;
  custNameTrialBilledTo: string;

  public termValue: string;

  termsList1: Array<{ id: number, name: string }> = [];
  taxesList: Array<{ name: string, taxAmount: number }> = [];
  taxesTotal: any = 0;

  trialCustomerWithAddress: Array<{ customerName: string, name: string, address: string }> = [];

  width: any = 5;
  height: any;
  originalDocNo: string;

  srNoArray: Array<number> = [];

  salesQuotationDocumentVariableArray: Array<any> = [];

  salesQuotationDocumentCalculationVariableArray: Array<any> = [];
  searchSalesQuotationDocumentCalculationArray: Array<any> = [];

  searchChallanInvoiceDocumentCalculationArray: Array<any> = [];

  challanCumInvoiceDocumentCalculationVariableArray: Array<any> = [];

  headerInformationArrayForSearchSales: Array<any> = [];
  invDocNumberWithPrefix: string;
  documentNumberFromSearchSales: string;
  documentNumberFromSearchInvoice: string;
  challanCumInvoiceArrayForSearchInvoice: any;

  termsIdForSearchSalesArray: Array<number> = [];

  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  dynamicArrayTrial1: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  customerIdForcustShippedTo: any;
  customerIdForcustBilledTo: any;

  itemType: any;
  taxes: any;
  documentName: string;
  documentType: string;
  documentDate = new Date();
  invoiceDocumentOFManageDocument: CreateChallanCumInvoice;
  leng: number;
  srNoToDelete: any;
  indexToDelete: number;

  selectTagEventForHeader: any;
  dcDate = new Date();
  dcNo: string;
  dateOfInvoice: any;
  poDate = new Date();
  poNo: any;
  vendorCodeNo: string;
  branch1: Branch = new Branch();
  branchesName: Array<any> = [];
  branchesIds: Array<any> = [];
  branchesDescription: Array<any> = [];
  branchWithDescription: Array<{ description: string, branch: string }> = [];
  dcustomerDcNumber: string;
  requestNumber: string;
  customerDcNumber: string;

  documentTypeObjectData: DocumentTypeForInvoice = new DocumentTypeForInvoice();
  documentTypeName: Array<any> = [];
  documentTypeIds: Array<any> = [];

  invoiceTypeObjectData: InvoiceTypeForInvoice = new InvoiceTypeForInvoice();
  invoiceTypeName: Array<any> = [];
  invoiceTypeIds: Array<any> = [];
  invoiceType: any;

  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];
  billedStateCode: any;
  discOnSubTotalForShow: number;
  cgstDisplay: number;
  sgstDisplay: number;
  igstDisplay: number;
  taxAmount: any;
  igst: any;
  cgst: any;
  sgst: any;

  dropdownSettings = {};
  dropdownList = [];
  dropdownList1 = [];
  countForRequestNumbers: {};
  countForRequestNumbers1: {};
  uniqueRequestNumbersAvailable: Array<any> = [];
  onlyRequestAndInwardNumberArray: Array<any> = [];
  selectedRequestNumberFromCreateInvoiceScreen: { itemId: number; itemText: string; }[];
  selectedItems: Array<{ itemId: number, itemText: string }> = [];
  ct: number = 0;
  trialArray: Array<any> = [];
  arrayOfInstruNameAndRange: Array<any> = [];
  countForInstruNameAndRange1: Array<any> = [];
  countForInstruNameAndRange: {};
  mii: number;
  instruNameWithRangeArray: Array<any> = [];
  instruNameWithRangeArray1: Array<any> = [];
  rangeFromArray: Array<any> = [];
  rangeToArray: Array<any> = [];
  hideRange: number;
  uniqueInstrumentsAvailable: Array<any> = [];
  instruNamesArray: Array<any> = [];
  mi: any;
  mi1: any;

  countForInstruName: {};
  countForInstruName1: {};
  countForInstruName2: {};
  countForInstruName3: {};

  requestNumberCollectionArrayForInvoice: Array<any> = [];
  onlyRequestNumberCollectionArrayForInvoice: Array<any> = [];
  allrequestNumbersDataForCalibrationTag: Array<any> = [];
  requestNumbersArray: Array<any> = [];
  requestNumbersCollectionArray: Array<any> = [];
  poDataForInvoice: any;
  redDataForCalibrationTag: any;
  hideHsn: number;
  AllRequestNumbers: any;
  allrequestNumbersUnique: Array<any> = [];
  called: boolean;
  docT: any;
  removeData: any;
  manDocForColor: any;
  printTermsId: any;
  dynamicArrayTrial2: any = [{}];
  onlyRangeFromArray: Array<any> = [];
  onlyRangeToArray: Array<any> = [];

  invoiceAndrequestNumberJunction: InvoiceAndrequestNumberJunction = new InvoiceAndrequestNumberJunction();
  trailReqNoData: any;
  createChallanCumInvoiceObjForColor: Object;
  property: number = 0;
  srNoInvoiceDocArray: any;
  finalInvoiceTagArray: Array<{ requestNo: string, instruName: string, reqDocId: string, invoiceDocNo: string, invoiceId: number, invUniqueNo: string }> = [];
  finalInvoiceTagArray1: string;
  finalInvoiceTagArrayForRange: Array<{ requestNo: string, instruName: string, rangeFrom: string, rangeTo: string, reqDocId: string, invoiceDocNo: string, invoiceId: number, invUniqueNo: string }> = [];
  finalInvoiceTagArray1ForRange: string;
  rrayOfInvoiceTagForReqNo: Array<any> = [];
  arryOfInvoiceTagForInstruName: Array<any> = [];
  trailInvoiceTagData: string[];
  trialInvoiceArray: Array<any> = [];
  arrayOfInvoiceTagForReqNo: Array<any> = [];
  arrayTosetInvoiceTagToOne: Array<any> = [];
  arrayTosetInvoiceTagToOneTrial: Array<any> = [];
  jj: any = 0;
  hideSaveColumn: number = 0;


  instruNamesArrayForTrial: Array<any> = [];
  arrayOfInstruNameAndRangeTrial: Array<any> = [];
  reqDocId: any;
  challanCumInvoiceDocumentVariableArrayForUniqueTrial: any;
  instruNameWithRangeArray1Edit: Array<any> = [];
  instruNameWithRangeArrayEdit: Array<any> = [];

  countForInstruNameAndRange2: {};
  countForInstruNameAndRange3: Array<any> = [];
  rangeToArrayEdit: Array<any> = [];
  rangeFromArrayEdit: Array<any> = [];

  instruNamesArrayForEdit: Array<any> = [];
  uniqueInstrumentsAvailableEdit: Array<any> = [];
  countForInstruName1Edit: {};
  countForInstruNameEdit: {};
  finalInvoiceTagArrayForReqDocNoAndInvDocNo: Array<{ invoiceDocNo: string, reqDocNo: string }> = [];
  finalInvoiceTagArrayForReqDocNoAndInvDocNo1: string;
  invoiceTagSetToZeroForReqNoAndInvNo: string[];
  allrequestNumbersUnique1: any;
  groupByIdInvoiceTagArray: Array<{ invoiceTag: number, instruName: string, reqDocId: number }> = [];
  showMessage: number = 0;



  //checkAmountArrayTrial:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
 // checkQuantityArrayTrial :Array<{instruName:string,docTypePO:string,remQuantity:any,srNoTableId:any,rangeFromPo:any,rangeToPo:any}>=[];
  remainingQuantity: number;
   jjj:number;
   purchaseOrderDocumentObject:PurchaseOrderDocument=new PurchaseOrderDocument();
   createCustomerPurchaseOrderObject:CreateCustomerPurchaseOrder=new CreateCustomerPurchaseOrder();
  showTotalQuantMsg: number;
  showPoMessage: number;

  checkAmountArray:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
  checkAmountArray1:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
  checkQuantityArray :Array<{instruName:string,docTypePO:string,remQuantity:any,srNoTableId:any,rangeFromPo:any,rangeToPo:any,ratePo:any}>=[];
  back: any;
 // poNo: any;
  docType1: any;
  trialPoValue: any;
  trialRemPoValue: any;
 
  dataForpoNumbersArray:Array<any>=[];
  onlyPoNumbersArray:Array<any>=[];
  uniquePoNumbersArray:Array<any>=[];
  onlyDocumentTypeArray:Array<any>=[];
  uniqueDocumentTypeArray:Array<any>=[];
  displayRate: number=1;
  instrumentNameForPo: any;
  uniqueInstrumentFromPo: Array<any> = [];
  instrumentFromPo: Array<any> = [];
  countForInstruNamePo: {};
  countForInstruNamePo1:{};
  rateArray1:Array<any> = [];
  trialSetFlag: any=1;
  mm:any=0;
  trialSetFlag1: number=1;
  trialremainingQuantityCheck:Array<any> = [];
  trialremainingQuantityCheckForRange:Array<any> = [];
  netValueOld: any;
  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private createInvoiceService: CreateInvoiceService, private searchInvoiceService: SearchInvoiceService, private createCustomerService: CreateCustomerService, private createUserService: CreateUserService, private createSalesService: CreateSalesService, public datepipe: DatePipe,private createCustPoService : CreateCustPoService) {

    this.manageDocumentForm = this.formBuilder.group({
      documentType: [],
      documentDate: [],
      documentNumber: [],
      requestNumber: [],
      selectedItems: [],
      customerDcNumber: [],
      archieveDate: [],
      status: [],
      invoiceTypeOfDoc: [],
      remark: [],
      createdBy: []

    });

    this.reqdocParaDetailsForm = this.formBuilder.group({
      Rows: this.formBuilder.array([this.initRows()])
    });

  }

  ngOnInit() {

    this.selectedItems = [

    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
    };

    this.newDynamic = {
      uniqueId: "",
      srNo: "",
      poSrNO: "",
      itemOrPartCode: "",
      instruName: "",
      description: "",
      idNo: "",
      accrediation: "",
      rangeFrom: "",
      rangeTo: "",
      sacCode: "",
      hsnNo: "",
      quantity: "",
      editQuant: "",
      rate: "",
      discountOnItem: "",
      unitPrice: "",
      amount: "",
      reqDocId: ""
    };
    this.dynamicArray.push(this.newDynamic);

    //get instrument list
    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));

    //get document type list
    this.createInvoiceService.getDocumentTypeListForInvoice().subscribe(data => {
      this.documentTypeObjectData = data;
      console.log(this.documentTypeObjectData);

      for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
        this.documentTypeName.push(this.documentTypeObjectData[i].documentTypeName);

        this.documentTypeIds.push(this.documentTypeObjectData[i].documentTypeId);

      }
    },
      error => console.log(error));

    //get invoice type list
    this.createInvoiceService.getInvoiceTypeListForInvoice().subscribe(data => {
      this.invoiceTypeObjectData = data;
      console.log(this.invoiceTypeObjectData);

      for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {
        this.invoiceTypeName.push(this.invoiceTypeObjectData[i].invoiceTypeName);

        this.invoiceTypeIds.push(this.invoiceTypeObjectData[i].invoiceTypeId);

      }
    },
      error => console.log(error));

    //get branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branch1 = data;
      console.log(this.branch1);
      for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
        this.branchesName.push(this.branch1[i].branchName);
        this.branchesIds.push(this.branch1[i].branchId);
        this.branchesDescription.push(this.branch1[i].description);

        this.branchWithDescription.push({ description: this.branch1[i].description, branch: this.branch1[i].branchName });
      }
      //this.getSrNoData();    
    },
      error => console.log(error));


    //Sr no data from serach invoice
    // this.dynamicArrayTrial = JSON.parse(localStorage.getItem('dynamicArrayForInvoice'));
    // this.dynamicArrayTrial1 = JSON.parse(localStorage.getItem('dynamicArrayForInvoice'));
    // this.dynamicArrayTrial=null;
    //this.dynamicArrayTrial1=null;

    if (this.dynamicArrayTrial == null || typeof this.dynamicArrayTrial == 'undefined' || this.dynamicArrayTrial.length == 0) {
      this.reqdocParaDetailsForm = this.formBuilder.group({
        Rows: this.formBuilder.array([this.initRows()])

      });


    } else {

      this.reqdocParaDetailsForm = this.formBuilder.group({
        Rows: this.formBuilder.array(
          this.dynamicArrayTrial.map(({

            srNo,
            poSrNO,
            itemOrPartCode,
            instruName,
            description,
            rangeFrom,
            rangeTo,
            sacCode,
            hsnNo,
            quantity,
            editQuant,
            unitPrice,
            amount
          }) =>
            this.formBuilder.group({
              srNo: [srNo],
              poSrNO: [poSrNO],
              description: [description],
              itemOrPartCode: [itemOrPartCode],
              rangeFrom: [rangeFrom],
              rangeTo: [rangeTo],
              sacCode: [sacCode],
              hsnNo: [hsnNo],
              quantity: [quantity],
              editQuant: [editQuant],
              unitPrice: [unitPrice],
              instruName: [instruName],
              amount: [amount]

            })
          )
        )

      })

    }

    //  this.GetDeliveryChallanInformation();
    $(function () {
      // on init
      $(".table-hideable .hide-col").each(HideColumnIndex);

      // on click
      $('.hide-column').click(HideColumnIndex)

      function HideColumnIndex() {
        var $el = $(this);
        var $cell = $el.closest('th,td')
        var $table = $cell.closest('table')

        // get cell location - https://stackoverflow.com/a/4999018/1366033
        var colIndex = $cell[0].cellIndex + 1;

        // find and hide col index
        $table.find("tbody tr, thead tr")
          .children(":nth-child(" + colIndex + ")")
          .addClass('hide-col');

        // show restore footer
        $table.find(".footer-restore-columns").show()
      }

      // restore columns footer
      $(".restore-columns").click(function (e) {
        var $table = $(this).closest('table')
        $table.find(".footer-restore-columns").hide()
        $table.find("th, td")
          .removeClass('hide-col');

      })

    })


    ////////////////comment if problem arise ////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////

    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customer1 = data;
      console.log(this.customer1);
      // this.getSrNoData();       ///comment if problem arise ////////////////////////////////////////

      this.getChallanCumInvoiceDocumentSearchInfo();


      CreateInvdocComponent.customerNameLength = Object.keys(this.customer1).length;
      for (var i = 0, l = Object.keys(this.customer1).length; i < l; i++) {
        this.customers.push(this.customer1[i].name + " / " + this.customer1[i].department);
        console.log("customers : " + this.customers);
        CreateInvdocComponent.customerNames.push(this.customer1[i].name);
        CreateInvdocComponent.customerDepartments.push(this.customer1[i].department);


        this.trialCustomerWithAddress.push({ customerName: this.customer1[i].name, name: this.customer1[i].name + " / " + this.customer1[i].department, address: this.customer1[i].addressLine1 + "," + this.customer1[i].addressLine2 + "," + this.customer1[i].city + "," + this.customer1[i].pin + "," + this.customer1[i].state + "," + this.customer1[i].country });

        this.customersIds.push(this.customer1[i].id);

        console.log("customer id : " + this.customersIds);
      }
      // if (this.customersIds.length == Object.keys(this.customer1).length)
      //   this.getDeliveryChallanInformation();

    },
      error => console.log(error));


    // //code when field value change and color of button
    // (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
    //   console.log(values);

    //   if (this.dynamicArrayTrial.length != 0) {

    //     for (var i = 0, len = values.length; i < len; i++) {
    //       if (this.dynamicArrayTrial[i].instruName != null) {
    //         if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].poSrNO !== values[i].poSrNO || this.dynamicArrayTrial[i].itemOrPartCode !== values[i].itemOrPartCode || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
    //           this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].quantity !== values[i].quantity
    //           || this.dynamicArrayTrial[i].editQuant !== values[i].editQuant
    //           || this.dynamicArrayTrial[i].unitPrice !== values[i].unitPrice || this.dynamicArrayTrial[i].amount !== values[i].amount || this.dynamicArrayTrial[i].hsnNo !== values[i].hsnNo)
    //         ) {
    //           console.log("change" + i);
    //           var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
    //           document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";


    //         }
    //       }
    //     }
    //   }
    // });



    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial.length != 0 && values.length != 0) {

        for (var i = 0, len = values.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].instruName != null && typeof this.dynamicArrayTrial[i].instruName != 'undefined' &&  typeof this.dynamicArrayTrial[i].editQuant !=='undefined' && values[i].unitPrice != null && values[i].amount != null && values[i].srNo != null && values[i].poSrNO != null && values[i].itemOrPartCode != null && values[i].instruName != null && values[i].sacCode != null && values[i].quantity != null && values[i].editQuant != null &&
              values[i].unitPrice != "" && values[i].amount != ""  && values[i].poSrNO != "" && values[i].itemOrPartCode != "" && values[i].instruName != "" && values[i].sacCode != "" && values[i].quantity != "" && values[i].editQuant != "") {

            if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].poSrNO !== values[i].poSrNO || this.dynamicArrayTrial[i].itemOrPartCode !== values[i].itemOrPartCode || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
              this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].hsnNo !== values[i].hsnNo || this.dynamicArrayTrial[i].range !== values[i].range || this.dynamicArrayTrial[i].quantity !== values[i].quantity || this.dynamicArrayTrial[i].editQuant !== values[i].editQuant
              || this.dynamicArrayTrial[i].unitPrice !== values[i].unitPrice ) //|| this.dynamicArrayTrial[i].amount !== values[i].amount
            ) {

              if(this.documentType!="UCSPL INVOICE BY ID" || this.docT!="UCSPL INVOICE BY ID"){
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
            //  if(typeof i!='undefined'|| i!=null)
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";//uncomment when problem arise

              }
            }
          }

        }
      }
    })




    //code when field value of manage document change and color of button
    // this.manDocForColor.archieveDate !== values.archieveDate ||

    this.manageDocumentForm.valueChanges.subscribe(values => {
      console.log(values);

      if (typeof this.manDocForColor != 'undefined' && this.manDocForColor != null) {


        if ((this.manDocForColor.documentType !== values.documentType ||
          this.manDocForColor.invoiceType !== values.invoiceTypeOfDoc || this.manDocForColor.remark !== values.remark
          || this.manDocForColor.createdBy !== values.createdBy || this.manDocForColor.status !== values.status)
        ) {
          // console.log("change" + change);
          // var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
          // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
        }
        //   else if ((this.manDocForColor.documentType == values.documentType  || 
        //     this.manDocForColor.invoiceType == values.invoiceTypeOfDoc || this.manDocForColor.remark == values.remark
        //    || this.manDocForColor.createdBy == values.createdBy || this.manDocForColor.status == values.status )
        //  ) {
        //     document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";
        //   }
        //   else{

        //   }
      }
    })

  }// end of ngoninit

  dataForGroupingInvoice() {
    this.onlyRequestAndInwardNumberArray.splice(0, this.onlyRequestAndInwardNumberArray.length);
    this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForCustId(this.customerIdForcustBilledTo).subscribe(data => {
      this.requestNumbersArray = data;
      console.log("requestNumbersArray" + this.requestNumbersArray);
      for (var i = 0; i < this.requestNumbersArray.length; i++) {
        this.onlyRequestAndInwardNumberArray.push(this.requestNumbersArray[i][2]);
      }

      //code for unique request numbers
      var filter = function (value, index) {
        return this.indexOf(value) == index
      };
      this.uniqueRequestNumbersAvailable = this.onlyRequestAndInwardNumberArray.filter(filter, this.onlyRequestAndInwardNumberArray);
      console.log(this.uniqueRequestNumbersAvailable);


      for (var i = 0; i < this.uniqueRequestNumbersAvailable.length; i++) {
        this.dropdownList1.push({
          itemId: i + 1, itemText: this.uniqueRequestNumbersAvailable[i]
        })

      }
      this.dropdownList = this.dropdownList1;

      // // this.requestAndInwardDataArray=JSON.parse(localStorage.getItem('RequestAndInwardArrayForInvoice'))
      // this.requestNumberCollectionArrayForInvoice = JSON.parse(localStorage.getItem('requestNumbersCollectionArray'));

      // for (var j = 0; j < this.dropdownList.length; j++) {
      //   for (var i = 0; i < this.requestNumberCollectionArrayForInvoice.length; i++) {
      //     if (this.requestNumberCollectionArrayForInvoice[i].itemId == this.dropdownList[j].itemId)
      //       this.selectedItems.push({ itemId: this.dropdownList[j].itemId, itemText: this.dropdownList[j].itemText });


      //   }
      // }
      // this.selectedRequestNumberFromCreateInvoiceScreen = this.selectedItems;
      // for (var i = 0; i < this.selectedRequestNumberFromCreateInvoiceScreen.length; i++) {
      //   this.requestNumbersCollectionArray.push(this.selectedRequestNumberFromCreateInvoiceScreen[i]);

      // }
      this.getSrNoData();
    })


  }

  onItemSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    console.log(item);
    // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.requestNumbersCollectionArray.push(item);
    localStorage.setItem('requestNumbersCollectionArray', JSON.stringify(this.requestNumbersCollectionArray));


    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.push({ invoiceDocNo: this.originalDocNo, reqDocNo: item.itemText });
    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1 = JSON.stringify(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo);


    // this.createInvoiceService.removeAllSrNoForInvDocNumber(this.originalDocNo).subscribe((res) => {
    //   this.removeData = res;
    //   this.docT = this.documentType;


    //   this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data => {

    //     this.invoiceTagSetToZeroForReqNoAndInvNo = data;
    //     alert("set invoice tag to 0")
    //     console.log("set invoice tag to 0")
    //   })



    //  })

     this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      this.removeData = res;
      this.docT = this.documentType;


      this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
        
        

      })
    })
    //this.removeAllData();
    //this.getSrNoData();
  }

  onSelectAll(items: any) {
    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    for (var j = 0; j < items.length; j++) {
      this.requestNumbersCollectionArray.push(items[j]);

    }
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    // this.getSrNoData();
    //this.DocTypeFunForGrouping();

  //  this.removeAllData();
  this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
    this.removeData = res;
    this.docT = this.documentType;


    this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      
      

    })
  })
    localStorage.setItem('requestNumbersCollectionArray', JSON.stringify(this.requestNumbersCollectionArray));
  }

  onDeselectAll(items: any) {
    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.documentNumber).subscribe((res) => {
      this.printTermsId = res;
    })
    var length = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    if (length >= 1) {
      while (length >= 0) {
        length--;
        this.formArr.removeAt(length);

      }
      this.addNewRow();

    }
    this.getAmount1(0);
  //  this.removeAllData();
  this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
    this.removeData = res;
    this.docT = this.documentType;


    this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      
      

    })
  })
    localStorage.setItem('requestNumbersCollectionArray', JSON.stringify(this.requestNumbersCollectionArray));
  }

  onItemDeSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";

    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.splice(0, this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.length);

    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {
      if (item.itemId == this.requestNumbersCollectionArray[i].itemId) {

        // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length); 
        var trial = [this.requestNumbersCollectionArray[i].itemId];
        this.requestNumbersCollectionArray = this.requestNumbersCollectionArray.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.requestNumbersCollectionArray);

      }

    }
    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.push({ invoiceDocNo: this.originalDocNo, reqDocNo: item.itemText });
    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1 = JSON.stringify(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo);


   //this.removeAllData();

    // this.createInvoiceService.removeAllSrNoForInvDocNumber(this.originalDocNo).subscribe((res) => {
    //   this.removeData = res;
    //   this.docT = this.documentType;


    //   this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data => {

    //     this.invoiceTagSetToZeroForReqNoAndInvNo = data;
    //     alert("set invoice tag to 0")
    //     console.log("set invoice tag to 0")
    //   })



    // })

    this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      this.removeData = res;
      this.docT = this.documentType;


      this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
        
        

      })
    })





    // this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data=>{

    //   this.invoiceTagSetToZeroForReqNoAndInvNo=data;
    //   alert("set invoice tag to 0")
    //   console.log("set invoice tag to 0")
    // })
    //  this.getSrNoData();
    localStorage.setItem('requestNumbersCollectionArray', JSON.stringify(this.requestNumbersCollectionArray));




  }



  getSrNoData() {

    this.onlyRequestNumberCollectionArrayForInvoice.splice(0, this.onlyRequestNumberCollectionArrayForInvoice.length);
    this.instruNamesArray.splice(0, this.instruNamesArray.length);


    this.instruNamesArrayForTrial.splice(0, this.instruNamesArrayForTrial.length);

    this.arrayOfInstruNameAndRange.splice(0, this.arrayOfInstruNameAndRange.length);

    this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length);


    this.allrequestNumbersDataForCalibrationTag.splice(0, this.allrequestNumbersDataForCalibrationTag.length);
    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {
      this.onlyRequestNumberCollectionArrayForInvoice.push(this.requestNumbersCollectionArray[i].itemText);
      console.log(this.onlyRequestNumberCollectionArrayForInvoice);
    }

    //  this.trialSrNoDataFunction();


    //trial below code

    this.createInvoiceService.trialgetReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(this.onlyRequestNumberCollectionArrayForInvoice).subscribe(data => {
      this.redDataForCalibrationTag = data;

      for (var r = 0; r < this.redDataForCalibrationTag.length; r++) {
        this.groupByIdInvoiceTagArray.push({ invoiceTag: 0, instruName: this.redDataForCalibrationTag[r][7], reqDocId: this.redDataForCalibrationTag[r][0] });
      }


      this.getDataForInvoice();

      // this.arrayTosetInvoiceTagToOne.splice(0, this.arrayTosetInvoiceTagToOne.length);
      // this.arrayTosetInvoiceTagToOneTrial.splice(0, this.arrayTosetInvoiceTagToOneTrial.length);
      // console.log("redDataForCalibrationTag" + this.redDataForCalibrationTag);
      // for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {

      //   this.allrequestNumbersDataForCalibrationTag.push(this.redDataForCalibrationTag[i]);
      //   console.log("allrequestNumbersDataForCalibrationTag" + this.allrequestNumbersDataForCalibrationTag);
      //   this.instruNamesArray.push(this.redDataForCalibrationTag[i][7]);
      //   var reqDocumentId = this.redDataForCalibrationTag[i][0].toString();
      //   //var reqDocumentId1=reqDocumentId.toString();
      //   this.arrayTosetInvoiceTagToOne.push({ instruName: this.redDataForCalibrationTag[i][7], reqNo: this.redDataForCalibrationTag[i][2], reqDocId: reqDocumentId, quantity: 1, rangeFrom: this.redDataForCalibrationTag[i][14], rangeTo: this.redDataForCalibrationTag[i][15] });
      // }
      // this.trialInvoiceArray = this.arrayTosetInvoiceTagToOne;
      // let key = "instruName";
      // let key1 = "reqNo";
      // let key2 = "reqDocId"

      // for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {
      //   this.arrayOfInstruNameAndRange.push(this.redDataForCalibrationTag[i][7] + "_" + this.redDataForCalibrationTag[i][14] + "_" + this.redDataForCalibrationTag[i][15]);
      //   console.log("arrayOfInstruNameAndRange" + this.arrayOfInstruNameAndRange);
      //   this.onlyRangeFromArray.push(this.redDataForCalibrationTag[i][14]);
      //   this.onlyRangeToArray.push(this.redDataForCalibrationTag[i][15]);
      // }

      // // this.DocTypeFunForGrouping(); //uncomment this

      // if (this.property == 1) {
      //   this.dynamicArrayTrial2.splice(0, this.dynamicArrayTrial2.length);
      //   this.dynamicArrayTrial1.splice(0, this.dynamicArrayTrial1.length);
      //   this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
      //   this.newDynamic1.splice(0, this.newDynamic1.length);
      //   // this.getPoData();
      // }

      // this.getPoData();

    })


    //trial above code

    // for (this.ct = 0; this.ct < this.onlyRequestNumberCollectionArrayForInvoice.length; this.ct++) {
    //   this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(this.onlyRequestNumberCollectionArrayForInvoice[this.ct]).subscribe(data => {
    //     this.redDataForCalibrationTag = data;
    //     console.log("redDataForCalibrationTag" + this.redDataForCalibrationTag);
    //     for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {

    //       this.allrequestNumbersDataForCalibrationTag.push(this.redDataForCalibrationTag[i]);
    //       console.log("allrequestNumbersDataForCalibrationTag" + this.allrequestNumbersDataForCalibrationTag);
    //       this.instruNamesArray.push(this.redDataForCalibrationTag[i][7]);
    //     }

    //     for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {
    //       this.arrayOfInstruNameAndRange.push(this.redDataForCalibrationTag[i][7] + "_" + this.redDataForCalibrationTag[i][14] + "_" + this.redDataForCalibrationTag[i][15]);
    //       console.log("arrayOfInstruNameAndRange" + this.arrayOfInstruNameAndRange);
    //     }

    //     // for(var i=0; i<this.allrequestNumbersDataForCalibrationTag.length;i++){
    //     //   this.instruNamesArray.push(this.allrequestNumbersDataForCalibrationTag[i][7]);
    //     //   console.log("instruNamesArray"+this.instruNamesArray);
    //     // }


    //     //     this.createInvoiceService.getReqAndInwDataWithCalibrationTagForReqDocNo(this.requestAndInwardDocumentNumber).subscribe(data => {
    //     //       this.redDataForCalibrationTag = data;


    //     //       this.redDataForCalibrationTag1 = this.redDataForCalibrationTag;
    //     //       console.log("redDataForCalibrationTag1 "+ this.redDataForCalibrationTag1);

    //     // for(var i=0;i<this.redDataForCalibrationTag1.length;i++){
    //     // this.instruNamesArray.push(this.redDataForCalibrationTag1[i][7]);

    //     // }
    //     //     })

    //   })

    // }

    // if (typeof this.redDataForCalibrationTag != 'undefined' && this.redDataForCalibrationTag != 0) {
    //   if (this.ct >= this.redDataForCalibrationTag.length) {
    //     this.getDeliveryChallanInformation();
    //   }
    // }

    // this.createInvoiceService.getInvoiceTypeListForInvoice().subscribe(data => {

    //   this.DocTypeFunForGrouping();
    // })
    //  return this.width;
  }


  trialSrNoDataFunction() {

    // this.getSrNoData().then(()=>this.DocTypeFunForGrouping());
    console.log(this.data);

  }
  //   function func() {
  //     return foo()
  //        .then(() => bar())
  //        .then(() => baz());
  //  }


  editQuantityFun(e) {
    this.instruNamesArrayForEdit.splice(0, this.instruNamesArrayForEdit.length);
    this.uniqueInstrumentsAvailableEdit.splice(0, this.uniqueInstrumentsAvailableEdit.length);
    this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(e).subscribe(data => {
      this.redDataForCalibrationTag = data;
      for (var i = 0; i < this.redDataForCalibrationTag.length - 1; i++) {

        //       this.allrequestNumbersDataForCalibrationTag.push(this.redDataForCalibrationTag[i]);
        //       console.log("allrequestNumbersDataForCalibrationTag" + this.allrequestNumbersDataForCalibrationTag);
        this.instruNamesArrayForEdit.push(this.redDataForCalibrationTag[i][7]);

      }
      this.countForInstruName1Edit = {};
      this.countForInstruNameEdit = {};
      //unique insrument count
      var count = {};
      this.instruNamesArrayForEdit.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
      console.log(count);
      this.countForInstruNameEdit = count;
      const mapped = Object.entries(this.countForInstruNameEdit).map(([type, value]) => ({ type, value }));
      console.log(mapped);
      this.countForInstruName1Edit = mapped;

      //unique instrument name
      var filter = function (value, index) {
        return this.indexOf(value) == index
      };
      this.uniqueInstrumentsAvailableEdit = this.instruNamesArrayForEdit.filter(filter, this.instruNamesArrayForEdit);
      console.log(this.uniqueInstrumentsAvailableEdit);
    })


  }

  // updateData() {
  //   if (this.documentType == "UCSPL INVOICE BY ID") {
  //     this.property = 1;
  //   }

  //   this.getSrNoData();                // comment this 
  //   this.onSubmit();
  // }


  updateData1(){

    if(this.documentType=='UCSPL INVOICE BY ID'){
   //  this.checkifidselected=1;
    }

    if(this.documentType!='UCSPL INVOICE BY ID'){
      this.updateData()
    }
  }

  updateData() {
    if (this.docT != null && typeof this.docT != 'undefined') {
      //  if (this.docT != this.documentType) {
      if (this.documentType == "UCSPL INVOICE BY ID") {
        this.property = 1;
        this.redDataForCalibrationTag.splice(0, this.redDataForCalibrationTag.length)
        this.instruNamesArrayForTrial.splice(0, this.instruNamesArrayForTrial.length)
        this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length)
        this.instruNamesArray.splice(0, this.instruNamesArray.length);
        this.instruNamesArrayForTrial.splice(0, this.instruNamesArrayForTrial.length);
        this.arrayOfInstruNameAndRange.splice(0, this.arrayOfInstruNameAndRange.length);
        this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length);
        this.groupByIdInvoiceTagArray.splice(0, this.groupByIdInvoiceTagArray.length);
        this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
        this.newDynamic1.splice(0, this.newDynamic1.length);


        this.getSrNoData();
        // this.getDataForInvoice();
      //  this.docT = this.documentType;
      }
      else {
        this.getSrNoData();
       // this.docT = this.documentType;
      }
      // }
    }
    // window.location.reload();
    // comment this 
    this.onSubmit();

  }


  getDataForInvoice() {
    this.createInvoiceService.sendDocNoForGettingInvoiceData(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
      this.challanCumInvoiceArrayForSearchInvoice = data;

      // for(var r= 0; r<this.challanCumInvoiceArrayForSearchInvoice.length;r++){
      //   this.groupByIdInvoiceTagArray.push({invoiceTag:1,instruName:this.challanCumInvoiceArrayForSearchInvoice[r][7],reqDocId:this.challanCumInvoiceArrayForSearchInvoice[r][0]});
      // }


      if (typeof this.challanCumInvoiceArrayForSearchInvoice != 'undefined' && this.challanCumInvoiceArrayForSearchInvoice != null) {
        if (this.challanCumInvoiceArrayForSearchInvoice.length != 0) {
          for (var z = 0; z < this.challanCumInvoiceArrayForSearchInvoice.length; z++) {
            this.redDataForCalibrationTag.push(this.challanCumInvoiceArrayForSearchInvoice[z]);
            this.instruNamesArrayForTrial.push(this.challanCumInvoiceArrayForSearchInvoice[z][7]);
            this.arrayOfInstruNameAndRangeTrial.push(this.challanCumInvoiceArrayForSearchInvoice[z][7] + "_" + this.challanCumInvoiceArrayForSearchInvoice[z][14] + "_" + this.challanCumInvoiceArrayForSearchInvoice[z][15]);
          }
        }
      }

      this.arrayTosetInvoiceTagToOne.splice(0, this.arrayTosetInvoiceTagToOne.length);
      this.arrayTosetInvoiceTagToOneTrial.splice(0, this.arrayTosetInvoiceTagToOneTrial.length);
      console.log("redDataForCalibrationTag" + this.redDataForCalibrationTag);
      for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {

        this.allrequestNumbersDataForCalibrationTag.push(this.redDataForCalibrationTag[i]);
        console.log("allrequestNumbersDataForCalibrationTag" + this.allrequestNumbersDataForCalibrationTag);
        this.instruNamesArray.push(this.redDataForCalibrationTag[i][7]);
        var reqDocumentId = this.redDataForCalibrationTag[i][0].toString();
        this.reqDocId = this.redDataForCalibrationTag[i][0];
        //var reqDocumentId1=reqDocumentId.toString();
        this.arrayTosetInvoiceTagToOne.push({ instruName: this.redDataForCalibrationTag[i][7], reqNo: this.redDataForCalibrationTag[i][2], reqDocId: reqDocumentId, quantity: 1, rangeFrom: this.redDataForCalibrationTag[i][14], rangeTo: this.redDataForCalibrationTag[i][15], invUniqueNo: this.redDataForCalibrationTag[i][33] });


      }
      this.trialInvoiceArray = this.arrayTosetInvoiceTagToOne;
      let key = "instruName";
      let key1 = "reqNo";
      let key2 = "reqDocId"

      //  this.trialInvoiceArray=this.findOcc(this.arrayTosetInvoiceTagToOne,key,key1,key2);


      for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {
        this.arrayOfInstruNameAndRange.push(this.redDataForCalibrationTag[i][7] + "_" + this.redDataForCalibrationTag[i][14] + "_" + this.redDataForCalibrationTag[i][15]);
        console.log("arrayOfInstruNameAndRange" + this.arrayOfInstruNameAndRange);
        this.onlyRangeFromArray.push(this.redDataForCalibrationTag[i][14]);
        this.onlyRangeToArray.push(this.redDataForCalibrationTag[i][15]);
      }



      if (this.property == 1) {
        this.dynamicArrayTrial2.splice(0, this.dynamicArrayTrial2.length);
        this.dynamicArrayTrial1.splice(0, this.dynamicArrayTrial1.length);
        this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
        this.newDynamic1.splice(0, this.newDynamic1.length);

      }

      this.getPoData();

    })

  }




  //////////////////////////////code for button color change////////////////////////


  get formArr() {
    return this.reqdocParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({

      srNo: [""],
      instruName: [""],
      description: [""],
      itemOrPartCode: [""],
      poSrNO: [""],
      rangeFrom: [""],
      rangeTo: [""],
      sacCode: [""],
      hsnNo: [""],
      quantity: [""],
      editQuant: [""],
      unitPrice: [""],
      amount: [""],
      uniqueNo: []

    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow1(index: number) {

    this.srNoToDelete = index + 1;
    for (var i = 0; i < this.dynamicArrayTrial.length; i++) {
      if (this.dynamicArrayTrial1[i].srNo == this.srNoToDelete) {
        this.indexToDelete = this.dynamicArrayTrial1[i].uniqueId;
        this.createInvoiceService.deleteRowForInvoiceSrNo(this.indexToDelete).subscribe(data => {
          console.log("*********deleted row********" + data);
        })
        break;
      }
    }

    this.dynamicArrayTrial.splice(index, 1);
    this.formArr.removeAt(index);

    this.calculationAfterCustChange()

  }


  // ngAfterViewInit() {
  //   this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
  //   for (var l = 0; l < this.leng; l++) {

  //     if (typeof this.dynamicArrayTrial != 'undefined') {
  //       if (this.dynamicArrayTrial != null) {
  //         if (this.dynamicArrayTrial[l].instruName != "") {
  //           document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
  //         }
  //       }
  //       else {
  //         document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";

  //       }
  //     }
  //   }

  // }

  //////////////////////////////////// Customer change ///////////////////////////////////////

  custNameChange(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedCustNameTrial == this.customers[s]) {

        this.custNameTrialShippedTo = this.customer1[s].name;
        this.shippedCustDepartment = this.customer1[s].department;
        this.shippedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.shippedCustPanNo = this.customer1[s].panNo;
        this.shippedCustGstNo = this.customer1[s].gstNo;
        this.shippedCustAddressline1 = this.customer1[s].addressLine1;
        this.shippedCustAddressline2 = this.customer1[s].addressLine2;
        this.shippedCustCountry = this.customer1[s].country;
        this.shippedCustState = this.customer1[s].state;
        this.shippedCustCity = this.customer1[s].city;
        this.shippedCustPin = this.customer1[s].pin;
        break;
      }
    }

  }


  custNameChange1(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.billedCustNameTrial == this.customers[s]) {
        this.custNameTrialBilledTo = this.customer1[s].name;
        this.billedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.billedCustDepartment = this.customer1[s].department;
        this.billedCustPanNo = this.customer1[s].panNo;
        this.billedCustGstNo = this.customer1[s].gstNo;
        this.billedCustAddressline1 = this.customer1[s].addressLine1;
        this.billedCustAddressline2 = this.customer1[s].addressLine2;
        this.billedCustCountry = this.customer1[s].country;
        this.billedCustState = this.customer1[s].state;
        this.billedCustCity = this.customer1[s].city;
        this.billedCustPin = this.customer1[s].pin;
        break;
      }
    }

  }

  custNameChange2(e) {

    console.log(e);
  }


  /////////////////////////////////////////////Get information from create-deli-chn-component/////////////////////

  getDeliveryChallanInformation() {
    this.customerIdForcustShippedTo = this.createInvoiceService.getCustomerNameForCreatingChallan();
    this.createChallanCumInvoiceObj = this.createInvoiceService.getCreateChallanCumInvoiceDocument();
    this.getInformation();
  }

  getInformation() {



    this.dateOfInvoice = this.createChallanCumInvoiceObj.dateOfInvoice;
    this.documentNumber = this.createChallanCumInvoiceObj.invDocumentNumber;
    // this.requestNumber = this.createChallanCumInvoiceObj.requestNumber;
    this.createdBy = this.createChallanCumInvoiceObj.createdBy;

    this.onlyRequestAndInwardNumberArray.splice(0, this.onlyRequestAndInwardNumberArray.length);
    this.dropdownList.splice(0, this.dropdownList.length);
    this.dropdownList1.splice(0, this.dropdownList1.length);
    if (typeof this.customerIdForcustBilledTo != 'undefined' || this.customerIdForcustBilledTo != null) {
      this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForCustId(this.customerIdForcustBilledTo).subscribe(data => {
        this.requestNumbersArray = data;
        console.log("requestNumbersArray" + this.requestNumbersArray);
        for (var i = 0; i < this.requestNumbersArray.length; i++) {
          this.onlyRequestAndInwardNumberArray.push(this.requestNumbersArray[i][2]);
        }

        //code for unique request numbers
        var filter = function (value, index) {
          return this.indexOf(value) == index
        };

        this.uniqueRequestNumbersAvailable = this.onlyRequestAndInwardNumberArray.filter(filter, this.onlyRequestAndInwardNumberArray);
        console.log(this.uniqueRequestNumbersAvailable);



        if (typeof this.documentNumber != 'undefined' || this.documentNumber != null) {
          this.createInvoiceService.getAllRequestNumbersForInvoiceDocNumber(this.documentNumber).subscribe(data => {
            console.log("*********sr no for doc no********" + data);
            this.AllRequestNumbers = data;





            for (this.jj = 0; this.jj < this.AllRequestNumbers.length; this.jj++) {
              this.selectedItems.push({ itemId: this.jj + 1, itemText: this.AllRequestNumbers[this.jj] });
              // this.dropdownList1.push({ itemId: this.jj+1, itemText: this.AllRequestNumbers[this.jj] });
              this.allrequestNumbersUnique.push(this.AllRequestNumbers[this.jj]);
            }

            for (var i = 0; i < this.uniqueRequestNumbersAvailable.length; i++) {
              // this.dropdownList1.push({
              //   itemId: this.jj + i+1, itemText: this.uniqueRequestNumbersAvailable[i]

              // })
              this.allrequestNumbersUnique.push(this.uniqueRequestNumbersAvailable[i]);

            }

            this.allrequestNumbersUnique1 = this.allrequestNumbersUnique.filter(filter, this.allrequestNumbersUnique);
            console.log(this.allrequestNumbersUnique1);
            //  this.dropdownList = this.dropdownList1;

            for (var k = 0; k < this.allrequestNumbersUnique1.length; k++) {

              this.dropdownList1.push({
                itemId: k + 1, itemText: this.allrequestNumbersUnique1[k]

              })
            }
            this.dropdownList = this.dropdownList1;

            this.selectedRequestNumberFromCreateInvoiceScreen = this.selectedItems;
            for (var i = 0; i < this.selectedRequestNumberFromCreateInvoiceScreen.length; i++) {
              this.requestNumbersCollectionArray.push(this.selectedRequestNumberFromCreateInvoiceScreen[i]);

            }
            for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
              if (this.documentTypeObjectData[i].documentTypeName == this.createChallanCumInvoiceObj.documentType) {


                this.documentType = this.documentTypeObjectData[i].documentTypeName;

                this.docT = this.documentType;

              }
            }

            this.getSrNoData();       ///comment if problem arise ////////////////////////////////////////
            // if(this.documentType=="UCSPL INVOICE BY ID"){

            // }

          })

        }

      })

    }




    for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {

      if (this.invoiceTypeObjectData[i].invoiceTypeName == this.createChallanCumInvoiceObj.invoiceType) {
        this.invoiceTypeOfDoc = this.invoiceTypeObjectData[i].invoiceTypeName;
      }
    }
    if (typeof this.invoiceTypeOfDoc != 'undefined') {
      if (this.invoiceTypeOfDoc == "Calibration Charges" || this.invoiceTypeOfDoc == "Calibration & Repair Charges" || this.invoiceTypeOfDoc == "Repair Charges") {
        this.hideHsn = 1;
      }
      else {
        this.hideHsn = 0;
      }
    }
    // for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
    //   if (this.documentTypeObjectData[i].documentTypeId == this.createChallanCumInvoiceObj.documentTypeId) {


    //     this.documentType = this.documentTypeObjectData[i].documentTypeName;
    //     this.docT=this.documentType;

    //   }
    // }

    for (var i = 0; i < Object.keys(this.customersIds).length; i++) {
      if (this.customerIdForcustShippedTo == this.customersIds[i]) {


        this.custNameTrialShippedTo = this.customer1[i].name;
        this.shippedCustDepartment = this.customer1[i].department;
        this.shippedNameAndDept = this.customer1[i].name + " / " + this.customer1[i].department;
        this.shippedCustNameTrial = this.customer1[i].name;
        this.shippedCustPanNo = this.customer1[i].panNo;
        this.shippedCustGstNo = this.customer1[i].gstNo;
        this.shippedCustAddressline1 = this.customer1[i].addressLine1;
        this.shippedCustAddressline2 = this.customer1[i].addressLine2;
        this.shippedCustCountry = this.customer1[i].country;
        this.shippedCustState = this.customer1[i].state;
        this.shippedCustCity = this.customer1[i].city;
        this.shippedCustPin = this.customer1[i].pin;
        break;
      }

    }
    for (var i = 0; i < Object.keys(this.customersIds).length; i++) {
      if (this.customerIdForcustBilledTo == this.customersIds[i]) {
        this.custNameTrialBilledTo = this.customer1[i].name;
        this.billedCustDepartment = this.customer1[i].department;
        this.billedNameAndDept = this.customer1[i].name + " / " + this.customer1[i].department;
        this.billedCustNameTrial = this.customer1[i].name;
        this.billedCustPanNo = this.customer1[i].panNo;
        this.billedCustGstNo = this.customer1[i].gstNo;
        this.billedCustAddressline1 = this.customer1[i].addressLine1;
        this.billedCustAddressline2 = this.customer1[i].addressLine2;
        this.billedCustCountry = this.customer1[i].country;
        this.billedCustState = this.customer1[i].state;
        this.billedCustCity = this.customer1[i].city;
        this.billedCustPin = this.customer1[i].pin;

        break;
      }
    }

    this.splitGstAndQuotationNumber();
    this.docT = this.documentType;

  }
  getPoData() {
    this.createInvoiceService.getPoDataForPoNo(this.poNo).subscribe(data => {
      this.poDataForInvoice = data;

      if (this.poDataForInvoice.length != 0) {
        for (var z = 0; z < this.poDataForInvoice.length; z++) {
          if (typeof this.redDataForCalibrationTag != 'undefined' && this.redDataForCalibrationTag != null) {
            for (var y = 0; y < this.redDataForCalibrationTag.length; y++) {
              if (this.poDataForInvoice[z][14] == this.redDataForCalibrationTag[y][7]) {
                this.newDynamic1.push({
                  uniqueId: this.poDataForInvoice[z][0],
                  poSrNO: this.poDataForInvoice[z][2],
                  instruName: this.poDataForInvoice[z][14],
                  description: this.poDataForInvoice[z][4],
                  idNo: this.poDataForInvoice[z][5],
                  // range: this.redDataForCalibrationTag1[y][14],
                  rangeFrom: this.redDataForCalibrationTag[y][14],
                  rangeTo: this.redDataForCalibrationTag[y][15],
                  quantity: this.poDataForInvoice[z][7],
                  unitPrice: this.poDataForInvoice[z][8],
                  discountOnItem: this.poDataForInvoice[z][9],
                  amount: this.poDataForInvoice[z][11],
                  itemOrPartCode: this.poDataForInvoice[z][12],
                  hsnNo: this.poDataForInvoice[z][13],
                  fullReqNo: this.redDataForCalibrationTag[y][2],
                  inwReqNo: this.redDataForCalibrationTag[y][3],
                  reqDocId: this.redDataForCalibrationTag[y][0],
                  invoiceTag: this.redDataForCalibrationTag[y][34],
                })
              }
            }
          }
        }
        this.dynamicArray.splice(0, this.dynamicArray.length);
        this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
        // for (var l = 0; l < this.newDynamic1.length; l++) {            //first it was for (var l = 1; l < this.newDynamic1.length; l++) { 
        //   //this.convertInsruIdtoName(l);
        //   this.dynamicArray.push(this.newDynamic1[l]);
        //   this.dynamicArrayTrial.push(this.newDynamic1[l]);
        //   this.dynamicArrayTrial1.push(this.newDynamic1[l]);
        // }

        if (this.documentType == "UCSPL INVOICE BY ID") {
          for (var l = 0; l < this.newDynamic1.length; l++) {// l=0 when problem arises
            //this.convertInsruIdtoName(l);
            //   this.dynamicArray.push(this.newDynamic1[l]); uncomment when problem arise
            this.dynamicArrayTrial.push(this.newDynamic1[l]);

            // this.dynamicArrayTrial2= this.dynamicArrayTrial1;
          }
          this.showMessage = 1;
        }
        else {
          for (var l = 1; l < this.newDynamic1.length; l++) {// l=0 when problem arises
            //this.convertInsruIdtoName(l);
            this.dynamicArray.push(this.newDynamic1[l]);
            this.dynamicArrayTrial.push(this.newDynamic1[l]);

            // this.dynamicArrayTrial2= this.dynamicArrayTrial1;
          }

        }

        this.dynamicArrayTrial1 = $.extend(true, [], this.dynamicArrayTrial);

        this.DocTypeFunForGrouping();
      }
    })
  }


  convertInsruIdtoName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;

      }
    }
  }


  DocTypeFunForGrouping() {

    switch (this.documentType) {
      case "UCSPL GROUPED BY NAME":
        // this.grouping();
        this.groupByName();
        break;
      case "UCSPL GROUPED BY RANGE":
        // this.grouping();
        this.groupByRange();
        break;
      case "UCSPL TAX INVOICE":
        //this.normalInvoice();
        break;
      case "UCSPL INVOICE BY ID":
        this.normalInvoiceById();
        break;
    }

  }







  grouping() {

    this.instruNamesArray.splice(0, this.instruNamesArray.length);
    this.instruNameWithRangeArray.splice(0, this.instruNameWithRangeArray.length);
    this.instruNameWithRangeArray1.splice(0, this.instruNameWithRangeArray1.length);

    for (var ii = 0; ii < this.dynamicArrayTrial1.length; ii++) {


      this.instruNamesArray.push(this.dynamicArrayTrial1[ii].instruName)
      var variable = this.dynamicArrayTrial1[ii].instruName + "_" + this.dynamicArrayTrial1[ii].rangeFrom + "_" + this.dynamicArrayTrial1[ii].rangeTo;
      this.instruNameWithRangeArray.push(variable);
      this.instruNameWithRangeArray1.push(variable);

    }

  }







  groupByName() {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    this.trialremainingQuantityCheck.splice(0,this.trialremainingQuantityCheck.length);
    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


      this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.challanCumInvoiceDocumentVariableArrayForUniqueTrial = data;



        this.hideRange = 1;
        this.countForInstruName1 = {};
        this.countForInstruName = {};
        this.countForInstruName2 = {};
        this.countForInstruName3 = {};


        //unique insrument count
        var count = {};
        this.instruNamesArray.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
        console.log(count);
        this.countForInstruName = count;
        const mapped = Object.entries(this.countForInstruName).map(([type, value]) => ({ type, value }));
        console.log(mapped);
        this.countForInstruName1 = mapped;

        //unique insrument count for edit
        var count = {};
        this.instruNamesArrayForTrial.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
        console.log(count);
        this.countForInstruName2 = count;
        const mapped1 = Object.entries(this.countForInstruName2).map(([type, value]) => ({ type, value }));
        console.log(mapped1);
        var size = Object.keys(mapped1).length;
        this.countForInstruName3 = mapped1;

        //unique instrument name
        var filter = function (value, index) {
          return this.indexOf(value) == index
        };
        this.uniqueInstrumentsAvailable = this.instruNamesArray.filter(filter, this.instruNamesArray);
        console.log(this.uniqueInstrumentsAvailable);


        this.trialArray = Array.from(new Set(this.instruNamesArray));
        console.log("trialArray" + this.trialArray);


/******************************code for unique instru names from  po ***************************************************************/
for(var z=0;z<this.poDataForInvoice.length;z++){
  this.instrumentFromPo.push(this.poDataForInvoice[z][14])
}

//unique instrument 
var filter1 = function (value1, index1) {
  return this.indexOf(value1) == index1
};
this.uniqueInstrumentFromPo = this.instrumentFromPo.filter(filter1, this.instrumentFromPo);
console.log(this.uniqueInstrumentFromPo);


 //unique insrument with occuerences for po
 var count2 = {};
 this.instrumentFromPo.forEach(function (i) { count2[i] = (count2[i] || 0) + 1; });
 console.log(count2);
 this.countForInstruNamePo = count2;
 const mapped2 = Object.entries(this.countForInstruNamePo).map(([type, value]) => ({ type, value }));
 console.log(mapped2);
 this.countForInstruNamePo1 = mapped2;


/*******************************************************************************************************************************/



        var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

        if (this.uniqueInstrumentsAvailable.length != 0) {
          if (leng1 == 0) {
            this.addNewRow();
          }

          if (leng1 >= 1) {
            while (leng1 >= 1) {

              leng1--;
              this.formArr.removeAt(leng1);

            }
          }

          for(var h=0;h< this.uniqueInstrumentsAvailable.length; h++){

            this.trialremainingQuantityCheck.push("0");
          }
          if (this.uniqueInstrumentsAvailable.length != 0) {

            this.addNewRow();
            for (this.mi = 0; this.mi < this.uniqueInstrumentsAvailable.length; this.mi++) {

              if (typeof ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup) != 'undefined') {

                var value = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').value;
                console.log("value is" + value);
                if (((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').value == null || ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').value == "") {
                  for (this.mii = 0; this.mii < this.uniqueInstrumentFromPo.length; this.mii++) {
                    if (this.uniqueInstrumentFromPo[this.mii] == this.uniqueInstrumentsAvailable[this.mi]) {

                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('poSrNO').patchValue(this.poDataForInvoice[this.mii][2]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('itemOrPartCode').patchValue(this.poDataForInvoice[this.mii][12]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').patchValue(this.uniqueInstrumentsAvailable[this.mi]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('description').patchValue(this.poDataForInvoice[this.mii][4]);
                      // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('range').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][9]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('sacCode').patchValue("00998346");
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('hsnNo').patchValue(this.poDataForInvoice[this.mii][13]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('quantity').patchValue(this.countForInstruName1[this.mi].value);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(0);
                      // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('unitPrice').patchValue(this.poDataForInvoice[this.mii][8]);
                      // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('amount').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][14]);


                      if (this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != null && typeof this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != undefined) {
                        for (var iii = 0; iii < this.challanCumInvoiceDocumentVariableArrayForUniqueTrial.length; iii++) {
                          if (this.poDataForInvoice[this.mii][15] == this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][6]) {
                            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][15]);
                          }
                        }

                      }



                      if (this.instruNamesArrayForTrial.length == 0) {
                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(0);
                        this.trialremainingQuantityCheck[this.mi]="0";
                      } else {
                        for (var s = 0; s < size; s++) {
                          if (this.countForInstruName3[s].type == this.uniqueInstrumentsAvailable[this.mi]) {

                    
                         ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(this.countForInstruName3[s].value);
                         
                         
                         if(this.countForInstruName3[s].value==0){
                          this.trialremainingQuantityCheck[this.mi]="0";
                            }
                            else{

                              this.trialremainingQuantityCheck[this.mi]=this.countForInstruName3[s].value;
                            }

                            
                          }
                        }
                      }

                      //display rate with either input or dropdown
                      // if(this.countForInstruNamePo1[this.mii].value==1){
                      //   this.displayRate=1;
                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('unitPrice').patchValue(this.poDataForInvoice[this.mii][8]);
                        
                      // }
                      // else{
                      //   this.displayRate=0;
                      //  // this.rateArray.push(this.poDataForInvoice[this.mii][8]);
                      //  this.onChange(this.uniqueInstrumentsAvailable[this.mi]);
                      // }


                      this.getAmount1(this.mi);
                      if (this.mi < this.uniqueInstrumentsAvailable.length - 1)
                        this.addNewRow();
                    }
                  }
                }
              }
            }
            this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;
          }
        }
      })
    })
  }


  onChange(instruName) {
    // this.rateArray.splice(0,this.rateArray.length);
     var rateArray = new Array();
 
     this.instrumentNameForPo =instruName;
     for (var i = 0, l = Object.keys(this.poDataForInvoice).length; i < l; i++) {
       if (this.poDataForInvoice[i][14] == this.instrumentNameForPo) {
         rateArray.push(this.poDataForInvoice[i][8]);
 
        
 
       }
     }
     this.rateArray1[this.mi]= rateArray;
 
   }

  groupByRange() {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


      this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.challanCumInvoiceDocumentVariableArrayForUniqueTrial = data;


        this.hideRange = 0;
        this.instruNameWithRangeArray.splice(0, this.instruNameWithRangeArray.length);
        this.instruNameWithRangeArray1.splice(0, this.instruNameWithRangeArray1.length);
        this.instruNameWithRangeArray1Edit.splice(0, this.instruNameWithRangeArray1Edit.length);
        this.instruNameWithRangeArrayEdit.splice(0, this.instruNameWithRangeArrayEdit.length);

        this.rangeFromArray.splice(0, this.rangeFromArray.length);
        this.rangeToArray.splice(0, this.rangeToArray.length);
        this.countForInstruNameAndRange1.splice(0, this.countForInstruNameAndRange1.length);
        this.countForInstruNameAndRange = {};
        this.countForInstruNameAndRange2 = {};
        this.countForInstruNameAndRange3.splice(0, this.countForInstruNameAndRange3.length);

        //unique insrument and range count
        var count = {};
        this.arrayOfInstruNameAndRange.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
        console.log(count);
        this.countForInstruNameAndRange = count;
        const mapped = Object.entries(this.countForInstruNameAndRange).map(([type, value]) => ({ type, value }));
        console.log(mapped);
        this.countForInstruNameAndRange1 = mapped;
        console.log("this.countForInstruNameAndRange1" + this.countForInstruNameAndRange1);

        //unique insrument and range count edit
        var count = {};
        this.arrayOfInstruNameAndRangeTrial.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
        console.log(count);
        this.countForInstruNameAndRange2 = count;
        const mapped1 = Object.entries(this.countForInstruNameAndRange2).map(([type, value]) => ({ type, value }));
        console.log(mapped1);
        var size = Object.keys(mapped1).length;
        this.countForInstruNameAndRange3 = mapped1;
        console.log("this.countForInstruNameAndRange3" + this.countForInstruNameAndRange3);

        for (var i = 0; i < this.countForInstruNameAndRange1.length; i++) {
          this.instruNameWithRangeArray.push(this.countForInstruNameAndRange1[i].type);
        }

        for (var i = 0; i < this.instruNameWithRangeArray.length; i++) {
          let value = this.instruNameWithRangeArray[i];
          let valueRange = this.instruNameWithRangeArray[i];
          let valueRange1 = valueRange.lastIndexOf('_');
          let value1 = value.lastIndexOf('_');
          let value2 = value.slice(0, value1);
          let value22 = value2;
          let rangeToValue = valueRange.slice(valueRange1 + 1, valueRange.length);
          console.log(rangeToValue);
          this.rangeToArray.push(rangeToValue);
          console.log("rangeToArray" + this.rangeToArray);

          let value3 = value2.lastIndexOf('_');
          let value4 = value.slice(0, value3);

          let valueRange2 = value22;
          let valueRange3 = valueRange2.lastIndexOf('_');

          let rangeFromValue = valueRange2.slice(valueRange3 + 1, valueRange2.length);
          console.log(rangeFromValue);
          this.rangeFromArray.push(rangeFromValue);
          console.log("rangeFromArray" + this.rangeFromArray);

          console.log(value4);

          this.instruNameWithRangeArray1.push(value4);


        }


        //code for edit
        for (var i = 0; i < this.countForInstruNameAndRange3.length; i++) {
          this.instruNameWithRangeArrayEdit.push(this.countForInstruNameAndRange3[i].type);
        }

        for (var i = 0; i < this.instruNameWithRangeArrayEdit.length; i++) {
          let valueE = this.instruNameWithRangeArrayEdit[i];
          let valueRangeE = this.instruNameWithRangeArrayEdit[i];
          let valueRange1E = valueRangeE.lastIndexOf('_');
          let value1E = valueE.lastIndexOf('_');
          let value2E = valueE.slice(0, value1E);
          let value22E = value2E;
          let rangeToValueE = valueRangeE.slice(valueRange1E + 1, valueRangeE.length);
          console.log(rangeToValueE);
          this.rangeToArrayEdit.push(rangeToValueE);
          console.log("rangeToArrayEdit" + this.rangeToArrayEdit);

          let value3E = value2E.lastIndexOf('_');
          let value4E = valueE.slice(0, value3E);

          let valueRange2E = value22E;
          let valueRange3E = valueRange2E.lastIndexOf('_');

          let rangeFromValueEdit = valueRange2E.slice(valueRange3E + 1, valueRange2E.length);
          console.log(rangeFromValueEdit);
          this.rangeFromArrayEdit.push(rangeFromValueEdit);
          console.log("rangeFromArrayEdit" + this.rangeFromArrayEdit);

          console.log(value4E);

          this.instruNameWithRangeArray1Edit.push(value4E);

        }


        if (this.instruNameWithRangeArray1.length != 0) {

          var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

          if (leng1 == 0) {
            this.addNewRow();
          }
          if (leng1 >= 1) {
            while (leng1 >= 1) {

              leng1--;
              this.formArr.removeAt(leng1);

            }
          }

          for(var h=0;h< this.instruNameWithRangeArray1.length; h++){

            this.trialremainingQuantityCheckForRange.push("0");
          }
      
          try{
          if (this.instruNameWithRangeArray1.length != 0) {

            this.addNewRow();
            for(this.mm=0; this.mm< this.instruNameWithRangeArray1.length-1;){

              if (typeof ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup) != 'undefined') {
                for (this.mi = 0; this.mi < this.instruNameWithRangeArray1.length; this.mi++) {
                var value5 = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').value;
                console.log("value is" + value5);
                if (((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').value == null || ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').value == "") {
                  for (this.mii = 0; this.mii < this.poDataForInvoice.length; this.mii++) {
                    if (this.poDataForInvoice[this.mii][14] == this.instruNameWithRangeArray1[this.mi]
                      && this.poDataForInvoice[this.mii][18]== this.rangeFromArray[this.mi]
                      && this.poDataForInvoice[this.mii][19]== this.rangeToArray[this.mi]) {

                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('poSrNO').patchValue(this.poDataForInvoice[this.mii][2]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('itemOrPartCode').patchValue(this.poDataForInvoice[this.mii][12]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').patchValue(this.instruNameWithRangeArray1[this.mi]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('description').patchValue(this.poDataForInvoice[this.mii][4]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('rangeFrom').patchValue(this.rangeFromArray[this.mi]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('rangeTo').patchValue(this.rangeToArray[this.mi]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('sacCode').patchValue("00998346");
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('hsnNo').patchValue(this.poDataForInvoice[this.mii][13]);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('quantity').patchValue(this.countForInstruNameAndRange1[this.mi].value);
                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('editQuant').patchValue(0);

                      if (this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != null && typeof this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != undefined) {
                        for (var iii = 0; iii < this.challanCumInvoiceDocumentVariableArrayForUniqueTrial.length; iii++) {
                          if (this.poDataForInvoice[this.mii][15] == this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][6]) {
                            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][15]);
                          }
                        }

                      }


                      if (this.arrayOfInstruNameAndRangeTrial.length == 0) {
                         this.trialremainingQuantityCheckForRange[this.mm]="0"
                      }
                      else {
                        for (var s = 0; s < size; s++) {
                          if (this.instruNameWithRangeArrayEdit[s] == this.instruNameWithRangeArray[this.mi]) {

                            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('editQuant').patchValue(this.countForInstruNameAndRange3[s].value);

                            if(this.countForInstruNameAndRange3[s].value==0){
                              this.trialremainingQuantityCheckForRange[this.mm]="0";
                                }
                                else{
    
                                  this.trialremainingQuantityCheckForRange[this.mm]=this.countForInstruNameAndRange3[s].value;
                                }
                          }
                        }
                      }



                      ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('unitPrice').patchValue(this.poDataForInvoice[this.mii][8]);
                      // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('amount').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][14]);
                      this.getAmount1(this.mi);
                      if (this.mi < this.instruNameWithRangeArray1.length - 1)
                        this.addNewRow();
                        this.mm++;
                    }
                  }
                }
                //lets try
            if(this.mi ==this.instruNameWithRangeArray1.length-1){
              throw 'error';
            }
              }
              }
            }
            this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;
          }
        }
        catch(error){
          console.log("not match");
        }
        }
       
      }
      )
    }
    )
  }



  normalInvoiceById() {
    this.hideRange = 0;
    this.hideSaveColumn = 1;

    var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

    if (leng1 == 0) {
      this.addNewRow();
    }
    if (leng1 >= 1) {
      while (leng1 >= 1) {

        leng1--;
        this.formArr.removeAt(leng1);

      }
    }

    if (this.dynamicArrayTrial1.length != 0) {


      /////////////////////////////trial below code ////////////////////////////
      this.dynamicArrayTrial2.splice(0, this.dynamicArrayTrial2.length);

      for (this.mi = 0; this.mi < this.dynamicArrayTrial1.length; this.mi++) {

        for (this.mi1 = 0; this.mi1 < this.poDataForInvoice.length; this.mi1++) {

          if (this.dynamicArrayTrial1[this.mi].instruName == this.poDataForInvoice[this.mi1][14] && this.dynamicArrayTrial1[this.mi].invoiceTag == 1) {


            this.dynamicArrayTrial2.push({
              poSrNO: this.poDataForInvoice[this.mi1][2],
              itemOrPartCode: this.poDataForInvoice[this.mi1][12],
              instruName: this.dynamicArrayTrial1[this.mi].instruName,
              description: this.dynamicArrayTrial1[this.mi].description,
              // rangeFrom: this.onlyRangeFromArray[this.mi-1],
              // rangeTo: this.onlyRangeToArray[this.mi-1],
              rangeFrom: this.dynamicArrayTrial1[this.mi].rangeFrom,
              rangeTo: this.dynamicArrayTrial1[this.mi].rangeTo,
              sacCode: "00998346",
              hsnNo: this.poDataForInvoice[this.mi1][13],
              quantity: 1,
              editQuant: 1,
              unitPrice: this.poDataForInvoice[this.mi1][8]
            })




          }
          else if (this.dynamicArrayTrial1[this.mi].instruName == this.poDataForInvoice[this.mi1][14] && this.dynamicArrayTrial1[this.mi].invoiceTag == 0) {


            this.dynamicArrayTrial2.push({
              poSrNO: this.poDataForInvoice[this.mi1][2],
              itemOrPartCode: this.poDataForInvoice[this.mi1][12],
              instruName: this.dynamicArrayTrial1[this.mi].instruName,
              description: this.dynamicArrayTrial1[this.mi].description,
              rangeFrom: this.dynamicArrayTrial1[this.mi].rangeFrom,
              rangeTo: this.dynamicArrayTrial1[this.mi].rangeTo,
              sacCode: "00998346",
              hsnNo: this.poDataForInvoice[this.mi1][13],
              quantity: 1,
              editQuant: 0,
              unitPrice: this.poDataForInvoice[this.mi1][8]
            })





          }

        }
      }

      if (this.dynamicArrayTrial2 == null || typeof this.dynamicArrayTrial2 == 'undefined' || this.dynamicArrayTrial2.length == 0) {
        this.reqdocParaDetailsForm = this.formBuilder.group({
          Rows: this.formBuilder.array([this.initRows()])

        });


      } else {

        this.reqdocParaDetailsForm = this.formBuilder.group({
          Rows: this.formBuilder.array(
            this.dynamicArrayTrial2.map(({

              srNo,
              poSrNO,
              itemOrPartCode,
              instruName,
              description,
              range,
              rangeFrom,
              rangeTo,
              sacCode,
              hsnNo,
              quantity,
              editQuant,
              amount,
              unitPrice

            }) =>
              this.formBuilder.group({
                srNo: [srNo],
                poSrNO: [poSrNO],
                description: [description],
                itemOrPartCode: [itemOrPartCode],
                range: [range],
                rangeFrom: [rangeFrom],
                rangeTo: [rangeTo],
                sacCode: ["00998346"],
                hsnNo: [hsnNo],
                quantity: [quantity],
                editQuant: [editQuant],
                instruName: [instruName],
                amount: [amount],
                unitPrice: [unitPrice],

              })
            )
          )

        })

      }

      for (this.mi = 0; this.mi < this.dynamicArrayTrial2.length; this.mi++) {

        for (this.mi1 = 0; this.mi1 < this.poDataForInvoice.length; this.mi1++) {

          if (this.dynamicArrayTrial2[this.mi].instruName == this.poDataForInvoice[this.mi1][14]) {
            this.getAmount1(this.mi);
          }
        }
      }


      ////////////////////////////trial above code ////////////////////////////

      this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;

    }
  }




  changeDocType() {

    if (this.documentType == "UCSPL INVOICE BY ID") {

      this.hideSaveColumn = 1;
      this.showMessage = 1;
    }
    else {
      this.hideSaveColumn = 0;
      this.showMessage = 0;
    }
  }


  getInformationYourReference() {
    this.dcDate = this.createChallanCumInvoiceObj.dcDate
    this.dcNo = this.createChallanCumInvoiceObj.dcNo
    this.createChallanCumInvoiceObj.branchId = this.createChallanCumInvoiceObj.branchId
    this.dateOfInvoice = this.createChallanCumInvoiceObj.dateOfInvoice;
    this.poDate = this.createChallanCumInvoiceObj.poDate
    this.poNo = this.createChallanCumInvoiceObj.poNo;
    this.checkQuantity()
    this.vendorCodeNo = this.createChallanCumInvoiceObj.vendorCodeNumber;
    //  this.getPoData();           
  }

  splitGstAndQuotationNumber() {
    if (typeof this.shippedCustGstNo != 'undefined') {
      this.stateCode = this.shippedCustGstNo.slice(0, 2);
      this.billedStateCode = this.shippedCustGstNo.slice(0, 2);
    }

    if (typeof this.createChallanCumInvoiceObj.invDocumentNumber != 'undefined') {
      this.originalDocNo = this.createChallanCumInvoiceObj.invDocumentNumber;
      this.invDocNumber = this.createChallanCumInvoiceObj.invDocumentNumber.slice(15, 20);
    }

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchId == this.createChallanCumInvoiceObj.branchId)

        this.prefixInvDocNumber = this.branch1[i].description;
    }

    this.invDocNumberWithPrefix = this.prefixInvDocNumber + this.invDocNumber;

    this.salesDocumentInfo.push(this.stateCode);
    this.salesDocumentInfo.push(this.invDocNumber);

    console.log("salesdocumentinfo..." + this.salesDocumentInfo)
  }

  changeBranchName(e) {
    this.invDocNumberWithPrefix = this.prefixInvDocNumber + this.invDocNumber;

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.prefixInvDocNumber == this.branch1[i].description) {
        this.createChallanCumInvoiceObj.branchId = this.branch1[i].branchId;
        break;
      }

    }
  }



  //////////////////////////////////////////////// code for adding new row //////////////////////////////////


  addRow() {

    this.newDynamic = {
      uniqueId: "",
      srNo: "",
      poSrNO: "",
      itemOrPartCode: "",
      instruName: "",
      description: "",
      range: "",
      sacCode: "",
      quantity: "",
      rate: "",
      discountOnItem: "",
      unitPrice: "",
      amount: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }


  deleteRow(index) {
    if (this.dynamicArray.length == 1) {

      return false;
    } else {
      this.dynamicArray.splice(index, 1);

      return true;
    }
  }


  checkAmount(k) {
    $("#alertEdit1-"+k).html(" ");
    $("#alertEdit-"+k).html(" ");

    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;

   
    if (this.dynamicArray[k].editQuant > this.dynamicArray[k].quantity 
     ) {
     
      this.showTotalQuantMsg=0;
      $("#alertEdit1-"+k).html("Entered Quantity is Greater Than Total Quantity");
      document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#F07470";
      this.trialSetFlag=0;
     
    }
    else {
      $("#alertEdit1-"+k).html(" ");
     this.showTotalQuantMsg=1;
      document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#FFFFFF";
      this.trialSetFlag=1;
      this.getAmount1(k);
    }
  
  //   if(this.documentType=="UCSPL GROUPED BY RANGE"){

  //   for(this.jj=0;this.jj<this.checkQuantityArray.length;this.jj++){
  //     if(this.checkQuantityArray[this.jj].instruName==this.dynamicArray[k].instruName 
  //        && this.checkQuantityArray[this.jj].rangeFromPo==this.dynamicArray[k].rangeFrom
  //        && this.checkQuantityArray[this.jj].rangeToPo==this.dynamicArray[k].rangeTo
  //        && parseInt(this.checkQuantityArray[this.jj].remQuantity)<parseInt(this.dynamicArray[k].editQuant.toString())){
  //         document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#F07470";
  //         this.showPoMessage=0;
  //         $("#alertEdit-"+k).html("Entered Quantity is Greater Than Po Quantity");
  //         this.trialSetFlag=0;
  //         break;
  //        }
  //        else{
          
  //         $("#alertEdit-"+k).html(" ");
  //         this.trialSetFlag=1;
  //        }
         
         
  //   }
  //   if(this.jj>this.checkQuantityArray.length){
  //     document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#FFFFFF";
  //     this.showPoMessage=1;
  //     $("#alertEdit-"+k).html(" ");
  //     this.trialSetFlag=1;
  //     this.getAmount1(k);
  //   }
  // }
    //this.getAmount1(k);
  }
  ////////////////////////// total cgst sgst net ////////////////////////////////////////////////////


  getAmount1(b) {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.dynamicArray[b].quantity = this.dynamicArray[b].editQuant;
    this.subTotal = 0;
    this.taxAmount = 0;

    this.dynamicArray[b].amount = this.dynamicArray[b].quantity * this.dynamicArray[b].unitPrice;
    this.dynamicArray[b].amount = parseFloat(this.dynamicArray[b].amount.toFixed(2));

    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('amount').patchValue(this.dynamicArray[b].amount);

    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discOnSubTotal == 0) {
      this.discOnSubTotalZero();

    }
    else {
      this.discOnSubTotalFunction();
    }

  }

  discOnSubTotalZero() {
    this.total = this.subTotal;
    this.taxAmount = 0;



    if (this.cgst == 1) {
      this.cgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.challanInvoiceDocCalculationObject.cgst = this.cgstAmount;

    }
    if (this.igst == 1) {
      this.igstAmount = this.total * (18 / 100);
      this.taxAmount = this.taxAmount + this.total * (18 / 100);
      this.challanInvoiceDocCalculationObject.igst = this.igstAmount;

    }
    if (this.sgst == 1) {
      this.sgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.challanInvoiceDocCalculationObject.sgst = this.sgstAmount;

    }

    this.netValue = this.total + this.taxAmount;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.taxesTotal = parseFloat(this.taxAmount).toFixed(2);

    this.cgstAmount = parseFloat(this.cgstAmount).toFixed(2);
    this.sgstAmount = parseFloat(this.sgstAmount).toFixed(2);
    this.igstAmount = parseFloat(this.igstAmount).toFixed(2);

    console.log(this.netValue);

    this.gstValueInWords = this.toWords(this.taxesTotal);
    this.netValueInWords = this.toWords(this.netValue);
    this.cgstValueInWords = this.toWords(this.cgstAmount);
    this.igstValueInWords = this.toWords(this.igstAmount);
    this.sgstValueInWords = this.toWords(this.sgstAmount);

  }
  discOnSubTotalFunction() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.total = this.subTotal - (this.subTotal * (this.discOnSubTotal / 100));
    this.total = parseFloat(this.total.toFixed(2));
    if (this.cgst == 1) {
      this.cgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.challanInvoiceDocCalculationObject.cgst = this.cgstAmount;

    }
    if (this.igst == 1) {
      this.igstAmount = this.total * (18 / 100);
      this.taxAmount = this.taxAmount + this.total * (18 / 100);
      this.challanInvoiceDocCalculationObject.igst = this.igstAmount;

    }
    if (this.sgst == 1) {
      this.sgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.challanInvoiceDocCalculationObject.sgst = this.sgstAmount;

    }

    this.netValue = this.total + this.taxAmount;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.taxesTotal = parseFloat(this.taxAmount).toFixed(2);
    this.cgstAmount = parseFloat(this.cgstAmount).toFixed(2);
    this.sgstAmount = parseFloat(this.sgstAmount).toFixed(2);
    this.igstAmount = parseFloat(this.igstAmount).toFixed(2);

    console.log(this.netValue);

    this.gstValueInWords = this.toWords(this.taxesTotal);
    this.netValueInWords = this.toWords(this.netValue);
    this.cgstValueInWords = this.toWords(this.cgstAmount);
    this.igstValueInWords = this.toWords(this.igstAmount);
    this.sgstValueInWords = this.toWords(this.sgstAmount);

  }
  discountOnSubTotal1() {

    if (this.igstAmount == 0) {
      this.netValue = 0;
      this.netValue = this.total + this.cgstAmount + this.sgstAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);

      this.netValueInWords = this.toWords(this.netValue)
    }

  }

  toWords(convert_to_word) {

    var th = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
    var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    this.valueConvertToWord = convert_to_word.toString();
    this.valueConvertToWord = this.valueConvertToWord.replace(/[\, ]/g, '');
    if (this.valueConvertToWord != parseFloat(this.valueConvertToWord)) return 'not a number';
    var x = this.valueConvertToWord.indexOf('.');
    if (x == -1) x = this.valueConvertToWord.length;
    if (x > 15) return 'too big';
    var n = this.valueConvertToWord.split('');
    var str = 'Rupees ';;
    var sk = 0;
    for (var i = 0; i < x; i++) {
      if ((x - i) % 3 == 2) {
        if (n[i] == '1') {
          str += tn[Number(n[i + 1])] + ' ';
          i++;
          sk = 1;
        }
        else if (n[i] != 0) {
          str += tw[n[i] - 2] + ' ';
          sk = 1;
        }
      }
      else if (n[i] != 0) {
        str += dg[n[i]] + ' ';
        if ((x - i) % 3 == 0) str += 'hundred ';
        sk = 1;
      }


      if ((x - i) % 3 == 1) {
        if (sk) str += th[(x - i - 1) / 3] + ' ';
        sk = 0;
      }
    }
    if (x != this.valueConvertToWord.length) {
      var y = this.valueConvertToWord.length;
      str += ' and ';

      for (var j = x + 1; j < y; j++) {
        if ((y - j) % 3 == 2) {
          if (n[j] == '1') {
            str += tn[Number(n[j + 1])] + ' ';
            j++;
            sk = 1;
          }
          else if (n[j] != 0) {
            str += tw[n[j] - 2] + ' ';
            sk = 1;
          }
          else if (n[j] == 0 && n[j + 1] == 0) {
            str += 'Zero ';
            sk = 1;
            break;
          }
        }
        else if (n[j] != 0) {
          str += dg[n[j]] + ' ';
          if ((y - j) % 3 == 0) str += 'hundred ';
          sk = 1;
        }

        if ((y - j) % 3 == 1) {
          if (sk) str += th[(y - j - 1) / 3] + ' ';
          sk = 0;
        }
      }
      str += ' paise only';
    }

    this.valueInWords = str.replace(/\s+/g, ' ');
    return this.valueInWords;
  }

  calculationAfterCustChange() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.subTotal = 0;
    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discOnSubTotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.challanInvoiceDocCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.challanInvoiceDocCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.challanInvoiceDocCalculationObject.sgst = this.sgstAmount;

      }



      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      this.taxesTotal = parseFloat(this.taxAmount).toFixed(2);
      this.cgstAmount = parseFloat(this.cgstAmount).toFixed(2);
      this.sgstAmount = parseFloat(this.sgstAmount).toFixed(2);
      this.igstAmount = parseFloat(this.igstAmount).toFixed(2);



      console.log(this.netValue);



      this.gstValueInWords = this.toWords(this.taxesTotal);
      this.netValueInWords = this.toWords(this.netValue);
      this.cgstValueInWords = this.toWords(this.cgstAmount);
      this.igstValueInWords = this.toWords(this.igstAmount);
      this.sgstValueInWords = this.toWords(this.sgstAmount);


    }
    else {
      this.discOnSubTotalFunction();
    }

  }


  //////////////////////// delete taxes //////////////////////////////////////////

  deleteTaxes(i) {

    this.netValue = this.netValue - this.taxesList[i].taxAmount;
    this.taxesList[i].taxAmount = 0;

    if (this.taxesList[i].name == 'CGST 9%') {
      this.challanInvoiceDocCalculationObject.cgst = this.taxesList[i].taxAmount;
      this.cgstDisplay = 0;

    }
    else if (this.taxesList[i].name == 'IGST 18%') {
      this.challanInvoiceDocCalculationObject.igst = this.taxesList[i].taxAmount;
      this.igstDisplay = 0;

    }
    else if (this.taxesList[i].name == 'SGST 9%') {
      this.challanInvoiceDocCalculationObject.sgst = this.taxesList[i].taxAmount;
      this.sgstDisplay = 0;
    }

    this.taxesList.splice(i, 1);
    this.taxesArrayName.splice(i, 1);
    this.netValue = parseFloat(this.netValue).toFixed(2);

    this.netValueInWords = this.toWords(this.netValue);

    this.taxes = null;

    console.log("this.taxeslist" + this.taxesList);
    return true;
  }

  deleteDiscountOnSubtotal() {

    // this.discOnSubTotal = 0;
    // this.itemType = null;
    // this.total = this.subTotal;

    // this.discOnSubTotalForShow = 0;
    // this.discOnSubTotalZero();

    this.discOnSubTotal = 0;
    this.itemType = null;
    this.total = this.subTotal;
    this.netValue = this.total;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.netValueInWords = this.toWords(this.netValue)
    this.discOnSubTotalForShow = 0;
    this.discOnSubTotalFunction();

  }


  ////////////////////////////////////////////  save data  /////////////////////////////////////////////  
  saveItemDetails(i) {

    document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
    console.log(this.reqdocParaDetailsForm.value);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('srNo').patchValue(i + 1);

    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    console.log(this.dynamicArray);

    this.challanCumInvoiceDocumentObject.invoiceNumber = this.invDocNumberWithPrefix;
    this.challanCumInvoiceDocumentObject.invDocumentNumber = this.createChallanCumInvoiceObj.invDocumentNumber;
    this.challanCumInvoiceDocumentObject.srNo = i + 1;
    this.challanCumInvoiceDocumentObject.poSrNo = this.dynamicArray[i].poSrNO;
    this.challanCumInvoiceDocumentObject.itemOrPartCode = this.dynamicArray[i].itemOrPartCode;

    this.challanCumInvoiceDocumentObject.description = this.dynamicArray[i].description;
    this.challanCumInvoiceDocumentObject.rangeFrom = this.dynamicArray[i].rangeFrom;
    this.challanCumInvoiceDocumentObject.rangeTo = this.dynamicArray[i].rangeTo;
    this.challanCumInvoiceDocumentObject.sacCode = this.dynamicArray[i].sacCode;
    if (this.invoiceTypeOfDoc == "Supplier Tax Invoice Charges") {
      this.challanCumInvoiceDocumentObject.hsnNo = this.dynamicArray[i].hsnNo;
    }
    this.challanCumInvoiceDocumentObject.quantity = this.dynamicArray[i].editQuant;
    this.challanCumInvoiceDocumentObject.unitPrice = this.dynamicArray[i].unitPrice;
    this.challanCumInvoiceDocumentObject.amount = this.dynamicArray[i].amount;

    this.convertInstruNameToId(i);

/******************************************************************************************************* */
    //save remaining quantity for range in sr no table
   
//     for(var c=0; c<this.checkQuantityArray.length;c++){

//       if(this.checkQuantityArray[c].docTypePO=="Customer PO Closed"){
//         if(this.documentType=="UCSPL GROUPED BY RANGE"){
//       if(this.dynamicArray[i].instruName==this.checkQuantityArray[c].instruName 
//         && this.dynamicArray[i].rangeFrom==this.checkQuantityArray[c].rangeFromPo
//         && this.dynamicArray[i].rangeTo==this.checkQuantityArray[c].rangeToPo){

//       //  this.purchaseOrderDocumentObject.remainingQuantity=this.checkQuantityArray[c].remQuantity-this.dynamicArray[i].editQuant;
//       this.trialSaveFunctionForRemQuantityForRange(i);
//         this.createCustPoService.updateRemainingQuantityInSrNoPoTable(this.checkQuantityArray[c].srNoTableId,this.purchaseOrderDocumentObject).subscribe(data => {


//         })
//       }
//       }


//     }

//  }

//  //save remaining quantity for group by name in sr no table
//  for(var c=0; c<this.checkQuantityArray.length;c++){
//   if(this.checkQuantityArray[c].docTypePO=="Customer PO Closed"){
//     if(this.documentType=="UCSPL GROUPED BY NAME" || this.documentType=="UCSPL INVOICE BY ID"){
//   if(this.dynamicArray[i].instruName==this.checkQuantityArray[c].instruName 
//     && this.dynamicArray[i].unitPrice==this.checkQuantityArray[c].ratePo
//   ){

//    // this.purchaseOrderDocumentObject.remainingQuantity=this.checkQuantityArray[c].remQuantity-this.dynamicArray[i].editQuant;
//    this.trialSaveFunctionForRemQuantity(i);
//     this.createCustPoService.updateRemainingQuantityInSrNoPoTable(this.checkQuantityArray[c].srNoTableId,this.purchaseOrderDocumentObject).subscribe(data => {


//     })
//   }
//   }


// }

// }
    

// //save remaining po value in create po table
// if(this.docType1=="Customer PO Open"){
//  if(this.checkAmountArray[0].docTypePO=="Customer PO Open"){
//    var remVal=this.checkAmountArray[0].remPOValue-this.netValue;

//  //this.createCustomerPurchaseOrderObject.remainingPoValue=remVal.toString();
//  this.trialSaveFunctionForRemPoValue(i);

//   this.createCustPoService.updateRemainingPoValueInCreatePoTable(this.checkAmountArray[0].createPoTableId,this.createCustomerPurchaseOrderObject).subscribe(data => {


//   })

//  }
// }
/*************************************************************************************************************** */

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


      this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.challanCumInvoiceDocumentVariableArray = data;
        // alert("data saved succesfully");
        this.dynamicArrayTrial = this.dynamicArray;
        this.saveSQDocDataWithSrNo(i);
        document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";

      })

    })

  }

  convertInstruNameToId(j) {
    for (let i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentName == this.dynamicArray[j].instruName) {
        this.challanCumInvoiceDocumentObject.instruId = this.instrumentList[i].instrumentId;
        break;
      }
    }
  }

  trialSaveFunction(i){
    if(this.checkQuantityArray!=null && typeof this.checkQuantityArray!='undefined' && this.checkQuantityArray.length!=0){
    if(this.documentType=="UCSPL GROUPED BY NAME"){
      for(this.jj=0;this.jj<this.checkQuantityArray.length;this.jj++){
        if(this.checkQuantityArray[this.jj].instruName==this.dynamicArray[i].instruName 
           && this.checkQuantityArray[this.jj].ratePo==this.dynamicArray[i].unitPrice
           && this.dynamicArray[i].editQuant > this.dynamicArray[i].quantity
           && parseInt(this.checkQuantityArray[this.jj].remQuantity)<parseInt(this.dynamicArray[i].editQuant.toString())){
            document.getElementById("btnQuantEdit-" + i).style.backgroundColor = "#F07470";
            this.showPoMessage=0;
            $("#alertEdit-"+i).html("Entered Quantity is Greater Than Po Quantity");
            this.trialSetFlag1=0;
            break;
           }
           else{
            
            $("#alertEdit-"+i).html(" ");
            this.trialSetFlag1=1;
           }
           
           
      }
      if(this.jj>this.checkQuantityArray.length){
        document.getElementById("btnQuantEdit-" + i).style.backgroundColor = "#FFFFFF";
        this.showPoMessage=1;
        $("#alertEdit-"+i).html(" ");
        this.getAmount1(i);
        this.trialSetFlag1=1;
      }
  
    }

    if(this.documentType=="UCSPL GROUPED BY RANGE"){

      for(this.jj=0;this.jj<this.checkQuantityArray.length;this.jj++){
        if(this.checkQuantityArray[this.jj].instruName==this.dynamicArray[i].instruName 
           && this.checkQuantityArray[this.jj].rangeFromPo==this.dynamicArray[i].rangeFrom
           && this.checkQuantityArray[this.jj].rangeToPo==this.dynamicArray[i].rangeTo
           && parseInt(this.checkQuantityArray[this.jj].remQuantity)<parseInt(this.dynamicArray[i].editQuant.toString())){
            document.getElementById("btnQuantEdit-" + i).style.backgroundColor = "#F07470";
            this.showPoMessage=0;
            $("#alertEdit-"+i).html("Entered Quantity is Greater Than Po Quantity");
            this.trialSetFlag1=0;
            break;
           }
           else{
            
            $("#alertEdit-"+i).html(" ");
            this.trialSetFlag1=1;
           }
           
           
      }
      if(this.jj>this.checkQuantityArray.length){
        document.getElementById("btnQuantEdit-" + i).style.backgroundColor = "#FFFFFF";
        this.showPoMessage=1;
        $("#alertEdit-"+i).html(" ");
        this.trialSetFlag1=1;
        this.getAmount1(i);
      }
    }
  }

    if(this.trialSetFlag1==0 && this.trialSetFlag==0){
      alert("cant' save");
    }
    

if(this.trialSetFlag1==1 && this.trialSetFlag==1){

 
  this.saveItemDetails(i);
}

  }

  //update remaining quantity properly based on old array and new array
  trialSaveFunctionForRemQuantity(j){

   var editQuantValue = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('editQuant').value;

    var updateRemQuantity=this.trialremainingQuantityCheck[j]- editQuantValue;

    var instruNameForQuantity=((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('instruName').value;

    for(var x=0;x<this.checkQuantityArray.length;x++){
      if(instruNameForQuantity==this.checkQuantityArray[x].instruName){

        this.purchaseOrderDocumentObject.remainingQuantity=parseInt(this.checkQuantityArray[x].remQuantity)+(updateRemQuantity);
        this.checkQuantityArray[x].remQuantity=this.purchaseOrderDocumentObject.remainingQuantity;
        this.trialremainingQuantityCheck[j]=editQuantValue;
        this.dataForpoNumbersArray[x][4]=this.checkQuantityArray[x].remQuantity;
      
  }
}
  }

   //update remaining quantity properly based on old array and new array for Group By Range
   trialSaveFunctionForRemQuantityForRange(j){
    let editQuantValueForRange = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('editQuant').value;
 
    let updateRemQuantityForRange=this.trialremainingQuantityCheckForRange[j]- editQuantValueForRange;

    let instruNameForQuantityForRange=((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('instruName').value;
    let rangeFromForRange=((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('rangeFrom').value;
    let rangeToForRange=((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(j) as FormGroup).get('rangeTo').value;

    for(var x=0;x<this.checkQuantityArray.length;x++){
      if(instruNameForQuantityForRange==this.checkQuantityArray[x].instruName &&
        rangeFromForRange==this.checkQuantityArray[x].rangeFromPo
        && rangeToForRange==this.checkQuantityArray[x].rangeToPo){

        this.purchaseOrderDocumentObject.remainingQuantity=parseInt(this.checkQuantityArray[x].remQuantity)+(updateRemQuantityForRange);
        this.checkQuantityArray[x].remQuantity=this.purchaseOrderDocumentObject.remainingQuantity;
        this.trialremainingQuantityCheckForRange[j]=editQuantValueForRange;
        this.checkQuantityArray[x][4]=this.checkQuantityArray[x].remQuantity;
      
  }
}


   }



  trialSaveFunctionForRemPoValue(j){
  //  var trialRemPoValue1= this.trialRemPoValue-this.netValue;

    var poValuedUpdated=parseFloat(this.netValueOld)-parseFloat(this.netValue);
    var trialRemPoValue1= parseFloat(this.trialRemPoValue)+(poValuedUpdated);

    this.createCustomerPurchaseOrderObject.remainingPoValue=trialRemPoValue1.toString();
    this.netValueOld=this.netValue;
    this.trialRemPoValue=trialRemPoValue1;
  }

  saveSQDocDataWithSrNo(i) {


    if (this.challanCumInvoiceDocumentVariableArray.length == 0) {
      this.createInvoiceService.createChallanCumInvoiceDocument(this.challanCumInvoiceDocumentObject).subscribe(data => {
        console.log("*********data saved********" + data);
        alert("data saved successfully");
        this.srNoInvoiceDocArray = data;
        this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
        this.createInvoiceService.updateChallanCumInvoiceDocumentForUniqueNo(this.srNoInvoiceDocArray.id, this.challanCumInvoiceDocumentObject).subscribe(data => {

          ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentObject.uniqueNo);
          this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;


          if (this.documentType == "UCSPL GROUPED BY NAME") {
            if (this.dynamicArray[i].editQuant != 0) {
              this.setInvoiceTagToOneForInstru(i);
            }
          }
          if (this.documentType == "UCSPL GROUPED BY RANGE") {
            if (this.dynamicArray[i].editQuant != 0) {
              this.setInvoiceTagToOneForRange(i);
            }
          }

        })


      })

    }
    else {
      for (this.n = 0; this.n < this.challanCumInvoiceDocumentVariableArray.length; this.n++) {

        if (this.documentType == "UCSPL GROUPED BY NAME") {

          if (this.challanCumInvoiceDocumentVariableArray[this.n][15] == this.dynamicArray[i].uniqueNo) {
            this.createInvoiceService.updateChallanCumInvoiceDocument(this.challanCumInvoiceDocumentVariableArray[this.n][0], this.challanCumInvoiceDocumentObject).subscribe(data => {
              alert("data updated successfully");
              this.srNoInvoiceDocArray = data;
              this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
              this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

              if (this.documentType == "UCSPL GROUPED BY NAME") {
                this.setInvoiceTagToZeroForInstru(i);
              }
              if (this.documentType == "UCSPL GROUPED BY RANGE") {
                this.setInvoiceTagToZeroForRange(i);
              }

            }
            );
            break;
          }
        }

        if (this.documentType == "UCSPL GROUPED BY RANGE") {
          // if (this.challanCumInvoiceDocumentVariableArray[this.n][15] == this.dynamicArray[i].uniqueNo &&  this.challanCumInvoiceDocumentVariableArray[this.n][16] ==this.dynamicArray[i].instruName && this.challanCumInvoiceDocumentVariableArray[this.n][8] == this.dynamicArray[i].rangeFrom && this.challanCumInvoiceDocumentVariableArray[this.n][9] == this.dynamicArray[i].rangeTo   ) {


          if (this.challanCumInvoiceDocumentVariableArray[this.n][16] == this.dynamicArray[i].instruName && this.challanCumInvoiceDocumentVariableArray[this.n][8] == this.dynamicArray[i].rangeFrom && this.challanCumInvoiceDocumentVariableArray[this.n][9] == this.dynamicArray[i].rangeTo) {
            this.createInvoiceService.updateChallanCumInvoiceDocument(this.challanCumInvoiceDocumentVariableArray[this.n][0], this.challanCumInvoiceDocumentObject).subscribe(data => {
              alert("data updated successfully");
              this.srNoInvoiceDocArray = data;
              this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
              this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

              if (this.documentType == "UCSPL GROUPED BY NAME") {
                this.setInvoiceTagToZeroForInstru(i);
              }
              if (this.documentType == "UCSPL GROUPED BY RANGE") {
                this.setInvoiceTagToZeroForRange(i);
              }

            }
            );
            break;
          }
        }

      }
      if (this.n >= this.challanCumInvoiceDocumentVariableArray.length) {
        this.createInvoiceService.createChallanCumInvoiceDocument(this.challanCumInvoiceDocumentObject).subscribe(data => {
          console.log("*********data saved********" + data);
          this.srNoInvoiceDocArray = data;
          this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
          this.createInvoiceService.updateChallanCumInvoiceDocumentForUniqueNo(this.srNoInvoiceDocArray.id, this.challanCumInvoiceDocumentObject).subscribe(data => {
            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentObject.uniqueNo);
            this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

            alert("data saved successfully");
            if (this.documentType == "UCSPL GROUPED BY NAME") {
              this.setInvoiceTagToOneForInstru(i);
            }
            if (this.documentType == "UCSPL GROUPED BY RANGE") {
              this.setInvoiceTagToOneForRange(i);
            }
          })

        })

      }

    }


    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceDocumentVariableArray = data;

    })


  }


  ///////////////set invoice tag for instrument
  setInvoiceTagToOneForInstru(i) {
    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "", invUniqueNo: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArray.splice(0, this.finalInvoiceTagArray.length);
    this.finalInvoiceTagArray1 = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalInvoiceTagArray);
          // this.arrayOfInvoiceTagForReqNo.push(this.trialInvoiceArray[m].reqNo);
          // this.arryOfInvoiceTagForInstruName.push(this.trialInvoiceArray[m].instruName);
          count++;
        }

      }

    }
    this.createInvoiceService.trialSetInvoiceTagToOneForInstru(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;

      alert("invoice tag=1");
      //  window.location.reload();
    })

  }

  setInvoiceTagToZeroForInstru(i) {

    var count = 0;
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArray.splice(0, this.finalInvoiceTagArray.length);
    // this.finalInvoiceTagArray1.splice(0,this.finalInvoiceTagArray1.length);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalInvoiceTagArray);
          // this.arrayOfInvoiceTagForReqNo.push(this.trialInvoiceArray[m].reqNo);
          // this.arryOfInvoiceTagForInstruName.push(this.trialInvoiceArray[m].instruName);
          count++;
        }
        if (this.dynamicArray[i].editQuant == 0) {
          this.finalInvoiceTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalInvoiceTagArray);
        }

      }

    }
    this.createInvoiceService.trialSetInvoiceTagToZeroForInstru(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;
      alert("invoice tag=0");
      if (this.dynamicArray[i].editQuant != 0) {
        //  this.setInvoiceTagToOneForInstruThroughInvoiceId(i);

        this.setInvoiceTagToOneForInstru(i);
      }
      //  window.location.reload();
    })


  }


  setInvoiceTagToOneForInstruThroughInvoiceId(i) {

    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArray.splice(0, this.finalInvoiceTagArray.length);
    this.finalInvoiceTagArray1 = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: "0" });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalInvoiceTagArray);
          // this.arrayOfInvoiceTagForReqNo.push(this.trialInvoiceArray[m].reqNo);
          // this.arryOfInvoiceTagForInstruName.push(this.trialInvoiceArray[m].instruName);
          count++;
        }

      }

    }
    this.createInvoiceService.trialSetInvoiceTagToOneForInstruThroughInvId(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;

      alert("invoice tag=1");
      //  window.location.reload();
    })

  }

  ///////////////set invoice tag for range
  setInvoiceTagToOneForRange(i) {
    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "", rangeFrom: "", rangeTo: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArrayForRange.splice(0, this.finalInvoiceTagArrayForRange.length);
    this.finalInvoiceTagArray1ForRange = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName &&
        this.trialInvoiceArray[m].rangeFrom == this.dynamicArrayTrial[i].rangeFrom &&
        this.trialInvoiceArray[m].rangeTo == this.dynamicArrayTrial[i].rangeTo) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
          // this.arrayOfInvoiceTagForReqNo.push(this.trialInvoiceArray[m].reqNo);
          // this.arryOfInvoiceTagForInstruName.push(this.trialInvoiceArray[m].instruName);
          count++;
        }

      }

    }
    this.createInvoiceService.trialSetInvoiceTagToOneForRange(this.finalInvoiceTagArray1ForRange).subscribe(data => {
      this.trailInvoiceTagData = data;

      alert("invoice tag=1");
      //  window.location.reload();
    })

  }

  setInvoiceTagToZeroForRange(i) {

    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "", rangeFrom: "", rangeTo: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArrayForRange.splice(0, this.finalInvoiceTagArrayForRange.length);
    this.finalInvoiceTagArray1ForRange = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName &&
        this.trialInvoiceArray[m].rangeFrom == this.dynamicArrayTrial[i].rangeFrom &&
        this.trialInvoiceArray[m].rangeTo == this.dynamicArrayTrial[i].rangeTo) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
          // this.arrayOfInvoiceTagForReqNo.push(this.trialInvoiceArray[m].reqNo);
          // this.arryOfInvoiceTagForInstruName.push(this.trialInvoiceArray[m].instruName);
          count++;
        }
        if (this.dynamicArray[i].editQuant == 0) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.invDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
        }

      }

    }
    this.createInvoiceService.trialSetInvoiceTagToZeroForRange(this.finalInvoiceTagArray1ForRange).subscribe(data => {
      this.trailInvoiceTagData = data;
      alert("invoice tag=0");
      if (this.dynamicArray[i].editQuant != 0) {

        this.setInvoiceTagToOneForRange(i);

      }
      //  window.location.reload();
    })


  }



  saveAllData() {
    ///////////////////////*********start code here///////////save vendor code also */

    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedNameAndDept == this.customers[s]) {

        this.createChallanCumInvoiceObj1.custShippedTo = this.customer1[s].id;
        break;
      }
    }

    for (var s = 0; s < this.customers.length; s++) {
      if (this.billedNameAndDept == this.customers[s]) {

        this.createChallanCumInvoiceObj1.custBilledTo = this.customer1[s].id;
        break;
      }
    }

    //  this.createChallanCumInvoiceObj1.dcDate = this.dcDate;
    var dcDate1 = this.convertStringToDate(this.dcDate);
    this.createChallanCumInvoiceObj1.dcDate = this.datepipe.transform(dcDate1, 'dd-MM-yyyy');

    this.createChallanCumInvoiceObj1.dcNo = this.dcNo
    this.createChallanCumInvoiceObj1.branchId = this.createChallanCumInvoiceObj.branchId

    this.createChallanCumInvoiceObj1.invDocumentNumber = this.createChallanCumInvoiceObj.invDocumentNumber
    this.convertDocTypeNameToId();
    this.convertInvoiceTypeNameToId();

    this.createChallanCumInvoiceObj1.id = this.createChallanCumInvoiceObj.id

    var invoiceDate1 = this.convertStringToDate(this.dateOfInvoice);
    // this.createChallanCumInvoiceObj1.invoiceDate = this.dateOfInvoice;
    this.createChallanCumInvoiceObj1.invoiceDate = this.datepipe.transform(invoiceDate1, 'dd-MM-yyyy');

    //  this.createChallanCumInvoiceObj1.poDate = this.poDate;
    var poDate1 = this.convertStringToDate(this.poDate);
    this.createChallanCumInvoiceObj1.poDate = this.datepipe.transform(poDate1, 'dd-MM-yyyy');

    this.createChallanCumInvoiceObj1.poNo = this.poNo
    this.createChallanCumInvoiceObj1.vendorCodeNumber = this.vendorCodeNo;
    this.createChallanCumInvoiceObj1.createdBy = this.createdBy;
    this.createChallanCumInvoiceObj1.remark = this.remark;
    this.createChallanCumInvoiceObj1.archieveDate = this.archieveDate;
    this.createChallanCumInvoiceObj1.requestNumber = this.requestNumber;



    this.createInvoiceService.updateCreateChallanCumInvoice(this.createChallanCumInvoiceObj.id, this.createChallanCumInvoiceObj1).subscribe(data => {
      console.log("*********data saved********" + data);
      this.createChallanCumInvoiceObjForColor = data;
      //  document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";
      this.convertStatusIdToName();

      if(this.docT!="UCSPL INVOICE BY ID" && this.documentType !="UCSPL INVOICE BY ID"){
        this.removeAllData();
       }

      // if (this.docT != null && typeof this.docT != 'undefined') {
      //   if (this.docT != this.documentType) {
      //     this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      //       this.removeData = res;
      //       this.docT = this.documentType;

      //     })

      //   }
      // }
    })

    // this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
    //   this.printTermsId = res;
    //   console.log(this.printTermsId);
    //   this.saveAllRequestNumbers();

    // })

 

    this.challanInvoiceDocCalculationObject.subTotal = this.subTotal;
    this.challanInvoiceDocCalculationObject.discountOnSubtotal = this.discOnSubTotal;
    this.challanInvoiceDocCalculationObject.total = this.total;
    this.challanInvoiceDocCalculationObject.net = parseFloat(this.netValue);
    this.challanInvoiceDocCalculationObject.invDocumentNumber = this.createChallanCumInvoiceObj.invDocumentNumber;
    this.challanInvoiceDocCalculationObject.cgst = this.cgstAmount;
    this.challanInvoiceDocCalculationObject.igst = this.igstAmount;
    this.challanInvoiceDocCalculationObject.sgst = this.sgstAmount;

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocumentCalculation(this.challanInvoiceDocCalculationObject.invDocumentNumber).subscribe(data => {
      console.log("*********send doc no ********" + data);
    })

    this.createInvoiceService.getDocDetailsForChallanCumInvoiceDocumentCalculation().subscribe(data => {
      console.log("*********doc no details collection********" + data);
      this.challanCumInvoiceDocumentCalculationVariableArray = data;
      alert("data saved succesfully");
      this.saveInvDocCalcDetails();

    })

  }

  convertStatusIdToName() {

    if (this.createChallanCumInvoiceObj.draft == 1) {
      this.status = "Draft";
      this.manDocForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObj.archieved == 1) {
      this.status = "Archieved";
      this.manDocForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObj.submitted == 1) {
      this.status = "Submitted";
      this.manDocForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObj.approved == 1) {
      this.status = "Approved";
      this.manDocForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObj.rejected == 1) {
      this.status = "Rejected";
      this.manDocForColor.status = this.status;
    }

  }

  removeAllData(){
    if (this.docT != null && typeof this.docT != 'undefined' && this.docT!='UCSPL INVOICE BY ID'){
      // if(this.checkifidselected!=1){
    if (this.docT != this.documentType && this.documentType!='UCSPL INVOICE BY ID') {
      this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
        this.removeData = res;
        this.docT = this.documentType;


        this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
          
          

        })
      })
    }
  }
  }

  convertStringToDate(value) {

    if (typeof value === 'string') {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);

      return dateFormat;
    }
    else {
      return value;
    }
  }

  saveAllRequestNumbers() {

    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {

      this.invoiceAndrequestNumberJunction.invoiceDocNo = this.createChallanCumInvoiceObj.invDocumentNumber;
      this.invoiceAndrequestNumberJunction.requestNumber = this.requestNumbersCollectionArray[i].itemText;
      this.createInvoiceService.saveReuestNumbers(this.invoiceAndrequestNumberJunction).subscribe(data => {
        console.log("request number saved successfully" + data);
      });
      // this.functionCallForRequestNumber(i,this.createChallanCumInvoiceObj.invDocumentNumber);

    }

  }

  convertDocTypeNameToId() {
    for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
      if (this.documentTypeObjectData[i].documentTypeName == this.documentType) {
        this.createChallanCumInvoiceObj1.documentTypeId = this.documentTypeObjectData[i].documentTypeId;
      }
    }
  }

  convertInvoiceTypeNameToId() {

    for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {
      if (this.invoiceTypeObjectData[i].invoiceTypeName == this.invoiceTypeOfDoc) {
        this.createChallanCumInvoiceObj1.invoiceTypeId = this.invoiceTypeObjectData[i].invoiceTypeId;

      }
    }
  }

  saveInvDocCalcDetails() {

    if (this.challanCumInvoiceDocumentCalculationVariableArray.length == 0) {
      this.createInvoiceService.createChallanCumInvoiceDocumentCalculation(this.challanInvoiceDocCalculationObject).subscribe(data => {
        console.log("data updated successfully in ChallanCumDocumentCalculationtable" + data);
        this.savePoValueAndQuantity();
      }
      );
    }
    else {
      this.createInvoiceService.updateChallanCumInvoiceDocumentCalculation(this.challanCumInvoiceDocumentCalculationVariableArray[0][0], this.challanInvoiceDocCalculationObject).subscribe(data => {
        console.log("data updated successfully" + data);
        this.savePoValueAndQuantity();
      }
      );
    }
  }


  onSubmit() {
    //this.getSrNoData();                // comment this 
    //this.DocTypeFunForGrouping();   uncomment this 
    this.createChallanCumInvoiceObj1.vendorCodeNumber = this.vendorCodeNo;
    this.saveAllData();
    this.createChallanCumInvoiceObj1.invDocumentNumber = this.documentNumber;

    this.createChallanCumInvoiceObj1.createdBy = this.createdBy;
    this.createChallanCumInvoiceObj1.requestNumber = this.requestNumber;

    switch (this.status) {
      case 'Draft': this.createChallanCumInvoiceObj1.draft = 1;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Archieved': this.createChallanCumInvoiceObj1.draft = 0
        this.createChallanCumInvoiceObj1.archieved = 1;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Rejected': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 1;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Approved': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 1;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Submitted': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 1;
        break;

    }
    this.createChallanCumInvoiceObj1.remark = this.remark;

    this.createInvoiceService.updateCreateChallanCumInvoice(this.createChallanCumInvoiceObj.id, this.createChallanCumInvoiceObj1).subscribe(data => {
      console.log(data);
      //  document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";

      // if (this.docT != null && typeof this.docT != 'undefined') {
      //   if (this.docT != this.documentType) {
      //     this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      //       this.removeData = res;
      //       this.docT = this.documentType;

      //     })

      //   }
      // }
    })
    // if(this.requestNumberCollectionArrayForInvoice.length!=this.requestNumbersCollectionArray.length){
    //   this.requestNumberCollectionArrayForInvoice.length=this.requestNumbersCollectionArray.length
    this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.createChallanCumInvoiceObj.invDocumentNumber).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);
      this.saveAllRequestNumbers();

    })
    // }
    //document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";
  }



  ////////////////////////////////////// pop up screens /////////////////////////////////////////
  manageFotters() {                                                      //for footer 
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }

  manageDocument() {                                                      // for document

    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()
  }

  manageHeaders() {                                                      // for header

    const buttonModal2 = document.getElementById("openModalButton2")
    console.log('buttonModal2', buttonModal2)
    buttonModal2.click()
  }

  printPage() {
    window.print();
  }


  toggle(e) {
    console.log(e);
    this.selectTagEvent = e;

    if (this.selectTagEvent == 4) {
      this.discOnSubTotalForShow = 1;
    }
    else {
      this.discOnSubTotalForShow = 0;
    }

  }


  toggleHeader(e) {
    console.log(e);
    this.selectTagEventForHeader = e;
  }




  changeCat1(e) {
    if (e == 1) {
      this.taxesArray.push(1);
      this.taxesArrayName.push("CGST 9%");
      this.taxesList.push({ name: "CGST 9%", taxAmount: 0 });
      this.getAmount1(0);
    }

    if (e == 2) {
      this.taxesArray.push(2);
      this.taxesArrayName.push("IGST 18%");
      this.taxesList.push({ name: "IGST 18%", taxAmount: 0 });
      this.getAmount1(0);
    }

    if (e == 3) {
      this.taxesArray.push(3);
      this.taxesArrayName.push("SGST 9%");
      this.taxesList.push({ name: "SGST 9%", taxAmount: 0 });
      this.getAmount1(0);
    }

    console.log("this.taxes_array" + this.taxesArray);

  }

  /////////////////////////////// search invoice document code starts from here ////////////////////////////

  getChallanCumInvoiceDocumentSearchInfo() {
    this.taxesTotal = 0;
    this.documentNumberFromSearchInvoice = this.searchInvoiceService.getDocNoFromSearchinvoice();


    var trial
    if (typeof this.documentNumberFromSearchInvoice != 'undefined') {
      this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocumentCalculation(this.documentNumberFromSearchInvoice).subscribe(data => {
        trial = data;
        console.log("*********send doc no ********" + trial);

      })

      this.createInvoiceService.getDocDetailsForChallanCumInvoiceDocumentCalculation().subscribe(data => {
        console.log("*********search doc no details collection********" + data);
        this.searchChallanInvoiceDocumentCalculationArray = data;

        if (this.searchChallanInvoiceDocumentCalculationArray.length != 0) {
          debugger;
          this.subTotal = this.searchChallanInvoiceDocumentCalculationArray[0][2];
          this.discOnSubTotal = this.searchChallanInvoiceDocumentCalculationArray[0][3];
          this.total = this.searchChallanInvoiceDocumentCalculationArray[0][4];
          this.netValue = this.searchChallanInvoiceDocumentCalculationArray[0][8];
          this.netValueOld = this.searchChallanInvoiceDocumentCalculationArray[0][8];
          this.cgstAmount = this.searchChallanInvoiceDocumentCalculationArray[0][5];
          this.igstAmount = this.searchChallanInvoiceDocumentCalculationArray[0][6];
          this.sgstAmount = this.searchChallanInvoiceDocumentCalculationArray[0][7];


          if (this.cgstAmount != null && this.cgstAmount != 0) {
            this.cgstDisplay = 1;
            this.cgst = 1;
          }
          if (this.igstAmount != null && this.igstAmount != 0) {
            this.igstDisplay = 1;
            this.igst = 1;
          }
          if (this.sgstAmount != null && this.sgstAmount != 0) {
            this.sgstDisplay = 1;
            this.sgst = 1;
          }
          if (this.discOnSubTotal != null && this.discOnSubTotal != 0) {
            this.discOnSubTotalForShow = 1;
          }

          this.taxesTotal = this.cgstAmount + this.igstAmount + this.sgstAmount;


          this.netValue = parseFloat(this.netValue).toFixed(2);
          this.taxesTotal = parseFloat(this.taxesTotal).toFixed(2);
          this.cgstAmount = parseFloat(this.cgstAmount).toFixed(2);
          this.sgstAmount = parseFloat(this.sgstAmount).toFixed(2);
          this.igstAmount = parseFloat(this.igstAmount).toFixed(2);
          this.netValueInWords = this.toWords(this.netValue);
          this.gstValueInWords = this.toWords(this.taxesTotal);
          this.cgstValueInWords = this.toWords(this.cgstAmount);
          this.igstValueInWords = this.toWords(this.igstAmount);
          this.sgstValueInWords = this.toWords(this.sgstAmount);

          this.getHeaderInfoForSearchInvoice();

        }
      })


    }


  }


  getHeaderInfoForSearchInvoice() {
    this.searchInvoiceService.sendDocNoForGettingCreateInvDetails(this.documentNumberFromSearchInvoice).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })


    this.searchInvoiceService.getCreateInvsDetailsForSearchInv().subscribe(data => {
      console.log("********* header information ********" + data);
      this.createInvoiceInfoForSearchInvoiceObject = data;

      if (typeof this.createInvoiceInfoForSearchInvoiceObject != 'undefined') {
        this.createChallanCumInvoiceObj.id = this.createInvoiceInfoForSearchInvoiceObject[0][0];
        this.createChallanCumInvoiceObj.documentType = this.createInvoiceInfoForSearchInvoiceObject[0][1];
        this.createChallanCumInvoiceObj.requestNumber = this.createInvoiceInfoForSearchInvoiceObject[0][2];

        this.createChallanCumInvoiceObj.branchId = this.createInvoiceInfoForSearchInvoiceObject[0][4];
        this.createChallanCumInvoiceObj.invoiceType = this.createInvoiceInfoForSearchInvoiceObject[0][5];

        this.createChallanCumInvoiceObj.custShippedTo = this.createInvoiceInfoForSearchInvoiceObject[0][6];
        this.createChallanCumInvoiceObj.custBilledTo = this.createInvoiceInfoForSearchInvoiceObject[0][7];

        this.createChallanCumInvoiceObj.dateOfInvoice = this.createInvoiceInfoForSearchInvoiceObject[0][8];
        this.createChallanCumInvoiceObj.poDate = this.createInvoiceInfoForSearchInvoiceObject[0][9];
        this.createChallanCumInvoiceObj.dcDate = this.createInvoiceInfoForSearchInvoiceObject[0][10];
        this.createChallanCumInvoiceObj.poNo = this.createInvoiceInfoForSearchInvoiceObject[0][11];
        this.createChallanCumInvoiceObj.dcNo = this.createInvoiceInfoForSearchInvoiceObject[0][12];
        this.createChallanCumInvoiceObj.vendorCodeNumber = this.createInvoiceInfoForSearchInvoiceObject[0][13];
        this.createChallanCumInvoiceObj.archieveDate = this.createInvoiceInfoForSearchInvoiceObject[0][14];
        this.createChallanCumInvoiceObj.createdBy = this.createInvoiceInfoForSearchInvoiceObject[0][15];
        this.createChallanCumInvoiceObj.draft = this.createInvoiceInfoForSearchInvoiceObject[0][16];
        this.createChallanCumInvoiceObj.approved = this.createInvoiceInfoForSearchInvoiceObject[0][17];
        this.createChallanCumInvoiceObj.rejected = this.createInvoiceInfoForSearchInvoiceObject[0][18];
        this.createChallanCumInvoiceObj.submitted = this.createInvoiceInfoForSearchInvoiceObject[0][19];
        this.createChallanCumInvoiceObj.archieved = this.createInvoiceInfoForSearchInvoiceObject[0][20];

        this.createChallanCumInvoiceObj.remark = this.createInvoiceInfoForSearchInvoiceObject[0][21];
        this.createChallanCumInvoiceObj.invDocumentNumber = this.createInvoiceInfoForSearchInvoiceObject[0][22];

        this.id = this.createChallanCumInvoiceObj.id;
        this.customerIdForcustShippedTo = this.createChallanCumInvoiceObj.custShippedTo;
        this.customerIdForcustBilledTo = this.createChallanCumInvoiceObj.custBilledTo;

        
  // get all po list
  if(typeof  this.customerIdForcustBilledTo!='undefined' ||  this.customerIdForcustBilledTo!=null){
    this.createInvoiceService.getAllPoNumbersForCustId(this.customerIdForcustBilledTo).subscribe(data => {
      this.dataForpoNumbersArray=data;
      console.log("dataForpoNumbersArray"+this. dataForpoNumbersArray);
      for(var i=0;i<this.dataForpoNumbersArray.length;i++){
        this.onlyPoNumbersArray.push(this.dataForpoNumbersArray[i][0]);
        this.onlyDocumentTypeArray.push(this.dataForpoNumbersArray[i][1]);
      }
       //code for unique po numbers
  var filter1 = function (value, index)
  { return this.indexOf(value) == index 
 };
 this.uniquePoNumbersArray = this.onlyPoNumbersArray.filter(filter1, this.onlyPoNumbersArray);
 console.log(this.uniquePoNumbersArray);


 this.invoiceDocumentOFManageDocument = this.createChallanCumInvoiceObj;

        this.getInformation();
        this.getInformationYourReference();
        this.getInformationForManageDocument();
        this.convertStatusIdToName();
      
    })

  }



       

      }
    });

  }


  getInformationForManageDocument() {

    this.manageDocumentVariable = this.invoiceDocumentOFManageDocument;
    this.manDocForColor = this.invoiceDocumentOFManageDocument;

    if (typeof this.invoiceDocumentOFManageDocument != 'undefined') {

      this.documentNumber = this.manageDocumentVariable.invDocumentNumber;
      this.documentType = this.manageDocumentVariable.documentType;

      this.documentDate = this.manageDocumentVariable.documentDate;
      this.custRefNo = this.manageDocumentVariable.custRefNo;
      this.refDate = this.manageDocumentVariable.refDate;
      this.archieveDate = this.manageDocumentVariable.archieveDate;
      this.requestNumber = this.manageDocumentVariable.requestNumber;
      this.email = this.manageDocumentVariable.email;
      this.status = this.manageDocumentVariable.status;

      this.remark = this.manageDocumentVariable.remark;
      this.id = this.manageDocumentVariable.id;
      this.createdBy = this.manageDocumentVariable.createdBy;
      this.invoiceTypeOfDoc = this.manageDocumentVariable.invoiceType;

      //this.DocTypeFunForGrouping();
      (this.manageDocumentForm.get('documentNumber').patchValue(this.documentNumber));
      (this.manageDocumentForm.get('documentType').patchValue(this.documentType));
      // (this.manageDocumentForm.get('documentDate').patchValue(this.documentDate));
      (this.manageDocumentForm.get('archieveDate').patchValue(this.archieveDate));
      (this.manageDocumentForm.get('status').patchValue(this.status));
      (this.manageDocumentForm.get('remark').patchValue(this.remark));
      (this.manageDocumentForm.get('createdBy').patchValue(this.createdBy));
      (this.manageDocumentForm.get('invoiceTypeOfDoc').patchValue(this.invoiceTypeOfDoc));

    }
  }

  getSrNoForSearchInvoice() {

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.documentNumberFromSearchInvoice).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })

    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceArrayForSearchInvoice = data;

      if (this.challanCumInvoiceArrayForSearchInvoice.length != 0) {

        for (var z = 0; z < this.challanCumInvoiceArrayForSearchInvoice.length; z++) {

          this.newDynamic1.push({
            srNo: this.challanCumInvoiceArrayForSearchInvoice[z][3],
            poSrNO: this.challanCumInvoiceArrayForSearchInvoice[z][4],
            itemOrPartCode: this.challanCumInvoiceArrayForSearchInvoice[z][5],
            instruName: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            description: this.challanCumInvoiceArrayForSearchInvoice[z][7],
            idNo: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            range: this.challanCumInvoiceArrayForSearchInvoice[z][8],
            sacCode: this.challanCumInvoiceArrayForSearchInvoice[z][9],
            quantity: this.challanCumInvoiceArrayForSearchInvoice[z][10],
            unitPrice: this.challanCumInvoiceArrayForSearchInvoice[z][11],
            amount: this.challanCumInvoiceArrayForSearchInvoice[z][12]


          })

        }

        this.dynamicArray.splice(0, 1);
        for (var l = 1; l < this.newDynamic1.length; l++) {
          this.convertInsruIdtoName(l);
          this.dynamicArray.push(this.newDynamic1[l]);
        }
      }
    })

  }


  backToMenu() {
    this.router.navigate(['/nav/searchdelichn']);
  }

  toggleHeaderForCustomer(e) {
    console.log(e);
    this.selectTagEventForHeader = e;
  }

  headerCustNameChange(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedCustNameTrial == this.customers[s]) {

        this.custNameTrialShippedTo = this.customer1[s].name;
        this.shippedCustDepartment = this.customer1[s].department;
        this.shippedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.shippedCustPanNo = this.customer1[s].panNo;
        this.shippedCustGstNo = this.customer1[s].gstNo;
        this.shippedCustAddressline1 = this.customer1[s].addressLine1;
        this.shippedCustAddressline2 = this.customer1[s].addressLine2;
        this.shippedCustCountry = this.customer1[s].country;
        this.shippedCustState = this.customer1[s].state;
        this.shippedCustCity = this.customer1[s].city;
        this.shippedCustPin = this.customer1[s].pin;
        break;
      }
    }
    this.splitGstAndQuotationNumber1();
  }


  headerCustNameChange1(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.billedCustNameTrial == this.customers[s]) {

        this.custNameTrialBilledTo = this.customer1[s].name;
        this.billedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.billedCustDepartment = this.customer1[s].department;
        this.billedCustPanNo = this.customer1[s].panNo;
        this.billedCustGstNo = this.customer1[s].gstNo;
        this.billedCustAddressline1 = this.customer1[s].addressLine1;
        this.billedCustAddressline2 = this.customer1[s].addressLine2;
        this.billedCustCountry = this.customer1[s].country;
        this.billedCustState = this.customer1[s].state;
        this.billedCustCity = this.customer1[s].city;
        this.billedCustPin = this.customer1[s].pin;
        this.cgst = this.customer1[s].cgst;
        this.sgst = this.customer1[s].sgst;
        this.igst = this.customer1[s].igst;

        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
        this.calculationAfterCustChange();
        break;
      }
    }
    this.splitGstAndQuotationNumber2();
  }

  splitGstAndQuotationNumber1() {
    if (typeof this.shippedCustGstNo != 'undefined') {
      this.stateCode = this.shippedCustGstNo.slice(0, 2);

    }
  }
  splitGstAndQuotationNumber2() {
    if (typeof this.billedCustGstNo != 'undefined') {
      this.billedStateCode = this.billedCustGstNo.slice(0, 2);


    }
  }

  trailFunction() {
    // this.dataForGroupingInvoice();   uncomment this 
    // this.DocTypeFunForGrouping();  uncomment this 

    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";


  }
  editQuantity(){
    this.checkQuantity();
  const buttonModal13 = document.getElementById("openModalButton13")
    console.log('buttonModal13', buttonModal13)
    buttonModal13.click();
   

  }

  checkQuantity(){
   

     this.createInvoiceService.getAllPoNumbersForCustId(this.customerIdForcustBilledTo).subscribe(data => {
      this.dataForpoNumbersArray=data;

      this.checkAmountArray.splice(0,this.checkAmountArray.length);
      this.checkAmountArray1.splice(0,this.checkAmountArray1.length);
       this.checkQuantityArray.splice(0,this.checkQuantityArray.length);

   for(var x=0;x<this.dataForpoNumbersArray.length;x++){
     if(this.poNo==this.dataForpoNumbersArray[x][0]){
       if(this.dataForpoNumbersArray[x][1]=="Customer PO Closed"){
       this.checkQuantityArray.push({instruName:this.dataForpoNumbersArray[x][2],docTypePO:this.dataForpoNumbersArray[x][1],remQuantity:this.dataForpoNumbersArray[x][4],srNoTableId:this.dataForpoNumbersArray[x][7],rangeFromPo:this.dataForpoNumbersArray[x][8],rangeToPo:this.dataForpoNumbersArray[x][9],ratePo:this.dataForpoNumbersArray[x][10]});
       this.docType1= this.checkQuantityArray[0].docTypePO;

      // this.checkAmountArray1.push({doctype:"Customer PO Closed",remPOValue:this.dataForpoNumbersArray[x][3],poValue:this.dataForpoNumbersArray[x][5]});;
     //  checkQuantityArray:Array<{instruName:string,doctype:string,remQuantity:any,remAmount:any}>=[];
    


    }
    else{
      this.checkAmountArray.push({docTypePO:this.dataForpoNumbersArray[x][1],remPOValue:this.dataForpoNumbersArray[x][3],poValue:this.dataForpoNumbersArray[x][5],createPoTableId:this.dataForpoNumbersArray[x][6]});
      this.docType1= this.checkAmountArray[0].docTypePO;
      this.trialPoValue=this.checkAmountArray[0].poValue;
      this.trialRemPoValue=this.checkAmountArray[0].remPOValue;
    }

     }
   }


  if(this.docType1=="Customer PO Closed"){
    this.checkAmountArray1.push({docTypePO:"Customer PO Closed",remPOValue:this.dataForpoNumbersArray[0][3],poValue:this.dataForpoNumbersArray[0][5],createPoTableId:this.dataForpoNumbersArray[0][6]});
  }else{
    this.checkAmountArray1.push(this.checkAmountArray[0]);
  //  console.

  }

})
   
  
  }

  savePoValueAndQuantity(){
    
    //save remaining quantity for range in sr no table
    if(this.checkQuantityArray!=null && typeof this.checkQuantityArray!='undefined' && this.checkQuantityArray.length!=0){
      if(this.documentType=="UCSPL GROUPED BY RANGE"){
    for(var i=0; i<this.dynamicArray.length;i++){
    for(var c=0; c<this.checkQuantityArray.length;c++){

      if(this.checkQuantityArray[c].docTypePO=="Customer PO Closed"){
       
      if(this.dynamicArray[i].instruName==this.checkQuantityArray[c].instruName 
        && this.dynamicArray[i].rangeFrom==this.checkQuantityArray[c].rangeFromPo
        && this.dynamicArray[i].rangeTo==this.checkQuantityArray[c].rangeToPo){
          this.trialSaveFunctionForRemQuantityForRange(i);
      //  this.purchaseOrderDocumentObject.remainingQuantity=this.checkQuantityArrayTrial[c].remQuantity-this.dynamicArray[i].editQuant;

        this.createCustPoService.updateRemainingQuantityInSrNoPoTable(this.checkQuantityArray[c].srNoTableId,this.purchaseOrderDocumentObject).subscribe(data => {


        })
      }
      }


    }

 }
 alert("save remaining po quantity for range");
}





//save remaining quantity for group by name in sr no table
if(this.documentType=="UCSPL GROUPED BY NAME" || this.documentType=="UCSPL INVOICE BY ID"){
for(var i=0; i<this.dynamicArray.length;i++){
 for(var c=0; c<this.checkQuantityArray.length;c++){
  if(this.checkQuantityArray[c].docTypePO=="Customer PO Closed"){
   
  if(this.dynamicArray[i].instruName==this.checkQuantityArray[c].instruName 
    && this.dynamicArray[i].unitPrice==this.checkQuantityArray[c].ratePo
  ){

  //  this.purchaseOrderDocumentObject.remainingQuantity=this.checkQuantityArrayTrial[c].remQuantity-this.dynamicArray[i].editQuant;
  this.trialSaveFunctionForRemQuantity(i);
    this.createCustPoService.updateRemainingQuantityInSrNoPoTable(this.checkQuantityArray[c].srNoTableId,this.purchaseOrderDocumentObject).subscribe(data => {


    })
  }
  }


}

}
alert("save remaining po quantity for name");
}

    }


//save remaining po value in create po table
if(this.checkAmountArray!=null && typeof this.checkAmountArray!='undefined' && this.checkAmountArray.length!=0){
 
if(this.docType1=="Customer PO Open"){
 if(this.checkAmountArray[0].docTypePO=="Customer PO Open"){
   var remVal=this.checkAmountArray[0].remPOValue-this.netValue;

 //this.createCustomerPurchaseOrderObject.remainingPoValue=remVal.toString();
 this.trialSaveFunctionForRemPoValue(i);

  this.createCustPoService.updateRemainingPoValueInCreatePoTable(this.checkAmountArray[0].createPoTableId,this.createCustomerPurchaseOrderObject).subscribe(data => {


  })

 }
}
  }
  
  }

}

