import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDelivChallandocComponent } from './create-deliv-challandoc.component';

describe('CreateDelivChallandocComponent', () => {
  let component: CreateDelivChallandocComponent;
  let fixture: ComponentFixture<CreateDelivChallandocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDelivChallandocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDelivChallandocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
