
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { mapChildrenIntoArray } from '@angular/router/src/url_tree';
import { forEach } from '@angular/router/src/utils/collection';
import { getMaxListeners } from 'process';
import { expand } from 'rxjs/operator/expand';
import { map } from 'rxjs/operator/map';
import { tap } from 'rxjs/operators';
import { Capabilities, Capability } from 'selenium-webdriver';
import { CreateCustomerPurchaseOrder, CreateCustPoService, PurchaseOrderDocument } from '../../../../services/serviceconnection/create-service/create-cust-po.service';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { Branch, DocumentTypeForInvoice, Instrument, InvoiceTypeForInvoice } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateDeliChallanService, CreateDeliveryChallan, SrNoDeliveryChallan} from '../../../../services/serviceconnection/create-service/create-deli-challan.service';
import { ChallanInvoiceDocCalculation, ChallanInvoiceDocument, CreateChallanCumInvoice, CreateInvoiceService, DynamicGrid, InvoiceAndrequestNumberJunction } from '../../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateSalesService } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { SearchInvoiceService } from '../../../../services/serviceconnection/search-service/search-invoice.service';
import { CreateSpecificationDetailsComponent } from '../../../certificate/master-instrument-detail/create-specification-details/create-specification-details.component';

declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-create-deliv-challandoc',
  templateUrl: './create-deliv-challandoc.component.html',
  styleUrls: ['./create-deliv-challandoc.component.css']
})
export class CreateDelivChallandocComponent implements OnInit {

  manageDocumentForm: FormGroup;
  public reqdocParaDetailsForm: FormGroup;
  //  editQuantityForm:FormGroup;

  clickToDisable:boolean=false;


  createChallanCumInvoiceObj: CreateDeliveryChallan = new CreateDeliveryChallan();
  createChallanCumInvoiceObjForColor: any;
  createChallanCumInvoiceObj1: CreateDeliveryChallan = new CreateDeliveryChallan();
  challanCumInvoiceDocumentObject: SrNoDeliveryChallan = new SrNoDeliveryChallan();
  challanInvoiceDocCalculationObject: ChallanInvoiceDocCalculation = new ChallanInvoiceDocCalculation();

  createSalesInfoForSearchInvoiceObject: CreateChallanCumInvoice = new CreateChallanCumInvoice();
  challanInvoiceForDocument: CreateChallanCumInvoice = new CreateChallanCumInvoice();

  invoiceAndrequestNumberJunction: InvoiceAndrequestNumberJunction = new InvoiceAndrequestNumberJunction();
  purchaseOrderDocumentObject:PurchaseOrderDocument=new PurchaseOrderDocument();
  createCustomerPurchaseOrderObject:CreateCustomerPurchaseOrder=new CreateCustomerPurchaseOrder();
 // PurchaseOrderDocument:PurchaseOrderDocument=new PurchaseOrderDocument();

  j: any = 0;
  l: any = 0;
  tl: Array<number> = [0];
  k: any;
  n: any = 0;
  sot: any = 0;
  sumAll: any = 0;
  tcode: string;
  subTotal: number = 0;
  discOnSubTotal: number = 0;
  total: number = 0;
  netValue: any;
  valueConvertToWord: any = 0;
  gstTotalValue: any = 0;
  gstValueInWords: string;
  cgstValueInWords: string;
  igstValueInWords: string;
  sgstValueInWords: string;

  invoiceTypeOfDoc: string;

  cgstAmount: any = 0;
  sgstAmount: any = 0;
  igstAmount: any = 0;
  amountArray: Array<any> = [];
  data: string[] = [];
  arrayOfDescription: string[] = [];
  arrayOfIdNo: string[] = [];
  arrayOfAccrediation: string[] = [];
  arrayOfRangeAccuracy: string[] = [];
  arrayOfSacCode: string[] = [];
  arrayOfQuantity: Array<number> = [];
  arrayOfRate: Array<number> = [];
  arrayOfDiscountOnItem: Array<number> = [];
  arrayOfDiscountOnTotal: Array<number> = [];
  arrayOfAmount: Array<number> = [];
  arrayOfTerms: Array<number> = [];

  selectedFileForQuotation: any;

  customers: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  static customerNameLength: number;

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  name: String;
  invDocNumber: string;
  prefixInvDocNumber: any;
  salesDocumentInfo: Array<any> = [];
  SalesQuotationDocumentform: FormGroup

  selectedRow: Number;
  checkBoxes: boolean[];

  kindAttnTrial: string;

  static customerDepartments: Array<any> = [];
  customer: Customer = new Customer();
  customer1: Customer = new Customer();
  custRefTrial: any;
  netValueInWords: string;
  valueInWords: string;

  headerType: any;
  billedCustNameTrial: any;
  billedCustPanNo: any;
  billedCustGstNo: any;
  billedCustAddressline1: any;
  billedCustAddressline2: any;
  billedCustCountry: any;
  billedCustState: any;
  billedCustCity: any;
  billedCustPin: any;
  billedCustDepartment: any;
  billedNameAndDept: any;

  shippedCustNameTrial: any;
  shippedCustPanNo: any;
  shippedCustGstNo: any;
  shippedCustAddressline1: any;
  shippedCustAddressline2: any;
  shippedCustCountry: any;
  shippedCustState: any;
  shippedCustCity: any;
  shippedCustPin: any;
  shippedCustDepartment: any;
  shippedNameAndDept: any;
  dateOfQuotation1: any;
  custRefNo1: number;
  refDate1: any;
  kindAttachment1: number;
  stateCode: any;


  termsIdArray: Array<any> = [];
  termsIdArray1: any;
  termsIdArray2: Array<any> = [];
  termsIdArray3: Array<any> = [];
  termsIdArray4: Array<any> = [];

  selectTagEvent: any;
  m: any = 0;
  mm:any=0;
  s: any = 0;
  taxesArray: Array<any> = [];
  taxesArrayName: Array<any> = [];

  dateString = '';
  format = 'dd/MM/yyyy';
  alive = true;
  public dateValue = new Date();
  dateOfQuotation: any;
  custRefNo: string;
  refDate: any;
  archieveDate = new Date();
  kindAttnTrial1: string;
  documentNumber: string;
  typeOfQuotation: string;
  contactNo: string;
  email: string;
  status: string;
  remark: string;
  createdBy: string;
  manageDocumentVariable: any;
  salesQuotationDocumentVariable: any;
  challanCumInvoiceDocumentVariableArray: any;
  challanCumInvoiceDocumentVariableArrayForUniqueTrial: any;
  id: any;


  custNameTrialShippedTo: string;
  custNameTrialBilledTo: string;

  public termValue: string;

  termsList1: Array<{ id: number, name: string }> = [];
  taxesList: Array<{ name: string, taxAmount: number }> = [];
  taxesTotal: any = 0;

  trialCustomerWithAddress: Array<{ customerName: string, name: string, address: string }> = [];

  width: any;
  height: any;
  originalDocNo: string;

  srNoArray: Array<number> = [];

  salesQuotationDocumentVariableArray: Array<any> = [];

  salesQuotationDocumentCalculationVariableArray: Array<any> = [];
  searchSalesQuotationDocumentCalculationArray: Array<any> = [];
  searchChallanInvoiceDocumentCalculationArray: Array<any> = [];
  challanCumInvoiceDocumentCalculationVariableArray: Array<any> = [];

  headerInformationArrayForSearchSales: Array<any> = [];
  invDocNumberWithPrefix: string;
  documentNumberFromSearchSales: string;
  documentNumberFromSearchInvoice: string;
  challanCumInvoiceArrayForSearchInvoice: any;

  termsIdForSearchSalesArray: Array<number> = [];

  customerIdForChallan: any;
  itemType: any;
  taxes: any;
  documentName: string;
  documentType: string;
  documentDate = new Date();
  cgst: any;
  sgst: any;
  igst: any;

  selectTagEventForHeader: any;
  dcDate = new Date();
  dcNo: string;
  dateOfInvoice: any;
  poDate = new Date();
  poNo: any;
  vendorCodeNo: string;
  branch1: Branch = new Branch();
  branchesName: Array<any> = [];
  branchesIds: Array<any> = [];
  branchesDescription: Array<any> = [];
  branchWithDescription: Array<{ description: string, branch: string }> = [];
  dcustomerDcNumber: string;
  requestNumber: string;
  customerDcNumber: string;
  documentTypeObjectData: DocumentTypeForInvoice = new DocumentTypeForInvoice();
  documentTypeName: Array<any> = [];
  documentTypeIds: Array<any> = [];

  invoiceTypeObjectData: InvoiceTypeForInvoice = new InvoiceTypeForInvoice();
  invoiceTypeName: Array<any> = [];
  invoiceTypeIds: Array<any> = [];
  invoiceType: any;

  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];
  billedStateCode: any;
  discOnSubTotalForShow: number;
  cgstDisplay: number;
  sgstDisplay: number;
  igstDisplay: number;
  taxAmount: any;
  leng: number;
  itemsPerPage: number;
  p: number;
  srNoToDelete: any;
  indexToDelete: any;

  requestAndInwardDataArray: Array<any> = [];
  requestAndInwardDocumentNumber: any;
  redDataForCalibrationTag: any;
  redDataForCalibrationTag1: any;
  poDataForInvoice: any;
  poSrNO: any;
  itemOrPartCode: any;
  instruName: any;
  description: any;
  idNo: any;
  range: any;
  hideRange: number;
  uniqueInstrumentsAvailable: Array<any> = [];
  instruNamesArray: Array<any> = [];
  uniqueInstrumentFromPo: Array<any> = [];
  instrumentFromPo: Array<any> = [];
  mi: any;
  mi1: any;
  countForInstruName: {};
  countForInstruName1: {};
  countForInstruName2: {};
  countForInstruName3: {};
  countForInstruNamePo: {};
  countForInstruNamePo1:{};

  countForInstruNameTrial1: {};
  countForInstruName1Edit: {};
  countForInstruNameEdit: {};
  requestNumberCollectionArrayForInvoice: Array<any> = [];
  onlyRequestNumberCollectionArrayForInvoice: Array<any> = [];
  allrequestNumbersDataForCalibrationTag: Array<any> = [];
  requestNumbersArray: Array<any> = [];
  requestNumbersCollectionArray: Array<any> = [];
  //selectedItems = [];
  dropdownSettings = {};
  dropdownList = [];
  dropdownList1 = [];
  countForRequestNumbers: {};
  countForRequestNumbers1: {};
  uniqueRequestNumbersAvailable: Array<any> = [];
  onlyRequestAndInwardNumberArray: Array<any> = [];
  selectedRequestNumberFromCreateInvoiceScreen: { itemId: number; itemText: string; }[];
  selectedItems: Array<{ itemId: number, itemText: string }> = [];
  
  finalDeliChallanTagArray: Array<{ requestNo: string, instruName: string, reqDocId: string, dcDocNo: string, srNoDcId: number, dcUniqueNo: string }> = [];
 
  finalInvoiceTagArray1: string;
  finalInvoiceTagArrayForRange: Array<{ requestNo: string, instruName: string, rangeFrom: string, rangeTo: string, reqDocId: string, invoiceDocNo: string, invoiceId: number, invUniqueNo: string }> = [];
  finalInvoiceTagArray1ForRange: string;
  finalInvoiceTagArrayForReqDocNoAndInvDocNo: Array<{invoiceDocNo:string,reqDocNo:string}>= [];
  finalInvoiceTagArrayForReqDocNoAndInvDocNo1: string;
  ct: number = 0;
  trialArray: Array<any> = [];
  arrayOfInstruNameAndRange: Array<any> = [];
  countForInstruNameAndRange: {};
  countForInstruNameAndRange1: Array<any> = [];
  countForInstruNameAndRange2: {};
  countForInstruNameAndRange3: Array<any> = [];
  arrayOfInstruNameAndRangeTrial: Array<any> = [];


  mii: number;
  instruNameWithRangeArray: Array<any> = [];
  instruNameWithRangeArray1: Array<any> = [];
  rangeFromArray: Array<any> = [];
  rangeToArray: Array<any> = [];
  savedRequestNumbersInInvoice: any;
  array3: Array<any> = [];
  hideHsn: number;
  printTermsId: any;
  docT: string;
  removeData: any;
  manDocForColor: any;
  onlyRangeFromArray: Array<any> = [];
  onlyRangeToArray: Array<any> = [];
  property: number = 0;
  instruNamesArrayForEdit: Array<any> = [];
  uniqueInstrumentsAvailableEdit: Array<any> = [];
  trialArrayTosetInvoiceTagToOne: string;
  trailInvoiceTagData: string[];
  trialInvoiceArray: Array<any> = [];
  rangeToArrayEdit: Array<any> = [];
  rangeFromArrayEdit: Array<any> = [];
  instruNameWithRangeArray1Edit: Array<any> = [];
  instruNameWithRangeArrayEdit: Array<any> = [];
  reqDocId: any;
  invoiceTagSetToZeroForReqNoAndInvNo: any;
  hideSaveColumn: any=0;
  back: number;
  showMessage: number=0;
  checkifidselected: any=0;
  // checkQuantityArrayTrial: any;
  // checkAmountArrayTrial: any;
 // checkAmountArrayTrial:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
 // checkQuantityArrayTrial :Array<{instruName:string,docTypePO:string,remQuantity:any,srNoTableId:any,rangeFromPo:any,rangeToPo:any,ratePo:any}>=[];
  remainingQuantity: number;
   jj:number;
  displayRate: number=1;
  instrumentNameForPo: any;
  trialSetFlag: any=1;
  trialSetFlag1: number=1;
  trialremainingQuantityCheck:Array<any> = [];
  trialremainingQuantityCheckForRange:Array<any> = [];
  name1: number;
  dispatchMode: string;
  modeOfSubmission: string;
 
  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private createInvoiceService: CreateInvoiceService, private searchInvoiceService: SearchInvoiceService, private createCustomerService: CreateCustomerService, private createUserService: CreateUserService, private createSalesService: CreateSalesService, public datepipe: DatePipe,private createCustPoService : CreateCustPoService,public createDeliChallanService:CreateDeliChallanService) {

    this.manageDocumentForm = this.formBuilder.group({
      documentType: [],
      documentDate: [],
      documentNumber: [],
      requestNumber: [],
      customerDcNumber: [],
      archieveDate: [],
      status: [],
      invoiceTypeOfDoc: [],
      remark: [],
      createdBy: [],
      selectedItems: []

    });

    // this.editQuantityForm = this.formBuilder.group({
    //   selectedRequestNumberEdit:[]

    // })
    this.reqdocParaDetailsForm = this.formBuilder.group({

      Rows: this.formBuilder.array([this.initRows()])

    });
  }
  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  dynamicArrayTrial1: Array<DynamicGrid> = [];
  dynamicArrayTrial2: any = [{}];

  dynamicArrayForUniqueNo: Array<DynamicGrid> = [];
  dynamicArrayTrialForUniqueNo: Array<DynamicGrid> = [];


  newDynamic: any = {};
  newDynamic1: any = [{}];
  arrayTosetInvoiceTagToOne: Array<any> = [];
  arrayTosetInvoiceTagToOneTrial: Array<any> = [];
  countForInstruNameTrial: {};
  arrayOfInvoiceTagForReqNo: Array<any> = [];
  arryOfInvoiceTagForInstruName: Array<any> = [];
  srNoInvoiceDocArray: any;
  instruNamesArrayForTrial: Array<any> = [];
  disableTextbox =  false;
  showTotalQuantMsg:any=1;
  showPoMessage:any=1;
 // rateArray:Array<any> = [];
  rateArray1:Array<any> = [];

 
  
  ngOnInit() {

    // $('#openModal1').modal({
    //   backdrop: 'static',
    //   keyboard: false
    // })
   this.name1=100;

    this.customerIdForChallan = this.createDeliChallanService.getCustomerNameForDeliveryChallan();
    this.customerIdForChallan = JSON.parse(localStorage.getItem('custIdForDeliInvoice'))

    this.createChallanCumInvoiceObj = this.createDeliChallanService.getCreateDeliveryChallanData();
    this.createChallanCumInvoiceObj = JSON.parse(localStorage.getItem('createDeliveryChallanData'));

  //  this.createChallanCumInvoiceObjForColor = this.createInvoiceService.getCreateChallanCumInvoiceDocument();
   // this.createChallanCumInvoiceObjForColor = JSON.parse(localStorage.getItem('challan1'));

  //  localStorage.setItem('InvFinalDocumentNumber',JSON.stringify(this.finalDocumentNumber));
    this.documentNumber= JSON.parse(localStorage.getItem('finalDocumentNumberForDeliveryChallan'));
    console.log("final document no: "+this.documentNumber)

   // localStorage.setItem('checkQuantityArray',JSON.stringify(this.checkQuantityArray));
   // localStorage.setItem('checkAmountArray',JSON.stringify(this.checkAmountArray));
   
  // this.checkQuantityArrayTrial= JSON.parse(localStorage.getItem('checkQuantityArrayForDeliveryChallan'));
   // this.checkAmountArrayTrial= JSON.parse(localStorage.getItem('checkAmountArrayForDeliveryChallan'));
   // console.log("checkQuantityArrayTrial"+ this.checkQuantityArrayTrial);


    this.selectedItems = [

    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
    };

    //   (this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('unitPrice').valueChanges.subscribe(x => {
    //     console.log('firstname value changed')
    //     console.log(x)
    //  })



    // code for button color change when fields change
    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial.length != 0 && values.length != 0 ) {

        for (var i = 0, len = values.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].instruName != null && typeof this.dynamicArrayTrial[i].instruName !='undefined' && values[i].unitPrice!=null && values[i].amount!=null && values[i].srNo!=null && values[i].poSrNO!=null && values[i].itemOrPartCode!=null && values[i].instruName!=null && values[i].sacCode!=null && values[i].quantity!=null && values[i].editQuant!=null) {

            if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].itemOrPartCode !== values[i].itemOrPartCode || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
              this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].hsnNo !== values[i].hsnNo || this.dynamicArrayTrial[i].range !== values[i].range || this.dynamicArrayTrial[i].quantity !== values[i].quantity || this.dynamicArrayTrial[i].editQuant !== values[i].editQuant
              )
            ) {
              if(this.documentType!="UCSPL INVOICE BY ID" || this.docT!="UCSPL INVOICE BY ID"){
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
               document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";//uncomment when problem arise
              }

            }
            // else{
            //   document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
            // }
          }
        
        }
      }
    })


    // (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
    //   console.log(values);

    //   if (this.dynamicArrayTrial.length != 0) {

    //     for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
    //       if (this.dynamicArrayTrial[i].instruName != null) {
    //         if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
    //           this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].accrediation !== values[i].accrediation || this.dynamicArrayTrial[i].range !== values[i].range || this.dynamicArrayTrial[i].quantity !== values[i].quantity
    //           || this.dynamicArrayTrial[i].rate !== values[i].rate || this.dynamicArrayTrial[i].discountOnItem !== values[i].discountOnItem || this.dynamicArrayTrial[i].amount !== values[i].amount)
    //         ) {
    //           console.log("change" + i);
    //           var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
    //           document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";

    //         }
    //       }
    //     }
    //   }
    // });

    //code when field value of manage document change and color of button
    // this.manDocForColor.archieveDate !== values.archieveDate ||

    this.manageDocumentForm.valueChanges.subscribe(values => {
      console.log(values);

      if (typeof this.createChallanCumInvoiceObjForColor != 'undefined' && this.createChallanCumInvoiceObjForColor != null) {


        if ((this.createChallanCumInvoiceObjForColor.documentType !== values.documentType ||
          this.createChallanCumInvoiceObjForColor.invoiceType !== values.invoiceTypeOfDoc || this.createChallanCumInvoiceObjForColor.remark !== values.remark
          || this.createChallanCumInvoiceObjForColor.createdBy !== values.createdBy || this.createChallanCumInvoiceObjForColor.status !== values.status)
        ) {
          // console.log("change" + change);
          // var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
          // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
        }
        else if ((this.createChallanCumInvoiceObjForColor.documentType == values.documentType ||
          this.createChallanCumInvoiceObjForColor.invoiceType == values.invoiceTypeOfDoc || this.createChallanCumInvoiceObjForColor.remark == values.remark
          || this.createChallanCumInvoiceObjForColor.createdBy == values.createdBy || this.createChallanCumInvoiceObjForColor.status == values.status)
        ) {
          // document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";
        }
        else {

        }
      }
    })


    this.newDynamic = {
      uniqueId: "",
      uniqueNo: "",
      srNo: "",
    //  poSrNO: "",
      itemOrPartCode: "",
      instruName: "",
      description: "",
      idNo: "",
      accrediation: "",
      range: "",
      sacCode: "",
      hsnNo: "",
      quantity: "",
      editQuant: "",
      rate: "",
      discountOnItem: "",
    //  unitPrice: "",
      fullReqNo: "",
      inwReqNo: "",
    };
    this.dynamicArray.push(this.newDynamic);


    //get instrument list
    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));

    //get document type list
    this.createInvoiceService.getDocumentTypeListForInvoice().subscribe(data => {
      this.documentTypeObjectData = data;
      console.log(this.documentTypeObjectData);

      for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
        this.documentTypeName.push(this.documentTypeObjectData[i].documentTypeName);
        this.documentTypeIds.push(this.documentTypeObjectData[i].documentTypeId);

      }
    },
      error => console.log(error));

    //get invoice type list
    this.createInvoiceService.getInvoiceTypeListForInvoice().subscribe(data => {
      this.invoiceTypeObjectData = data;
      console.log(this.invoiceTypeObjectData);

      for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {
        this.invoiceTypeName.push(this.invoiceTypeObjectData[i].invoiceTypeName);
        this.invoiceTypeIds.push(this.invoiceTypeObjectData[i].invoiceTypeId);

      }
    },
      error => console.log(error));

    //get branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branch1 = data;
      console.log(this.branch1);
      for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
        this.branchesName.push(this.branch1[i].branchName);
        this.branchesIds.push(this.branch1[i].branchId);
        this.branchesDescription.push(this.branch1[i].description);
        this.branchWithDescription.push({ description: this.branch1[i].description, branch: this.branch1[i].branchName });
      }
    },
      error => console.log(error));


    //  this.GetDeliveryChallanInformation();
    $(function () {
      // on init
      $(".table-hideable .hide-col").each(HideColumnIndex);

      // on click
      $('.hide-column').click(HideColumnIndex)
  
      function HideColumnIndex() {
        var $el = $(this);
        var $cell = $el.closest('th,td')
        var $table = $cell.closest('table')

        // get cell location - https://stackoverflow.com/a/4999018/1366033
        var colIndex = $cell[0].cellIndex + 1;

        // find and hide col index
        $table.find("tbody tr, thead tr")
          .children(":nth-child(" + colIndex + ")")
          .addClass('hide-col');

        // show restore footer
        $table.find(".footer-restore-columns").show()
       
      }

      // restore columns footer
      $(".restore-columns").click(function (e) {
        var $table = $(this).closest('table')
        $table.find(".footer-restore-columns").hide()
        $table.find("th, td")
          .removeClass('hide-col');

      })

    })


    console.log("requestAndInwardDataArray" + this.requestAndInwardDataArray);
    this.requestAndInwardDocumentNumber = this.requestAndInwardDataArray[0];
    // this.requestNumber=this.requestAndInwardDataArray[1];
    // this.customerDcNumber=this.requestAndInwardDataArray[2];
    this.customerIdForChallan = this.createDeliChallanService.getCustomerNameForDeliveryChallan();
    this.customerIdForChallan = JSON.parse(localStorage.getItem('custIdForDeliInvoice'))

    this.createDeliChallanService.getReqAndInwDataWithCalibrationDcAndInvoiceTagForCustId(this.customerIdForChallan).subscribe(data => {
      this.requestNumbersArray = data;
      this.requestNumbersArray = JSON.parse(localStorage.getItem('requestNumbersArrayForDeliveryChallan'));
      console.log("requestNumbersArray" + this.requestNumbersArray);
      for (var i = 0; i < this.requestNumbersArray.length; i++) {
        this.onlyRequestAndInwardNumberArray.push(this.requestNumbersArray[i][2]);
      }

      //code for unique request numbers
      var filter = function (value, index) {
        return this.indexOf(value) == index
      };
      this.uniqueRequestNumbersAvailable = this.onlyRequestAndInwardNumberArray.filter(filter, this.onlyRequestAndInwardNumberArray);
      console.log(this.uniqueRequestNumbersAvailable);


      for (var i = 0; i < this.uniqueRequestNumbersAvailable.length; i++) {
        this.dropdownList1.push({
          itemId: i + 1, itemText: this.uniqueRequestNumbersAvailable[i]

        })

      }
     
     // localStorage.setItem('requestNumbersArray',JSON.stringify(this.requestNumbersArray));
      this.dropdownList = this.dropdownList1;

      // this.requestAndInwardDataArray=JSON.parse(localStorage.getItem('RequestAndInwardArrayForInvoice'))
      this.requestNumberCollectionArrayForInvoice = JSON.parse(localStorage.getItem('requestNumbersCollectionArrayForDeliveryChallan'));

      for (var j = 0; j < this.dropdownList.length; j++) {
        for (var i = 0; i < this.requestNumberCollectionArrayForInvoice.length; i++) {
          if (this.requestNumberCollectionArrayForInvoice[i].itemId == this.dropdownList[j].itemId)
            this.selectedItems.push({ itemId: this.dropdownList[j].itemId, itemText: this.dropdownList[j].itemText });


        }
      }
      this.selectedRequestNumberFromCreateInvoiceScreen = this.selectedItems;
      for (var i = 0; i < this.selectedRequestNumberFromCreateInvoiceScreen.length; i++) {
        this.requestNumbersCollectionArray.push(this.selectedRequestNumberFromCreateInvoiceScreen[i]);

      }
      this.getSrNoData();
    })


    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customer1 = data;
      console.log(this.customer1);
   

      CreateDelivChallandocComponent.customerNameLength = Object.keys(this.customer1).length;
      for (var i = 0, l = Object.keys(this.customer1).length; i < l; i++) {
        this.customers.push(this.customer1[i].name + " / " + this.customer1[i].department);
        console.log("customers : " + this.customers);
        CreateDelivChallandocComponent.customerNames.push(this.customer1[i].name);
        CreateDelivChallandocComponent.customerDepartments.push(this.customer1[i].department);


        this.trialCustomerWithAddress.push({ customerName: this.customer1[i].name, name: this.customer1[i].name + " / " + this.customer1[i].department, address: this.customer1[i].addressLine1 + "," + this.customer1[i].addressLine2 + "," + this.customer1[i].city + "," + this.customer1[i].pin + "," + this.customer1[i].state + "," + this.customer1[i].country });

        this.customersIds.push(this.customer1[i].id);

        console.log("customer id : " + this.customersIds);
      }
      if (this.customersIds.length == Object.keys(this.customer1).length)
        this.getDeliveryChallanInformation();
    },
      error => console.log(error));



  }// end of ngOnInit


  onItemSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    console.log(item);
    // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.requestNumbersCollectionArray.push(item);
   localStorage.setItem('requestNumbersCollectionArrayForDeliveryChallan',JSON.stringify(this.requestNumbersCollectionArray));


   this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.push({invoiceDocNo:this.originalDocNo,reqDocNo:item.itemText});
   this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1 = JSON.stringify(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo);


  //  this.createInvoiceService.removeAllSrNoForInvDocNumber(this.originalDocNo).subscribe((res) => {
  //    this.removeData = res;
  //    this.docT = this.documentType;


  //    this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data=>{

  //      this.invoiceTagSetToZeroForReqNoAndInvNo=data;
  //      alert("set invoice tag to 0")
  //      console.log("set invoice tag to 0")
  //    })



  //  })
 // this.removeAllData();

 this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
  this.removeData = res;
  this.docT = this.documentType;


  this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
    
    

  })
})

    //this.getSrNoData();
  }

  onSelectAll(items: any) {
    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    for (var j = 0; j < items.length; j++) {
      this.requestNumbersCollectionArray.push(items[j]);

    }
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
    // this.getSrNoData();
    //this.DocTypeFunForGrouping();
   // this.removeAllData();
   this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
    this.removeData = res;
    this.docT = this.documentType;


    this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
      
      

    })
  })
    localStorage.setItem('requestNumbersCollectionArrayForDeliveryChallan',JSON.stringify(this.requestNumbersCollectionArray));
  }

  onDeselectAll(items: any) {
    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.documentNumber).subscribe((res) => {
      this.printTermsId = res;
    })
    var length = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    if (length >= 1) {
      while (length >= 0) {
        length--;
        this.formArr.removeAt(length);

      }
      this.addNewRow();

    }
   
   // this.removeAllData();
   this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
    this.removeData = res;
    this.docT = this.documentType;


    this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
      
      

    })
  })
   localStorage.setItem('requestNumbersCollectionArrayForDeliveryChallan',JSON.stringify(this.requestNumbersCollectionArray));
  }

  onItemDeSelect(item: any) {
    // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";

    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.splice(0,this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.length);

    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {
      if (item.itemId == this.requestNumbersCollectionArray[i].itemId) {

        // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length); 
        var trial = [this.requestNumbersCollectionArray[i].itemId];
        this.requestNumbersCollectionArray = this.requestNumbersCollectionArray.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.requestNumbersCollectionArray);

      }

    }
    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo.push({invoiceDocNo:this.originalDocNo,reqDocNo:item.itemText});
    this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1 = JSON.stringify(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo);


    // this.createInvoiceService.removeAllSrNoForInvDocNumber(this.originalDocNo).subscribe((res) => {
    //   this.removeData = res;
    //   this.docT = this.documentType;


    //   this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data=>{

    //     this.invoiceTagSetToZeroForReqNoAndInvNo=data;
    //     alert("set invoice tag to 0")
    //     console.log("set invoice tag to 0")
    //   })



    // })



  //  this.removeAllData();
  this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
    this.removeData = res;
    this.docT = this.documentType;


    this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
      
      

    })
  })



    // this.createInvoiceService.setInvoiceTagToZeroForRequestNoAndInvNo(this.finalInvoiceTagArrayForReqDocNoAndInvDocNo1).subscribe(data=>{

    //   this.invoiceTagSetToZeroForReqNoAndInvNo=data;
    //   alert("set invoice tag to 0")
    //   console.log("set invoice tag to 0")
    // })
    //  this.getSrNoData();
   localStorage.setItem('requestNumbersCollectionArrayForDeliveryChallan',JSON.stringify(this.requestNumbersCollectionArray));




  }



  getSrNoData() {

    this.onlyRequestNumberCollectionArrayForInvoice.splice(0, this.onlyRequestNumberCollectionArrayForInvoice.length);
    this.instruNamesArray.splice(0, this.instruNamesArray.length);
    this.instruNamesArrayForTrial.splice(0, this.instruNamesArrayForTrial.length);
    this.arrayOfInstruNameAndRange.splice(0, this.arrayOfInstruNameAndRange.length);
    this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length);

    this.allrequestNumbersDataForCalibrationTag.splice(0, this.allrequestNumbersDataForCalibrationTag.length);
    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {
      this.onlyRequestNumberCollectionArrayForInvoice.push(this.requestNumbersCollectionArray[i].itemText);
      console.log(this.onlyRequestNumberCollectionArrayForInvoice);
    }


    //trial below code

    this.createDeliChallanService.trialgetReqAndInwDataWithCalibrationDcAndInvoiceTagForReqDocNo(this.onlyRequestNumberCollectionArrayForInvoice).subscribe(data => {
      this.redDataForCalibrationTag = data;
      this.getDataForInvoice();
    })
  }

  findOcc(arr, key, key1, key2) {
    let arr2 = [];

    arr.forEach((x) => {

      // Checking if there is any object in arr2
      // which contains the key value
      if (arr2.some((val) => { return val[key] == x[key] && val[key1] == x[key1] && val[key2] == x[key2] })) {

        // If yes! then increase the occurrence by 1
        arr2.forEach((k) => {
          if (k[key] === x[key] && k[key1] === x[key1] && k[key2] === x[key2]) {
            k["occurrence"]++
          }
        })

      } else {
        // If not! Then create a new object initialize 
        // it with the present iteration key's value and 
        // set the occurrence to 1
        let a = {}
        a[key] = x[key]
        a[key1] = x[key1]
        a[key2] = x[key2]
        a["occurrence"] = 1
        arr2.push(a);
      }
    })

    return arr2;
  }


  updateData1(){

    if(this.documentType=='UCSPL INVOICE BY ID'){
   //  this.checkifidselected=1;
    }

    if(this.documentType!='UCSPL INVOICE BY ID'){
      this.updateData()
    }
  }


  updateData() {

   // this.trialfunc()

    if (this.docT != null && typeof this.docT != 'undefined'){

   if (this.documentType == "UCSPL INVOICE BY ID") {
      this.property = 1;
      this.redDataForCalibrationTag.splice(0, this.redDataForCalibrationTag.length)
      this.instruNamesArrayForTrial.splice(0,  this.instruNamesArrayForTrial.length)
      this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length)
      this.instruNamesArray.splice(0, this.instruNamesArray.length);
      this.instruNamesArrayForTrial.splice(0, this.instruNamesArrayForTrial.length);
      this.arrayOfInstruNameAndRange.splice(0, this.arrayOfInstruNameAndRange.length);
      this.arrayOfInstruNameAndRangeTrial.splice(0, this.arrayOfInstruNameAndRangeTrial.length);
    //  this.groupByIdInvoiceTagArray.splice(0, this.groupByIdInvoiceTagArray.length);
      this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
      this.newDynamic1.splice(0, this.newDynamic1.length);

     // this. getDataForInvoice();
     this.getSrNoData();
      //this.docT=this.documentType;
    }
    else{
      this.getSrNoData();  
     // this.docT=this.documentType; 
    }
  
}
    // window.location.reload();
                 // comment this 
    this.onSubmit();



  }

  getDataForInvoice() {

 
    this.createDeliChallanService.sendDocNoForGettingDeliveryChallanDataWithInvoiceTag(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
      this.challanCumInvoiceArrayForSearchInvoice = data;



      if (typeof this.challanCumInvoiceArrayForSearchInvoice != 'undefined' && this.challanCumInvoiceArrayForSearchInvoice != null) {
        if (this.challanCumInvoiceArrayForSearchInvoice.length != 0) {
          for (var z = 0; z < this.challanCumInvoiceArrayForSearchInvoice.length; z++) {
            this.redDataForCalibrationTag.push(this.challanCumInvoiceArrayForSearchInvoice[z]);
            this.instruNamesArrayForTrial.push(this.challanCumInvoiceArrayForSearchInvoice[z][7]);
            this.arrayOfInstruNameAndRangeTrial.push(this.challanCumInvoiceArrayForSearchInvoice[z][7] + "_" + this.challanCumInvoiceArrayForSearchInvoice[z][14] + "_" + this.challanCumInvoiceArrayForSearchInvoice[z][15]);
          }
        }
      }

      this.arrayTosetInvoiceTagToOne.splice(0, this.arrayTosetInvoiceTagToOne.length);
      this.arrayTosetInvoiceTagToOneTrial.splice(0, this.arrayTosetInvoiceTagToOneTrial.length);
      console.log("redDataForCalibrationTag" + this.redDataForCalibrationTag);
      for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {

        this.allrequestNumbersDataForCalibrationTag.push(this.redDataForCalibrationTag[i]);
        console.log("allrequestNumbersDataForCalibrationTag" + this.allrequestNumbersDataForCalibrationTag);
        this.instruNamesArray.push(this.redDataForCalibrationTag[i][7]);
        var reqDocumentId = this.redDataForCalibrationTag[i][0].toString();
        this.reqDocId = this.redDataForCalibrationTag[i][0];
        //var reqDocumentId1=reqDocumentId.toString();
        this.arrayTosetInvoiceTagToOne.push({ instruName: this.redDataForCalibrationTag[i][7], reqNo: this.redDataForCalibrationTag[i][2], reqDocId: reqDocumentId, quantity: 1, rangeFrom: this.redDataForCalibrationTag[i][14], rangeTo: this.redDataForCalibrationTag[i][15], invUniqueNo: this.redDataForCalibrationTag[i][33] });


      

      }
      this.trialInvoiceArray = this.arrayTosetInvoiceTagToOne;
      let key = "instruName";
      let key1 = "reqNo";
      let key2 = "reqDocId"

      //  this.trialInvoiceArray=this.findOcc(this.arrayTosetInvoiceTagToOne,key,key1,key2);


      for (var i = 0; i < this.redDataForCalibrationTag.length; i++) {
        this.arrayOfInstruNameAndRange.push(this.redDataForCalibrationTag[i][7] + "_" + this.redDataForCalibrationTag[i][14] + "_" + this.redDataForCalibrationTag[i][15]);
        console.log("arrayOfInstruNameAndRange" + this.arrayOfInstruNameAndRange);
        this.onlyRangeFromArray.push(this.redDataForCalibrationTag[i][14]);
        this.onlyRangeToArray.push(this.redDataForCalibrationTag[i][15]);
      }



      if (this.property == 1) {
        this.dynamicArrayTrial2.splice(0, this.dynamicArrayTrial2.length);
        this.dynamicArrayTrial1.splice(0, this.dynamicArrayTrial1.length);
        this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
        this.newDynamic1.splice(0, this.newDynamic1.length);
        // this.getPoData();

      }

     this.getPoData();
  //  this.DocTypeFunForGrouping();
    
    })

  }

  //////////////////////////////code for button color change////////////////////////


  get formArr() {
    return this.reqdocParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({

      srNo: [],
      poSrNO: [],
      itemOrPartCode: [],
      instruName: [],
      description: [],
      range: [],
      rangeFrom: [],
      rangeTo: [],
      sacCode: [],
      hsnNo: [],
      quantity: [],
      editQuant: [],
     // unitPrice: [],
      uniqueNo: [],


    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow1(index: number) {
    this.srNoToDelete = index + 1;

    for (var i = 0; i < this.challanCumInvoiceDocumentVariableArray.length; i++) {
      if (this.challanCumInvoiceDocumentVariableArray[i][3] == this.srNoToDelete) {
        this.indexToDelete = this.challanCumInvoiceDocumentVariableArray[i][0];
        this.createInvoiceService.deleteRowForInvoiceSrNo(this.indexToDelete).subscribe(data => {
          console.log("*********deleted row********" + data);
        })
        break;
      }
    }

    this.dynamicArrayTrial.splice(index, 1);
    this.challanCumInvoiceDocumentVariableArray.splice(index, 1);
    this.formArr.removeAt(index);

   

  }

  ngAfterViewInit() {
    this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial.length != 0) {
        if (this.dynamicArrayTrial[l].instruName != null) {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        break;
      }
    }
  }

  //////////////////////////////////// Customer change ////////////////////////////////////////

  custNameChange(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedCustNameTrial == this.customers[s]) {

        this.custNameTrialShippedTo = this.customer1[s].name;
        this.shippedCustDepartment = this.customer1[s].department;
        this.shippedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.shippedCustPanNo = this.customer1[s].panNo;
        this.shippedCustGstNo = this.customer1[s].gstNo;
        this.shippedCustAddressline1 = this.customer1[s].addressLine1;
        this.shippedCustAddressline2 = this.customer1[s].addressLine2;
        this.shippedCustCountry = this.customer1[s].country;
        this.shippedCustState = this.customer1[s].state;
        this.shippedCustCity = this.customer1[s].city;
        this.shippedCustPin = this.customer1[s].pin;
        break;
      }
    }

  }


  /////////////////////////////////////////////Get information from create-deli-chn-component/////////////////////

  getDeliveryChallanInformation() {

    this.getInformation();
  }

  getInformation() {

    this.dateOfInvoice = this.createChallanCumInvoiceObj.invoiceDate;
    this.documentNumber = this.createChallanCumInvoiceObj.dcDocumentNumber;
    this.poNo = this.createChallanCumInvoiceObj.poNo;
    this.dcNo = this.createChallanCumInvoiceObj.dcNo;
    this.requestNumber = this.createChallanCumInvoiceObj.requestNumber;
    this.createdBy = this.createChallanCumInvoiceObj.createdBy;

    for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {

      if (this.invoiceTypeObjectData[i].invoiceTypeId == this.createChallanCumInvoiceObj.invoiceTypeId) {
        this.invoiceTypeOfDoc = this.invoiceTypeObjectData[i].invoiceTypeName;
      }
    }

    if (this.invoiceTypeOfDoc == "Calibration Charges" || this.invoiceTypeOfDoc == "Calibration & Repair Charges" || this.invoiceTypeOfDoc == "Repair Charges") {
      this.hideHsn = 1;
    }
    else {
      this.hideHsn = 0;
    }

    for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
      if (this.documentTypeObjectData[i].documentTypeId == this.createChallanCumInvoiceObj.documentTypeId) {
        this.documentType = this.documentTypeObjectData[i].documentTypeName;
        this.docT= this.documentType
      }
    }

    for (var i = 0; i < Object.keys(this.customersIds).length; i++) {
      if (this.customerIdForChallan == this.customersIds[i]) {

        this.custNameTrialShippedTo = this.customer1[i].name;
        this.shippedCustDepartment = this.customer1[i].department;
        this.shippedNameAndDept = this.customer1[i].name + " / " + this.customer1[i].department;
        this.shippedCustNameTrial = this.customer1[i].name;
        this.shippedCustPanNo = this.customer1[i].panNo;
        this.shippedCustGstNo = this.customer1[i].gstNo;
        this.shippedCustAddressline1 = this.customer1[i].addressLine1;
        this.shippedCustAddressline2 = this.customer1[i].addressLine2;
        this.shippedCustCountry = this.customer1[i].country;
        this.shippedCustState = this.customer1[i].state;
        this.shippedCustCity = this.customer1[i].city;
        this.shippedCustPin = this.customer1[i].pin;

        this.custNameTrialBilledTo = this.customer1[i].name;
        this.billedCustDepartment = this.customer1[i].department;
        this.billedNameAndDept = this.customer1[i].name + " / " + this.customer1[i].department;
        this.billedCustNameTrial = this.customer1[i].name;
        this.billedCustPanNo = this.customer1[i].panNo;
        this.billedCustGstNo = this.customer1[i].gstNo;
        this.billedCustAddressline1 = this.customer1[i].addressLine1;
        this.billedCustAddressline2 = this.customer1[i].addressLine2;
        this.billedCustCountry = this.customer1[i].country;
        this.billedCustState = this.customer1[i].state;
        this.billedCustCity = this.customer1[i].city;
        this.billedCustPin = this.customer1[i].pin;
        this.cgst = this.customer1[i].cgst;
        this.sgst = this.customer1[i].sgst;
        this.igst = this.customer1[i].igst;

        this.vendorCodeNo=this.customer1[i].vendorCode;
        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }


        break;

      }

    }
    this.documentNumber = this.createChallanCumInvoiceObj.dcDocumentNumber;
    this.splitGstAndQuotationNumber();

    //  this.getPoData();

  }

  getPoData() {
    this.createInvoiceService.getPoDataForPoNo(this.poNo).subscribe(data => {
      this.poDataForInvoice = data;

      if (this.poDataForInvoice.length != 0) {
        for (var z = 0; z < this.poDataForInvoice.length; z++) {
          if (typeof this.redDataForCalibrationTag != 'undefined' && this.redDataForCalibrationTag != null) {
            for (var y = 0; y < this.redDataForCalibrationTag.length; y++) {
              if (this.poDataForInvoice[z][14] == this.redDataForCalibrationTag[y][7] ) {
                this.newDynamic1.push({
                  uniqueId: this.poDataForInvoice[z][0],
                  poSrNO: this.poDataForInvoice[z][2],
                  instruName: this.poDataForInvoice[z][14],
                  description: this.poDataForInvoice[z][4],
                  idNo: this.poDataForInvoice[z][5],
                  // range: this.redDataForCalibrationTag1[y][14],
                  rangeFrom: this.redDataForCalibrationTag[y][14],
                  rangeTo: this.redDataForCalibrationTag[y][15],
                  quantity: this.poDataForInvoice[z][7],
                //  unitPrice: this.poDataForInvoice[z][8],
                  discountOnItem: this.poDataForInvoice[z][9],
                 // amount: this.poDataForInvoice[z][11],
                  itemOrPartCode: this.poDataForInvoice[z][12],
                  hsnNo: this.poDataForInvoice[z][13],
                  fullReqNo: this.redDataForCalibrationTag[y][2],
                  inwReqNo: this.redDataForCalibrationTag[y][3],
                  reqDocId:this.redDataForCalibrationTag[y][0],
                  invoiceTag:this.redDataForCalibrationTag[y][34],

                })
              }
            }
          }
        }
        this.dynamicArray.splice(0, this.dynamicArray.length);
        this.dynamicArrayTrial.splice(0, this.dynamicArrayTrial.length);
        
        if(this.documentType=="UCSPL INVOICE BY ID"){
        for (var l = 0; l < this.newDynamic1.length; l++) {// l=0 when problem arises
       
          this.dynamicArrayTrial.push(this.newDynamic1[l]);

        
        }
      }
      else{
        for (var l = 1; l < this.newDynamic1.length; l++) {// l=0 when problem arises
          //this.convertInsruIdtoName(l);
          this.dynamicArray.push(this.newDynamic1[l]);
          this.dynamicArrayTrial.push(this.newDynamic1[l]);

        
        }

      }
        this.dynamicArrayTrial1 = $.extend(true, [], this.dynamicArrayTrial);
     
        this.DocTypeFunForGrouping();
     

      }

     

    })

  }

  convertInsruIdtoName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;
      }
    }
  }

  DocTypeFunForGrouping() {
    switch (this.documentType) {
      case "UCSPL GROUPED BY NAME":
        this.groupByName();
        break;
      case "UCSPL GROUPED BY RANGE":
        this.groupByRange();
        break;
      case "UCSPL TAX INVOICE":
        // this.normalInvoice();
        break;
      case "UCSPL INVOICE BY ID":
        this.normalInvoiceById();
        break;
    }

  }

  groupByName() {

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceDocumentVariableArrayForUniqueTrial = data;


      // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
      this.hideRange = 1;
      this.countForInstruName1 = {};
      this.countForInstruName = {};
      this.countForInstruName2 = {};
      this.countForInstruName3 = {};

      //unique insrument count
      var count = {};
      this.instruNamesArray.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
      console.log(count);
      this.countForInstruName = count;
      const mapped = Object.entries(this.countForInstruName).map(([type, value]) => ({ type, value }));
      console.log(mapped);
      this.countForInstruName1 = mapped;

      //unique insrument count for edit
      var count = {};
      this.instruNamesArrayForTrial.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
      console.log(count);
      this.countForInstruName2 = count;
      const mapped1 = Object.entries(this.countForInstruName2).map(([type, value]) => ({ type, value }));
      console.log(mapped1);
      var size = Object.keys(mapped1).length;
      this.countForInstruName3 = mapped1;

      //unique instrument name
      var filter = function (value, index) {
        return this.indexOf(value) == index
      };
      this.uniqueInstrumentsAvailable = this.instruNamesArray.filter(filter, this.instruNamesArray);
      console.log(this.uniqueInstrumentsAvailable);

      /******************************code for unique instru names from  po ***************************************************************/
      for(var z=0;z<this.poDataForInvoice.length;z++){
        this.instrumentFromPo.push(this.poDataForInvoice[z][14])
      }

      //unique instrument 
      var filter1 = function (value1, index1) {
        return this.indexOf(value1) == index1
      };
      this.uniqueInstrumentFromPo = this.instrumentFromPo.filter(filter1, this.instrumentFromPo);
      console.log(this.uniqueInstrumentFromPo);


       //unique insrument with occuerences for po
       var count2 = {};
       this.instrumentFromPo.forEach(function (i) { count2[i] = (count2[i] || 0) + 1; });
       console.log(count2);
       this.countForInstruNamePo = count2;
       const mapped2 = Object.entries(this.countForInstruNamePo).map(([type, value]) => ({ type, value }));
       console.log(mapped2);
       this.countForInstruNamePo1 = mapped2;


/*******************************************************************************************************************************/
      this.trialArray = Array.from(new Set(this.instruNamesArray));
      console.log("trialArray" + this.trialArray);

      var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

      if (leng1 == 0) {
        this.addNewRow();
      }
      if (leng1 >= 1) {
        while (leng1 >= 1) {

          leng1--;
          this.formArr.removeAt(leng1);

        }
      }

      for(var h=0;h< this.uniqueInstrumentsAvailable.length; h++){

        this.trialremainingQuantityCheck.push("0");
      }

      if (this.uniqueInstrumentsAvailable.length != 0) {

        this.addNewRow();
        for (this.mi = 0; this.mi < this.uniqueInstrumentsAvailable.length; this.mi++) {

          if (typeof ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup) != 'undefined') {

            var value = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').value;
            console.log("value is" + value);
            if (((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').value == null) {
              for (this.mii = 0; this.mii < this.uniqueInstrumentFromPo.length; this.mii++) {
                if ( this.uniqueInstrumentFromPo[this.mii]== this.uniqueInstrumentsAvailable[this.mi]) {

                 // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('poSrNO').patchValue(this.poDataForInvoice[this.mii][2]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('itemOrPartCode').patchValue(this.poDataForInvoice[this.mii][12]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('instruName').patchValue(this.uniqueInstrumentsAvailable[this.mi]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('description').patchValue(this.poDataForInvoice[this.mii][4]);
                  // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('range').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][9]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('sacCode').patchValue("00998346");
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('hsnNo').patchValue(this.poDataForInvoice[this.mii][13]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('quantity').patchValue(this.countForInstruName1[this.mi].value);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(0);


                  if (this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != null && typeof this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != undefined) {
                    for (var iii = 0; iii < this.challanCumInvoiceDocumentVariableArrayForUniqueTrial.length; iii++) {
                      if (this.poDataForInvoice[this.mii][15] == this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][6]) {
                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][15]);
                      }
                    }

                  }



                  if (this.instruNamesArrayForTrial.length == 0) {
                    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(0);
                    this.trialremainingQuantityCheck[this.mi]="0";
                  } else {
                    for (var s = 0; s < size; s++) {
                      if (this.countForInstruName3[s].type == this.uniqueInstrumentsAvailable[this.mi]) {

                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('editQuant').patchValue(this.countForInstruName3[s].value);
                        //this.trialremainingQuantityCheck.push(this.countForInstruName3[s].value);
                        if(this.countForInstruName3[s].value==0){
                          this.trialremainingQuantityCheck[this.mi]="0";
                            }
                            else{

                              this.trialremainingQuantityCheck[this.mi]=this.countForInstruName3[s].value;
                            }

                      }
                    }
                  }

                 

                 // if(this.countForInstruNamePo1[this.mii].value==1){
                  //  this.displayRate=1;
                //    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('unitPrice').patchValue(this.poDataForInvoice[this.mii][8]);
                    
                 // }
                  // else{
                  //   this.displayRate=0;
                  //  // this.rateArray.push(this.poDataForInvoice[this.mii][8]);
                  //  this.onChange(this.uniqueInstrumentsAvailable[this.mi]);
                  // }




                 
               //   document.getElementById("btn-" + this.mi).style.backgroundColor = "#5cb85c";


                  // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('amount').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][14]);
                
                  if (this.mi < this.uniqueInstrumentsAvailable.length - 1)
                    this.addNewRow();
                }
              }
            }
          }
         // document.getElementById("btn-" + this.mi).style.backgroundColor = "#5cb85c";
        }
        this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;

      }
    })
  })
  }

 


  groupByRange() {


    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceDocumentVariableArrayForUniqueTrial = data;


      // document.getElementById("btnUpdate").style.backgroundColor = "#d9534f";
      this.hideRange = 0;
      this.instruNameWithRangeArray.splice(0, this.instruNameWithRangeArray.length);
      this.instruNameWithRangeArray1.splice(0, this.instruNameWithRangeArray1.length);
      this.instruNameWithRangeArray1Edit.splice(0, this.instruNameWithRangeArray1Edit.length);
      this.instruNameWithRangeArrayEdit.splice(0, this.instruNameWithRangeArrayEdit.length);


      this.rangeFromArray.splice(0, this.rangeFromArray.length);
      this.rangeToArray.splice(0, this.rangeToArray.length);
      this.countForInstruNameAndRange1.splice(0, this.countForInstruNameAndRange1.length);
      this.countForInstruNameAndRange = {};
      this.countForInstruNameAndRange2 = {};
      this.countForInstruNameAndRange3.splice(0, this.countForInstruNameAndRange3.length);
      //unique insrument and range count
      var count = {};
      this.arrayOfInstruNameAndRange.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
      console.log(count);
      this.countForInstruNameAndRange = count;
      const mapped = Object.entries(this.countForInstruNameAndRange).map(([type, value]) => ({ type, value }));
      console.log(mapped);
      this.countForInstruNameAndRange1 = mapped;
      console.log("this.countForInstruNameAndRange1" + this.countForInstruNameAndRange1);

      //unique insrument and range count edit
      var count = {};
      this.arrayOfInstruNameAndRangeTrial.forEach(function (i) { count[i] = (count[i] || 0) + 1; });
      console.log(count);
      this.countForInstruNameAndRange2 = count;
      const mapped1 = Object.entries(this.countForInstruNameAndRange2).map(([type, value]) => ({ type, value }));
      console.log(mapped1);
      var size = Object.keys(mapped1).length;
      this.countForInstruNameAndRange3 = mapped1;
      console.log("this.countForInstruNameAndRange3" + this.countForInstruNameAndRange3);


      for (var i = 0; i < this.countForInstruNameAndRange1.length; i++) {
        this.instruNameWithRangeArray.push(this.countForInstruNameAndRange1[i].type);
      }

      for (var i = 0; i < this.instruNameWithRangeArray.length; i++) {
        let value = this.instruNameWithRangeArray[i];
        let valueRange = this.instruNameWithRangeArray[i];
        let valueRange1 = valueRange.lastIndexOf('_');
        let value1 = value.lastIndexOf('_');
        let value2 = value.slice(0, value1);
        let value22 = value2;
        let rangeToValue = valueRange.slice(valueRange1 + 1, valueRange.length);
        console.log(rangeToValue);
        this.rangeToArray.push(rangeToValue);
        console.log("rangeToArray" + this.rangeToArray);

        let value3 = value2.lastIndexOf('_');
        let value4 = value.slice(0, value3);

        let valueRange2 = value22;
        let valueRange3 = valueRange2.lastIndexOf('_');

        let rangeFromValue = valueRange2.slice(valueRange3 + 1, valueRange2.length);
        console.log(rangeFromValue);
        this.rangeFromArray.push(rangeFromValue);
        console.log("rangeFromArray" + this.rangeFromArray);

        console.log(value4);

        this.instruNameWithRangeArray1.push(value4);


      }


      //code for edit
      for (var i = 0; i < this.countForInstruNameAndRange3.length; i++) {
        this.instruNameWithRangeArrayEdit.push(this.countForInstruNameAndRange3[i].type);
      }

      for (var i = 0; i < this.instruNameWithRangeArrayEdit.length; i++) {
        let valueE = this.instruNameWithRangeArrayEdit[i];
        let valueRangeE = this.instruNameWithRangeArrayEdit[i];
        let valueRange1E = valueRangeE.lastIndexOf('_');
        let value1E = valueE.lastIndexOf('_');
        let value2E = valueE.slice(0, value1E);
        let value22E = value2E;
        let rangeToValueE = valueRangeE.slice(valueRange1E + 1, valueRangeE.length);
        console.log(rangeToValueE);
        this.rangeToArrayEdit.push(rangeToValueE);
        console.log("rangeToArrayEdit" + this.rangeToArrayEdit);

        let value3E = value2E.lastIndexOf('_');
        let value4E = valueE.slice(0, value3E);

        let valueRange2E = value22E;
        let valueRange3E = valueRange2E.lastIndexOf('_');

        let rangeFromValueEdit = valueRange2E.slice(valueRange3E + 1, valueRange2E.length);
        console.log(rangeFromValueEdit);
        this.rangeFromArrayEdit.push(rangeFromValueEdit);
        console.log("rangeFromArrayEdit" + this.rangeFromArrayEdit);

        console.log(value4E);

        this.instruNameWithRangeArray1Edit.push(value4E);

      }

      var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

      if (leng1 == 0) {
        this.addNewRow();
      }
      if (leng1 >= 1) {
        while (leng1 >= 1) {

          leng1--;
          this.formArr.removeAt(leng1);

        }
      }

      
      for(var h=0;h< this.instruNameWithRangeArray1.length; h++){

        this.trialremainingQuantityCheckForRange.push("0");
      }
      

      try{

     
      if (this.instruNameWithRangeArray1.length != 0) {

        this.addNewRow();
        for(this.mm=0; this.mm< this.instruNameWithRangeArray1.length-1;){

          if (typeof ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup) != 'undefined') {
        for (this.mi = 0; this.mi < this.instruNameWithRangeArray1.length; this.mi++) {

        //  if (typeof ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup) != 'undefined') {

            var value5 = ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').value;
            console.log("value is" + value5);
            if (((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').value == null) {
              for (this.mii = 0; this.mii < this.poDataForInvoice.length; this.mii++) {
                if (this.poDataForInvoice[this.mii][14] == this.instruNameWithRangeArray1[this.mi]
                    && this.poDataForInvoice[this.mii][18]== this.rangeFromArray[this.mi]
                    && this.poDataForInvoice[this.mii][19]== this.rangeToArray[this.mi]) {

                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('poSrNO').patchValue(this.poDataForInvoice[this.mii][2]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('itemOrPartCode').patchValue(this.poDataForInvoice[this.mii][12]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('instruName').patchValue(this.instruNameWithRangeArray1[this.mi]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('description').patchValue(this.poDataForInvoice[this.mii][4]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('rangeFrom').patchValue(this.rangeFromArray[this.mi]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('rangeTo').patchValue(this.rangeToArray[this.mi]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('sacCode').patchValue("00998346");
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('hsnNo').patchValue(this.poDataForInvoice[this.mii][13]);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('quantity').patchValue(this.countForInstruNameAndRange1[this.mi].value);
                  ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('editQuant').patchValue(0);



                  if (this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != null && typeof this.challanCumInvoiceDocumentVariableArrayForUniqueTrial != undefined) {
                    for (var iii = 0; iii < this.challanCumInvoiceDocumentVariableArrayForUniqueTrial.length; iii++) {
                      if (this.poDataForInvoice[this.mii][15] == this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][6]) {
                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentVariableArrayForUniqueTrial[iii][15]);
                      }
                    }

                  }


                  if (this.arrayOfInstruNameAndRangeTrial.length == 0) {
                    this.trialremainingQuantityCheckForRange[this.mm]="0"
                  }
                  else {
                    for (var s = 0; s < size; s++) {
                      if (this.instruNameWithRangeArrayEdit[s] == this.instruNameWithRangeArray[this.mi]) {


                        ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('editQuant').patchValue(this.countForInstruNameAndRange3[s].value);

                        if(this.countForInstruNameAndRange3[s].value==0){
                          this.trialremainingQuantityCheckForRange[this.mm]="0";
                            }
                            else{

                              this.trialremainingQuantityCheckForRange[this.mm]=this.countForInstruNameAndRange3[s].value;
                            }
                      }
                    }
                  }



         //for(var g=0; g<)
               //   ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mm) as FormGroup).get('unitPrice').patchValue(this.poDataForInvoice[this.mii][8]);
                //  document.getElementById("btn-" + this.mi).style.backgroundColor = "#5cb85c";
                  // ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(this.mi) as FormGroup).get('amount').patchValue(this.srNoNablSpecifiDetailsArrayByNablIdParaNm[this.mi][14]);
              
                  if (this.mi < this.instruNameWithRangeArray1.length - 1)
                    this.addNewRow();
                    this.mm++
                }
              }
            }

            //lets try
            if(this.mi ==this.instruNameWithRangeArray1.length-1){
              throw 'error';
            }
          }
        }
      }
        this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;

      }
    }
    catch(error){
console.log("no match");
    }

    })
  })
  }



  normalInvoiceById() {
    this.hideRange = 0;
    this.hideSaveColumn = 1;

    var leng1 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;

    if (leng1 == 0) {
      this.addNewRow();
    }
    if (leng1 >= 1) {
      while (leng1 >= 1) {

        leng1--;
        this.formArr.removeAt(leng1);

      }
    }

    if (this.dynamicArrayTrial1.length != 0) {


      /////////////////////////////trial below code ////////////////////////////
      this.dynamicArrayTrial2.splice(0, this.dynamicArrayTrial2.length);

      for (this.mi = 0; this.mi < this.dynamicArrayTrial1.length; this.mi++) {

        for (this.mi1 = 0; this.mi1 < this.poDataForInvoice.length; this.mi1++) {

          if (this.dynamicArrayTrial1[this.mi].instruName == this.poDataForInvoice[this.mi1][14] && this.dynamicArrayTrial1[this.mi].invoiceTag==1) {


            this.dynamicArrayTrial2.push({
              poSrNO: this.poDataForInvoice[this.mi1][2],
              itemOrPartCode: this.poDataForInvoice[this.mi1][12],
              instruName: this.dynamicArrayTrial1[this.mi].instruName,
              description: this.dynamicArrayTrial1[this.mi].description,
             // rangeFrom: this.onlyRangeFromArray[this.mi-1],
            //  rangeTo: this.onlyRangeToArray[this.mi-1],
            rangeFrom: this.dynamicArrayTrial1[this.mi].rangeFrom,
            rangeTo: this.dynamicArrayTrial1[this.mi].rangeTo,
              sacCode: "00998346",
              hsnNo: this.poDataForInvoice[this.mi1][13],
              quantity: 1,
              editQuant: 1,
          //    unitPrice: this.poDataForInvoice[this.mi1][8]
            })

           


          }
          else if (this.dynamicArrayTrial1[this.mi].instruName == this.poDataForInvoice[this.mi1][14] && this.dynamicArrayTrial1[this.mi].invoiceTag==0) {


              this.dynamicArrayTrial2.push({
                poSrNO: this.poDataForInvoice[this.mi1][2],
                itemOrPartCode: this.poDataForInvoice[this.mi1][12],
                instruName: this.dynamicArrayTrial1[this.mi].instruName,
                description: this.dynamicArrayTrial1[this.mi].description,
                rangeFrom: this.dynamicArrayTrial1[this.mi].rangeFrom,
            rangeTo: this.dynamicArrayTrial1[this.mi].rangeTo,
                sacCode: "00998346",
                hsnNo: this.poDataForInvoice[this.mi1][13],
                quantity: 1,
                editQuant: 0,
              //  unitPrice: this.poDataForInvoice[this.mi1][8]
              })
  
             
  
  
            
          }

        }
      }

      console.log("this.dynamicArrayTrial2 : "+ this.dynamicArrayTrial2);

      if (this.dynamicArrayTrial2 == null || typeof this.dynamicArrayTrial2 == 'undefined' || this.dynamicArrayTrial2.length == 0) {
        this.reqdocParaDetailsForm = this.formBuilder.group({
          Rows: this.formBuilder.array([this.initRows()])

        });


      } else {

        this.reqdocParaDetailsForm = this.formBuilder.group({
          Rows: this.formBuilder.array(
            this.dynamicArrayTrial2.map(({

              srNo,
              poSrNO,
              itemOrPartCode,
              instruName,
              description,
              range,
              rangeFrom,
              rangeTo,
              sacCode,
              hsnNo,
              quantity,
              editQuant,
           //   amount,
            //  unitPrice
             
            }) =>
              this.formBuilder.group({
                srNo: [srNo],
                poSrNO: [poSrNO],
                description: [description],
                itemOrPartCode: [itemOrPartCode],
                range: [range],
                rangeFrom: [rangeFrom],
                rangeTo: [rangeTo],
                sacCode: ["00998346"],
                hsnNo: [hsnNo],
                quantity: [quantity],
                editQuant: [editQuant],
                instruName: [instruName],
              //  amount: [amount],
              //  unitPrice: [unitPrice],

              })
            )
          )

        })

      }


      ////////////////////////////trial above code ////////////////////////////

      this.dynamicArrayTrial = this.reqdocParaDetailsForm.get('Rows').value;

    }
  }



  
changeDocType(){
  
  if(this.documentType=="UCSPL INVOICE BY ID"){

    this.hideSaveColumn=1;
    this.showMessage=1;
   
  }
  else{
    this.hideSaveColumn=0;
    this.showMessage=0;
  }
 
}


  getInformationYourReference() {
    this.dcDate = this.createChallanCumInvoiceObj.dcDate
    this.dcNo = this.createChallanCumInvoiceObj.dcNo
    this.createChallanCumInvoiceObj.branchId = this.createChallanCumInvoiceObj.branchId

    this.poDate = this.createChallanCumInvoiceObj.poDate
    this.poNo = this.createChallanCumInvoiceObj.poNo
    this.vendorCodeNo = this.createChallanCumInvoiceObj.vendorCodeNumber;
  }

  splitGstAndQuotationNumber() {
    if (typeof this.shippedCustGstNo != 'undefined') {
      this.stateCode = this.shippedCustGstNo.slice(0, 2);
      this.billedStateCode = this.shippedCustGstNo.slice(0, 2);
    }

    if (typeof this.createChallanCumInvoiceObj.dcDocumentNumber != 'undefined') {
      this.originalDocNo = this.createChallanCumInvoiceObj.dcDocumentNumber;
      this.invDocNumber = this.createChallanCumInvoiceObj.dcDocumentNumber.slice(15, 20);
    }

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchId == this.createChallanCumInvoiceObj.branchId)

        this.prefixInvDocNumber = this.branch1[i].description;
    }

    this.invDocNumberWithPrefix = this.prefixInvDocNumber + this.invDocNumber;

    this.salesDocumentInfo.push(this.stateCode);
    this.salesDocumentInfo.push(this.invDocNumber);

    console.log("salesDocumentInfo..." + this.salesDocumentInfo);

  }

  changeBranchName(e) {
    this.invDocNumberWithPrefix = this.prefixInvDocNumber + this.invDocNumber;

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.prefixInvDocNumber == this.branch1[i].description) {
        this.createChallanCumInvoiceObj.branchId = this.branch1[i].branchId;
        break;
      }

    }
  }

  //////////////////////////////////////////////// code for adding new row //////////////////////////////////

  addRow() {

    this.newDynamic = {
      srNo: "",
      poSrNO: "",
      itemOrPartCode: "",
      instruName: "",
      description: "",
      range: "",
      sacCode: "",
      quantity: "",
      rate: "",
      discountOnItem: "",
    //  unitPrice: "",
    //  amount: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }


  deleteRow(index) {
    if (this.dynamicArray.length == 1) {
      return false;
    } else {
      this.dynamicArray.splice(index, 1);

      return true;
    }
  }

  // ||(
  checkAmount(k) {
    $("#alertEdit1-"+k).html(" ");
    $("#alertEdit-"+k).html(" ");

    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;

   
    if (this.dynamicArray[k].editQuant > this.dynamicArray[k].quantity 
     ) {
     
      this.showTotalQuantMsg=0;
      $("#alertEdit1-"+k).html("Entered Quantity is Greater Than Total Quantity");
      document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#F07470";
      this.trialSetFlag=0;
     
    }
    else {
      $("#alertEdit1-"+k).html(" ");
     this.showTotalQuantMsg=1;
      document.getElementById("btnQuantEdit-" + k).style.backgroundColor = "#FFFFFF";
      this.trialSetFlag=1;
     
    }
  

  }
 
  ////////////////////////////////////////////  save data  /////////////////////////////////////////////  
  saveItemDetails(i) {

   
    document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
    console.log(this.reqdocParaDetailsForm.value);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('srNo').patchValue(i + 1);

    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    console.log(this.dynamicArray);

    this.challanCumInvoiceDocumentObject.deliChallanNumber = this.invDocNumberWithPrefix;
    this.challanCumInvoiceDocumentObject.deliChallanDocumentNumber = this.createChallanCumInvoiceObj.dcDocumentNumber;
    this.challanCumInvoiceDocumentObject.srNo = i + 1;
   // this.challanCumInvoiceDocumentObject.poSrNo = this.dynamicArray[i].poSrNO;
    this.challanCumInvoiceDocumentObject.itemOrPartCode = this.dynamicArray[i].itemOrPartCode;

    this.challanCumInvoiceDocumentObject.description = this.dynamicArray[i].description;
    this.challanCumInvoiceDocumentObject.rangeFrom = this.dynamicArray[i].rangeFrom;
    this.challanCumInvoiceDocumentObject.rangeTo = this.dynamicArray[i].rangeTo;
    this.challanCumInvoiceDocumentObject.sacCode = this.dynamicArray[i].sacCode;
    if (this.invoiceTypeOfDoc == "Supplier Tax Invoice Charges") {
      this.challanCumInvoiceDocumentObject.hsnNo = this.dynamicArray[i].hsnNo;
    }
    this.challanCumInvoiceDocumentObject.quantity = this.dynamicArray[i].editQuant;
  
    this.convertInstruNameToId(i);

 
    this.createDeliChallanService.getSrNoDataForDeliChallanDocumentNo(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceDocumentVariableArray = data;
      // alert("data saved succesfully");
      this.dynamicArrayTrial = this.dynamicArray;
      this.saveSQDocDataWithSrNo(i);
      document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";

    })



  }

  convertInstruNameToId(j) {
    for (let i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentName == this.dynamicArray[j].instruName) {
        this.challanCumInvoiceDocumentObject.instruId = this.instrumentList[i].instrumentId;
        break;
      }

    }
  }

   

  saveSQDocDataWithSrNo(i) {


    if (this.challanCumInvoiceDocumentVariableArray.length == 0) {
      this.createDeliChallanService.createSrNoDeliveryChallanDocument(this.challanCumInvoiceDocumentObject).subscribe(data => {
        console.log("*********data saved********" + data);
        alert("data saved successfully");
        this.srNoInvoiceDocArray = data;
        this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.deliChallanDocumentNumber + "_" + this.srNoInvoiceDocArray.id;

        this.createDeliChallanService.updateSrNoDeliveryChallanDocumentForUniqueNo(this.srNoInvoiceDocArray.id, this.challanCumInvoiceDocumentObject).subscribe(data => {

          ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentObject.uniqueNo);
          this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;


          if (this.documentType == "UCSPL GROUPED BY NAME") {
            if (this.dynamicArray[i].editQuant != 0) {
              this.setInvoiceTagToOneForInstru(i);
            }
          }
          if (this.documentType == "UCSPL GROUPED BY RANGE") {
            if (this.dynamicArray[i].editQuant != 0) {
              this.setInvoiceTagToOneForRange(i);
            }
          }

        })


      })

    }
    else {
      for (this.n = 0; this.n < this.challanCumInvoiceDocumentVariableArray.length; this.n++) {

        if (this.documentType == "UCSPL GROUPED BY NAME") {

        if (this.challanCumInvoiceDocumentVariableArray[this.n][15] == this.dynamicArray[i].uniqueNo) {
          this.createInvoiceService.updateChallanCumInvoiceDocument(this.challanCumInvoiceDocumentVariableArray[this.n][0], this.challanCumInvoiceDocumentObject).subscribe(data => {
            alert("data updated successfully");
            this.srNoInvoiceDocArray = data;
            this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
            this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

            if (this.documentType == "UCSPL GROUPED BY NAME") {
              this.setInvoiceTagToZeroForInstru(i);
            }
            if (this.documentType == "UCSPL GROUPED BY RANGE") {
              this.setInvoiceTagToZeroForRange(i);
            }

          }
          );
          break;
        }
    }

      if (this.documentType == "UCSPL GROUPED BY RANGE") {
        // if (this.challanCumInvoiceDocumentVariableArray[this.n][15] == this.dynamicArray[i].uniqueNo &&  this.challanCumInvoiceDocumentVariableArray[this.n][16] ==this.dynamicArray[i].instruName && this.challanCumInvoiceDocumentVariableArray[this.n][8] == this.dynamicArray[i].rangeFrom && this.challanCumInvoiceDocumentVariableArray[this.n][9] == this.dynamicArray[i].rangeTo   ) {
        
        
        if (this.challanCumInvoiceDocumentVariableArray[this.n][16] ==this.dynamicArray[i].instruName && this.challanCumInvoiceDocumentVariableArray[this.n][8] == this.dynamicArray[i].rangeFrom && this.challanCumInvoiceDocumentVariableArray[this.n][9] == this.dynamicArray[i].rangeTo   ) {
          this.createInvoiceService.updateChallanCumInvoiceDocument(this.challanCumInvoiceDocumentVariableArray[this.n][0], this.challanCumInvoiceDocumentObject).subscribe(data => {
            alert("data updated successfully");
            this.srNoInvoiceDocArray = data;
            this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
            this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

            if (this.documentType == "UCSPL GROUPED BY NAME") {
              this.setInvoiceTagToZeroForInstru(i);
            }
            if (this.documentType == "UCSPL GROUPED BY RANGE") {
              this.setInvoiceTagToZeroForRange(i);
            }

          }
          );
          break;
        }
      }

    }
      if (this.n >= this.challanCumInvoiceDocumentVariableArray.length) {
        this.createInvoiceService.createChallanCumInvoiceDocument(this.challanCumInvoiceDocumentObject).subscribe(data => {
          console.log("*********data saved********" + data);
          this.srNoInvoiceDocArray = data;
          this.challanCumInvoiceDocumentObject.uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + this.srNoInvoiceDocArray.id;
          this.createInvoiceService.updateChallanCumInvoiceDocumentForUniqueNo(this.srNoInvoiceDocArray.id, this.challanCumInvoiceDocumentObject).subscribe(data => {
            ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(i) as FormGroup).get('uniqueNo').patchValue(this.challanCumInvoiceDocumentObject.uniqueNo);
            this.dynamicArray[i].uniqueNo = this.challanCumInvoiceDocumentObject.uniqueNo;

            alert("data saved successfully");
            if (this.documentType == "UCSPL GROUPED BY NAME") {
              this.setInvoiceTagToOneForInstru(i);
            }
            if (this.documentType == "UCSPL GROUPED BY RANGE") {
              this.setInvoiceTagToOneForRange(i);
            }
          })

        })

      }

    }


    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceDocumentVariableArray = data;

    })


  }

  ///////////////set invoice tag for instrument
  setInvoiceTagToOneForInstru(i) {
    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", dcDocNo: "", srNoDcId: "", dcUniqueNo: "" };
    var srNoDcId = this.srNoInvoiceDocArray.id.toString();
    this.finalDeliChallanTagArray.splice(0, this.finalDeliChallanTagArray.length);
    this.finalInvoiceTagArray1 = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalDeliChallanTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, dcDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, srNoDcId: srNoDcId, dcUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalDeliChallanTagArray);
       
          count++;
        }

      }

    }
    this.createDeliChallanService.trialSetDcTagToOneForInstru(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;

      alert("invoice tag=1");
    
    })

  }

  setInvoiceTagToZeroForInstru(i) {

    var count = 0;
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalDeliChallanTagArray.splice(0, this.finalDeliChallanTagArray.length);
    // this.finalInvoiceTagArray1.splice(0,this.finalInvoiceTagArray1.length);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalDeliChallanTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, dcDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, srNoDcId: invDocId, dcUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalDeliChallanTagArray);
        
          count++;
        }
        if (this.dynamicArray[i].editQuant == 0) {
          this.finalDeliChallanTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, dcDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, srNoDcId: invDocId, dcUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalDeliChallanTagArray);
        }

      }

    }
    this.createDeliChallanService.trialSetDcTagToZeroForInstru(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;
      alert("invoice tag=0");
      if (this.dynamicArray[i].editQuant != 0) {
        //  this.setInvoiceTagToOneForInstruThroughInvoiceId(i);

        this.setInvoiceTagToOneForInstru(i);
      }
      //  window.location.reload();
    })


  }


  setInvoiceTagToOneForInstruThroughInvoiceId(i) {

    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalDeliChallanTagArray.splice(0, this.finalDeliChallanTagArray.length);
    this.finalInvoiceTagArray1 = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalDeliChallanTagArray.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, reqDocId: this.trialInvoiceArray[m].reqDocId, dcDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, srNoDcId: invDocId, dcUniqueNo: "0" });
          this.finalInvoiceTagArray1 = JSON.stringify(this.finalDeliChallanTagArray);
         
          count++;
        }

      }

    }
    this.createDeliChallanService.trialSetDcTagToOneForInstruThroughInvId(this.finalInvoiceTagArray1).subscribe(data => {
      this.trailInvoiceTagData = data;

      alert("invoice tag=1");
      //  window.location.reload();
    })

  }

  ///////////////set invoice tag for range
  setInvoiceTagToOneForRange(i) {
    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "", rangeFrom: "", rangeTo: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArrayForRange.splice(0, this.finalInvoiceTagArrayForRange.length);
    this.finalInvoiceTagArray1ForRange = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);

    if(typeof this.dynamicArrayTrial[i].uniqueNo == 'undefined')
    {
      this.dynamicArrayTrial[i].uniqueNo = this.srNoInvoiceDocArray.invDocumentNumber + "_" + invDocId;
    }

    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName &&
        this.trialInvoiceArray[m].rangeFrom == this.dynamicArrayTrial[i].rangeFrom &&
        this.trialInvoiceArray[m].rangeTo == this.dynamicArrayTrial[i].rangeTo) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
        
          count++;
        }

      }

    }
    this.createDeliChallanService.trialSetDcTagToOneForRange(this.finalInvoiceTagArray1ForRange).subscribe(data => {
      this.trailInvoiceTagData = data;
      
      alert("invoice tag=1");
      //  window.location.reload();
    })

  }

  setInvoiceTagToZeroForRange(i) {

    var count = 0;
    var invoiceTrial = { requestNo: "", instruName: "", reqDocId: "", invoiceDocNo: "", invoiceId: "", rangeFrom: "", rangeTo: "" };
    var invDocId = this.srNoInvoiceDocArray.id.toString();
    this.finalInvoiceTagArrayForRange.splice(0, this.finalInvoiceTagArrayForRange.length);
    this.finalInvoiceTagArray1ForRange = JSON.stringify(invoiceTrial);
    this.arrayOfInvoiceTagForReqNo.splice(0, this.arrayOfInvoiceTagForReqNo.length);
    this.arryOfInvoiceTagForInstruName.splice(0, this.arryOfInvoiceTagForInstruName.length);
    for (var m = 0; m < this.trialInvoiceArray.length; m++) {
      if (this.trialInvoiceArray[m].instruName == this.dynamicArrayTrial[i].instruName &&
        this.trialInvoiceArray[m].rangeFrom == this.dynamicArrayTrial[i].rangeFrom &&
        this.trialInvoiceArray[m].rangeTo == this.dynamicArrayTrial[i].rangeTo) {
        if (count < this.dynamicArray[i].editQuant) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
        
          count++;
        }
        if (this.dynamicArray[i].editQuant == 0) {
          this.finalInvoiceTagArrayForRange.push({ requestNo: this.trialInvoiceArray[m].reqNo, instruName: this.trialInvoiceArray[m].instruName, rangeFrom: this.trialInvoiceArray[m].rangeFrom, rangeTo: this.trialInvoiceArray[m].rangeTo, reqDocId: this.trialInvoiceArray[m].reqDocId, invoiceDocNo: this.createChallanCumInvoiceObj.dcDocumentNumber, invoiceId: invDocId, invUniqueNo: this.dynamicArrayTrial[i].uniqueNo });
          this.finalInvoiceTagArray1ForRange = JSON.stringify(this.finalInvoiceTagArrayForRange);
        }

      }

    }
    this.createDeliChallanService.trialSetDcTagToZeroForRange(this.finalInvoiceTagArray1ForRange).subscribe(data => {
      this.trailInvoiceTagData = data;
      alert("invoice tag=0");
      if (this.dynamicArray[i].editQuant != 0) {
      
        this.setInvoiceTagToOneForRange(i);

      }
      //  window.location.reload();
    })


  }
  saveAllData1(){
    this.reqdocParaDetailsForm.disable();
    this.clickToDisable=true;
    this.saveAllData();

  }

  saveAllData() {
    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedNameAndDept == this.customers[s]) {

        this.createChallanCumInvoiceObj1.custShippedTo = this.customer1[s].id;
        break;
      }
    }

   

    var dcDate1 = this.convertStringToDate(this.dcDate);
    this.createChallanCumInvoiceObj1.dcDate = this.datepipe.transform(dcDate1, 'dd-MM-yyyy');

    this.createChallanCumInvoiceObj1.branchId = this.createChallanCumInvoiceObj.branchId

    this.createChallanCumInvoiceObj1.dcDocumentNumber = this.createChallanCumInvoiceObj.dcDocumentNumber

    this.convertDocTypeNameToId();
    this.convertInvoiceTypeNameToId();
    this.createChallanCumInvoiceObj1.id = this.createChallanCumInvoiceObj.id

    var invoiceDate1 = this.convertStringToDate(this.dateOfInvoice);
    this.createChallanCumInvoiceObj1.invoiceDate = this.datepipe.transform(invoiceDate1, 'dd-MM-yyyy');

    // this.createChallanCumInvoiceObj1.poDate = this.datepipe.transform(this.poDate, 'dd-MM-yyyy');
    var poDate1 = this.convertStringToDate(this.poDate);
    this.createChallanCumInvoiceObj1.poDate = this.datepipe.transform(poDate1, 'dd-MM-yyyy');

    this.createChallanCumInvoiceObj1.poNo = this.poNo
    this.createChallanCumInvoiceObj1.vendorCodeNumber = this.vendorCodeNo;
    this.createChallanCumInvoiceObj1.dcNo = this.dcNo;
    this.createChallanCumInvoiceObj1.createdBy = this.createdBy;
    this.createChallanCumInvoiceObj1.archieveDate = this.archieveDate;

    this.createChallanCumInvoiceObj1.dispatchMode=this.dispatchMode;
    this.createChallanCumInvoiceObj1.modeOfSubmission=this.modeOfSubmission;
    this.createChallanCumInvoiceObj1.totalQuantity=this.name1;
  

    // code for request numbers
    this.createDeliChallanService.updateDeliveryChallan(this.createChallanCumInvoiceObj.id, this.createChallanCumInvoiceObj1).subscribe(data => {
      console.log("*********data saved********" + data);
      this.createChallanCumInvoiceObjForColor = data;
      // document.getElementById("btnUpdate").style.backgroundColor = "#5cb85c";
      this.convertStatusIdToName();

      if(this.docT!="UCSPL INVOICE BY ID" && this.documentType !="UCSPL INVOICE BY ID"){
     this.removeAllData();
    }
    //  }

    //  this.checkifidselected=0;
    })

    

    this.challanInvoiceDocCalculationObject.subTotal = this.subTotal;
    this.challanInvoiceDocCalculationObject.discountOnSubtotal = this.discOnSubTotal;
    this.challanInvoiceDocCalculationObject.total = this.total;
    this.challanInvoiceDocCalculationObject.net = parseFloat(this.netValue);
    this.challanInvoiceDocCalculationObject.invDocumentNumber = this.createChallanCumInvoiceObj.dcDocumentNumber;
    this.challanInvoiceDocCalculationObject.cgst = this.cgstAmount;
    this.challanInvoiceDocCalculationObject.igst = this.igstAmount;
    this.challanInvoiceDocCalculationObject.sgst = this.sgstAmount;


    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocumentCalculation(this.challanInvoiceDocCalculationObject.invDocumentNumber).subscribe(data => {
      console.log("*********send doc no ********" + data);
    

    this.createInvoiceService.getDocDetailsForChallanCumInvoiceDocumentCalculation().subscribe(data => {
      console.log("*********doc no details collection********" + data);
      this.challanCumInvoiceDocumentCalculationVariableArray = data;
      alert("data saved succesfully");
      this.saveInvDocCalcDetails();
     
   
    this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);
     
      this.saveAllRequestNumbers();
      

    })
  })
  })
 
  }
  convertStatusIdToName() {

    if (this.createChallanCumInvoiceObjForColor.draft == 1) {
      this.status = "Draft";
      this.createChallanCumInvoiceObjForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObjForColor.archieved == 1) {
      this.status = "Archieved";
      this.createChallanCumInvoiceObjForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObjForColor.submitted == 1) {
      this.status = "Submitted";
      this.createChallanCumInvoiceObjForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObjForColor.approved == 1) {
      this.status = "Approved";
      this.createChallanCumInvoiceObjForColor.status = this.status;
    }
    if (this.createChallanCumInvoiceObjForColor.rejected == 1) {
      this.status = "Rejected";
      this.createChallanCumInvoiceObjForColor.status = this.status;
    }

  }

  removeAllData(){
    if (this.docT != null && typeof this.docT != 'undefined' && this.docT!='UCSPL INVOICE BY ID'){
      // if(this.checkifidselected!=1){
    if (this.docT != this.documentType && this.documentType!='UCSPL INVOICE BY ID') {
      this.createInvoiceService.removeAllSrNoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
        this.removeData = res;
        this.docT = this.documentType;


        this.createInvoiceService.setInvoiceTagAndUniqueNoToZeoForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
          
          

        })
      })
    }
  }
  }

  convertStringToDate(value) {

    if (typeof value === 'string') {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);

      return dateFormat;
    }
    else {
      return value;
    }
  }

  saveAllRequestNumbers() {

    for (var i = 0; i < this.requestNumbersCollectionArray.length; i++) {

      this.invoiceAndrequestNumberJunction.invoiceDocNo = this.createChallanCumInvoiceObj.dcDocumentNumber;
      this.invoiceAndrequestNumberJunction.requestNumber = this.requestNumbersCollectionArray[i].itemText;
      this.createInvoiceService.saveReuestNumbers(this.invoiceAndrequestNumberJunction).subscribe(data => {
        console.log("request number saved successfully" + data);
        alert("request number saved successfully");
      });
      // this.functionCallForRequestNumber(i,this.createChallanCumInvoiceObj.invDocumentNumber);

    }
    var leng2 = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for(var z=0; z<leng2;z++){
      
 document.getElementById("btn-" + z).style.backgroundColor = "#5cb85c";
    }

  }

  functionCallForRequestNumber(j, value) {
    this.createInvoiceService.getAllRequestNumbersForInvoiceDocNumber(value).pipe(
      tap(data => {
        // debug error here
        console.log(data);
      })
    ).subscribe(data => {
      console.log("*********send doc no ********" + data);
      this.savedRequestNumbersInInvoice = data;
      if (this.savedRequestNumbersInInvoice.length != 0) {
        // this.distinctFunction();

      }
      else {
        // for(var j=0; j<this.savedRequestNumbersInInvoice.length;j++){
        this.createInvoiceService.saveReuestNumbers(this.invoiceAndrequestNumberJunction).subscribe(data => {
          console.log("request number saved successfully" + data);
        });

        // }
      }

    })

  }

  distinctFunction() {

    this.array3 = this.requestNumbersCollectionArray.filter(function (obj) { return this.savedRequestNumbersInInvoice.indexOf(obj) == -1; });
    console.log("array3" + this.array3);
    for (var k = 0; k < this.array3.length; k++) {
      if (this.invoiceAndrequestNumberJunction.requestNumber == this.array3[k])
        this.createInvoiceService.saveReuestNumbers(this.invoiceAndrequestNumberJunction).subscribe(data => {
          console.log("request number saved successfully" + data);
        });
    }
  }


  convertDocTypeNameToId() {
    for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
      if (this.documentTypeObjectData[i].documentTypeName == this.documentType) {
        this.createChallanCumInvoiceObj1.documentTypeId = this.documentTypeObjectData[i].documentTypeId;
      }
    }
  }

  convertInvoiceTypeNameToId() {

    for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {
      if (this.invoiceTypeObjectData[i].invoiceTypeName == this.invoiceTypeOfDoc) {
        this.createChallanCumInvoiceObj1.invoiceTypeId = this.invoiceTypeObjectData[i].invoiceTypeId;

      }
    }
  }
  saveInvDocCalcDetails() {

    if (this.challanCumInvoiceDocumentCalculationVariableArray.length == 0) {
      this.createInvoiceService.createChallanCumInvoiceDocumentCalculation(this.challanInvoiceDocCalculationObject).subscribe(data => {
        console.log("data updated successfully in ChallanCumDocumentCalculationtable" + data);
       // this.savePoValueAndQuantity();
      }
      );
    }
    else {
      this.createInvoiceService.updateChallanCumInvoiceDocumentCalculation(this.challanCumInvoiceDocumentCalculationVariableArray[0][0], this.challanInvoiceDocCalculationObject).subscribe(data => {
        console.log("data updated successfully" + data);
      // this.savePoValueAndQuantity();
      }
      );
    }
  }

  

  onSubmit() {
    //this.getSrNoData();                // comment this 
    //this.DocTypeFunForGrouping();   uncomment this 
    this.createChallanCumInvoiceObj1.vendorCodeNumber = this.vendorCodeNo;
    this.saveAllData();
    this.createChallanCumInvoiceObj1.dcDocumentNumber = this.documentNumber;

    this.createChallanCumInvoiceObj1.createdBy = this.createdBy;
    this.createChallanCumInvoiceObj1.requestNumber = this.requestNumber;
    this.createChallanCumInvoiceObj1.dispatchMode=this.dispatchMode;
    this.createChallanCumInvoiceObj1.modeOfSubmission=this.modeOfSubmission;
    this.createChallanCumInvoiceObj1.totalQuantity=this.name1;

    switch (this.status) {
      case 'Draft': this.createChallanCumInvoiceObj1.draft = 1;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Archieved': this.createChallanCumInvoiceObj1.draft = 0
        this.createChallanCumInvoiceObj1.archieved = 1;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Rejected': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 1;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Approved': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 1;
        this.createChallanCumInvoiceObj1.submitted = 0;
        break;
      case 'Submitted': this.createChallanCumInvoiceObj1.draft = 0;
        this.createChallanCumInvoiceObj1.archieved = 0;
        this.createChallanCumInvoiceObj1.rejected = 0;
        this.createChallanCumInvoiceObj1.approved = 0;
        this.createChallanCumInvoiceObj1.submitted = 1;
        break;

    }
    this.createChallanCumInvoiceObj1.remark = this.remark;

    this.createDeliChallanService.updateDeliveryChallan(this.createChallanCumInvoiceObj.id, this.createChallanCumInvoiceObj1).subscribe(data => {
      console.log(data);
    

    })

    this.createInvoiceService.removeAllRequestNumbersForInvDocNumber(this.createChallanCumInvoiceObj.dcDocumentNumber).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);
      this.saveAllRequestNumbers();

    })
    

  }
  ////////////////////////////////////// pop up screens /////////////////////////////////////////

  manageFooters() {                                                      //for footer 
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }

  manageDocument() {                                                      // for document

    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()
  }

  manageHeaders() {                                                      // for header

    const buttonModal2 = document.getElementById("openModalButton2")
    console.log('buttonModal2', buttonModal2)
    buttonModal2.click()
  }

  editQuantity() {                                                      // for document

    const buttonModal12 = document.getElementById("openModalButton12")
    console.log('buttonModal12', buttonModal12)
    buttonModal12.click()
  }

  printPage() {
    window.print();
  }


  toggle(e) {
    console.log(e);
    this.selectTagEvent = e;

    if (this.selectTagEvent == 4) {
      this.discOnSubTotalForShow = 1;
    }

  }

  toggleHeader(e) {
    console.log(e);
    this.selectTagEventForHeader = e;
  }


 



  getHeaderInfoForSearchInvoice() {
    this.searchInvoiceService.sendDocNoForGettingCreateInvDetails(this.documentNumberFromSearchInvoice).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })

    this.searchInvoiceService.getCreateInvsDetailsForSearchInv().subscribe(data => {
      console.log("********* header information ********" + data);
      this.createSalesInfoForSearchInvoiceObject = data;

      if (typeof this.createSalesInfoForSearchInvoiceObject != 'undefined') {
        this.createChallanCumInvoiceObj.id = this.createSalesInfoForSearchInvoiceObject[0][0];
        this.createChallanCumInvoiceObj.branchId = this.createSalesInfoForSearchInvoiceObject[0][3];
        this.createChallanCumInvoiceObj.custShippedTo = this.createSalesInfoForSearchInvoiceObject[0][6];
     //   this.createChallanCumInvoiceObj.custBilledTo = this.createSalesInfoForSearchInvoiceObject[0][7];
        this.createChallanCumInvoiceObj.dateOfInvoice = this.createSalesInfoForSearchInvoiceObject[0][8];
        this.createChallanCumInvoiceObj.poDate = this.createSalesInfoForSearchInvoiceObject[0][9];
        this.createChallanCumInvoiceObj.dcDate = this.createSalesInfoForSearchInvoiceObject[0][10];
        this.createChallanCumInvoiceObj.poNo = this.createSalesInfoForSearchInvoiceObject[0][11];
        this.createChallanCumInvoiceObj.dcNo = this.createSalesInfoForSearchInvoiceObject[0][12];
        this.createChallanCumInvoiceObj.vendorCodeNumber = this.createSalesInfoForSearchInvoiceObject[0][13];
        this.createChallanCumInvoiceObj.archieveDate = this.createSalesInfoForSearchInvoiceObject[0][14];
        this.createChallanCumInvoiceObj.createdBy = this.createSalesInfoForSearchInvoiceObject[0][15];
        this.createChallanCumInvoiceObj.remark = this.createSalesInfoForSearchInvoiceObject[0][17];
        this.createChallanCumInvoiceObj.dcDocumentNumber = this.createSalesInfoForSearchInvoiceObject[0][18];

        this.id = this.createChallanCumInvoiceObj.id;
        this.customerIdForChallan = this.createChallanCumInvoiceObj.custShippedTo;

        this.getInformation();
        this.getInformationYourReference();
      }

    });

  }

  getSrNoForSearchInvoice() {

    this.createInvoiceService.sendDocNoOfChallanCumInvoiceDocument(this.documentNumberFromSearchInvoice).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })

    this.createInvoiceService.getSrNoForChallanCumInvoiceDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.challanCumInvoiceArrayForSearchInvoice = data;

      if (this.challanCumInvoiceArrayForSearchInvoice.length != 0) {

        for (var z = 0; z < this.challanCumInvoiceArrayForSearchInvoice.length; z++) {

          this.newDynamic1.push({
            SR_NO: this.challanCumInvoiceArrayForSearchInvoice[z][3],
            INSTRU_NAME: this.challanCumInvoiceArrayForSearchInvoice[z][4],
            DESCRIPTION: this.challanCumInvoiceArrayForSearchInvoice[z][5],
            ID_NO: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            ACCREDIATION: this.challanCumInvoiceArrayForSearchInvoice[z][7],
            RANGE: this.challanCumInvoiceArrayForSearchInvoice[z][6],
            SAC_CODE: this.challanCumInvoiceArrayForSearchInvoice[z][7],
            QUANTITY: this.challanCumInvoiceArrayForSearchInvoice[z][8],
            RATE: this.challanCumInvoiceArrayForSearchInvoice[z][1],
            DISCOUNT_ON_ITEM: this.challanCumInvoiceArrayForSearchInvoice[z][2],
            UNIT_PRICE: this.challanCumInvoiceArrayForSearchInvoice[z][9],
            AMOUNT: this.challanCumInvoiceArrayForSearchInvoice[z][10]

          })

        }

        this.dynamicArray.splice(0, 1);
        for (var l = 1; l < this.newDynamic1.length; l++) {
          this.dynamicArray.push(this.newDynamic1[l]);
        }
      }
    })


  }
  backToMenu() {
    this.back=1;
    localStorage.setItem('back',JSON.stringify(this.back));
    this.router.navigate(['/nav/crtdelichallan']);
   
    window.localStorage.removeItem('checkQuantityArray');
    window.localStorage.removeItem('checkAmountArray');

  }

  toggleHeaderForCustomer(e) {
    console.log(e);
    this.selectTagEventForHeader = e;
  }

  headercustNameChange(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.shippedCustNameTrial == this.customers[s]) {

        this.custNameTrialShippedTo = this.customer1[s].name;
        this.shippedCustDepartment = this.customer1[s].department;
        this.shippedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.shippedCustPanNo = this.customer1[s].panNo;
        this.shippedCustGstNo = this.customer1[s].gstNo;
        this.shippedCustAddressline1 = this.customer1[s].addressLine1;
        this.shippedCustAddressline2 = this.customer1[s].addressLine2;
        this.shippedCustCountry = this.customer1[s].country;
        this.shippedCustState = this.customer1[s].state;
        this.shippedCustCity = this.customer1[s].city;
        this.shippedCustPin = this.customer1[s].pin;

        break;
      }
    }
    this.splitGstAndQuotationNumber1();
  }


  headercustNameChange1(e) {
    console.log(e);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.billedCustNameTrial == this.customers[s]) {

        this.custNameTrialBilledTo = this.customer1[s].name;
        this.billedNameAndDept = this.customer1[s].name + " / " + this.customer1[s].department;
        this.billedCustDepartment = this.customer1[s].department;
        this.billedCustPanNo = this.customer1[s].panNo;
        this.billedCustGstNo = this.customer1[s].gstNo;
        this.billedCustAddressline1 = this.customer1[s].addressLine1;
        this.billedCustAddressline2 = this.customer1[s].addressLine2;
        this.billedCustCountry = this.customer1[s].country;
        this.billedCustState = this.customer1[s].state;
        this.billedCustCity = this.customer1[s].city;
        this.billedCustPin = this.customer1[s].pin;
        this.cgst = this.customer1[s].cgst;
        this.sgst = this.customer1[s].sgst;
        this.igst = this.customer1[s].igst;

        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
     
        break;
      }
    }
    this.splitGstAndQuotationNumber2();
  }

  splitGstAndQuotationNumber1() {
    if (typeof this.shippedCustGstNo != 'undefined') {
      this.stateCode = this.shippedCustGstNo.slice(0, 2);

    }
  }
  splitGstAndQuotationNumber2() {
    if (typeof this.billedCustGstNo != 'undefined') {
      this.billedStateCode = this.billedCustGstNo.slice(0, 2);

    }
  }



}



