import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInvdocComponent } from './create-invdoc.component';

describe('CreateInvdocComponent', () => {
  let component: CreateInvdocComponent;
  let fixture: ComponentFixture<CreateInvdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInvdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInvdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
