
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateChallanCumInvoice, CreateInvoiceService } from '../../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { Branch, DocumentTypeForInvoice, FinancialYear, InvoiceTypeForInvoice } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CustomerbyName, SearchCustomerService } from '../../../../services/serviceconnection/search-service/search-customer.service';
import { Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
declare var $:any;
@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.css']
})
export class CreateInvoiceComponent implements OnInit {

  createInvoiceChallanForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  customerByNameObject: CustomerbyName = new CustomerbyName();
  model: any = {};
  emp: any;

  customerIdForChallan: any;
  customersArray: Array<any> = [];
  customersName: Array<any> = [];
  customersDepartmentName: Array<any> = [];
  customersAddressLine1: Array<any> = [];
  customersAddressLine2: Array<any> = [];
  customersCountry: Array<any> = [];
  customersState: Array<any> = [];
  customersCity: Array<any> = [];
  customersPin: Array<any> = [];

  customersIds: Array<any> = [];
  customerIdLength: number;

  branch: Branch = new Branch();
  branch1: Branch = new Branch();
  branches: Array<any> = [];
  branchesIds: Array<any> = [];

  dateOfDocumentNo1: any;
  dateOfDocumentNo2: any;

  createChallanCumInvoiceObj1: CreateChallanCumInvoice = new CreateChallanCumInvoice();
  challan1: any;
  alive = true;


  ////////////for doc no declaration//////////
  finalDocumentNumber: string;
  prefixForQuotation: any;
  dateOfDocumentNo: any;
  counterForDocumentNoString: string;
  counterForDocumentNo: any;
  public dateValue1 = Date.now();
  dateValue2: any;
  documentNumber: string;
  loginUserName: any;
  dateValue3: any;
  dateValue4: any;
  dateValue5: any;

  documentTypeObjectData: DocumentTypeForInvoice = new DocumentTypeForInvoice();
  documentTypeName: Array<any> = [];
  documentTypeIds: Array<any> = [];
  documentType: any;

  invoiceTypeObjectData: InvoiceTypeForInvoice = new InvoiceTypeForInvoice();
  invoiceTypeName: Array<any> = [];
  invoiceTypeIds: Array<any> = [];
  invoiceType: any;

  financialYearData: FinancialYear = new FinancialYear();
  startDate: any;
  idForFromDate: any;
  inwardNumberFromReqAndInward: any;
  requestNumber: any;
  requestAndInwardDataArray:Array<any>=[];
  customerDcNumber: any;
  requestAndInwardDocumentNumber: any;

  customers: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds1: Array<any> = [];
  static customerNameLength: number;
  customer: Customer = new Customer();
  customer1: Customer = new Customer();
  static customerDepartments: Array<any> = [];
  custId: any;
  requestNumbersArray:Array<any>=[];
  requestNumbersCollectionArray:Array<any>=[];
  selectedItems = [];
  dropdownSettings = {};
  dropdownList = [];
  dropdownList1 = [];
  countForRequestNumbers: {};
  countForRequestNumbers1: {};
  uniqueRequestNumbersAvailable:Array<any>=[];
  onlyRequestAndInwardNumberArray:Array<any>=[];
  dataForpoNumbersArray:Array<any>=[];
  onlyPoNumbersArray:Array<any>=[];
  uniquePoNumbersArray:Array<any>=[];
  onlyDocumentTypeArray:Array<any>=[];
  uniqueDocumentTypeArray:Array<any>=[];
  checkAmountArray:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
  checkAmountArray1:Array<{docTypePO:string,remPOValue:any,poValue:any,createPoTableId:any}>=[];
  checkQuantityArray :Array<{instruName:string,docTypePO:string,remQuantity:any,srNoTableId:any,rangeFromPo:any,rangeToPo:any,ratePo:any}>=[];
  back: any;
  poNo: any;
  docType1: any;
  trialPoValue: any;
  trialRemPoValue: any;


  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient, private router: Router, private searchCustomerService: SearchCustomerService, private createUserService: CreateUserService, private createInvoiceService: CreateInvoiceService, public datepipe: DatePipe) {

    this.createInvoiceChallanForm = this.formBuilder.group({

      documentType: [],
      requestNumber: [],
      customerDcNumber: [],
      branchName: [],
      invoiceType: [],
      documentDate: [],
      poNo:[],
      selectedItems:[]

    })
  }


  ngOnInit() {

this.back=JSON.parse(localStorage.getItem('back'));
if(this.back==1){
  this.back=0;
localStorage.setItem('back',JSON.stringify(this.back));
  location.reload();
  
  
}

    this.selectedItems = [

    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
    };

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.requestAndInwardDataArray=JSON.parse(localStorage.getItem('RequestAndInwardArrayForInvoice'))
   console.log("requestAndInwardDataArray"+this.requestAndInwardDataArray);
   this.requestAndInwardDocumentNumber=this.requestAndInwardDataArray[0];
    this.requestNumber=this.requestAndInwardDataArray[1];
    this.customerDcNumber=this.requestAndInwardDataArray[2];
    this.custId=this.requestAndInwardDataArray[11];
    this.customerIdForChallan = this.createInvoiceService.getCustomerNameForCreatingChallan();

    if(typeof this.customerIdForChallan!='undefined' || this.customerIdForChallan!=null){
    this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForCustId(this.customerIdForChallan).subscribe(data => {
      this.requestNumbersArray=data;
      console.log("requestNumbersArray"+this.requestNumbersArray);
      for(var i=0; i<this.requestNumbersArray.length;i++){
        this.onlyRequestAndInwardNumberArray.push( this.requestNumbersArray[i][2]);
      }
      
    })

    

  }
  else{

    this.createInvoiceService.getReqAndInwDataWithCalibrationTagAndInvoiceTagForCustId(this.custId).subscribe(data => {
      this.requestNumbersArray=data;
      console.log("requestNumbersArray"+this.requestNumbersArray);
      for(var i=0; i<this.requestNumbersArray.length;i++){
        this.onlyRequestAndInwardNumberArray.push( this.requestNumbersArray[i][2]);
      }

//code for unique request numbers
      var filter = function (value, index)
       { return this.indexOf(value) == index 
      };
      this.uniqueRequestNumbersAvailable = this.onlyRequestAndInwardNumberArray.filter(filter, this.onlyRequestAndInwardNumberArray);
      console.log(this.uniqueRequestNumbersAvailable);


      for(var i=0;i<this.uniqueRequestNumbersAvailable.length;i++){
        this.dropdownList1.push({itemId:i+1,itemText:this.uniqueRequestNumbersAvailable[i]

        })

     }
     this.dropdownList=this.dropdownList1;
      })
      

     
  }
 
  // get all po list
  if(typeof this.customerIdForChallan!='undefined' || this.customerIdForChallan!=null){
    this.createInvoiceService.getAllPoNumbersForCustId(this.customerIdForChallan).subscribe(data => {
      this.dataForpoNumbersArray=data;
      console.log("dataForpoNumbersArray"+this. dataForpoNumbersArray);
      for(var i=0;i<this.dataForpoNumbersArray.length;i++){
        this.onlyPoNumbersArray.push(this.dataForpoNumbersArray[i][0]);
        this.onlyDocumentTypeArray.push(this.dataForpoNumbersArray[i][1]);
      }
       //code for unique po numbers
  var filter1 = function (value, index)
  { return this.indexOf(value) == index 
 };
 this.uniquePoNumbersArray = this.onlyPoNumbersArray.filter(filter1, this.onlyPoNumbersArray);
 console.log(this.uniquePoNumbersArray);

      
    })

  }else{
    this.createInvoiceService.getAllPoNumbersForCustId(this.custId).subscribe(data => {
      this.dataForpoNumbersArray=data;
      console.log("dataForpoNumbersArray"+this. dataForpoNumbersArray);
      for(var i=0;i<this.dataForpoNumbersArray.length;i++){
        this.onlyPoNumbersArray.push(this.dataForpoNumbersArray[i][0]);
        this.onlyDocumentTypeArray.push(this.dataForpoNumbersArray[i][1]);
      }
       //code for unique po numbers
  var filter1 = function (value, index)
  { return this.indexOf(value) == index 
 };
 this.uniquePoNumbersArray = this.onlyPoNumbersArray.filter(filter1, this.onlyPoNumbersArray);
 console.log(this.uniquePoNumbersArray);

    })


  }

 



    // get branch list
    this.createUserService
      .getBranchList().subscribe(data => {
        this.branch1 = data;

        console.log(this.branch1);

        for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
          this.branches.push(this.branch1[i].branchName);
          this.branchesIds.push(this.branch1[i].branchId);
        }
      },
        error => console.log(error));

    //get document type list
    this.createInvoiceService.getDocumentTypeListForInvoice().subscribe(data => {
      this.documentTypeObjectData = data;
      console.log(this.documentTypeObjectData);

      for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {
        this.documentTypeName.push(this.documentTypeObjectData[i].documentTypeName);
        this.documentTypeIds.push(this.documentTypeObjectData[i].documentTypeId);

      }
    },
      error => console.log(error));

    //get invoice type list
    this.createInvoiceService.getInvoiceTypeListForInvoice().subscribe(data => {
      this.invoiceTypeObjectData = data;
      console.log(this.invoiceTypeObjectData);

      for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {
        this.invoiceTypeName.push(this.invoiceTypeObjectData[i].invoiceTypeName);
        this.invoiceTypeIds.push(this.invoiceTypeObjectData[i].invoiceTypeId);

      }
    },
      error => console.log(error));


    // get financial year dates list
    this.createInvoiceService.getFinancialYearDates().subscribe(data => {
      this.financialYearData = data;
      console.log(this.financialYearData);

      for (var i = 0, l = Object.keys(this.financialYearData).length; i < l; i++) {
        if (this.financialYearData[i].status == 1) {
          this.startDate = this.financialYearData[i].fromDate;
          this.idForFromDate = this.financialYearData[i].id;
          break;
        }

      }
    },
      error => console.log(error));

    //   $("select option").each(function() {
    //     var $thisOption = $(this);
    //     var valueToCompare = "UCSPL INVOICE BY ID";
        
    //     if($thisOption.val() == valueToCompare) {
    //         $thisOption.attr("disabled", "disabled");
    //     }
    // });
  
    

  }//end of ngOnInit


  onItemSelect(item: any) {
    console.log(item);
   // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
    this.requestNumbersCollectionArray.push(item);

  }

   onSelectAll(items: any) {
  this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length);
  for (var j = 0; j < items.length; j++){
    this.requestNumbersCollectionArray.push(items[j]);

 }

   }

  onDeselectAll(items: any) {
    this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length); 
  }
  onItemDeSelect(item: any) {
    for(var i=0; i<this.requestNumbersCollectionArray.length;i++){
      if(item.itemId==this.requestNumbersCollectionArray[i].itemId){

       // this.requestNumbersCollectionArray.splice(0, this.requestNumbersCollectionArray.length); 
       var trial=[this.requestNumbersCollectionArray[i].itemId];
       this.requestNumbersCollectionArray = this.requestNumbersCollectionArray.filter(el => (-1 == trial.indexOf(el.itemId)));
        console.log(this.requestNumbersCollectionArray);

      }
     
    }

  }


  

  clearForm() {
    this.router.navigateByUrl('/crtinvdoc')
  }

  onSubmit() {
    this.createInvoiceService.documentNumberForCreateChallanCumInvoice().subscribe(data => {
      console.log(data);
      this.createChallanCumInvoiceObj1.invDocumentNumber = data.number;
      this.addOneToDocumentNumber();
     
    })



  }
  save() {

    this.checkQuantity();
    this.createChallanCumInvoiceObj1.requestNumber = this.createInvoiceChallanForm.value.requestNumber;
    this.createChallanCumInvoiceObj1.dcNo = this.createInvoiceChallanForm.value.customerDcNumber;
    this.createChallanCumInvoiceObj1.invoiceDate = this.createInvoiceChallanForm.value.documentDate;
    this.createChallanCumInvoiceObj1.poNo = this.createInvoiceChallanForm.value.poNo;
    this.createChallanCumInvoiceObj1.createdBy = this.loginUserName;
    this.createChallanCumInvoiceObj1.archieveDate = "";

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchName == this.createInvoiceChallanForm.value.branchName) {
        this.createChallanCumInvoiceObj1.branchId = this.branch1[i].branchId;
        console.log(this.createChallanCumInvoiceObj1.branchId);
      }
    }

    for (var i = 0, l = Object.keys(this.documentTypeObjectData).length; i < l; i++) {

      if (this.documentTypeObjectData[i].documentTypeName == this.documentType) {
        this.createChallanCumInvoiceObj1.documentTypeId = this.documentTypeObjectData[i].documentTypeId;
      }
    }
    
    for (var i = 0, l = Object.keys(this.invoiceTypeObjectData).length; i < l; i++) {

      if (this.invoiceTypeObjectData[i].invoiceTypeName == this.invoiceType) {
        this.createChallanCumInvoiceObj1.invoiceTypeId = this.invoiceTypeObjectData[i].invoiceTypeId;
      }
    }

    if(typeof this.customerIdForChallan!='undefined' || this.customerIdForChallan!=null){
    this.createInvoiceService.setCustomerNameForCreatingChallan(this.customerIdForChallan);
    localStorage.setItem('custIdForInvoice1',JSON.stringify(this.customerIdForChallan));
  }
  else{
    this.createInvoiceService.setCustomerNameForCreatingChallan(this.custId);
    localStorage.setItem('custIdForInvoice1',JSON.stringify(this.custId));
  }
    this.createInvoiceService.createCreateChallanCumInvoice(this.createChallanCumInvoiceObj1).subscribe(data => {
      this.challan1 = data;
      console.log(this.challan1);

      this.createInvoiceService.setCreateChallanCumInvoiceDocument(this.challan1);
      localStorage.setItem('challan1',JSON.stringify(this.challan1));
      localStorage.setItem('requestNumbersCollectionArray',JSON.stringify(this.requestNumbersCollectionArray));
      localStorage.setItem('requestNumbersArray',JSON.stringify(this.requestNumbersArray));
      localStorage.setItem('InvFinalDocumentNumber',JSON.stringify(this.finalDocumentNumber));

      if(this.docType1=="Customer PO Closed"){
        localStorage.setItem('checkQuantityArray',JSON.stringify(this.checkQuantityArray));
       
      }
      else{
        localStorage.setItem('checkAmountArray',JSON.stringify(this.checkAmountArray));
        
      }
    
      this.callToNextComponent();
    })

  }
  addOneToDocumentNumber() {

    this.prefixForQuotation = this.createChallanCumInvoiceObj1.invDocumentNumber.slice(0, 10);
    this.dateOfDocumentNo1 = this.createChallanCumInvoiceObj1.invDocumentNumber.slice(10, 12);
    this.dateOfDocumentNo2 = this.createChallanCumInvoiceObj1.invDocumentNumber.slice(12, 14);
    this.counterForDocumentNoString = this.createChallanCumInvoiceObj1.invDocumentNumber.slice(15, 20);

    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);

    this.dateValue2 = this.startDate;
    this.dateValue3 = Date.now();
    this.dateValue4 = this.datepipe.transform(this.dateValue3, 'yy');
    this.dateValue5 = this.datepipe.transform(this.dateValue3, 'dd-MM');

    if (this.dateValue2 == this.dateValue5) {
      if (this.dateValue4 == this.dateOfDocumentNo2) {
        this.counterForDocumentNo = 0;
        this.dateOfDocumentNo1 = parseInt(this.dateOfDocumentNo1);
        this.dateOfDocumentNo2 = parseInt(this.dateOfDocumentNo2);
        this.dateOfDocumentNo1 = this.dateOfDocumentNo1 + 1;
        this.dateOfDocumentNo2 = this.dateOfDocumentNo2 + 1;

      }
    }

    this.counterForDocumentNo = this.counterForDocumentNo + 1;

    var output = [], n = 1, padded;
    padded = ('0000' + this.counterForDocumentNo).slice(-5);
    output.push(padded);

    this.counterForDocumentNoString = this.counterForDocumentNo.toString();
    this.counterForDocumentNoString = "_" + this.counterForDocumentNoString;


    var a = this.prefixForQuotation + this.dateOfDocumentNo1 + this.dateOfDocumentNo2 + "_" + padded;
    this.finalDocumentNumber = a;

    console.log(this.finalDocumentNumber);
    this.createChallanCumInvoiceObj1.invDocumentNumber = a;

    this.save();

  }

  callToNextComponent() {
    this.back=0;
    localStorage.setItem('back',JSON.stringify(this.back));
   
    this.router.navigateByUrl('/crtinvdoc');
  }

  callToCreateProforma() {
    this.router.navigateByUrl('/crtprodoc');
  }

  backToCreateDeliveryChallan() {
    this.router.navigateByUrl('/nav/srhcustinv');
  }
  backToSearchRequest(){
    this.router.navigateByUrl('/nav/searchreq')
  }



  editQuantity() {                                                      // for document

    const buttonModal12 = document.getElementById("openModalButton12")
    console.log('buttonModal12', buttonModal12)
    buttonModal12.click();
    this.checkQuantity();
  }

  checkQuantity(){
    this.checkAmountArray.splice(0,this.checkAmountArray.length);
    this.checkAmountArray1.splice(0,this.checkAmountArray1.length);
     this.checkQuantityArray.splice(0,this.checkQuantityArray.length);

   for(var x=0;x<this.dataForpoNumbersArray.length;x++){
     if(this.poNo==this.dataForpoNumbersArray[x][0]){
       if(this.dataForpoNumbersArray[x][1]=="Customer PO Closed"){
       this.checkQuantityArray.push({instruName:this.dataForpoNumbersArray[x][2],docTypePO:this.dataForpoNumbersArray[x][1],remQuantity:this.dataForpoNumbersArray[x][4],srNoTableId:this.dataForpoNumbersArray[x][7],rangeFromPo:this.dataForpoNumbersArray[x][8],rangeToPo:this.dataForpoNumbersArray[x][9],ratePo:this.dataForpoNumbersArray[x][10]});
       this.docType1= this.checkQuantityArray[0].docTypePO;
      // this.checkAmountArray1.push({doctype:"Customer PO Closed",remPOValue:this.dataForpoNumbersArray[x][3],poValue:this.dataForpoNumbersArray[x][5]});;
     //  checkQuantityArray:Array<{instruName:string,doctype:string,remQuantity:any,remAmount:any}>=[];
    }
    else{
      this.checkAmountArray.push({docTypePO:this.dataForpoNumbersArray[x][1],remPOValue:this.dataForpoNumbersArray[x][3],poValue:this.dataForpoNumbersArray[x][5],createPoTableId:this.dataForpoNumbersArray[x][6]});
      this.docType1= this.checkAmountArray[0].docTypePO;
      this.trialPoValue=this.checkAmountArray[0].poValue;
      this.trialRemPoValue=this.checkAmountArray[0].remPOValue;
    }

     }
   }
  if(this.docType1=="Customer PO Closed"){
    this.checkAmountArray1.push({docTypePO:"Customer PO Closed",remPOValue:this.dataForpoNumbersArray[0][3],poValue:this.dataForpoNumbersArray[0][5],createPoTableId:this.dataForpoNumbersArray[0][6]});
  }else{
    this.checkAmountArray1.push(this.checkAmountArray[0]);
  }
   
  
  }


}


