import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../services/config/config.service';
import { CreateCustomerService, Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateInvoiceService } from '../../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateProformaService } from '../../../../services/serviceconnection/create-service/create-proforma.service';
import { CustomerbyName, SearchCustomerService } from '../../../../services/serviceconnection/search-service/search-customer.service';

@Component({
  selector: 'app-search-cust-inv',
  templateUrl: './search-cust-inv.component.html',
  styleUrls: ['./search-cust-inv.component.css']
})
export class SearchCustInvComponent implements OnInit {

  searchCustomerForm: FormGroup;
  p: number = 1;
  itemsPerPage: number = 5;

  customerByName: CustomerbyName = new CustomerbyName();
  model: any = {};
  emp: any;

  customers: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  static customerNameLength: number;
  customer: Customer = new Customer();
  customer1: Customer = new Customer();
  static customerDepartments: Array<any> = [];

  customersArray: Array<any> = [];
  customersName: Array<any> = [];
  customersDepartmentName: Array<any> = [];
  customersAddressLine1: Array<any> = [];
  customersAddressLine2: Array<any> = [];
  customersCountry: Array<any> = [];
  customersState: Array<any> = [];
  customersCity: Array<any> = [];
  customersPin: Array<any> = [];

  customerIdLength: number;
  status: string;
  back: number;

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient, private router: Router, private searchCustomerService: SearchCustomerService, private createInvoiceService: CreateInvoiceService, private createCustomerService: CreateCustomerService, private createProformaService: CreateProformaService) {

    this.searchCustomerForm = this.formBuilder.group({

      name: ['', [Validators.required, ValidationService.nameValidator]],
      status: [],
      department: [''],
      address: [''],
      itemsPerPage: []
    })
  }


  ngOnInit() {
    this.status = "1";
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customer1 = data;
      console.log(this.customer1);

      SearchCustInvComponent.customerNameLength = Object.keys(this.customer1).length;
      for (var i = 0, l = Object.keys(this.customer1).length; i < l; i++) {
        this.customers.push(this.customer1[i].name + " / " + this.customer1[i].department);
        console.log("customers : " + this.customers);
        SearchCustInvComponent.customerNames.push(this.customer1[i].name);
        SearchCustInvComponent.customerDepartments.push(this.customer1[i].department);

        this.customersIds.push(this.customer1[i].id);
        console.log("customer id : " + this.customersIds);
      }

    },
      error => console.log(error));
  }

  showdata() {
    this.searchCustomerService.showData().subscribe((res: any) => {
      this.emp = res;
      console.log(this.emp);
      this.listOfCustomers();
    })
  }

  searchdata() {
    debugger;
    this.customerByName = this.searchCustomerForm.value;

    if ((!this.searchCustomerForm.value.name.includes('and')) && (!this.searchCustomerForm.value.name.includes('&')))
      this.customerByName.nameAnd = "";

    this.searchCustomerService.searchData(this.customerByName).subscribe((res: any) => {
      this.emp = res;
      console.log(this.emp);
    })
    this.showdata();
  }

  listOfCustomers() {
    this.searchCustomerService.showData().subscribe(data => {
      this.customersArray = data;
      console.log("...trial" + this.customersArray);

      for (var i = 0; i < Object.keys(this.customersArray).length; i++) {
        if (this.customersArray[i][22] == 0) {
          this.customersArray[i][22] = "Inactive";
        }
        else
          this.customersArray[i][22] = "Active";
      }
    },
      error => console.log(error));
  }

  CreateChallan(i) {
    console.log("index we are getting " + i);

    for (var s = 0; s < this.customers.length; s++) {
      if (this.customersArray[i][3] == SearchCustInvComponent.customerNames[s]) {
        this.createInvoiceService.setCustomerNameForCreatingChallan(this.customer1[s].id);
        break;
      }
    }
    this.back=0;
    localStorage.setItem('back',JSON.stringify(this.back));
    this.router.navigateByUrl('nav/createInvoice');
  }

  CreateProformaChallan(i) {

    console.log("index we are getting " + i);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.customersArray[i][3] == SearchCustInvComponent.customerNames[s]) {
        this.createProformaService.setCustomerNameForCreatingChallan(this.customer1[s].id);
        break;
      }
    }
    this.router.navigateByUrl('nav/crtproforma');
  }

  CreateDeliveryChallan(i){
    console.log("index we are getting " + i);
    for (var s = 0; s < this.customers.length; s++) {
      if (this.customersArray[i][3] == SearchCustInvComponent.customerNames[s]) {
        this.createProformaService.setCustomerNameForCreatingChallan(this.customer1[s].id);
        break;
      }
    }
    this.router.navigateByUrl('nav/crtdelichallan');

  }


  clearForm() {
    this.router.navigateByUrl('/crtinvdoc')
  }
 

}