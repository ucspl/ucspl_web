import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCustInvComponent } from './search-cust-inv.component';

describe('SearchCustInvComponent', () => {
  let component: SearchCustInvComponent;
  let fixture: ComponentFixture<SearchCustInvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchCustInvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCustInvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
