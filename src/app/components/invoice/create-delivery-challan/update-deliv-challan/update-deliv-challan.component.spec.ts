import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDelivChallanComponent } from './update-deliv-challan.component';

describe('UpdateDelivChallanComponent', () => {
  let component: UpdateDelivChallanComponent;
  let fixture: ComponentFixture<UpdateDelivChallanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDelivChallanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDelivChallanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
