import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDelivChallanComponent } from './create-deliv-challan.component';

describe('CreateDelivChallanComponent', () => {
  let component: CreateDelivChallanComponent;
  let fixture: ComponentFixture<CreateDelivChallanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDelivChallanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDelivChallanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
