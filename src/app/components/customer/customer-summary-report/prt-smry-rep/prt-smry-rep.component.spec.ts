import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrtSmryRepComponent } from './prt-smry-rep.component';

describe('PrtSmryRepComponent', () => {
  let component: PrtSmryRepComponent;
  let fixture: ComponentFixture<PrtSmryRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrtSmryRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrtSmryRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
