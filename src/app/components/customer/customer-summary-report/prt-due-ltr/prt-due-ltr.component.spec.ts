import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrtDueLtrComponent } from './prt-due-ltr.component';

describe('PrtDueLtrComponent', () => {
  let component: PrtDueLtrComponent;
  let fixture: ComponentFixture<PrtDueLtrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrtDueLtrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrtDueLtrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
