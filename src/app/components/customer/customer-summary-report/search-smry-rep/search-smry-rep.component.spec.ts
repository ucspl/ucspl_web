import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSmryRepComponent } from './search-smry-rep.component';

describe('SearchSmryRepComponent', () => {
  let component: SearchSmryRepComponent;
  let fixture: ComponentFixture<SearchSmryRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSmryRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSmryRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
