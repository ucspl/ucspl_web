
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-search-smry-rep',
  templateUrl: './search-smry-rep.component.html',
  styleUrls: ['./search-smry-rep.component.css']
})
export class SearchSmryRepComponent implements OnInit {

  SearchDocumentform: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {
    this.SearchDocumentform = this.formBuilder.group({
      document: []
    });
  }

  ngOnInit() {
  }

}
