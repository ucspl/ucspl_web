
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { Customer, RegionAreaCode, CreateCustomerService, BillingCreditPeriod, DynamicGridCustContact, Taxes, CustomerTaxId, CustomerContactList, TaxpayerTypeForCustomer, States, Country, CustomerAttachments } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { SearchCustomerService } from '../../../../services/serviceconnection/search-service/search-customer.service';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-cust-update',
  templateUrl: './cust-update.component.html',
  styleUrls: ['./cust-update.component.css']
})
export class CustUpdateComponent implements OnInit {


  addCustomerForm: FormGroup;

  regionAreaCodeSelected: any;
  regionAreaCode: RegionAreaCode = new RegionAreaCode();
  arrayRegionAreaCodeName: Array<any> = [];
  arrayRegionAreaCodeId: Array<any> = [];

  billingCreditPeriodSelected: any;
  billingCreditPeriod: BillingCreditPeriod = new BillingCreditPeriod();
  arrayBillingCreditPeriodName: Array<any> = [];
  arrayBillingCreditPeriodId: Array<any> = [];


  taxpayerTypeData: TaxpayerTypeForCustomer = new TaxpayerTypeForCustomer();
  arrayTaxpayerTypeName: Array<any> = [];
  arrayTaxpayerTypeId: Array<any> = [];
  arrayStatesCountryId: Array<any> = [];
  taxpayerTypeSelected: any;

  country: Country = new Country();
  arrayCountryName: Array<any> = [];
  arrayCountryId: Array<any> = [];

  statesData: States = new States();
  arrayStatesName: Array<any> = [];
  arrayStatesId: Array<any> = [];
  arrayStatesCode: Array<any> = [];
  arrState: Array<{ id: number, nm: string }> = [];


  customerTaxId: CustomerTaxId[] = [];
  taxesObject: Taxes = new Taxes();
  arrayTaxesName: Array<any> = [];
  arrayTaxesId: Array<any> = [];
  taxesDisplay: any;
  taxesIdsSelected: Array<any> = [];
  taxesData: Array<{ id: number, name: string }> = [];

  trialTaxesData: Array<{ id: number, name: string, check: boolean }> = [];

  arrayCustomerTax: Array<{ customerId: number, taxId: number }> = [];
  arrayTaxId: Array<any> = [];

  customerContactListObj: CustomerContactList = new CustomerContactList();
  custContactList: CustomerContactList = new CustomerContactList();
  arrayCustContactListId: Array<any> = [];
  arrayCustContactListId1: Array<any> = [];

  dateString = '';
  format = 'dd/MM/yyyy';

  opt: any; len: any;
  newSel: HTMLElement;
  static optionCount: number = 0;
  arrayApplicableTaxes: Array<any> = [];
  customerListArray: Array<any> = [];

  triale: any;
  trialPhoneNumber1: any;
  trialPhoneNumber2: any;
  prefixCountryCode1: any;
  prefixCountryCode2: any;
  public dateValue = Date.now();


  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  custObject: Customer = new Customer();
  customerObject: any;
  static customerNameLength: number;


  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;

  selectCountryId;
  countryList: any[] = [
    { name: '--Select Country--', states: [] },
    //  { name: 'India', states: [{ id: 1, nm: 'Andhra Pradesh' }, { id: 2, nm: 'Arunachal Pradesh' }, { id: 3, nm: 'Assam' }, { id: 4, nm: 'Bihar' }, { id: 5, nm: 'Chhattisgarh' }, { id: 6, nm: 'Goa' }, { id: 7, nm: 'Gujarat' }, { id: 8, nm: 'Haryana' }, { id: 9, nm: 'Himachal Pradesh' }, { id: 10, nm: 'Jammu and Kashmir' }, { id: 11, nm: 'Jharkhand' }, { id: 12, nm: 'Karnataka' }, { id: 13, nm: 'Kerala' }, { id: 14, nm: 'Ladakh' }, { id: 15, nm: 'Madhya Pradesh' }, { id: 16, nm: 'Maharashtra' }, { id: 17, nm: 'Manipur' }, { id: 18, nm: 'Meghalaya' }, { id: 19, nm: 'Mizoram' }, { id: 20, nm: 'Nagaland' }, { id: 21, nm: 'Odisha' }, { id: 22, nm: 'Puducherry' }, { id: 23, nm: 'Punjab' }, { id: 24, nm: 'Rajasthan' }, { id: 25, nm: 'Sikkim' }, { id: 26, nm: 'Tamil Nadu' }, { id: 27, nm: 'Telangana' }, { id: 28, nm: 'Tripura' }, { id: 29, nm: 'Uttar Pradesh' }, { id: 30, nm: 'Uttarakhand' }, { id: 31, nm: 'West Bengal' }] },

  ];
  states: Array<any>;


  countries = [{
    id: '8f8c6e98',
    name: 'USA',
    code: 'USD'
  },
  {
    id: '169fee1a',
    name: 'Canada',
    code: 'CAD'
  },
  {
    id: '3953154c',
    name: 'UK',
    code: 'GBP'
  }]
  parentCustomerWindow: number;
  statusOfCustomer: number;


  dynamicArray: Array<DynamicGridCustContact> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  newDynamic2: any = [{}];
  parentCustNotSelected: number;
  typeOfCustomer: number;
  loginUserName: any;

  count: number;
  customerIdForCustName: any;
  isParent: string;
  mobileNumber1: any;
  c: number;
  countryNm1: string;
  countryNm2: string;
  mobileNumber2: string;
  phoneNumberCountryNm1: any;
  phoneNumberCountryNm2: any;
  displaySezNote: number;
  stateCodeForSelectedState: any;


  selectedFile: File;
  selectedFileForCustAttachment: File;
  selectedFileForCustAttachment_trial: any;
  selectedFileForQuotation_quo: any;
  trial_file_reader: any;
  retrievedImage: any;
  base64Data: any;
  arrayBase64Data: Array<any> = [];
  arrayRetrievedImage: Array<any> = [];
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  // enquiry_attachment_Data: EnquiryAttachment = new EnquiryAttachment();
  enquiry_attachment_Data1: Object;
  cust_attachment_file_data: CustomerAttachments = new CustomerAttachments();
  printtermsid: any;

  custAttachUpload: CustomerAttachments = new CustomerAttachments();
  custAttachObj: CustomerAttachments = new CustomerAttachments();
  custAttach: any;

  docName: string;
  docType: string;
  docNumber: string;
  custAttachFiles: any;
  gstNo1: string;
  gstNo2: string;
  gstNo3: string;
  custNameForNewFileName: any;
  suppAttachFiles: any;



  get ordersFormArray() {
    return this.addCustomerForm.controls.orders as FormArray;
  }

  constructor(private formBuilder: FormBuilder, private router: Router, private searchCustomerService: SearchCustomerService, private createCustomerService: CreateCustomerService) {

    const control = this.trialTaxesData.map(c => new FormControl(c.check));
    // this.addCheckboxes(); 

    this.addCustomerForm = this.formBuilder.group({

      isParent: [],
      parentReference: [],
      name: [''],
      custSuffix: [],
      companyNameForSysReference: [],
      department: [],
      //  panNo: [''],
      //  gstNo1: [],
      panNo: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo1: ['', [optionalValidator([Validators.maxLength(2), ValidationService.StateCodeValidator])]],
      gstNo2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo3: ['', [optionalValidator([Validators.maxLength(3), ValidationService.lastThreeDigitOfGSTValidator])]],

      email1: ['', [Validators.required, ValidationService.emailValidator]],
      email2: ['', [optionalValidator([Validators.maxLength(255), ValidationService.emailValidator])]],
      mobileNumber1: ['', [Validators.required, ValidationService.phoneValidator]],
      mobileNumber2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      landlineNumber: [],
      addressLine1: [],
      addressLine2: [],
      status: [],
      country: [],
      state: [],
      city: [],
      pin: [],
      regionAreaCodes: [],
      vendorCode: [],
      billCreditPeriod: [],
      passFailRemark: [],
      taxes: [],
      applicableTaxes: [],
      taxpayerTypes: [],
      sezNote: [],
      checkedBy: [],
      createdBy: [],
      modifiedBy: [],
      calibratedProcedure: [],
      referencedUsed: [],
      expandedUncertainty: [],
      note: [],
      lut: [],
      sez: [],
      masterlhs: [],
      uuclhs: [],
      parentCustomer: [],
      customFields: [],
      orders: this.formBuilder.array(control)

    });

  }

  addCheckboxes() {

    this.taxesData.forEach(() => this.ordersFormArray.push(new FormControl(false)));
    console.log(this.ordersFormArray);
    for (var c = 0; c < this.arrayTaxId.length; c++) {
      //  this.ordersFormArray.value[this.arrayTaxId[c]-1]=true;
      this.addCustomerForm.value.orders[this.arrayTaxId[c] - 1] = true;

    }

    // this.taxesIdsSelected = this.addCustomerForm.value.orders
    // .map((checked, i) => checked ? this.taxesData[i].id : null)
    // .filter(v => v !== null);

    // this.trialTaxesData

  }



  ngOnInit() {

    this.countryNm1 = this.searchCustomerService.getCustomerCountryForMobileNo1();
    this.countryNm2 = this.searchCustomerService.getCustomerCountryForMobileNo2();


    this.newDynamic1 = [{
      id: "",
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    }]

    this.newDynamic2 = [{
      id: "",
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    }]

    this.newDynamic = {
      id: "",
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    };
    this.dynamicArray.push(this.newDynamic);


    // get region area code list
    this.createCustomerService.getRegionAreaCodeList().subscribe(data => {

      this.regionAreaCode = data;

      for (var i = 0, l = Object.keys(this.regionAreaCode).length; i < l; i++) {
        this.arrayRegionAreaCodeName.push(this.regionAreaCode[i].regionAreaCodeName);
        this.arrayRegionAreaCodeId.push(this.regionAreaCode[i].regionAreaCodeId);
      }
    },
      error => console.log(error));


    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;

      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);

        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);
        if (this.customerObj[i].isParent == 1) {

          this.parentCustNames.push(this.customerObj[i].name);

          this.parentCustIds.push(this.customerObj[i].id);

        }
      }
    },
      error => console.log(error));




    // get billing credit period list
    this.createCustomerService.getBillingCreditPeriodList().subscribe(data => {

      this.billingCreditPeriod = data;

      for (var i = 0, l = Object.keys(this.billingCreditPeriod).length; i < l; i++) {
        this.arrayBillingCreditPeriodName.push(this.billingCreditPeriod[i].billingCreditPeriodName);

        this.arrayBillingCreditPeriodId.push(this.billingCreditPeriod[i].billingCreditPeriodId);

      }


    },
      error => console.log(error));


    // get countries list
    this.createCustomerService.getCountryList().subscribe(data => {

      this.country = data;

      for (var i = 0, l = Object.keys(this.country).length; i < l; i++) {
        this.arrayCountryName.push(this.country[i].countryName);
        this.arrayCountryId.push(this.country[i].countryId);

      }
    },
      error => console.log(error));



    // get states list
    this.createCustomerService.getStatesList().subscribe(data => {

      this.statesData = data;

      for (var i = 0, l = Object.keys(this.statesData).length; i < l; i++) {
        this.arrayStatesName.push(this.statesData[i].stateName);
        this.arrayStatesId.push(this.statesData[i].stateId);
        this.arrayStatesCode.push(this.statesData[i].stateCode);
        this.arrayStatesCountryId.push(this.statesData[i].countryId);

        this.arrState.push({ id: this.statesData[i].stateId, nm: this.statesData[i].stateName });

        //     console.log(this.arrayStatesName)
      }
      console.log(this.arrState)
      this.countryAndStates();

    },
      error => console.log(error));




    // get taxpayer type list
    this.createCustomerService.getTaxpayerTypeList().subscribe(data => {

      this.taxpayerTypeData = data;

      for (var i = 0, l = Object.keys(this.taxpayerTypeData).length; i < l; i++) {
        this.arrayTaxpayerTypeName.push(this.taxpayerTypeData[i].taxpayerTypeName);
        this.arrayTaxpayerTypeId.push(this.taxpayerTypeData[i].taxpayerTypeId);

      }
    },
      error => console.log(error));


    this.createCustomerService.getCustomerContactList().subscribe(data => {
      console.log(data)
      this.custContactList = data;
      for (var c = 0; c < Object.keys(this.custContactList).length; c++) {
        this.arrayCustContactListId.push(this.custContactList[c].customerId)
      }
    },
      error => console.log(error));




    // get taxes list
    this.createCustomerService.getTaxesList().subscribe(data => {

      this.taxesObject = data;

      for (var i = 0, l = Object.keys(this.taxesObject).length; i < l; i++) {
        this.arrayTaxesName.push(this.taxesObject[i].taxName);
        this.arrayTaxesId.push(this.taxesObject[i].taxId);

        this.taxesData.push({ id: this.taxesObject[i].taxId, name: this.taxesObject[i].taxName });
        this.trialTaxesData.push({ id: this.taxesObject[i].taxId, name: this.taxesObject[i].taxName, check: false })

      }
      this.taxesDisplay = 1;

    },
      error => console.log(error));

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    this.customerIdForCustName = this.searchCustomerService.getIdForCustomerName();

    //getting tax id array
    this.searchCustomerService.getCustomerInCustomerAndTaxJunctionTable(this.customerIdForCustName).subscribe(data => {

      this.arrayTaxId = data;
      console.log(this.arrayTaxId);
    },
      error => console.log(error));


    // get customer details for id
    this.searchCustomerService.getCustomerDetailsById(this.customerIdForCustName).subscribe(data => {

      this.custObject = data;
      console.log(this.custObject);
      this.dataForUpdateCust();
      this.addCheckboxes();
    },
      error => console.log(error));



    // mobileNumber code

    var input1 = document.querySelector("#mobileNumber1");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": [this.countryNm1],
      "separateDialCode": true
    });
    //  iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();

    var countryData1 = iti1.getSelectedCountryData();


    //  this.trialPhoneNumber1 = 91;

    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      document.getElementById("country1").innerText = this.trialPhoneNumber1;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      console.log(this.trialPhoneNumber1);
    });


    var input2 = document.querySelector("#mobileNumber2");
    var country2 = $('#country2');
    var iti2 = (<any>window).intlTelInput(input2, {

      "preferredCountries": [this.countryNm2],
      "separateDialCode": true
    });
    //  iti2.setNumber("+917733123456");
    var number2 = iti2.getNumber();

    var countryData2 = iti2.getSelectedCountryData();

    //  this.trialPhoneNumber2 = 91;
    input2.addEventListener('countrychange', (e) => {

      var a2 = country2.val(iti2.getSelectedCountryData().dialCode);
      console.log(a2);
      this.trialPhoneNumber2 = iti2.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm2 = iti1.getSelectedCountryData().iso2;
    });

  }

  countryAndStates() {

    for (var i = 0; i < this.arrayCountryName.length; i++) {
      if (this.arrayCountryId[i] == this.arrayStatesCountryId[i]) {
        this.countryList.push({ name: this.arrayCountryName[i], states: this.arrState })
      }
    }

  }

  checkTypeOfCustomer() {
    if (this.customer.isParent == 0) {
      this.parentCustNotSelected = 0;
    }
    else
      this.parentCustNotSelected = 1;
  }

  checkCustName() {
    if (this.custSuffix == "Limited")
      this.companyNameForSysReference = this.name + " " + "LTD";

    if (this.custSuffix == "Private Limited")
      this.companyNameForSysReference = this.name + " " + "Pvt LTD";

    if (this.custSuffix == "Proprietorship")
      this.companyNameForSysReference = this.name + " ";

    if (this.custSuffix == "Patnership")
      this.companyNameForSysReference = this.name + " ";

  }

  findStateCodeForState() {
    for (var s = 0; s < this.arrayStatesName.length; s++) {
      if (this.customer.state == this.arrayStatesName[s] && this.customer.country == "India") {
        this.stateCodeForSelectedState = this.arrayStatesCode[s];
      }

    }

  }

  taxpayerTypeValue(e) {
    // if (e == "Sez supplies with payment" || e == "Sez supplies without payment")
    //   this.displaySezNote = 1
    // else
    //   this.displaySezNote = 0


    switch (e) {
      case "Regular":
        this.findStateCodeForState();
        if (this.stateCodeForSelectedState == 27) {
          this.customer.cgst = 1;
          this.customer.sgst = 1;
          this.customer.igst = 0;
          this.displaySezNote = 0
        }
        else {
          this.customer.cgst = 0;
          this.customer.sgst = 0;
          this.customer.igst = 1;
          this.displaySezNote = 0
        }
        break;

      case "Sez supplies with payment":
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 1;
        this.displaySezNote = 1
        break;

      case "Sez supplies without payment":
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 1
        break;

      case "LUT":
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 0
        break;

      default:
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 0

    }

  }

  changeCountry(count) {
    this.states = this.countryList.find(con => con.name == count).states;

  }

  getTaxesId() {
    this.taxesIdsSelected = this.addCustomerForm.value.orders
      .map((checked, i) => checked ? this.taxesData[i].id : null)
      .filter(v => v !== null);
  }

  phoneNumberUpdate() {
    this.trialPhoneNumber1 = this.customer.mobileNumber1.slice(1, 3);
    this.trialPhoneNumber2 = this.customer.mobileNumber2.slice(1, 3);

    this.customer.mobileNumber1 = this.customer.mobileNumber1.slice(3, 13);
    this.customer.mobileNumber2 = this.customer.mobileNumber2.slice(3, 13);
  }


  dataForUpdateCust() {


    this.customer.isParent = this.custObject.isParent;
    if (this.customer.isParent == 1) {
      this.parentCustNotSelected = 1;
      this.isParent = "Yes";
    }
    else {
      this.parentCustNotSelected = 0;
      this.isParent = "No";
    }


    this.customer.parentReference = this.custObject.parentReference;

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].id == this.customer.parentReference) {
        this.customer.parentReference = this.customerObj[i].name;
      }

    }

    this.customer.regionAreaCode = this.custObject.regionAreaCode;

    for (var i = 0; i < Object.keys(this.regionAreaCode).length; i++) {
      if (this.regionAreaCode[i].regionAreaCodeId == this.custObject.regionAreaCode) {
        this.customer.regionAreaCode = this.regionAreaCode[i].regionAreaCodeName;
        this.regionAreaCodeSelected = this.regionAreaCode[i].regionAreaCodeName;
      }
    }

    this.customer.billingCreditPeriod = this.custObject.billingCreditPeriod;

    for (var i = 0; i < Object.keys(this.billingCreditPeriod).length; i++) {
      if (this.billingCreditPeriod[i].billingCreditPeriodId == this.custObject.billingCreditPeriod) {
        this.customer.billingCreditPeriod = this.billingCreditPeriod[i].billingCreditPeriodName;
        this.billingCreditPeriodSelected = this.billingCreditPeriod[i].billingCreditPeriodName;
      }
    }

    this.taxpayerTypeSelected = this.custObject.taxpayerType;

    for (var i = 0; i < Object.keys(this.taxpayerTypeData).length; i++) {
      if (this.taxpayerTypeData[i].taxpayerTypeId == this.custObject.taxpayerType) {
        this.customer.taxpayerType = this.taxpayerTypeData[i].taxpayerTypeName;
        this.taxpayerTypeSelected = this.taxpayerTypeData[i].taxpayerTypeName;
      }
    }
    this.taxpayerTypeValue(this.taxpayerTypeSelected);

    this.companyNameForSysReference = this.custObject.name;
    this.customer.department = this.custObject.department;
    this.customer.panNo = this.custObject.panNo;
    this.customer.gstNo = this.custObject.gstNo;

    if (typeof this.customer.gstNo != 'undefined') {
      this.gstNo1 = this.customer.gstNo.slice(0, 2);
      this.gstNo2 = this.customer.gstNo.slice(2, 12);
      this.gstNo3 = this.customer.gstNo.slice(12);

    }




    this.mobileNumber1 = this.custObject.mobileNumber1.slice(-10);
    var mb1 = new String(this.custObject.mobileNumber1)
    this.trialPhoneNumber1 = this.custObject.mobileNumber1.slice(-mb1.length + 1, -10);
    console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

    if (this.custObject.mobileNumber2 != null) {
      var mb2 = new String(this.custObject.mobileNumber2)
      this.trialPhoneNumber2 = this.custObject.mobileNumber2.slice(-mb2.length + 1, -10);
      this.mobileNumber2 = this.custObject.mobileNumber2.slice(-10);
      console.log("value of this.trialPhoneNumber2" + this.trialPhoneNumber2)
    }

    this.customer.landlineNumber = this.custObject.landlineNumber;
    this.customer.countryPrefixForMobile1 = this.custObject.countryPrefixForMobile1
    this.customer.countryPrefixForMobile2 = this.custObject.countryPrefixForMobile2

    this.phoneNumberCountryNm1 = this.custObject.countryPrefixForMobile1;
    this.phoneNumberCountryNm2 = this.custObject.countryPrefixForMobile2;
    //this.phoneNumberUpdate();

    this.customer.email1 = this.custObject.email1;
    this.customer.email2 = this.custObject.email2;
    this.customer.addressLine1 = this.custObject.addressLine1;
    this.customer.addressLine2 = this.custObject.addressLine2

    this.customer.country = this.custObject.country;
    this.customer.state = this.custObject.state;

    this.changeCountry(this.customer.country);

    this.customer.city = this.custObject.city;
    this.customer.pin = this.custObject.pin;
    this.customer.vendorCode = this.custObject.vendorCode;
    this.customer.passFailRemark = this.custObject.passFailRemark;
    this.customer.status = this.custObject.status;

    this.customer.createdBy = this.custObject.createdBy;
    this.customer.dateCreated = this.custObject.dateCreated;
    this.customer.sezNote = this.custObject.sezNote;

    if (this.custObject.calibratedProcedure == 1) {
      $('#calibratedprocedure').prop('checked', true);
    }
    else {
      $('#calibratedprocedure').prop('checked', false);
    }

    if (this.custObject.referencedUsed == 1) {
      $('#referencedused').prop('checked', true);
    }
    else {
      $('#referencedused').prop('checked', false);
    }

    if (this.custObject.expandedUncertainty == 1) {
      $('#expandeduncertainty').prop('checked', true);
    }
    else {
      $('#expandeduncertainty').prop('checked', false);
    }

    if (this.custObject.note == 1) {
      $('#note').prop('checked', true);
    }
    else {
      $('#note').prop('checked', false);
    }

    if (this.custObject.lut == 1) {
      $('#lut').prop('checked', true);
    }
    else {
      $('#lut').prop('checked', false);
    }

    if (this.custObject.sez == 1) {
      $('#sez').prop('checked', true);
    }
    else {
      $('#sez').prop('checked', false);
    }

    if (this.custObject.masterlhs == 1) {
      $('#masterlhs').prop('checked', true);
    }
    else {
      $('#masterlhs').prop('checked', false);
    }

    if (this.custObject.uuclhs == 1) {
      $('#uuclhs').prop('checked', true);
    }
    else {
      $('#uuclhs').prop('checked', false);
    }

    if (this.custObject.parentCustomer == 1) {
      $('#parentcustomer').prop('checked', true);
    }
    else {
      $('#parentcustomer').prop('checked', false);
    }

    if (this.custObject.customFields == 1) {
      $('#customfields').prop('checked', true);
    }
    else {
      $('#customfields').prop('checked', false);
    }


    for (var c = 0; c < Object.keys(this.custContactList).length; c++) {
      //  this.arrayCustContactListId.push(this.custContactList[c].customerId)
      if (this.customerIdForCustName == this.arrayCustContactListId[c]) {
        this.getCustContactList();
        break;
      }

    }

    //this.arrayTaxId.map((i) => this.addCustomerForm.value.orders[i].checked)
    //this.taxesIdsSelected = this.addCustomerForm.value.orders
    //.map((checked, i) => checked ? this.taxesData[i].id : null)
    //.filter(v => v !== null);

    for (var c = 0; c < this.arrayTaxId.length; c++) {
      this.addCustomerForm.value.orders[this.arrayTaxId[c] - 1] = new FormControl(true);

      //  this.taxesData.forEach(() => this.ordersFormArray.push(new FormControl(false)));


      //  this.form.controls['checkboxes'].addControl(item, new FormControl(true));
      //   for(var a=0;a<this.addCustomerForm.value.orders;a++)
      //   {
      //   if(c==a-1)
      //   {

      //   }
      //   else{

      //   }
      // }

    }

  }

  onSubmit() {
    this.save();
  }

  save() {


    this.addCustomerForm.value.calibratedProcedure = $('#calibratedprocedure').is(':checked') ? $('#calibratedprocedure').val() : 0
    this.customer.calibratedProcedure = this.addCustomerForm.value.calibratedProcedure;
    this.addCustomerForm.value.referencedUsed = $('#referencedused').is(':checked') ? $('#referencedused').val() : 0
    this.customer.referencedUsed = this.addCustomerForm.value.referencedUsed;
    this.addCustomerForm.value.expandedUncertainty = $('#expandeduncertainty').is(':checked') ? $('#expandeduncertainty').val() : 0
    this.customer.expandedUncertainty = this.addCustomerForm.value.expandedUncertainty;
    this.addCustomerForm.value.note = $('#note').is(':checked') ? $('#note').val() : 0
    this.customer.note = this.addCustomerForm.value.note;
    this.addCustomerForm.value.lut = $('#lut').is(':checked') ? $('#lut').val() : 0
    this.customer.lut = this.addCustomerForm.value.lut;
    this.addCustomerForm.value.sez = $('#sez').is(':checked') ? $('#sez').val() : 0
    this.customer.sez = this.addCustomerForm.value.sez;
    this.addCustomerForm.value.masterlhs = $('#masterlhs').is(':checked') ? $('#masterlhs').val() : 0
    this.customer.masterlhs = this.addCustomerForm.value.masterlhs;
    this.addCustomerForm.value.uuclhs = $('#uuclhs').is(':checked') ? $('#uuclhs').val() : 0
    this.customer.uuclhs = this.addCustomerForm.value.uuclhs;
    this.addCustomerForm.value.parentCustomer = $('#parentcustomer').is(':checked') ? $('#parentcustomer').val() : 0
    this.customer.parentCustomer = this.addCustomerForm.value.parentCustomer;
    this.addCustomerForm.value.customFields = $('#customfields').is(':checked') ? $('#customfields').val() : 0
    this.customer.customFields = this.addCustomerForm.value.customFields;

    this.addCustomerForm.value.gstNo2 = this.addCustomerForm.value.panNo;
    this.customer.sezNote = this.addCustomerForm.value.sezNote;

    if (typeof this.addCustomerForm.value.custSuffix == 'undefined' || this.addCustomerForm.value.custSuffix == null) {
      this.customer.name = this.customer.name
    }
    else {
      this.customer.name = this.customer.name + " " + this.addCustomerForm.value.custSuffix;
    }


    //  this.customer=this.AddCustomerform.value;             

    for (var i = 0; i < Object.keys(this.regionAreaCode).length; i++) {
      if (this.regionAreaCode[i].regionAreaCodeName == this.regionAreaCodeSelected) {
        this.customer.regionAreaCode = this.regionAreaCode[i].regionAreaCodeId;

      }
    }

    for (var i = 0; i < Object.keys(this.billingCreditPeriod).length; i++) {
      if (this.billingCreditPeriod[i].billingCreditPeriodName == this.billingCreditPeriodSelected) {
        this.customer.billingCreditPeriod = this.billingCreditPeriod[i].billingCreditPeriodId;

      }
    }

    for (var i = 0; i < Object.keys(this.taxpayerTypeData).length; i++) {
      if (this.taxpayerTypeData[i].taxpayerTypeName == this.taxpayerTypeSelected) {
        this.customer.taxpayerType = this.taxpayerTypeData[i].taxpayerTypeId;
      }
    }

    this.getTaxesId();


    var gst;
    var gst1 = this.addCustomerForm.value.gstNo1.toString();
    var gst2 = this.addCustomerForm.value.gstNo2.toString();
    var gst3 = this.addCustomerForm.value.gstNo3.toString();

    gst = gst1.concat(gst2.toString());
    this.customer.gstNo = gst.concat(gst3);

    this.customer.applicableTaxes = "";   //not useful 

    // if (this.customer.cgst != 1 || typeof this.customer.cgst === 'undefined')
    //   this.customer.cgst = 0;
    // if (this.customer.igst != 1 || typeof this.customer.igst === 'undefined')
    //   this.customer.igst = 0;
    // if (this.customer.sgst != 1 || typeof this.customer.sgst === 'undefined')
    //   this.customer.sgst = 0;


    //save Mobile number with country code

    this.customer.mobileNumber1 = "+" + this.trialPhoneNumber1 + this.mobileNumber1;

    if (typeof this.mobileNumber2 != 'undefined' || this.mobileNumber2 != null) {
      this.customer.mobileNumber2 = "+" + this.trialPhoneNumber2 + this.mobileNumber2;
    }
    else {
      this.customer.mobileNumber2 = null;
    }

    this.customer.countryPrefixForMobile1 = this.phoneNumberCountryNm1;
    this.customer.countryPrefixForMobile2 = this.phoneNumberCountryNm2;


    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].name == this.customer.parentReference) {
        this.customer.parentReference = this.customerObj[i].id;

      }

    }

    this.customer.createdBy = this.loginUserName;
    this.customer.name = this.companyNameForSysReference;
    this.customer.modifiedBy = this.loginUserName;
    this.customer.dateModified = this.dateValue;
    // this.customer.regionAreaCode=this.regionAreaCodeSelected;
    // this.customer.billingCreditPeriod=this.billingCreditPeriodSelected;
    this.customer.sezNote = this.customer.sezNote;

    this.createCustomerService.updateCustomer(this.customerIdForCustName, this.customer).subscribe(data => {

      this.customerObject = data;

      alert("Customer updated Successfully!!");

    },
      error => console.log(error));


    // this.checkForDuplicate();
    //  this.clearForm();
  }

  clearForm() {
    window.location.reload();
  }


  ///////////////////////////////////Check for dublicancy ///////////////////////////

  checkForDuplicate() {
    for (this.count = 0; this.count < Object.keys(this.customerObj).length; this.count++) {

      if (this.customers[this.count] == this.customer.name && this.customerDept[this.count] == this.customer.department && this.customerGST[this.count] == this.customer.gstNo) {
        alert("This Customer is already present");
        break;
      }
      else {

      }
    }

    if (this.count >= Object.keys(this.customerObj).length) {

      this.createCustomerService.createCustomer(this.customer).subscribe(data => {

        this.customerObject = data;
        alert("Customer created Successfully!!");

        for (var e = 0; e < this.taxesIdsSelected.length; e++) {
          this.arrayCustomerTax.push({ customerId: this.customerObject.id, taxId: this.taxesIdsSelected[e] })
        }

        this.customerTaxId.push(...this.arrayCustomerTax);

        this.createCustomerService.addCustomerInCustomerAndTaxJunctionTable(this.customerTaxId).subscribe(data => {

        },
          error => console.log(error));

        this.clearForm();
      },
        error => console.log(error));


    }
  }

  addTaxAndCustomerId() {
    for (var count = 0; count < this.customerTaxId.length; count++) {
      this.createCustomerService.addCustomerInCustomerAndTaxJunctionTable(this.customerTaxId[count]).subscribe(data => {
        console.log(data)
      },
        error => console.log(error));
    }
    //  alert("Customer id and tax id successfully!!");
    this.clearForm()
  }

  //////////////////////////////////////////////// Code For Adding New Row //////////////////////////////////



  AddRow() {

    this.newDynamic = {
      id: "",
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }


  DeleteRow(index) {

    //var txt;
    if (confirm("Do you want to Delete?")) {
      console.log("delete customer contact details");

      this.createCustomerService.deleteCustomerContact(this.dynamicArray[index].id).subscribe(data => {
        console.log(data)
      },
        error => console.log(error));
      this.dynamicArray.splice(index, 1);

      return true;
    }
    else {
      //  txt = " ";
      return false;
    }
    //document.getElementById("demo").innerHTML = txt;


    // if (this.dynamicArray.length == 1) {
    //   console.log("only one row")

    //   return false;
    // } else {
    //   console.log("delete customer contact details");

    //   this.createCustomerService.deleteCustomerContact(this.dynamicArray[index].ID).subscribe(data => {
    //     console.log(data)
    //   },
    //     error => console.log(error));
    //   this.dynamicArray.splice(index, 1);

    //   return true;
    // }
  }

  myFunction() {
    var txt;
    if (confirm("Do you want to Logout?")) {
      this.router.navigate(['/login']);
    }
    else {
      txt = " ";
    }
    document.getElementById("demo").innerHTML = txt;
  }


  //////////////////////grt contact list data /////////////////////////////////////




  getCustContactList() {

    // this.createCustomerService.getCustomerContactList().subscribe(data => {
    //   console.log(data)
    //   this.custContactList=data;
    //   for(var c=0;c< Object.keys(this.custContactList).length;c++){
    //     this.arrayCustContactListId.push(this.custContactList[c].customerId)
    //   }      
    // },
    //   error => console.log(error));


    if (this.arrayCustContactListId.length != 0) {

      for (var z = 0; z < Object.keys(this.custContactList).length; z++) {

        if (this.customerIdForCustName == this.arrayCustContactListId[z]) {
          this.newDynamic1.push({
            id: this.custContactList[z].customerContactListId,
            name: this.custContactList[z].personName,
            custDepartment: this.custContactList[z].department,
            jobTitle: this.custContactList[z].jobTitle,
            mobileNo1: this.custContactList[z].mobileNumber1,
            mobileNo2: this.custContactList[z].mobileNumber2,
            phoneNo: this.custContactList[z].phoneNumber,
            email: this.custContactList[z].email,
            status: this.custContactList[z].status,
          })
        }
      }

      this.dynamicArray.splice(0, this.dynamicArray.length);

      for (var l = 1; l < this.newDynamic1.length; l++) {
        this.dynamicArray.push(this.newDynamic1[l]);
      }
    }

  }

  getCustUpdatedContactList() {

    if (this.arrayCustContactListId1.length != 0) {

      for (var z = 0; z < Object.keys(this.custContactList).length; z++) {

        if (this.customerIdForCustName == this.arrayCustContactListId1[z]) {
          this.newDynamic2.push({
            id: this.custContactList[z][0],
            name: this.custContactList[z][2],
            custDepartment: this.custContactList[z][3],
            jobTitle: this.custContactList[z][4],
            mobileNo1: this.custContactList[z][5],
            mobileNo2: this.custContactList[z][6],
            phoneNo: this.custContactList[z][7],
            email: this.custContactList[z][8],
            status: this.custContactList[z][9],
          })

        }
      }

      this.dynamicArray.splice(0, this.dynamicArray.length);

      for (var l = 0; l < this.newDynamic2.length; l++) {
        this.dynamicArray.push(this.newDynamic2[l]);
      }
    }

  }

  getCoustomerContactList() {
    // this.createCustomerService.getCustomerContactList().subscribe(data => {
    //   console.log(data)
    //   this.custContactList=data;
    //   for(var c=0;c< Object.keys(this.custContactList).length;c++){
    //     this.arrayCustContactListId1.push(this.custContactList[c].customerId)
    //   }
    //   this.getCustUpdatedContactList();
    // },
    //   error => console.log(error));

    this.createCustomerService.getCustomerContactListByCustId(this.customerIdForCustName).subscribe(data => {
      console.log(data)
      this.custContactList = data;
      for (var c = 0; c < Object.keys(this.custContactList).length; c++) {
        this.arrayCustContactListId1.push(this.custContactList[c][1])
      }
      this.getCustUpdatedContactList();

    },
      error => console.log(error));

  }





  /////////////////////////////// save contact list data /////////////////////////

  onSave(i) {
    this.customerContactListObj.customerId = this.customerIdForCustName;
    this.customerContactListObj.personName = this.dynamicArray[i].name;
    //  this.customerContactListObj.department=this.dynamicArray[i].CUST_DEPARTMENT;
    this.customerContactListObj.jobTitle = this.dynamicArray[i].jobTitle;
    this.customerContactListObj.mobileNumber1 = this.dynamicArray[i].mobileNo1;
    this.customerContactListObj.mobileNumber2 = this.dynamicArray[i].mobileNo2;
    this.customerContactListObj.phoneNumber = this.dynamicArray[i].phoneNo;
    this.customerContactListObj.email = this.dynamicArray[i].email;
    this.customerContactListObj.status = this.dynamicArray[i].status;

    // this.createCustomerService.getCustomerContactList().subscribe(data => {
    //   console.log(data)
    //   this.custContactList=data;
    //   for(var c=0;c< Object.keys(this.custContactList).length;c++){
    //     this.arrayCustContactListId.push(this.custContactList[c].customerId)
    //   }
    this.checkCustContactPresentOrNot(i);
    // },
    //   error => console.log(error));



  }

  departmentSelect($event) {
    this.customerContactListObj.department = this.customer.department;
  }


  checkCustContactPresentOrNot(i) {


    console.log("this.custContactList" + this.custContactList);
    console.log("this.custContactList" + this.dynamicArray[i].id);
    for (this.c = 0; this.c < Object.keys(this.custContactList).length; this.c++) {

      if (typeof this.custContactList[this.c] != 'undefined') {
        //    if(this.custContactList[this.c].customerId==this.customerIdForCustName && this.custContactList[this.c].mobileNumber1== this.customerContactListObj.mobileNumber1 && this.custContactList[this.c].email==this.customerContactListObj.email)
        if (this.custContactList[this.c][0] == this.dynamicArray[i].id) {
          this.createCustomerService.updateCustomerContact(this.custContactList[this.c][0], this.customerContactListObj).subscribe(data => {
            console.log(data)
          },
            error => console.log(error));
          break;
        }
      }
    }

    if (this.c >= Object.keys(this.custContactList).length) {
      this.createCustomerService.createCustomerContact(this.customerContactListObj).subscribe(data => {
        console.log(data)

      },
        error => console.log(error));

      this.newDynamic2.splice(0, this.newDynamic2.length);
      this.arrayCustContactListId1.splice(0, this.arrayCustContactListId1.length);
      this.getCoustomerContactList();

    }
  }


  /////////////////// customer attachments ///////////////////////////


  public onFileChangedForDocument(event) {

    this.selectedFileForCustAttachment = event.target.files[0];
    this.selectedFileForQuotation_quo = $("#docFile")[0].files[0]
    this.selectedFileForCustAttachment_trial = event.target;
    console.log(" this.selectedFileForCustAttachment " + this.selectedFileForCustAttachment)

  }

  onUploadForDocument() {

    console.log(this.selectedFileForCustAttachment);

    const uploadFileData = new FormData();

    //loook from here put this code in document component not here look this line reader.readAsText($("#quotation_item_file")[0].files[0]);
    //  var csv = $("#fileUploadCSV").val();
    var trial_for_target = this.selectedFileForCustAttachment_trial;

    debugger;
    this.addCustAttach();
    // var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;//regex for Checking valid files csv of txt 

    // if (regex.test($("#quotation_item_file").val().toLowerCase())) {
    //   if (typeof (FileReader) != "undefined") {
    //     var reader = new FileReader();
    //     reader.onload = (e) => {
    //       //   (<FileReader>e.target).result  let fileUrl = (<FileReader>event.target).result;
    //       //     var rows = ((<FileReader>trial_for_target).result as string).split("\r\n");
    //       this.trial_file_reader = ((<FileReader>e.target).result as string).split("\r\n");
    //       //     console.log(".......rows"+rows);
    //       console.log("...trial" + this.trial_file_reader)
    //     }
    //     reader.readAsText(this.selectedFileForQuotation_quo);
    //   }
    // }

  }


  addCustAttach() {

    // get customers list    
    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerIdForCustName == this.customerObj[i].id) {
        this.custNameForNewFileName = this.customerObj[i].name;
      }
    }

    var originalName = this.selectedFileForCustAttachment.name;
    const uploadImageData = new FormData();
    var newFileName = this.customerIdForCustName + "_" + this.custNameForNewFileName + "_" + this.selectedFileForCustAttachment.name;

    uploadImageData.append('imageFile', this.selectedFileForCustAttachment, newFileName);

    this.createCustomerService.custAttachmentsFileUpload(uploadImageData).subscribe(data => {
      console.log(data);
      this.custAttachUpload = data;

      this.custAttachObj.custId = this.customerIdForCustName;
      this.custAttachObj.documentName = this.docName;
      this.custAttachObj.documentType = this.docType;
      this.custAttachObj.documentNumber = this.docNumber;
      this.custAttachObj.originalFileName = originalName;

      this.createCustomerService.createCustomerAttachments(this.custAttachObj).subscribe(data => {
        this.custAttach = data;
        console.log(data);
        alert("Document uploaded successfully !!");
        this.getCustomerAttachmentsFiles();
      },
        error => console.log(error));
    });


  }


  getCustomerAttachmentsFiles() {

    this.createCustomerService.getCustomerAttachLists(this.customerIdForCustName).subscribe(data => {

      this.custAttachFiles = data;
      this.retrieveResonse = data;
      console.log(data);

      this.custAttachFiles;

      // for (var i = 0; i < this.retrieveResonse.length; i++) {
      //   this.arrayBase64Data.push(this.retrieveResonse[i].picbyte)
      //   console.log(this.arrayBase64Data);
      // }
      // //  this.base64Data = this.retrieveResonse.picByte;
      // for (var i = 0; i < this.arrayBase64Data.length; i++) {
      //   this.arrayRetrievedImage.push('data:image/jpeg;base64,' + this.arrayBase64Data[i])
      //   console.log(this.arrayRetrievedImage);

      // }

      // this.retrievedImage = this.arrayRetrievedImage[0];

    },
      error => console.log(error));

  }



  // getImage() {
  //   if(this.imageName.includes('.png') || this.imageName.includes('.jpg') || this.imageName.includes('.jpeg') || this.imageName.includes('.gif'))
  //   {    
  //        this.httpClient.get('http://localhost:8090/image/get/' + this.imageName).subscribe(res => {
  //        this.retrieveResonse = res;
  //        this.base64Data = this.retrieveResonse.picByte;
  //        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
  //      }
  //    );
  //   }
  //   else{

  //     this.httpClient.get('http://localhost:8090/image/getfile/' + this.imageName).subscribe(res => {
  //       this.retrieveResonse = res;
  //       this.base64Data = this.retrieveResonse.picByte;
  //       this.base64Datafile = atob(this.base64Data); 
  //       const retrievedImage1 = 'data:application/pdf;base64,' + this.base64Data;
  //  //     window.open('data:application/pdf;base64,' + this.base64Datafile);

  //       const downloadLink = document.createElement("a");
  //       const fileName = this.imageName;

  //       downloadLink.href =  retrievedImage1;
  //       downloadLink.download = fileName;
  //       downloadLink.click();
  //     }
  //   );


  //   }
  // }


  getSelectedFile(event) {

    this.createCustomerService.getCustAttchDetailsToGetDoc(this.custAttachFiles[event]).subscribe(data => {
      console.log(data);

      // this.retrieveResonse = data;
      // this.base64Data = this.retrieveResonse.picByte;
      // this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;

      // var img = '<img src="'+this.retrievedImage+'">';
      // var popup = window.open();
      // popup.document.write(img);   
      
      
      if (this.retrieveResonse.type.includes('/png') || this.retrieveResonse.type.includes('/jpg') || this.retrieveResonse.type.includes('/jpeg') || this.retrieveResonse.type.includes('/gif')) {

        this.base64Data = this.retrieveResonse.picByte;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        var img = '<img src="' + this.retrievedImage + '">';
        var popup = window.open();
        popup.document.write(img);
        popup.document.title = this.suppAttachFiles[event].originalFileName;

      }
      else {

        this.base64Data = this.retrieveResonse.picByte;
        this.base64Datafile = atob(this.base64Data);
        const retrievedImage1 = 'data:application/pdf;base64,' + this.base64Data;

        let pdfWindow = window.open("")
        pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
          encodeURI(this.base64Data) + "'></iframe>"
        )

      }









    })

  }


  //////////////////////////////// pop-up screens /////////////////////////

  openModal1() {
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()

  }

  openModal2() {
    const buttonModal2 = document.getElementById("openModalButton2")
    console.log('buttonModal2', buttonModal2)
    buttonModal2.click()
    this.getCustomerAttachmentsFiles();
  }



}
