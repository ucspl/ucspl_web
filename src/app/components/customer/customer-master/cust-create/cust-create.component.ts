

import { Component, OnInit } from '@angular/core';

import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { Customer, RegionAreaCode, CreateCustomerService, BillingCreditPeriod, DynamicGridCustContact, Taxes, CustomerTaxId, TaxpayerTypeForCustomer, Country, States } from '../../../../services/serviceconnection/create-service/create-customer.service';

import { StudentService } from '../../../../services/student/student.service';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-cust-create',
  templateUrl: './cust-create.component.html',
  styleUrls: ['./cust-create.component.css']
})
export class CustCreateComponent implements OnInit {

  addCustomerForm: FormGroup;


  regionAreaCode: RegionAreaCode = new RegionAreaCode();
  arrayRegionAreaCodeName: Array<any> = [];
  arrayRegionAreaCodeId: Array<any> = [];

  billingCreditPeriod: BillingCreditPeriod = new BillingCreditPeriod();
  arrayBillingCreditPeriodName: Array<any> = [];
  arrayBillingCreditPeriodId: Array<any> = [];


  taxpayerTypeData: TaxpayerTypeForCustomer = new TaxpayerTypeForCustomer();
  arrayTaxpayerTypeName: Array<any> = [];
  arrayTaxpayerTypeId: Array<any> = [];
  arrayStatesCountryId: Array<any> = [];
  taxpayerTypeSelected: any;

  country: Country = new Country();
  arrayCountryName: Array<any> = [];
  arrayCountryId: Array<any> = [];

  statesData: States = new States();
  arrayStatesName: Array<any> = [];
  arrayStatesId: Array<any> = [];
  arrayStatesCode: Array<any> = [];
  arrState: Array<{ id: number, nm: string }> = [];

  customerTaxId: CustomerTaxId[] = [];
  taxesObject: Taxes = new Taxes();
  arrayTaxesName: Array<any> = [];
  arrayTaxesId: Array<any> = [];
  taxesDisplay: any;
  taxesIdsSelected: Array<any> = [];

  taxesData: Array<{ id: number, name: string }> = [];
  arrayCustomerTax: Array<{ customerId: number, taxId: number }> = [];

  public dateValue = Date.now();
  dateString = '';
  format = 'dd/MM/yyyy';

  opt: any; len: any;
  newSel: HTMLElement;
  static optionCount: number = 0;
  arrayApplicableTaxes: Array<any> = [];
  customerListArray: Array<any> = [];

  triale: any;
  trialPhoneNumber1: any;
  trialPhoneNumber2: any;
  phoneNumberCountryNm1:any;
  phoneNumberCountryNm2:any;
  prefixCountryCode1: any;
  prefixCountryCode2: any;
  // public dateValue = new Date();


  name: any;
  companyNameForSysReference: string;
  customers: Array<any> = [];
  customerDept: Array<any> = [];
  customerGST: Array<any> = [];
  static customerNames: Array<any> = [];
  customerIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObj: Customer = new Customer();
  customerObject: any;
  static customerNameLength: number;


  parentCustNames: Array<any> = [];
  parentCustIds: Array<any> = [];

  custSuffix: any;
  customerNameTrial: string;

  selectCountryId;
  countryList: any[] = [
    { name: '--Select Country--', states: [] },
    // { name: 'India', states: [{ id: 1, nm: 'Andhra Pradesh' }, { id: 2, nm: 'Arunachal Pradesh' }, { id: 3, nm: 'Assam' }, { id: 4, nm: 'Bihar' }, { id: 5, nm: 'Chhattisgarh' }, { id: 6, nm: 'Goa' }, { id: 7, nm: 'Gujarat' }, { id: 8, nm: 'Haryana' }, { id: 9, nm: 'Himachal Pradesh' }, { id: 10, nm: 'Jammu and Kashmir' }, { id: 11, nm: 'Jharkhand' }, { id: 12, nm: 'Karnataka' }, { id: 13, nm: 'Kerala' }, { id: 14, nm: 'Ladakh' }, { id: 15, nm: 'Madhya Pradesh' }, { id: 16, nm: 'Maharashtra' }, { id: 17, nm: 'Manipur' }, { id: 18, nm: 'Meghalaya' }, { id: 19, nm: 'Mizoram' }, { id: 20, nm: 'Nagaland' }, { id: 21, nm: 'Odisha' }, { id: 22, nm: 'Puducherry' }, { id: 23, nm: 'Punjab' }, { id: 24, nm: 'Rajasthan' }, { id: 25, nm: 'Sikkim' }, { id: 26, nm: 'Tamil Nadu' }, { id: 27, nm: 'Telangana' }, { id: 28, nm: 'Tripura' }, { id: 29, nm: 'Uttar Pradesh' }, { id: 30, nm: 'Uttarakhand' }, { id: 31, nm: 'West Bengal' }] },

  ];
  states: Array<any>;


  countries = [{
    id: '8f8c6e98',
    name: 'USA',
    code: 'USD'
  },
  {
    id: '169fee1a',
    name: 'Canada',
    code: 'CAD'
  },
  {
    id: '3953154c',
    name: 'UK',
    code: 'GBP'
  }]
  parentCustomerWindow: number;
  statusOfCustomer: number;


  dynamicArray: Array<DynamicGridCustContact> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  parentCustNotSelected: number;
  typeOfCustomer: number;
  loginUserName: any;

  count: number;

  mobileNumber1: any;
  mobileNumber2: any;
  displaySezNote: number;
  stateCodeForSelectedState: any;

  get ordersFormArray() {
    return this.addCustomerForm.controls.orders as FormArray;
  }

  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService, private createCustomerService: CreateCustomerService) {



    this.addCustomerForm = this.formBuilder.group({

      isParent: [],
      parentReference: [],
      name: ['', [Validators.required, ValidationService.nameValidator, ValidationService.companyNameValidator]],
      custSuffix: [],
      companyNameForSysReference: [],
      department: [],
      panNo:  ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo1: ['', [optionalValidator([Validators.maxLength(2), ValidationService.StateCodeValidator])]],
      gstNo2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.panNumberValidator])]],
      gstNo3: ['', [optionalValidator([Validators.maxLength(3), ValidationService.lastThreeDigitOfGSTValidator])]],
      email1: ['', [Validators.required, ValidationService.emailValidator]],
      email2: ['', [optionalValidator([Validators.maxLength(255), ValidationService.emailValidator])]],
      mobileNumber1: ['', [Validators.required, ValidationService.phoneValidator]],
      mobileNumber2: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      landlineNumber: [],
      addressLine1: [],
      addressLine2: [],
      status: [],
      country: [],
      state: [],
      city: [],
      pin: [],
      regionAreaCodes: [],
      vendorCode: [],
      billCreditPeriod: [],
      passFailRemark: [],
      taxes: [],
      applicableTaxes: [],
      taxpayerType: [],
      sezNote: [],
      checkedBy: [],
      createdBy: [],
      modifiedBy: [],
      calibratedProcedure: [],
      referencedUsed: [],
      expandedUncertainty: [],
      note: [],
      lut: [],
      sez: [],
      masterlhs: [],
      uuclhs: [],
      parentCustomer: [],
      customFields: [],
      orders: new FormArray([])

    });



  }

  addCheckboxes() {

    this.taxesData.forEach(() => this.ordersFormArray.push(new FormControl(false)));

  }



  ngOnInit() {

    this.customer.status = 1;

    this.newDynamic1 = [{
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    }]


    this.newDynamic = {

      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phoneNo: "",
      email: "",
      status: ""
    };
    this.dynamicArray.push(this.newDynamic);


    // get region area code list
    this.createCustomerService.getRegionAreaCodeList().subscribe(data => {

      this.regionAreaCode = data;

      for (var i = 0, l = Object.keys(this.regionAreaCode).length; i < l; i++) {
        this.arrayRegionAreaCodeName.push(this.regionAreaCode[i].regionAreaCodeName);

        this.arrayRegionAreaCodeId.push(this.regionAreaCode[i].regionAreaCodeId);

      }


    },
      error => console.log(error));


    // get customers list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObj = data;



      for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
        this.customers.push(this.customerObj[i].name);

        this.customerIds.push(this.customerObj[i].id);
        this.customerDept.push(this.customerObj[i].department);
        this.customerGST.push(this.customerObj[i].gstNo);
        if (this.customerObj[i].isParent == 1) {

          this.parentCustNames.push(this.customerObj[i].name);

          this.parentCustIds.push(this.customerObj[i].id);

        }
      }
    },
      error => console.log(error));




    // get billing credit period list
    this.createCustomerService.getBillingCreditPeriodList().subscribe(data => {

      this.billingCreditPeriod = data;

      for (var i = 0, l = Object.keys(this.billingCreditPeriod).length; i < l; i++) {
        this.arrayBillingCreditPeriodName.push(this.billingCreditPeriod[i].billingCreditPeriodName);

        this.arrayBillingCreditPeriodId.push(this.billingCreditPeriod[i].billingCreditPeriodId);

      }


    },
      error => console.log(error));


    // get countries list
    this.createCustomerService.getCountryList().subscribe(data => {

      this.country = data;

      for (var i = 0, l = Object.keys(this.country).length; i < l; i++) {
        this.arrayCountryName.push(this.country[i].countryName);
        this.arrayCountryId.push(this.country[i].countryId);

      }


    },
      error => console.log(error));



    // get states list
    this.createCustomerService.getStatesList().subscribe(data => {

      this.statesData = data;

      for (var i = 0, l = Object.keys(this.statesData).length; i < l; i++) {
        this.arrayStatesName.push(this.statesData[i].stateName);
        this.arrayStatesId.push(this.statesData[i].stateId);
        this.arrayStatesCode.push(this.statesData[i].stateCode);
        this.arrayStatesCountryId.push(this.statesData[i].countryId);

        this.arrState.push({ id: this.statesData[i].stateId, nm: this.statesData[i].stateName });

        //     console.log(this.arrayStatesName)
      }
      console.log(this.arrState)
      this.countryAndStates();

    },
      error => console.log(error));



    // get taxpayer type list
    this.createCustomerService.getTaxpayerTypeList().subscribe(data => {

      this.taxpayerTypeData = data;

      for (var i = 0, l = Object.keys(this.taxpayerTypeData).length; i < l; i++) {
        this.arrayTaxpayerTypeName.push(this.taxpayerTypeData[i].taxpayerTypeName);
        this.arrayTaxpayerTypeId.push(this.taxpayerTypeData[i].taxpayerTypeId);

      }

    },
      error => console.log(error));



    // get taxes list
    this.createCustomerService.getTaxesList().subscribe(data => {

      this.taxesObject = data;

      for (var i = 0, l = Object.keys(this.taxesObject).length; i < l; i++) {
        this.arrayTaxesName.push(this.taxesObject[i].taxName);
        this.arrayTaxesId.push(this.taxesObject[i].taxId);

        this.taxesData.push({ id: this.taxesObject[i].taxId, name: this.taxesObject[i].taxName });


      }
      this.taxesDisplay = 1;
      this.addCheckboxes();
    },
      error => console.log(error));

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));


    // mobileNumber code

    var input1 = document.querySelector("#mobileNumber1");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();

    var countryData1 = iti1.getSelectedCountryData();


    this.trialPhoneNumber1 = 91;
    this.phoneNumberCountryNm1 = "in";
    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      document.getElementById("country1").innerText = this.trialPhoneNumber1;

      console.log(this.trialPhoneNumber1);
    });


    var input2 = document.querySelector("#mobileNumber2");
    var country2 = $('#country2');
    var iti2 = (<any>window).intlTelInput(input2, {

      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti2.setNumber("+917733123456");
    var number2 = iti2.getNumber();

    var countryData2 = iti2.getSelectedCountryData();


    this.trialPhoneNumber2 = 91;
    this.phoneNumberCountryNm2 = "in";
    input2.addEventListener('countrychange', (e) => {

      var a2 = country2.val(iti2.getSelectedCountryData().dialCode);
      console.log(a2);
      this.trialPhoneNumber2 = iti2.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm2 = iti2.getSelectedCountryData().iso2;
    });



  }


  countryAndStates() {

    for (var i = 0; i < this.arrayCountryName.length; i++) {
      if (this.arrayCountryId[i] == this.arrayStatesCountryId[i]) {
        this.countryList.push({ name: this.arrayCountryName[i], states: this.arrState })
      }
    }

  }

  checkTypeOfCustomer() {
    if (this.customer.isParent == 0) {
      this.parentCustNotSelected = 0;
    }
    else
      this.parentCustNotSelected = 1;
  }

  checkCustName() {
    if (this.custSuffix == "Limited")
      this.companyNameForSysReference = this.name + " " + "LTD";

    if (this.custSuffix == "Private Limited")
      this.companyNameForSysReference = this.name + " " + "Pvt LTD";

    if (this.custSuffix == "Proprietorship")
      this.companyNameForSysReference = this.name + "";

    if (this.custSuffix == "Patnership")
      this.companyNameForSysReference = this.name + "";

    if (this.custSuffix == "LLP")
      this.companyNameForSysReference = this.name + "";
  }

  findStateCodeForState() {
    for (var s = 0; s < this.arrayStatesName.length; s++) {
      if (this.customer.state == this.arrayStatesName[s] && this.customer.country == "India") {
        this.stateCodeForSelectedState = this.arrayStatesCode[s];
      }

    }

  }

  taxpayerTypeValue(e) {
    // if (e == "Sez supplies with payment" || e == "Sez supplies without payment")
    //   this.displaySezNote = 1
    // else
    //   this.displaySezNote = 0


    switch (e) {
      case "Regular": 
        this.findStateCodeForState();
        if (this.stateCodeForSelectedState == 27) {
          this.customer.cgst = 1;
          this.customer.sgst = 1;
          this.customer.igst = 0;
          this.displaySezNote = 0
        }
        else {
          this.customer.cgst = 0;
          this.customer.sgst = 0;
          this.customer.igst = 1;
          this.displaySezNote = 0
        }
        break;

      case "Sez supplies with payment": 
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 1;
        this.displaySezNote = 1
        break;

      case "Sez supplies without payment": 
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 1
        break;

      case "LUT": 
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 0
        break;

      default:
        this.customer.cgst = 0;
        this.customer.sgst = 0;
        this.customer.igst = 0;
        this.displaySezNote = 0

    }

  }

  changeCountry(count) {
    this.states = this.countryList.find(con => con.name == count).states;

  }

  getTaxesId() {
    this.taxesIdsSelected = this.addCustomerForm.value.orders
      .map((checked, i) => checked ? this.taxesData[i].id : null)
      .filter(v => v !== null);

  }

  onSubmit() {
    this.save();
  }

  save() {


    this.addCustomerForm.value.calibratedProcedure = $('#calibratedprocedure').is(':checked') ? $('#calibratedprocedure').val() : 0
    this.customer.calibratedProcedure = this.addCustomerForm.value.calibratedProcedure;
    this.addCustomerForm.value.referencedUsed = $('#referencedused').is(':checked') ? $('#referencedused').val() : 0
    this.customer.referencedUsed = this.addCustomerForm.value.referencedUsed;
    this.addCustomerForm.value.expandedUncertainty = $('#expandeduncertainty').is(':checked') ? $('#expandeduncertainty').val() : 0
    this.customer.expandedUncertainty = this.addCustomerForm.value.expandedUncertainty;
    this.addCustomerForm.value.note = $('#note').is(':checked') ? $('#note').val() : 0
    this.customer.note = this.addCustomerForm.value.note;
    this.addCustomerForm.value.lut = $('#lut').is(':checked') ? $('#lut').val() : 0
    this.customer.lut = this.addCustomerForm.value.lut;
    this.addCustomerForm.value.sez = $('#sez').is(':checked') ? $('#sez').val() : 0
    this.customer.sez = this.addCustomerForm.value.sez;
    this.addCustomerForm.value.masterlhs = $('#masterlhs').is(':checked') ? $('#masterlhs').val() : 0
    this.customer.masterlhs = this.addCustomerForm.value.masterlhs;
    this.addCustomerForm.value.uuclhs = $('#uuclhs').is(':checked') ? $('#uuclhs').val() : 0
    this.customer.uuclhs = this.addCustomerForm.value.uuclhs;
    this.addCustomerForm.value.parentCustomer = $('#parentcustomer').is(':checked') ? $('#parentcustomer').val() : 0
    this.customer.parentCustomer = this.addCustomerForm.value.parentCustomer;
    this.addCustomerForm.value.customFields = $('#customfields').is(':checked') ? $('#customfields').val() : 0
    this.customer.customFields = this.addCustomerForm.value.customFields;

    this.addCustomerForm.value.gstNo2 = this.addCustomerForm.value.panNo;
    this.customer.sezNote = this.addCustomerForm.value.sezNote;

    if (typeof this.addCustomerForm.value.custSuffix == 'undefined' || this.addCustomerForm.value.custSuffix == null) {
      this.customer.name = this.customer.name
    }
    else {
      this.customer.name = this.customer.name + " " + this.addCustomerForm.value.custSuffix;
    }


    //  this.customer=this.AddCustomerform.value;             

    for (var i = 0; i < Object.keys(this.regionAreaCode).length; i++) {
      if (this.regionAreaCode[i].regionAreaCodeName == this.addCustomerForm.value.regionAreaCodes) {
        this.customer.regionAreaCode = this.regionAreaCode[i].regionAreaCodeId;
      }
    }

    for (var i = 0; i < Object.keys(this.billingCreditPeriod).length; i++) {
      if (this.billingCreditPeriod[i].billingCreditPeriodName == this.addCustomerForm.value.billCreditPeriod) {
        this.customer.billingCreditPeriod = this.billingCreditPeriod[i].billingCreditPeriodId;
      }
    }

    for (var i = 0; i < Object.keys(this.taxpayerTypeData).length; i++) {
      if (this.taxpayerTypeData[i].taxpayerTypeName == this.addCustomerForm.value.taxpayerType) {
        this.customer.taxpayerType = this.taxpayerTypeData[i].taxpayerTypeId;
      }
    }

    this.getTaxesId();

    var gst;
    var gst1 = this.addCustomerForm.value.gstNo1.toString();
    var gst2 = this.addCustomerForm.value.gstNo2.toString();
    var gst3 = this.addCustomerForm.value.gstNo3.toString();

    gst = gst1.concat(gst2.toString());
    this.customer.gstNo = gst.concat(gst3);

    this.customer.applicableTaxes = "";   //not useful 

    if (this.customer.cgst != 1 || typeof this.customer.cgst === 'undefined')
      this.customer.cgst = 0;
    if (this.customer.igst != 1 || typeof this.customer.igst === 'undefined')
      this.customer.igst = 0;
    if (this.customer.sgst != 1 || typeof this.customer.sgst === 'undefined')
      this.customer.sgst = 0;


    //save Mobile number with country code

    this.customer.mobileNumber1 = "+" + this.trialPhoneNumber1 + this.mobileNumber1;

    if (typeof this.mobileNumber2 != 'undefined') {
      this.customer.mobileNumber2 = "+" + this.trialPhoneNumber2 + this.mobileNumber2;
    } 
    else {
      this.customer.mobileNumber2 = null;
    }

    for (var i = 0, l = Object.keys(this.customerObj).length; i < l; i++) {
      if (this.customerObj[i].name == this.addCustomerForm.value.parentReference) {
        this.customer.parentReference = this.customerObj[i].id;
      }
    }

    this.customer.createdBy = this.loginUserName;
    this.customer.name = this.companyNameForSysReference;
    this.customer.dateCreated = this.dateValue;
    this.customer.countryPrefixForMobile1 = this.phoneNumberCountryNm1;
    this.customer.countryPrefixForMobile2 = this.phoneNumberCountryNm2;

    this.checkForDuplicate();

    //  this.clearForm();
  }

  clearForm() {

    window.location.reload();
  }

  /////////////////////////////////// Check for dublicancy /////////////////////////////

  checkForDuplicate() {
    for (this.count = 0; this.count < Object.keys(this.customerObj).length; this.count++) {

      if (this.customers[this.count] == this.customer.name && this.customerDept[this.count] == this.customer.department || this.customerGST[this.count] == this.customer.gstNo) {
        alert("This Customer is already present");
        break;
      }
      else {

      }
    }

    if (this.count >= Object.keys(this.customerObj).length) {


      this.createCustomerService.createCustomer(this.customer).subscribe(data => {

        this.customerObject = data;


        alert("Customer created Successfully!!");

        for (var e = 0; e < this.taxesIdsSelected.length; e++) {
          this.arrayCustomerTax.push({ customerId: this.customerObject.id, taxId: this.taxesIdsSelected[e] })
        }

        this.customerTaxId.push(...this.arrayCustomerTax);

        this.createCustomerService.addCustomerInCustomerAndTaxJunctionTable(this.customerTaxId).subscribe(data => {

        },
          error => console.log(error));


        this.clearForm();
      },
        error => console.log(error));


    }
  }

  addTaxAndCustomerId() {
    for (var count = 0; count < this.customerTaxId.length; count++) {
      this.createCustomerService.addCustomerInCustomerAndTaxJunctionTable(this.customerTaxId[count]).subscribe(data => {
        console.log(data)
      },
        error => console.log(error));
    }
    //  alert("Customer id and tax id successfully!!");
    this.clearForm()
  }

  //////////////////////////////////////////////// Code For Adding New Row //////////////////////////////////



  AddRow() {

    this.newDynamic = {
      name: "",
      custDepartment: "",
      jobTitle: "",
      mobileNo1: "",
      mobileNo2: "",
      phone_no: "",
      email: "",
      status: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }


  DeleteRow(index) {
    if (this.dynamicArray.length == 1) {
      console.log("delete row");

      return false;
    } else {
      this.dynamicArray.splice(index, 1);

      return true;
    }
  }

  //////////////////////// save contacts data /////////////////////////////////////

onSave(i){
    
}

  ////////////////// pop-up screens /////////////////////////

  openModal1() {
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    buttonModal1.click()
  }

  openModal2() {
    const buttonModal2 = document.getElementById("openModalButton2")
    console.log('buttonModal2', buttonModal2)
    buttonModal2.click()
  }


}
