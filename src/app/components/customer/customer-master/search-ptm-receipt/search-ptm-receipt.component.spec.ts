import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPtmReceiptComponent } from './search-ptm-receipt.component';

describe('SearchPtmReceiptComponent', () => {
  let component: SearchPtmReceiptComponent;
  let fixture: ComponentFixture<SearchPtmReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPtmReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPtmReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
