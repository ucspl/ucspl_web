
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { Customer } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CustomerbyName, SearchCustomerService } from '../../../../services/serviceconnection/search-service/search-customer.service';

@Component({
  selector: 'app-cust-search',
  templateUrl: './cust-search.component.html',
  styleUrls: ['./cust-search.component.css']
})
export class CustSearchComponent implements OnInit {

  searchCustomerForm:FormGroup;
  p: number = 1;
  itemsPerPage:number=5;

  customerByName:CustomerbyName = new CustomerbyName();
  customerObj:Customer = new Customer();

  model : any={};    
  emp:any;  

  customersArray:Array<any> =[];
  customersName:Array<any> =[];
  customersDepartmentName:Array<any> =[];
  customersAddressLine1:Array<any> =[];
  customersAddressLine2:Array<any> =[];
  customersCountry:Array<any> =[];
  customersState:Array<any> =[];
  customersCity:Array<any> =[];
  customersPin:Array<any> =[];

  custObject:Customer =new Customer();

  customersIds:Array<any>=[];
  customerIdLength:number;
 
  customerIdForCustName:number;
  status: any;
  
  constructor(private formBuilder: FormBuilder, private router: Router, private searchCustomerService:SearchCustomerService) {

    this.searchCustomerForm = this.formBuilder.group({
      
      name: ['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      status:[],
      department:['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      address:['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      itemsPerPage:[]
    })
   }


  ngOnInit() {

    this.status="1";
  }
  searchandmodifycustomer(){
    alert(" searchcustomer");
  }

  
showData()  
{  
  this.searchCustomerService.showData().subscribe((res) => {  
  //  this.emp=res; 
    this.customerObj =res; 
    console.log(this.customerObj); 
  
    this.listOfCustomers();
})  
}  
searchData() {  
 debugger;  

 this.customerByName= this.searchCustomerForm.value;

 if( this.customerByName.name=="" && this.customerByName.department=="" && this.customerByName.address==""){
   alert("Please enter criteria to search customer")
 }
 else{


 
   var regex = /^[A-Za-z0-9 ]+$/
 
   //Validate TextBox value against the Regex.                           
   var isValid = regex.test(this.searchCustomerForm.value.name);
   if (!isValid) {
      // alert("Contains Special Characters.");                                 // this for & and And
       var str = this.searchCustomerForm.value.name;  
       var re = /&/gi;  
       var newstr = str.replace(re, "and");  
       console.log(newstr); 
       this.customerByName.nameAnd=newstr;
   }
   else{
    
    var convertedName=this.searchCustomerForm.value.name.toLowerCase();

    if(convertedName.includes('and'))
    {
      var str = convertedName;  
      var re = /and/gi;  
      var newstr = str.replace(re, "&");  
      console.log(newstr); 
      this.customerByName.name=convertedName;
      this.customerByName.nameAnd=newstr;
 //     this.SearchCustomerform.value.name=newstr;
    }
 /*  else if(this.SearchCustomerform.value.name.includes('AND'))
    {
      var str = this.SearchCustomerform.value.name;  
      var re = /AND/gi;  
      var newstr = str.replace(re, "&");  
      console.log(newstr); 
      this.customerbyname.name_and=newstr;
      this.SearchCustomerform.value.name=newstr;
    }
*/
   }

   if((!this.searchCustomerForm.value.name.includes('and')) && (!this.searchCustomerForm.value.name.includes('&')))
     this.customerByName.nameAnd="";
   

 this.searchCustomerService.searchData(this.customerByName).subscribe((res: any) => {  
          
      this.emp=res;   
      console.log(this.emp);   
  })  

  this.showData();
}
}  

listOfCustomers()
{

  this.searchCustomerService.showData().subscribe(data => {

    this.customersArray=data;
    this.customerObj=data;
    console.log("..." +this.customerObj);
    console.log("...trial" +this.customersArray);

    for(var i=0;i< Object.keys(this.customersArray).length;i++)
      {
        if(this.customersArray[i][22]==0)
        {
          this.customersArray[i][22]="Inactive";
        }          
        else
           this.customersArray[i][22]="Active";
      }
     
 
  }, 
  error => console.log(error));
}

updateCustomer(e)
{
  console.log(this.itemsPerPage);
  console.log(this.p);
  console.log(e);
  e=((this.itemsPerPage * (this.p-1))+(e));
  console.log("index will be...."+e);

  this.customerIdForCustName=parseInt(e);

  this.searchCustomerService.setIdForCustomerName(this.customersArray[this.customerIdForCustName][0]);

// get customer details for id
  this.searchCustomerService.getCustomerDetailsById(this.customersArray[this.customerIdForCustName][0]).subscribe(data => {

  this.custObject = data;
  console.log(this.custObject);
  this.searchCustomerService.setCustomerCountryForMobileNo1( this.custObject.countryPrefixForMobile1);
  this.searchCustomerService.setCustomerCountryForMobileNo2( this.custObject.countryPrefixForMobile2);
  this.router.navigateByUrl('/nav/custupdate')
},
  error => console.log(error));
  
}


}

