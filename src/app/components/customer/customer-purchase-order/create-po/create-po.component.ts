import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { Customer, CreateCustomerService } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { DynamicGrid, User } from '../../../../services/serviceconnection/create-service/create-user.service';
import { CreateCustomerPurchaseOrder, CreateCustPoService } from '../../../../services/serviceconnection/create-service/create-cust-po.service';
import { FinancialYear, Instrument } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateInvoiceService } from '../../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateSalesService } from '../../../../services/serviceconnection/create-service/create-sales.service';
declare var $: any;

@Component({
  selector: 'app-create-po',
  templateUrl: './create-po.component.html',
  styleUrls: ['./create-po.component.css']
})
export class CreatePoComponent implements OnInit {

  createCustomerPurchaseForm: FormGroup;
  frm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  customers: Array<any> = [];
  customersIds: Array<any> = [];
  customer: Customer = new Customer();
  customerObject: Customer = new Customer();
  trialCustomerWithAddress: Array<{ customerName: string, name: string, address: string }> = [];

  customerNames: Array<any> = [];
  customerNameLength: number;
  customerDepartments: Array<any> = [];

  createCustomerPurchaseOrder: CreateCustomerPurchaseOrder = new CreateCustomerPurchaseOrder();
  customerName: any;
  documentNumberForPo: any;

  ////////////for doc no declaration//////////
  finalDocumentNumber: string;
  prefixForQuotation: any;
  dateOfDocumentNo: any;
  counterForDocumentNoString: string;
  counterForDocumentNo: any;
  public dateValue1 = Date.now();
  dateValue2: any;
  documentNumber: string;
  loginUserName: any;
  dateValue3: any;
  dateValue4: any;
  dateValue5: any;
  dateOfDocumentNo1: any;
  dateOfDocumentNo2: any;

  financialYearData: FinancialYear = new FinancialYear();
  startDate: any;
  idForFromDate: any;
  poDate: any;
  validToDate: any;
  documentNumberFromSales: any;
  salesQuotationDocumentVariableArrayForSearchSales: any;

  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];
  customerNameForPo: any;
  createPoData: Object;
  trial1: any;
  windowRef2 = null;

  constructor(private formBuilder: FormBuilder, public datepipe: DatePipe, private router: Router, private createCustomerService: CreateCustomerService, private createCustPoService: CreateCustPoService, private createInvoiceService: CreateInvoiceService, private createSalesService: CreateSalesService) {

    this.createCustomerPurchaseForm = this.formBuilder.group({
      documentType: [],
      customerName: [],
      custPoNo: [],
      poDate: [],
      validToDate: []
    });

    this.frm = this.formBuilder.group({
      cName: [''],
    })

  }

  ngOnInit() {

    this.documentNumberFromSales = JSON.parse(localStorage.getItem('DocumentNumberForPo'));
    console.log("document no for sales:" + this.documentNumberFromSales);
    this.customerNameForPo = JSON.parse(localStorage.getItem('customerNameForPo'));
    this.customerName = this.customerNameForPo;
    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    //get customer list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerObject = data;
      console.log(this.customerObject);

      for (var i = 0, l = Object.keys(this.customerObject).length; i < l; i++) {
        this.customers.push(this.customerObject[i].name + " / " + this.customerObject[i].department);
        console.log("customers : " + this.customers);
        this.customerNames.push(this.customerObject[i].name);
        this.customerDepartments.push(this.customerObject[i].department);

        this.trialCustomerWithAddress.push({ customerName: this.customerObject[i].name, name: this.customerObject[i].name + " / " + this.customerObject[i].department, address: this.customerObject[i].addressLine1 + "," + this.customerObject[i].addressLine2 + "," + this.customerObject[i].city + "," + this.customerObject[i].pin + "," + this.customerObject[i].state + "," + this.customerObject[i].country });
        this.customersIds.push(this.customerObject[i].id);
        console.log("customer id : " + this.customersIds);
      }

    },
      error => console.log(error));

    // get financial year dates list
    this.createInvoiceService.getFinancialYearDates().subscribe(data => {
      this.financialYearData = data;
      console.log(this.financialYearData);

      for (var i = 0, l = Object.keys(this.financialYearData).length; i < l; i++) {
        if (this.financialYearData[i].status == 1) {
          this.startDate = this.financialYearData[i].fromDate;
          this.idForFromDate = this.financialYearData[i].id;
          break;
        }

      }
    },
      error => console.log(error));

    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));

  }


  onSubmit() {
    alert(" Po created successfully..")
    console.log(this.createCustomerPurchaseForm.value);

    this.createCustPoService.documentNumberForPurchaseOrder().subscribe(data => {
      this.createCustomerPurchaseOrder.documentNumber = data.number;
      this.addOneToDocumentNumber();
    })
  }

  save() {

    this.createCustomerPurchaseOrder = this.createCustomerPurchaseForm.value;
    this.poDate = this.convertStringToDate(this.createCustomerPurchaseForm.value.poDate);
    this.validToDate = this.convertStringToDate(this.createCustomerPurchaseForm.value.validToDate);
    this.createCustomerPurchaseOrder.poDate = this.datepipe.transform(this.poDate, 'dd-MM-yyyy');
    this.createCustomerPurchaseOrder.validToDate = this.datepipe.transform(this.validToDate, 'dd-MM-yyyy');
    this.createCustomerPurchaseOrder.documentNumber = this.finalDocumentNumber;
    this.createCustomerPurchaseOrder.createdBy = this.loginUserName;

    for (var i = 0, l = Object.keys(this.customerObject).length; i < l; i++) {
      if (this.customers[i] == this.createCustomerPurchaseForm.value.customerName) {
        this.createCustomerPurchaseOrder.customerId = this.customerObject[i].id;
        console.log(this.createCustomerPurchaseOrder.customerId);
      }
    }

    this.createCustPoService.createCustomerPurchaseOrder(this.createCustomerPurchaseOrder).subscribe(data => {
      console.log(data)
      this.createPoData = data;
      this.createCustPoService.setCreatePoDataWithId(this.createPoData);
      localStorage.setItem('createPoData', JSON.stringify(this.createPoData));

      if (typeof this.documentNumberFromSales != undefined && this.documentNumberFromSales != null && this.documentNumberFromSales != "") {
        this.getSrNoForSearchSales();
      }
      else {
        this.router.navigateByUrl('/createpodoc');
      }
    },
      error => console.log(error));
  }

  // get sr no for serach sales
  getSrNoForSearchSales() {

    this.createSalesService.sendDocNoOfSalesQuotationDocument(this.documentNumberFromSales).subscribe(data => {
      console.log("*********sr no for doc no********" + data);

      this.createSalesService.getSrNoForSalesQuotationDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.salesQuotationDocumentVariableArrayForSearchSales = data;

        if (this.salesQuotationDocumentVariableArrayForSearchSales.length != 0) {

          for (var z = 0; z < this.salesQuotationDocumentVariableArrayForSearchSales.length; z++) {
            this.newDynamic1.push({
              srNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][2],
              instruName: this.salesQuotationDocumentVariableArrayForSearchSales[z][3],
              description: this.salesQuotationDocumentVariableArrayForSearchSales[z][4],
              idNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][5],
              accrediation: this.salesQuotationDocumentVariableArrayForSearchSales[z][6],
              range: this.salesQuotationDocumentVariableArrayForSearchSales[z][7],
              sacCode: this.salesQuotationDocumentVariableArrayForSearchSales[z][8],
              quantity: this.salesQuotationDocumentVariableArrayForSearchSales[z][9],
              rate: this.salesQuotationDocumentVariableArrayForSearchSales[z][10],
              discountOnItem: this.salesQuotationDocumentVariableArrayForSearchSales[z][11],
              amount: this.salesQuotationDocumentVariableArrayForSearchSales[z][13]
            })
          }

          this.dynamicArray.splice(0, 1);
          for (var l = 1; l < this.newDynamic1.length; l++) {
            this.convertInsruIdToName(l);
            this.dynamicArray.push(this.newDynamic1[l]);
          }
        }

        localStorage.setItem('dynamicArrayForPo', JSON.stringify(this.dynamicArray));
        this.clearForm();

      })

    })
  }


  convertInsruIdToName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;
      }
    }
  }

  customerNameValid() {

    for (var i = 0; i < this.customers.length; i++) {
      if (this.customers[i] == this.customerName) {
        $("#customerNameValidation").html(" ");
        break;
      }
      else {
        $("#customerNameValidation").html("customer name does not exist in list");

      }
    }
  }


  clearForm() {

    this.openChildWindow();

  }

  openChildWindow() {
    localStorage.setItem('customerNameForPo', JSON.stringify(this.customerNameForPo));
    localStorage.setItem('documentNumberFromSales', JSON.stringify(this.documentNumberFromSales));
    var url = '/updatepodoc';
    this.windowRef2 = window.open(url, "child2", "width=900,height=420,top=100");
    this.windowRef2.addEventListener("message", this.receiveMessage2.bind(this), false);

  }
  receiveMessage2(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }

  poDoc() {
    this.router.navigateByUrl('/createpodoc')
  }
  moveToSearchsales(){
    this.router.navigateByUrl('/nav/searchsales');
   // this.router.navigate(['/nav/searchpo']);
  }

  addOneToDocumentNumber() {

    this.prefixForQuotation = this.createCustomerPurchaseOrder.documentNumber.slice(0, 9);
    this.dateOfDocumentNo1 = this.createCustomerPurchaseOrder.documentNumber.slice(9, 11);
    this.dateOfDocumentNo2 = this.createCustomerPurchaseOrder.documentNumber.slice(11, 13);
    this.counterForDocumentNoString = this.createCustomerPurchaseOrder.documentNumber.slice(14, 19);

    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);

    this.dateValue2 = this.startDate;
    this.dateValue3 = Date.now();
    this.dateValue4 = this.datepipe.transform(this.dateValue3, 'yy');
    this.dateValue5 = this.datepipe.transform(this.dateValue3, 'dd-MM');

    if (this.dateValue2 == this.dateValue5) {
      if (this.dateValue4 == this.dateOfDocumentNo2) {
        this.counterForDocumentNo = 0;
        this.dateOfDocumentNo1 = parseInt(this.dateOfDocumentNo1);
        this.dateOfDocumentNo2 = parseInt(this.dateOfDocumentNo2);
        this.dateOfDocumentNo1 = this.dateOfDocumentNo1 + 1;
        this.dateOfDocumentNo2 = this.dateOfDocumentNo2 + 1;

      }
    }

    this.counterForDocumentNo = this.counterForDocumentNo + 1;

    var output = [], n = 1, padded;
    padded = ('0000' + this.counterForDocumentNo).slice(-5);
    output.push(padded);

    this.counterForDocumentNoString = this.counterForDocumentNo.toString();
    this.counterForDocumentNoString = "_" + this.counterForDocumentNoString;

    var a = this.prefixForQuotation + this.dateOfDocumentNo1 + this.dateOfDocumentNo2 + "_" + padded;
    this.finalDocumentNumber = a;

    console.log(this.finalDocumentNumber);
    this.createCustomerPurchaseOrder.documentNumber = a;

    this.save();

  }


  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }


}