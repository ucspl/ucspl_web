import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePoDocumentComponent } from './update-po-document.component';

describe('UpdatePoDocumentComponent', () => {
  let component: UpdatePoDocumentComponent;
  let fixture: ComponentFixture<UpdatePoDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePoDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePoDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
