
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Customer, CreateCustomerService } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateSalesAndQuotation, termsForSalesAndQuotation, EnquiryAttachment, JsonForCsvFile, QuotationItemFile, CreateSalesService, DynamicGrid, SalesQuotationDocument, SalesQuotationDocumentCalculation } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { ReferenceForSalesQuotation, Instrument, TypeOfQuotationForSalesAndQuotation, Terms, Branch } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { SearchSalesService } from '../../../../services/serviceconnection/search-service/search-sales.service';
import { CreateCustomerPurchaseOrder, CreateCustPoService, POAttachments, PoDocumentCalculation, PurchaseOrderDocument } from '../../../../services/serviceconnection/create-service/create-cust-po.service';

declare var $: any;

@Component({
  selector: 'app-create-po-document',
  templateUrl: './create-po-document.component.html',
  styleUrls: ['./create-po-document.component.css']
})
export class CreatePoDocumentComponent implements OnInit {
  [x: string]: any;

  manageDocumentForm: FormGroup;
  public reqdocParaDetailsForm: FormGroup;
  salesAndQuotationForDocument: CreateSalesAndQuotation = new CreateSalesAndQuotation();
  createSalesandQuotation: CreateSalesAndQuotation = new CreateSalesAndQuotation();
  createSalesInfoForSearchSalesObject: CreateSalesAndQuotation = new CreateSalesAndQuotation();

  quotationItemFileData1: QuotationItemFile = new QuotationItemFile();
  quotationItemFileData2: QuotationItemFile = new QuotationItemFile();

  customerAndTermId: termsForSalesAndQuotation = new termsForSalesAndQuotation();
  printTermsId: any;

  salesQuotationDocumentObject: SalesQuotationDocument = new SalesQuotationDocument();
  salesQuotationDocumentObject1: SalesQuotationDocument = new SalesQuotationDocument();
  purchaseOrderDocumentObject: PurchaseOrderDocument = new PurchaseOrderDocument();

  salesQuotationDocumentCalculationObject: SalesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();
  salesQuotationDocumentCalculationObject1: SalesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();
  poDocumentCalculationObject: PoDocumentCalculation = new PoDocumentCalculation();

  j: any = 0;
  l: any = 0;
  tl: Array<number> = [0];
  k: any;
  n: any = 0;
  sot: any = 0;
  sumAll: any = 0;
  tCode: string;
  subTotal: number = 0;
  subTotal1: number = 0;
  amountArray: Array<any> = [];
  amountArray1: Array<any> = [];
  data: string[] = [];
  arrayOfDescription: string[] = [];
  arrayOfIdNo: string[] = [];
  arrayOfAccrediation: string[] = [];
  arrayOfRangeAccuracy: string[] = [];
  arrayOfSacCode: string[] = [];

  arrayOfQuantity: Array<number> = [];
  arrayOfRate: Array<number> = [];
  arrayOfDiscountOnItem: Array<number> = [];
  arrayOfDiscountOnTotal: Array<number> = [];
  arrayOfAmount: Array<number> = [];
  arrayOfTerms: Array<number> = [];

  selectedFileForQuotation: any;
  referenceList: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  referenceName: Array<any> = [];
  referenceId: Array<any> = [];

  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];

  customersName: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  arrayForCsv: Array<any> = [];

  customer: Customer = new Customer();
  customerDataObject: Customer = new Customer();
  static customerNameLength: number;

  retrievedImage: any;
  base64Data: any;
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  name: String;
  quotationNumber: string;
  prefixQuotationNumber1: any;
  salesDocumentInfo: Array<any> = [];
  salesQuotationDocumentForm: FormGroup

  selectedRow: Number;
  checkboxes: boolean[];

  kindAttentionTrial: string;

  trialRows: FormArray;
  arr: FormArray;
  serverData = [];
  discountOnSubtotal: number = 0;
  discountOnSubtotal1: number = 0;
  total: number = 0;
  netValue: any;
  netValue1: any;
  total1: number = 0;
  cgstAmount: any;
  sgstAmount: number;
  cgstAmount1: any;
  sgstAmount1: number;
  customerReferenceTrial: any;
  netValueInWords: string;
  customerNameTrial: any;
  customerPanNo: any;
  customerGstNo: any;
  customerAddressline1: any;
  customerAddressline2: any;
  customerCountry: any;
  customerState: any;
  customerCity: any;
  customerPin: any;
  dateOfQuotation1: string;
  customerReferenceNo1: number;
  referenceDate1: string;
  kindAttachment1: number;
  stateCode: any;
  salesAndQuotationForDocument1: CreateSalesAndQuotation;
  salesAndQuotationForDocument1ForManageDocument: CreateSalesAndQuotation;
  igstAmount: number;
  igstAmount1: number;

  termsIdArray: Array<any> = [];
  termsIdArray1: any;
  termsIdArray2: Array<any> = [];
  termsIdArray3: Array<any> = [];
  termsIdArray4: Array<any> = [];

  selectTagEvent: any;
  m: any = 0;
  s: any = 0;
  taxesArray: Array<any> = [];
  taxesArrayName: Array<any> = [];

  dateString = '';
  format = 'dd/MM/yyyy';
  alive = true;
  public dateValue = new Date();
  dateOfQuotation: string;
  custRefNo: string;
  refDate: string;
  archieveDate: string;
  kindAttentionTrial1: string;
  documentNumber: string;
  typeOfQuotation: string;
  contactNo: string;
  email: string;
  status: string;
  remark: string;
  createdBy: string;
  manageDocumentVariable: any;
  salesQuotationDocumentVariable: any;
  id: any;
  customerNameArray: Array<any> = [];
  itemIdArray: Array<any> = [];
  termDescriptionArray: Array<any> = [];
  termDescriptionArray1: Array<any> = [];

  public termValue: string;

  paymentTermList: Array<{ id: number, name: string }> = [];
  termsList1: Array<{ id: number, name: string }> = [];
  termDescriptionList: Array<{ id: number, name: string }> = [];
  taxesList: Array<{ name: string, taxAmount: number }> = [];
  taxesList1: Array<{ name1: string, taxAmount1: number }> = [];
  taxesTotal: number = 0;

  trialCustomerWithAddress: Array<{ name: string, address: string }> = [];
  branchWithDescription: Array<{ description: string, branch: string }> = [];
  static customerDepartments: Array<any> = [];
  width: any;
  height: any;
  originalDocNo: string;

  srNoArray: Array<number> = [];

  salesQuotationDocumentVariableArray: Array<any> = [];
  poDocumentArray: Array<any> = [];

  salesQuotationDocumentCalculationVariableArray: Array<any> = [];
  poDocumentCalculationVariableArray: Array<any> = [];
  searchSalesQuotationDocumentCalculationArray: Array<any> = [];
  quotationNumberWithPrefix: string;
  documentNumberFromSearchSales: string;
  salesQuotationDocumentVariableArrayForSearchSales: any;
  headerInformationArrayForSearchSales: Array<any> = [];
  termsIdForSeArchSalesArray: Array<number> = [];
  paymentTermsId: number;
  termDescriptionArray2: Array<number> = [];

  quotationTypeData: TypeOfQuotationForSalesAndQuotation = new TypeOfQuotationForSalesAndQuotation();
  docTypeName: Array<any> = [];
  docTypeId: Array<any> = [];
  disc: number;
  draft: number;
  archieved: number;
  rejected: number;
  approved: number;
  submitted: number;
  countryNm1: any;
  trialPhoneNumber1: any;
  paymentTerms: any;
  box1: number = 0;
  box2: number;
  dropdownList = [];
  dropdownList1 = [];
  dropdownList2 = [];
  trialDropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  termsData: Terms = new Terms();
  paymentTermsName: Array<any> = [];
  paymentTermsIds: Array<any> = [];
  normalTermsName: Array<any> = [];
  normalTermsIds: Array<any> = [];
  termsId: any;
  paymentTermsArray: Array<any> = [];
  paymentTermDescription: any;
  itemId: any;
  remarkForTerm: string;
  branch1: Branch = new Branch();
  branchesName: Array<any> = [];
  branchesIds: Array<any> = [];
  branchesDescription: Array<any> = [];

  cgstDisplay: number;
  sgstDisplay: number;
  igstDisplay: number;
  taxAmount: any;
  dataFromCreatePo: CreateCustomerPurchaseOrder = new CreateCustomerPurchaseOrder();
  createPoForDoc: CreateCustomerPurchaseOrder = new CreateCustomerPurchaseOrder();
  createPoDataForManageDocument: CreateCustomerPurchaseOrder = new CreateCustomerPurchaseOrder();
  poDocumentVariable: CreateCustomerPurchaseOrder = new CreateCustomerPurchaseOrder();

  selectedFile: File;
  selectedFileForCustAttachment: File;
  selectedFileForCustAttachmentTrial: any;
  selectedFileForQuotationQuo: any;

  poAttachObj: POAttachments = new POAttachments();
  poAttachUpload: POAttachments = new POAttachments();
  custAttach: any;
  documentNumberForSales: string;
  closeQuantity:number=0;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService, private createSalesService: CreateSalesService, private createCustomerService: CreateCustomerService, private searchSalesService: SearchSalesService, private createCustPoService: CreateCustPoService, public datepipe: DatePipe) {

    this.manageDocumentForm = this.formBuilder.group({
      documentType: [],
      documentNumber: [],
      validToDate: [],
      createdBy: [],
      approvedBy: [],
      status: [],
      updateHistory:[]

    });

    this.reqdocParaDetailsForm = this.formBuilder.group({
      Rows: this.formBuilder.array([this.initRows()])
    });

  }

  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];

  @ViewChild('focus') input: ElementRef;
  public toggleButton: boolean = true;

  ngOnInit() {
    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.createdBy= this.loginUserName;

    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial.length != 0) {

        for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].instruName != null) {
            if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
              this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].quantity !== values[i].quantity
              || this.dynamicArrayTrial[i].rate !== values[i].rate || this.dynamicArrayTrial[i].amount !== values[i].amount || this.dynamicArrayTrial[i].customerPartNo !== values[i].customerPartNo|| this.dynamicArrayTrial[i].hsnNo !== values[i].hsnNo)
            ) {
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";

            }
          }
        }
      }
    });

    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial.length != 0) {

        for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].instruName != null) {
            if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
              this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].accrediation !== values[i].accrediation || this.dynamicArrayTrial[i].rangeFrom !== values[i].rangeFrom || this.dynamicArrayTrial[i].rangeTo !== values[i].rangeTo || this.dynamicArrayTrial[i].quantity !== values[i].quantity
              || this.dynamicArrayTrial[i].rate !== values[i].rate || this.dynamicArrayTrial[i].discountOnItem !== values[i].discountOnItem || this.dynamicArrayTrial[i].amount !== values[i].amount)
            ) {
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";

            }
          }
        }
      }
    });


    //get branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branch1 = data;
      console.log(this.branch1);
      for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
        this.branchesName.push(this.branch1[i].branchName);
        this.branchesIds.push(this.branch1[i].branchId);
        this.branchesDescription.push(this.branch1[i].description);

        this.branchWithDescription.push({ description: this.branch1[i].description, branch: this.branch1[i].branchName });
      }
    },
      error => console.log(error));


    //get instrument list
    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));



    $(function () {
      // on init
      $(".table-hideable .hide-col").each(HideColumnIndex);

      // on click
      $('.hide-column').click(HideColumnIndex)

      function HideColumnIndex() {
        var $el = $(this);
        var $cell = $el.closest('th,td')
        var $table = $cell.closest('table')

        // get cell location - https://stackoverflow.com/a/4999018/1366033
        var colIndex = $cell[0].cellIndex + 1;

        // find and hide col index
        $table.find("tbody tr, thead tr")
          .children(":nth-child(" + colIndex + ")")
          .addClass('hide-col');

        // show restore footer
        $table.find(".footer-restore-columns").show()
      }

      // restore columns footer
      $(".restore-columns").click(function (e) {
        var $table = $(this).closest('table')
        $table.find(".footer-restore-columns").hide()
        $table.find("th, td")
          .removeClass('hide-col');

      })

      $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
      })

    })


    this.newDynamic1 = [{
      uniqueId: "",
      srNo: "",
      instruName: "",
      description: "",
      idNo: "",
      range: "",
      quantity: "",
      rate: "",
      customerPartNo: "",
      hsnNo:"",
      amount: "",
      discountOnItem: "",
      rangeFrom:"",
      rangeTo:""

    }]

    this.newDynamic = {
      uniqueId: "",
      srNo: "",
      instruName: "",
      description: "",
      idNo: "",
      range: "",
      quantity: "",
      rate: "",
      customerPartNo: "",
      hsnNo:"",
      amount: "",
      discountOnItem: "",
      rangeFrom:"",
      rangeTo:""

    };
    this.dynamicArray.push(this.newDynamic);


    // get customer list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerDataObject = data;
      console.log("customer data" + this.customerDataObject);

      this.getSalesAndQuotationDocumentInfo();

      for (var i = 0, l = Object.keys(this.customerDataObject).length; i < l; i++) {
        this.customersName.push(this.customerDataObject[i].name + " / " + this.customerDataObject[i].department);
        console.log("customers : " + this.customersName);

        this.customerNameArray.push(this.customerDataObject[i].name);
        console.log("CustomerList:" + this.customerNameArray);

        this.trialCustomerWithAddress.push({ name: this.customerDataObject[i].name + " / " + this.customerDataObject[i].department, address: this.customerDataObject[i].addressLine1 + "," + this.customerDataObject[i].addressLine2 + "," + this.customerDataObject[i].city + "," + this.customerDataObject[i].pin + "," + this.customerDataObject[i].state + "," + this.customerDataObject[i].country });
        console.log(this.trialCustomerWithAddress);
        this.customersIds.push(this.customerDataObject[i].id);

        console.log("customer id : " + this.customersIds);
      }
      if (this.customersIds.length == Object.keys(this.customerDataObject).length)
        this.getInformation();
    },
      error => console.log(error));

  }

  //////////////////////////////code for button color change////////////////////////
  fieldGlobalIndex(index) {

    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.reqdocParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      srNo: [],
      instruName: [],
      description: [],
      idNo: [],
      range: [],
      quantity: [],
      rate: [],
      amount: [],
      customerPartNo: [],
      discountOnItem: [],
      hsnNo:[],
      rangeFrom:[],
      rangeTo:[]
    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow1(index: number) {
    this.srNoToDelete = index + 1;

    for (var i = 0; i < this.poDocumentArray.length; i++) {
      if (this.poDocumentArray[i][2] == this.srNoToDelete) {
        this.indexToDelete = this.poDocumentArray[i][0];
        this.createCustPoService.deleteRowForSrNo(this.indexToDelete).subscribe(data => {
          console.log("*********deleted row********" + data);
        })
        break;
      }
    }

    this.poDocumentArray.splice(index, 1);
    this.dynamicArrayTrial.splice(index, 1);
    this.formArr.removeAt(index);

    this.calculationAfterCustChange()

  }


  ngAfterViewInit() {
    this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial != null && typeof this.dynamicArrayTrial != 'undefined' && this.dynamicArrayTrial.length != 0) {
        if (this.dynamicArrayTrial[l].instruName != null && this.dynamicArrayTrial[l].instruName != "") {   //if (this.dynamicArrayTrial[l].inwardInstruNo != "") {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        break;
      }
    }
  }


  // get customer information from create po screen
  getInformation() {

    for (var i = 0; i < Object.keys(this.customersIds).length; i++) {
      if (this.createPoForDoc.customerId == this.customersIds[i]) {
        this.customerNameTrial = this.customerDataObject[i].name + " / " + this.customerDataObject[i].department;
        this.customerPanNo = this.customerDataObject[i].panNo;
        this.customerGstNo = this.customerDataObject[i].gstNo;
        this.customerAddressline1 = this.customerDataObject[i].addressLine1;
        this.customerAddressline2 = this.customerDataObject[i].addressLine2;
        this.customerCountry = this.customerDataObject[i].country;
        this.customerState = this.customerDataObject[i].state;
        this.customerCity = this.customerDataObject[i].city;
        this.customerPin = this.customerDataObject[i].pin;
        this.cgst = this.customerDataObject[i].cgst;
        this.sgst = this.customerDataObject[i].sgst;
        this.igst = this.customerDataObject[i].igst;
        this.poDate = this.dataFromCreatePo.poDate;
        this.ucsplPoNo = this.dataFromCreatePo.documentNumber;
        this.custPoNo = this.dataFromCreatePo.custPoNo;
        this.documentType=this.dataFromCreatePo.documentType;

             if(this.documentType=="Customer PO Open"){
               this.closeQuantity=1;
             }
             else{
              this.closeQuantity=0;
            }
        
        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
        break;
      }

      console.log(this.salesDocumentInfo);
    }
    this.splitGstAndQuotationNumber();
  }

  // To get quotation number with prefix
  splitGstAndQuotationNumber() {
    if (typeof this.customerGstNo != 'undefined') {
      this.stateCode = this.customerGstNo.slice(0, 2);
    }

    if (typeof this.salesAndQuotationForDocument.documentNumber != 'undefined') {
      this.originalDocNo = this.salesAndQuotationForDocument.documentNumber;
      this.quotationNumber = this.salesAndQuotationForDocument.documentNumber.slice(17, 22);
    }

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchId == this.salesAndQuotationForDocument.branchId)

        this.prefixQuotationNumber = this.branch1[i].description;
    }
    this.quotationNumberWithPrefix = this.prefixQuotationNumber + this.quotationNumber;
    this.salesDocumentInfo.push(this.stateCode);
    this.salesDocumentInfo.push(this.quotationNumber);
    console.log("salesDocumentInfo..." + this.salesDocumentInfo)
  }


  /////////////////////////////////Customer name change///////////////////////////

  customerNameChange() {
    for (var s = 0; s < this.customersName.length; s++) {
      if (this.customerNameTrial == this.customersName[s]) {
        this.customerPanNo = this.customerDataObject[s].panNo;
        this.customerGstNo = this.customerDataObject[s].gstNo;
        this.customerAddressline1 = this.customerDataObject[s].addressLine1;
        this.customerAddressline2 = this.customerDataObject[s].addressLine2;
        this.customerCountry = this.customerDataObject[s].country;
        this.customerState = this.customerDataObject[s].state;
        this.customerCity = this.customerDataObject[s].city;
        this.customerPin = this.customerDataObject[s].pin;
        this.cgst = this.customerDataObject[s].cgst;
        this.sgst = this.customerDataObject[s].sgst;
        this.igst = this.customerDataObject[s].igst;

        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
        this.calculationAfterCustChange();
        break;
      }
    }

  }

  ///////////////// get Quotation item file///////
  getSalesAndQuotationDocumentInfo() {
    this.dataFromCreatePo = this.createCustPoService.getCreatePoDataWithId();
    this.dataFromCreatePo = JSON.parse(localStorage.getItem('createPoData'));

    this.createPoForDoc.customerId = this.dataFromCreatePo.customerId
    console.log(this.salesAndQuotationForDocument);
    this.selectedFileForQuotation = this.createSalesService.getQuotationFileDocument();

  }


  ///////////////code for calculation//////////////////

  // if Item Discount is 0
  getAmount1(b) {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.subTotal = 0;
    this.taxesTotal = 0;
    this.taxAmount = 0;

    this.dynamicArray[b].amount = this.dynamicArray[b].quantity * this.dynamicArray[b].rate;
    this.dynamicArray[b].amount = parseFloat(this.dynamicArray[b].amount.toFixed(2));

    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('amount').patchValue(this.dynamicArray[b].amount);


    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }

      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    if (this.dynamicArray[b].discountOnItem != 0) {
      this.getAmount(b);
    }
  }

  // convert net value in words
  toWords() {

    var th = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
    var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    this.netValue = this.netValue.toString();
    this.netValue = this.netValue.replace(/[\, ]/g, '');
    if (this.netValue != parseFloat(this.netValue)) return 'not a number';
    var x = this.netValue.indexOf('.');
    if (x == -1) x = this.netValue.length;
    if (x > 15) return 'too big';
    var n = this.netValue.split('');
    var str = 'Rupees ';
    var sk = 0;
    for (var i = 0; i < x; i++) {
      if ((x - i) % 3 == 2) {
        if (n[i] == '1') {
          str += tn[Number(n[i + 1])] + ' ';
          i++;
          sk = 1;
        }
        else if (n[i] != 0) {
          str += tw[n[i] - 2] + ' ';
          sk = 1;
        }
      }
      else if (n[i] != 0) {
        str += dg[n[i]] + ' ';
        if ((x - i) % 3 == 0) str += 'hundred ';
        sk = 1;
      }


      if ((x - i) % 3 == 1) {
        if (sk) str += th[(x - i - 1) / 3] + ' ';
        sk = 0;
      }
    }
    if (x != this.netValue.length) {
      var y = this.netValue.length;
      str = str + 'and ';

      for (var j = x + 1; j < y; j++) {
        if ((y - j) % 3 == 2) {
          if (n[j] == '1') {
            str += tn[Number(n[j + 1])] + ' ';
            j++;
            sk = 1;
          }
          else if (n[j] != 0) {
            str += tw[n[j] - 2] + ' ';
            sk = 1;
          }
          else if (n[j] == 0 && n[j + 1] == 0) {
            str += 'Zero ';
            sk = 1;
            break;
          }
        }
        else if (n[j] != 0) {
          str += dg[n[j]] + ' ';
          if ((y - j) % 3 == 0) str += 'hundred ';
          sk = 1;
        }


        if ((y - j) % 3 == 1) {
          if (sk) str += th[(y - j - 1) / 3] + ' ';
          sk = 0;
        }
      }
      str += ' paise only';
    }
    this.netValueInWords = str.replace(/\s+/g, ' ');
  }

  toWords1() {

    var th = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
    var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    this.netValue1 = this.netValue1.toString();
    this.netValue1 = this.netValue1.replace(/[\, ]/g, '');
    if (this.netValue1 != parseFloat(this.netValue1)) return 'not a number';
    var x = this.netValue1.indexOf('.');
    if (x == -1) x = this.netValue1.length;
    if (x > 15) return 'too big';
    var n = this.netValue1.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
      if ((x - i) % 3 == 2) {
        if (n[i] == '1') {
          str += tn[Number(n[i + 1])] + ' ';
          i++;
          sk = 1;
        }
        else if (n[i] != 0) {
          str += tw[n[i] - 2] + ' ';
          sk = 1;
        }
      }
      else if (n[i] != 0) {
        str += dg[n[i]] + ' ';
        if ((x - i) % 3 == 0) str += 'hundred ';
        sk = 1;
      }


      if ((x - i) % 3 == 1) {
        if (sk) str += th[(x - i - 1) / 3] + ' ';
        sk = 0;
      }
    }
    if (x != this.netValue1.length) {
      var y = this.netValue1.length;
      str = 'rupees ' + str;

      for (var j = x + 1; j < y; j++) {
        if ((y - j) % 3 == 2) {
          if (n[j] == '1') {
            str += tn[Number(n[j + 1])] + ' ';
            j++;
            sk = 1;
          }
          else if (n[j] != 0) {
            str += tw[n[j] - 2] + ' ';
            sk = 1;
          }
          else if (n[j] == 0 && n[j + 1] == 0) {
            str += 'Zero ';
            sk = 1;
            break;
          }
        }
        else if (n[j] != 0) {
          str += dg[n[j]] + ' ';
          if ((y - j) % 3 == 0) str += 'hundred ';
          sk = 1;
        }


        if ((y - j) % 3 == 1) {
          if (sk) str += th[(y - j - 1) / 3] + ' ';
          sk = 0;
        }
      }
      str += ' paise only';
    }
    this.netValueInWords = str.replace(/\s+/g, ' ');
  }



  // if item discount is not 0
  getAmount(b) {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.subTotal = 0;
    this.taxesTotal = 0;
    this.taxAmount = 0;

    var discountedRate = (this.dynamicArray[b].rate - (this.dynamicArray[b].rate * (this.dynamicArray[b].discountOnItem / 100)));
    this.dynamicArray[b].amount = this.dynamicArray[b].quantity * discountedRate;
    this.dynamicArray[b].amount = parseFloat(this.dynamicArray[b].amount.toFixed(2));

    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('amount').patchValue(this.dynamicArray[b].amount);

    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }

      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    else {
      this.discountOnSubTotal();
    }

  }

  // if discount on subtotal is not 0
  discountOnSubTotal() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.total = this.subTotal - (this.subTotal * (this.discountOnSubtotal / 100));
    this.total = parseFloat(this.total.toFixed(2));

    if (this.cgst == 1) {
      this.cgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

    }
    if (this.igst == 1) {
      this.igstAmount = this.total * (18 / 100);
      this.taxAmount = this.taxAmount + this.total * (18 / 100);
      this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

    }
    if (this.sgst == 1) {
      this.sgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

    }

    this.netValue = this.total + this.taxAmount;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    console.log(this.netValue);
    this.toWords();
  }

  calculationAfterCustChange() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.subTotal = 0;
    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }

      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    else {
      this.discountOnSubTotal();
    }

  }

  ///////////////////// code to save details for sr no for one row/////////////////
  saveItemDetails(i) {
    document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
    console.log(this.reqdocParaDetailsForm.value);
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    console.log(this.dynamicArray);
    this.purchaseOrderDocumentObject.documentNumber = this.ucsplPoNo;
    this.purchaseOrderDocumentObject.srNo = i + 1;
    this.convertInstruNameToId(i);
    this.purchaseOrderDocumentObject.description = this.dynamicArray[i].description;
    this.purchaseOrderDocumentObject.idNo = this.dynamicArray[i].idNo;
    
    if(parseFloat(this.dynamicArray[i].rangeFrom)>=0 || parseFloat(this.dynamicArray[i].rangeFrom)<0){
      this.purchaseOrderDocumentObject.rangeFrom = this.dynamicArray[i].rangeFrom;
    } else{
      this.purchaseOrderDocumentObject.rangeFrom = "0";
    }

     if(parseFloat(this.dynamicArray[i].rangeTo)>=0 ||  parseFloat(this.dynamicArray[i].rangeTo)<0){
    
      this.purchaseOrderDocumentObject.rangeTo = this.dynamicArray[i].rangeTo;
    }
    else{ 
    this.purchaseOrderDocumentObject.rangeTo = "0";
    }
    
    this.purchaseOrderDocumentObject.quantity = this.dynamicArray[i].quantity;
    this.purchaseOrderDocumentObject.remainingQuantity = this.dynamicArray[i].quantity;
    this.purchaseOrderDocumentObject.rate = this.dynamicArray[i].rate;
    this.purchaseOrderDocumentObject.discountOnItem = this.dynamicArray[i].discountOnItem;
    this.purchaseOrderDocumentObject.amount = this.dynamicArray[i].amount;
    this.purchaseOrderDocumentObject.customerPartNo = this.dynamicArray[i].customerPartNo;
    this.purchaseOrderDocumentObject.hsnNo = this.dynamicArray[i].hsnNo;

    this.createCustPoService.sendDocNoOfPoDocument(this.ucsplPoNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
      
      this.createCustPoService.getSrNoForPoDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.poDocumentArray = data;
        alert("data saved succesfully");
        this.savePoDocItemDataWithSrNo();
        this.dynamicArrayTrial = this.dynamicArray;
  
      })
    })

    
  }

  convertInstruNameToId(k) {
    this.dynamicArray[k].instruName.trim();
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentName == this.dynamicArray[k].instruName) {
        this.purchaseOrderDocumentObject.instruName = this.instrumentList[i].instrumentId;

      }
    }

  }
  savePoDocItemDataWithSrNo() {
    if (this.poDocumentArray.length == 0) {
      this.createCustPoService.createPoDocumentForDatabase(this.purchaseOrderDocumentObject).subscribe(data => {
        console.log("*********data saved********" + data);
      })
    }

    else {
      for (this.n = 0; this.n < this.poDocumentArray.length; this.n++) {
        if (this.poDocumentArray[this.n][2] == parseInt(this.purchaseOrderDocumentObject.srNo)) {
          this.createCustPoService.updatePoDocumentForDatabase(this.poDocumentArray[this.n][0], this.purchaseOrderDocumentObject).subscribe(data => {
            console.log("data updated successfully" + data);
          }
          );
          break;
        }
      }
      if (this.n >= this.poDocumentArray.length) {
        this.createCustPoService.createPoDocumentForDatabase(this.purchaseOrderDocumentObject).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("data saved successfully");
        })
      }
    }


    this.createCustPoService.getSrNoForPoDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.poDocumentArray = data;
    })

  }


  //////////////////////////////////////////////// code for adding new row //////////////////////////////////

  addRow() {

    this.newDynamic = {
      uniqueId: "",
      srNo: "",
      instruName: "",
      description: "",
      idNo: "",
      accrediation: "",
      range: "",
      sacCode: "",
      quantity: "",
      rate: "",
      discountOnItem: "",
      amount: ""
    };
    this.dynamicArray.push(this.newDynamic);

    console.log(this.dynamicArray);
    return true;
  }

  //////////////////////////////////////////////// code for deleting row //////////////////////////////////
  deleteRow(index) {
    if (this.dynamicArray.length == 1) {

      return false;
    } else {
      this.dynamicArray.splice(index, 1);

      return true;
    }
  }




  /////////////////////////save all data///////////////////////
  saveAllData() {

    this.poForDocument1ForManageDocument = this.createCustPoService.getCreatePoDataWithId();
    this.poForDocument1ForManageDocument = JSON.parse(localStorage.getItem('createPoData'));
    this.poDocumentVariable = this.poForDocument1ForManageDocument;

    for (var s = 0; s < this.customersName.length; s++) {
      if (this.customerNameTrial == this.customersName[s]) {
        this.poDocumentVariable.customerId = this.customersIds[s];
        console.log(this.poDocumentVariable.customerId);
        break;
      }
    }

    this.id = this.poDocumentVariable.id;
    this.poDocumentVariable.custPoNo = this.custPoNo;;
    this.poDocumentVariable.documentNumber = this.ucsplPoNo;
    this.poDate = this.convertStringToDate(this.poDate);
    this.poDocumentVariable.poDate = this.datepipe.transform(this.poDate, 'dd-MM-yyyy');
    this.poDocumentVariable.approvedBy = this.approvedBy;

    this.poDocumentVariable.documentType = this.documentType;
    this.poDocumentVariable.createdBy = this.createdBy;

    this.validToDate = this.convertStringToDate(this.manageDocumentForm.value.validToDate);
    this.poDocumentVariable.validToDate = this.datepipe.transform(this.validToDate, 'dd-MM-yyyy');
    this.poDocumentVariable.updateHistory=this.updateHistory; 
    if(typeof this.poValue!='undefined'  && this.poValue!=null){
      this.poDocumentVariable.poValue=this.poValue.toString();
      this.poDocumentVariable.remainingPoValue=this.poValue.toString();
    }
   


    switch (this.status) {
      case 'Draft': this.poDocumentVariable.draft = 1;
        this.poDocumentVariable.archieved = 0;
        this.poDocumentVariable.rejected = 0;
        this.poDocumentVariable.approved = 0;
        this.poDocumentVariable.submitted = 0;
        break;
      case 'Archieved': this.poDocumentVariable.draft = 0
        this.poDocumentVariable.archieved = 1;
        this.poDocumentVariable.rejected = 0;
        this.poDocumentVariable.approved = 0;
        this.poDocumentVariable.submitted = 0;
        break;
      case 'Rejected': this.poDocumentVariable.draft = 0;
        this.poDocumentVariable.archieved = 0;
        this.poDocumentVariable.rejected = 1;
        this.poDocumentVariable.approved = 0;
        this.poDocumentVariable.submitted = 0;
        break;
      case 'Approved': this.poDocumentVariable.draft = 0;
        this.poDocumentVariable.archieved = 0;
        this.poDocumentVariable.rejected = 0;
        this.poDocumentVariable.approved = 1;
        this.poDocumentVariable.submitted = 0;
        break;
      case 'Submitted': this.poDocumentVariable.draft = 0;
        this.mpoDocumentVariable.archieved = 0;
        this.poDocumentVariable.rejected = 0;
        this.poDocumentVariable.approved = 0;
        this.poDocumentVariable.submitted = 1;
        break;

    }

    this.createCustPoService.updateCustomerPurchaseOrder(this.id, this.poDocumentVariable).subscribe(data => {
      console.log(data);
      localStorage.setItem("createPoData",JSON.stringify(data));
    })


    this.poDocumentCalculationObject.documentNumber = this.ucsplPoNo;
    this.poDocumentCalculationObject.subTotal = this.subTotal;
    this.poDocumentCalculationObject.discountOnSubtotal = this.discountOnSubtotal;
    this.poDocumentCalculationObject.total = this.total;
    this.poDocumentCalculationObject.net = parseFloat(this.netValue);
    if(this.cgstDisplay==1){
      this.poDocumentCalculationObject.cgst = this.cgstAmount;
    }
    else{
      this.poDocumentCalculationObject.cgst = 0;
    }
    if(this.igstDisplay==1){
      this.poDocumentCalculationObject.igst = this.igstAmount;
    }
    else{
      this.poDocumentCalculationObject.igst = 0;
    }
    if(this.sgstDisplay==1){
      this.poDocumentCalculationObject.sgst = this.sgstAmount;
    }
    else{
      this.poDocumentCalculationObject.sgst = 0;
    }
    
    // this.poDocumentCalculationObject.igst = this.igstAmount;
    // this.poDocumentCalculationObject.sgst = this.sgstAmount;


    this.createCustPoService.sendDocNoOfPoDocumentCalculation(this.ucsplPoNo).subscribe(data => {
      console.log("*********send doc no ********" + data);
    })

    this.createCustPoService.getDocDetailsForPODocumentCalculation().subscribe(data => {
      console.log("*********doc no details collection********" + data);
      this.poDocumentCalculationVariableArray = data;
      alert("Data Submitted Successfully!!")
      this.savePoDocCalcDetails();

    })

  }



  //////////////////save SQ Doc Calculation Detail
  savePoDocCalcDetails() {
    if (this.poDocumentCalculationVariableArray.length == 0) {
      this.createCustPoService.createPoDocumentCalculation(this.poDocumentCalculationObject).subscribe(data => {
        console.log("data updated successfully in Po Document Calculation table" + data);
      }
      );

    }
    else {
      this.createCustPoService.updatePoDocumentCalculation(this.poDocumentCalculationVariableArray[0][0], this.poDocumentCalculationObject).subscribe(data => {
        console.log("data updated successfully" + data);
      }
      );
    }

  }


  ///////////////// back to sales and quotation /////////////////////
  backToMenu() {
    window.localStorage.removeItem('dynamicArrayForPo');
    window.localStorage.removeItem('DocumentNumberForPo');
    window.localStorage.removeItem('customerNameForPo');
    window.localStorage.removeItem('documentNumberFromSales');
    self.close();
    this.router.navigate(['/nav/createpo']);

  }

  /******************************  code for manage document  ****************************************/

  manageDocument() {
    this.createPoDataForManageDocument = this.createCustPoService.getCreatePoDataWithId();
    this.createPoDataForManageDocument = JSON.parse(localStorage.getItem('createPoData'));
    this.createdBy= this.loginUserName;
    this.getInformationForManageDocument();
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)
    console.log("hi manage document")
    buttonModal1.click()
  }

  manageFooter() {
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }



  getInformationForManageDocument() {
    this.manageDocumentVariable = this.createPoDataForManageDocument;

    if (typeof this.createPoDataForManageDocument != 'undefined') {
      this.documentNumber = this.manageDocumentVariable.documentNumber;
      this.documentType = this.manageDocumentVariable.documentType;
      this.validToDate = this.manageDocumentVariable.validToDate;
      this.approvedBy = this.manageDocumentVariable.approvedBy;
      this.id = this.manageDocumentVariable.id;
    //  this.createdBy = this.manageDocumentVariable.createdBy;

    }
  }



  convertIdToName() {
    for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
      if (this.quotationTypeData[i].docTypeId == this.manageDocumentVariable.typeOfQuotation) {
        this.typeOfQuotation = this.quotationTypeData[i].docTypeName;
      }
    }
  }


  convertStringToDate(value) {

    if (typeof (value) === 'string' && value != null) {
      var dateV = value.slice(0, 2)
      var monthV = value.slice(3, 5)
      var yearV = value.slice(6, 10)
      var yyyyV = parseInt(yearV)
      var mmV = parseInt(monthV)
      var ddV = parseInt(dateV)

      mmV = mmV - 1;

      var dateFormat = new Date(yyyyV, mmV, ddV);
      return dateFormat;
    }
    else {
      return value;
    }
  }

  onSubmit() {
    this.saveAllData();
    this.manageDocumentVariable.documentNumber = this.ucsplPoNo;
    this.manageDocumentVariable.documentType = this.documentType;
    if(this.documentType=="Customer PO Open"){
      this.closeQuantity=1;
    }
    else{
      this.closeQuantity=0;
    }

    this.manageDocumentVariable.createdBy = this.createdBy;
    this.manageDocumentVariable.approvedBy = this.approvedBy;

    this.validToDate = this.convertStringToDate(this.manageDocumentForm.value.validToDate);
    this.manageDocumentVariable.validToDate = this.datepipe.transform(this.validToDate, 'dd-MM-yyyy');
    this.manageDocumentVariable.updateHistory=this.updateHistory;
    if(typeof this.poValue!='undefined'  && this.poValue!=null){
      this.manageDocumentVariable.poValue=this.poValue.toString();
      this.manageDocumentVariable.remainingPoValue=this.poValue.toString();

    }

    switch (this.status) {
      case 'Draft': this.manageDocumentVariable.draft = 1;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Archieved': this.manageDocumentVariable.draft = 0
        this.manageDocumentVariable.archieved = 1;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Rejected': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 1;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Approved': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 1;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Submitted': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 1;
        break;

    }


    alert("Data Updated succesfully!!");
    this.createCustPoService.updateCustomerPurchaseOrder(this.id, this.manageDocumentVariable).subscribe(data => {
      console.log(data);
      localStorage.setItem("createPoData",JSON.stringify(data));


    })
  }

  ////////////////////////Manage footer///////////////


  footerType(e) {
    console.log(e);
    this.selectTagEvent = e;
    if (this.selectTagEvent == 4) {
      this.disc = 1;
    }
    else if (this.selectTagEvent == 3)
      this.file = 1;

  }

  // get selected taxes from manage footer
  changeTaxes(e) {
    if (e == 1) {
      this.taxesArray.push(1);
      this.taxesArrayName.push("CGST 9%");
      this.taxesList.push({ name: "CGST 9%", taxAmount: 0 });
      this.getAmount(0);
    }

    if (e == 2) {
      this.taxesArray.push(2);
      this.taxesArrayName.push("IGST 18%");
      this.taxesList.push({ name: "IGST 18%", taxAmount: 0 });
      this.getAmount(0);
    }

    if (e == 3) {
      this.taxesArray.push(3);
      this.taxesArrayName.push("SGST 9%");
      this.taxesList.push({ name: "SGST 9%", taxAmount: 0 });
      this.getAmount(0);
    }

    console.log("this.taxesArray" + this.taxesArray);


  }

  //////////////////////// delete taxes /////////////////////
  deleteTaxes(i) {

    this.netValue = this.netValue - this.taxesList[i].taxAmount;
    this.taxesList[i].taxAmount = 0;

    if (this.taxesList[i].name == 'CGST 9%') {
      this.salesQuotationDocumentCalculationObject.cgst = this.taxesList[i].taxAmount;
    }
    else if (this.taxesList[i].name == 'IGST 18%') {
      this.salesQuotationDocumentCalculationObject.igst = this.taxesList[i].taxAmount;
    }
    else if (this.taxesList[i].name == 'SGST 9%') {
      this.salesQuotationDocumentCalculationObject.sgst = this.taxesList[i].taxAmount;
    }

    this.taxesArray.splice(i, 1);
    this.taxesList.splice(i, 1);
    this.taxesArrayName.splice(i, 1);
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.toWords();

    console.log("this.taxeslist" + this.taxesList);
    return true;
  }


  //////////////////delete discount on subtotal//////////////////
  deleteDiscOnSubtotal() {

    this.discountOnSubtotal = 0;
    this.total = this.subTotal;
    this.netValue = this.total;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.toWords();
    this.disc = 0;
    this.discountOnSubTotal();

  }



  ////////////////code for print/////////////////////

  printPage() {
    window.print();
  }

  printTable() {
    var tab = document.getElementById('tab');
    var style = "<style>";

    //   style = style + "table {width: 200px;font: 14px Calibri;}";
    //   style = style + "table, th, td {border: solid 5px #DDD; border-collapse: collapse; width: 200px;";
    //   style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "input[type=text] {width: 60%; boredr:0px}";
    style = style + "</style>";

    // var win = window.open('', '', 'height=700,width=700');
    // win.document.write(style);          //  add the style.
    // win.document.write(tab.outerHTML);
    // win.document.write(tab.innerHTML);
    // win.document.close();

    window.document.write(style);
    window.document.write(tab.innerHTML);

    window.print();
  }
  printDiv(divName) {

    //    $('form#myId input').bind("change", function() {
    //      var val = $(this).val();
    //      $(this).attr('value',val);
    //  });

    var style = "<style>";
    style = style + "table {width: 200px;font: 14px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse; width: 200px;";
    style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "input[type=text] {width: 60%; border: 1px solid white}";
    style = style + "</style>";

    var printContents = document.getElementById(divName).innerHTML;
    var printContents1 = document.getElementById(divName).innerHTML;

    var popupWin = window.open('', '_blank', 'width=300,height=300');
    popupWin.document.open();
    // popupWin.document.write('<html><head><link rel="stylesheet" href="sample.css" /></head><body onload="window.print()">' + printContents + '</body></html>');

    popupWin.document.write('<html><head>' + style + '</head><body onload="window.print()">' + printContents + '</body></html>');

    popupWin.document.close();
  };



  // /////////////////// customer PO attachments ///////////////////////////

  public onFileChangedForDocument(event) {

    this.selectedFileForCustAttachment = event.target.files[0];
    this.selectedFileForQuotationQuo = $("#docFile")[0].files[0]
    this.selectedFileForCustAttachmentTrial = event.target;
    console.log(" this.selectedFileForCustAttachment " + this.selectedFileForCustAttachment)

  }

  onUploadForDocument() {

    console.log(this.selectedFileForCustAttachment);
    const uploadFileData = new FormData();
    var trialForTarget = this.selectedFileForCustAttachmentTrial;
    $("#enquiryFile").html("File uploaded successfully!!!");
    debugger;
    this.addPoAttach();

  }

  addPoAttach() {

    var originalName = this.selectedFileForCustAttachment.name;
    const uploadImageData = new FormData();
    var newFileName = this.ucsplPoNo + "_" + this.selectedFileForCustAttachment.name;

    uploadImageData.append('imageFile', this.selectedFileForCustAttachment, newFileName);

    this.createCustPoService.PoAttachmentsFileUpload(uploadImageData).subscribe(data => {
      console.log(data);
      this.custAttachUpload = data;

      this.poAttachUpload.documentNumber = this.ucsplPoNo;
      this.poAttachUpload.originalFileName = originalName;
      this.poAttachUpload.documentName = this.documentName
      this.createCustPoService.createPoAttachments(this.poAttachUpload).subscribe(data => {
        this.custAttach = data;
        console.log(data);
        this.getPOAttachmentsFiles();
      },
        error => console.log(error));
    });

  }


  getPOAttachmentsFiles() {
    this.createCustPoService.getPoAttachLists(this.ucsplPoNo).subscribe(data => {
      this.poAttachFiles = data;
      this.retrieveResonse = data;
      console.log(data);
      this.poAttachFiles;
    },
      error => console.log(error));
  }

  getSelectedFile(event) {

    this.createCustPoService.getPoDetailsToGetDoc(this.poAttachFiles[event]).subscribe(data => {
      console.log(data);
      this.retrieveResonse = data;

      if (this.retrieveResonse.type.includes('/png') || this.retrieveResonse.type.includes('/jpg') || this.retrieveResonse.type.includes('/jpeg') || this.retrieveResonse.type.includes('/gif')) {

        this.base64Data = this.retrieveResonse.picByte;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        var img = '<img src="' + this.retrievedImage + '">';
        var popup = window.open();
        popup.document.write(img);
        popup.document.title = this.suppAttachFiles[event].originalFileName;

      }
      else {

        this.base64Data = this.retrieveResonse.picByte;
        this.base64Datafile = atob(this.base64Data);
        const retrievedImage1 = 'data:application/pdf;base64,' + this.base64Data;

        let pdfWindow = window.open("")
        pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
          encodeURI(this.base64Data) + "'></iframe>"
        )

      }
    })
  }

  deletePoFile(e) {

    this.indexToDeleteFile = this.poAttachFiles[e].purchaseOrderAttachmentId;

    this.createCustPoService.deleteAttachedPoFile(this.indexToDeleteFile).subscribe((res) => {
      this.deletedFile = res;
      console.log(this.deletedFile);
      this.poAttachFiles.splice(e, 1);

    })

  }

}

