import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoDocumentComponent } from './create-po-document.component';

describe('CreatePoDocumentComponent', () => {
  let component: CreatePoDocumentComponent;
  let fixture: ComponentFixture<CreatePoDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePoDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePoDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
