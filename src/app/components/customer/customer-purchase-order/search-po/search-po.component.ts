import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DynamicGrid } from '../../../../app.module';
import { CreateCustPoService } from '../../../../services/serviceconnection/create-service/create-cust-po.service';
import { Instrument, ReferenceForSalesQuotation } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateSalesService } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { SearchPo, SearchPoService } from '../../../../services/serviceconnection/search-service/search-po.service';
import { SearchSalesQuotationByDocument, SearchSalesService } from '../../../../services/serviceconnection/search-service/search-sales.service';
import { StudentService } from '../../../../services/student/student.service';

@Component({
  selector: 'app-search-po',
  templateUrl: './search-po.component.html',
  styleUrls: ['./search-po.component.css']
})


export class SearchPoComponent implements OnInit {

  searchPoForm: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';
  p: number = 1;
  itemsPerPage: number = 5;
  public dateValue1 = new Date();
  public dateValue2 = new Date();

  data: any;
  searchPoData: SearchPo = new SearchPo();
  poDataArray: Array<any> = [];

  docNo: string;
  statusChoice: any;
  status: string;
  idForSearchPo: string;
  salesObject: any;
  createPoInfoForSearchPoObject: any;
  documentNumberFromSearchSales: string;
  termsIdForSearchSalesArray: Array<number> = [];

  referenceList: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  referenceName: Array<any> = [];
  referenceId: Array<any> = [];
  dateOfQuotation: Array<any> = [];
  salesQuotationDocumentVariableArrayForSearchSales: any;
  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];

  constructor(private formBuilder: FormBuilder, public datepipe: DatePipe, private router: Router, private studentService: StudentService, private searchSalesService: SearchSalesService, private createSalesService: CreateSalesService, private searchPoService: SearchPoService, private createCustPoService: CreateCustPoService) {
    this.searchPoForm = this.formBuilder.group({
      searchDocument: [],
      itemsPerPage: [],
      toDate: [],
      fromDate: [],
      statusChoice: []
    });
  }

  ngOnInit() {
    this.statusChoice = "selectStatus";

    this.createSalesService.getReferenceList().subscribe(data => {
      this.referenceList = data;
      console.log(this.referenceList);
      for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
        this.referenceName.push(this.referenceList[i].referenceName);
        this.referenceId.push(this.referenceList[i].referenceId);
      }
    },
      error => console.log(error));

    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));
  }

  showData() {
    this.searchPoService.showData().subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.poDataArray = this.data;

      for (var i = 0; i < this.poDataArray.length; i++) {

        if (this.poDataArray[i][5] == 1) {
          this.status = "Draft";
          this.poDataArray[i].push("Draft")
        }
        if (this.poDataArray[i][6] == 1) {
          this.status = "Archieved";
          this.poDataArray[i].push("Archieved")
        }
        if (this.poDataArray[i][7] == 1) {
          this.status = "Submitted";
          this.poDataArray[i].push("Submitted")
        }
        if (this.poDataArray[i][8] == 1) {
          this.status = "Approved";
          this.poDataArray[i].push("Approved")
        }
        if (this.poDataArray[i][9] == 1) {
          this.status = "Rejected";
          this.poDataArray[i].push("Rejected")
        }
      }

      for (var i = 0; i < this.poDataArray.length; i++) {
        this.poDataArray[i][2] = this.poDataArray[i][2] + " / " + this.poDataArray[i][3];

      }

    })
  }
  searchData() {
    debugger;
    if (typeof this.searchPoForm.value.toDate == "undefined" || this.searchPoForm.value.toDate == null) {
      this.searchPoData.searchDocument = this.searchPoForm.value.searchDocument;
      if (this.searchPoData.searchDocument != null && typeof this.searchPoData.searchDocument != 'undefined') {
        this.searchPoData.searchDocument = this.searchPoData.searchDocument.trim();
      }

      switch (this.statusChoice) {
        case 'selectStatus': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Draft': this.searchPoData.draft = 1;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Archieved': this.searchPoData.draft = 0
          this.searchPoData.archieved = 1;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Rejected': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 1;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Approved': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 1;
          this.searchPoData.submitted = 0;
          break;
        case 'Submitted': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 1;
          break;

      }
    }
    else {
      this.searchPoForm.value.toDate.setDate(this.searchPoForm.value.toDate.getDate() + 1);
      this.searchPoData.toDate = this.datepipe.transform(this.searchPoForm.value.toDate, 'yyyy-MM-dd');
      this.searchPoData.fromDate = this.datepipe.transform(this.searchPoForm.value.fromDate, 'yyyy-MM-dd');
      this.searchPoData.searchDocument = this.searchPoForm.value.searchDocument;
      if (this.searchPoData.searchDocument != null && typeof this.searchPoData.searchDocument != 'undefined') {
        this.searchPoData.searchDocument = this.searchPoData.searchDocument.trim();
      }

      switch (this.statusChoice) {
        case 'selectStatus': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Draft': this.searchPoData.draft = 1;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Archieved': this.searchPoData.draft = 0
          this.searchPoData.archieved = 1;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Rejected': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 1;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 0;
          break;
        case 'Approved': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 1;
          this.searchPoData.submitted = 0;
          break;
        case 'Submitted': this.searchPoData.draft = 0;
          this.searchPoData.archieved = 0;
          this.searchPoData.rejected = 0;
          this.searchPoData.approved = 0;
          this.searchPoData.submitted = 1;
          break;

      }
    }

    if (this.searchPoData == null) {
      alert("please enter criteria to search");
    }
    else {
      this.searchPoService.searchData(this.searchPoData).subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
      })
      this.showData();
    }

  }

  moveToPODocUpdateScreen(i) {

    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;
    this.searchPoService.setDocNoForPODocScreen(this.docNo);
    this.idForSearchPo = this.docNo;

    this.searchPoService.sendDocNoForGettingCreatePODetails(this.idForSearchPo).subscribe(data => {
      console.log(data);
      localStorage.setItem('DocumentNumberForPo', JSON.stringify(this.docNo));
    })
    this.searchPoService.getCreatePODetailsForSearchPo().subscribe(data => {
      this.createPoInfoForSearchPoObject = data;
      this.getSrNoForSearchPo()
    })

  }

  getSrNoForSearchPo() {
    this.createCustPoService.sendDocNoOfPoDocument(this.docNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })

    this.createCustPoService.getSrNoForPoDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.salesQuotationDocumentVariableArrayForSearchSales = data;

      if (this.salesQuotationDocumentVariableArrayForSearchSales.length != 0) {
        for (var z = 0; z < this.salesQuotationDocumentVariableArrayForSearchSales.length; z++) {
          this.newDynamic1.push({
            uniqueId: this.salesQuotationDocumentVariableArrayForSearchSales[z][0],
            srNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][2],
            instruName: this.salesQuotationDocumentVariableArrayForSearchSales[z][3],
            description: this.salesQuotationDocumentVariableArrayForSearchSales[z][4],
            idNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][5],
            range: this.salesQuotationDocumentVariableArrayForSearchSales[z][6],
            quantity: this.salesQuotationDocumentVariableArrayForSearchSales[z][7],
            rate: this.salesQuotationDocumentVariableArrayForSearchSales[z][8],
            discountOnItem: this.salesQuotationDocumentVariableArrayForSearchSales[z][9],
            amount: this.salesQuotationDocumentVariableArrayForSearchSales[z][11],
            hsnNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][12],
            customerPartNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][13],
            rangeFrom: this.salesQuotationDocumentVariableArrayForSearchSales[z][15],
            rangeTo: this.salesQuotationDocumentVariableArrayForSearchSales[z][16]
          })
        }

        this.dynamicArray.splice(0, 1);
        for (var l = 1; l < this.newDynamic1.length; l++) {
          this.convertInsruIdtoName(l);
          this.dynamicArray.push(this.newDynamic1[l]);
        }
      }

      localStorage.setItem('dynamicArrayForPo', JSON.stringify(this.dynamicArray));
      this.router.navigateByUrl('/updatepodoc');

    })

  }

  convertInsruIdtoName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;
      }
    }
  }

}
