import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPoComponent } from './search-po.component';

describe('SearchPoComponent', () => {
  let component: SearchPoComponent;
  let fixture: ComponentFixture<SearchPoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
