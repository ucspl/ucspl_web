
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../services/student/student.service';

@Component({
  selector: 'app-ledger',
  templateUrl: './ledger.component.html',
  styleUrls: ['./ledger.component.css']
})
export class LedgerComponent implements OnInit {

  CutomerLedgerform: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {

    this.CutomerLedgerform = this.formBuilder.group({
      //    firstName: ['', [Validators.required]],

      customer: [],
      SelectCustomer: [],
      StartDate: [],
      EndDate: []

    });
  }

  ngOnInit() {
  }

}
