import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';
import { ValidationService } from '../../services/config/config.service';
import { UserService } from '../../services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../services/config/config.service';
import { User, CreateUserService } from '../../services/serviceconnection/create-service/create-user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    //  animations: [routerTransition()],
    //  host: {'[@routerTransition]': ''}
})
export class LoginComponent implements OnInit {

    private loginForm: FormGroup;
    j: number;
    user: User = new User();
    userForUserName: User = new User();
    usersArray: Array<any> = [];
    usersIds: Array<any> = [];
    userIdLength: number;
    usersUserName: Array<any> = [];
    usersFullName: Array<any> = [];
    usersPasswords: Array<any> = [];
    userNamesLength: number;

    rmCheck: any;
    emailInput: any;
    passInput: any;
    email: any;
    password: any;


    constructor(private formBuilder: FormBuilder, private serviceForCreateUserService: CreateUserService, private router: Router, private userService: UserService, private toastr: ToastrService) {
        this.loginForm = this.formBuilder.group({
            email: [''],
            password: ['', [Validators.required, ValidationService.passwordValidator]]
        });
    }

    // Check if user already logged in
    ngOnInit() {
        // if (localStorage.getItem('userData')) {
        //     this.router.navigate(['/']);
        // }

        this.call();

    }


    navigateToRegister() {
        this.router.navigateByUrl('/registration')
    }
    navigateToEmployeelist() {
        this.router.navigateByUrl('/sampleemployees')
    }
    navigateToaddEmployee() {
        this.router.navigateByUrl('/sampleadd')
    }

    homebuttonfunction() {
        this.router.navigateByUrl('/UserTabCreateUser')
    }


    doLogin() {

        this.loginForm.value.email = this.email;
        this.loginForm.value.password = this.password;
        this.lsRememberMe(this.loginForm.value);


    }


    // Login success function
    success(data) {
        if (data.code == 200) {
            localStorage.setItem('userData', JSON.stringify(data.data));
            this.router.navigate(['']);
            this.toastr.success('Success', "Logged In Successfully");
        } else {
            this.toastr.error('Failed', "Invalid Credentials");
        }
    }


    call() {
        this.rmCheck = document.getElementById("rememberMe");
        this.emailInput = document.getElementById("email");
        this.passInput = document.getElementById("pass");
        if (localStorage.checkbox && localStorage.checkbox !== "") {
            this.rmCheck.setAttribute("checked", "checked");


            this.email = localStorage.getItem('emailInput');

            this.password = localStorage.getItem('passInput');
        } else {
            this.rmCheck.removeAttribute("checked");
            this.emailInput.value = "";
            this.passInput.value = "";
        }
    }



    lsRememberMe(data1) {
        if (this.rmCheck.checked && this.emailInput.value !== "") {
            localStorage.username = this.emailInput.value;
            localStorage.passname = this.passInput.value;
            localStorage.setItem('emailInput', this.emailInput.value);
            localStorage.setItem('passInput', this.passInput.value);
            localStorage.checkbox = this.rmCheck.value;


            this.serviceForCreateUserService.getUserList().subscribe(data => {

                this.userForUserName = data;
                for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {

                    this.usersUserName.push(this.userForUserName[i].userName);
                    this.usersPasswords.push(this.userForUserName[i].password);
                    this.usersFullName.push(this.userForUserName[i].firstName + " " + this.userForUserName[i].middleName + " " + this.userForUserName[i].lastName);
                }
                this.userNamesLength = Object.keys(this.usersUserName).length;


                for (this.j = 0; this.j < this.userNamesLength; this.j++) {

                    if (this.usersUserName[this.j] == this.emailInput.value && this.usersPasswords[this.j] == this.passInput.value) {
                        localStorage.setItem('loginUserData', JSON.stringify(this.usersFullName[this.j]));

                        this.router.navigateByUrl('/nav/home')
                        break;
                    } else {

                    }

                }

                if (this.j >= this.userNamesLength) {
                    alert("login failed wrong username or password combination...");
                }

            });



        } else {
            localStorage.username = "";
            localStorage.checkbox = "";
            localStorage.passname = "";

            this.serviceForCreateUserService.getUserList().subscribe(data => {

                this.userForUserName = data;
                for (var i = 0, l = Object.keys(this.userForUserName).length; i < l; i++) {

                    this.usersUserName.push(this.userForUserName[i].userName);
                    this.usersPasswords.push(this.userForUserName[i].password);
                    this.usersFullName.push(this.userForUserName[i].firstName + " " + this.userForUserName[i].middleName + " " + this.userForUserName[i].lastName);
                }
                this.userNamesLength = Object.keys(this.usersUserName).length;


                for (this.j = 0; this.j < this.userNamesLength; this.j++) {

                    if (this.usersUserName[this.j] == data1.email && this.usersPasswords[this.j] == data1.password) {
                        localStorage.setItem('loginUserData', JSON.stringify(this.usersFullName[this.j]));

                        this.router.navigateByUrl('/nav/home')
                        break;
                    } else {

                    }

                }

                if (this.j >= this.userNamesLength) {
                    alert("login failed wrong username or password combination...");
                }

            });
        }
    }




}

