import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { optionalValidator } from '../../../services/config/config.service';
import { SearchDefinition, SearchDefinitionService } from '../../../services/serviceconnection/search-service/search-definition.service';

@Component({
  selector: 'app-def-search',
  templateUrl: './def-search.component.html',
  styleUrls: ['./def-search.component.css']
})
export class DefSearchComponent implements OnInit {
  SearchDefinitionForm: FormGroup;
  searchDefData:SearchDefinition=new SearchDefinition();
  emp: any;
  status: any;
  DefinitionData: any;
  definitionDataArray: Array<any> = [];
  p: number = 1;
  itemsPerPage: number = 5;
  SearchDefName: any;
  createDefinitionInfoForSearchDefinitionObject: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private searchDefinitionServiceObject: SearchDefinitionService ) {

    this.SearchDefinitionForm = this.formBuilder.group({


      searchDefinitionValue: ['', [optionalValidator([Validators.required,Validators.minLength(3)])]],
      status: [],
      itemsPerPage: []
    })
   }

  ngOnInit() {
    this.status="1";
  }

  searchData() {
    debugger;

    this.searchDefData.searchDefinitionValue = this.SearchDefinitionForm.value.searchDefinitionValue;
    this.searchDefData.status=this.SearchDefinitionForm.value.status;
     if(this.searchDefData.searchDefinitionValue==""&&this.searchDefData.searchDefinitionValue==null){
    alert("Please enter criteria to search definition");
   }
   else{
   this.searchDefinitionServiceObject.searchData(this.searchDefData).subscribe((res: any) => {

      this.emp = res;
    
    })
    this.showData();
  }

  }
  showData() {
    this.searchDefinitionServiceObject.showData().subscribe((res: any) => {
      
    
      this. definitionDataArray = res;
      

      for (var i = 0; i < Object.keys(this. definitionDataArray).length; i++) {
        if (this. definitionDataArray[i][3] == 0) {
          this. definitionDataArray[i][3] = "Inactive";
        }
        else
          this. definitionDataArray[i][3] = "Active";
      }


    },
      error => console.log(error));
} 
moveToUpdateDefinitionScreen(i){

  console.log("index is : " + i.target.innerHTML);
  this.SearchDefName = i.target.innerText;
  //this.searchSalesService.setDocNoForsalesQuotDocScreen(this.docNo);
 // this.idForSearchSales=this.docNo;

 this.searchDefinitionServiceObject.sendNameForGettingCreateDefinitionDetails(this.SearchDefName).subscribe(data => {
    console.log(data);

 // this.router.navigateByUrl('/nav/defupdate')
 
 })

 this.searchDefinitionServiceObject.getCreateDefinitionDetailsForSearchDefinition().subscribe(data => {
       
  this.createDefinitionInfoForSearchDefinitionObject= data;

    
})
}
}