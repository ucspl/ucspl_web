import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefSearchComponent } from './def-search.component';

describe('DefSearchComponent', () => {
  let component: DefSearchComponent;
  let fixture: ComponentFixture<DefSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
