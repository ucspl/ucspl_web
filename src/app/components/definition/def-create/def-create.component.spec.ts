import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefCreateComponent } from './def-create.component';

describe('DefCreateComponent', () => {
  let component: DefCreateComponent;
  let fixture: ComponentFixture<DefCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
