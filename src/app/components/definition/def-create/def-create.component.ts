
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../services/config/config.service';
import { BillingCreditPeriod, Country, CreateDefService, DefinitionCategory, Department, Instrument, ReferenceForSalesQuotation, RegionAreaCode, ReqTypeOfRequestAndInward, InwardTypeOfRequestAndInward, SecurityQuestion, SupplierType, TaxpayerTypeForCustomer, Terms, TypeOfQuotationForSalesAndQuotation, FinancialYear, UncMasterType, UncMasterNumber, UncProbabilityDistribution, UncSourceOfUncertainty, UncType, UncDividingFactor, UncDegreeOfFreedom, UncMasterName } from '../../../services/serviceconnection/create-service/create-def.service';
import { Branch, Role } from '../../../services/serviceconnection/create-service/create-def.service';
import { CreateInvoiceService } from '../../../services/serviceconnection/create-service/create-invoice.service';
import { CreateReqAndInwardService, ParameterListForRequestAndInward, UomListForRequestAndInward } from '../../../services/serviceconnection/create-service/create-req-and-inward.service';
import { StudentService } from '../../../services/student/student.service';

@Component({
  selector: 'app-def-create',
  templateUrl: './def-create.component.html',
  styleUrls: ['./def-create.component.css']
})
export class DefCreateComponent implements OnInit {

  addDefinitionForm: FormGroup;
  categoryData: DefinitionCategory = new DefinitionCategory();
  categoryNames: Array<any> = [];
  categoryId: Array<any> = [];
  status: any;
  //createDefinitionObject:CreateDefinition=new CreateDefinition();
  branchObject: Branch = new Branch();
  roleObject: Role = new Role();
  departmentObject: Department = new Department();
  securityQuestionObject: SecurityQuestion = new SecurityQuestion();
  countryObject: Country = new Country();
  regionAreaCodeObject: RegionAreaCode = new RegionAreaCode()
  billingCreditPeriodObject: BillingCreditPeriod = new BillingCreditPeriod()
  taxpayerTypeForCustomerObject: TaxpayerTypeForCustomer = new TaxpayerTypeForCustomer();
  typeOfQuotationForSalesAndQuotationObject: TypeOfQuotationForSalesAndQuotation = new TypeOfQuotationForSalesAndQuotation();
  referenceForSalesQuotationObject: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  instrumentObject: Instrument = new Instrument();
  termsObject: Terms = new Terms();
  supplierTypeObject: SupplierType = new SupplierType();
  inwardTypeOfRequestAndInwardObject: InwardTypeOfRequestAndInward = new InwardTypeOfRequestAndInward();
  reqTypeOfRequestAndInwardObject: ReqTypeOfRequestAndInward = new ReqTypeOfRequestAndInward();
  financialYearObject: FinancialYear = new FinancialYear();


  ////////////////// unc screen class and obj //////////////////////////////

  uncMasterTypeObject: UncMasterType = new UncMasterType();
  uncMasterNumberObject: UncMasterNumber = new UncMasterNumber();
  uncProbabilityDistributionObject: UncProbabilityDistribution = new UncProbabilityDistribution();
  uncSourceOfUncertaintyObject: UncSourceOfUncertainty = new UncSourceOfUncertainty();
  uncTypeObject: UncType = new UncType();
  uncDividingFactorObject: UncDividingFactor = new UncDividingFactor();
  uncDegreeOfFreedomObject: UncDegreeOfFreedom = new UncDegreeOfFreedom();
  uncMasterNameObject: UncMasterName=new UncMasterName();


  categoryName: string;
  name: string;
  description: string;
  type: string;
  public dateValue1 = Date.now();
  loginUserName: any;
  submitted = false;
  hideDisplayName: number;
  public dateValue4 = new Date();
  financialYearData: FinancialYear = new FinancialYear();
  statrtDateCollection: Array<any> = [];
  idCollection: Array<any> = [];
  displayDateData: number;
  hideNameAndStatus: number;
  categoryName1: string;
  j: number;

  ParameterListForReqAndIwdObj: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();
  arrayOfParameters: Array<any> = [];
  arrayParameterId: Array<any> = [];
  arrayParameterNames: Array<any> = [];

  uomListForRequestAndInwardObj: UomListForRequestAndInward = new UomListForRequestAndInward();
  arrayOfUom: Array<any> = [];
  arrayUomId: Array<any> = [];
  arrayUomName: Array<any> = [];
  arrayUomNameAccorToParam: Array<any> = [];
  arrayUomNameAccorToParam1: Array<any> = [];
  arrayUomNameAccorToParam2: Array<any> = [];
  arrayPrameterIdOfUom: Array<any> = [];

  uomObject: UomListForRequestAndInward = new UomListForRequestAndInward();
  parameterObject: ParameterListForRequestAndInward = new ParameterListForRequestAndInward();

  selectParameter: any;
  displayUomData: number;
  frequency: number;
  displayParameterData: number;

  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService, private createDefService: CreateDefService, public datepipe: DatePipe, private createInvoiceService: CreateInvoiceService, private createReqAndInwardService: CreateReqAndInwardService) {

    this.addDefinitionForm = this.formBuilder.group({

      name: ['', [Validators.required]],
      category: ['', [Validators.required]],
      description: [],
      status: ['', [Validators.required]],
      fromDate: [],
      dateColletion: [],
      selectParameter: [],
      frequency: []

    });
  }

  ngOnInit() {

    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));
    this.status = 1;
    this.hideNameAndStatus = 0;

    // Get Category list
    this.createDefService.getCategoryList().subscribe(data => {
      this.categoryData = data;

      console.log(this.categoryData);
      for (var i = 0, l = Object.keys(this.categoryData).length; i < l; i++) {
        this.categoryNames.push(this.categoryData[i].categoryName);
        this.categoryId.push(this.categoryData[i].categoryId);

      }

    },
      error => console.log(error));

    // get financial year dates list

    this.createInvoiceService.getFinancialYearDates().subscribe(data => {
      this.financialYearData = data;
      console.log(this.financialYearData);

      for (var i = 0, l = Object.keys(this.financialYearData).length; i < l; i++) {
        this.statrtDateCollection.push(this.financialYearData[i].fromDate);
        this.idCollection.push(this.financialYearData[i].id);


      }
    },
      error => console.log(error));


    //get parameter list
    this.createReqAndInwardService.getParameterListOfRequestAndInwards().subscribe(data => {
      this.ParameterListForReqAndIwdObj = data;
      for (var i = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; i < l; i++) {
        this.arrayParameterNames.push(this.ParameterListForReqAndIwdObj[i].parameterName);
        this.arrayParameterId.push(this.ParameterListForReqAndIwdObj[i].parameterId);
      }
    })

    //get UOM list
    this.createReqAndInwardService.getUomListOfRequestAndInwards().subscribe(data => {
      this.uomListForRequestAndInwardObj = data;
      for (var i = 0, l = Object.keys(this.uomListForRequestAndInwardObj).length; i < l; i++) {
        this.arrayUomName.push(this.uomListForRequestAndInwardObj[i].uomName);
        this.arrayUomId.push(this.uomListForRequestAndInwardObj[i].uomId);
        this.arrayPrameterIdOfUom.push(this.uomListForRequestAndInwardObj[i].parameterId);
      }
    });


  }

  changeStructure() {
    this.categoryName1 = this.categoryName.toLowerCase();
    switch (this.categoryName1) {
      case "branch":
        this.hideDisplayName = 0;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "role":
        this.hideDisplayName = 0;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "department":
        this.hideDisplayName = 0;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
      case "security question":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "region/area code":
        this.hideDisplayName = 0;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "billing credit period":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "Taxpayer Type":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "Type of Quotation":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "customer reference number":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "uuc_nomenclature":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "terms":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "supplier type":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;
      case "inward type":
        this.hideDisplayName = 1;
        this.displayDateData = 0;
        this.hideNameAndStatus = 0;
        break;
      case "request type":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "starting date of financial year":
        this.hideDisplayName = 1;
        this.displayDateData = 1;
        this.hideNameAndStatus = 1;
        break;

      case "uom":
        this.hideDisplayName = 0;
        this.displayDateData = 0;
        this.hideNameAndStatus = 0;
        this.displayUomData = 1;
        this.displayParameterData = 0;
        break;

      case "parameter":
        this.hideDisplayName = 0;
        this.displayDateData = 0;
        this.hideNameAndStatus = 0;
        this.displayUomData = 0;
        this.displayParameterData = 1;
        break;

      case "unc source of uncertainty":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc probability distribution":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc type":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc dividing factor":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc degree of freedom":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc master type":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc master number":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      case "unc name master ":
        this.hideDisplayName = 1;
        this.hideNameAndStatus = 0;
        this.displayDateData = 0;
        break;

      default:


    }
  }
  onSubmit() {

    this.submitted = true;
    alert("Definition added Successfully");
    this.save();
  }

  save() {
    this.categoryName1 = this.categoryName.toLowerCase();
    switch (this.categoryName1) {
      case "branch":
        this.hideDisplayName = 0;
        this.branchObject.branchName = this.name;
        this.branchObject.description = this.description;
        this.branchObject.status = this.status;
        this.branchObject.dateCreated = this.dateValue1;
        this.branchObject.createdBy = this.loginUserName;
        //this.createDefinitionObject.type=this.type;



        this.createDefService.createBranch(this.branchObject).subscribe(data => {

        },
          error => console.log(error));
        break;
      case "role":
        this.hideDisplayName = 0;
        this.roleObject.roleName = this.name;
        this.roleObject.description = this.description;
        this.roleObject.status = this.status;
        this.roleObject.dateCreated = this.dateValue1;
        this.roleObject.createdBy = this.loginUserName;

        this.createDefService.createRole(this.roleObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "department":
        this.hideDisplayName = 0;
        this.departmentObject.departmentName = this.name;
        this.departmentObject.description = this.description;
        this.departmentObject.status = this.status;
        this.departmentObject.dateCreated = this.dateValue1;
        this.departmentObject.createdBy = this.loginUserName;

        this.createDefService.createDepartment(this.departmentObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "security question":
        this.hideDisplayName = 1;
        this.securityQuestionObject.securityQuestionName = this.name;
        this.securityQuestionObject.status = this.status;
        this.securityQuestionObject.dateCreated = this.dateValue1;
        this.securityQuestionObject.createdBy = this.loginUserName;

        this.createDefService.createSecurityQuestion(this.securityQuestionObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "country":
        this.hideDisplayName = 1;
        this.countryObject.countryName = this.name;
        this.countryObject.status = this.status;
        this.countryObject.dateCreated = this.dateValue1;
        this.countryObject.createdBy = this.loginUserName;

        this.createDefService.createCountry(this.countryObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "region/area code":
        this.hideDisplayName = 1;
        this.regionAreaCodeObject.regionAreaCodeName = this.name;
        this.regionAreaCodeObject.status = this.status;
        this.regionAreaCodeObject.dateCreated = this.dateValue1;
        this.regionAreaCodeObject.createdBy = this.loginUserName;

        this.createDefService.createRegionAreaCode(this.regionAreaCodeObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "billing credit period":
        this.hideDisplayName = 1;
        this.billingCreditPeriodObject.billingCreditPeriodName = this.name;
        this.billingCreditPeriodObject.status = this.status;
        this.billingCreditPeriodObject.dateCreated = this.dateValue1;
        this.billingCreditPeriodObject.createdBy = this.loginUserName;

        this.createDefService.createBillingCreditPeriod(this.billingCreditPeriodObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "taxpayer type":
        this.hideDisplayName = 1;
        this.taxpayerTypeForCustomerObject.taxpayerTypeName = this.name;
        this.taxpayerTypeForCustomerObject.status = this.status;
        this.taxpayerTypeForCustomerObject.dateCreated = this.dateValue1;
        this.taxpayerTypeForCustomerObject.createdBy = this.loginUserName;

        this.createDefService.createTaxpayerType(this.taxpayerTypeForCustomerObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "quotation type":
        this.hideDisplayName = 1;
        this.typeOfQuotationForSalesAndQuotationObject.docTypeName = this.name;
        this.typeOfQuotationForSalesAndQuotationObject.status = this.status;
        this.typeOfQuotationForSalesAndQuotationObject.dateCreated = this.dateValue1;
        this.typeOfQuotationForSalesAndQuotationObject.createdBy = this.loginUserName;

        this.createDefService.createTypeOfQuotation(this.typeOfQuotationForSalesAndQuotationObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "reference for sales quotation":
        this.hideDisplayName = 1;
        this.referenceForSalesQuotationObject.referenceName = this.name;
        this.referenceForSalesQuotationObject.status = this.status;
        this.referenceForSalesQuotationObject.dateCreated = this.dateValue1;
        this.referenceForSalesQuotationObject.createdBy = this.loginUserName;

        this.createDefService.createReferenceNumberForQuotation(this.referenceForSalesQuotationObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "uuc_nomenclature":
        this.hideDisplayName = 0;
        this.instrumentObject.instrumentName = this.name;
        this.instrumentObject.parameterName = this.description;
        this.instrumentObject.status = this.status;
        this.instrumentObject.dateCreated = this.dateValue1;
        this.instrumentObject.createdBy = this.loginUserName;

        this.createDefService.createInstrument(this.instrumentObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "terms":
        this.hideDisplayName = 0;
        this.termsObject.termsName = this.name;
        this.termsObject.description = this.description;
        this.termsObject.status = this.status;
        this.termsObject.dateCreated = this.dateValue1;
        this.termsObject.createdBy = this.loginUserName;

        this.createDefService.createTerms(this.termsObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "supplier type":
        this.hideDisplayName = 1;
        this.supplierTypeObject.supplierTypeName = this.name;
        this.supplierTypeObject.status = this.status;
        this.supplierTypeObject.dateCreated = this.dateValue1;
        this.supplierTypeObject.createdBy = this.loginUserName;

        this.createDefService.createSupplierType(this.supplierTypeObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "inward type":
        this.hideDisplayName = 0;
        this.inwardTypeOfRequestAndInwardObject.inwardTypeName = this.name;
        this.inwardTypeOfRequestAndInwardObject.description = this.description;
        this.inwardTypeOfRequestAndInwardObject.status = this.status;
        this.inwardTypeOfRequestAndInwardObject.dateCreated = this.dateValue1;
        this.inwardTypeOfRequestAndInwardObject.createdBy = this.loginUserName;

        this.createDefService.createRequestAndInwardType(this.inwardTypeOfRequestAndInwardObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "request type":
        this.hideDisplayName = 0;
        this.reqTypeOfRequestAndInwardObject.reqTypeName = this.name;
        this.reqTypeOfRequestAndInwardObject.description = this.description;
        this.reqTypeOfRequestAndInwardObject.status = this.status;
        this.reqTypeOfRequestAndInwardObject.dateCreated = this.dateValue1;
        this.reqTypeOfRequestAndInwardObject.createdBy = this.loginUserName;

        this.createDefService.createRequestType(this.reqTypeOfRequestAndInwardObject).subscribe(data => {

        },
          error => console.log(error));
        break;

      case "starting date of financial year":
        this.displayDateData = 1;


        for (var i = 0, l = Object.keys(this.financialYearData).length; i < l; i++) {

          if (this.financialYearData[i].status == 1) {
            //this.addDefinitionForm.value.fromDate= this.datepipe.transform(this.addDefinitionForm.value.fromDate, 'dd-MM');
            this.financialYearObject.status = 0;
            //this.financialYearObject.createdBy = this.loginUserName;
            this.createDefService.updateFinancialYear(this.financialYearData[i].id, this.financialYearObject).subscribe(data => {

            },
              error => console.log(error));
          }
        }

        for (this.j = 0, l = Object.keys(this.financialYearData).length; this.j < l; this.j++) {
          if (this.financialYearData[this.j].fromDate == this.datepipe.transform(this.addDefinitionForm.value.fromDate, 'dd-MM')) {
            //this.addDefinitionForm.value.fromDate= this.datepipe.transform(this.addDefinitionForm.value.fromDate, 'dd-MM');
            this.financialYearObject.status = 1;
            // this.financialYearObject.createdBy = this.loginUserName;

            this.createDefService.updateFinancialYear(this.financialYearData[this.j].id, this.financialYearObject).subscribe(data => {

            },
              error => console.log(error));
            break;

          }

        }

        if (this.j >= Object.keys(this.financialYearData).length) {
          this.addDefinitionForm.value.fromDate = this.datepipe.transform(this.addDefinitionForm.value.fromDate, 'dd-MM');
          this.financialYearObject.fromDate = this.addDefinitionForm.value.fromDate;
          this.financialYearObject.status = 1;
          this.financialYearObject.createdBy = this.loginUserName;
          this.createDefService.createFinancialYear(this.financialYearObject).subscribe(data => {

          },
            error => console.log(error));
        }


        break;


      case "uom":
        this.uomObject.uomName = this.description;
        this.uomObject.description = this.name;
        this.uomObject.status = this.status;
        this.uomObject.dateCreated = this.datepipe.transform(this.dateValue1, 'dd-MM-yyyy');
        this.uomObject.createdBy = this.loginUserName;

        for (var k = 0, l = Object.keys(this.ParameterListForReqAndIwdObj).length; k < l; k++) {
          if (this.selectParameter == this.arrayParameterNames[k]) {
            this.uomObject.parameterId = this.arrayParameterId[k];
            break;
          }
          else {
            this.uomObject.parameterId = 1;
          }
        }

        this.createDefService.createUOMType(this.uomObject).subscribe(data => {
          console.log(data);
          alert("uom created successfully !!")
        })

        break;


      case "parameter":
        this.parameterObject.parameterName = this.description;
        this.parameterObject.frequency = this.frequency;
        this.parameterObject.description = this.name;
        this.parameterObject.status = this.status;
        this.parameterObject.dateCreated = this.datepipe.transform(this.dateValue1, 'dd-MM-yyyy');
        this.parameterObject.createdBy = this.loginUserName;

        this.createDefService.createParameterType(this.parameterObject).subscribe(data => {
          console.log(data);
          alert("Parameter created successfully !!")
        })

        break;

      case "unc source of uncertainty":
        this.hideDisplayName = 0;
        this.uncSourceOfUncertaintyObject.uncSourceOfUncertaintyName = this.name;
        this.uncSourceOfUncertaintyObject.description = this.description;
        this.uncSourceOfUncertaintyObject.status = this.status;
        this.uncSourceOfUncertaintyObject.dateCreated = this.dateValue1;
        this.uncSourceOfUncertaintyObject.createdBy = this.loginUserName;

        this.createDefService.createUncSourceOfUncertainty(this.uncSourceOfUncertaintyObject).subscribe(data => {
          console.log(data);
          alert("Source of uncertainty created successfully !!")
        })



        break;

      case "unc probability distribution":
        this.hideDisplayName = 0;
        this.uncProbabilityDistributionObject.probabilityDistributionName = this.name;
        this.uncProbabilityDistributionObject.description = this.description;
        this.uncProbabilityDistributionObject.status = this.status;
        this.uncProbabilityDistributionObject.dateCreated = this.dateValue1;
        this.uncProbabilityDistributionObject.createdBy = this.loginUserName;

        this.createDefService.createUncProbabilityDistribution(this.uncProbabilityDistributionObject).subscribe(data => {
          console.log(data);
          alert("Probability distribution created successfully !!")
        })

        break;

      case "unc type":
        this.hideDisplayName = 0;
        this.uncTypeObject.uncTypeName = this.name;
        this.uncTypeObject.description = this.description;
        this.uncTypeObject.status = this.status;
        this.uncTypeObject.dateCreated = this.dateValue1;
        this.uncTypeObject.createdBy = this.loginUserName;

        this.createDefService.createUncType(this.uncTypeObject).subscribe(data => {
          console.log(data);
          alert("Unc type created successfully !!")
        })

        break;

      case "unc dividing factor":
        this.hideDisplayName = 0;
        this.uncDividingFactorObject.uncDividingFactorName = this.name;
        this.uncDividingFactorObject.description = this.description;
        this.uncDividingFactorObject.status = this.status;
        this.uncDividingFactorObject.dateCreated = this.dateValue1;
        this.uncDividingFactorObject.createdBy = this.loginUserName;

        this.createDefService.createUncDividingFactor(this.uncDividingFactorObject).subscribe(data => {
          console.log(data);
          alert("Dividing factor created successfully !!")
        })


        break;

      case "unc degree of freedom":
        this.hideDisplayName = 0;
        this.uncDegreeOfFreedomObject.uncDegreeOfFreedomName = this.name;
        this.uncDegreeOfFreedomObject.description = this.description;
        this.uncDegreeOfFreedomObject.status = this.status;
        this.uncDegreeOfFreedomObject.dateCreated = this.dateValue1;
        this.uncDegreeOfFreedomObject.createdBy = this.loginUserName;

        this.createDefService.createUncDegreeOfFreedom(this.uncDegreeOfFreedomObject).subscribe(data => {
          console.log(data);
          alert("Degree of Freedom created successfully !!")
        })

        break;

      case "unc master type":
        this.hideDisplayName = 0;
        this.uncMasterTypeObject.uncMasterTypeName = this.name;
        this.uncMasterTypeObject.description = this.description;
        this.uncMasterTypeObject.status = this.status;
        this.uncMasterTypeObject.dateCreated = this.dateValue1;
        this.uncMasterTypeObject.createdBy = this.loginUserName;

        this.createDefService.creatUncMasterType(this.uncMasterTypeObject).subscribe(data => {
          console.log(data);
          alert("Master type created successfully !!")
        })

        break;

      case "unc master number":
        this.hideDisplayName = 0;
        this.uncMasterNumberObject.uncMasterNumberName = this.name;
        this.uncMasterNumberObject.description = this.description;
        this.uncMasterNumberObject.status = this.status;
        this.uncMasterNumberObject.dateCreated = this.dateValue1;
        this.uncMasterNumberObject.createdBy = this.loginUserName;

        this.createDefService.createUncMasterNumber(this.uncMasterNumberObject).subscribe(data => {
          console.log(data);
          alert("Master number created successfully !!")
        })

        break;

        case "unc name master":
          this.hideDisplayName = 0;
          this.uncMasterNameObject.uncMasterName = this.name;
          this.uncMasterNameObject.description = this.description;
          this.uncMasterNameObject.status = this.status;
          this.uncMasterNameObject.dateCreated = this.dateValue1;
          this.uncMasterNameObject.createdBy = this.loginUserName;
  
          this.createDefService.createUncMasterName(this.uncMasterNameObject).subscribe(data => {
            console.log(data);
            alert("Master name created successfully !!")
          })
  
          break;


      default:
        alert("please select category");
    }
    this.windowReload();
  }
  
  windowReload() {
    window.location.reload();

  }
}

