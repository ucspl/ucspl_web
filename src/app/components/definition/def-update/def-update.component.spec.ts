import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefUpdateComponent } from './def-update.component';

describe('DefUpdateComponent', () => {
  let component: DefUpdateComponent;
  let fixture: ComponentFixture<DefUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
