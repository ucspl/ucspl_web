import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSqdocComponent } from './create-sqdoc.component';

describe('CreateSqdocComponent', () => {
  let component: CreateSqdocComponent;
  let fixture: ComponentFixture<CreateSqdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSqdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSqdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
