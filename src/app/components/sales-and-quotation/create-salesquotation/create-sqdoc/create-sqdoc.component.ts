import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ResizedEvent } from 'angular-resize-event';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Customer, CreateCustomerService } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateSalesAndQuotation, termsForSalesAndQuotation, EnquiryAttachment, JsonForCsvFile, QuotationItemFile, CreateSalesService, DynamicGrid, SalesQuotationDocument, SalesQuotationDocumentCalculation } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { ReferenceForSalesQuotation, Instrument, TypeOfQuotationForSalesAndQuotation, Terms, Branch } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateUserService } from '../../../../services/serviceconnection/create-service/create-user.service';
import { SearchSalesService } from '../../../../services/serviceconnection/search-service/search-sales.service';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';
import { parseTwoDigitYear } from 'ngx-bootstrap/chronos/units/year';

declare var $: any;

@Component({
  selector: 'app-create-sqdoc',
  templateUrl: './create-sqdoc.component.html',
  styleUrls: ['./create-sqdoc.component.css']
})
export class CreateSqdocComponent implements OnInit {
  [x: string]: any;

  manageDocumentForm: FormGroup;
  public reqdocParaDetailsForm: FormGroup;
  salesAndQuotationForDocument: CreateSalesAndQuotation = new CreateSalesAndQuotation();
  createSalesandQuotation: CreateSalesAndQuotation = new CreateSalesAndQuotation();
  createSalesInfoForSearchSalesObject: CreateSalesAndQuotation = new CreateSalesAndQuotation();

  quotationItemFileData1: QuotationItemFile = new QuotationItemFile();
  quotationItemFileData2: QuotationItemFile = new QuotationItemFile();

  customerAndTermId: termsForSalesAndQuotation = new termsForSalesAndQuotation();
  printTermsId: any;

  salesQuotationDocumentObject: SalesQuotationDocument = new SalesQuotationDocument();
  salesQuotationDocumentObject1: SalesQuotationDocument = new SalesQuotationDocument();

  salesQuotationDocumentCalculationObject: SalesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();
  salesQuotationDocumentCalculationObject1: SalesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();

  j: any = 0;
  l: any = 0;
  tl: Array<number> = [0];
  k: any;
  n: any = 0;
  sot: any = 0;
  sumAll: any = 0;
  tCode: string;
  subTotal: number = 0;
  subTotal1: number = 0;
  amountArray: Array<any> = [];
  amountArray1: Array<any> = [];
  data: string[] = [];
  arrayOfDescription: string[] = [];
  arrayOfIdNo: string[] = [];
  arrayOfAccrediation: string[] = [];
  arrayOfRangeAccuracy: string[] = [];
  arrayOfSacCode: string[] = [];

  arrayOfQuantity: Array<number> = [];
  arrayOfRate: Array<number> = [];
  arrayOfDiscountOnItem: Array<number> = [];
  arrayOfDiscountOnTotal: Array<number> = [];
  arrayOfAmount: Array<number> = [];
  arrayOfTerms: Array<number> = [];

  selectedFileForQuotation: any;

  referenceList: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  referenceName: Array<any> = [];
  referenceId: Array<any> = [];

  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];

  customersName: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  arrayForCsv: Array<any> = [];

  customer: Customer = new Customer();
  customerDataObject: Customer = new Customer();
  static customerNameLength: number;

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  base64Datafile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  name: String;
  quotationNumber: string;
  prefixQuotationNumber1: any;
  prefixQuotationNumberPune: any = "PN";
  prefixQuotationNumberKolhapur: any = "KH";
  prefixQuotationNumberNagpur: any = "NG";
  prefixQuotationNumberMumbai: any = "MB";
  salesDocumentInfo: Array<any> = [];
  SalesQuotationDocumentform: FormGroup

  selectedRow: Number;
  checkboxes: boolean[];

  kindAttentionTrial: string;

  trialRows: FormArray;
  arr: FormArray;
  serverData = [];
  pageEmployes: FormArray[] = [];
  discountOnSubtotal: number = 0;
  discountOnSubtotal1: number = 0;
  total: number = 0;
  netValue: any;
  netValue1: any;
  total1: number = 0;
  cgstAmount: any;
  sgstAmount: number;
  cgstAmount1: any;
  sgstAmount1: number;
  customerReferenceTrial: any;
  netValueInWords: string;
  customerNameTrial: any;
  customerPanNo: any;
  customerGstNo: any;
  customerAddressline1: any;
  customerAddressline2: any;
  customerCountry: any;
  customerState: any;
  customerCity: any;
  customerPin: any;
  dateOfQuotation1: string;
  customerReferenceNo1: number;
  referenceDate1: string;
  kindAttachment1: number;
  stateCode: any;
  salesAndQuotationForDocument1: CreateSalesAndQuotation;
  salesAndQuotationForDocument1ForManageDocument: CreateSalesAndQuotation;
  igstAmount: number;
  igstAmount1: number;

  termsIdArray: Array<any> = [];
  termsIdArray1: any;
  termsIdArray2: Array<any> = [];
  termsIdArray3: Array<any> = [];
  termsIdArray4: Array<any> = [];

  selectTagEvent: any;
  m: any = 0;
  s: any = 0;
  taxesArray: Array<any> = [];
  taxesArrayName: Array<any> = [];

  dateString = '';
  format = 'dd/MM/yyyy';
  alive = true;
  public dateValue = new Date();
  dateOfQuotation: string;
  custRefNo: string;
  refDate: string;
  archieveDate: string;
  kindAttentionTrial1: string;
  documentNumber: string;
  typeOfQuotation: string;
  contactNo: string;
  email: string;
  status: string;
  remark: string;
  createdBy: string;
  manageDocumentVariable: any;
  salesQuotationDocumentVariable: any;
  id: any;
  customerNameArray: Array<any> = [];
  itemIdArray: Array<any> = [];
  termDescriptionArray: Array<any> = [];
  termDescriptionArray1: Array<any> = [];

  public termValue: string;

  paymentTermList: Array<{ id: number, name: string }> = [];
  termsList1: Array<{ id: number, name: string }> = [];
  termDescriptionList: Array<{ id: number, name: string }> = [];
  taxesList: Array<{ name: string, taxAmount: number }> = [];
  taxesList1: Array<{ name1: string, taxAmount1: number }> = [];
  taxesTotal: number = 0;

  trialCustomerWithAddress: Array<{ name: string, address: string }> = [];
  branchWithDescription: Array<{ description: string, branch: string }> = [];
  static customerDepartments: Array<any> = [];
  width: any;
  height: any;
  originalDocNo: string;

  srNoArray: Array<number> = [];
  salesQuotationDocumentVariableArray: Array<any> = [];

  salesQuotationDocumentCalculationVariableArray: Array<any> = [];
  searchSalesQuotationDocumentCalculationArray: Array<any> = [];
  quotationNumberWithPrefix: string;
  documentNumberFromSearchSales: string;
  salesQuotationDocumentVariableArrayForSearchSales: any;
  headerInformationArrayForSearchSales: Array<any> = [];
  termsIdForSaerchSalesArray: Array<number> = [];
  paymentTermsId: number;
  termDescriptionArray2: Array<number> = [];

  quotationTypeData: TypeOfQuotationForSalesAndQuotation = new TypeOfQuotationForSalesAndQuotation();
  docTypeName: Array<any> = [];
  docTypeId: Array<any> = [];
  disc: number;
  draft: number;
  archieved: number;
  rejected: number;
  approved: number;
  submitted: number;
  countryNm1: any;
  trialPhoneNumber1: any;
  paymentTerms: any;
  box1: number = 0;
  box2: number;
  dropdownList = [];
  dropdownList1 = [];
  dropdownList2 = [];
  trialDropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  termsData: Terms = new Terms();
  paymentTermsName: Array<any> = [];
  paymentTermsIds: Array<any> = [];
  normalTermsName: Array<any> = [];
  normalTermsIds: Array<any> = [];
  termsId: any;
  paymentTermsArray: Array<any> = [];
  paymentTermDescription: any;
  itemId: any;
  remarkForTerm: string;
  branch1: Branch = new Branch();
  branchesName: Array<any> = [];
  branchesIds: Array<any> = [];
  branchesDescription: Array<any> = [];

  cgstDisplay: number;
  sgstDisplay: number;
  igstDisplay: number;
  taxAmount: any;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private createUserService: CreateUserService, private createSalesService: CreateSalesService, private createCustomerService: CreateCustomerService, private searchSalesService: SearchSalesService) {

    this.manageDocumentForm = this.formBuilder.group({
      typeOfQuotation: [],
      dateOfQuotation: [],
      documentNumber: [],
      custRefNo: [],
      refDate: [],
      archieveDate: [],
      contactNo: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      email: ['', [optionalValidator([Validators.required, ValidationService.emailValidator])]],
      status: [],
      remark: [],
      createdBy: []
    });

    this.reqdocParaDetailsForm = this.formBuilder.group({

      Rows: this.formBuilder.array([this.initRows()])

    });

  }

  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];



  @ViewChild('focus') input: ElementRef;
  public toggleButton: boolean = true;

  ngOnInit() {


    (this.reqdocParaDetailsForm.get('Rows') as FormArray).valueChanges.subscribe(values => {
      console.log(values);

      if (this.dynamicArrayTrial.length != 0) {

        for (var i = 0, len = this.dynamicArrayTrial.length; i < len; i++) {
          if (this.dynamicArrayTrial[i].instruName != null) {
            if ((this.dynamicArrayTrial[i].srNo !== values[i].srNo || this.dynamicArrayTrial[i].sacCode !== values[i].sacCode || this.dynamicArrayTrial[i].idNo !== values[i].idNo || this.dynamicArrayTrial[i].instruName !== values[i].instruName ||
              this.dynamicArrayTrial[i].description !== values[i].description || this.dynamicArrayTrial[i].accrediation !== values[i].accrediation || this.dynamicArrayTrial[i].range !== values[i].range || this.dynamicArrayTrial[i].quantity !== values[i].quantity
              || this.dynamicArrayTrial[i].rate !== values[i].rate || this.dynamicArrayTrial[i].discountOnItem !== values[i].discountOnItem || this.dynamicArrayTrial[i].amount !== values[i].amount)
            ) {
              console.log("change" + i);
              var o = this.reqdocParaDetailsForm.get('Rows').value[i].active;
              document.getElementById("btn-" + i).style.backgroundColor = "#d9534f";

            }
          }
        }
      }
    });

    //get branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branch1 = data;
      console.log(this.branch1);
      for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
        this.branchesName.push(this.branch1[i].branchName);
        this.branchesIds.push(this.branch1[i].branchId);
        this.branchesDescription.push(this.branch1[i].description);

        this.branchWithDescription.push({ description: this.branch1[i].description, branch: this.branch1[i].branchName });
      }
    },
      error => console.log(error));

    // get term List
    this.createSalesService.getTermsList().subscribe(data => {
      this.termsData = data;
      console.log(this.termsData);

      for (var i = 0, l = Object.keys(this.termsData).length; i < l; i++) {
        if (this.termsData[i].description == "Payment") {
          this.paymentTermsName.push(this.termsData[i].termsName);
          this.paymentTermsIds.push(this.termsData[i].termsId);
          this.paymentTermList.push({ id: this.termsData[i].termsId, name: this.termsData[i].termsName })
        }
        else {

          this.dropdownList1.push({
            itemId: this.termsData[i].termsId, itemText: this.termsData[i].description

          })
          this.dropdownList2.push({
            itemId: this.termsData[i].termsId, itemText: this.termsData[i].termsName

          })

        }
      }
      this.dropdownList = this.dropdownList1;
    },
      error => console.log(error));


    //get reference list
    this.createSalesService.getReferenceList().subscribe(data => {
      this.referenceList = data;
      console.log(this.referenceList);
      for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
        this.referenceName.push(this.referenceList[i].referenceName);
        this.referenceId.push(this.referenceList[i].referenceId);
      }
    },
      error => console.log(error));

    //get instrument list
    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));


    this.selectedItems = [

    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'itemId',
      textField: 'itemText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
    };


    this.countryNm1 = this.createSalesService.getCustomerCountryForMobileNo1();


    // mobileNumber code

    var input1 = document.querySelector("#phoneno");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": [this.countryNm1],
      "separateDialCode": true
    });
    //  iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();

    var countryData1 = iti1.getSelectedCountryData();


    //  this.trialPhoneNumber1 = 91;

    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);
      console.log(a1);
      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      document.getElementById("country1").innerText = this.trialPhoneNumber1;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;
      console.log(this.trialPhoneNumber1);
    });

    //get quotation type list
    this.createSalesService.getQuotationTypeList().subscribe(data => {
      this.quotationTypeData = data;
      console.log(this.quotationTypeData);

      for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
        this.docTypeName.push(this.quotationTypeData[i].docTypeName);
        this.docTypeId.push(this.quotationTypeData[i].docTypeId);
      }
    },
      error => console.log(error));



    $(function () {
      // on init
      $(".table-hideable .hide-col").each(HideColumnIndex);

      // on click
      $('.hide-column').click(HideColumnIndex)

      function HideColumnIndex() {
        var $el = $(this);
        var $cell = $el.closest('th,td')
        var $table = $cell.closest('table')

        // get cell location - https://stackoverflow.com/a/4999018/1366033
        var colIndex = $cell[0].cellIndex + 1;

        // find and hide col index
        $table.find("tbody tr, thead tr")
          .children(":nth-child(" + colIndex + ")")
          .addClass('hide-col');

        // show restore footer
        $table.find(".footer-restore-columns").show()
      }

      // restore columns footer
      $(".restore-columns").click(function (e) {
        var $table = $(this).closest('table')
        $table.find(".footer-restore-columns").hide()
        $table.find("th, td")
          .removeClass('hide-col');

      })

      $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
      })

    })




    this.newDynamic1 = [{
      uniqueId: "",
      srNO: "",
      instruName: "",
      description: "",
      idNo: "",
      accreadtion: "",
      range: "",
      sacCode: "",
      quantity: "",
      rate: "",
      discountOnItem: "",
      amount: ""
    }]

    this.newDynamic = {
      uniqueId: "",
      srNo: "",
      instruName: "",
      description: "",
      idNo: "",
      accrediation: "",
      range: "",
      sacCode: "",
      quantity: "",
      rate: "",
      discountOnItem: "",
      amount: ""
    };
    this.dynamicArray.push(this.newDynamic);



    var jsonArrayForcsv = new Array();
    jsonArrayForcsv = this.createSalesService.getJsonArrayForCsv();

    this.arrayOfTerms = this.createSalesService.getArrayofTermsId();

    this.getDescriptionForId(this.arrayOfTerms);


    // get customer list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customerDataObject = data;
      console.log("customer data" + this.customerDataObject);

      this.getSalesAndQuotationDocumentInfo();

      for (var i = 0, l = Object.keys(this.customerDataObject).length; i < l; i++) {
        this.customersName.push(this.customerDataObject[i].name + " / " + this.customerDataObject[i].department);
        console.log("customers : " + this.customersName);

        this.customerNameArray.push(this.customerDataObject[i].name);
        console.log("CustomerList:" + this.customerNameArray);

        this.trialCustomerWithAddress.push({ name: this.customerDataObject[i].name + " / " + this.customerDataObject[i].department, address: this.customerDataObject[i].addressLine1 + "," + this.customerDataObject[i].addressLine2 + "," + this.customerDataObject[i].city + "," + this.customerDataObject[i].pin + "," + this.customerDataObject[i].state + "," + this.customerDataObject[i].country });
        console.log(this.trialCustomerWithAddress);
        this.customersIds.push(this.customerDataObject[i].id);

        console.log("customer id : " + this.customersIds);
      }
      if (this.customersIds.length == Object.keys(this.customerDataObject).length)
        this.getInformation();
    },
      error => console.log(error));


    $('form#myId input[type="text"]').bind("keyup", function () {
      var val = $(this).val();
      $(this).attr('value', val);
    });

  }

  //////////////////////////////code for button color change////////////////////////
  fieldGlobalIndex(index) {
    return (this.itemsPerPage * (this.p - 1)) + index;
  }


  get formArr() {
    return this.reqdocParaDetailsForm.get("Rows") as FormArray;
  }

  initRows() {
    return this.formBuilder.group({
      srNo: [],
      instruName: [],
      description: [],
      idNo: [],
      accrediation: [],
      range: [],
      sacCode: [],
      quantity: [],
      rate: [],
      discountOnItem: [],
      amount: [0],
    });
  }

  addNewRow() {
    this.formArr.push(this.initRows());
  }

  deleteRow1(index: number) {
    this.srNoToDelete = index + 1;

    for (var i = 0; i < this.salesQuotationDocumentVariableArray.length; i++) {
      if (this.salesQuotationDocumentVariableArray[i][2] == this.srNoToDelete) {
        this.indexToDelete = this.salesQuotationDocumentVariableArray[i][0];
        this.createSalesService.deleteRowForSaleseSrNo(this.indexToDelete).subscribe(data => {
          console.log("*********deleted row********" + data);
        })
        break;
      }
    }

    this.dynamicArrayTrial.splice(index, 1);
    this.salesQuotationDocumentVariableArray.splice(index, 1);
    this.formArr.removeAt(index);

    this.calculationAfterCustChange()

  }

  ngAfterViewInit() {
    this.leng = (this.reqdocParaDetailsForm.get('Rows') as FormArray).length;
    for (var l = 0; l < this.leng; l++) {

      if (this.dynamicArrayTrial.length != 0) {
        if (this.dynamicArrayTrial[l].instruName != null) {
          document.getElementById("btn-" + l).style.backgroundColor = "#5cb85c";
        }
        else {
          document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        }
      }
      else {
        document.getElementById("btn-" + l).style.backgroundColor = "#d9534f";
        break;
      }
    }
  }

  //Terms and condition
  changePaymentTerms() {
    this.box2 = 0;
    if (this.paymentTerms == "3") {

      this.termbox1 = 1;
      this.termbox2 = 1;

    }
    else if (this.paymentTerms == "4") {

      this.termbox1 = 1;
      this.termbox2 = 1;

    }
    else {
      this.termbox1 = 0;
      this.termbox2 = 0;
    }
    this.descriptionForPaymentTerms();
  }

  descriptionForPaymentTerms() {
    if (this.paymentTerms == "1") {
      this.paymentTermDescription = "Payment-Against completion of work";
      this.paymentTermsId = 1;

    }
    else if (this.paymentTerms == "2") {
      this.paymentTermDescription = "Payment-Against Proforma Invoice";
      this.paymentTermsId = 2;
    }
    else if (this.paymentTerms == "3") {
      this.box2 = 100 - this.box1;
      this.paymentTermDescription = "Payment-@" + " " + this.box1 + "% advance &@" + this.box2 + " " + "against  Performa Invoice";
      this.paymentTermsId = 3;

    } else if (this.paymentTerms == "4") {
      this.box2 = 100 - this.box1;
      this.paymentTermDescription = "Payment-@" + " " + this.box1 + "% advance &@" + this.box2 + " " + "against  completion of work.";
      this.paymentTermsId = 4;
    }
    else {

    }
  }

  onItemSelect(item: any) {
    console.log(item);
    this.termDescriptionArray.splice(0, this.termDescriptionArray.length);
    this.itemId = item.itemId;
    this.termDescriptionArray.push(item.itemId);
    this.descriptionForTerms();

  }

  onSelectAll(items: any) {
    console.log(items);
    this.termDescriptionArray1.splice(0, this.termDescriptionArray1.length);

    for (var j = 0; j < this.dropdownList2.length; j++) {

      this.termDescriptionArray1.push(this.dropdownList2[j].itemText);
      this.termDescriptionList.push({ id: this.dropdownList2[j].itemId, name: this.dropdownList2[j].itemText });
      this.termDescriptionArray2.push(this.dropdownList2[j].itemId);

    }

  }

  onDeselectAll(items: any) {
    this.termDescriptionArray1.splice(0, this.termDescriptionArray1.length);
    this.termDescriptionList.splice(0, this.termDescriptionList.length);
    this.termDescriptionArray2.splice(0, this.termDescriptionArray2.length);
    this.createSalesService.removeAllCustomerInCustomerAndTermsJunctionTable(this.originalDocNo).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);

    })

  }

  onItemDeSelect(item: any) {
    console.log(item);
    for (var i = 0; i < this.termDescriptionList.length; i++) {

      if (item.itemId == this.termDescriptionList[i].id) {
        var trial = [this.termDescriptionList[i].id];
        this.termDescriptionArray1.splice(0, this.termDescriptionArray1.length);
        this.termDescriptionArray2.splice(0, this.termDescriptionArray2.length);
        this.termDescriptionList = this.termDescriptionList.filter(el => (-1 == trial.indexOf(el.id)));
        for (let k = 0; k < this.termDescriptionList.length; k++) {
          this.termDescriptionArray1.push(this.termDescriptionList[k].name);
          this.termDescriptionArray2.push(this.termDescriptionList[k].id);
        }

        break;
      }

    }
    this.createSalesService.removeAllCustomerInCustomerAndTermsJunctionTable(this.originalDocNo).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);

    })

  }

  descriptionForTerms() {

    for (var i = 0; i < this.termDescriptionArray.length; i++) {
      for (var j = 0; j < this.dropdownList2.length; j++) {

        if (this.termDescriptionArray[i] == this.dropdownList2[j].itemId) {
          this.termDescriptionArray1.push(this.dropdownList2[j].itemText);
          this.termDescriptionList.push({ id: this.dropdownList2[j].itemId, name: this.dropdownList2[j].itemText });
          this.termDescriptionArray2.push(this.dropdownList2[j].itemId);
        }

        else {

        }

      }
    }

  }

  // get customer information from create sales and quotation screen
  getInformation() {

    for (var i = 0; i < Object.keys(this.customersIds).length; i++) {
      if (this.createSalesandQuotation.customerId == this.customersIds[i]) {
        this.customerNameTrial = this.customerDataObject[i].name;
        this.customerPanNo = this.customerDataObject[i].panNo;
        this.customerGstNo = this.customerDataObject[i].gstNo;
        this.customerAddressline1 = this.customerDataObject[i].addressLine1;
        this.customerAddressline2 = this.customerDataObject[i].addressLine2;
        this.customerCountry = this.customerDataObject[i].country;
        this.customerState = this.customerDataObject[i].state;
        this.customerCity = this.customerDataObject[i].city;
        this.customerPin = this.customerDataObject[i].pin;
        this.cgst = this.customerDataObject[i].cgst;
        this.sgst = this.customerDataObject[i].sgst;
        this.igst = this.customerDataObject[i].igst;
        this.dateOfQuotation1 = this.salesAndQuotationForDocument.dateOfQuotation;
        this.convertCustomerRefNoToName();

        this.referenceDate1 = this.salesAndQuotationForDocument.refDate;
        this.kindAttentionTrial = this.salesAndQuotationForDocument.kindAttachment;

        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
        break;
      }

      console.log(this.salesDocumentInfo);
    }

    this.splitGstAndQuotationNumber();
    this.clearArray();

  }


  convertCustomerRefNoToName() {
    if (typeof this.referenceList != 'undefined') {
      for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
        if (this.salesAndQuotationForDocument.custRefNo == this.referenceList[i].referenceId) {
          this.customerReferenceTrial = this.referenceList[i].referenceName;
          break;

        }
      }
    }
  }
  // To get quotation number with prefix
  splitGstAndQuotationNumber() {
    if (typeof this.customerGstNo != 'undefined') {
      this.stateCode = this.customerGstNo.slice(0, 2);
    }

    if (typeof this.salesAndQuotationForDocument.documentNumber != 'undefined') {
      this.originalDocNo = this.salesAndQuotationForDocument.documentNumber;
      this.quotationNumber = this.salesAndQuotationForDocument.documentNumber.slice(17, 22);
    }


    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchId == this.salesAndQuotationForDocument.branchId)

        this.prefixQuotationNumber = this.branch1[i].description;
    }
    this.quotationNumberWithPrefix = this.prefixQuotationNumber + this.quotationNumber;

    this.salesDocumentInfo.push(this.stateCode);
    this.salesDocumentInfo.push(this.quotationNumber);

    console.log("salesdocumentinfo..." + this.salesDocumentInfo)
  }

  clearArray() {
    this.salesDocumentInfo.slice(0, this.salesDocumentInfo.length);
    this.csvUpload();
  }





  /////////////////////////////////Customer name change///////////////////////////

  customerNameChange() {
    for (var s = 0; s < this.customersName.length; s++) {
      if (this.customerNameTrial == this.customersName[s]) {
        this.customerPanNo = this.customerDataObject[s].panNo;
        this.customerGstNo = this.customerDataObject[s].gstNo;
        this.customerAddressline1 = this.customerDataObject[s].addressLine1;
        this.customerAddressline2 = this.customerDataObject[s].addressLine2;
        this.customerCountry = this.customerDataObject[s].country;
        this.customerState = this.customerDataObject[s].state;
        this.customerCity = this.customerDataObject[s].city;
        this.customerPin = this.customerDataObject[s].pin;
        this.cgst = this.customerDataObject[s].cgst;
        this.sgst = this.customerDataObject[s].sgst;
        this.igst = this.customerDataObject[s].igst;

        if (this.cgst == 0) {
          this.cgstDisplay = 0;
        }
        else {
          this.cgstDisplay = 1;
        }
        if (this.sgst == 0) {
          this.sgstDisplay = 0;
        }
        else {
          this.sgstDisplay = 1;
        }
        if (this.igst == 0) {
          this.igstDisplay = 0;
        }
        else {
          this.igstDisplay = 1;
        }
        this.calculationAfterCustChange();
        break;
      }
    }

  }

  calculationAfterCustChange() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.subTotal = 0;
    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }



      this.netValue = this.total + this.taxAmount;
     // this.netValue = this.total + this.taxesTotal;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    else {
      this.discountOnSubTotal();
    }

  }

  ///////////////// get Quotation item file///////
  getSalesAndQuotationDocumentInfo() {
    this.salesAndQuotationForDocument = this.createSalesService.getSalesQuotationDocument();
    console.log(this.salesAndQuotationForDocument);
    this.createSalesandQuotation.customerId = this.salesAndQuotationForDocument.customerId
    this.selectedFileForQuotation = this.createSalesService.getQuotationFileDocument();


  }



  csvUpload() {

    var trial = this.selectedFileForQuotation;
    var csv = $("#fileUploadCSV").val();
    console.log("#fileUploadCSV ..." + csv);

    debugger;
    if (typeof this.selectedFileForQuotation != 'undefined') {

      if (this.selectedFileForQuotation.length > 0) {
        var first_Row_Cells = splitCSVtoCells(this.selectedFileForQuotation[0], ","); //Taking Headings

        if (first_Row_Cells.length != 10) {
          alert('Please upload a valid csv ,Colums count not matching');
          return;
        }
        if (first_Row_Cells[0] != "srNo") {
          alert('Please upload a valid csv, Check Heading Sr. No.');
          return;
        }
        if (first_Row_Cells[1] != "instruName") {
          alert('Please upload a valid csv, Check Heading DESCRIPTION');
          return;

        }
        if (first_Row_Cells[2] != "idNo") {
          alert('Please upload a valid csv, Check heading ID. NO');
          return;
        }
        if (first_Row_Cells[3] != "accrediation") {
          alert('Please upload a valid csv, Check heading RANGE/ACCURACY');
          return;
        }
        if (first_Row_Cells[4] != "range") {
          alert('Please upload a valid csv, Check heading RANGE/ACCURACY');
          return;
        }
        if (first_Row_Cells[5] != "sacCode") {
          alert('Please upload a valid csv, Check heading SAC CODE');
          return;
        }
        if (first_Row_Cells[6] != "quantity") {
          alert('Please upload a valid csv, Check heading QUANTITY');
          return;
        }
        if (first_Row_Cells[7] != "rate") {
          alert('Please upload a valid csv, Check heading RATE');
          return;
        }
        if (first_Row_Cells[8] != "discountOnItem") {
          alert('Please upload a valid csv, Check heading DISCOUNT_ON_ITEM');
          return;
        }

        if (first_Row_Cells[9] != "amount") {
          alert('Please upload a valid csv, Check heading AMOUNT');
          return;
        }


        var jsonArray = new Array();

        for (var i = 1; i < this.selectedFileForQuotation.length; i++) {
          var cells = splitCSVtoCells(this.selectedFileForQuotation[i], ",");

          var obj = {};
          for (var j = 0; j < cells.length; j++) {
            obj[first_Row_Cells[j]] = cells[j];
          }
          jsonArray.push(obj);
        }

        console.log(jsonArray);
        var html = "";

        var count = 0;
        for (i = 0; i < jsonArray.length - 2; i++) {
          this.addNewRow();
        }

        this.dynamicArray.splice(0, this.dynamicArray.length);
        for (i = 0; i < jsonArray.length - 1; i++) {

          if (jsonArray[i].Name != "") {

            this.dynamicArray.push(jsonArray[i]);
            this.getAmount(i);
            this.patchValueFunction(i);

          }

        }

      }
    }

    function splitCSVtoCells(row, separator) {
      return row.split(separator);
    }


  }

  patchValueFunction(b) {
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('instruName').patchValue(this.dynamicArray[b].instruName);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('description').patchValue(this.dynamicArray[b].description);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('idNo').patchValue(this.dynamicArray[b].idNo);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('accrediation').patchValue(this.dynamicArray[b].accrediation);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('range').patchValue(this.dynamicArray[b].range);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('quantity').patchValue(this.dynamicArray[b].quantity);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('rate').patchValue(this.dynamicArray[b].rate);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('discountOnItem').patchValue(this.dynamicArray[b].discountOnItem);
    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('sacCode').patchValue(this.dynamicArray[b].sacCode);

  }

  ///////////////code for calculation//////////////////

  // if Item Discount is 0
  getAmount1(b) {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.subTotal = 0;
    this.taxesTotal = 0;
    this.taxAmount = 0;


    this.dynamicArray[b].amount = this.dynamicArray[b].quantity * this.dynamicArray[b].rate;
    this.dynamicArray[b].amount = parseFloat(this.dynamicArray[b].amount.toFixed(2));



    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('amount').patchValue(this.dynamicArray[b].amount);


    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }


    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;


      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }

      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    if (this.dynamicArray[b].discountOnItem != 0) {
      this.getAmount(b);
    }
  }


  // convert net value in words
  toWords() {

    var th = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
    var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    this.netValue = this.netValue.toString();
    this.netValue = this.netValue.replace(/[\, ]/g, '');
    if (this.netValue != parseFloat(this.netValue)) return 'not a number';
    var x = this.netValue.indexOf('.');
    if (x == -1) x = this.netValue.length;
    if (x > 15) return 'too big';
    var n = this.netValue.split('');
    var str = 'Rupees ';
    var sk = 0;
    for (var i = 0; i < x; i++) {
      if ((x - i) % 3 == 2) {
        if (n[i] == '1') {
          str += tn[Number(n[i + 1])] + ' ';
          i++;
          sk = 1;
        }
        else if (n[i] != 0) {
          str += tw[n[i] - 2] + ' ';
          sk = 1;
        }
      }
      else if (n[i] != 0) {
        str += dg[n[i]] + ' ';
        if ((x - i) % 3 == 0) str += 'hundred ';
        sk = 1;
      }


      if ((x - i) % 3 == 1) {
        if (sk) str += th[(x - i - 1) / 3] + ' ';
        sk = 0;
      }
    }
    if (x != this.netValue.length) {
      var y = this.netValue.length;
      str = str + 'and ';
      for (var j = x + 1; j < y; j++) {
        if ((y - j) % 3 == 2) {
          if (n[j] == '1') {
            str += tn[Number(n[j + 1])] + ' ';
            j++;
            sk = 1;
          }
          else if (n[j] != 0) {
            str += tw[n[j] - 2] + ' ';
            sk = 1;
          }
          else if (n[j] == 0 && n[j + 1] == 0) {
            str += 'Zero ';
            sk = 1;
            break;
          }
        }
        else if (n[j] != 0) {
          str += dg[n[j]] + ' ';
          if ((y - j) % 3 == 0) str += 'hundred ';
          sk = 1;
        }


        if ((y - j) % 3 == 1) {
          if (sk) str += th[(y - j - 1) / 3] + ' ';
          sk = 0;
        }
      }
      str += ' paise only';
    }
    this.netValueInWords = str.replace(/\s+/g, ' ');
  }



  getAmountTrial(b) {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.getAmount(b);
  }

  // if item discount is not 0
  getAmount(b) {

    this.subTotal = 0;
    this.taxesTotal = 0;
    this.taxAmount = 0;

    var discountedRate = (this.dynamicArray[b].rate - (this.dynamicArray[b].rate * (this.dynamicArray[b].discountOnItem / 100)));
    this.dynamicArray[b].amount = this.dynamicArray[b].quantity * discountedRate;
    this.dynamicArray[b].amount = parseFloat(this.dynamicArray[b].amount.toFixed(2));

    ((this.reqdocParaDetailsForm.get('Rows') as FormArray).at(b) as FormGroup).get('amount').patchValue(this.dynamicArray[b].amount);


    for (var a = 0; a < this.dynamicArray.length; a++) {
      this.subTotal = this.subTotal + this.dynamicArray[a].amount;
    }

    if (this.discountOnSubtotal == 0) {
      this.total = this.subTotal;

      if (this.cgst == 1) {
        this.cgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

      }
      if (this.igst == 1) {
        this.igstAmount = this.total * (18 / 100);
        this.taxAmount = this.taxAmount + this.total * (18 / 100);
        this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

      }
      if (this.sgst == 1) {
        this.sgstAmount = this.total * (9 / 100);
        this.taxAmount = this.taxAmount + this.total * (9 / 100);
        this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

      }

      this.netValue = this.total + this.taxAmount;
      this.netValue = parseFloat(this.netValue).toFixed(2);
      console.log(this.netValue);
      this.toWords();
    }
    else {
      this.discountOnSubTotal();
    }

  }

  // if discount on subtotal is not 0
  discountOnSubTotal() {
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    this.taxAmount = 0;
    this.total = 0;
    this.taxesTotal = 0;
    this.total = this.subTotal - (this.subTotal * (this.discountOnSubtotal / 100));
    this.total = parseFloat(this.total.toFixed(2));
    if (this.cgst == 1) {
      this.cgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;

    }
    if (this.igst == 1) {
      this.igstAmount = this.total * (18 / 100);
      this.taxAmount = this.taxAmount + this.total * (18 / 100);
      this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;

    }
    if (this.sgst == 1) {
      this.sgstAmount = this.total * (9 / 100);
      this.taxAmount = this.taxAmount + this.total * (9 / 100);
      this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;

    }

    this.netValue = this.total + this.taxAmount;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    console.log(this.netValue);
    this.toWords();
  }


  ///////////////////// code to save details for sr no for one row/////////////////
  saveItemDetails(i) {
    document.getElementById("btn-" + i).style.backgroundColor = "#5cb85c";
    console.log(this.reqdocParaDetailsForm.value);
    this.dynamicArray = this.reqdocParaDetailsForm.get('Rows').value;
    console.log(this.dynamicArray);
    this.salesQuotationDocumentObject.documentNumber = this.originalDocNo;

    this.salesQuotationDocumentObject.srNo = i + 1;
    this.convertInstruNametoId(i);

    this.salesQuotationDocumentObject.description = this.dynamicArray[i].description;
    this.salesQuotationDocumentObject.idNo = this.dynamicArray[i].idNo;
    this.salesQuotationDocumentObject.accrediation = this.dynamicArray[i].accrediation;
    this.salesQuotationDocumentObject.rangeAccuracy = this.dynamicArray[i].range;
    this.salesQuotationDocumentObject.sacCode = this.dynamicArray[i].sacCode;
    this.salesQuotationDocumentObject.quantity = this.dynamicArray[i].quantity;
    this.salesQuotationDocumentObject.rate = this.dynamicArray[i].rate;
    this.salesQuotationDocumentObject.discountOnItem = this.dynamicArray[i].discountOnItem;
    this.salesQuotationDocumentObject.amount = this.dynamicArray[i].amount;

    this.createSalesService.sendDocNoOfSalesQuotationDocument(this.originalDocNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);
    })

    this.createSalesService.getSrNoForSalesQuotationDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.salesQuotationDocumentVariableArray = data;
     
      this.saveSQDocItemDatawithSrNo();
      this.dynamicArrayTrial = this.dynamicArray;

    })
  }

  convertInstruNametoId(k) {
    this.dynamicArray[k].instruName = this.dynamicArray[k].instruName.trim();
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentName == this.dynamicArray[k].instruName) {
        this.salesQuotationDocumentObject.instruName = this.instrumentList[i].instrumentId;
        break;
      }
    }

  }
  saveSQDocItemDatawithSrNo() {
    if (this.salesQuotationDocumentVariableArray.length == 0) {
      this.createSalesService.createSalesQuotationDocumentForDatabase(this.salesQuotationDocumentObject).subscribe(data => {
        console.log("*********data saved********" + data);
        alert("data saved succesfully");
      })
    }

    else {
      for (this.n = 0; this.n < this.salesQuotationDocumentVariableArray.length; this.n++) {
        if (this.salesQuotationDocumentVariableArray[this.n][2] == parseInt(this.salesQuotationDocumentObject.srNo)) {
          this.createSalesService.updateSalesQuotationDocumentForDatabase(this.salesQuotationDocumentVariableArray[this.n][0], this.salesQuotationDocumentObject).subscribe(data => {
            console.log("data updated successfully" + data);
            alert("data updated succesfully");
          }
          );
          break;
        }
      }
      if (this.n >= this.salesQuotationDocumentVariableArray.length) {
        this.createSalesService.createSalesQuotationDocumentForDatabase(this.salesQuotationDocumentObject).subscribe(data => {
          console.log("*********data saved********" + data);
          alert("data saved successfully");
        })
      }
    }


    this.createSalesService.getSrNoForSalesQuotationDocument().subscribe(data => {
      console.log("*********sr nos collection********" + data);
      this.salesQuotationDocumentVariableArray = data;
    })

  }



  ////////////////change term name////////////////////
  termNameChange(e) {
    let name = e
    if (name != "" && typeof name != 'undefined') {
      let list = this.termslist.filter(x => x.name == name)[0];

      if (typeof list != 'undefined') {
        this.arrayOfTerms.splice(0, this.arrayOfTerms.length);
        this.arrayOfTerms.push(list.id);
        this.getDescriptionForId(this.arrayOfTerms);
      }
    }

  }


  ////////////////get description for term id///////////////////////
  getDescriptionForId(arrTerms) {

    this.arrayOfTerms = arrTerms;

    for (let i = 0; i < this.arrayOfTerms.length; i++) {
      if (this.arrayOfTerms[i] == 1) {
        this.termsIdArray[this.j] = "• Bus/train/taxi/local charges are at actual extra." + '\n';
        this.termsIdArray3.push(" Bus/train/taxi/local charges are at actual extra." + '\n')
        this.termsList1.push({ id: 1, name: " Bus/train/taxi/local charges are at actual extra." + '\n' })
        this.termsIdArray4.push(1);
        this.l++;
      }
      if (this.arrayOfTerms[i] == 2) {
        this.termsIdArray[this.j] = "• 5% discount on quotation no. PN-171.";
        this.termsIdArray3.push(" 5% discount on quotation no. PN-171." + '\n')
        this.termsList1.push({ id: 2, name: " 5% discount on quotation no. PN-171." + '\n' })
        this.termsIdArray4.push(2);
        this.l++;
      }
      if (this.arrayOfTerms[i] == 3) {
        this.termsIdArray[this.j] = "• 10 % QTN NO 159";
        this.termsIdArray3.push(" 10 % QTN NO 159" + '\n')
        this.termsList1.push({ id: 3, name: " 10 % QTN NO 159" + '\n' })
        this.termsIdArray4.push(3);
        this.l++;
      }
      if (this.arrayOfTerms[i] == 4) {

        this.termsIdArray4.push(4);

        this.termsIdArray[this.j] = "Payment: Against Proforma invoice. *Delivery Period: -within 1 WEEK Packing Forward: NIL Octroi; *Other duties such as insurance both to and fro if any in your A/C 12.5 % VAT EXTRA."

        this.termsIdArray2 = this.termsIdArray[this.j].split('*');
        var leng = this.termsIdArray2.length;
        for (let i of this.termsIdArray2) {
          this.termsIdArray3.push(i);
          this.termsList1.push({ id: 4, name: i })

        }

        this.tl.push(this.termsIdArray[this.j].length);

        console.log(this.tl);
        this.j++;

        this.termsIdArray2.splice(0, this.termsIdArray2.length);
      }
      if (this.arrayOfTerms[i] == 5) {

        this.termsIdArray4.push(5);
        this.termsIdArray[this.j] = "Payment- @ % advance &@  %against Completion of work. *GST NO. 27AABCU3149C1ZL,Taxes: CSGT 9%, SGCST 9%,IGST 18% (SAC: 00998346) *On site calibration- For onsite calibration To & from traveling,lodging,boarding & local conveyance to be arranged by you. (Nil for PCMC) *Spares-Extra if any for repair *Transportation-Transportation of your instruments, octri (Nil for PCMC) & other duties such as insurance both to and from if any in your Account.*Instruments status-While calibration instruments should be in working condition *Calibration Schedule-Calibration schedule should be informed at least  one week in advance or as per mutual disscussion. *Certainty-Quoted rates are best understanding from information’s given by you. At the time of calibration capability  for calibration & charges may changes."
        this.termsIdArray2 = this.termsIdArray[this.j].split('*');

        var leng = this.termsIdArray2.length;
        for (let i of this.termsIdArray2) {
          this.termsIdArray3.push(i);
          this.termsList1.push({ id: 5, name: i })
        }


        this.tl.push(this.termsIdArray[this.j].length);

        console.log(this.tl);
        this.j++;

        this.termsIdArray2.splice(0, this.termsIdArray2.length);

      }
      if (this.arrayOfTerms[i] == 6) {
        this.termsIdArray4.push(6);
        this.termsIdArray[this.j] = 'Payment-Against Completion of work. *GST NO. 27AABCU3149C1ZL, Taxes: CSGT 9%, SGCST 9%, IGST 18% (SAC: 00998346)* On site calibration-For onsite calibration To & from traveling, lodging, boarding & local conveyance to be arranged by you.(Nil for PCMC) * Spares-Extra if any for repair * Transportation-Transportation of your instruments, octri (Nil for PCMC) & other duties such as insurance both to and fro if any in your Account. * Instruments status-While calibration instruments should be in working condition * Calibration Schedule-Calibration schedule should be informed at least one week in advance or as per mutual discussion. * Certainty-Quoted rates are best understanding from information’s given by you. At the time of calibration capability for calibration & charges may changes.';
        this.termsIdArray2 = this.termsIdArray[this.j].split('*');

        var leng = this.termsIdArray2.length;

        for (let i of this.termsIdArray2) {
          this.termsIdArray3.push(i);
          this.termsList1.push({ id: 6, name: i })
        }


        this.tl.push(this.termsIdArray[this.j].length);

        console.log(this.tl);
        this.j++;

        this.termsIdArray2.splice(0, this.termsIdArray2.length);
      }
      if (this.arrayOfTerms[i] == 7) {
        this.termsIdArray4.push(7);
        this.termsIdArray[this.j] = "Payment-Against Proforma Invoice. * GST NO. 27AABCU3149C1ZL, Taxes: CSGT 9%, SGCST 9%, IGST 18% (SAC: 00998346) *On site calibration-For onsite calibration To & from traveling , lodging , boarding & local conveyance to be arranged by you.(Nil for PCMC) *Spares-Extra if any for repair *Transportation-Transportation of your instruments, octri (Nil for PCMC) & other duties such as insurance both to and fro if any in your Account. *Instruments status-While calibration instruments should be in working condition *Calibration Schedule-Calibration schedule should be informed at least one week in advance or as per mutual discussion. *Certainty-Quoted rates are best understanding from information’s given by you. At the time of calibration capability for calibration & charges may changes.";
        this.termsIdArray2 = this.termsIdArray[this.j].split('*');

        var leng = this.termsIdArray2.length;

        for (let i of this.termsIdArray2) {
          this.termsIdArray3.push(i);
          this.termsList1.push({ id: 7, name: i })
        }

        this.tl.push(this.termsIdArray[this.j].length);
        console.log(this.tl);

        this.j++;

        this.termsIdArray2.splice(0, this.termsIdArray2.length);

      }
      if (this.arrayOfTerms[i] == 8) {
        this.termsIdArray[this.j] = "• Payment :-Against Delivery Service Tax :-@14%, S.B.C: @ 0.5%, K.K.C @ 0.5%";
        this.termsIdArray3.push(" Payment :-Against Delivery Service Tax :-@14%, S.B.C: @ 0.5%, K.K.C @ 0.5%" + '\n')
        this.termsList1.push({ id: 8, name: " Payment :-Against Delivery Service Tax :-@14%, S.B.C: @ 0.5%, K.K.C @ 0.5%" + '\n' })
        this.termsIdArray4.push(8);
        this.j++;
      }
      if (this.arrayOfTerms[i] == 9) {
        this.termsIdArray[this.j] = "• On site calibration visiting charges are rs. 1000/- extra.";
        this.termsIdArray3.push(" On site calibration visiting charges are rs. 1000/- extra." + '\n')
        this.termsList1.push({ id: 9, name: " On site calibration visiting charges are rs. 1000/- extra." + '\n' })
        this.termsIdArray4.push(9);
        this.j++;
      }
      if (this.arrayOfTerms[i] == 10) {
        this.termsIdArray[this.j] = "• On site calibration visiting charges are rs. 2000/- are extra." + "\n";
        this.termsIdArray3.push(" On site calibration visiting charges are rs. 2000/- are extra." + '\n')
        this.termsList1.push({ id: 10, name: " On site calibration visiting charges are rs. 2000/- are extra." + '\n' })
        this.termsIdArray4.push(10);
        this.j++;

      }
    }

  }


  //////////////////////////////////////////////// code for delete term with id ////////////////////////////

  deleteTerms(i) {

    this.termsIdArray3.splice(i, 1);
    var listToDelete = [this.termsList1[i].id];
    var termsToDelete = this.termsList1[i].id;
    this.termsList1 = this.termsList1.filter(el => (-1 == listToDelete.indexOf(el.id)));

    this.termsIdArray3.splice(0, this.termsIdArray3.length);
    this.termsIdArray4.splice(this.termsIdArray4.indexOf(termsToDelete), 1);

    for (let k = 0; k < this.termsList1.length; k++) {
      this.termsIdArray3.push(this.termsList1[k].name)

    }

    console.log(" this.termslist1" + this.termsList1);

    return true;

  }

  /////////////////////////save all data///////////////////////


  saveAllData() {

    this.salesAndQuotationForDocument1ForManageDocument = this.createSalesService.getSalesQuotationDocumentWithId();
    this.salesQuotationDocumentVariable = this.salesAndQuotationForDocument1ForManageDocument;

    for (var s = 0; s < this.customersName.length; s++) {
      if (this.customerNameTrial == this.customersName[s]) {
        this.salesQuotationDocumentVariable.customerId = this.customerDataObject[s].id;
        break;
      }
    }


    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].description == this.prefixQuotationNumber) {
        this.salesQuotationDocumentVariable.branchId = this.branch1[i].branchId;
        this.salesQuotationDocumentVariable.quotationNumber = this.prefixQuotationNumber + this.quotationNumber;
      }
    }


    this.id = this.salesQuotationDocumentVariable.id;
    this.salesQuotationDocumentVariable.custRefNo = this.customerReferenceTrial;
    this.salesQuotationDocumentVariable.refDate = this.referenceDate1;
    this.salesQuotationDocumentVariable.dateOfQuotation = this.dateOfQuotation1;
    this.salesQuotationDocumentVariable.kindAttachment = this.kindAttentionTrial;

    for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
      if (this.referenceList[i].referenceName == this.customerReferenceTrial) {
        this.salesQuotationDocumentVariable.custRefNo = this.referenceList[i].referenceId;
        break;

      }
    }

    this.createSalesService.updateSalesAndQuotation(this.id, this.salesQuotationDocumentVariable).subscribe(data => {
      console.log(data);
    })


    this.salesQuotationDocumentCalculationObject.documentNumber = this.originalDocNo;
    this.salesQuotationDocumentCalculationObject.subTotal = this.subTotal;
    this.salesQuotationDocumentCalculationObject.discountOnSubtotal = this.discountOnSubtotal;
    this.salesQuotationDocumentCalculationObject.total = this.total;
    this.salesQuotationDocumentCalculationObject.net = parseFloat(this.netValue);
    if(this.cgstDisplay==1){
      this.salesQuotationDocumentCalculationObject.cgst = this.cgstAmount;
    }
    else{
      this.salesQuotationDocumentCalculationObject.cgst = 0;
    }
    if(this.igstDisplay==1){
      this.salesQuotationDocumentCalculationObject.igst = this.igstAmount;
    }
    else{
      this.salesQuotationDocumentCalculationObject.igst = 0;
    }
    if(this.sgstDisplay==1){
      this.salesQuotationDocumentCalculationObject.sgst = this.sgstAmount;
    }
    else{
      this.salesQuotationDocumentCalculationObject.sgst =0;
    }
  
   
    


    this.createSalesService.sendDocNoOfSalesQuotationDocumentCalculation(this.originalDocNo).subscribe(data => {
      console.log("*********send doc no ********" + data);
    })

    this.createSalesService.getDocDetailsForSalesQuotationDocumentCalculation().subscribe(data => {
      console.log("*********doc no details collection********" + data);
      this.salesQuotationDocumentCalculationVariableArray = data;

      this.saveSQDocCalcDetails();

    })




    this.createSalesService.removeAllCustomerInCustomerAndTermsJunctionTable(this.originalDocNo).subscribe((res) => {
      this.printTermsId = res;
      console.log(this.printTermsId);
      this.saveAllTermsId();

    })

    alert("Data Submitted Successfully!!")

  }

  saveAllTermsId() {

    for (var p = 0; p < this.paymentTermsIds.length; p++) {
      if (this.paymentTermsIds[p] == this.paymentTermsId) {
        this.customerAndTermId.customerId = this.salesQuotationDocumentVariable.customerId;
        this.customerAndTermId.termsId = this.paymentTermsId;
        this.customerAndTermId.documentNumber = this.originalDocNo;
        if (this.paymentTermsId == 3 || this.paymentTermsId == 4) {
          this.customerAndTermId.box1 = this.box1;
          this.customerAndTermId.box2 = this.box2;
        }
        else {
          this.customerAndTermId.box1 = 0;
          this.customerAndTermId.box2 = 0;
        }
        if (this.remarkForTerm == null || typeof this.remarkForTerm == 'undefined')
          this.customerAndTermId.remark = " ";
        else
          this.customerAndTermId.remark = this.remarkForTerm;

        this.createSalesService.addCustomerInCustomerAndTermsJunctionTable(this.customerAndTermId).subscribe((res) => {
          this.printTermsId = res;
          console.log(this.printTermsId);

        })
      }
    }


    for (var p = 0; p < this.dropdownList.length; p++) {
      for (var m = 0; m < this.termDescriptionArray2.length; m++) {
        if (this.termDescriptionArray2[m] == this.dropdownList[p].itemId) {
          this.customerAndTermId.customerId = this.salesQuotationDocumentVariable.customerId;
          this.customerAndTermId.termsId = this.termDescriptionArray2[m];
          this.customerAndTermId.documentNumber = this.originalDocNo;
          this.customerAndTermId.box1 = 0;
          this.customerAndTermId.box2 = 0;
          if (this.remarkForTerm == null || typeof this.remarkForTerm == 'undefined')
            this.customerAndTermId.remark = " ";
          else
            this.customerAndTermId.remark = this.remarkForTerm;

          this.createSalesService.addCustomerInCustomerAndTermsJunctionTable(this.customerAndTermId).subscribe((res) => {
            this.printTermsId = res;
            console.log(this.printTermsId);
          })
        }
      }
    }


  }

  ////////////////// save SQ Doc Calculation Detail
  saveSQDocCalcDetails() {
    if (this.salesQuotationDocumentCalculationVariableArray.length == 0) {
      this.createSalesService.createSalesQuotationDocumentCalculation(this.salesQuotationDocumentCalculationObject).subscribe(data => {
        console.log("data updated successfully in SalesQuotationDocumentCalculationtable" + data);
      }
      );

    }
    else {
      this.createSalesService.updateSalesQuotationDocumentCalculation(this.salesQuotationDocumentCalculationVariableArray[0][0], this.salesQuotationDocumentCalculationObject).subscribe(data => {
        console.log("data updated successfully" + data);
      }
      );


    }


  }


  ///////////////// back to sales and quotation
  backToMenu() {
    this.router.navigate(['/nav/createsales']);
  }

  /******************************  code for manage document****************************************/

  manageDocument() {

    this.salesAndQuotationForDocument1ForManageDocument = this.createSalesService.getSalesQuotationDocumentWithId();
    this.getInformationForManageDocument();
    const buttonModal1 = document.getElementById("openModalButton1")
    console.log('buttonModal1', buttonModal1)

    buttonModal1.click()
  }


  getInformationForManageDocument() {
    this.manageDocumentVariable = this.salesAndQuotationForDocument1ForManageDocument;

    if (typeof this.salesAndQuotationForDocument1ForManageDocument != 'undefined') {
      this.convertIdToName();
      this.documentNumber = this.manageDocumentVariable.documentNumber;
      this.dateOfQuotation = this.manageDocumentVariable.dateOfQuotation;
      this.refDate = this.manageDocumentVariable.refDate;
      this.archieveDate = this.manageDocumentVariable.archieveDate;
      this.kindAttentionTrial1 = this.manageDocumentVariable.kindAttachment;
      this.contactNo = this.manageDocumentVariable.contactNo;
      this.email = this.manageDocumentVariable.email;
      this.id = this.manageDocumentVariable.id;
      this.createdBy = this.manageDocumentVariable.createdBy;


      this.contactNo = this.manageDocumentVariable.contactNo.slice(-10);
      var mb1 = new String(this.manageDocumentVariable.contactNo)
      this.trialPhoneNumber1 = this.manageDocumentVariable.contactNo.slice(-mb1.length + 1, -10);
      console.log("value of this.trialPhoneNumber1" + this.trialPhoneNumber1)

    }
  }

  convertIdToName() {
    for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
      if (this.quotationTypeData[i].docTypeId == this.manageDocumentVariable.typeOfQuotation) {
        this.typeOfQuotation = this.quotationTypeData[i].docTypeName;

      }

    }
  }


  onSubmit() {

    this.manageDocumentVariable.email = this.email;
    this.manageDocumentVariable.archieveDate = this.archieveDate;
    this.manageDocumentVariable.createdBy = this.createdBy;
    this.manageDocumentVariable.remark = this.remark;

    for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
      if (this.quotationTypeData[i].docTypeName == this.typeOfQuotation) {
        this.manageDocumentVariable.typeOfQuotation = this.quotationTypeData[i].docTypeId;
      }
    }


    for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
      if (this.referenceList[i].referenceName == this.customerReferenceTrial) {
        this.manageDocumentVariable.custRefNo = this.referenceList[i].referenceId;
        break;

      }
    }


    //save Mobile number with country code
    if (typeof this.contactNo != 'undefined') {
      this.manageDocumentVariable.contactNo = "+" + this.trialPhoneNumber1 + this.contactNo;

    }
    else {
      this.manageDocumentVariable.contactNo = null;
    }

    this.manageDocumentVariable.countryPrefixForMobile = this.phoneNumberCountryNm1;


    // this.manageDocumentVariable.status = this.status;
    switch (this.status) {
      case 'Draft': this.manageDocumentVariable.draft = 1;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Archieved': this.manageDocumentVariable.draft = 0
        this.manageDocumentVariable.archieved = 1;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Rejected': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 1;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Approved': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 1;
        this.manageDocumentVariable.submitted = 0;
        break;
      case 'Submitted': this.manageDocumentVariable.draft = 0;
        this.manageDocumentVariable.archieved = 0;
        this.manageDocumentVariable.rejected = 0;
        this.manageDocumentVariable.approved = 0;
        this.manageDocumentVariable.submitted = 1;
        break;

    }


    alert("Data Updated succesfully!!");
    this.createSalesService.updateSalesAndQuotation(this.id, this.manageDocumentVariable).subscribe(data => {
      console.log(data);

    })
  }

  ////////////////////////Manage footer///////////////

  manageFooter() {
    const buttonModal = document.getElementById("openModalButton")
    console.log('buttonModal', buttonModal)
    buttonModal.click()
  }

  footerType(e) {
    console.log(e);
    this.selectTagEvent = e;
    if (this.selectTagEvent == 4) {
      this.disc = 1;
    }

  }

  // get selected taxes from manage footer
  changeTaxes(e) {
    if (e == 1) {
      this.taxesArray.push(1);
      this.taxesArrayName.push("CGST 9%");
      this.taxesList.push({ name: "CGST 9%", taxAmount: 0 });
      this.getAmount(0);
    }

    if (e == 2) {
      this.taxesArray.push(2);
      this.taxesArrayName.push("IGST 18%");
      this.taxesList.push({ name: "IGST 18%", taxAmount: 0 });
      this.getAmount(0);
    }

    if (e == 3) {
      this.taxesArray.push(3);
      this.taxesArrayName.push("SGST 9%");
      this.taxesList.push({ name: "SGST 9%", taxAmount: 0 });
      this.getAmount(0);
    }

    console.log("this.taxesArray" + this.taxesArray);


  }

  ////////////////////////delete taxes/////////////////////
  deleteTaxes(i) {

    this.netValue = this.netValue - this.taxesList[i].taxAmount;
    this.taxesList[i].taxAmount = 0;

    if (this.taxesList[i].name == 'CGST 9%') {
      this.salesQuotationDocumentCalculationObject.cgst = this.taxesList[i].taxAmount;
    }
    else if (this.taxesList[i].name == 'IGST 18%') {
      this.salesQuotationDocumentCalculationObject.igst = this.taxesList[i].taxAmount;
    }
    else if (this.taxesList[i].name == 'SGST 9%') {
      this.salesQuotationDocumentCalculationObject.sgst = this.taxesList[i].taxAmount;
    }

    this.taxesArray.splice(i, 1);
    this.taxesList.splice(i, 1);
    this.taxesArrayName.splice(i, 1);
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.toWords();

    console.log("this.taxeslist" + this.taxesList);
    return true;
  }


  //////////////////delete discount on subtotal//////////////////
  deleteDiscOnSubtotal() {

    this.discountOnSubtotal = 0;
    this.total = this.subTotal;
    this.netValue = this.total;
    this.netValue = parseFloat(this.netValue).toFixed(2);
    this.toWords();
    this.disc = 0;
    this.discountOnSubTotal();

  }


  ////////////////code for print/////////////////////

  printPage() {
    window.print();
  }

  printTable() {
    var tab = document.getElementById('tab');


    var style = "<style>";
    //   style = style + "table {width: 200px;font: 14px Calibri;}";
    //   style = style + "table, th, td {border: solid 5px #DDD; border-collapse: collapse; width: 200px;";
    //   style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "input[type=text] {width: 60%; boredr:0px}";
    style = style + "</style>";

    // var win = window.open('', '', 'height=700,width=700');
    // win.document.write(style);          //  add the style.
    // win.document.write(tab.outerHTML);
    // win.document.write(tab.innerHTML);
    // win.document.close();

    window.document.write(style);
    window.document.write(tab.innerHTML);

    window.print();
  }
  printDiv(divName) {

    //    $('form#myId input').bind("change", function() {
    //      var val = $(this).val();
    //      $(this).attr('value',val);
    //  });

    var style = "<style>";
    style = style + "table {width: 200px;font: 14px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse; width: 200px;";
    style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "input[type=text] {width: 60%; border: 1px solid white}";
    style = style + "</style>";

    var printContents = document.getElementById(divName).innerHTML;
    var printContents1 = document.getElementById(divName).innerHTML;

    var popupWin = window.open('', '_blank', 'width=300,height=300');
    popupWin.document.open();
    // popupWin.document.write('<html><head><link rel="stylesheet" href="sample.css" /></head><body onload="window.print()">' + printContents + '</body></html>');

    popupWin.document.write('<html><head>' + style + '</head><body onload="window.print()">' + printContents + '</body></html>');

    popupWin.document.close();
  };

}

