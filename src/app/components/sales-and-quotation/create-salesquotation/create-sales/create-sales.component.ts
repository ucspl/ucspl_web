import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FileUploadService } from '../../../../services/serviceconnection/create-service/file-upload.service';
import { Customer, CreateCustomerService, CustomerAttachments } from '../../../../services/serviceconnection/create-service/create-customer.service';
import { CreateSalesAndQuotation, termsForSalesAndQuotation, EnquiryAttachment, JsonForCsvFile, QuotationItemFile, CreateSalesService } from '../../../../services/serviceconnection/create-service/create-sales.service';
import { ReferenceForSalesQuotation, TypeOfQuotationForSalesAndQuotation, Terms } from '../../../../services/serviceconnection/create-service/create-def.service';
import { CreateUserService, User } from '../../../../services/serviceconnection/create-service/create-user.service';
import { Branch } from '../../../../services/serviceconnection/create-service/create-def.service';
import { StudentService } from '../../../../services/student/student.service';
import { optionalValidator, ValidationService } from '../../../../services/config/config.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-create-sales',
  templateUrl: './create-sales.component.html',
  styleUrls: ['./create-sales.component.css']
})
export class CreateSalesComponent implements OnInit {
  createSalesQuotationForm: FormGroup
  dateString = '';
  format = 'dd/MM/yyyy';


  // customersName: Array<any> = [];
  // static customerNames: Array<any> = [];
  // customersIds: Array<any> = [];
  //Customer: User = new User();
  customer: Customer = new Customer();
  customer1: Customer = new Customer();
  // static customerNameLength: number;

  branch: Branch = new Branch();
  branch1: Branch = new Branch();
  branchesName: Array<any> = [];
  branchesIds: Array<any> = [];

  referenceList: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  referenceName: Array<any> = [];
  referenceId: Array<any> = [];

  documentNumberForQuotation: termsForSalesAndQuotation = new termsForSalesAndQuotation();
  customerAndTermId: termsForSalesAndQuotation = new termsForSalesAndQuotation();
  removeCustomerAndTermId: termsForSalesAndQuotation = new termsForSalesAndQuotation();

  createSalesAndQuotation: CreateSalesAndQuotation = new CreateSalesAndQuotation();

  phoneNumberCountryNm1: any;
  trialPhoneNumber1: any;


  newSel: HTMLElement;
  static otionCount: number = 0;
  arrayForTerms: Array<any> = [];

  // Variable to store shortLink from api response 
  shortLink: string = "";
  loading: boolean = false; // Flag variable 
  file: File = null; // Variable to store file 
  filePath: File = null;

  selectedEnuiryAttachmentFile: File;
  selectedQuotationItemFile: File;
  selectedFileForQuotationTrial: any;
  selectedFileForQuotationItem: any;
  trialFileReader: any;
  retrievedImage: any;
  base64Data: any;
  base64DataFile: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  enquiryAttachmentData: EnquiryAttachment = new EnquiryAttachment();
  enquiryAttachmentData1: Object;
  quotationItemFileData: QuotationItemFile = new QuotationItemFile();
  printTermsId: any;

  public dateValue = Date.now();
  dateValue2: any;
  dateValue3 = new Date();
  public dateValue1 = Date.now();
  public dateValue4 = new Date();

  alive = true;
  jsonArray1 = new Array();
  jsonArray3: any[];
  jsonArray4 = [];

  object: JsonForCsvFile[] = [];
  object2: Object[];

  finalDocumentNumber: string;
  prefixForQuotation: any;
  dateOfDocumentNo: any;

  counterForDocumentNoString: string;
  counterForDocumentNo: any;
  quotationTypeData: TypeOfQuotationForSalesAndQuotation = new TypeOfQuotationForSalesAndQuotation();
  docTypeName: Array<any> = [];
  docTypeId: Array<any> = [];
  termsData: Terms = new Terms();
  termsName: Array<any> = [];
  termsIds: Array<any> = [];
  termsId: any;
  loginUserName: any;
  trialPhoneNumber: any;
  contactNo: string;
  termsWholeData: any;
  termsIdArray: Array<any> = [];
  contactNo1: any;

  selectedFile: File;
  selectedFileForCustAttachment: File;
  selectedFileForCustAttachmentTrial: any;
  selectedFileForQuotationQuo: any;

  custAttachObj: CustomerAttachments = new CustomerAttachments();
  custAttachUpload: CustomerAttachments = new CustomerAttachments();
  custAttach: any;
  termbox1: any;
  termbox2: any;
  trialDropdownList = [];
  paymentTermsName: Array<any> = [];
  paymentTermsIds: Array<any> = [];

  trialCustomerWithAddress: Array<{ customer_name: string, name: string, address: string }> = [];
  customers: Array<any> = [];
  static customerNames: Array<any> = [];
  customersIds: Array<any> = [];
  static customerNameLength: number;
  static customerDepartments: Array<any> = [];

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient, private router: Router, public datepipe: DatePipe, private studentService: StudentService, private fileUploadService: FileUploadService, private createUserService: CreateUserService, private createSalesService: CreateSalesService, private createCustomerService: CreateCustomerService) {

    this.createSalesQuotationForm = this.formBuilder.group({
      typeOfQuotation: [],
      dateOfQuotation: [],
      documentNumber: [],
      customerName: [],
      branchName: [],
      kindAttachment: [],
      custRefNo: [],
      refDate: [],
      archieveDate: [],
      contactNo: ['', [optionalValidator([Validators.maxLength(10), ValidationService.phoneValidator])]],
      email: ['', [optionalValidator([Validators.required, ValidationService.emailValidator])]],
      quotationItemFile: [],
      terms: [],
      enquiryAttachment: [],
      termsAndCondition: [],

    });

  }


  ngOnInit() {

    //add days in archieve date
    this.dateValue3 = new Date();
    this.dateValue3.setDate(this.dateValue3.getDate() + 7);

    //this.dateValue4=new Date();
    this.loginUserName = JSON.parse(localStorage.getItem('loginUserData'));

    //get customer list
    this.createCustomerService.getCustomerList().subscribe(data => {
      this.customer1 = data;
      console.log(this.customer1);


      CreateSalesComponent.customerNameLength = Object.keys(this.customer1).length;
      for (var i = 0, l = Object.keys(this.customer1).length; i < l; i++) {
        this.customers.push(this.customer1[i].name + " / " + this.customer1[i].department);
        console.log("customers : " + this.customers);
        CreateSalesComponent.customerNames.push(this.customer1[i].name);
        CreateSalesComponent.customerDepartments.push(this.customer1[i].department);

        this.trialCustomerWithAddress.push({ customer_name: this.customer1[i].name, name: this.customer1[i].name + " / " + this.customer1[i].department, address: this.customer1[i].addressLine1 + "," + this.customer1[i].addressLine2 + "," + this.customer1[i].city + "," + this.customer1[i].pin + "," + this.customer1[i].state + "," + this.customer1[i].country });
        this.customersIds.push(this.customer1[i].id);

        console.log("customer id : " + this.customersIds);
      }

    },
      error => console.log(error));

    //get branch list
    this.createUserService.getBranchList().subscribe(data => {
      this.branch1 = data;
      console.log(this.branch1);
      for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
        this.branchesName.push(this.branch1[i].branchName);
        this.branchesIds.push(this.branch1[i].branchId);
      }
    },
      error => console.log(error));

    //get reference list
    this.createSalesService.getReferenceList().subscribe(data => {
      this.referenceList = data;
      console.log(this.referenceList);
      for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
        this.referenceName.push(this.referenceList[i].referenceName);
        this.referenceId.push(this.referenceList[i].referenceId);
      }
    },
      error => console.log(error));

    //get quotation type list
    this.createSalesService.getQuotationTypeList().subscribe(data => {
      this.quotationTypeData = data;
      console.log(this.quotationTypeData);

      for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
        this.docTypeName.push(this.quotationTypeData[i].docTypeName);
        this.docTypeId.push(this.quotationTypeData[i].docTypeId);
      }
    },
      error => console.log(error));

    //get terms list
    this.createSalesService.getTermsList().subscribe(data => {
      this.termsWholeData = data;
      console.log(this.termsWholeData);

      for (var i = 0, l = Object.keys(this.termsWholeData).length; i < l; i++) {
        this.termsName.push(this.termsWholeData[i].termsName);
        this.termsIds.push(this.termsWholeData[i].termsId);
      }
    },
      error => console.log(error));


    // mobileNumber code

    var input1 = document.querySelector("#phoneNo");
    var country1 = $('#country1');
    var iti1 = (<any>window).intlTelInput(input1, {
      // any initialisation options go here
      "preferredCountries": ["in"],
      "separateDialCode": true
    });
    iti1.setNumber("+917733123456");
    var number1 = iti1.getNumber();
    var countryData1 = iti1.getSelectedCountryData();


    this.trialPhoneNumber1 = 91;
    this.phoneNumberCountryNm1 = "in";
    input1.addEventListener('countrychange', (e) => {
      // change the hidden input value to the selected country code
      var a1 = country1.val(iti1.getSelectedCountryData().dialCode);

      this.trialPhoneNumber1 = iti1.getSelectedCountryData().dialCode;
      this.phoneNumberCountryNm1 = iti1.getSelectedCountryData().iso2;


      //  this.prefixCountryCode1=this.trialp1
      document.getElementById("country1").innerText = this.trialPhoneNumber1;

    });
  }

  public chooseEnquiryAttachmentFile(event) {

    this.selectedEnuiryAttachmentFile = event.target.files[0];

  }

  uploadEnquiryAttachment() {

    console.log(this.selectedEnuiryAttachmentFile);
    const uploadImageData = new FormData();
    const uploadFileData = new FormData();

    uploadImageData.append('imageFile', this.selectedEnuiryAttachmentFile, this.selectedEnuiryAttachmentFile.name);

    $("#enquiryFile").html("File uploaded successfully!!!");


    this.createSalesService.enquiryAttachmentFileUpload(uploadImageData).subscribe(data => {

      console.log(data);
      this.enquiryAttachmentData = data;
      this.createSalesAndQuotation.enquiryAttachment = this.enquiryAttachmentData.id
    });

  }

  public chooseQuotationItemFile(event) {

    this.selectedQuotationItemFile = event.target.files[0];
    this.selectedFileForQuotationItem = $("#quotationItemFile")[0].files[0]
    // this.selectedFileForQuotationTrial = event.target;
    console.log(" this.selectedFileForQuotation " + this.selectedQuotationItemFile)


  }

  uploadQuotationItemFile() {

    console.log(this.selectedQuotationItemFile);
    const uploadImageData = new FormData();
    const uploadFileData = new FormData();



    debugger;
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;//regex for Checking valid files csv of txt 

    if (regex.test($("#quotationItemFile").val().toLowerCase())) {
      if (typeof (FileReader) != "undefined") {
        var reader = new FileReader();
        reader.onload = (e) => {

          this.trialFileReader = ((<FileReader>e.target).result as string).split("\r\n");
          console.log("...trial" + this.trialFileReader)
        }
        reader.readAsText(this.selectedFileForQuotationItem);
      }
    }


    uploadImageData.append('imageFile', this.selectedQuotationItemFile, this.selectedQuotationItemFile.name);
    $("#quotationFile").html("File uploaded successfully!!!");


  }


  onSubmit() {
    console.log(this.createSalesQuotationForm.value);
    this.createSalesService.documentNumberForSalesAndQuotation().subscribe(data => {
      //    console.log(data);
      this.documentNumberForQuotation.documentNumber = data.number;
      this.addOneToDocumentNumber();
      this.save();
    })

  }

  save() {

    this.createSalesAndQuotation = this.createSalesQuotationForm.value;
    this.createSalesAndQuotation.dateOfQuotation = this.createSalesAndQuotation.dateOfQuotation;
    this.createSalesAndQuotation.refDate = this.createSalesAndQuotation.refDate;
    this.createSalesAndQuotation.archieveDate = this.createSalesAndQuotation.archieveDate;
    this.createSalesAndQuotation.enquiryAttachment = this.enquiryAttachmentData.id
    this.createSalesAndQuotation.quotationItemFile = this.quotationItemFileData.id;
    this.createSalesAndQuotation.createdBy = this.loginUserName;
    this.createSalesAndQuotation.dateCreated = this.datepipe.transform(this.dateValue, 'yyyy-MM-dd');


    this.createSalesAndQuotation.documentNumber = this.finalDocumentNumber;
    if (typeof this.selectedFileForCustAttachment != 'undefined') {
      this.addCustAttach();
    }

    for (var i = 0, l = Object.keys(this.customers).length; i < l; i++) {
      if (this.customers[i] == this.createSalesQuotationForm.value.customerName) {
        this.createSalesAndQuotation.customerId = this.customer1[i].id;
        console.log(this.createSalesAndQuotation.customerId);
      }

    }

    for (var i = 0, l = Object.keys(this.branch1).length; i < l; i++) {
      if (this.branch1[i].branchName == this.createSalesQuotationForm.value.branchName) {
        this.createSalesAndQuotation.branchId = this.branch1[i].branchId;
        console.log(this.createSalesAndQuotation.branchId);
      }
    }


    for (var i = 0, l = Object.keys(this.quotationTypeData).length; i < l; i++) {
      if (this.quotationTypeData[i].docTypeName == this.createSalesQuotationForm.value.typeOfQuotation) {
        this.createSalesAndQuotation.typeOfQuotation = this.quotationTypeData[i].docTypeId;
        console.log(this.createSalesAndQuotation.typeOfQuotation);
      }
    }

    for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
      if (this.referenceList[i].referenceName == this.createSalesQuotationForm.value.custRefNo) {
        this.createSalesAndQuotation.custRefNo = this.referenceList[i].referenceId;
        break;

      }
    }



    this.createSalesService.setSalesQuotationDocument(this.createSalesAndQuotation);
    this.createSalesService.setQuotationFileDocument(this.trialFileReader);

    if (typeof this.contactNo1 != 'undefined') {
      this.createSalesAndQuotation.contactNo = "+" + this.trialPhoneNumber1 + this.contactNo1;
    }
    else {
      this.createSalesAndQuotation.contactNo = null;
    }

    this.createSalesAndQuotation.countryPrefixForMobile = this.phoneNumberCountryNm1;
    this.createSalesService.setCustomerCountryForMobileNo1(this.createSalesAndQuotation.countryPrefixForMobile);

    this.createSalesService.createSalesAndQuotation(this.createSalesAndQuotation).subscribe(data => {
      console.log(data)
      this.createSalesService.setSalesQuotationDocumentWithId(data);

      this.createSalesQuotationForm.reset();
    },
      error => console.log(error));

    this.clearForm();

  }

  clearForm() {

    this.router.navigateByUrl('/createsqdoc')

  }

  customerNameValid() {

    for (var i = 0; i < this.customers.length; i++) {
      if (this.customers[i] == this.createSalesAndQuotation.customerName) {
        $("#customerNameValidation").html(" ");
        break;
      }
      else {
        $("#customerNameValidation").html("customer name does not exist in list");

      }
    }
  }


  addOneToDocumentNumber() {

    this.prefixForQuotation = this.documentNumberForQuotation.documentNumber.slice(0, 10);
    this.dateOfDocumentNo = this.documentNumberForQuotation.documentNumber.slice(10, 16);

    this.counterForDocumentNoString = this.documentNumberForQuotation.documentNumber.slice(17, 22);
    this.counterForDocumentNo = parseInt(this.counterForDocumentNoString);
    this.dateValue2 = this.datepipe.transform(this.dateValue1, 'yyyyMM');
    if (this.dateOfDocumentNo != this.dateValue2) {
      this.dateOfDocumentNo = this.dateValue2
      this.counterForDocumentNo = 0;

    }

    this.counterForDocumentNo = this.counterForDocumentNo + 1;
    var output = [], n = 1, padded;
    padded = ('0000' + this.counterForDocumentNo).slice(-5);
    output.push(padded);

    var a = this.prefixForQuotation + this.dateOfDocumentNo + "_" + padded;
    this.finalDocumentNumber = a;
    this.createSalesAndQuotation.documentNumber = a;
  }



  /////////////////// customer attachments ///////////////////////////


  public onFileChangedForDocument(event) {

    this.selectedFileForCustAttachment = event.target.files[0];
    this.selectedFileForQuotationQuo = $("#docFile")[0].files[0]
    this.selectedFileForCustAttachmentTrial = event.target;
    console.log(" this.selectedFileForCustAttachment " + this.selectedFileForCustAttachment)

  }

  onUploadForDocument() {

    console.log(this.selectedFileForCustAttachment);
    const uploadFileData = new FormData();
    var trial_for_target = this.selectedFileForCustAttachmentTrial;
    $("#enquiryFile").html("File uploaded successfully!!!");
    debugger;

  }

  addCustAttach() {

    var originalName = this.selectedFileForCustAttachment.name;
    const uploadImageData = new FormData();
    var newFileName = this.finalDocumentNumber + "_" + this.selectedFileForCustAttachment.name;

    uploadImageData.append('imageFile', this.selectedFileForCustAttachment, newFileName);

    this.createSalesService.custAttachmentsFileUpload(uploadImageData).subscribe(data => {
      console.log(data);
      this.custAttachUpload = data;

      this.custAttachObj.documentNumber = this.finalDocumentNumber;
      this.custAttachObj.originalFileName = originalName;

      this.createSalesService.createCustomerAttachments(this.custAttachObj).subscribe(data => {
        this.custAttach = data;
        console.log(data);

      },
        error => console.log(error));
    });

  }

}
