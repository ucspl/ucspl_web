import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSalesQuotationDocumentComponent } from './update-sales-quotation-document.component';

describe('UpdateSalesQuotationDocumentComponent', () => {
  let component: UpdateSalesQuotationDocumentComponent;
  let fixture: ComponentFixture<UpdateSalesQuotationDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSalesQuotationDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSalesQuotationDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
