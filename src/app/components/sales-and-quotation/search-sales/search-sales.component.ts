import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateSalesService, DynamicGrid } from '../../../services/serviceconnection/create-service/create-sales.service';
import { Instrument, ReferenceForSalesQuotation } from '../../../services/serviceconnection/create-service/create-def.service';
import { SearchSalesQuotationByDocument, SearchSalesService } from '../../../services/serviceconnection/search-service/search-sales.service';
import { StudentService } from '../../../services/student/student.service';


@Component({
  selector: 'app-search-sales',
  templateUrl: './search-sales.component.html',
  styleUrls: ['./search-sales.component.css']
})
export class SearchSalesComponent implements OnInit {

  searchAndModifySalesQuotationForm: FormGroup;
  f1: FormGroup;
  dateString = '';
  format = 'dd/MM/yyyy';
  p: number = 1;
  itemsPerPage: number = 5;
  public dateValue1 = new Date();
  public dateValue2 = new Date();

  data: any;
  searchSalesQuotationData: SearchSalesQuotationByDocument = new SearchSalesQuotationByDocument();
  salesAndQuotationDataArray: Array<any> = [];

  docNo: string;
  statusChoice: any;
  status: string;
  idForSearchSales: string;
  salesObject: any;
  createSalesInfoForSearchSalesObject: any;
  documentNumberFromSearchSales: string;
  termsIdForSearchSalesArray: Array<number> = [];

  referenceList: ReferenceForSalesQuotation = new ReferenceForSalesQuotation();
  referenceName: Array<any> = [];
  referenceId: Array<any> = [];
  dateOfQuotation: Array<any> = [];
  salesQuotationDocumentVariableArrayForSearchSales: any;
  dynamicArray: Array<DynamicGrid> = [];
  dynamicArrayTrial: Array<DynamicGrid> = [];
  newDynamic: any = {};
  newDynamic1: any = [{}];
  instrumentList: Instrument = new Instrument();
  instrumentName: Array<any> = [];
  instrumentId: Array<any> = [];
  windowRef = null;
  windowRef1 = null;
  trial1: any;
  poLink: number;
  DocumentNumberForPo: number;
  indexForDocNo: number;
  customerNameForPo: any;

  constructor(private formBuilder: FormBuilder, public datepipe: DatePipe, private router: Router, private studentService: StudentService, private searchSalesService: SearchSalesService, private createSalesService: CreateSalesService) {
    this.searchAndModifySalesQuotationForm = this.formBuilder.group({
      searchDocument: [],
      itemsPerPage: [],
      toDate: [],
      fromDate: [],
      statusChoice: []
    });

    this.f1 = this.formBuilder.group({
      tName: [''],
    })
  }

  ngOnInit() {

    this.statusChoice = "selectStatus";

    this.createSalesService.getReferenceList().subscribe(data => {
      this.referenceList = data;
      console.log(this.referenceList);
      for (var i = 0, l = Object.keys(this.referenceList).length; i < l; i++) {
        this.referenceName.push(this.referenceList[i].referenceName);
        this.referenceId.push(this.referenceList[i].referenceId);
      }
    },
      error => console.log(error));

    this.createSalesService.getInstrumentList().subscribe(data => {
      this.instrumentList = data;
      console.log(this.instrumentList);
      for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
        this.instrumentName.push(this.instrumentList[i].instrumentName);
        this.instrumentId.push(this.instrumentList[i].instrumentId);
      }
    },
      error => console.log(error));
  }

  showData() {
    this.searchSalesService.showData().subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.salesAndQuotationDataArray = this.data;

      for (var i = 0; i < this.salesAndQuotationDataArray.length; i++) {

        if (this.salesAndQuotationDataArray[i][5] == 1) {
          this.status = "Draft";
          this.salesAndQuotationDataArray[i].push("Draft")
        }
        if (this.salesAndQuotationDataArray[i][6] == 1) {
          this.status = "Archieved";
          this.salesAndQuotationDataArray[i].push("Archieved")
        }
        if (this.salesAndQuotationDataArray[i][7] == 1) {
          this.status = "Submitted";
          this.salesAndQuotationDataArray[i].push("Submitted")
        }
        if (this.salesAndQuotationDataArray[i][8] == 1) {
          this.status = "Approved";
          this.salesAndQuotationDataArray[i].push("Approved")
        }
        if (this.salesAndQuotationDataArray[i][9] == 1) {
          this.status = "Rejected";
          this.salesAndQuotationDataArray[i].push("Rejected")
        }
      }

      for (var i = 0; i < this.salesAndQuotationDataArray.length; i++) {

        this.salesAndQuotationDataArray[i][3] = this.salesAndQuotationDataArray[i][3].slice(0, 10);
        this.salesAndQuotationDataArray[i][3] = this.datepipe.transform(this.salesAndQuotationDataArray[i][3], 'dd-MM-yyyy');

      }

      for (var i = 0; i < this.salesAndQuotationDataArray.length; i++) {
        this.salesAndQuotationDataArray[i][1] = this.salesAndQuotationDataArray[i][1] + " / " + this.salesAndQuotationDataArray[i][2];

      }


    })
  }
  searchData() {
    debugger;
    if (typeof this.searchAndModifySalesQuotationForm.value.toDate == "undefined" || this.searchAndModifySalesQuotationForm.value.toDate == null) {
      this.searchSalesQuotationData.searchDocument = this.searchAndModifySalesQuotationForm.value.searchDocument;
      if (this.searchSalesQuotationData.searchDocument != null && typeof this.searchSalesQuotationData.searchDocument != 'undefined') {
        this.searchSalesQuotationData.searchDocument = this.searchSalesQuotationData.searchDocument.trim();
      }
      switch (this.statusChoice) {
        case 'selectStatus': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Draft': this.searchSalesQuotationData.draft = 1;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Archieved': this.searchSalesQuotationData.draft = 0
          this.searchSalesQuotationData.archieved = 1;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Rejected': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 1;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Approved': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 1;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Submitted': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 1;
          break;

      }
    }
    else {
      this.searchAndModifySalesQuotationForm.value.toDate.setDate(this.searchAndModifySalesQuotationForm.value.toDate.getDate() + 1);
      this.searchSalesQuotationData.toDate = this.datepipe.transform(this.searchAndModifySalesQuotationForm.value.toDate, 'yyyy-MM-dd');
      this.searchSalesQuotationData.fromDate = this.datepipe.transform(this.searchAndModifySalesQuotationForm.value.fromDate, 'yyyy-MM-dd');
      this.searchSalesQuotationData.searchDocument = this.searchAndModifySalesQuotationForm.value.searchDocument;
      if (this.searchSalesQuotationData.searchDocument != null && typeof this.searchSalesQuotationData.searchDocument != 'undefined') {
        this.searchSalesQuotationData.searchDocument = this.searchSalesQuotationData.searchDocument.trim();
      }
      switch (this.statusChoice) {
        case 'selectStatus': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Draft': this.searchSalesQuotationData.draft = 1;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Archieved': this.searchSalesQuotationData.draft = 0
          this.searchSalesQuotationData.archieved = 1;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Rejected': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 1;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Approved': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 1;
          this.searchSalesQuotationData.submitted = 0;
          break;
        case 'Submitted': this.searchSalesQuotationData.draft = 0;
          this.searchSalesQuotationData.archieved = 0;
          this.searchSalesQuotationData.rejected = 0;
          this.searchSalesQuotationData.approved = 0;
          this.searchSalesQuotationData.submitted = 1;
          break;

      }
    }

    if (this.searchSalesQuotationData.approved == 1) {
      this.poLink = 1
    }

    if (this.searchSalesQuotationData == null) {
      alert("please enter criteria to search");
    }
    else {
      this.searchSalesService.searchData(this.searchSalesQuotationData).subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
      })
      this.showData();
    }

  }

  moveToSQDocUpdateScreen(i) {

    console.log("index is : " + i.target.innerHTML);
    this.docNo = i.target.innerText;
    this.searchSalesService.setDocNoForsalesQuotDocScreen(this.docNo);
    this.idForSearchSales = this.docNo;

    this.searchSalesService.sendDocNoForGettingCreateSalesDetails(this.idForSearchSales).subscribe(data => {

      console.log(data);

      this.searchSalesService.getCreateSalesDetailsForSearchSales().subscribe(data => {

        this.createSalesInfoForSearchSalesObject = data;

        this.searchSalesService.setCustomerCountryForMobileNo1(this.createSalesInfoForSearchSalesObject[0][20]);

        this.createSalesService.sendDocNoForGettingTermsId(this.idForSearchSales).subscribe(data => {
          console.log("*********terms id for doc no********" + data);

          this.createSalesService.getTermsIdForSearchSales().subscribe(data => {
            console.log("*********Terms id collection********" + data);
            this.termsIdForSearchSalesArray = data;


            localStorage.setItem('termsIdForSearchSalesArray', JSON.stringify(this.termsIdForSearchSalesArray));
            this.getSrNoForSearchSales()

          })

        })
      })
    })
  }

  getSrNoForSearchSales() {

    this.newDynamic1.splice(0, this.newDynamic1.length - 1);
    this.createSalesService.sendDocNoOfSalesQuotationDocument(this.docNo).subscribe(data => {
      console.log("*********sr no for doc no********" + data);


      this.createSalesService.getSrNoForSalesQuotationDocument().subscribe(data => {
        console.log("*********sr nos collection********" + data);
        this.salesQuotationDocumentVariableArrayForSearchSales = data;


        if (this.salesQuotationDocumentVariableArrayForSearchSales.length != 0) {

          for (var z = 0; z < this.salesQuotationDocumentVariableArrayForSearchSales.length; z++) {

            this.newDynamic1.push({
              uniqueId: this.salesQuotationDocumentVariableArrayForSearchSales[z][0],
              srNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][2],
              instruName: this.salesQuotationDocumentVariableArrayForSearchSales[z][3],
              description: this.salesQuotationDocumentVariableArrayForSearchSales[z][4],
              idNo: this.salesQuotationDocumentVariableArrayForSearchSales[z][5],
              accrediation: this.salesQuotationDocumentVariableArrayForSearchSales[z][6],
              range: this.salesQuotationDocumentVariableArrayForSearchSales[z][7],
              sacCode: this.salesQuotationDocumentVariableArrayForSearchSales[z][8],
              quantity: this.salesQuotationDocumentVariableArrayForSearchSales[z][9],
              rate: this.salesQuotationDocumentVariableArrayForSearchSales[z][10],
              discountOnItem: this.salesQuotationDocumentVariableArrayForSearchSales[z][11],
              amount: this.salesQuotationDocumentVariableArrayForSearchSales[z][13]

            })

          }

          this.dynamicArray.splice(0, this.dynamicArray.length);

          for (var l = 1; l < this.newDynamic1.length; l++) {
            this.convertInsruIdtoName(l);
            this.dynamicArray.push(this.newDynamic1[l]);
          }
        }

        localStorage.setItem('dynamicArrayForSearchSales', JSON.stringify(this.dynamicArray));

        this.openChildWindow1();
      })

    })
  }



  convertInsruIdtoName(n) {
    for (var i = 0, l = Object.keys(this.instrumentList).length; i < l; i++) {
      if (this.instrumentList[i].instrumentId == this.newDynamic1[n].instruName) {
        this.newDynamic1[n].instruName = this.instrumentList[i].instrumentName;

      }
    }
  }

  docNoForPo(e) {

    e = ((this.itemsPerPage * (this.p - 1)) + (e));
    console.log("index will be...." + e);

    this.indexForDocNo = parseInt(e);
    this.DocumentNumberForPo = (this.salesAndQuotationDataArray[this.indexForDocNo][0]);
    this.customerNameForPo = (this.salesAndQuotationDataArray[this.indexForDocNo][1]);

    this.openChildWindow();
  }

  openChildWindow() {
    localStorage.setItem('customerNameForPo', JSON.stringify(this.customerNameForPo));
    localStorage.setItem('DocumentNumberForPo', JSON.stringify(this.DocumentNumberForPo));
    this.windowRef = window.open("http://localhost:4200/crtPo", "child", "width=900,height=420,top=100");
    this.windowRef.focus();
    this.windowRef.addEventListener("message", this.receiveMessage.bind(this), false);

  }
  receiveMessage(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }

  openChildWindow1() {
    localStorage.setItem('DocumentNumberForUpdateSales', JSON.stringify(this.docNo));
    this.windowRef1 = window.open("http://localhost:4200/updatesalesdoc", "child", "width=900,height=420,top=100");
    this.windowRef1.addEventListener("message", this.receiveMessage1.bind(this), false);

  }

  DocumentNumberForUpdateSales12(DocumentNumberForUpdateSales: any): string {
    throw new Error('Method not implemented.');
  }

  receiveMessage1(evt: any) {
    console.log(evt.data);
    this.trial1 = evt.data;
  }

}