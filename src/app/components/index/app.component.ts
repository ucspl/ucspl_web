/**
 * Created By : PoojaG MechLean 
 */

import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent {
	title = 'Student Management By PoojaG MechLean';

	// Add few students for initial listing
	studentsList = [
	{	
		id : 1,
		first_name : "PoojaG",
		last_name : "MechLean",
		email : "PoojaG@mechlean.co.in",
		phone : 9503733178,
		department : "Science"
	},
	{
		id : 2,
		first_name : "Yogesh",
		last_name : "Mechlean",
		email : "yogesh@mechlean.co.in",
		phone : 8574889658,
		department : "Commerce"
	},
	{
		id : 3,
		first_name : "Neha",
		last_name : "Mechlean",
		email : "neha@mechlean.co.in",
		phone : 7485889658,
		department : "Science"
	},
	{
		id : 4,
		first_name : "Shrutika",
		last_name : "Mechlean",
		email : "john@mechlean.co.in",
		phone : 9685589748,
		department : "Arts"
	},
	{
		id : 5,
		first_name : "Priyanka",
		last_name : "Mechlean",
		email : "priyanka@mechlean.co.in",
		phone : 8595856547,
		department : "Engineering"
	}
	];

	constructor() {
		// Save students to localStorage
		localStorage.setItem('students', JSON.stringify(this.studentsList));
	}
}

/**
 * Created By : PoojaG MechLean 
 */
