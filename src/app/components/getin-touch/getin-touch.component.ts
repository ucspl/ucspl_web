import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-getin-touch',
  templateUrl: './getin-touch.component.html',
  styleUrls: ['./getin-touch.component.css']
})

export class GetinTouchComponent implements OnInit {

  title = 'read-qrcodes-angular7';  
  elementType = 'url';  
  public imagePath;  
  value : any;  
  @ViewChild('result') resultElement: ElementRef;  
  showQRCode: boolean = false;  
 
  
  constructor(private renderer: Renderer2) { }

  ngOnInit() {

    $("#vertical-menu h3").click(function () {
      //Inner 
      var jqInner = $(this).next();
      if (jqInner.is(":visible"))
      {
          jqInner.slideUp()
          $(this).find('span').html('-');
      }
      else
      {
          jqInner.slideDown()
          $(this).find('span').html('+');
      }
  })
  }


  preview(files) {  
    if (files.length === 0)  
      return;  
    var mimeType = files[0].type;  
    if (mimeType.match(/image\/*/) == null) {  
      alert("Only images are supported.");  
      return;  
    }  
    var reader = new FileReader();  
    reader.readAsDataURL(files[0]);  
    reader.onload = (_event) => {  
      this.value = reader.result;  
      console.log(reader.result);  
      this.showQRCode = true;  
    }  
  }  
  render(e) {  
    let element: Element = this.renderer.createElement('h1');  
    element.innerHTML = e.result;  
    this.renderElement(element);  
  }  
  
  renderElement(element) {  
    for (let node of this.resultElement.nativeElement.childNodes) {  
      this.renderer.removeChild(this.resultElement.nativeElement, node);  
    }  
    this.renderer.appendChild(this.resultElement.nativeElement, element);  
  }  
}
