import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePtmRecptComponent } from './create-ptm-recpt.component';

describe('CreatePtmRecptComponent', () => {
  let component: CreatePtmRecptComponent;
  let fixture: ComponentFixture<CreatePtmRecptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePtmRecptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePtmRecptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
