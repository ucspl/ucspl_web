import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPtmRecptComponent } from './search-ptm-recpt.component';

describe('SearchPtmRecptComponent', () => {
  let component: SearchPtmRecptComponent;
  let fixture: ComponentFixture<SearchPtmRecptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPtmRecptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPtmRecptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
