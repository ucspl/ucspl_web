import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesRepComponent } from './taxes-rep.component';

describe('TaxesRepComponent', () => {
  let component: TaxesRepComponent;
  let fixture: ComponentFixture<TaxesRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxesRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxesRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
