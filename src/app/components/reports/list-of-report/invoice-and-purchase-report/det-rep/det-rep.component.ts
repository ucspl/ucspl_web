
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../../services/student/student.service';

@Component({
  selector: 'app-det-rep',
  templateUrl: './det-rep.component.html',
  styleUrls: ['./det-rep.component.css']
})
export class  DetRepComponent implements OnInit {


  InvoiceDetailReport:FormGroup
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();
  

  constructor(private formBuilder: FormBuilder,private router: Router,private studentService:StudentService) {
    this.InvoiceDetailReport = this.formBuilder.group({
      branch:[],
      invoicetype:[],
      companyprefix:[],
      invoicenumber:[],
      customername:[],
      customer1:[],
      itemtype:[],
      status:[],
      Date1:[],
      Date2:[],
      reportformat:[],
      sortby:[]
      
    });

  //  this.dateString = FormData(this.dateValue, 'dd-MM-yyyy', 'en-AU', '+1100');
  }
 

  ngOnInit() {
  }
}





  


  