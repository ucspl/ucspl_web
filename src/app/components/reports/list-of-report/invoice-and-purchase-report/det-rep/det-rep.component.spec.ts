import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetRepComponent } from './det-rep.component';

describe('DetRepComponent', () => {
  let component: DetRepComponent;
  let fixture: ComponentFixture<DetRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
