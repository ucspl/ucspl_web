import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmryRepComponent } from './smry-rep.component';

describe('SmryRepComponent', () => {
  let component: SmryRepComponent;
  let fixture: ComponentFixture<SmryRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmryRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmryRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
