import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../../services/student/student.service';


@Component({
  selector: 'app-smry-rep',
  templateUrl: './smry-rep.component.html',
  styleUrls: ['./smry-rep.component.css']
})
export class SmryRepComponent implements OnInit {



  InvoiceSummaryReport: FormGroup
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();


  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {
    this.InvoiceSummaryReport = this.formBuilder.group({
      branch: [],
      invoicetype: [],
      companyprefix: [],
      invoicenumber: [],
      customername: [],
      customer1: [],
      RecoveryStatus: [],
      status: [],
      Date1: [],
      Date2: [],
      reportformat: [],
      sortby: [],
      PaymentDate1: [],
      PaymentDate2: [],
      Paymentreceived: [],
      PaymentReceiptNo: []


    });

    //  this.dateString = FormData(this.dateValue, 'dd-MM-yyyy', 'en-AU', '+1100');
  }


  ngOnInit() {
  }
}