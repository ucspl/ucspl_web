import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotSmryRepComponent } from './quot-smry-rep.component';

describe('QuotSmryRepComponent', () => {
  let component: QuotSmryRepComponent;
  let fixture: ComponentFixture<QuotSmryRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotSmryRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotSmryRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
