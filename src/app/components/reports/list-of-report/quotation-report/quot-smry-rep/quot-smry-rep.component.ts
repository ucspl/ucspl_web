import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidationService } from '../../../../../services/config/config.service';
import { StudentService } from '../../../../../services/student/student.service';


@Component({
  selector: 'app-quot-smry-rep',
  templateUrl: './quot-smry-rep.component.html',
  styleUrls: ['./quot-smry-rep.component.css']
})
export class QuotSmryRepComponent implements OnInit {

  SalesQuotationSummaryReport: FormGroup
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();


  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {
    this.SalesQuotationSummaryReport = this.formBuilder.group({
      branch: [],
      companyprefix: [],
      docnumber: [],
      customer: [],
      Kindatt: [],
      status: [],
      email: [],
      phoneno: [],
      Date1: [],
      Date2: [],
      reportformat: [],
      sortby: [],
      DocumentDate1: [],
      DocumentDate2: [],
      custrefno: [],

    });

    //  this.dateString = FormData(this.dateValue, 'dd-MM-yyyy', 'en-AU', '+1100');
  }


  ngOnInit() {
  }

}
