
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../../../../../services/student/student.service';

@Component({
  selector: 'app-quot-det-rep',
  templateUrl: './quot-det-rep.component.html',
  styleUrls: ['./quot-det-rep.component.css']
})
export class QuotDetRepComponent implements OnInit {
  CustomerSalesQuotationDetailReport: FormGroup
  dateString = '';
  format = 'dd/MM/yyyy';

  public dateValue = new Date();

  constructor(private formBuilder: FormBuilder, private router: Router, private studentService: StudentService) {

    this.CustomerSalesQuotationDetailReport = this.formBuilder.group({

      quotationtype: [],
      companyprefix: [],
      quotationnumber: [],
      customerrefnumber: [],
      customername: [],
      customer1: [],
      instrumentname: [],
      status: [],
      Date1: [],
      Date2: [],
      reportformat: [],
      sortby: [],
      Date3: [],
      Date4: []

    });
  }

  ngOnInit() {
  }

}
