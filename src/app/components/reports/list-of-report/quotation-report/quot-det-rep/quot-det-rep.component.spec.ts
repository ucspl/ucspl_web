import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotDetRepComponent } from './quot-det-rep.component';

describe('QuotDetRepComponent', () => {
  let component: QuotDetRepComponent;
  let fixture: ComponentFixture<QuotDetRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotDetRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotDetRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
