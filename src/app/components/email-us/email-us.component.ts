import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-email-us',
  templateUrl: './email-us.component.html',
  styleUrls: ['./email-us.component.css']
})
export class EmailUsComponent implements OnInit {
  rmCheck:any;
  emailInput:any;
  passInput: any;
  
  constructor() { }

  ngOnInit() {

  }

  call(){
 this.rmCheck = document.getElementById("rememberMe");
this.emailInput = document.getElementById("email");
this.passInput = document.getElementById("pass");
if (localStorage.checkbox && localStorage.checkbox !== "") {
  this.rmCheck .setAttribute("checked", "checked");
this.emailInput.value = localStorage.username;
this.passInput.value = localStorage.passname;
} else {
this.rmCheck .removeAttribute("checked");
this.emailInput.value = "";
}
}
   

 lsRememberMe() {
if (this.rmCheck.checked && this.emailInput.value !== "") {
  localStorage.username = this.emailInput.value;
  localStorage.passname = this.passInput.value;
  localStorage.checkbox = this.rmCheck.value;
} else {
  localStorage.username = "";
  localStorage.checkbox = "";
  localStorage.passname = "";
}
}
}
