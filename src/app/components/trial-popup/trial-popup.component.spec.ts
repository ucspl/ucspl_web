import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialPopupComponent } from './trial-popup.component';

describe('TrialPopupComponent', () => {
  let component: TrialPopupComponent;
  let fixture: ComponentFixture<TrialPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrialPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
