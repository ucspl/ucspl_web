import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicGrid } from '../../services/serviceconnection/create-service/create-user.service';

declare var $:any;
@Component({
  selector: 'app-call-us',
  templateUrl: './call-us.component.html',
  styleUrls: ['./call-us.component.css']
})
export class CallUsComponent implements OnInit {

  addForm: FormGroup;

  rows: FormArray;
  itemForm: FormGroup;

  constructor() { }  

  dynamicArray: Array<DynamicGrid> = [];  
  newDynamic: any = {}; 

  ngOnInit(): void {  
      this.newDynamic = {name: "", email: "",phone:""};  
      this.dynamicArray.push(this.newDynamic);  
  }  

  addRow() {    
    this.newDynamic = {name: "", email: "",phone:""};  
      this.dynamicArray.push(this.newDynamic);  
      console.log(this.dynamicArray);  
      return true;  
  }  

  deleteRow(index) {  
      if(this.dynamicArray.length ==1) {  
      
          return false;  
      } else {  
          this.dynamicArray.splice(index, 1);  
     
          return true;  
      }  
  }

/*addForm: FormGroup;

  rows: FormArray;
  itemForm: FormGroup;

  constructor(private fb: FormBuilder) {

    this.addForm = this.fb.group({
      items: [null, Validators.required],
      items_value: ['no', Validators.required]
    });

    this.rows = this.fb.array([]);

  }

  ngOnInit() {
    this.addForm.get("items").valueChanges.subscribe(val => {
      if (val === true) {
        this.addForm.get("items_value").setValue("yes");

        this.addForm.addControl('rows', this.rows);
      }
      if (val === false) {
        this.addForm.get("items_value").setValue("no");
        const control = <FormArray>this.rows
    for (let i = control.length - 1; i >= 0; i--) {
      control.removeAt(i);
    }
        this.addForm.removeControl('rows');
      }
    });
  }

  onAddRow() {
    this.rows.push(this.createItemFormGroup());
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      name: null,
      description: null,
      qty: null
    });
  }

  onSave() {
    console.log(this.rows.value)
  }

} */
}