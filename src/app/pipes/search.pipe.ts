import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  // transform(items: any[], field: string, value: string): any[] {
  //   if (!items) {
  //     return [];
  //   }
  //   if (!field || !value) {
  //     return items;
  //   }

  //   return items.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
  // }

  
transform(items: any[], searchText: string): any[] {
  if(!items) return [];
  if(!searchText) return items;

  searchText = searchText.toLowerCase();
  return items.filter( it => {
    console.log('abc', it.value.idNo);

  //  return it.value.idNo.toString().toLowerCase().includes(searchText)
   
    return (it.value.idNo.toString().toLowerCase().includes(searchText) || it.value.srNo.toString().toLowerCase().includes(searchText) );
  });
}
}

