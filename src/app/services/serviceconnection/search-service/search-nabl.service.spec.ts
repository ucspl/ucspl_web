import { TestBed, inject } from '@angular/core/testing';

import { SearchNablService } from './search-nabl.service';

describe('SearchNablService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchNablService]
    });
  });

  it('should be created', inject([SearchNablService], (service: SearchNablService) => {
    expect(service).toBeTruthy();
  }));
});
