
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

@Injectable()
export class SearchPo{

   searchDocument:String;
   draft:number;
   archieved:number;
   submitted:number;
   rejected:number;
   approved:number;
   toDate:any;
   fromDate:any;
}

@Injectable()
export class SearchPoService  {

  documentNumber:string;
  custCountryNmMobileNo1:  string;
  selectedDropdown: Array<any> = [];

 // private searchandmodifysalesandquotation = 'http://localhost:8090/api/v1/search_and_modify_sales_and_quotation';
  
  constructor(private http: HttpClient) { }

  SearchAndModifyPO(purchaseOrder: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_PO_URL}`, purchaseOrder);
  }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_PO_URL}`,model);    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_PO_URL}`);    
  }  


  ////////////////////////////// send document no to sales quotation document //////////////////////

  getDocNoInPoDocScreen()
  {
     return this.documentNumber;
  }

  setDocNoForPODocScreen(docNo)
  {
    this.documentNumber=docNo;
  }

  
  sendDocNoForGettingCreatePODetails(salesQuotationDocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_PO_DETAILS_URL}`, salesQuotationDocument);
  }

  getCreatePODetailsForSearchPo(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_PO_DETAILS_URL}`);
  }
  


  

}

