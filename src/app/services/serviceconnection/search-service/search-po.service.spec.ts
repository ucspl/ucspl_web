import { TestBed, inject } from '@angular/core/testing';

import { SearchPoService } from './search-po.service';

describe('SearchPoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchPoService]
    });
  });

  it('should be created', inject([SearchPoService], (service: SearchPoService) => {
    expect(service).toBeTruthy();
  }));
});
