import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';


export class SearchMasterInstrument {                                             
                                                                          
  searchDocument: String;                                                                           
  draft: number;                                                                 
  archieved: number;                                                       
  submitted: number;                                                                     
  rejected: number;                                                                  
  approved: number;                                                                          
  toDate: any;                                                                                       
  fromDate: any;                                                                            
}

export class SearchEnvMasterInstrument {

  envMasterId: number;
  searchDocument: String;
  calibratedAt: String;
  draft: number;
  archieved: number;
  submitted: number;
  rejected: number;
  approved: number;
  toDate: any;
  fromDate: any;
}


@Injectable()
export class SearchCertificateService {

  number: string;
  custCountryNmMobileNo1: string;
  selectedDropdown: Array<any> = [];

  constructor(private http: HttpClient) { }

  searchAndModifyReqAndIwd(salesandquotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_MASTER_INSTRUMENT_URL}`, salesandquotation);
  }

  searchData(model: Object): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_MASTER_INSTRUMENT_URL}`, model);
  }

  showData(): Observable<any> {
    debugger;
    return this.http.get(`${SystemConstants.SEARCH_MASTER_INSTRUMENT_URL}`);
  }

  ////////////////////////////// send master name for master instrument document //////////////////////

  getDocNoInMasterInstruScreen() {
    return this.number;
  }

  setDocNoForMasterInstruDocScreen(no) {
    this.number = no;
  }


  /////////////////////////  Env master search screen  ////////////////////////////

  searchEnvMasterData(model: Object): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_ENV_MASTER_INSTRUMENT_URL}`, model);
  }

  showEnvMasterData(): Observable<any> {
    debugger;
    return this.http.get(`${SystemConstants.SEARCH_ENV_MASTER_INSTRUMENT_URL}`);
  }



  //////////////////////// Unc master screen ////////////////////////////////////
  searchUncMasterData(model: string): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_UNC_MASTER_INSTRUMENT_URL}`, model);
  }

  showUncMasterData(): Observable<any> {
    debugger;
    return this.http.get(`${SystemConstants.SEARCH_UNC_MASTER_INSTRUMENT_URL}`);
  }

  trialSetInvoiceTagToOneForInstruThroughInvId(value: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let body = '{}';

    let response = this.http.post(`${SystemConstants.SEARCH_UNC_MASTER_INSTRUMENT_URL}`, value, httpOptions)
      .pipe(
        tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );

    return response;
  }


  private handleError(error: any) {
    let errMsg = error.message || 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }



  //////////////////////// Uom Calculator master screen ////////////////////////////////////
  searchUomCalculatorData(model: string): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_UOM_CALCULATOR_URL}`, model);
  }


}
