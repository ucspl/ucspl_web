import { TestBed, inject } from '@angular/core/testing';

import { SearchSalesService } from './search-sales.service';

describe('SearchSalesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchSalesService]
    });
  });

  it('should be created', inject([SearchSalesService], (service: SearchSalesService) => {
    expect(service).toBeTruthy();
  }));
});
