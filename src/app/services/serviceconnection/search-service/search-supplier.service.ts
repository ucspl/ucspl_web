import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';


export class SupplierByNameOrAddress  {

  name: String;
  nameAnd: String;
  status: number;
  address: String;

}

@Injectable()
export class SearchSupplierService {

  idForSupplierName: any;
  supplierCountryNmPhoneNo:  string;
  supplierCountryNmMobileNo1:  string;
  supplierCountryNmMobileNo2:  string;

  //private baseUrl = 'http://localhost:8090/api/v1/customers';
  //private searchurl = 'http://localhost:8090/api/v1/search_and_modify_customer_by_name';
  //private showdataurl = 'http://localhost:8090/api/v1/customershowdata';
  constructor(private http: HttpClient) { }



  searchData(model: Object): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_SUPPLIER}`, model);
  }

  showData(): Observable<any> {
    debugger;
    return this.http.get(`${SystemConstants.SEARCH_SUPPLIER}`);
  }


  //////////// Search customer with id///////////////////

  getIdForSupplierName() {
    return this.idForSupplierName;
  }

  setIdForSupplierName(id) {
    this.idForSupplierName = id;
  }

  getSupplierCountryForPhoneNo(){
    return this.supplierCountryNmPhoneNo
  }

  setSupplierCountryForPhoneNo(c){
    this.supplierCountryNmPhoneNo=c;
  }


  getSupplierCountryForMobileNo1(){
    return this.supplierCountryNmMobileNo1
  }

  setSupplierCountryForMobileNo1(c){
    this.supplierCountryNmMobileNo1=c;
  }

  getSupplierCountryForMobileNo2(){
    return this.supplierCountryNmMobileNo2
  }

  setSupplierCountryForMobileNo2(c){
    this.supplierCountryNmMobileNo2=c;
  }


  /////////////////////////// Get customer details by id to update customer ////////////////////////

  getSupplierDetailsById(supplier: any): Observable<any> {
    return this.http.post(`${SystemConstants.SUPPLIER_DETAILS_FOR_ID}`, supplier);
  }

}
