import { TestBed, inject } from '@angular/core/testing';

import { SearchCertificateService } from './search-certificate.service';

describe('SearchCertificateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchCertificateService]
    });
  });

  it('should be created', inject([SearchCertificateService], (service: SearchCertificateService) => {
    expect(service).toBeTruthy();
  }));
});
