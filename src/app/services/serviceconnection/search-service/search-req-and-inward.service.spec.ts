import { TestBed, inject } from '@angular/core/testing';

import { SearchReqAndInwardService } from './search-req-and-inward.service';

describe('SearchReqAndInwardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchReqAndInwardService]
    });
  });

  it('should be created', inject([SearchReqAndInwardService], (service: SearchReqAndInwardService) => {
    expect(service).toBeTruthy();
  }));
});
