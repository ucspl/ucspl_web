import { TestBed, inject } from '@angular/core/testing';

import { SearchInvoiceService } from './search-invoice.service';

describe('SearchInvoiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchInvoiceService]
    });
  });

  it('should be created', inject([SearchInvoiceService], (service: SearchInvoiceService) => {
    expect(service).toBeTruthy();
  }));
});
