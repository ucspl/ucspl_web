import { TestBed, inject } from '@angular/core/testing';

import { SearchProformaInvoiceService } from './search-proforma-invoice.service';

describe('SearchProformaInvoiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchProformaInvoiceService]
    });
  });

  it('should be created', inject([SearchProformaInvoiceService], (service: SearchProformaInvoiceService) => {
    expect(service).toBeTruthy();
  }));
});
