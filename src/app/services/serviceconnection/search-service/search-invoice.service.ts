import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class SearchInvoiceDocByDocument{

  searchDocument:String;
  draft:number;
  archieved:number;
  submitted:number;
  rejected:number;
  approved:number;
  toDate:any;
  fromDate:any;
}


@Injectable()
export class SearchInvoiceService {

  document_number:string;

  constructor(private http: HttpClient) { }

  SearchAndModifySalesAndQuotation(invdoc: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_INVOICE_URL}`, invdoc);
  }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_INVOICE_URL}`,model);    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_INVOICE_URL}`);    
  }  


  ////////////////////////////// send document no to invoice document //////////////////////

  getDocNoFromSearchinvoice()
  {
     return this.document_number;
  }

  setDocNoToUpdateInvoiceDoc(doc_no)
  {
    this.document_number=doc_no;
  }

  ///////////////////////////// get info for search invoice from create-challan-cum-invoice-document-table /////////////////////////


  getCreateInvsDetailsForSearchInv(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_INVOICE_DETAILS_URL}`);
  }
  
  sendDocNoForGettingCreateInvDetails(invoicedocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_INVOICE_DETAILS_URL}`, invoicedocument);
  }




}