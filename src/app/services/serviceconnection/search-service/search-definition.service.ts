import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';


export class SearchDefinition{

  searchDefinitionValue:any;
  status:number;
  
}



@Injectable()
export class SearchDefinitionService {

  constructor(private http: HttpClient) { }
  searchData(searchDefinitionObject : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_DEFINITION_URL}`,searchDefinitionObject).pipe(
    retry(3)
  );    ;    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_DEFINITION_URL}`).pipe(
    retry(3)
  );    ;    
  }  

  sendNameForGettingCreateDefinitionDetails(nameForSearchDefinition: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_DEFINITION_DETAILS_URL}`, nameForSearchDefinition);
  }

  getCreateDefinitionDetailsForSearchDefinition(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_DEFINITION_DETAILS_URL}`);
  }
  
}
