
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class SearchProformaClass{

  searchDocument:String;
  draft:number;
  archieved:number;
  submitted:number;
  rejected:number;
  approved:number;
  toDate:any;
  fromDate:any;
}

@Injectable()
export class SearchProformaInvoiceService {

  document_number:string;

  constructor(private http: HttpClient) { }

  // SearchAndModifySalesAndQuotation(invdoc: Object): Observable<Object> {
  //   return this.http.post(`${SystemConstants.SEARCH_INVOICE_URL}`, invdoc);
  // }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_PROFORMA_INVOICE_URL}`,model);    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_PROFORMA_INVOICE_URL}`);    
  }  

  ////////////////////////////// send document no to invoice document //////////////////////

  getDocNoFromSearchProformaInvoice()
  {
     return this.document_number;
  }

  setDocNoToUpdateProformaInvoiceDoc(doc_no)
  {
    this.document_number=doc_no;
  }

  ///////////////////////////// get info for search invoice from create-challan-cum-invoice-document-table /////////////////////////

  getCreateProformaInvsDetailsForSearchProformaInv(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_PROFORMA_INVOICE_DETAILS_URL}`);
  }
  
  sendDocNoForGettingCreateProformaInvDetails(invoicedocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_PROFORMA_INVOICE_DETAILS_URL}`, invoicedocument);
  }

}


