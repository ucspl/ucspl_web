
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class SearchNablScopeMaster{

   searchNablScope:String;
  
}

@Injectable()
export class SearchNablService {  

  documentNumber:string;
  custCountryNmMobileNo1:  string;
  selectedDropdown: Array<any> = [];
  certificateNumber: any;

 // private searchandmodifysalesandquotation = 'http://localhost:8090/api/v1/search_and_modify_sales_and_quotation';
  
  constructor(private http: HttpClient) { }

  SearchAndModifySalesAndQuotation(salesandquotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_NABL_URL}`, salesandquotation);
  }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_NABL_URL}`,model);    
  }  

  showdata(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_NABL_URL}`);    
  }  


  ////////////////////////////// send document no to sales quotation document //////////////////////

  getCertificateNoForUpdateNablScreen()
  {
     return this.certificateNumber;
  }

  setCertificateNoForUpdateNablScreen(certiNo)
  {
    this.certificateNumber=certiNo;
  }


  sendCertificateNoForGettingCreateNablDetails( nablObject: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_NABL_DETAILS_URL}`, nablObject);
  }

  getCreateNablDetailsForSearchNabl(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_NABL_DETAILS_URL}`);
  }
  

}
