import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';

export class UserByName{

  name:String;
  status:number;
  department:String;

}


@Injectable()
export class SearchUserService {
  idForUserNumber:string;
  custCountryNmMobileNo1:  string;
  countryPrefixForMobile1:string;

  //private baseUrl = 'http://localhost:8090/api/v1/users';
  //private searchurl = 'http://localhost:8090/api/v1/search_and_modify_user_by_name';
  //private showdataurl = 'http://localhost:8090/api/v1/showdata';
  constructor(private http: HttpClient) { }


 

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_USER_BY_NAME_DEPT}`,model).pipe(
    retry(3)
  );    ;    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_USER_BY_NAME_DEPT}`).pipe(
    retry(3)
  );    ;    
  }  
////////////////////////////// send user number to create user //////////////////////

getUserNumberForCreateUser ()
{
   return this.idForUserNumber;
}

 setUserNumberForCreateUser(id)
{
  this.idForUserNumber=id;
}

  /////////////////////////// Get user details by id to update customer ////////////////////////

  sendUserNoForGettingCreateUserDetails(objectForUserNumber: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_USERS_DETAILS_URL}`, objectForUserNumber);
  }

  getCustomerCountryForMobileNo1(){
    return this.custCountryNmMobileNo1
  }

  setCustomerCountryForMobileNo1(c){
    this.custCountryNmMobileNo1=c;
  }

}
