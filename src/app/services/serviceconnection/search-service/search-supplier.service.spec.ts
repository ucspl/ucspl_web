import { TestBed, inject } from '@angular/core/testing';

import { SearchSupplierService } from './search-supplier.service';

describe('SearchSupplierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchSupplierService]
    });
  });

  it('should be created', inject([SearchSupplierService], (service: SearchSupplierService) => {
    expect(service).toBeTruthy();
  }));
});
