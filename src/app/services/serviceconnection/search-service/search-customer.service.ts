

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';
import { Customer } from '../create-service/create-customer.service';



export class CustomerbyName {

  name: String;
  nameAnd: String;
  status: number;
  department: String;
  address: String;

}


@Injectable()
export class SearchCustomerService {

  idForCustomerName: any;
  custCountryNmMobileNo1:  string;
  custCountryNmMobileNo2:  string;

  //private baseUrl = 'http://localhost:8090/api/v1/customers';
  //private searchurl = 'http://localhost:8090/api/v1/search_and_modify_customer_by_name';
  //private showdataurl = 'http://localhost:8090/api/v1/customershowdata';
  constructor(private http: HttpClient) { }



  searchData(model: Object): Observable<Object> {
    debugger;
    return this.http.post(`${SystemConstants.SEARCH_CUST}`, model);
  }

  showData(): Observable<any> {
    debugger;
    return this.http.get(`${SystemConstants.SEARCH_CUST}`);
  }


  //////////// Search customer with id///////////////////

  getIdForCustomerName() {
    return this.idForCustomerName;
  }

  setIdForCustomerName(id) {
    this.idForCustomerName = id;
  }

  getCustomerCountryForMobileNo1(){
    return this.custCountryNmMobileNo1
  }

  setCustomerCountryForMobileNo1(c){
    this.custCountryNmMobileNo1=c;
  }

  getCustomerCountryForMobileNo2(){
    return this.custCountryNmMobileNo2
  }

  setCustomerCountryForMobileNo2(c){
    this.custCountryNmMobileNo2=c;
  }


  /////////////////////////// Get customer details by id to update customer ////////////////////////

  getCustomerDetailsById(customer: any): Observable<any> {
    return this.http.post(`${SystemConstants.CUSTOMER_DETAILS_FOR_ID}`, customer);
  }


  getCustomerInCustomerAndTaxJunctionTable(model): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.GET_CUSTOMER_TAX_ID_URL}`,model);    
  } 

  
}
