import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class SearchReqAndIwdByDocument{

   searchDocument:String;
   draft:number;
   archieved:number;
   submitted:number;
   rejected:number;
   approved:number;
   toDate:any;
   fromDate:any;
}


@Injectable()
export class SearchReqAndInwardService {

  documentNumber:string;
  custCountryNmMobileNo1:  string;
  selectedDropdown: Array<any> = [];

 // private searchandmodifysalesandquotation = 'http://localhost:8090/api/v1/search_and_modify_sales_and_quotation';
  
  constructor(private http: HttpClient) { }

  searchAndModifyReqAndIwd(reqInward: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_REQUEST_AND_INWARD_URL}`, reqInward);
  }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_REQUEST_AND_INWARD_URL}`,model);    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_REQUEST_AND_INWARD_URL}`);    
  }  


  ////////////////////////////// send document no to request and inward document //////////////////////

  getDocNoInSalesquotDocScreen()
  {
     return this.documentNumber;
  }

  setDocNoForReqAndIwdDocScreen(docNo)
  {
    this.documentNumber=docNo;
  }

  getCustomerCountryForMobileNo1(){
    return this.custCountryNmMobileNo1
  }

  setCustomerCountryForMobileNo1(c){
    this.custCountryNmMobileNo1=c;
  }

 ///////////////////// get info for search request and inward from create request and inward table /////////

  sendDocNoForGettingCreateReqDetails(salesQuotationDocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_REQUEST_DETAILS_URL}`, salesQuotationDocument);
  }

  getCreateReqDetailsForSearchReq(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_REQUEST_DETAILS_URL}`);
  }
  
}
