import { TestBed, inject } from '@angular/core/testing';

import { SearchDefinitionService } from './search-definition.service';

describe('SearchDefinitionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchDefinitionService]
    });
  });

  it('should be created', inject([SearchDefinitionService], (service: SearchDefinitionService) => {
    expect(service).toBeTruthy();
  }));
});
