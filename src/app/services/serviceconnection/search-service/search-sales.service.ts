import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class SearchSalesQuotationByDocument{

   searchDocument:String;
   draft:number;
   archieved:number;
   submitted:number;
   rejected:number;
   approved:number;
   toDate:any;
   fromDate:any;
}

@Injectable()
export class SearchSalesService  {

  documentNumber:string;
  custCountryNmMobileNo1:  string;
  selectedDropdown: Array<any> = [];

 // private searchandmodifysalesandquotation = 'http://localhost:8090/api/v1/search_and_modify_sales_and_quotation';
  
  constructor(private http: HttpClient) { }

  SearchAndModifySalesAndQuotation(salesandquotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SEARCH_SALES_URL}`, salesandquotation);
  }

  searchData(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_SALES_URL}`,model);    
  }  

  showData(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_SALES_URL}`);    
  }  


  ////////////////////////////// send document no to sales quotation document //////////////////////

  getDocNoInSalesquotDocScreen()
  {
     return this.documentNumber;
  }

  setDocNoForsalesQuotDocScreen(docNo)
  {
    this.documentNumber=docNo;
  }

  getCustomerCountryForMobileNo1(){
    return this.custCountryNmMobileNo1
  }

  setCustomerCountryForMobileNo1(c){
    this.custCountryNmMobileNo1=c;
  }
  setMultiselectDropdown(c){
    this.selectedDropdown=c;
  }

  getMultiselectDropdown(){
    return this.selectedDropdown;
  }
 

  sendDocNoForGettingCreateSalesDetails(salesQuotationDocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_GET_CREATE_SALES_DETAILS_URL}`, salesQuotationDocument);
  }

  getCreateSalesDetailsForSearchSales(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_GET_CREATE_SALES_DETAILS_URL}`);
  }
  

}
