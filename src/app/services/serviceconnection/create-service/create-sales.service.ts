import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class CreateSalesAndQuotation {
  id:number;
  typeOfQuotation:number;
  documentNumber:string;
  quotationNumber:string;
  customerId:number;
  branchId:number;
  custRefNo:number;
  refDate:any;
  kindAttachment:string;
  countryPrefixForMobile:string;
  contactNo:string;
  email:string;
  dateOfQuotation:any;
  quotationItemFile:number;
  terms:string;
  termsAndCondition:string;
  archieveDate:any;
  enquiryAttachment:number;
  customerName:String;
  createdBy:string;
  draft:number;
  approved:number;
  submitted:number;
  archieved:number;
  rejected:number;
  remark:string;
  dateCreated:any;
  contactNo1: string;

}
export class EnquiryAttachment {
  id:number;
  name:String;
  type:String;
  picByte:String;
}

export class QuotationItemFile {
  id:number;
  name:String;
  type:String;
  picByte:String;
}


export class termsForSalesAndQuotation {
  documentNumber:String;
  customerId:number;
  termsId:number;
  box1:number;
  box2:number;
  remark:string;
}

export class JsonForCsvFile {
  ACCREDIATION:String;
   AMOUNT:number;
   DESCRIPTION:String;
   ID_NO:String;
   QUANTITY:number;
   RANGE_OR_ACCURACY:String;
   RATE:number;
   SAC_CODE:number;
   Sr_NO:number;
}

export class SalesQuotationDocument {
  id:number;
  documentNumber:string;
 // quotationNumber:string;
  srNo:any;
  instruName:number;
  description:String;
  idNo:String;
  accrediation:String;
  rangeAccuracy:String;
  sacCode:String;
  quantity:number;
  rate:number;
  discountOnItem:number;
  discountOnTotal:number;
  amount:number;
  subTotal:number;
  total:number;
  cgst:number;
  sgst:number;
  igst:number;
  net:number;
}

export class SalesQuotationDocumentCalculation {
  id:number;
  documentNumber:string;
  subTotal:number;
  discountOnSubtotal:number;
  total:number;
  cgst:number;
  sgst:number;
  igst:number;
  net:number;
}


export class DynamicGrid{     
  uniqueId:number;
  srNo : number;
  instruName:string;
  description: string;
  idNo:string;
  accrediation:string;
  range: string;
  sacCode: string;
  quantity:number;
  rate: number;
  discountOnItem: number;
  discountOnTotal:number;
  amount : number;
  total: any;
  customerPartNo: any;
  hsnNo:any;
  rangeFrom:string;
  rangeTo:string;
}

export class CustomerAttachments {
  
  enquiryAttachmentId:number;
  originalFileName:string;
  attachedFileName:string;
  type:string;
  documentNumber:string;
  fileLocation:string;
  
}


@Injectable()
export class CreateSalesService {
 
 
  jsonArray1= new Array();
  jsonArray2:any[]
  jsonArray4: JsonForCsvFile[] =[];
  salesAndQuotationForDocument:CreateSalesAndQuotation =new CreateSalesAndQuotation();
  quotationItemFileData1: QuotationItemFile = new QuotationItemFile();
  selectedFileForQuotation1 : File;
  trialForFileUpload:any;
  arrayForTermsId:Array<any>=[];
  salesAndQuotationForDocument1: any;
  custCountryNmMobileNo1: any;
  arrayForDynamicTerms:Array<any>=[];
  constructor(private http: HttpClient) { 
   
  }

  ngOnInit()
  {

    var jsonArray3: JsonForCsvFile[] =[];
    function iSetJsonArrayForCsv(jsonArray)
    {
       jsonArray3.push(jsonArray);
    }

  }

  
  updateSalesAndQuotation(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_SALES_URL}/${id}`, value);
  }

  deleteSalesAndQuotation(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_SALES_URL}/${id}`, { responseType: 'text' });
  }
  createSalesAndQuotation(salesAndQuotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_SALES_URL}`, salesAndQuotation);
  }

  getCustomerList(): Observable<any> {
    return this.http.get(`${SystemConstants.CUSTOMER_LIST_URL}`);
  }
  
  getQuotationTypeList(): Observable<any> {
    return this.http.get(`${SystemConstants.QUOTATION_TYPE_URL}`);
  } 
  getTermsList(): Observable<any> {
    return this.http.get(`${SystemConstants.TERMS_URL}`);
  }
  getBranchList(): Observable<any> {
    return this.http.get(`${SystemConstants.BRANCH_URL}`);
  }
  getReferenceList(): Observable<any> {
    return this.http.get(`${SystemConstants.REFERENCE_FOR_SALES_URL}`);
  }
  
  enquiryAttachmentFileUpload(uploadImageData: Object): Observable<any> {
    return this.http.post(`${SystemConstants.ENQUIRY_ATTACHMENT_URL}`, uploadImageData);
  }

  quotationItemFileUpload(uploadImageData: Object): Observable<any> {
    return this.http.post(`${SystemConstants.QUOTATION_ITEM_FILE_URL}`, uploadImageData);
  }

  addCustomerInCustomerAndTermsJunctionTable(model : any): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.ADDING_CUSTOMER_TERM_ID_URL}`,model);    
  } 

  removeCustomerInCustomerAndTermsJunctionTable(model : any): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.REMOVE_CUSTOMER_TERM_ID_URL}`,model);    
  } 
  removeAllCustomerInCustomerAndTermsJunctionTable(model : any): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.REMOVE_ALL_CUSTOMER_TERM_ID_URL}`,model);    
  } 

  documentNumberForSalesAndQuotation(): Observable<any>{  
 //   debugger;    
    return this.http.get(`${SystemConstants.DOC_NO_FOR_SALES_QUOT_URL}`);    
  } 

  getSalesQuotationDocument()
  {
    return this.salesAndQuotationForDocument;
  }

  setSalesQuotationDocument(salesAndQuotation: CreateSalesAndQuotation)
  {
     this.salesAndQuotationForDocument=salesAndQuotation;
  }

  getQuotationFileDocument()
  {
  //  return this.quotation_item_file_data1;
    return  this.trialForFileUpload;
  }

  setQuotationFileDocument(quotation: any)
  {
   //  this.quotation_item_file_data1=quotation; 
   this.trialForFileUpload=quotation;
  }


  getJsonArrayForCsv()
  {
    return this.jsonArray4;
  }

  setJsonArrayForCsv(jsonArray)  
  {
    console.log(jsonArray);
   //  this.jsonArray4.push(jsonArray);
   //  this.jsonArray1.next(jsonArray);
    jsonArray.forEach((order: any) => {
    this.jsonArray4.push(order);
});
  }




  /////////////////////////////// Get array of terms id for sales quotation document //////////////////////////

getArrayofTermsId()
{
  return this.arrayForTermsId;
}

setArrayofTermsId(arrayForTerms)
{
   this.arrayForTermsId=arrayForTerms;
}

getAllTermsIdsDynamically(){
  return this.arrayForDynamicTerms;
}
setAllTermsIdsDynamically(arrayForTermsDynamically){
  this.arrayForDynamicTerms=arrayForTermsDynamically

}



//////////////////////////////// Get SalesQuotationDocumentWithId for manage Document //////////////////////////

getSalesQuotationDocumentWithId()
{
  return this.salesAndQuotationForDocument1;
}

setSalesQuotationDocumentWithId(salesAndQuotation)
{
   this.salesAndQuotationForDocument1=salesAndQuotation;
}





  //////////////////////////////////////////////////////// for salesquotationdocument /////////////////////////

  getSalesQuotationDocumentForDatabase(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_SALES_QUOT_DOCUMENT_URL}/${id}`);
  }

  createSalesQuotationDocumentForDatabase(salesQuotationDocument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_SALES_QUOT_DOCUMENT_URL}`, salesQuotationDocument);
  }

  updateSalesQuotationDocumentForDatabase(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_SALES_QUOT_DOCUMENT_URL}/${id}`, value);
  }

  deleteSalesQuotationDocumentForDatabase(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_SALES_QUOT_DOCUMENT_URL}/${id}`, { responseType: 'text' });
  }


////////////////////////// store procedure call for sr no with doc no and quot no for sales quotation document screen ///////////////////////////////

getSrNoForSalesQuotationDocument(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_SR_NO_SALES_QUOT_DOC_URL}`);
}

sendDocNoOfSalesQuotationDocument(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_SR_NO_SALES_QUOT_DOC_URL}`, salesQuotationDocument);
}


 //////////////////////////////////////////////////////// for salesquotationdocumentCalculation /////////////////////////

 getSalesQuotationDocumentCalculation(id: number): Observable<any> {
  return this.http.get(`${SystemConstants.SALES_QUOT_DOCUMENT_CAL_URL}/${id}`);
}

createSalesQuotationDocumentCalculation(salesQuotationDocumentCalculation: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SALES_QUOT_DOCUMENT_CAL_URL}`, salesQuotationDocumentCalculation);
}

updateSalesQuotationDocumentCalculation(id: number, value: any): Observable<Object> {
  return this.http.put(`${SystemConstants.SALES_QUOT_DOCUMENT_CAL_URL}/${id}`, value);
}

deleteSalesQuotationDocumentCalculation(id: number): Observable<any> {
  return this.http.delete(`${SystemConstants.SALES_QUOT_DOCUMENT_CAL_URL}/${id}`, { responseType: 'text' });
}

getDocDetailsForSalesQuotationDocumentCalculation(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_DOC_DETAILS_SALES_QUOT_DOC_CAL_URL}`);
}

sendDocNoOfSalesQuotationDocumentCalculation(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_DOC_DETAILS_SALES_QUOT_DOC_CAL_URL}`, salesQuotationDocument);
}

///////////////////////////// get info for search sales from create sales quotation table /////////

getCreateSalesDetailsForSearchSales(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_GET_CREATE_SALES_DETAILS_URL}`);
}

sendDocNoForGettingCreateSalesDetails(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_GET_CREATE_SALES_DETAILS_URL}`, salesQuotationDocument);
}

///////////////////////////// get terms id for document number of sales quotation /////////

getTermsIdForSearchSales(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_GET_TERMS_ID_FOR_SEARCH_SALES_URL}`);
}

sendDocNoForGettingTermsId(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_GET_TERMS_ID_FOR_SEARCH_SALES_URL}`, salesQuotationDocument);
}
getCustomerCountryForMobileNo1(){
  return this.custCountryNmMobileNo1
}

setCustomerCountryForMobileNo1(c){
  this.custCountryNmMobileNo1=c;
}

////////////////////file attachments ////////////////////////////////////

custAttachmentsFileUpload(uploadImageData: Object): Observable<any> {
  return this.http.post(`${SystemConstants.ENQUIRY_ATTACHMENTS_FILE_URL}`, uploadImageData);
}

createCustomerAttachments(customerContact: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.ENQUIRY_ATTACHMENTS}`, customerContact);
}

getInstrumentList(): Observable<any> {
  return this.http.get(`${SystemConstants.INSTRUMENT_LIST_URL}`);
}
deleteRowForSaleseSrNo(id: number): Observable<Object> {
  return this.http.post(`${SystemConstants.DELETE_SALES_ROW_FOR_SR_NO_URL}`,id);
}

}
