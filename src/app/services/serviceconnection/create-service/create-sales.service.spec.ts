import { TestBed, inject } from '@angular/core/testing';

import { CreateSalesService } from './create-sales.service';

describe('CreateSalesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateSalesService]
    });
  });

  it('should be created', inject([CreateSalesService], (service: CreateSalesService) => {
    expect(service).toBeTruthy();
  }));
});
