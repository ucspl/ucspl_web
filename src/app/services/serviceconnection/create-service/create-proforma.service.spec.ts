import { TestBed, inject } from '@angular/core/testing';

import { CreateProformaService } from './create-proforma.service';

describe('CreateProformaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateProformaService]
    });
  });

  it('should be created', inject([CreateProformaService], (service: CreateProformaService) => {
    expect(service).toBeTruthy();
  }));
});
