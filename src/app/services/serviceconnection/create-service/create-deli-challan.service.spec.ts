import { TestBed, inject } from '@angular/core/testing';

import { CreateDeliChallanService } from './create-deli-challan.service';

describe('CreateDeliChallanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateDeliChallanService]
    });
  });

  it('should be created', inject([CreateDeliChallanService], (service: CreateDeliChallanService) => {
    expect(service).toBeTruthy();
  }));
});
