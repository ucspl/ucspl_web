
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';

export class Customer {

  id: number;
  isParent: number;
  parentReference: number;
  name: string;
  department:string;
  panNo: string;
  gstNo:string;
  email1:string;
  email2:string;
  countryPrefixForMobile1:string;
  countryPrefixForMobile2:string;
  mobileNumber1:string;
  mobileNumber2:string;
  landlineNumber:string;
  addressLine1:string;
  addressLine2:string;
  country:string;
  state:string;
  city:string;
  pin:number;
  regionAreaCode:number;
  billingCreditPeriod:number;
  status:number;
  vendorCode:string;
  passFailRemark:number;
  taxes:string;
  applicableTaxes:string;
  taxpayerType:number;
  cgst:number;
  igst:number;
  sgst:number;
  sezNote:string;;
  checkedBy:string;
  createdBy:string;
  modifiedBy:string;
  dateCreated:any;
  dateModified:any;
  calibratedProcedure:number;
  referencedUsed:number;
  expandedUncertainty:number;
  note:number;
  lut:number;
  sez:number;
  masterlhs:number;
  uuclhs:number;
  parentCustomer:number;
  customFields:number;
         
}
export class RegionAreaCode {

  regionAreaCodeId: number;
  regionAreaCodeName: string;
}

export class BillingCreditPeriod {

  billingCreditPeriodId: number;
  billingCreditPeriodName: string;
}

export class Taxes {

  taxId: number;
  taxName: string;
}


export class DynamicGridCustContact{     
  
  id:number;
  name: string;
  custDepartment:  string;
  jobTitle:  string;
  mobileNo1:  string;
  mobileNo2:  string;
  phoneNo:  string;
  email:  string;
  status:  string;
}

export class CustomerTaxId{
  customerId:number;
  taxId:number;
}

export class TaxpayerTypeForCustomer{
  taxpayerTypeId: number;
	taxpayerTypeName: string;
}

export class Country{
	countryId:number;
	countryName:string;
}

export class States{
  stateId: number;
	countryId: number;
	stateName: string;
	stateCode: number;
}

export class CustomerContactList{
  customerContactListId: number;
	customerId: number;
	personName: string;
	department: string;
	jobTitle: string;
	mobileNumber1: string;
	mobileNumber2: string;
	phoneNumber: string;
	email: string;
	status: string;
}

export class CustomerAttachments {
  
  custContactId:number;
  custId:number;
	documentType:string;
	documentName:string;;
	documentNumber:string;;
	originalFileName:string;
	attachedFileName:string;
	type:string;
	picbyte:string;
  fileLocation:string;

}


@Injectable()
export class CreateCustomerService {
  //private baseUrl = 'http://localhost:8090/api/v1/customers';
  //private regionareacodeUrl = 'http://localhost:8090/api/v1/regionareacode';

  constructor(private http: HttpClient) {}
   getCustomer(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_CUSTOMER}/${id}`);
  }

  createCustomer(customer: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_CUSTOMER}`, customer);
  }

  updateCustomer(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_CUSTOMER}/${id}`, value);
  }

  deleteCustomer(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_CUSTOMER}/${id}`, { responseType: 'text' });
  }

  getCustomerList(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_CUSTOMER}`);
  }

  getRegionAreaCodeList(): Observable<any> {
    return this.http.get(`${SystemConstants.REGIONAREACODE}`);
  }

  getBillingCreditPeriodList(): Observable<any> {
    return this.http.get(`${SystemConstants.BILLING_CREDIT_PERIOD}`);
  }

  getTaxesList(): Observable<any>{
    return this.http.get(`${SystemConstants.TAXES}`);
  }

  getCountryList(): Observable<any>{
    return this.http.get(`${SystemConstants.COUNTRY}`);
  }

  getStatesList(): Observable<any>{
    return this.http.get(`${SystemConstants.STATES}`);
  }

  getTaxpayerTypeList(): Observable<any>{
    return this.http.get(`${SystemConstants.CUSTOMER_TAXPAYER_TYPE_URL}`);
  }

  addCustomerInCustomerAndTaxJunctionTable(model): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.ADDING_CUSTOMER_TAX_ID_URL}`,model);    
  } 

  addCustomerInCustomerAndTaxJunctionTableTrial(model): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.ADDING_CUSTOMER_TAX_ID_URL_TRIAL}`,model);    
  } 

 //////////////////////// cust contact list /////////////////////////////////////
 
getCustomerContact(id: number): Observable<any> {
  return this.http.get(`${SystemConstants.CUST_CONTACT_LIST}/${id}`);
}

createCustomerContact(customerContact: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.CUST_CONTACT_LIST}`, customerContact);
}

updateCustomerContact(id: number, value: any): Observable<Object> {
  return this.http.put(`${SystemConstants.CUST_CONTACT_LIST}/${id}`, value);
}

deleteCustomerContact(id: number): Observable<any> {
  return this.http.delete(`${SystemConstants.CUST_CONTACT_LIST}/${id}`, { responseType: 'text' });
}

getCustomerContactList(): Observable<any> {
  return this.http.get(`${SystemConstants.GET_CUST_CONTACT_LIST}`);
}
 
getCustomerContactListByCustId(id): Observable<any> {
  return this.http.get(`${SystemConstants.CUST_CONTACT_LIST_BY_CUST_ID}/${id}`);
}
 
//////////////////// cust attachments ////////////////////////////////////

custAttachmentsFileUpload(uploadImageData: Object): Observable<any> {
  return this.http.post(`${SystemConstants.CUST_ATTACHMENTS_FILE_URL}`, uploadImageData);
}

createCustomerAttachments(customerContact: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.CUST_ATTACHMENTS}`, customerContact);
}

// getCustomerAttachList(): Observable<any> {
//   return this.http.get(`${SystemConstants.CUST_ATTACHMENTS}`);
// }

getCustomerAttachLists(id): Observable<any> {
  return this.http.get(`${SystemConstants.CUST_ATTACHMENTS_GET_FILE}/${id}`);
}

sendCustAttchDetailsToGetDoc(value): Observable<any> {
  return this.http.get(`${SystemConstants.GET_SELECTED_CUSTATTACH_FILE}`,value);
}

getCustAttchDetailsToGetDoc(value): Observable<any> {
  return this.http.post(`${SystemConstants.GET_SELECTED_CUSTATTACH_FILE}`,value);
}


}



