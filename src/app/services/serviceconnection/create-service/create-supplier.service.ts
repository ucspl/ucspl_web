import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class Supplier {

  name:string;
  panNo:string;
  gstNo:string;
  primaryContact:string;
  secondaryContact:string;
  phoneNo:string;

  countryPrefixForPhoneNo:String;
  countryPrefixForPrimaryMobile:String;
  countryPrefixForSecondaryMobile:String;
  primaryPhoneNo: string;
  secondaryPhoneNo:string;
  supplierType:string;
  email:string;
  approvalStatus:string;
  status: number;
  country:string;
  addressLine1:string;
  addressLine2: string;
  state:string;
  city: string;
  code: string;
  createdBy: string;
  modifiedBy: string;
  dateCreated:any;
  dateModified:any;
     
}

export class SupplierAttachments {
  
  suppContactId:number;
  suppId:number;
	documentType:string;
	documentName:string;;
	documentNumber:string;;
	originalFileName:string;
	attachedFileName:string;
	type:string;
	picbyte:string;
  fileLocation:string;

}


@Injectable()
export class CreateSupplierService {

  
  constructor(private http: HttpClient) {}
  getSupplier(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_SUPPLIER}/${id}`);
  }

  createSupplier(supplier: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_SUPPLIER}`, supplier);
  }

  updateSupplier(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_SUPPLIER}/${id}`, value);
  }

  deleteSupplier(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_SUPPLIER}/${id}`, { responseType: 'text' });
  }

  getSuppliersList(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_SUPPLIER}`);
  }

  ////////////////// GET SUPPLIER TYPE ///////////////////////

  getSupplierTypes(): Observable<any> {
    return this.http.get(`${SystemConstants.SUPPLIER_TYPE}`);
  }

  //////////////////// Supplier attachments ////////////////////////////////////

suppAttachmentsFileUpload(uploadImageData: Object): Observable<any> {
  return this.http.post(`${SystemConstants.SUPP_ATTACHMENTS_FILE_URL}`, uploadImageData);
}

createSupplierAttachments(customerContact: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SUPP_ATTACHMENTS}`, customerContact);
}


getSupplierAttachLists(id): Observable<any> {
  return this.http.get(`${SystemConstants.SUPP_ATTACHMENTS_GET_FILE}/${id}`);
}

getSuppAttchDetailsToGetDoc(value): Observable<any> {
  return this.http.post(`${SystemConstants.GET_SELECTED_SUPPATTACH_FILE}`,value);
}


}
