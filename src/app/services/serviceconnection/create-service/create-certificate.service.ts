import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';


export class CreateMasterInstrumentDetails {

	id: number;
	calibrationLabId: number;
	idNo: string;
	personHolding: string;
	masterName: string;
	instrumentTypeId: number;
	calibrationFrequency: string;
	srNo: string;
	rangeTypeId: number;
	purchasedFrom: string;
	make: string;
	masterTypeId: number;
	dateOfPurchase: string;
	model: string;
	readingOverloading: number;
	dateOfInstallation: string;
	lcUomChart: number;
	status: number;
	branchId: number;
	dateCreated: string;
	dateModified: string;
	createdBy: string;
	ModifiedBy: string;
	draft: number;
	approved: number;
	archieved: number;
	submitted: number;
	rejected: number;

}

export class CreateMasterInstrumentSpecificationDetails {

	checkbox: boolean;
	masterSpecificationId: number;
	createMasterInstruId: number;
	parameterId: number;
	masterIdParaName: string;
	parameterNumber: string;
	rangeFrom: string;
	rangeTo: string;
	rangeUom: number;
	freqFrom: string;
	freqFromUom: number;
	freqTo: string;
	freqToUom: number;
	leastCount: string;
	leastCountUom: number;
	accuracy: string;
	formulaAccuracy: number;
	uomAccuracy: number;
	accuracy1: string;
	formulaAccuracy1: number;
	uomAccuracy1: number;
	tempChartType: number;
	modeType: number;
	draft: number;
	approved: number;
	archieved: number;
	submitted: number;
	rejected: number;
}

export class CreateMasterInstruCalibrationCertificateDetails {

	mastInstruCalCertificateDetId: number;
	masterInstruId: number;
	parameterNumber: string;
	parameterId: number;
	calCertificate: string;
	calCertificateAgencyId: number;
	calibrationDate: any;
	calibrationDue: any;
	dateOfOutward: any;
	dateOfInward: any;
	calibrationCharges: string;
	calibrationData: string;
	attachment: any;
	draft: number;
	approved: number;
	archieved: number;
	submitted: number;
	rejected: number;
}

export class CreateCalibrationResultDataForMasterInstru {

	id: number;
	masterSpecificationId: number;
	createMasterInstruId: number;
	parameterId: number;
	parameterNumber: string;
	masterIdParaName: string;
	uniqueSrNumber: string;
	stdReading: string;
	uucReading: string;
	error: string;
	unc: string;
	acFrequency: string;
	uncFormula: number;
	uncUom: number;
	acFrequencyUom: number;
}




export class CalibrationLabTypeForMasterInstrument {

	calibrationLabTypeId: number;
	calibrationLabTypeName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class InstrumentTypeForMasterInstrument {

	instruTypeId: number;
	instruTypeName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class MasterTypeForMasterInstrument {

	masterTypeId: number;
	masterTypeName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class RangeTypeForMasterInstrument {

	rangeTypeId: number;
	rangeTypeName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class ModeTypeForMasterInstrument {

	modeId: number;
	modeName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class TempChartTypeForMasterInstrument {

	tempChartId: number;
	tempChartName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;
}

export class CalibrationAgencyOfMasterInstrument {

	calibrationAgencyId: number;
	calibrationAgencyName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: string;
}

export class SearchMasterInstruByMIdPNmAndStatus {

	msIdParaName: String;
	draft: number;
	archieved: number;
	submitted: number;
	rejected: number;
	approved: number;
	toDate: any;
	fromDate: any;
}

export class SearchMasterInstruByMastIdAndStatus {

	mastId: number;
	draft: number;
	archieved: number;
	submitted: number;
	rejected: number;
	approved: number;
	// toDate:any;
	// fromDate:any;
}


export class FrequencyUom {
	freqUomId: number;
	freqUomName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}

export class MasterInstruCalCertAttachments {

	id: number;
	mastId: number;
	mastCalCertId: number;
	documentType: string;
	documentName: string;;
	documentNumber: string;;
	originalFileName: string;
	attachedFileName: string;
	type: string;
	picbyte: string;
	fileLocation: string;

}

export class SearchAttachmentByMastIdAndCalCertId {
	mastId: number;
	mastCalCertId: number;
}

export class SearchMasterInstruByMastIdAndParameterId {

	createMasterInstruId: number;
	parameterId: number;
}

export class UncFormula {

	uncFormulaId: number;
	uncFormulaName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: any;

}


export class CreateUncNameMaster {

	createUncNameMasterId: number;
	uncName: number;
	dateOfConfiguration:string;
	draft: number;
	archieved: number;
	submitted: number;
	rejected: number;
	approved: number;
}

export class CreateUncParameterMasterSpecificationDetails{

	createUncParameterMasterSpecificationId: number;
	uncMasterInstruId:number;
	parameterNumber: String;
	sourceOfUncertainity: number;
    probabiltyDistribution: number;
    type: number;
    dividingFactor: number;
    sensitivityCoefficient: string;
    valueForEstimate: string;
    percentageForEstimateAg:  string;
	percentageForEstimateDg:  string;
    degreeOfFreedom: number;
    masterType:number;
    masterNumber: number;

}





export class CreateUucNameMaster {

	createUucNameMasterId: number;
	instrument: number;
	draft: number;
	archieved: number;
	submitted: number;
	rejected: number;
	approved: number;
}
export class CreateUucParameterMasterSpecificationDetails {

	createUucParameterMasterSpecificationId: number;
	uucMasterInstruId:number;
	parameter: number;
	parameterNumber: String;
	instruUom: number;
	calibrationLabParameter: number;
	rAndRTable: number;
	uncMaster: number;
	ipParameter: number;
	ipParameterUom: number;
	calibrationProcedure: number;
	defaultRepetability: number;
	uncReading: string;
	typeOfRepetability: string;
	uncPrintUnit: string;
	massCalMethod: string;
	nablScopeName: number;
	remarks: number;
	accrediation: string;
}

export class CreateEnvMaster {

	id: number;
	calibrationLab: number;
	calibratedAt: string;
	status: string;
	validFromDate: string;
	validToDate: string;
	temperature1: string;
	tempSign: string;
	temperature2: string;
	humidity1: string;
	humiditySign: string;
	humidity2: string;
	atmpress1: string;
	atmpressSign: string;
	atmpress2: string;
	draft: number;
	approved: number;
	submitted: number;

}

export class CreateUomCalculator {

	uomCalculatorId:number; 
	parameter:number;
	draft: number;
	archieved: number;
	submitted: number;
	rejected: number;
	approved: number;

}


export class CreateUomCalculatorDetails {

	createUomCalculatorDetailsId:number; 
	uomCalculatorId:number;
	parameterNumber:string;
	uomFrom:number;
	uomTo:number;
	multiplyBy:string;
	draft: number;
	approved: number;
	submitted: number;

}



export class JunctionOfInputUomAndCreateUUCParaSpecification {
	junctionOfInputUomAndCreateUUCParaSpecId: number;
	createUucId: number;
	createUucParameterMasterSpecificationId: number;
	uomId: number;
}


export class JunctionOfUomAndCreateUUCParaSpecification {
	junctionOfUomAndCreateUUCParaSpecId: number;
	createUucId: number;
	createUucParameterMasterSpecificationId: number;
	uomId: number;
}


@Injectable()
export class CreateCertificateService {

	constructor(private http: HttpClient) { }

	getMasterInstrumentDetails(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_DETAILS_URL}`);
	}

	getMasterInstrumentDetailById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_DETAILS_URL}/${id}`);
	}

	createMasterInstrumentDetail(masterInstru: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_MASTER_INSTRU_DETAILS_URL}`, masterInstru);
	}

	updateMasterInstrumentDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_MASTER_INSTRU_DETAILS_URL}/${id}`, value);
	}

	deleteMasterInstrumentDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_MASTER_INSTRU_DETAILS_URL}/${id}`, { responseType: 'text' });
	}


	///////////////////////// Master specification details ///////////////////////
	getMasterInstrumentSpecificationDetails(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL}`);
	}

	getMasterInstrumentSpecificationDetailById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL}/${id}`);
	}

	createMasterInstrumentSpecificationDetail(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL}`, mastSpecDet);
	}

	updateMasterInstrumentSpecificationDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL}/${id}`, value);
	}

	updateMasterInstrumentSpecificationDetailForApproval(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_FOR_ARROVE_URL}/${id}`, value);
	}

	updateParameterNoMasterInstrumentSpecificationDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_PARANO_URL}/${id}`, value);
	}

	deleteMasterInstrumentSpecificationDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_MASTER_INSTRU_SPECIFICATION_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	getSrNoOfMasterInstrumentSpecificationDetail(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_MASTER_SPECIFICATION_DETAILS_URL}`, value);
	}

	getSrNoOfMasterInstruSpecifiDetailByMastIdParaName(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_MASTER_SPECIFICATION_DETAILS_BY_MASTID_PARANAME_URL}`, value);
	}

	///////////////////////// Master instrument calibration certificate details ///////////////////////
	getMasterInstrumentCalCertificateDetails(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL}`);
	}

	getMasterInstrumentCalCertificateDetailById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL}/${id}`);
	}

	createMasterInstrumentCalCertificateDetail(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL}`, mastSpecDet);
	}

	updateMasterInstrumentCalCertificateDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL}/${id}`, value);
	}

	deleteMasterInstrumentCalCertificateDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_MASTER_INSTRU_CAL_CERTIFICATE_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	getSrNoOfMasterInstrumentCalCertificateDetail(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_MASTER_CAL_CERTIFICATE_DETAILS_URL}`, value);
	}

	getSrNoOfMasterInstrumentCalCertificateDetailByStatus(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_MASTER_CAL_CERTIFICATE_DETAILS_BY_STATUS_URL}`, value);
	}

	getSrNoOfMasterInstrumentCalSpeciDetailByMastIdStatus(value: any): Observable<any> {

		return this.http.post(`${SystemConstants.SP_SRNO_MASTER_CAL_SPEC_DETAILS_BY_MASTID_STATUS_URL}`, value);
	}
	//////////////////// Cal certificate attachments ////////////////////////////////////

	mastCalCertiAttachmentsFileUpload(uploadImageData: Object): Observable<any> {
		return this.http.post(`${SystemConstants.MAST_CAL_CERTIFICATE_ATTACHMENTS_FILE_URL}`, uploadImageData);
	}

	createMastCalCertiAttachments(customerContact: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.MAST_CAL_CERTIFICATE_ATTACHMENTS}`, customerContact);
	}

	getMastCalCertiAttachLists(id): Observable<any> {
		return this.http.get(`${SystemConstants.MAST_CAL_CERTIFICATE_ATTACHMENTS_GET_FILE}/${id}`);
	}

	sendMastCalCertiAttchDetailsToGetDoc(value): Observable<any> {
		return this.http.get(`${SystemConstants.GET_SELECTED_MAST_CAL_CERTIFICATEATTACH_FILE}`, value);
	}

	getMastCalCertiAttchDetailsToGetDoc(value): Observable<any> {
		return this.http.post(`${SystemConstants.GET_SELECTED_MAST_CAL_CERTIFICATEATTACH_FILE}`, value);
	}

	getMastCalCertiAttchDetailsByMastIdCalId(value): Observable<any> {
		return this.http.post(`${SystemConstants.MAST_CAL_CERTIFICATE_ATTACHMENTS_GET_BY_MASTID_AND_CALID}`, value);
	}

	deleteMastCalCertiAttchFile(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.MAST_CAL_CERTIFICATE_ATTACHMENTS_FOR_DETETE_FILE}/${id}`, { responseType: 'text' });
	}

	deleteMastCalCertiAttchDetailsByCalId(value): Observable<any> {
		return this.http.post(`${SystemConstants.DELETE_MAST_CAL_CERTIFICATE_ATTACHMENTS_BY_CALID}`, value);
	}


	//////////////////////////// create calibration result data for master instrument ////////////////////


	createCalCertificateResultDataForSelectedParameter(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL}`, mastSpecDet);
	}

	updateCalCertificateResultDataForSelectedParameter(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL}/${id}`, value);
	}

	updateCalCertificateResultDataForUniqueSrNo(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.SP_UPDATE_SRNO_CALIBRATION_RESULT_DATA_FOR_UNIQUESRNO_URL}/${id}`, value);
	}

	deleteCalCertificateResultDataForSelectedParameter(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL}/${id}`, { responseType: 'text' });
	}

	getCalCertificateResultDataForSelectedParameter(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL}`);
	}

	getSrNoOfCalCertificateResultDataForSelectedParaByParaId(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_CALIBRATION_RESULT_DATA_FOR_SELECTED_PARAMETER_URL}`, value);
	}

	getSrNoOfCalCertificateResultDataForSelectedParaByCreateMastInstruId(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_CALIBRATION_RESULT_DATA_FOR_CREATEMASTERINSTRUID_URL}`, value);
	}

	getSrNoOfMISpecifiByMastIdParaNameForCalResultData(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_MI_SPECIFI_DETAILS_BY_MASTID_PARANAME_FOR_CALIBRATION_RESULT_DATA_URL}`, value);
	}

	/////////////////////////// UUC Name Master Details /////////////////////////////////

	getCreatedUucNameMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UUC_NAME_MASTER_DETAILS_URL}`);
	}

	getCreatedUucNameMasterById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UUC_NAME_MASTER_DETAILS_URL}/${id}`);
	}

	createUucNameMasterDetail(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UUC_NAME_MASTER_DETAILS_URL}`, mastSpecDet);
	}

	updateUucNameMasterDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UUC_NAME_MASTER_DETAILS_URL}/${id}`, value);
	}

	deleteUucNameMasterDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UUC_NAME_MASTER_DETAILS_URL}/${id}`, { responseType: 'text' });
	}


	////////////////////////// Uuc name parameter master specification details /////////////////////

	getCreatedUucParameterMasterSpecificationDetails(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}`);
	}

	getCreatedUucParameterMasterSpecificationDetailById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}/${id}`);
	}

	createdUucParameterMasterSpecificationDetailsFunction(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}`, mastSpecDet);
	}

	updateCreatedUucParameterMasterSpecificationDetails(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}/${id}`, value);
	}

	deleteCreatedUucParameterMasterSpecificationDetails(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UUC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	getCreatedSrNoUucParameterMasterSpecificationDetails(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_UUC_MASTER_SPECIFICATION_DETAILS_URL}`, value);
	}

	updateSrNoUucParameterMasterSpecificationDataForParaNumber(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.SP_UPDATE_SRNO_UUC_PARA_SPECIFICATION_FOR_PARANO_URL}/${id}`, value);
	}

	saveUomAndUucSpeciDataInJuncTable(value: Array<string>): Observable<Array<string>> {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			})
		};

		let body = '{}';

		let response = this.http.post(`${SystemConstants.SAVE_UOM_AND_UUC_SPECI_DATA_IN_JUNC_TABLE_URL}`, value, httpOptions)
			.pipe(
				tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
				catchError(this.handleError)
			);

		return response;
	}

	saveInputUomAndUucSpeciDataInJuncTable(value: Array<string>): Observable<Array<string>> {
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			})
		};

		let body = '{}';

		let response = this.http.post(`${SystemConstants.SAVE_INPUT_UOM_AND_UUC_SPECI_DATA_IN_JUNC_TABLE_URL}`, value, httpOptions)
			.pipe(
				tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
				catchError(this.handleError)
			);

		return response;
	}

	private handleError(error: any) {
		let errMsg = error.message || 'Server error';
		console.error(errMsg); // log to console instead
		return Observable.throw(errMsg);
	}


	/////////////////////////// Environmental Master Details /////////////////////////////////

	getCreatedEnvironmentalMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}`);
	}

	getCreatedEnvironmentalMasterById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}/${id}`);
	}

	createEnvironmentalMasterDetail(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}`, mastSpecDet);
	}

	updateEnvironmentalMasterDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}/${id}`, value);
	}

	deleteEnvironmentalMasterDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}/${id}`, { responseType: 'text' });
	}


	getDataForEnvironmentalMasterFunc(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_ENVIRONMENTAL_MASTER_DETAILS_URL}`)
	}

	getLastCreatedEnvMasterDetails(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.GET_LAST_CREATED_ENVIRONMENTAL_MASTER_DETAILS_URL}`, value)
	}

	getSecondLastCreatedEnvMasterDetails(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.GET_SECOND_LAST_CREATED_ENVIRONMENTAL_MASTER_DETAILS_URL}`, value)
	}

	updateToDateForLab(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.UPDATE_TO_DATE_OF_ENVIRONMENTAL_MASTER_URL}/${id}`, value);
	}

	
	///////////////////////// Unc Master Screen ///////////////////////////////////////


	getCreatedUncNameMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UNC_NAME_MASTER_DETAILS_URL}`);
	}

	getCreatedUncNameMasterById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UNC_NAME_MASTER_DETAILS_URL}/${id}`);
	}

	createUncNameMasterDetail(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UNC_NAME_MASTER_DETAILS_URL}`, mastSpecDet);
	}

	updateUncNameMasterDetail(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UNC_NAME_MASTER_DETAILS_URL}/${id}`, value);
	}

	deleteUncNameMasterDetail(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UNC_NAME_MASTER_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	createdUncParameterMasterSpecificationDetailsFunction(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UNC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}`, mastSpecDet);
	}

	updateCreatedUncParameterMasterSpecificationDetails(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UNC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}/${id}`, value);
	}

	deleteCreatedUncParameterMasterSpecificationDetails(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UNC_NAME_PARAMETER_MASTER_SPECIFICATION_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	getCreatedSrNoUncParameterMasterSpecificationDetails(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_UNC_MASTER_SPECIFICATION_DETAILS_URL}`, value);
	}

	updateSrNoUncParameterMasterSpecificationDataForParaNumber(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.SP_UPDATE_SRNO_UNC_PARA_SPECIFICATION_FOR_PARANO_URL}/${id}`, value);
	}




	///////////////////////// UOM Master Screen ///////////////////////////////////////


	getCreatedUomCalculator(): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UOM_CALCULATOR_URL}`);
	}

	getCreatedUomCalculatorById(id: number): Observable<any> {
		return this.http.get(`${SystemConstants.CREATE_UOM_CALCULATOR_URL}/${id}`);
	}

	createUomCalculator(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UOM_CALCULATOR_URL}`, mastSpecDet);
	}

	updateUomCalculator(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UOM_CALCULATOR_URL}/${id}`, value);
	}

	deleteUomCalculator(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UOM_CALCULATOR_URL}/${id}`, { responseType: 'text' });
	}

	createUomCalculatorDetailsFunction(mastSpecDet: Object): Observable<Object> {
		return this.http.post(`${SystemConstants.CREATE_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL}`, mastSpecDet);
	}

	updateCreatedUomCalculatorSpecificationDetails(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.CREATE_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL}/${id}`, value);
	}

	deleteCreatedUomCalculatorSpecificationDetails(id: number): Observable<any> {
		return this.http.delete(`${SystemConstants.CREATE_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL}/${id}`, { responseType: 'text' });
	}

	getCreatedSrNoUomCalculatorSpecificationDetails(value: any): Observable<any> {
		return this.http.post(`${SystemConstants.SP_SRNO_UOM_CALCULATOR_SPECIFICATION_DETAILS_URL}`, value);
	}

	updateSrNoUomCalculatorSpecificationDataForParaNumber(id: number, value: any): Observable<Object> {
		return this.http.put(`${SystemConstants.SP_UPDATE_SRNO_UOM_CALCULATOR_SPECIFICATION_FOR_PARANO_URL}/${id}`, value);
	}























	///////////////////////// get master list for master type //////////////////////

	getMasterTypesListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_MASTER_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get calibration lab list for calibration type//////////////////////

	getCalibrationLabListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_CALIBRATIONLAB_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get instrument list for master type//////////////////////

	getInstrumentTypesListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_INSTRUMENT_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get range list for master type//////////////////////

	getRangeTypesListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_RANGE_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get Mode list for master type//////////////////////

	getModeTypesListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_MODE_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get temp chart list for master type//////////////////////

	getTempChartTypesListForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_TEMP_CHART_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get temp chart list for master type//////////////////////

	getCalibrationAgencyTypesForMaster(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_CALIBRATION_AGENCY_TYPES_OF_MASTERINSTRUMENT_URL}`);
	}

	/////////////////////////get frequency uom list for master type//////////////////////
	getFrequencyUomList(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_FREQUENCY_UOM_URL}`);
	}

	///////////////////////get unc frequency ///////////////////////////
	getUncFormulaList(): Observable<any> {
		return this.http.get(`${SystemConstants.GET_UNC_FORMULA_URL}`);
	}
}
