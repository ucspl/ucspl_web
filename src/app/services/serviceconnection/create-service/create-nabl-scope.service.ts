import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';


export class CreateNablScopeMaster {

  createNableScopeMasterId:number;
  nablCertificateNo:string;
  labLocation:string;
  dateOfIssue: any;
  validUpTo:any;
  draft:number;
  approved:number;
  archieved:number;
  submitted:number;
  rejected:number;
  createdBy:string
  systemCertificateNo:string;

}

export class CreateNablSpecificationDetails {

  nablSpecificationId:number;
  createNablId:number;
  calibrationLabId:number;
  groupId:number;
  parameterId:number;
  parameterNumber:string;
  nablIdWithParameterName:string;
  nablScopeId:number;
  rangeFrom:string;
  rangeTo:string;
  rangeUom:number;
  freqFrom:string;
  freqFromUom:number;
  freqTo:string;
  freqToUom:number;
  leastCount:string;
  leastCountUom:number;
  uncValue:string;
  uncFormulaId:number;
  uncUom:number;
  tempChartType:number;
  modeType:number;
  amendmentDate:string;
  inhouse:number;
  onsite:number;
  draft:number;
  approved:number;
  archieved:number;
  submitted:number;
  rejected:number;
}

export class SearchNablSpecificationByNablIdPNmAndStatus{

	nablIdParaName:String;
	draft:number;
	archieved:number;
	submitted:number;
	rejected:number;
	approved:number;
	toDate:any;
	fromDate:any;
 }

 export class DynamicGridForNabl{     
  nablSpecificationId:number;
  createNablId:number;
  calibrationLabId:number;
  groupId:number;
  parameterId:number;
  parameterNumber:string;
  nablIdWithParameterName:string;
  nablScopeId:number;
  rangeFrom:string;
  rangeTo:string;
  rangeUom:number;
  freqFrom:string;
  freqFromUom:number;
  freqTo:string;
  freqToUom:number;
  leastCount:string;
  leastCountUom:number;
  uncValue:string;
  uncFormulaId:number;
  uncUom:number;
  tempChartType:number;
  modeType:number;
  amendmentDate:string;
  inhouse:number;
  onsite:number;
  draft:number;
  approved:number;
  archieved:number;
  submitted:number;
  rejected:number;

}

export class CalibrationLabTypeForMasterInstrument {

  calibrationLabTypeId:number;
  calibrationLabTypeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

export class InstrumentTypeForMasterInstrument {

  instruTypeId:number;
  instruTypeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;
  
}

export class MasterTypeForMasterInstrument {

  masterTypeId:number;
  masterTypeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

export class RangeTypeForMasterInstrument {

  rangeTypeId:number;
  rangeTypeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

export class ModeTypeForMasterInstrument {

  modeId:number;
  modeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

export class GroupForNablScopeMaster {

  groupId:number;
  groupName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;
  calibrationLabId:number;

}

export class NablScopeName {

  nablScopeNameId:number;
  nablScopeName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

export class UncFormula {

  uncFormulaId:number;
  uncFormulaName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}
export class FrequencyUom {
  freqUomId:number;
  freqUomName:string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;

}

@Injectable()
export class CreateNablScopeService {

  constructor(private http: HttpClient) { }

  createNablScopeMaster(nableScope: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_NABL_SCOPE_MASTER_URL}`, nableScope);
  }  
  updateNablScopeMaster(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_NABL_SCOPE_MASTER_URL}/${id}`, value); 
  }
  getGroupForNablScopeMaster(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_GROUP_FOR_NABL_SCOPE_MASTER_URL}`); 
  }
  getNablScopeName(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_NABL_SCOPE_NAME_URL}`); 
  }

  getUncFormulaList(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_UNC_FORMULA_URL}`); 
  }
  getFrequencyUomList(): Observable<any> {
    return this.http.get(`${SystemConstants. GET_FREQUENCY_UOM_URL}`); 
  }
 

  ///////////////////////// Master specification details ///////////////////////
  getNablSpecificationDetails(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_NABL_SPECIFICATION_DETAILS_URL}`);
  }

  getNablSpecificationDetailById(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_NABL_SPECIFICATION_DETAILS_URL}/${id}`);
  }

  createNablSpecificationDetail(mastSpecDet: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_NABL_SPECIFICATION_DETAILS_URL}`, mastSpecDet);
  }  

  updateNablSpecificationDetail(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_NABL_SPECIFICATION_DETAILS_URL}/${id}`, value); 
  }

  deleteNablSpecificationDetail(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_NABL_SPECIFICATION_DETAILS_URL}/${id}`, { responseType: 'text' });
  }

  getSrNoOfNablSpecificationDetail(value:any): Observable<any> {
    return this.http.post(`${SystemConstants.SP_SRNO_NABL_SPECIFICATION_DETAILS_URL}`,value);
  }

  getSrNoOfNablSpecifiDetailByNablIdParaName(value:any): Observable<any> {
    return this.http.post(`${SystemConstants.SP_SRNO_NABL_SPECIFICATION_DETAILS_BY_NABLID_PARANAME_URL}`,value);
  }

  ///////////////////////////////////////

getCreateNablSpecificationDetailsForSearchNabl(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_GET_CREATE_NABL_SPECIFICATION_DETAILS_URL}`);
}

sendNablIdForGettingCreateNablSpecificationDetails(nablId: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_GET_CREATE_NABL_SPECIFICATION_DETAILS_URL}`, nablId);
}

}
// /////////////////////////get master list for master type//////////////////////

// getMasterTypesListForMaster(): Observable<any> {
//   return this.http.get(`${SystemConstants.GET_MASTER_TYPES_OF_MASTERINSTRUMENT_URL}`);
// }

//  /////////////////////////get calibration lab list for calibration type//////////////////////

//  getCalibrationLabListForMaster(): Observable<any> {
//   return this.http.get(`${SystemConstants.GET_CALIBRATIONLAB_TYPES_OF_MASTERINSTRUMENT_URL}`);
// }

//  /////////////////////////get instrument list for master type//////////////////////

//  getInstrumentTypesListForMaster(): Observable<any> {
//   return this.http.get(`${SystemConstants.GET_INSTRUMENT_TYPES_OF_MASTERINSTRUMENT_URL}`); 
// }

//  /////////////////////////get range list for master type//////////////////////

//  getRangeTypesListForMaster(): Observable<any> {
//   return this.http.get(`${SystemConstants.GET_RANGE_TYPES_OF_MASTERINSTRUMENT_URL}`); 
// }

// /////////////////////////get Mode list for master type//////////////////////

// getModeTypesListForMaster(): Observable<any> {
//   return this.http.get(`${SystemConstants.GET_MODE_TYPES_OF_MASTERINSTRUMENT_URL}`); 
// }
