import { TestBed, inject } from '@angular/core/testing';

import { CreateCustPoService } from './create-cust-po.service';

describe('CreateCustPoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateCustPoService]
    });
  });

  it('should be created', inject([CreateCustPoService], (service: CreateCustPoService) => {
    expect(service).toBeTruthy();
  }));
});
