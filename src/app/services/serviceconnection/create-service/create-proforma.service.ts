
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class DynamicGrid{     
  
  srNo : number;
  poSrNO:string;
  itemOrPartCode:string;
  instruName :string;
  description: string;
  range: string;
  sacCode: string;
  quantity:number;
  rate: number;
  discountOnItem: number;
  unitPrice:number;
  amount : number;
  uniqueId:number;
}

export class CreateProformaInvoice{   
  id:number;
  documentTypeId: number;
  proformaDocumentNumber: string;
  requestNumber:string;
  customerDcNumber:string;
  branchId:number;
  invoiceTypeId:number; 
  custShippedTo:number;
  custBilledTo:number;
  invoiceDate:any;
  poDate:any;
  dcDate:any;
  poNo:string;
  dcNo:string;
  vendorCodeNumber:string;
  archieveDate:any;
  createdBy:string;
  draft:number;
  approved:number;
  submitted:number;
  archieved:number;
  rejected:number;
  remark:string;
  documentType: any;
  invoiceType: any;
  dateOfInvoice: any;
}

export class ProformaInvoiceDocument{
  id:number;
  proformaDocumentNumber:string;
  invoiceNumber:string;
  srNo:number;
  poSrNo:string;
  itemOrPartCode:string;
  instruId:number;
  description:string;
  range:string;
  sacCode:string;
  hsnNo:string;
  quantity:number;
  unitPrice:number;
  amount:number;
 

}

export class ProformaInvoiceDocumentCalculation{
  id:string;
  proformaDocumentNumber:string;
  subTotal:number;
  total:number;
  discountOnSubtotal:number;
  cgst:number;
  sgst:number;
  igst:number;
  net:number;
}


@Injectable()
export class CreateProformaService {

  CustomeridforChallan:any;
  createProformaInvoiceobj:CreateProformaInvoice=new CreateProformaInvoice();

  constructor(private http: HttpClient) { }

  getFinancialYearDates(): Observable<any> {
    return this.http.get(`${SystemConstants.FINANCIAL_YEAR_DATE_URL}`);
  }

  getDocumentTypeListForInvoice(): Observable<any> {
    return this.http.get(`${SystemConstants.DOCUMENT_TYPE_FOR_INVOICE_URL}`);
  }
  getInvoiceTypeListForInvoice(): Observable<any> {
    return this.http.get(`${SystemConstants.INVOICE_TYPE_FOR_INVOICE_URL}`);
  }


  getProformaInvoice(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_INVOICE_URL}/${id}`);
  }

  createProformaInvoice(createChallanCumInvoice: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_PROFORMA_INVOICE_URL}`, createChallanCumInvoice);
  }

  updateProformaInvoice(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_PROFORMA_INVOICE_URL}/${id}`, value);
  }

  deleteProformaInvoice(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_PROFORMA_INVOICE_URL}/${id}`, { responseType: 'text' });
  }

  /////////////////////// Getting customer name from search cust form to create challan cum invoice////////////////

  getCustomerNameForCreatingChallan()
  {
     return this.CustomeridforChallan;
  }

  setCustomerNameForCreatingChallan(Customerid)
  {
     this.CustomeridforChallan=Customerid;
  }

  //////////////////////////// Get document number //////////////////////

  documentNumberForProforma(): Observable<any>{  
    //   debugger;    
       return this.http.get(`${SystemConstants.DOC_NO_FOR_PROFORMA_URL}`);    
  } 

  ////////////////set data for create-invdoc component ///////////////////////

  getProformaDocument()
  {
    return this.createProformaInvoiceobj;
  }

  setProformaDocument(createProformaInvoice: CreateProformaInvoice)
  {
     this.createProformaInvoiceobj=createProformaInvoice;
  }

  ////////////////// challan cum invoice document /////////////////////////////////

  GetProformaDocument(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.PROFORMA_DOCUMENT_CREATE_URL}/${id}`);
  }

  createProformaDocument(ChallanCumInvoiceDoc: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.PROFORMA_DOCUMENT_CREATE_URL}`, ChallanCumInvoiceDoc);
  }

  updateProformaDocument(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.PROFORMA_DOCUMENT_CREATE_URL}/${id}`, value);
  }

  deleteProformaDocument(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.PROFORMA_DOCUMENT_CREATE_URL}/${id}`, { responseType: 'text' });
  }


  ////////////////// challan cum invoice document calculation /////////////////////////////////

  getProformaDocumentCalculation(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.PROFORMA_DOCUMENT_CALCULATION_URL}/${id}`);
  }

  createProformaDocumentCalculation(ChallanCumInvoiceDocCal: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.PROFORMA_DOCUMENT_CALCULATION_URL}`, ChallanCumInvoiceDocCal);
  }

  updateProformaDocumentCalculation(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.PROFORMA_DOCUMENT_CALCULATION_URL}/${id}`, value);
  }

  deleteProformaDocumentCalculation(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.PROFORMA_DOCUMENT_CALCULATION_URL}/${id}`, { responseType: 'text' });
  }

////////////////////////// store procedure call for sr no with doc no for challan invoice document screen ///////////////////////////////

getSrNoForProformaDocument(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_SR_NO_PROFORMA_DOC_URL}`);
}

sendDocOfProformaDocument(challaninvoicedocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_SR_NO_PROFORMA_DOC_URL}`, challaninvoicedocument);
}

/////////////////////// store procedure call for calculations with doc no for challan invoice document screen ///////////////////////////////
getDocDetailsForProformaDocumentCalculation(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_PROFORMA_DOCUMENT_CAL_URL}`);
}

SendDocNoOfProformaDocumentCalculation(salesquotationdocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_PROFORMA_DOCUMENT_CAL_URL}`, salesquotationdocument);
}

deleteRowForProformaInvoiceSrNo(id: number): Observable<Object> {
  return this.http.post(`${SystemConstants.DELETE_PROFORMA_ROW_FOR_SR_NO_URL}`,id);
}

}


