import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

@Injectable()
export class MngApproversService {


  //private searchurl = 'http://localhost:8090/api/v1/search_and_modify_user_by_documentType';
  //private searchurl_adduser_table = 'http://localhost:8090/api/v1/search_and_modify_user_by_documentType_adduser_table';
  //private adduser_table_in_database = 'http://localhost:8090/api/v1/adduser_table_in_database';
  //private existinguser_table_in_database = 'http://localhost:8090/api/v1/existinguser_table_in_database';
 
  constructor(private http: HttpClient) { }


  searchdata(model : Object): Observable<Object>{  
    debugger;    
   return this.http.post(`${SystemConstants.SEARCH_AND_MODIFY_USER_BY_DOCUMENT_TYPE}`,model);    
  }  

  showdata(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_AND_MODIFY_USER_BY_DOCUMENT_TYPE}`);    
  }  

  showdata_adduser_table(): Observable<any> {  
    debugger;    
   return this.http.get(`${SystemConstants.SEARCH_AND_MODIFY_USER_BY_DOCUMENT_TYPE_ADDUSER_TABLE}`);    
  }  

  AddUserInExistingTable(model : any): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.ADDUSER_TABLE_IN_DATABASE}`,model);    
  } 

  RemoveUserFromExistingTable(model : any): Observable<any>{  
    debugger;    
    return this.http.post(`${SystemConstants.EXISTINGUSER_TABLE_IN_DATABASE}`,model);    
  } 
}
