import { TestBed, inject } from '@angular/core/testing';

import { MngApproversService } from './mng-approvers.service';

describe('MngApproversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MngApproversService]
    });
  });

  it('should be created', inject([MngApproversService], (service: MngApproversService) => {
    expect(service).toBeTruthy();
  }));
});
