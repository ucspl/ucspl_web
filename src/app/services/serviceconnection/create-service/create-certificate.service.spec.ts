import { TestBed, inject } from '@angular/core/testing';

import { CreateCertificateService } from './create-certificate.service';

describe('CreateCertificateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateCertificateService]
    });
  });

  it('should be created', inject([CreateCertificateService], (service: CreateCertificateService) => {
    expect(service).toBeTruthy();
  }));
});
