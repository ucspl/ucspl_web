import { TestBed, inject } from '@angular/core/testing';

import { CreateNablScopeService } from './create-nabl-scope.service';

describe('CreateNablScopeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateNablScopeService]
    });
  });

  it('should be created', inject([CreateNablScopeService], (service: CreateNablScopeService) => {
    expect(service).toBeTruthy();
  }));
});
