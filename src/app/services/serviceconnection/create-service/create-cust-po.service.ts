import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class CreateCustomerPurchaseOrder {
id: number;
documentType: string;
documentNumber:string;
customerId:number;
poName:String;
poDate:String;
validToDate:String;
custPoNo: any;
createdBy:string;
approvedBy:string;
approved:number;
rejected:number;
submitted:number;
archieved:number;
draft:number;
poAttachedFileId:number;
updateHistory:string;
poValue:string;
remainingPoValue:string;

}

export class PurchaseOrderDocument {
  id:number;
  documentNumber:string;
  srNo:any;
  instruName:number;
  description:String;
  idNo:String;
  range:String;
  quantity:number;
  rate:number;
  discountOnItem:number;
  amount:number;
  customerPartNo:String;
  hsnNo:String;
  remainingQuantity:number;
  rangeFrom:String;
  rangeTo:String;
 // remainingAmount:number;
}


export class DynamicGrid{     
  uniqueId:number;
  srNo : number;
  instruName:string;
  description: string;
  idNo:string;
  accrediation:string;
  range: string;
  sacCode: string;
  quantity:number;
  rate: number;
  discountOnItem: number;
  discountOnTotal:number;
  amount : number;
  total: any;
  customerPartNo: any;
}

export class PoDocumentCalculation {
  id:number;
  documentNumber:string;
  subTotal:number;
  discountOnSubtotal:number;
  total:number;
  cgst:any;
  sgst:number;
  igst:number;
  net:number;
}

export class POAttachments {
  
  enquiryAttachmentId:number;
  originalFileName:string;
  attachedFileName:string;
  type:string;
  documentNumber:string;
  fileLocation:string;
  documentName:string;
  
}


@Injectable()
export class CreateCustPoService {
  createPoData1: any;
  SalesDocNo: any;
  //private baseUrlForcreateCustomerPurchaseOrder = 'http://localhost:8090/api/v1/createCustomerPurchaseOrder';
  constructor(private http: HttpClient) { }


  updateRemainingQuantityInSrNoPoTable(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.UPDATE_REMAINING_QUANTITY}/${id}`, value);
  }
  updateRemainingPoValueInCreatePoTable(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.UPDATE_REMAINING_PO_VALUE}/${id}`, value);
  }


  getCreatedCustomerPurchaseOrder(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_CUST_PO}/${id}`);
  }

  createCustomerPurchaseOrder(createcustomerpurchaseorder: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_CUST_PO}`, createcustomerpurchaseorder);
  }

  updateCustomerPurchaseOrder(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_CUST_PO}/${id}`, value);
  }

  deleteCustomerPurchaseOrder(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_CUST_PO}/${id}`, { responseType: 'text' });
  }
  
  documentNumberForPurchaseOrder(): Observable<any>{  
    //   debugger;    
       return this.http.get(`${SystemConstants.DOC_NO_FOR_PO_URL}`);    
    
      } 

 getCreatePoDataWithId()
{
  return this.createPoData1;
}

setCreatePoDataWithId(createPoData)
{
   this.createPoData1=createPoData;
}
setSalesDocumentNumber(salesQuotDocumentNo){
this.SalesDocNo=salesQuotDocumentNo;
}
 getSalesDocumentNumber()
{
  return this.SalesDocNo;
}
///////////////////////////Create Po Document//////////////////////
getPoDocumentForDatabase(id: number): Observable<any> {
  return this.http.get(`${SystemConstants.CREATE_PO_DOCUMENT_URL}/${id}`);
}

createPoDocumentForDatabase(poDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.CREATE_PO_DOCUMENT_URL}`, poDocument);
}

updatePoDocumentForDatabase(id: number, value: any): Observable<Object> {
  return this.http.put(`${SystemConstants.CREATE_PO_DOCUMENT_URL}/${id}`, value);
}

deletePoDocumentForDatabase(id: number): Observable<any> {
  return this.http.delete(`${SystemConstants.CREATE_PO_DOCUMENT_URL}/${id}`, { responseType: 'text' });
}

//////////////////////////////////////////////////////// for Po documentCalculation /////////////////////////

getPoDocumentCalculation(id: number): Observable<any> {
  return this.http.get(`${SystemConstants.PO_DOCUMENT_CAL_URL}/${id}`);
}

createPoDocumentCalculation(poDocumentCalculation: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.PO_DOCUMENT_CAL_URL}`, poDocumentCalculation);
}

updatePoDocumentCalculation(id: number, value: any): Observable<Object> {
  return this.http.put(`${SystemConstants.PO_DOCUMENT_CAL_URL}/${id}`, value);
}

deletePoDocumentCalculation(id: number): Observable<any> {
  return this.http.delete(`${SystemConstants.PO_DOCUMENT_CAL_URL}/${id}`, { responseType: 'text' });
}

getDocDetailsForPODocumentCalculation(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_DOC_DETAILS_PO_DOC_CAL_URL}`);
}

sendDocNoOfPoDocumentCalculation(poDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_DOC_DETAILS_PO_DOC_CAL_URL}`, poDocument);
}

///////////////////po file attachments ////////////////////////////////////

PoAttachmentsFileUpload(uploadImageData: Object): Observable<any> {
  return this.http.post(`${SystemConstants.PO_ATTACHMENTS_FILE_URL}`, uploadImageData);
}

createPoAttachments(customerContact: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.PO_ATTACHMENTS}`, customerContact);
}

getPoAttachLists(id): Observable<any> {
  return this.http.get(`${SystemConstants.PO_ATTACHMENTS_GET_FILE}/${id}`);
}

getPoDetailsToGetDoc(value): Observable<any> {
  return this.http.post(`${SystemConstants.GET_SELECTED_POATTACH_FILE}`,value);
}


deleteAttachedPoFile(id: number): Observable<Object> {
  return this.http.post(`${SystemConstants.PO_ATTACHMENTS_FOR_DETETE_FILE}`,id);
}

////////////////////////// store procedure call for sr no with doc no  po document screen ///////////////////////////////

getSrNoForPoDocument(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_SR_NO_PO_DOC_URL}`);
}

sendDocNoOfPoDocument(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_SR_NO_PO_DOC_URL}`, salesQuotationDocument);
}


getCreatePoDetailsForSearchPo(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_GET_CREATE_PO_DETAILS_URL}`);
}

sendDocNoForGettingCreatePoDetails(salesQuotationDocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_GET_CREATE_PO_DETAILS_URL}`, salesQuotationDocument);
}
deleteRowForSrNo(id: number): Observable<Object> {
  return this.http.post(`${SystemConstants.DELETE_PO_ROW_FOR_SR_NO_URL}`,id);
}

}