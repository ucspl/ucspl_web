import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class CreateReqAndInward {

  id: number;
  reqInwDocumentNo: string;
  inwardReqNo: string;
  inwardType: number;
  inwardDate: string;
  reqNoCounter:number;
  customerName: number;
  shippedTo: number;
  endDate: string
  reportInNameOf: number
  countryPrefixForMobile1: string;
  customerDCNo: string
  dcDate: string
  expectedDeliveryDate: string
  calibratedAt: string
  labLocation: number
  noOfInstruments: number
  reqContactName: string
  reqContactPhone: string
  reqEmail: string
  reqType: string
  checkedBy: string;
  createdBy: string;
  modifiedBy: string;
  dateCreated: any;
  dateModified: any;
  assignedTo: string;
  typeOfDocument: string;
  draft: number;
  approved: number;
  archieved: number;
  submitted: number;
  rejected: number;
  multiParameterFlag:number;
 

}

export class InwardTypeOfRequestAndInward {
  inwardTypeId: number;
  inwardTypeName: string;
  description:string;
  status:number;
  createdBy:string;
  dateCreated:any;
}

export  class FormulaAccuracyReqAndIwd {

	formulaAccuracyId: number;
	formulaAccuracyName: string;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: string;
}

// export class InwardTypeOfRequestAndInward {
//   inwardTypeId: number;
//   inwardTypeName: string;
//   description:string;
//   status:number;
//   createdBy:string;
//   dateCreated:any;
// }

export class ReqTypeOfRequestAndInward {
  reqTypeId: number;
  reqTypeName: string;
}

export class InstrumentListForRequestAndInward {
  instrumentId: number;
  instrumentName: string;
  parameterId: string;
}

export class ParameterListForRequestAndInward {
  parameterId: number;
  parameterName: string;
  frequency:number;
  description: string;
	status: number;
	createdBy: string;
	dateCreated: string;
}

export  class UomListForRequestAndInward {

	uomId: number;
	uomName: string;
  parameterId: number;
	description: string;
	status: number;
	createdBy: string;
	dateCreated: string;
}

export class DynamicGrid {
  srNo: string;
  inwardInstruNo: string;
  nomenclature: string;
  description: string;
  rangeSize: string;
  rangeFrom: number;
  rangeTo: number;
  leastCount: number;
  idNo: string;
  make: string;
  calibrationFrequency: string;
  instruType: string;
  assignedTo: string;
  custInstruIdentificationId:number;
}




//////////////  request inward document  /////////////////////

export class RequestAndInwardDocument {

  id: number;
  custInstruIdentificationId:number;
  createReqAndIwdTableId: number;
  reqInwDocumentNo: string;
  inwardReqNo: string;
  srNo: string;
  inwardInstrumentNo: string;
  parameterNumber: string;
  nomenclature: number;
  parameter: number;
  rangeSize: string;
  make: string;
  idNo: string;
  frequencyCondition: string;
  instrumentType: string;
  rangeFrom: string;
  rangeTo: string;
  ruom:number;
  leastCount: any;
  lcuom1: number;
  accuracy: string;
  formulaAccuracy: number;
  uomAccuracy: number;
  accuracy1: string;
  formulaAccuracy1: number;
  uomAccuracy1: number;
  location: string;
  calibrationFrequency: string;
  labType: string;
	dateOfCalibration: string;
	dateOfIssueCertificate: string;
	receivedDate: string;
  certificateNo: string;
	orderNo: string;
	certificatePrintMode: string;
	remark: string;
	calibrationDueOn: string;
	calibratedBy: string;
	authorizedBy: string;
	reminderDate: string;
	reminderDateMonth: string;
  instruCondition:string;
  assignedTo: string;
  statusOfOrder: string;
  active:boolean;
  calibrationTag:number;
  isMultiPara:number;
}

export class CustomerInstrumentIdentification{
  custInstruIdentificationId:number;
	reqDocId:number;
	custId:number;
	nomenclatureId:number;
	idNo:string;
	srNo:string;
	checkedBy:string;
	createdBy:string;
	modifiedBy:string;
	dateCreated:string;
	dateModified:string;
}

export class ReqDocnoAndCustInstruRelationship {
	
	reqDocnoAndCustInstruId:number;
	reqDocId:number;
	custInstruIdentificationId:number;
	createReqAndIwdTableId:number;
	reqInwDocumentNo:string;
	inwardReqNo:string;
	inwardInstrumentNo:string;	
	parameterNumber:string;
  singleParameter:number;
	multipleParameter:number;
	createdBy:string;
  calibrationTag:number;
}

export class DynamicGridReqdocParaDetails {

  inwardInstruNo: string;
  nomenclature: string;
  statusOfOrder: string;
  parameter:string;
  parameterNumber: string;
  idNo: string;
  make: string;
  srNo: string;
  rangeFrom: string;
  rangeTo: string;
  ruom: number;
  leastCount: any;
  lcuom1: number;
  accuracy: string;
  formulaAccuracy: number;
  uomAccuracy: number;
  accuracy1: string;
  formulaAccuracy1: number;
  uomAccuracy1:number;
  location: string;
  calibrationFrequency: string;
  instruType: string;
  labType: string;
  active:boolean; //delete if error occurs
	custInstruIdentificationId:number;
}


export class DynamicGridReqdocParaDetailsForCalibrationTag {

  inwardInstruNo: string;
  nomenclature: string;
  statusOfOrder: string;
  parameter:string;
  parameterNumber: string;
  idNo: string;
  make: string;
  srNo: string;
  rangeFrom: number;
  rangeTo: number;
  ruom: number;
  leastCount: any;
  lcuom1: number;
  accuracy: number;
  formulaAccuracy: number;
  uomAccuracy: number;
  accuracy1: number;
  formulaAccuracy1: number;
  uomAccuracy1:number;
  location: string;
  calibrationFrequency: string;
  instruType: string;
  labType: string;
  active:boolean; //delete if error occurs
	custInstruIdentificationId:number;
  calibrationTag:number;
}

///////// Multi parameter document //////////////////////

export class DynamicGridReqdocMultiParaDetails {

  inwardInstruNo: string;
  nomenclature: string;
  parameter: string;
  parameterNumber: number;
  idNo: string;
  make: string;
  srNo: number;
  rangeFrom: number;
  rangeTo: number;
  ruom: string;
  leastCount: number;
  lcuom1: string;
  accuracy: number;
  formulaAccuracy: string;
  uomAccuracy: string;
  accuracy1: number;
  formulaAccuracy1: string;
  uomAccuracy1: string;
  location: string;
  calibrationFrequency: string;
  instruType: string;
  labType: string;
}




@Injectable()
export class CreateReqAndInwardService {

  reqAndInwardDataObj: CreateReqAndInward = new CreateReqAndInward();
  countryFlagForMobile: any;
  dynamicArrayTrial: Array<DynamicGrid> = [];
  inwardReqNo: any;
  constructor(private http: HttpClient) { }

  getReqAndInward(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_REQUEST_AND_INWARD}/${id}`);
  }

  createReqAndInward(ReqAndInward: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_REQUEST_AND_INWARD}`, ReqAndInward);
  }  

  updateReqAndInward(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_REQUEST_AND_INWARD}/${id}`, value); 
  }

  updateReqAndInwardForCerti(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.REQUEST_INWARD_DOCUMENT_FOR_UPDATECRECERTIURL}/${id}`, value); 
  }

  deleteReqAndInward(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_REQUEST_AND_INWARD}/${id}`, { responseType: 'text' });
  }

  getReqAndInwardList(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_REQUEST_AND_INWARD}`);
  }

  getChildCustomers(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_CHILD_CUSTOMERS}`);
  }

  getInwardTypesOfRequestAndInwards(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_REQUEST_AND_INWARD_TYPES}`);
  }

  getReqTypeOfRequestAndInwards(): Observable<any> {
    return this.http.get(`${SystemConstants.GET_REQ_TYPES_OF_REQUEST_AND_INWARDS}`);
  }

  getFormulaAccuracyListOfRequestAndInwards(): Observable<any> {
    return this.http.get(`${SystemConstants.FORMULA_ACCURACY_LIST_REQ_IWD_URL}`);
  }

  getParameterListOfRequestAndInwards(): Observable<any> {
    return this.http.get(`${SystemConstants.PARAMETER_LIST_REQ_IWD_URL}`);
  }

  getUomListOfRequestAndInwards(): Observable<any> {
    return this.http.get(`${SystemConstants.UOM_LIST_REQ_IWD_URL}`);
  }


///////////// customer instrument identification /////////////////////
  getCustInstruIdentification(): Observable<any> {
    return this.http.get(`${SystemConstants.CUSTOMER_INSTRUMENT_IDENTIFICATION_URL}`);
  }
  getCustomerInstrumentIdentificationByReqIwdId(id: number):Observable<any>{
    return this.http.get(`${SystemConstants.CUSTOMER_INSTRUMENT_IDENTIFICATION_URL}/${id}`);
  }
  createCustInstruIdentification(custInstru: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CUSTOMER_INSTRUMENT_IDENTIFICATION_URL}`, custInstru);
  }
  updateCustInstruIdentification(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CUSTOMER_INSTRUMENT_IDENTIFICATION_URL}/${id}`, value);
  }


///////////// request doc no customer instru relation identification /////////////////////
  getReqDocNoCustInstruRelationship(): Observable<any> {
    return this.http.get(`${SystemConstants.REQ_DOC_CUSTOMER_INSTRU_RELATION_URL}`);
  }
  getReqDocNoCustInstruRelationshipById(id: number):Observable<any>{
    return this.http.get(`${SystemConstants.REQ_DOC_CUSTOMER_INSTRU_RELATION_URL}/${id}`);
  }
  createReqDocNoCustInstruRelationship(custInstru: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.REQ_DOC_CUSTOMER_INSTRU_RELATION_URL}`, custInstru);
  }
  updateReqDocNoCustInstruRelationship(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.REQ_DOC_CUSTOMER_INSTRU_RELATION_URL}/${id}`, value);
  }

  getReqDocNoCustInstruRelationshipByCustInstruIdentiId(value): Observable<any> {
    return this.http.post(`${SystemConstants.SP_FOR_REQ_DOC_CUSTOMER_INSTRU_RELATION_BY_CUSTINSTRU_IDENTIFICATION_ID}`, value);
  }

  getReqDocNoCustInstruRelationshipByCreateReqIwdTableId(value): Observable<any> {
    return this.http.post(`${SystemConstants.SP_FOR_REQ_DOC_CUSTOMER_INSTRU_RELATION_BY_CREATE_REQ_IWD_TABLE_ID}`, value);
  }


  


///////// set and get data of created request and inward /////////
  setReqAndInwDataFromCreateReq(reqInwData) {
    this.reqAndInwardDataObj = reqInwData;
  }

  getReqAndInwDataFromCreateReq() {
    return this.reqAndInwardDataObj;
  }



////////////////  req and inward document number /////////////////
  getReqInwDocumentNo(value): Observable<any> {
    return this.http.post(`${SystemConstants.DOC_NO_FOR_REQUEST_INWARD_URL}`, value);
  }

  getCounterReqInwDocumentNo(value): Observable<any> {
    return this.http.post(`${SystemConstants.SP_COUNTER_DOC_NO_FOR_REQUEST_INWARD__URL}`, value);
  }



///////// set and get inwardReqNo of created request and inward /////////
  setInwardReqNoFromReqDoc(reqInwData) {
    this.inwardReqNo = reqInwData;
  }

  getinwardReqNoFromReqDoc() {
    return this.inwardReqNo;
  }

/////////////// request and inward document /////////////////////

  getPhoneNoCountryFlag() {
    return this.countryFlagForMobile;
  }
  setPhoneNoCountryFlag(countryFlag) {
    this.countryFlagForMobile = countryFlag;
  }

  getInstrumentList(): Observable<any> {
    return this.http.get(`${SystemConstants.INSTRUMENT_LIST_URL}`);
  }

  getsingleparalisttrial() {
    return this.dynamicArrayTrial;
  }

  setSingleParaListTrial(trial) {
    this.dynamicArrayTrial = trial;
  }

  getReqAndInwardDocument(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.REQUEST_INWARD_DOCUMENT_URL}/${id}`);
  }

  createReqAndInwardDocument(ReqAndInward: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.REQUEST_INWARD_DOCUMENT_URL}`, ReqAndInward);
  }

  updateReqAndInwardDocument(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.REQUEST_INWARD_DOCUMENT_URL}/${id}`, value);
  }

  deleteReqAndInwardDocument(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.REQUEST_INWARD_DOCUMENT_URL}/${id}`, { responseType: 'text' });
  }

  getReqAndInwardDocumentList(): Observable<any> {
    return this.http.get(`${SystemConstants.REQUEST_INWARD_DOCUMENT_URL}`);
  }




  ////////////////////////// store procedure call for sr no with doc no for req screen ///////////////////////////////

  getSrNoInstruListForReqIwd(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL}`);
  }

  sendSrNoInstruListForReqIwd(reqIwdDocumentNo: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL}`, reqIwdDocumentNo);
  }

  getSrNoInstruIdenListForReqIwd(): Observable<any> {
    return this.http.get(`${SystemConstants.SP_TRIAL_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL}`);
  }

  sendSrNoInstruIdenListForReqIwd(reqIwdDocumentNo: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.SP_TRIAL_SR_NO_REQ_IWD_INSTRU_LIST_SCREEN_URL}`, reqIwdDocumentNo);
  }


  // getSrNoInstruListWithNamesForReqIwd(): Observable<any> {
  //   return this.http.get(`${SystemConstants.SP_SR_NO_REQ_IWD_INSTRU_LIST_WITH_NAMES_SCREEN_URL}`);
  // }

  // getSrNoForDocNoWithNamesReqByInstruNo(): Observable<any> {
  //   return this.http.get(`${SystemConstants.SP_SR_NO_REQ_IWD_INSTRU_LIST_WITH_NAMES_BY_INSTRUNO_SCREEN_URL}`);
  // }



 ////////////////  store procedure for getting multi parameters  /////////////////
  getMultiParaDetails(value): Observable<any> {
      return this.http.post(`${SystemConstants.SP_MULTIPARA_DETAILS_URL}`, value);
  } 


  /////////////////// store procedure to get cust instru id through reqDocNo ////////////////////////

  getCustInstruIdForReqDocNo(value): Observable<any> {
    return this.http.post(`${SystemConstants.SP_GET_CUSTINSTRUID_FOR_REQDOCNO_URL}`, value);
} 

}
