
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';


export class CreateDeliveryChallan{   

  id:number;
  documentTypeId: number;
  dcDocumentNumber: string;
  requestNumber:string;
  customerDcNumber:string;
  branchId:number;
  invoiceTypeId:number; 
  //documentDate:any;
  custShippedTo:number;
 // custBilledTo:number;
  invoiceDate:any;
  poDate:any;
  dcDate:any;
  poNo:string;
  dcNo:string;
  vendorCodeNumber:string;
  archieveDate:any;
  createdBy:string;
  draft:number;
  approved:number;
  submitted:number;
  archieved:number;
  rejected:number;
  remark:string;
  documentType: any;
  invoiceType: any;
  dateOfInvoice: any;
  dispatchMode:string;
  modeOfSubmission:string;
  totalQuantity:number;
  
}

export class SrNoDeliveryChallan{


   id:number;
   deliChallanDocumentNumber:string;
   deliChallanNumber:string;;
   srNo:number;
   itemOrPartCode:string;
   instruId:number;
   description:string;
   rangeFrom:string;
   rangeTo:string;
   sacCode:string;
   hsnNo:string;
   quantity:number;
   uniqueNo:string;

}


@Injectable()
export class CreateDeliChallanService {
  CustomeridforDeliChallan:any;
  dataOfCreateDeliveryChallan: any;

  constructor(private http: HttpClient) { }

  //////////////////////////// Get last saved document number //////////////////////

  documentNumberForCreateDeliveryChallan(): Observable<any>{  
    //   debugger;    
       return this.http.get(`${SystemConstants.DOC_NO_FOR_DELIVERY_CHALLAN_URL}`);    
  } 


  //save update delete
  getDeliveryChallan(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_DELIVERY_CHALLAN_URL}/${id}`);
  }

  createDeliveryChallan(createChallanCumInvoice: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_DELIVERY_CHALLAN_URL}`, createChallanCumInvoice);
  }

  updateDeliveryChallan(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_DELIVERY_CHALLAN_URL}/${id}`, value);
  }

  deleteDeliveryChallan(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_DELIVERY_CHALLAN_URL}/${id}`, { responseType: 'text' });
  }

//////////////////////////////////////////////////

  /////////////////////// Get and set customer id for delivery challan////////////////

  getCustomerNameForDeliveryChallan()
  {
     return this.CustomeridforDeliChallan;
  }

  setCustomerNameForDeliveryChallan(Customerid)
  {
     this.CustomeridforDeliChallan=Customerid;
  }
  setCreateDeliveryChallanData(data){
    this.dataOfCreateDeliveryChallan=data;

  }
  getCreateDeliveryChallanData(){
    return this.dataOfCreateDeliveryChallan;

  }

  //////////////////////save sr no delivery challan data /////////////////////////
  
  getSrNoDeliveryChallanDocumentForId(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_SR_NO_DELI_CHALLAN_URL}/${id}`);
  }

  createSrNoDeliveryChallanDocument(ChallanCumInvoiceDoc: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_SR_NO_DELI_CHALLAN_URL}`, ChallanCumInvoiceDoc);
  }

  updateSrNoDeliveryChallanDocument(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_SR_NO_DELI_CHALLAN_URL}/${id}`, value);
  }
  updateSrNoDeliveryChallanDocumentForUniqueNo(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.UPDATE_UNIQUE_NO_FOR_DELI_CHALLAN}/${id}`, value);
  }

  deleteSrNoDeliveryChallanDocument(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_SR_NO_DELI_CHALLAN_URL}/${id}`, { responseType: 'text' });
  }


  /////////////////store procedure with different tags///////////////////////////////////
  getReqAndInwDataWithCalibrationDcAndInvoiceTagForCustId(value): Observable<any> {
    return this.http.post(`${SystemConstants.SP_REQ_AND_INW_DATA_FOR_CALIBRATION_DC_AND_INVOICE_TAG_FOR_CUST_ID_URL}`, value);
  } 
  // getReqAndInwDataWithCalibrationDcAndInvoiceTagForReqDocNo(value): Observable<any> {
  //   return this.http.post(`${SystemConstants.SP_REQ_AND_INW_DATA_FOR_CALIBRATION_DC_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL}`, value);
  // } 
  
  trialgetReqAndInwDataWithCalibrationDcAndInvoiceTagForReqDocNo(value: Array<string>): Observable<Array<string>> {
  //  let headers = new Headers({ 'Content-Type': 'application/json'} );
   // let options = new RequestOptions({ headers: headers}); 
      
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
        })
      };
      
    let body = '{}';
  
    let response = this.http.post(`${SystemConstants.TRIAL_SP_REQ_AND_INW_DATA_FOR_CALIBRATION_DC_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL}`, value, httpOptions)
    .pipe(
      tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
      
            return response;
  } 
  
  private handleError (error: any) {
    let errMsg = error.message || 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
 }

 sendDocNoForGettingDeliveryChallanDataWithInvoiceTag(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_SEND_DOC_NO_FOR_GETTING_DELIVERY_CHALLAN_DATA}`, value);
} 

getSrNoDataForDeliChallanDocumentNo(challaninvoicedocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_SR_NO_DELI_CHALLAN_DOC_URL}`, challaninvoicedocument);
}


/********************************************set dc tag***********************************/


trialSetDcTagToOneForInstru(value: string): Observable<Array<string>> {
  
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
        })
      };
      
    let body = '{}';
  
    let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_INSTRU_URL}`,value, httpOptions)
    .pipe(
      tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
      
            return response;
  } 

  trialSetDcTagToOneForInstruThroughInvId(value: string): Observable<Array<string>> {
    //  let headers = new Headers({ 'Content-Type': 'application/json'} );
     // let options = new RequestOptions({ headers: headers}); 
        
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
          })
        };
        
      let body = '{}';
    
      let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_INSTRU_THROUGH_INVID_URL}`,value, httpOptions)
      .pipe(
        tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
        
              return response;
    } 


  trialSetDcTagToZeroForInstru(value: string): Observable<Array<string>> {
    //  let headers = new Headers({ 'Content-Type': 'application/json'} );
     // let options = new RequestOptions({ headers: headers}); 
        
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
          })
        };
        
      let body = '{}';
    
      let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_DC_TAG_TO_ZERO_FOR_INSTRU_URL}`,value, httpOptions)
      .pipe(
        tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
        
              return response;
    } 

    // set invoice tag for range
    trialSetDcTagToOneForRange(value: string): Observable<Array<string>> {
      //  let headers = new Headers({ 'Content-Type': 'application/json'} );
       // let options = new RequestOptions({ headers: headers}); 
          
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
            })
          };
          
        let body = '{}';
      
        let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_DC_TAG_TO_ONE_FOR_RANGE_URL}`,value, httpOptions)
        .pipe(
          tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
          
                return response;
      } 
    
      trialSetDcTagToZeroForRange(value: string): Observable<Array<string>> {
        //  let headers = new Headers({ 'Content-Type': 'application/json'} );
         // let options = new RequestOptions({ headers: headers}); 
            
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
              })
            };
            
          let body = '{}';
        
          let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_DC_TAG_TO_ZERO_FOR_RANGE_URL}`,value, httpOptions)
          .pipe(
            tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
            catchError(this.handleError)
          );
            
                  return response;
        } 

}
