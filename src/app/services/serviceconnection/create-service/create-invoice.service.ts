import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { SystemConstants } from '../../../system-constants';

export class DynamicGrid{     

  srNo : number;
  poSrNO:string;
  itemOrPartCode:string;
  instruName :string;
  description: string;
  range: string;
  rangeFrom:string;
  rangeTo:string;
  sacCode: string;
  hsnNo: string;
  quantity:number;
  editQuant:number;
  rate: number;
  discountOnItem: number;
  unitPrice:number;
  amount : number;
  uniqueId:number;
  fullReqNo:string;
  inwReqNo:string;
  uniqueNo:string;
  reqDocId:number;
  invoiceTag:number;

}

export class CreateChallanCumInvoice{   

  id:number;
  documentTypeId: number;
  invDocumentNumber: string;
  requestNumber:string;
  customerDcNumber:string;
  branchId:number;
  invoiceTypeId:number; 
  //documentDate:any;
  custShippedTo:number;
  custBilledTo:number;
  invoiceDate:any;
  poDate:any;
  dcDate:any;
  poNo:string;
  dcNo:string;
  vendorCodeNumber:string;
  archieveDate:any;
  createdBy:string;
  draft:number;
  approved:number;
  submitted:number;
  archieved:number;
  rejected:number;
  remark:string;
  documentType: any;
  invoiceType: any;
  dateOfInvoice: any;
}

export class ChallanInvoiceDocument{
  id:number;
  invDocumentNumber:string;
  invoiceNumber:string;
  srNo:number;
  poSrNo:string;
  itemOrPartCode:string;
  instruId:number;
  description:string;
  rangeFrom:string;
  rangeTo:string;
  sacCode:string;
  hsnNo: string;
  quantity:number;
  unitPrice:number;
  amount:number;
  fullReqNo:string;
  inwReqNo:string;
  uniqueNo:string;

}

export class ChallanInvoiceDocCalculation{
  id:string;
  invDocumentNumber:string;
  subTotal:number;
  total:number;
  discountOnSubtotal:number;
  cgst:number;
  sgst:number;
  igst:number;
  net:number;
}

export class InvoiceAndrequestNumberJunction{
   invoiceRequestNoJunctionId:number;
	 requestNumber:string;
   invoiceDocNo:string;
    }



@Injectable()
export class CreateInvoiceService {

  CustomeridforChallan:any;
  createChallanCumInvoiceobj:CreateChallanCumInvoice=new CreateChallanCumInvoice();


  constructor(private http: HttpClient) { }

  getFinancialYearDates(): Observable<any> {
    return this.http.get(`${SystemConstants.FINANCIAL_YEAR_DATE_URL}`);
  }

  getDocumentTypeListForInvoice(): Observable<any> {
    return this.http.get(`${SystemConstants.DOCUMENT_TYPE_FOR_INVOICE_URL}`);
  }
  getInvoiceTypeListForInvoice(): Observable<any> {
    return this.http.get(`${SystemConstants.INVOICE_TYPE_FOR_INVOICE_URL}`);
  }

  getCreateChallanCumInvoice(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_INVOICE_URL}/${id}`);
  }

  createCreateChallanCumInvoice(createChallanCumInvoice: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_INVOICE_URL}`, createChallanCumInvoice);
  }

  updateCreateChallanCumInvoice(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_INVOICE_URL}/${id}`, value);
  }

  deleteCreateChallanCumInvoice(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_INVOICE_URL}/${id}`, { responseType: 'text' });
  }


  /////////////////////// Getting customer name from search cust form to create challan cum invoice////////////////

  getCustomerNameForCreatingChallan()
  {
     return this.CustomeridforChallan;
  }

  setCustomerNameForCreatingChallan(Customerid)
  {
     this.CustomeridforChallan=Customerid;
  }

  //////////////////////////// Get document number //////////////////////

  documentNumberForCreateChallanCumInvoice(): Observable<any>{  
    //   debugger;    
       return this.http.get(`${SystemConstants.DOC_NO_FOR_CHALLAN_CUM_INVOICE_URL}`);    
  } 


  ////////////////set data for create-invdoc component ///////////////////////

  getCreateChallanCumInvoiceDocument()
  {
    return this.createChallanCumInvoiceobj;
  }

  setCreateChallanCumInvoiceDocument(createChallanCumInvoice: CreateChallanCumInvoice)
  {
     this.createChallanCumInvoiceobj=createChallanCumInvoice;
  }



  ////////////////// challan cum invoice document /////////////////////////////////

  getChallanCumInvoiceDocument(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.INVOICE_DOCUMENT_CREATE_URL}/${id}`);
  }

  createChallanCumInvoiceDocument(ChallanCumInvoiceDoc: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.INVOICE_DOCUMENT_CREATE_URL}`, ChallanCumInvoiceDoc);
  }

  updateChallanCumInvoiceDocument(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.INVOICE_DOCUMENT_CREATE_URL}/${id}`, value);
  }
  updateChallanCumInvoiceDocumentForUniqueNo(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.UPDATE_UNIQUE_NO_FOR_INVOICE}/${id}`, value);
  }

  deleteChallanCumInvoiceDocument(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.INVOICE_DOCUMENT_CREATE_URL}/${id}`, { responseType: 'text' });
  }

  ////////////////// challan cum invoice document calculation /////////////////////////////////

  getChallanCumInvoiceDocumentCalculation(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.INVOICE_DOCUMENT_CALCULATION_URL}/${id}`);
  }

  createChallanCumInvoiceDocumentCalculation(ChallanCumInvoiceDocCal: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.INVOICE_DOCUMENT_CALCULATION_URL}`, ChallanCumInvoiceDocCal);
  }

  updateChallanCumInvoiceDocumentCalculation(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.INVOICE_DOCUMENT_CALCULATION_URL}/${id}`, value);
  }

  deleteChallanCumInvoiceDocumentCalculation(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.INVOICE_DOCUMENT_CALCULATION_URL}/${id}`, { responseType: 'text' });
  }


////////////////////////// store procedure call for sr no with doc no for challan invoice document screen ///////////////////////////////

getSrNoForChallanCumInvoiceDocument(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_SR_NO_INVOICE_DOC_URL}`);
}

sendDocNoOfChallanCumInvoiceDocument(challaninvoicedocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_SR_NO_INVOICE_DOC_URL}`, challaninvoicedocument);
}


/////////////////////// store procedure call for calculations with doc no for challan invoice document screen ///////////////////////////////
getDocDetailsForChallanCumInvoiceDocumentCalculation(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_INC_DOCUMENT_CAL_URL}`);
}

sendDocNoOfChallanCumInvoiceDocumentCalculation(salesquotationdocument: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_INC_DOCUMENT_CAL_URL}`, salesquotationdocument);
}


deleteRowForInvoiceSrNo(id: number): Observable<Object> {
  return this.http.post(`${SystemConstants.DELETE_INVOICE_ROW_FOR_SR_NO_URL}`,id);
}

/////////////////// store procedure to get cust instru id through reqDocNo ////////////////////////

// getReqAndInwDataWithCalibrationTagForReqDocNo(value): Observable<any> {
//   return this.http.post(`${SystemConstants.SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_URL}`, value);
// } 
getPoDataForPoNo(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_PO_DATA_FOR_PO_NO_URL}`, value);

} 

getReqAndInwDataWithCalibrationTagAndInvoiceTagForCustId(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_CUST_ID_URL}`, value);
} 
getReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL}`, value);
} 

trialgetReqAndInwDataWithCalibrationTagAndInvoiceTagForReqDocNo(value: Array<string>): Observable<Array<string>> {
//  let headers = new Headers({ 'Content-Type': 'application/json'} );
 // let options = new RequestOptions({ headers: headers}); 
    
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
      })
    };
    
  let body = '{}';

  let response = this.http.post(`${SystemConstants.TRIAL_SP_REQ_AND_INW_DATA_FOR_CALIBRATION_TAG_AND_INVOICE_TAG_FOR_REQ_DOC_NO_URL}`, value, httpOptions)
  .pipe(
    tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
    catchError(this.handleError)
  );
    
          return response;
} 




trialSetInvoiceTagToOneForInstru(value: string): Observable<Array<string>> {
  //  let headers = new Headers({ 'Content-Type': 'application/json'} );
   // let options = new RequestOptions({ headers: headers}); 
      
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
        })
      };
      
    let body = '{}';
  
    let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_INSTRU_URL}`,value, httpOptions)
    .pipe(
      tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
      
            return response;
  } 

  trialSetInvoiceTagToOneForInstruThroughInvId(value: string): Observable<Array<string>> {
    //  let headers = new Headers({ 'Content-Type': 'application/json'} );
     // let options = new RequestOptions({ headers: headers}); 
        
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
          })
        };
        
      let body = '{}';
    
      let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_INSTRU_THROUGH_INVID_URL}`,value, httpOptions)
      .pipe(
        tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
        
              return response;
    } 


  trialSetInvoiceTagToZeroForInstru(value: string): Observable<Array<string>> {
    //  let headers = new Headers({ 'Content-Type': 'application/json'} );
     // let options = new RequestOptions({ headers: headers}); 
        
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
          })
        };
        
      let body = '{}';
    
      let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_INVOICE_TAG_TO_ZERO_FOR_INSTRU_URL}`,value, httpOptions)
      .pipe(
        tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
        
              return response;
    } 

    // set invoice tag for range
    trialSetInvoiceTagToOneForRange(value: string): Observable<Array<string>> {
      //  let headers = new Headers({ 'Content-Type': 'application/json'} );
       // let options = new RequestOptions({ headers: headers}); 
          
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
            })
          };
          
        let body = '{}';
      
        let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_INVOICE_TAG_TO_ONE_FOR_RANGE_URL}`,value, httpOptions)
        .pipe(
          tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
          
                return response;
      } 
    
      trialSetInvoiceTagToZeroForRange(value: string): Observable<Array<string>> {
        //  let headers = new Headers({ 'Content-Type': 'application/json'} );
         // let options = new RequestOptions({ headers: headers}); 
            
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
              })
            };
            
          let body = '{}';
        
          let response = this.http.post(`${SystemConstants.TRIAL_SP_SET_INVOICE_TAG_TO_ZERO_FOR_RANGE_URL}`,value, httpOptions)
          .pipe(
            tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
            catchError(this.handleError)
          );
            
                  return response;
        } 



private handleError (error: any) {
   let errMsg = error.message || 'Server error';
   console.error(errMsg); // log to console instead
   return Observable.throw(errMsg);
}


getAllPoNumbersForCustId(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_GET_ALL_PO_NUMBERS_FOR_CUST_ID}`, value);
} 

// save all request numbers 
saveReuestNumbers(requestNumbers: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SAVE_REQUEST_NUMBER_FOR_INVOICE}`, requestNumbers);
}

getAllRequestNumbersForInvoiceDocNumber(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_GET_ALL_REQ_NUMBERS_FOR_INVOICE_DOC_NO}`, value);
} 
sendDocNoForGettingInvoiceData(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_SEND_DOC_NO_FOR_GETTING_INVOICE_DATA}`, value);
} 

removeAllRequestNumbersForInvDocNumber(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_REMOVE_ALL_REQ_NO_FOR_INV_DOC_NO}`, value);
} 
removeAllSrNoForInvDocNumber(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_REMOVE_ALL_SR_NO_FOR_INV_DOC_NO}`, value);
} 

setInvoiceTagAndUniqueNoToZeoForInvDocNumber(value): Observable<any> {
  return this.http.post(`${SystemConstants.SP_SET_INV_TAG_AND_UNI_NO_TO_ZERO_FOR_INV_DOC_NO}`, value);
} 

setInvoiceTagToZeroForRequestNoAndInvNo(value: string): Observable<Array<string>> {
  
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
        })
      };
      
    let body = '{}';
  
    let response = this.http.post(`${SystemConstants.SP_INVOICE_TAG_TO_ONE_FOR_REQ_NO_AND_INV_NO_URL}`,value, httpOptions)
    .pipe(
      tap(data => console.log('getIncCompany: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
      
            return response;
  } 






}



