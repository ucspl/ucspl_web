
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class User {

  id: number;
  firstName: string;
  middleName:string;
  lastName: string;
  gender:number;
  status:number;
  customer:number;
  role:number;
  department:number;
  email:string;
  countryPrefixForMobile:string;
  phoneNo:string;
  selectBranch:number;
  userNumber:number;
  password:string;
  userName:string;
  securityQuestionId:number;
  securityAnswer:string; 
  createdBy:string;
  dateCreated:any;
  modifiedBy:string;
         
}

export class DocumentType {

  documentId: number;
  documentName: string;
}
export class DynamicGrid{     
  name:string;  
  email:string;  
  phone:string;  
}

@Injectable()
export class CreateUserService {

  //private baseUrl = 'http://localhost:8090/api/v1/users';
  //private baseUrlForBranch = 'http://localhost:8090/api/v1/branch';
  //private baseUrlForDepartment = 'http://localhost:8090/api/v1/department';
 // private baseUrlForCustomer = 'http://localhost:8090/api/v1/customerlist';
  //private baseUrlForRole = 'http://localhost:8090/api/v1/role';
  //private baseUrlForDocument = 'http://localhost:8090/api/v1/document';
  
  constructor(private http: HttpClient) { }


  getUser(id: number): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_USER_URL}/${id}`);
  }

  createUser(user: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_USER_URL}`, user);
  }

  updateUser(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_USER_URL}/${id}`, value);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${SystemConstants.CREATE_USER_URL}/${id}`, { responseType: 'text' });
  }

  getUserList(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_USER_URL}`);
  }

  getBranchList(): Observable<any> {
    return this.http.get(`${SystemConstants.BRANCH_URL}`);
  }
  getDepartmentList(): Observable<any> {
    return this.http.get(`${SystemConstants.DEPARTMENT_URL}`);
  }

  getCustomerList(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_CUSTOMER}`);
  }

  getSecurityQuestionList(): Observable<any> {
    return this.http.get(`${SystemConstants.Security_Question_List_URL}`);
  }

  getRoleList(): Observable<any> {
    return this.http.get(`${SystemConstants.ROLE_URL}`);
  }

  getDocumentList(): Observable<any> {
    return this.http.get(`${SystemConstants.MANAGE_APPROVERS_DOCUMENT_URL}`);
  }

  forgotPassword(user: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.FORGOT_PASSWORD_URL}`, user);
  }

  ///////////////////////////// get info for search user from create user table /////////

  getCreateUserDetailsForSearchUser(): Observable<any> {
  return this.http.get(`${SystemConstants.SP_GET_CREATE_USERS_DETAILS_URL}`);
}

sendUserNoForGettingCreateUserDetails(objectForUserNumber: Object): Observable<Object> {
  return this.http.post(`${SystemConstants.SP_GET_CREATE_USERS_DETAILS_URL}`, objectForUserNumber);
}

}
