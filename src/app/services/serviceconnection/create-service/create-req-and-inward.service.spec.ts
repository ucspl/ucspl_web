import { TestBed, inject } from '@angular/core/testing';

import { CreateReqAndInwardService } from './create-req-and-inward.service';

describe('CreateReqAndInwardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateReqAndInwardService]
    });
  });

  it('should be created', inject([CreateReqAndInwardService], (service: CreateReqAndInwardService) => {
    expect(service).toBeTruthy();
  }));
});
