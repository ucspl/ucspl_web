import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SystemConstants } from '../../../system-constants';

export class DefinitionCategory {

  categoryId: number;
  categoryName: string;
}
// export class CreateDefinition {
//   id: number;
//   categoryId:number;
//   type:string;
//   description:string;
//   name:string;
//   status:number;  
//   dateCreated:any;
//   createdBy:string;
// }
export class Branch {

  branchId: number;
  branchName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class Role {
  roleId: number;
  roleName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class Department {

  departmentId: number;
  departmentName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class SecurityQuestion {

  securityQuestionId: number;
  securityQuestionName: string;
  // description:string;
  status: number;
  createdBy: string;
  dateCreated: any;
}

export class Country {
  countryId: number;
  countryName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}


export class RegionAreaCode {
  regionAreaCodeId: number;
  regionAreaCodeName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class BillingCreditPeriod {

  billingCreditPeriodId: number;
  billingCreditPeriodName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}

export class TaxpayerTypeForCustomer {
  taxpayerTypeId: number;
  taxpayerTypeName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class TypeOfQuotationForSalesAndQuotation {
  docTypeId: number;
  docTypeName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class ReferenceForSalesQuotation {

  referenceId: number;
  referenceName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class Instrument {
  instrumentId: number;
  instrumentName: string;
  parameterName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class Terms {
  termsId: number;
  termsName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class SupplierType {
  supplierTypeId: number;
  supplierTypeName: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}

export class InwardTypeOfRequestAndInward {
  inwardTypeId: number;
  inwardTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class ReqTypeOfRequestAndInward {
  reqTypeId: number;
  reqTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}

export class DocumentTypeForInvoice {
  documentTypeId: number;
  documentTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class InvoiceTypeForInvoice {
  invoiceTypeId: number;
  invoiceTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;
}
export class FinancialYear {
  id: number;
  fromDate: string;
  status: number;
  createdBy: string;
  // dateCreated:any;
}



/////////////////////////////////////////unc screen dropdown list //////////////////////////////////
export class UncMasterType {

  uncMasterTypeId: number;
  uncMasterTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncMasterNumber {

  uncMasterNumberId: number;
  uncMasterNumberName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncMasterName {

  uncMasterNameId: number;
  uncMasterName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncProbabilityDistribution {

  probabilityDistributionId: number;
  probabilityDistributionName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncSourceOfUncertainty {

  uncSourceOfUncertaintyId: number;
  uncSourceOfUncertaintyName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncType {

  uncTypeId: number;
  uncTypeName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncDividingFactor {

  uncDividingFactorId: number;
  uncDividingFactorName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}

export class UncDegreeOfFreedom {

  uncDegreeOfFreedomId: number;
  uncDegreeOfFreedomName: string;
  description: string;
  status: number;
  createdBy: string;
  dateCreated: any;

}




@Injectable()
export class CreateDefService {

  constructor(private http: HttpClient) { }

  getCategoryList(): Observable<any> {
    return this.http.get(`${SystemConstants.CATEGORY_URL}`);
  }
  createBranch(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_BRANCH_URL}`, branch);
  }
  createRole(role: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_ROLE_URL}`, role);
  }
  createDepartment(department: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_DEPARTMENT_URL}`, department);
  }
  createSecurityQuestion(securityQuestion: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_Security_Question_URL}`, securityQuestion);
  }
  createCountry(country: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_COUNTRY_URL}`, country);
  }
  createRegionAreaCode(regionAreaCode: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_REGION_AREA_CODE_URL}`, regionAreaCode);
  }
  createBillingCreditPeriod(billingCreditPeriod: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_BILLING_CREDIT_PERIOD_URL}`, billingCreditPeriod);
  }
  createTaxpayerType(taxpayerType: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_TAXPAYER_TYPE_URL}`, taxpayerType);
  }
  createTypeOfQuotation(typeOfQuotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_TYPE_OF_QUOTATION_URL}`, typeOfQuotation);
  }
  createReferenceNumberForQuotation(referenceNUmberForQuotation: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_REFERENCE_NUMBER_FOR_QUOTATION_URL}`, referenceNUmberForQuotation);
  }
  createInstrument(instrument: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_INSTRUMENT_URL}`, instrument);
  }
  createTerms(terms: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_TERMS_URL}`, terms);
  }
  createSupplierType(supplierType: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_SUPPLIER_TYPE_URL}`, supplierType);
  }
  createRequestAndInwardType(requestAndInwardType: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_INWARD_TYPE_URL}`, requestAndInwardType);
  }
  createRequestType(requestType: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_REQUEST_TYPE_URL}`, requestType);
  }
  createFinancialYear(financialYear: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_FINANCIAL_YEAR_URL}`, financialYear);
  }
  updateFinancialYear(id: number, value: any): Observable<Object> {
    return this.http.put(`${SystemConstants.CREATE_FINANCIAL_YEAR_URL}/${id}`, value);
  }

  createUOMType(uomType: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.UOM_LIST_REQ_IWD_URL}`, uomType);
  }
  createParameterType(parameterType: Object): Observable<Object> {

    return this.http.post(`${SystemConstants.PARAMETER_LIST_REQ_IWD_URL}`, parameterType);
  }



  /////////////////////////////////// unc screen dropdowns ////////////////////////////////////////////////////


  getUncMasterType(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_MASTER_TYPE_URL}`);
  }
  creatUncMasterType(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_MASTER_TYPE_URL}`, branch);
  }
  getUncMasterNumber(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_MASTER_NUMBER_URL}`);
  }
  createUncMasterNumber(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_MASTER_NUMBER_URL}`, branch);
  }
  getUncMasterName(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_MASTER_NAME_URL}`);
  }
  createUncMasterName(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_MASTER_NAME_URL}`, branch);
  }
  getUncProbabilityDistribution(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_PROBABILITY_DISTRIBUTION_URL}`);
  }
  createUncProbabilityDistribution(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_PROBABILITY_DISTRIBUTION_URL}`, branch);
  }
  getUncSourceOfUncertainty(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_SOURCE_OF_UNCERTAINTY_URL}`);
  }
  createUncSourceOfUncertainty(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_SOURCE_OF_UNCERTAINTY_URL}`, branch);
  }
  getUncType(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_TYPE_URL}`);
  }
  createUncType(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_TYPE_URL}`, branch);
  }
  getUncDividingFactor(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_DIVIDING_FACTOR_URL}`);
  }
  createUncDividingFactor(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_DIVIDING_FACTOR_URL}`, branch);
  }
  getUncDegreeOfFreedom(): Observable<any> {
    return this.http.get(`${SystemConstants.CREATE_UNC_DEGREE_OF_FREEDOM_URL}`);
  }
  createUncDegreeOfFreedom(branch: Object): Observable<Object> {
    return this.http.post(`${SystemConstants.CREATE_UNC_DEGREE_OF_FREEDOM_URL}`, branch);
  }




}
