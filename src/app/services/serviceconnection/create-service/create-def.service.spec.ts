import { TestBed, inject } from '@angular/core/testing';

import { CreateDefService } from './create-def.service';

describe('CreateDefService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateDefService]
    });
  });

  it('should be created', inject([CreateDefService], (service: CreateDefService) => {
    expect(service).toBeTruthy();
  }));
});
