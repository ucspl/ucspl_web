
import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';
import {trigger, state, animate, style, transition} from '@angular/core';

@Injectable()
export class ConfigService {
	apiURL:string;
	constructor() {
		this.apiURL = "http://localhost/saNG4-Demo-App/api/";
	}

}

export function optionalValidator(validators?: (ValidatorFn | null | undefined)[]): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } => {

		return control.value ? Validators.compose(validators)(control) : null;
	};
}

export class ValidationService {   //


	static emailValidator(control) {
		// RFC 2822 compliant regex
		if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
			return null;
		} else {
			return { 'invalidEmailAddress': true };
		}
	}

	static phoneValidator(control) {
		// RFC 2822 compliant regex
		if (control.value.match(/^((\\+91-?)|0)?[0-9]{10}$/)) {
			return null;
		} else {
			return { 'invalidPhoneNumber': true };
		}
	}
	

	static companyNameValidator(control){
		var nameCheck=control.value.toUpperCase();
		var pvt=" pvt";
		var pvtDot=" pvt.";
		var ltd=" ltd";
		var ltdDot=" ltd.";
		var privt=" private"
		var limited=" limited"
		var pvtLimited=" Private limited"
		if (nameCheck.match(pvt.toUpperCase()) || nameCheck.match(pvtDot.toUpperCase()) || nameCheck.match(ltd.toUpperCase()) || nameCheck.match(ltdDot.toUpperCase()) || nameCheck.match(privt.toUpperCase()) || nameCheck.match(limited.toUpperCase()) || nameCheck.match(pvtLimited.toUpperCase())) {
			return { 'invalidname': true };
		} else {
			return null;
		}
	}

	static nameValidator(control) {
		// RFC 2822 compliant regex   ///^[a-zA-Z. ]*[a-zA-Z]{2,60}$/
		if (control.value.match(/^[a-zA-Z0-9]+[(?<=\w\s]([a-zA-Z0-9-_!@#$%^&*\.]+\s)*[a-zA-Z0-9]+$/)) {
			return null; 
		} else {
			return { 'invalidname': true };
		}
	}

	static numberValidator(control) {
		// RFC 2822 compliant regex   ///^[a-zA-Z. ]*[a-zA-Z]{2,60}$/
		if (control.value.match(/^\d*\.?\d{0,20}$/)) {
			return null; 
		} else {
			return { 'invalidname': true };
		}
	}



	static passwordValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if(control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) //  .....atlest 6 character inlcuding atleast one number
		//("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$") //atleast one lower case,one symbol and one number..atleast 8 character
		//("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$")---one uppercase,atleast one lower case,one symbol and one number..atleast 8 character
		
		 {
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}


	static panNumberValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if (control.value.match(/^([A-Z]){5}([0-9]){4}([A-Z]){1}$/)) {              ///([A-Z]){5}([0-9]){4}([A-Z]){1}$/
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}


	static aadharNoValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if (control.value.match(/^\d{12}$/)) {
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}

	static lastThreeDigitOfGSTValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if (control.value.match(/^[1-9A-Z]{1}Z[0-9A-Z]{1}$/)) {
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}

	static StateCodeValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if (control.value.match(/^[0-9]{2}$/)) {
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}

	static GSTValidator(control) {
		// {6,100}           - Assert password is between 6 and 100 characters
		// (?=.*[0-9])       - Assert a string has at least one number
		if (control.value.match(/^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}$/)) {
			return null;
		} else {
			return { 'invalidPassword': true };
		}
	}



	static checkLimit(min: number, max: number): ValidatorFn {
		return (c: AbstractControl): { [key: string]: boolean } | null => {
			if (c.value && (isNaN(c.value) || c.value < min || c.value > max)) {
				return { 'range': true };
			}
			return null;
		};
	}
}



export function routerTransition() {
	return slideToLeft();
}

function slideToLeft() {
	return trigger('routerTransition', [
		transition(':enter', [
			style({transform: 'translateX(100%)', position:'fixed', width:'100%'}),
			animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
			]),
		transition(':leave', [
			style({transform: 'translateX(0%)', position:'fixed', width:'100%'}),
			animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
			])
		]);
}




